/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.scriptcheckrunlogictree;

import pacJnvmApi.NSApi;

public class runlogic
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the Flow1 class
    Flow1 Flow1Obj = new Flow1();
    //Initialise the JsonXML class
    JsonXML JsonXMLObj = new JsonXML();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence block - Start
        //Executing flow - Flow1
        Flow1Obj.execute(nsApi);
        //Executing flow - JsonXML
        JsonXMLObj.execute(nsApi);

        //logging
        nsApi.ns_end_session();
    }

}