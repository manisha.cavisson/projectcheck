/*-----------------------------------------------------------------------------
    Name: JsonXML
    Recorded By: Manisha
    Date of recording: 06/04/2021 01:13:38
    Flow details:
    Build details: 4.6.1 (build# 7)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.scriptcheckrunlogictree;
import pacJnvmApi.NSApi;

public class JsonXML implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("colors_json");
        status = nsApi.ns_web_url ("colors_json",
            "URL=http://65.49.37.71:9100/json/colors.json",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("colors_json", NS_AUTO_STATUS);
    

        status = nsApi.ns_start_transaction("Book_xml");
        status = nsApi.ns_web_url ("Book_xml",
            "URL=http://65.49.37.71:9100/xml/Book.xml",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("Book_xml", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.642);


          //Page Auto split for Image Link 'SignOff Button' Clicked by User
        status = nsApi.ns_start_transaction("welcome2");
        status = nsApi.ns_web_url ("welcome2",
        //URLId = 53, TimeStamp (Start = 35.353 secs, End = 35.37 secs, Resp Time = 0.017 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/welcome",
            "HEADER=Upgrade-Insecure-Requests:1******{ParamJSon}***{ParamXML}***",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 54, TimeStamp (Start = 35.42 secs, End = 35.466 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 55, TimeStamp (Start = 35.43 secs, End = 35.477 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 56, TimeStamp (Start = 35.436 secs, End = 35.487 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 57, TimeStamp (Start = 35.451 secs, End = 35.529 secs)
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 58, TimeStamp (Start = 35.459 secs, End = 35.537 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(2.45);


        return status;
    }
}
