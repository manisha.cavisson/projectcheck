/*-----------------------------------------------------------------------------
    Name: runlogic 
    runlogic details:
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_new123;

import pacJnvmApi.NSApi;

public class runlogic
{

    // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
    //Start - List of used flows in the runlogic
    //Initialise the FlowNew123 class
    FlowNew123 FlowNew123Obj = new FlowNew123();
    //Initialise the flow2 class
    flow2 flow2Obj = new flow2();
    //Initialise the flow3 class
    flow3 flow3Obj = new flow3();
    //End - List of used flows in the runlogic

    public void execute(NSApi nsApi) throws Exception
    {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence block - Start
        //Executing flow - FlowNew123
        FlowNew123Obj.execute(nsApi);
        //Executing flow - flow2
        flow2Obj.execute(nsApi);
        //Executing flow - flow3
        flow3Obj.execute(nsApi);

        //logging
        nsApi.ns_end_session();
    }

}