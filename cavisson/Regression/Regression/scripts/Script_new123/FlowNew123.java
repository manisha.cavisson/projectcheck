/*-----------------------------------------------------------------------------
    Name: FlowNew123
    Recorded By: cavisson
    Date of recording: 05/31/2021 11:37:33
    Flow details:
    Build details: 4.6.1 (build# 4)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_new123;
import pacJnvmApi.NSApi;

public class FlowNew123 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_web_url ("index_html",
            "URL=http://10.10.30.102:8111/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_page_think_time(2.769);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_web_url ("login",
            "URL=http://10.10.30.102:8111/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=45&login.y=19&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

     
        status = nsApi.ns_page_think_time(1.157);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_web_url ("reservation",
            "URL=http://10.10.30.102:8111/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_page_think_time(10.563);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("SearchFlight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?depart=Acapulco&departDate=06-01-2021&arrive=Acapulco&returnDate=06-02-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=40&findFlights.y=23",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("SearchFlight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(13.916);

        //Page Auto split for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("FlightSelect");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C06-01-2021&hidden_outboundFlight_button1=001%7C0%7C06-01-2021&hidden_outboundFlight_button2=002%7C0%7C06-01-2021&hidden_outboundFlight_button3=003%7C0%7C06-01-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=87&reserveFlights.y=11",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("FlightSelect", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.851);

        return status;
    }
}
