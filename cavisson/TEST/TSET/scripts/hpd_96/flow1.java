/*-----------------------------------------------------------------------------
    Name: flow1
    Recorded By: cavisson
    Date of recording: 06/03/2021 01:52:05
    Flow details:
    Build details: 4.6.1 (build# 7)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.kljhv;
import pacJnvmApi.NSApi;

public class flow1 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome_1",
            "URL=http://10.10.30.96/cgi-bin/welcome",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9"
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login_1",
            "URL=http://10.10.30.96/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=sam&password=sam%40123&login.x=22&login.y=8&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(2.608);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
            "URL=http://10.10.30.96/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(12.808);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://10.10.30.96/cgi-bin/findflight?depart=Frankfurt&departDate=06-04-2021&arrive=Acapulco&returnDate=06-05-2021&numPassengers=1&seatPref=Aisle&seatType=First&findFlights.x=48&findFlights.y=4",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(9.402);

        //Page Auto split for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://10.10.30.96/cgi-bin/findflight?hidden_outboundFlight_button0=100%7C1920%7C06-04-2021&hidden_outboundFlight_button1=101%7C1746%7C06-04-2021&hidden_outboundFlight_button2=102%7C1833%7C06-04-2021&outboundFlight=button3&hidden_outboundFlight_button3=103%7C1571%7C06-04-2021&numPassengers=1&advanceDiscount=&seatType=First&seatPref=Aisle&reserveFlights.x=35&reserveFlights.y=23",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.769);

        //Page Auto split for Image Link 'buyFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://10.10.30.96/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=First&seatPref=Aisle&outboundFlight=103%7C1571%7C06-04-2021&advanceDiscount=&buyFlights.x=74&buyFlights.y=18&.cgifields=saveCC",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/images/bookanother.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.96/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("plugins_win_json");
        status = nsApi.ns_web_url ("plugins_win_json",
            "URL=https://www.gstatic.com/chrome/config/plugins_3/plugins_win.json",
            "HEADER=Sec-Fetch-Site:none",
            "HEADER=Sec-Fetch-Mode:no-cors"
        );

        status = nsApi.ns_end_transaction("plugins_win_json", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.264);

        return status;
    }
}
