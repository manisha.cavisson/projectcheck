/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 06/03/2021 03:58:16
    Flow details:
    Build details: 4.6.1 (build# 7)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_check123;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_html");
        status = nsApi.ns_web_url ("index_html",
            "URL=http://10.10.30.96/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("index_html", NS_AUTO_STATUS);
    
    //Page Auto split for Method = HEAD
        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=http://mnidnsnenh/",
            "METHOD=HEAD"
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
    
    //Page Auto split for Method = HEAD
        status = nsApi.ns_start_transaction("index_2");
        status = nsApi.ns_web_url ("index_2",
            "URL=http://fkafeihicqbac/",
            "METHOD=HEAD"
        );

        status = nsApi.ns_end_transaction("index_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = HEAD
        status = nsApi.ns_start_transaction("index_3");
        status = nsApi.ns_web_url ("index_3",
            "URL=http://cdzmeqwxbtbbv/",
            "METHOD=HEAD",
            INLINE_URLS,
                "URL=http://10.10.30.96/tours/index.html", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("plugins_win_json");
        status = nsApi.ns_web_url ("plugins_win_json",
            "URL=https://www.gstatic.com/chrome/config/plugins_3/plugins_win.json",
            "HEADER=Sec-Fetch-Site:none",
            "HEADER=Sec-Fetch-Mode:no-cors"
        );

        status = nsApi.ns_end_transaction("plugins_win_json", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(174.026);

        //Page Auto split for 
        status = nsApi.ns_start_transaction("index_html_2");
        status = nsApi.ns_web_url ("index_html_2",
            "URL=http://65.49.37.71:9100/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_html_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.316);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login",
            "URL=http://65.49.37.71:9100/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=40&login.y=11&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.944);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
            "URL=http://65.49.37.71:9100/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.544);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://65.49.37.71:9100/cgi-bin/findflight?depart=Acapulco&departDate=06-04-2021&arrive=Acapulco&returnDate=06-05-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=61&findFlights.y=13",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.376);

        //Page Auto split for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://65.49.37.71:9100/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C06-04-2021&hidden_outboundFlight_button1=001%7C0%7C06-04-2021&hidden_outboundFlight_button2=002%7C0%7C06-04-2021&hidden_outboundFlight_button3=003%7C0%7C06-04-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=66&reserveFlights.y=14",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(2.46);

        //Page Auto split for Image Link 'buyFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://65.49.37.71:9100/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C06-04-2021&advanceDiscount=&buyFlights.x=68&buyFlights.y=5&.cgifields=saveCC",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.099);

        //Page Auto split for Image Link 'SignOff Button' Clicked by User
        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome",
            "URL=http://65.49.37.71:9100/cgi-bin/welcome",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://65.49.37.71:9100/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.843);

        return status;
    }
}
