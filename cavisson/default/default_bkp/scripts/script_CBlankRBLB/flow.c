/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 05/26/2021 01:27:47
    Flow details:
    Build details: 4.6.0 (build# 73)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.102:8111/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.102:8111/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=36&login.y=2&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8*******{BlankLBRB}******88",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(2.199);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.102:8111/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8****{BlankRB}*******",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(1.877);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.102:8111/cgi-bin/findflight?depart=Acapulco&departDate=05-27-2021&arrive=Acapulco&returnDate=05-28-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=72&findFlights.y=11",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8*******{BlankLB}**88",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(1.289);

    //Page Auto split for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.102:8111/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-27-2021&hidden_outboundFlight_button1=001%7C0%7C05-27-2021&hidden_outboundFlight_button2=002%7C0%7C05-27-2021&hidden_outboundFlight_button3=003%7C0%7C05-27-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=62&reserveFlights.y=13",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(1.733);

    //Page Auto split for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.102:8111/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C05-27-2021&advanceDiscount=&buyFlights.x=44&buyFlights.y=10&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(15.763);

    //Page Auto split for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.102:8111/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SL_Cookie",
        INLINE_URLS,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
            "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(8.112);

}
