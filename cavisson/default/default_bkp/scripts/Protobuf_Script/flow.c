   
/*------------------------------------------------------------------------------548
    Name: flow
    Recorded By: cavisson
    Date of recording: 04/29/2019 03:40:09
    Flow details:
    Build details: 4.1.14 (build# 109)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
	

    ns_start_transaction("index_html");
   ns_web_url ("index_html",
      // "URL=http://{URL_Parameter}/tiny",
      "URL=http://10.10.30.96/url_service/url_test_1.xml?RespSameAsReq=Y",
      //"URL=http://10.10.30.41:7010/tours/index.html",
       //  "URL=https://10.10.30.17/test.html",
        //"URL=https://10.10.30.17/tours/index.html",
       // "URL=https://10.10.30.17/protobuftesting.xml",
        // "URL=http://10.10.70.8:9007/url_service/url_test.xml?RespSameAsReq=Y",

        //   "URL=http://10.10.30.41:7010/Noida.html",
       "HEADER=Content-Type:application/x-protobuf",
     // "RespProtoFile=data_2.proto",
     // "RespProtoMessageType=Address",
      "ReqProtoFile=data_2.proto",
      "ReqProtoMessageType=Address",
      "BODY=$CAVINCLUDE$=data_2.xml"
        );
    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(2);
}
