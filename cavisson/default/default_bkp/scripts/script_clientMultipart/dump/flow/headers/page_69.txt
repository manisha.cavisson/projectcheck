
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/json
server: SAP
content-length: 17572
cache-control: public, max-age=21856783
expires: Thu, 06 Jan 2022 06:17:20 GMT
date: Wed, 28 Apr 2021 06:57:37 GMT
timing-allow-origin: *
x-cache-akamai: stable
----


--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/fiori/flp-controls.js
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 52760
cache-control: public, max-age=21856380
expires: Thu, 06 Jan 2022 06:15:58 GMT
date: Wed, 28 Apr 2021 07:02:58 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ushell/components/homepage/Component-preload.js
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 15642
cache-control: public, max-age=22285560
expires: Tue, 11 Jan 2022 05:28:58 GMT
date: Wed, 28 Apr 2021 07:02:58 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
content-length: 367097
cache-control: public, max-age=22285881
expires: Tue, 11 Jan 2022 05:28:57 GMT
date: Wed, 28 Apr 2021 06:57:36 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
content-length: 336522
cache-control: public, max-age=21856702
expires: Thu, 06 Jan 2022 06:15:58 GMT
date: Wed, 28 Apr 2021 06:57:36 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
content-length: 323457
cache-control: public, max-age=21856759
expires: Thu, 06 Jan 2022 06:16:55 GMT
date: Wed, 28 Apr 2021 06:57:36 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
content-length: 387533
cache-control: public, max-age=21856845
expires: Thu, 06 Jan 2022 06:18:21 GMT
date: Wed, 28 Apr 2021 06:57:36 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

