--Request 
GET https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D
Host: help.sap.com
Host: help.sap.com
Connection: keep-alive
Accept: application/json
Origin: https://my304874.s4hana.ondemand.com
X-CSRF-Token: Fetch
Access-Control-Allow_CORS: true
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200 OK
UACP-Server-Response-Date: 2021-04-28 02:54:26 GMT+0000
Content-Type: application/json;charset=utf-8
Server: SAP
Strict-Transport-Security: max-age=31536000; includeSubDomains; preload
Vary: Accept-Encoding
Content-Encoding: gzip
Expires: Wed, 28 Apr 2021 06:58:04 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Wed, 28 Apr 2021 06:58:04 GMT
Content-Length: 45
Connection: keep-alive
Content-Security-Policy: frame-ancestors *
Access-Control-Expose-Headers: X-CSRF-Token,Access-Control-Expose-Headers,iFrame
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: GET,POST,OPTIONS
Access-Control-Allow-Origin: https://my304874.s4hana.ondemand.com
----
--Request 
OPTIONS https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D
Host: education.hana.ondemand.com
Host: education.hana.ondemand.com
Connection: keep-alive
Access-Control-Request-Method: GET
Origin: https://my304874.s4hana.ondemand.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Access-Control-Request-Headers: access-control-allow_cors
Accept: */*
Sec-Fetch-Site: same-site
Sec-Fetch-Mode: cors
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
Cache-Control: no-cache, no-store, must-revalidate
Expires: 0
Access-Control-Expose-Headers: X-CSRF-Token,Access-Control-Expose-Headers,iFrame
Vary: Origin
Access-Control-Allow-Origin: https://my304874.s4hana.ondemand.com
Access-Control-Allow-Credentials: true
Access-Control-Allow-Headers: access-control-allow_cors
Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS, HEAD
Allow: GET, PUT, POST, DELETE, HEAD, OPTIONS
Content-Length: 0
Date: Wed, 28 Apr 2021 06:58:04 GMT
Keep-Alive: timeout=20
Connection: keep-alive
Server: SAP
Strict-Transport-Security: max-age=31536000; includeSubDomains; preload
----

