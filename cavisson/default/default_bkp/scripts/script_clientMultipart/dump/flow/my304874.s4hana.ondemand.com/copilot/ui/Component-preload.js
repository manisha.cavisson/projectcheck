<!DOCTYPE html>
<html>
<head>    
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<FONT SIZE=1 color=#146E88 >-51-
			s4-ccd-stl-shp-z3-
			No target system found-
			my304874.s4hana.ondemand.com-
			Wed Apr 28 06:57:44 2021
</FONT>
    <title>System currently not available</title>
    <style>
		.bodyBg {
			width: 90%;
			text-align: center;
			background-image: url("/img-err/BG.png");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-position: center top;
			background-color: #195a86;
			font-family:Verdana,Sans-serif;
			color: #f2f2f2;
			font-style: normal;
			margin: 0 auto;
		}

		#centerText {
			font-size: 26px;
			padding-bottom: 5%;
		}

		#goodbyeText {
			font-size: 48px;
			color: #f2f2f2;
		}

		.normalText {
		    text-align: left;
			font-size: 16px;
			color: #f2f2f2;
			padding-left: 5%;
			padding-right: 5%;
		}

		#bottomText {
			font-size: 14px;
			color: #ffffff;
		}

		#biggerBottomText {
			font-size: 16px;
			color: #ffffff;
		}

		#header {

		}

		#footer {
			width:99%;
			text-align:center;
			padding-top: 40px;
			bottom: 0;
			left: 0;
		}

		#content {
			display: inline-block;
			padding-top: 2%;
		}

		#footerLeft {
			float:left;
			padding-left: 5%;
			padding-bottom: 20px;
		}

		#footerRight {
			float:right;
			padding-right: 5%;
			bottom: 0;
			right: 0;
			padding-bottom: 20px;
		}

		#footerSystemInformation {
			width:99%;
			text-align:center;
			padding-top: 40px;
			bottom: 0;
			left: 0;
			color: #555555;
			font-size: 10px;
		}

		a {
				color: #f2f2f2;
		}

		table {
			border-collapse: collapse;
			text-align:left;
			width:70%;
			margin-left:15%;
			margin-right:15%;
		}

		th, td {
			padding: 5px;
			border: 1px solid #f2f2f2;
			color: #f2f2f2;
			font-size: 14px;
}
    </style>
</head>

<body class="bodyBg" id="logoffBody" >
		<div id="header"></div>

		<div id="content">
			<img src="./../../../my304874.s4hana.ondemand.com/img-err/Icon.png" alt="Icon">

			<div>
				<p id="centerText"><span id="goodbyeText">Sorry</span><br>System Maintenance / Temporary System Downtime<br>
				</p>
			</div>

			<div>
				<p class="normalText">
				Dear SAP User,	<br><br>
				Your system is currently unavailable.<br>
				This may be due to a required maintenance on the system that was scheduled within your agreed maintenance window. We invite you to access your system after the maintenance window.
				</p>


				<p class="normalText" id="variableText">
				If any other issue, please be informed that we are currently working on having your system back online as soon as possible.<br>
				You can contact us via our support channels at <a href="https://service.sap.com/call1sap">https://service.sap.com/call1sap</a> if you require immediate assistance or have any question.<br>
				</p>

				<p class="normalText">
				We apologize for any inconvenience.<br><br>
				Best regards,<br>
				SAP Cloud Support
				</p>
			</div>

			<div id="footer">
				<div id="footerLeft">
					<img src="./../../../my304874.s4hana.ondemand.com/img-err/Logo.png" alt="Logo">
				</div>

				<div id="footerRight">
					<p id="bottomText"><span id='biggerBottomText'>&copy; <script>document.write( new Date().getUTCFullYear() );</script></span> SAP SE, All rights reserved.</p>
				</div>

				<div id="footerSystemInformation">
				</div>
			</div>
		</div>


		<script>
                        var solexpSystems = [
                                        ["my300055","E0W"],
										["my300187","E4L"],
										["my300180","E4M"],
										["my300434","EBO"],
										["my300436","EBQ"],
										["my300450","EBS"],
										["my300456","EBY"],
										["my301361","MD2"],
										["my301857","OH7"],
										["my302392","QAO"],
										["my302459","QCQ"],
										["my300018","O5P"],
										["my300047","E0T"],
										["my300156","E3T"],
										["my300155","E3U"],
										["my300174","E4D"],
										["my300191","E4R"],
										["my300211","E5I"],
										["my300232","E5Z"],
										["my300271","E6L"],
										["my300289","E7D"],
										["my300356","E94"],
										["my300405","EAD"],
										["my300403","EAE"],
										["my300408","EAF"],
										["my300448","EAY"],
										["my300446","EAZ"],
										["my300473","EC2"],
										["my300578","EFT"],
										["my302265","PNK"],
										["my301800","OFY"],
										["my301799","OG0"],
										["my301832","OH9"],
										["my302247","Q1G"],
										["my302246","Q1H"],
										["my303233","RA9"],
										["my303234","RAA"],
										["my300183","E4Q"],
										["my300181","E4W"],
										["my300378","E8O"],
										["my300428","EBB"],
										["my300444","EBD"],
										["my300458","EBH"],
										["my300658","EHX"],
										["my301209","M8A"],
										["my301198","M8J"],
                                ];

                        for(var x = 0; x<solexpSystems.length; x++){
                                if(window.location.href.includes(solexpSystems[x][0])){

                                var centerText = document.getElementById('centerText');
                                var pElement = document.getElementById('variableText');

                                centerText.innerHTML = '<span id="goodbyeText">Sorry</span><br>Demo System Maintenance / Temporary Demo System Downtime<br>';

                                pElement.innerHTML =
                                    'If any other issue, please be informed that we are currently working on having your system back online as soon as possible.<br>' +
                                    'As this is a demo system, please check the Solution Experience <a target="_blank" href="https://jam4.sapjam.com/wiki/show/e3X5L2e2f1uCkq7Urf7I2B">'+
                                    'S/4HANA Cloud Demo Jam</a> for issue details and possible workarounds.';
                                }
                        }
        </script>
</body>
</html>
