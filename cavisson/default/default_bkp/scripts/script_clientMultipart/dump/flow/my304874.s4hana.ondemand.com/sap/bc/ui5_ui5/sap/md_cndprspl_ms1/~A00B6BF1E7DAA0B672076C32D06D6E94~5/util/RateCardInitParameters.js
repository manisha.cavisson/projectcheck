/*
 * Copyright (C) 2009-2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(function(){"use strict";var R=function(){this.oLocaleData=null;this.sProjectId=null;this.sProjectKey=null;this.sProjectName=null;this.dStartDate=null;this.dEndDate=null;this.sSalesOrg=null;this.sDistributionChannel=null;this.bEditMode=null;this.checkChangesInBackend=false;this.sProjectCurrency=null;};return R;},true);
