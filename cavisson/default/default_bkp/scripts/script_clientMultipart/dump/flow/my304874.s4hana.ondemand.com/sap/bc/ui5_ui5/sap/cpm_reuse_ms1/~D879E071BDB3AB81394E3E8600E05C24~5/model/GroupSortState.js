/*
 * Copyright (C) 2009-2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(["sap/ui/base/Object","sap/ui/model/Sorter"],function(B,S){"use strict";return B.extend("cus.cpm.lib.projectengagement.model.GroupSortState",{constructor:function(v){this._oViewModel=v;},sort:function(k){var g=this._oViewModel.getProperty("/groupBy");if(g!=="None"){this._oViewModel.setProperty("/groupBy","None");}return[new S(k,true)];},group:function(k){var s=[];if(k==="None"){this._oViewModel.setProperty("/sortBy","ChangedOn");}else{this._oViewModel.setProperty("/sortBy",k);s.push(new S(k,true,function(c){return c.getProperty(k);}));}return s;}});});
