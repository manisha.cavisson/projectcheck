sap.ui.require(["sap/ui/core/Component","sap/m/Button","sap/m/MessageToast"],function(e,t,n){
"use strict"
;return e.extend("sap.wdr.UIPluginAdaptUI.Component",{
metadata:{
manifest:"json"
},
init:function(){
var e=this._getRenderer()
;this.i18n=this.getModel("i18n").getResourceBundle(),e.i18n=this.i18n,
sap.wdr.UIPluginAdaptUI.Component.aPostMessage=[],
this._getContainer().getService("AppLifeCycle").attachAppLoaded(this._onAppLoaded,this),
e.then(function(e){
e.addActionButton("sap.m.Button",{
id:"wdrAdaptUI",
icon:"sap-icon://wrench",
text:this.i18n.getText("adapt_button"),
press:function(e){
sap.wdr.UIPluginAdaptUI.Component.prototype._navigateToCustomizingMode()||n.show("Error: There is no available navigation target")
}
},!0,!1,[sap.ushell.renderers.fiori2.RendererExtensions.LaunchpadState.App]).setVisible(!1)
;var t=sap.ushell.Container.getService("CrossApplicationNavigation")
;t&&t.isInitialNavigation()&&sap.wdr.UIPluginAdaptUI.Component.prototype._onAppLoaded()
})
},
_isCustomizingMode:function(){
var e=sap.ushell.Container.getService("CrossApplicationNavigation")
;if(!e)return!1
;var t=e.hrefForExternal()
;if(t.length<=0)return!1
;var n=t.split("?")
;return!(!n[1]||-1===n[1].indexOf("sap-config-mode=A"))
},
_navigateToCustomizingMode:function(){
var e=sap.ushell.Container.getService("CrossApplicationNavigation")
;if(!e)return!1
;var t=e.hrefForExternal()
;if(t.length<=0)return!1
;var n=t.split("-"),i={
target:{
semanticObject:n[0].substring(1,n[0].length),
action:n[1]
},
params:{
"sap-config-mode":"A"
}
}
;return e.toExternal(i),!0
},
_getApplConfigId:function(){
for(var e=this._getSrc().split("?"),t=e[1],n=t.split("&"),i="",r=0;r<n.length;r++){
var a=n[r].split("=")
;if("sap-wd-configId"===a[0]){
i=a[1]
;break
}
}
if(""===i)for(n=(t=e[0]).split("/"),r=0;r<n.length;r++){
if("wda"===n[r]&&n[r+1]){
i=n[r+1]
;break
}
if("webdynpro"===n[r]&&n[r+2]){
i=n[r+2]
;break
}
}
return i
},
_getSrc:function(){
var e=sap.ushell.Container.getService("NavTargetResolution").getCurrentResolution()
;if(e)return e.url
},
_getFrame:function(){
for(var e=document.getElementsByTagName("IFRAME"),t=0;t<e.length;t++)if(e[t].src&&(-1!==e[t].src.indexOf("app/wda/")||-1!==e[t].src.indexOf("bc/webdynpro/")))return e[t]
},
_receivePostMessage:function(e){
if(e&&e.origin===window.location.origin&&e.data){
try{
var t=JSON.parse(e.data)
}catch(e){
return
}
;if(t&&"response"===t.type&&"sap.wdr.services.ApplWhiteList.check"===t.service)t.body&&t.body.IsAllowed?sap.wdr.UIPluginAdaptUI.Component.prototype._adaptButtonVisibility("wdrAdaptUI",!0):sap.wdr.UIPluginAdaptUI.Component.prototype._adaptButtonVisibility(
"wdrAdaptUI",!1),
window.removeEventListener("message",this._receivePostMessage,!1);else if(e.source){
var n=sap.wdr.UIPluginAdaptUI.Component.aPostMessage
;if(n&&n.length>0){
for(var i=0;i<n.length;i++)e.source.postMessage(JSON.stringify(n[i]),window.location.origin)
;sap.wdr.UIPluginAdaptUI.Component.aPostMessage=[]
}
}
}
},
_getUUID:function(){
var e=(new Date).getTime()
;return"id-xxxxxxxyxxxxx-0".replace(/[xy]/g,function(t){
var n=(e+16*function(){
var e=window.crypto||window.msCrypto
;if(e){
var t=new Uint8Array(1)
;return 10/e.getRandomValues(t)[0]
}
return Math.random()
}())%16|0
;return e=Math.floor(e/16),("x"==t?n:3&n|8).toString(16)
})
},
_checkWhiteList:function(){
var e=this._getApplConfigId()
;if(this._adaptButtonVisibility("wdrAdaptUI",!1),""!==e){
window.addEventListener("message",this._receivePostMessage,!1)
;var t={
type:"request",
service:"sap.wdr.services.ApplWhiteList.check",
request_id:this._getUUID(),
body:{
sApplConfigId:e
}
}
;sap.wdr.UIPluginAdaptUI.Component.aPostMessage.push(t)
}
},
_checkWebdynproApp:function(){
var e=this._getCurrentRunningApplication()
;return"NWBC"===e.applicationType&&!e.homePage
},
_onAppLoaded:function(){
this._checkWebdynproApp()&&!this._isCustomizingMode()?this._checkWhiteList():this._adaptButtonVisibility("wdrAdaptUI",!1)
},
_getContainer:function(){
var e=jQuery.sap.getObject("sap.ushell.Container")
;if(!e)throw new Error("Illegal state: shell container not available; this component must be executed in a unified shell runtime context.")
;return e
},
_getCurrentRunningApplication:function(){
return this._getContainer().getService("AppLifeCycle").getCurrentApplication()
},
_adaptButtonVisibility:function(e,t){
"string"==typeof e&&(e=sap.ui.getCore().byId(e)),
e&&e.setVisible(t)
},
_getRenderer:function(){
var e,t=new jQuery.Deferred
;return this._oShellContainer=jQuery.sap.getObject("sap.ushell.Container"),
this._oShellContainer?(e=this._oShellContainer.getRenderer())?t.resolve(e):(this._onRendererCreated=function(n){
(e=n.getParameter("renderer"))?t.resolve(e):t.reject("Illegal state: shell renderer not available after recieving 'rendererLoaded' event.")
},
this._oShellContainer.attachRendererCreatedEvent(this._onRendererCreated)):t.reject("Illegal state: shell container not available; this component must be executed in a unified shell runtime context."),
t.promise()
}
})
});