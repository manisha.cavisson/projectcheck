/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/21/2021 12:43:09
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_Kohls;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
