<script id="pdpThirdParty" type="text/x-jsrender">
<script type="text/javascript">
    {{if $env.ishookLogicEnabled}}
        $requires($env.dfpProductionEnvironmentURL);
    {{else $env.isNewHookLogicEnabled}}
        $requires($env.hookLogicEndPointURL);
    {{/if}}
    if (Kjs.msie('le 10')) { // https://sites.google.com/a/kohls.com/kjs/usage/api/core/guts/kjs-msie
        $requires('deploy/compatability.js');
    }
    if (Kjs.msie()) {
        $load.css('ie.css');
    }
{{:"<"}}/script>

<script type="text/javascript">
        //This jsp include contains javascript variables for checkout PB
        $env.diginotify_banner = {{:$env.diginotify_banner}};
        $env.diginotify_popover = {{:$env.diginotify_popover}};
      //No need of adding "script tag" since this page included as a script file only in WCS
        $env.inValidatePBOrderOmniChannel = $tf('{{:$env.inValidatePBOrderOmniChannel}}');	// inValidatePBOrderOmniChannel - This is used to clear PB cache
        $env.isGuestUser = Kjs.cookie.get("VisitorUsaFullName")? false : true; // loginStatus - true/false
        $env.pbEnabled = $tf('{{:$env.pbEnabled}}');	// PB Killswitch
    {{:"<"}}/script>
    
    {{if $env.bigDataEnabled == "true" || $env.bigDataEnabled == true }}
    <script type="text/javascript">
        $ready(function _$ready_bdRendering($) {
            var bdURL = "{{v:$env.bdRenderingEndPoint}}/update/ede/assets/experiences/{{v:$env.bdRenderingChannelName}}/bd-experience-rendering-sdk.min.js";
            $requires(bdURL,$requires.K.foreign, {
            onLoad: function(error) {
                    if (!error) $env('loadBigDataPDP');	
                }
            });
        }, { isAfterReady: true });
        {{:"<"}}/script>
    {{/if}}

    <script type="text/javascript" language="JavaScript">
                 var pageName = "regularProductPage";
    	{{:"<"}}/script>
		{{if $env.bvConversion}}
			{{if $env.enablePDPUIChanges}}
				<script>$requires('{{:$env.newBvConversionUrl}}', $requires.K.foreign);{{:"<"}}/script>
			{{else}}
				<script>$requires('{{:$env.bvConversionUrl}}', $requires.K.foreign);{{:"<"}}/script>
			{{/if}}
			<script type="text/javascript">
				window.bvCallback = function (BV) {
					BV.reviews.on('show', function () {
						sc_bvTabOpen();
					});
					BV.questions.on('show', function () {
						sc_bvQATabOpen();
					});
				};
        	{{:"<"}}/script>
        {{/if}}
        
		<script type="text/javascript">
			$requires('//assets.pinterest.com/js/pinit.js', $requires.K.standalone);
			/*
			(function(d){
					var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
					p.type = 'text/javascript';
					p.async = true;
					p.src = 'http://assets.pinterest.com/js/pinit.js';
					f.parentNode.insertBefore(p, f);
			}(document));
			*/
		{{:"<"}}/script>

        <script type="text/javascript" charset="utf-8">
			(function() {
				_googCsa = window._googCsa || function() {
					(_googCsa.q = _googCsa.q || []).push(arguments)
				};
				_googCsa.t = 1 * new Date;
				$requires('//www.google.com/adsense/search/async-ads.js', $requires.K.foreign);
			})();
		{{:"<"}}/script>

        <script type="text/javascript" charset="utf-8">
	$ready(function _$ready_trendage($) {
		var urlSkuId = getParameterByName(location.href, 'skuId');
		var skuMatch = true;
        urlSkuId = urlSkuId === "" ? getParameterByName(location.href, 'skuid') : urlSkuId ;
		for(var i=0; i < productV2JsonData.SKUS.length; i++) {
			if (productV2JsonData.SKUS[i].skuCode == urlSkuId) {
				skuMatch = false;
				return false;
			}
		}
		if (skuMatch) {
			if (
				$env.enableTrendageGrid &&
				window.completeTheLook == "gridViewCTL" &&
				productV2JsonData.preSelectedSku
			  ) {
				var gridURL =
				  $env.trendateTopURL +
				  "&id=" +
				  productV2JsonData.preSelectedSku +
				  "&target=trendage_grid&_v=" +
				  Math.random();
				$requires(gridURL, $requires.K.foreign);
			  }
		
			  if (
				$env.enableTrendageCarousal &&
				window.completeTheLook == "newCarouselCTL" &&
				productV2JsonData.preSelectedSku
			  ) {
				var carousalURL =
				  $env.trendateBottomURL +
				  "&id=" +
				  productV2JsonData.preSelectedSku +
				  "&target=trendage_carousal&_v=" +
				  Math.random();
				$requires(carousalURL, $requires.K.foreign);
			  }
		}
	}, { isAfterReady: true });	
{{:"<"}}/script>
	
    <script type="text/javascript">
     $ready(function _$ready_navigation($){
		      var customerNameCookie = Kjs.cookie.get( 'VisitorUsaFullName' );
			  if(customerNameCookie == '' || customerNameCookie == null ){
                       $('div#notLoggedInUser').show();
                       $('div#LoggedInUser').hide();
			  }
			  else{
                         $('div#LoggedInUser').show();
                         $('div#notLoggedInUser').hide();
			   }
		    });
	{{:"<"}}/script>

    <script type="text/javascript">
			if (typeof getCookie == 'undefined') {
				getCookie = function(cname) {
					var name = cname + "=";
					var ca = document.cookie.split(';');
					for(var i=0; i<ca.length; i++) {
						var c = ca[i];
						while (c.charAt(0)==' ') c = c.substring(1);
						if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
					}
					return "";
				};
			}
			// googletag is a global variable
			var googletag = window.googletag || {};
			googletag.cmd = googletag.cmd || [];

			(function() {
				$env('Kjs', function() {
					$requires('//www.googletagservices.com/tag/js/gpt.js', $requires.K.foreign);
				});
			})();
		{{:"<"}}/script>
        
		{{if $env.page == "prdNotAvailablePage" && $env.trackingServerSide && $env.siteCatalystEnabled}}
	
		<script type="text/javascript">
		$init(function _$init_sessionomniture($) {
		window.localStorage.removeItem("breadcrumbClicked");
		var RRTrackingBean = new Object();
		RRTrackingBean.userSessionId = $env.appId;
		RRTrackingBean.profileId = Kjs.cookie.get("VisitorId");
	$err.exec(function _sitecatalyst() { // [EAEV-12]
		s.currencyCode="USD";
		if(rewardshome_omniture)
			s.pageName="";
		else{
			if(reDesignSignIn || myWalletSection)
				s.pageName="";
			else if(typeof manageOrder != 'undefined' && manageOrder)
				s.pageName="";
			else
				s.pageName="";
		}
		s.pageType="";
		s.prop1="";
		s.prop2="";
		s.prop3="";
		s.prop4="";
		s.prop5="";
		s.prop6="";
		s.prop7="";
		s.prop9="";
		s.prop10="";
		s.prop11="";
		s.prop16="";
		s.prop17=$env.userStatus + "|" + $env.lyStatus;
		s.prop22=$env.sysDate;
		s.prop24="";
		//s.prop25="";
		s.prop25="";
		s.prop37="";
		s.prop38="";
		s.prop43="";
		s.prop44="";
		s.prop45="";
		s.prop46="";
		s.prop50="D=s_tempsess";
		s.prop52="";
		if(rewardshome_omniture)
		s.prop53="";
		else if(reDesignSignIn)
		s.prop53="D=pageName";
		else if(typeof manageOrder != 'undefined' && manageOrder)
		s.prop53="";
		else
		s.prop53="";
		pageType="";
		if(reDesignSignIn || pageType=="Checkout" || pageType=="Confirmation"){
			if(typeof isMVTNuDataEnable != "undefined"){
				if(isMVTNuDataEnable && trJsonData.isNuDataEnabled){
					if(pageType=="Confirmation" && dfpscoreBand){
						s.prop60 = "ND:";
					}else if(pageType=="Checkout"){
						s.prop60 = "ND:ON";
					}else if(reDesignSignIn){
						s.prop60 = "ND:ON";
					}else{
						s.prop60 = "ND:OFF";
					}
				}else{
					s.prop60 = "ND:OFF";
				}
			}else{
				s.prop60 = "ND:OFF";
			}
		}
		s.prop55="";
		s.events="";
		s.products="";
		s.channel="";
		s.eVar3="";
		//s.eVar5="";
		s.eVar5="";
		s.eVar14="";
		s.eVar8="";
		s.eVar17=$env.userStatus + "|" + $env.lyStatus;
		s.eVar18=$env.sysTime;
		s.eVar19=$env.sysDay;
		s.eVar20=$env.sysDayType;
		s.eVar22="desktop";
		s.eVar23="";
		s.eVar24="";
		s.eVar25="";
		s.eVar26="";
		s.eVar27="";
		s.eVar28="";
		s.eVar35="";
		s.eVar36="";
		if(pageNotAvail)
			s.eVar39="";
		else
			s.eVar39=Kjs.cookie.get("VisitorUsaFullName")!="" ? Kjs.cookie.get("VisitorId") :"no customer id";
		s.eVar40="cloud17";
		s.eVar46="";
		s.eVar47="";
		s.eVar48="";
		s.eVar59="";
		s.eVar60="";
		s.eVar61="";
		s.eVar66="";
		s.eVar67="";
		var isCartEmpty = Kjs.cookie.get("VisitorBagTotals") ? false : true;
		var loggedinProfile = Kjs.cookie.get("VisitorUsaFullName")!="" ? Kjs.cookie.get("VisitorId") :"";
		if(isCartEmpty){
			s.eVar42="no cart";
		}
		else if(loggedinProfile == "" && !isCartEmpty)
		{
				s.eVar42="guest";
		}
		else{
			s.eVar42="registered";
		}
		if(rewardshome_omniture)
			s.eVar68="";
		else if(reDesignSignIn)
			s.eVar68="D=pageName";
		else if(typeof manageOrder != 'undefined' && manageOrder)
			s.eVar68="";
		else
			s.eVar68="";
		s.eVar22 = "desktop";
		s.eVar4="";
		if(pageNotAvail)
			s.eVar70="";
		else
			s.eVar70=Kjs.cookie.get("VisitorUsaFullName")!="" ?"" : Kjs.cookie.get("VisitorId");
		if(pageNotAvail)
			s.eVar71="";
		else
			s.eVar71="klsbrwcki|" + Kjs.cookie.get("VisitorId");
		if(pageNotAvail)
			s.eVar73="";
		else
			s.eVar73="no loyalty id";
		s.state="";
		s.zip="";
		s.purchaseID="";
		if(myWalletSection){
			s.eVar22="desktop";
			s.eVar3="wallet";
			var tab = window.location.hash;
			var walletSection = "your offers";
			switch(tab){
				case '#yourWallet':
					walletSection = "using wallet";
					break;
				case '#y2y-rewards':
					walletSection = "yes2you";
					break;
				case '#kohls-cash':
					walletSection = "kc and rewards";
					break;
				case '#gift-cards':
					walletSection = "gift cards";
					break;
			}
			s.pageName = s.pageName+walletSection;
			s.prop53 = s.eVar68 = s.pageName;
		}
		var s_code=s.t();if(s_code)document.write(s_code);
	});
});
{{:"<"}}/script>
{{/if}}

		{{if $env.enable_googleDFP_product_details_page_728x90 || $env.enable_googleDFP_product_details_page_160x600 || $env.enable_googleDFP_product_details_page_970x90_AND_728x90_AND_1024_top || $env.enable_googleDFP_product_details_page_300x250_bottomleft || $env.enable_googleDFP_product_details_page_300x250_bottommiddle || $env.enable_googleDFP_product_details_page_300x250_bottomright || $env.enable_googleDFP_product_details_page_wallpaper || $env.enable_googleDFP_product_details_page_super_leaderboard}}
			<script type="text/javascript">
				if(getCookie('disableDFPDisplayAds') === '' || getCookie('disableDFPDisplayAds') === 'false'){
					{{if $env.enable_googleDFP_product_details_page_728x90}}
						Kjs.dfp_ads.slot("dfp_leaderboard_728x90", {type: 'slot',
							adUnit: "/{{:$env.googleDFP_networkID}}{{:productV2JsonData.monetizationData.adUnit}}",
							sizes: [[728, 90]],targeting: {"pos": "bottom"}
						});
					{{/if}}

					{{if $env.enable_googleDFP_product_details_page_300x250_bottomleft}}
						Kjs.dfp_ads.slot("dfp_medium_rectangle_300x250_bottomleft", {type: 'slot',
							adUnit: "/{{:$env.googleDFP_networkID}}{{:productV2JsonData.monetizationData.adUnit}}",
							sizes: [[300,250]],targeting: {"pos": "bottomleft"}
						});
					{{/if}}

					{{if $env.enable_googleDFP_product_details_page_300x250_bottommiddle}}
						Kjs.dfp_ads.slot("dfp_medium_rectangle_300x250_bottommiddle", {type: 'slot',
							adUnit: "/{{:$env.googleDFP_networkID}}{{:productV2JsonData.monetizationData.adUnit}}",
							sizes: [[300,250]],targeting: {"pos": "bottommiddle"}
						});
					{{/if}}

					{{if v:$env.enable_googleDFP_product_details_page_300x250_bottomright}}
						Kjs.dfp_ads.slot("dfp_medium_rectangle_300x250_bottomright", {type: 'slot',
							adUnit: "/{{:$env.googleDFP_networkID}}{{:productV2JsonData.monetizationData.adUnit}}",
							sizes: [[300,250]],targeting: {"pos": "bottomright"}
						});
					{{/if}}

					{{if v:$env.enable_googleDFP_product_details_page_wallpaper}}
						Kjs.dfp_ads.slot("dfp_wallpaper_1x1", {
							type: 'oop',adUnit: "/{{:$env.googleDFP_networkID}}{{:productV2JsonData.monetizationData.adUnit}}",targeting: {"pos": "wallpaper"}
						});
					{{/if}}

					if(window.test_and_target_bucketnames) {
						Kjs.dfp_ads.registerTargets({"tnt_buckets": window.test_and_target_bucketnames});
					}
					Kjs.dfp_ads.registerTargets({"channel":"desktop"});
					Kjs.dfp_ads.registerTargets({"pgtype":"pdp"});
					Kjs.dfp_ads.registerTargets({"env":"{{:$env.googleDFPenvironment}}"});
					{{props v:productV2JsonData.adParameterMap}}
						Kjs.dfp_ads.registerTargets({"{{>key}}":"{{>prop}}"});
					{{/props}}
				}
			{{:"<"}}/script>

        {{/if}}
        
        {{for ~populatebloomreach(productV2JsonData.SKUS, selectedSkuId, $env.productInfoBeanisActive) }}
		<script type="text/javascript" charset="utf-8">
			var br_data = {};
			/* --- Begin parameters section: fill in below --- */
			br_data.acct_id = "{{:~root.$env.brDataAccId}}";
			br_data.ptype = "product";
			br_data.prod_id = "{{:~root.productV2JsonData.webID}}";
			br_data.sku = "{{v:#data.skuId}}";
			br_data.prod_name = "{{v:~root.productV2JsonData.productTitle}}";
			br_data.sale_price ="{{v:#data.salePrice}}";
			br_data.price = "{{v:#data.regularPrice}}";
			br_data.pstatus = "{{v:#data.pstatus}}";
			/* --- End parameter section --- */


			(function() {
				var src = ('https:' == document.location.protocol) ? 'https://cdns.brsrvr.com/v1/br-trk-5117.js' : 'http://cdn.brcdn.com/v1/br-trk-5117.js';
				$requires(src, $requires.K.standalone);
			})();
		{{:"<"}}/script>
		{{/for}}

		{{if v:$env.brightTagEnabled}}
	<script type="text/javascript">
			var ishookLogicEnabled = "{{:$env.hooklogicEnabled}}";
			if(window.enableHookLogic==false){
				ishookLogicEnabled ="false";
			}
			var placement = null;
			var placementA = document.getElementById('hl_1_999');
			var placementB = document.getElementById('hl_2_999');
			if(placementA!=null){
					placement = "hl_1_999";
			}
			if(placementB!=null){
					placement = "hl_2_999";
			}
			if(placementA!=null && placementB!=null){
					placement = "hl_1_999,hl_2_999";
			}
			var pagetype ="pdpPage";
		{{:"<"}}/script>
{{/if}}



</script>