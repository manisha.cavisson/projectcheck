/*-----------------------------------------------------------------------------
    Name: flow3
    Recorded By: cavisson
    Date of recording: 05/21/2021 01:07:55
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_Kohls;
import pacJnvmApi.NSApi;

public class flow3 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_8");
        status = nsApi.ns_web_url ("index_8",
            "URL=http://www.kohls.com/",
            "REDIRECT=YES",
            "LOCATION=https://www.kohls.com/",
            INLINE_URLS,
                "URL=https://www.kohls.com/", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environ3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/newHomepagestyle.css", END_INLINE,
                "URL=https://www.kohls.com/akam/11/7d34ecfd", END_INLINE,
                "URL=https://www.kohls.com/media/images/global-header-refresh-icons/order-status-icon.png", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/global-header-refresh-icons/search-icon.svg", END_INLINE,
                "URL=https://s.go-mpulse.net/boomerang/4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/gh-test-shoppingcart?scl=1&fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20190729-hp-od-kcash-offer-bg?fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-kc-bkg02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_3", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_3", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.default.theme.min.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_4", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_4", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn10?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn20?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_09", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-dads-rail-bkg1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-golf-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210401-beauty-half", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-bopus-utility", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-bopus-utility", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-2020w1226-rewardsplate", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-social2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-social2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-kck", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/AppStore-qr_code2020?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_hero", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210405-kck-top", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp-dt-e12-20190408-leaf?fmt=png8&scl=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json");
        status = nsApi.ns_web_url ("config_json",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405275&v=1.720.0&if=&sl=0&si=add04714-f5e3-44cb-b049-fbb1f8d64517-qtg545&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159"
        );

        status = nsApi.ns_end_transaction("config_json", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("pixel_7d34ecfd");
        status = nsApi.ns_web_url ("pixel_7d34ecfd",
            "URL=https://www.kohls.com/akam/11/pixel_7d34ecfd",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("pixel_7d34ecfd", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json");
        status = nsApi.ns_web_url ("mobilemenu_json",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_2");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_2",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_3");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_3",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://dpm.demdex.net/id?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621582569222", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/id/rd?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621582569222", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rd");
        status = nsApi.ns_web_url ("rd",
            "URL=https://dpm.demdex.net/id/rd?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621582569222",
            INLINE_URLS,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("rd", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_4");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_4",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/id?d_visid_ver=5.2.0&d_fieldgroup=A&mcorgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&mid=84795902680038641394405446550017377092&ts=1621582569552", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery");
        status = nsApi.ns_web_url ("delivery",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://cm.everesttech.net/cm/dd?d_uuid=89726096032446728673750329446651071084", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdi6gAAAMG4uCaq", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp");
        status = nsApi.ns_web_url ("session_jsp",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdi6gAAAMG4uCaq", END_INLINE,
                "URL=https://idsync.rlcdn.com/365868.gif?partner_uid=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://aa.agkn.com/adscores/g.pixel?sid=9211132908&aam=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://ib.adnxs.com/getuid?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D358%26dpuuid%3D%24UID", "REDIRECT=YES", "LOCATION=https://ib.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fdpm.demdex.net%252Fibs%253Adpid%253D358%2526dpuuid%253D%2524UID", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=21&dpuuid=164570303793000736257", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_static.js", END_INLINE,
                "URL=https://ib.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fdpm.demdex.net%252Fibs%253Adpid%253D358%2526dpuuid%253D%2524UID", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=358&dpuuid=2174005878281702669", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=358&dpuuid=2174005878281702669", END_INLINE,
                "URL=https://idsync.rlcdn.com/1000.gif?memo=CKyqFhIxCi0IARCYEhomODk3MjYwOTYwMzI0NDY3Mjg2NzM3NTAzMjk0NDY2NTEwNzEwODQQABoNCOrFnYUGEgUI6AcQAEIASgA", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=477&dpuuid=3a9b95aab4e0a8e1692717cd5b3577131fa36ee0a06ca0d012660a1ec37e0f64b0da87c991749652", END_INLINE,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp?isHome=true", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=0&jsession=&ref=&scriptVersion=1.11.2&dyid_server=Dynamic%20Yield&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm&gdpr=0&gdpr_consent=&google_hm=ODk3MjYwOTYwMzI0NDY3Mjg2NzM3NTAzMjk0NDY2NTEwNzEwODQ=", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.min.js", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s64262244003255?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A10%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-false&mcidcto=-false&aidcto=-false&.mcid&.c&cc=USD&pageType=homepage&c4=homepage&c9=homepage%7Chomepage&c10=homepage&c11=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=homepage&c64=VisitorAPI%20Present&v68=homepage&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%3A04c7b2a4-9eb5-41be-bf31-441b23617ef2&v86=68&v87=hp19&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://cdn.dynamicyield.com/scripts/1.11.2/dy-coll-nojq-min.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm=&gdpr=0&gdpr_consent=&google_hm=ODk3MjYwOTYwMzI0NDY3Mjg2NzM3NTAzMjk0NDY2NTEwNzEwODQ=&google_tc=", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCe9bf84b227de49e19116aff6da1d63ec-source.min.js", END_INLINE,
                "URL=https://navdmp.com/req?adID=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s64262244003255?AQB=1&pccr=true&vidn=3053B175BA7DF942-600006ECB012BDA4&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A10%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-false&mcidcto=-false&aidcto=-false&.mcid&.c&cc=USD&pageType=homepage&c4=homepage&c9=homepage%7Chomepage&c10=homepage&c11=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=homepage&c64=VisitorAPI%20Present&v68=homepage&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%3A04c7b2a4-9eb5-41be-bf31-441b23617ef2&v86=68&v87=hp19&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=771&dpuuid=CAESEPDzHvg95eKWsOKp8KHVhLc&google_cver=1?gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC45c202be87f24f4b869276b8ad2213dd-source.min.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC7d3594a211b44b26a76f4645d4b39315-source.min.js", END_INLINE,
                "URL=https://dp2.33across.com/ps/?pid=897&random=1723995428", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf6d45831b2294615923f29a2042b366f-source.min.js", END_INLINE,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;12462;1480;iframe/?spotName=Homepage&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U10=N/A&cachebuster=147029.3625524426", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=601&dpuuid=116994355400325&random=1621582571", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC6e9a66d466e14e7595ac7a2d79c6ca6d-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8f39aead6ce84f38a17325c1cde458bc-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities");
        status = nsApi.ns_web_url ("userAffinities",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=6440147674919822059",
            INLINE_URLS,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://d.impactradius-event.com/A375953-1cd4-4523-a263-b5b3c8c11fb81.js", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.everesttech.net%2F1x1%3F", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537072980%26val%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fib.adnxs.com%2Fpxj%3Faction%3Dsetuid(%27__EFGSURFER__.__EFGCK__%27)%26bidder%3D51%26seg%3D2634060der%3D51%26seg%3D2634060", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fexpires%3D30%26nid%3D2181%26put%3D__EFGSURFER__.__EFGCK__%26v%3D11782", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE,
                "URL=https://cdn.navdmp.com/req?adID=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%3D%26piggybackCookie%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621582571541&mi_u=anon-1621582571535-2013629335&mi_cid=8212&page_title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&timezone_offset=-330&event_type=pageview&cdate=1621582571535&ck=false&anon=true", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/cav_nv.js?v=REL20170123", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities_2");
        status = nsApi.ns_web_url ("userAffinities_2",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=6440147674919822059",
            INLINE_URLS,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkaTZnQUFBTUc0dUNhcQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349");
        status = nsApi.ns_web_url ("X349",
            "URL=https://kohls.sjv.io/xc/385561/362119/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=348520&msn=webserve-b85027d.use&uid=6440147674919822059&sec=8776374&t=ri&e=1100747&p=1&ve=10123479&va=%5B26082590%5D&ses=f92493d279704577a598f2d3468cebde&expSes=42381&aud=1408117.1362540.1362542&expVisitId=-7338859663556444369&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621582571253&rri=1536428", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=408688&msn=webserve-b85027d.use&uid=6440147674919822059&sec=8776374&t=ri&e=1096558&p=1&ve=10092862&va=%5B26047526%5D&ses=f92493d279704577a598f2d3468cebde&expSes=42381&aud=1408117.1362540.1362542&expVisitId=-7338859666072486190&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621582571179&rri=8980709", END_INLINE,
                "URL=https://async-px.dynamicyield.com/id?cnst=1&msn=webserve-b85027d.use&uid=6440147674919822059&sec=8776374&cuid=04c7b2a4-9eb5-41be-bf31-441b23617ef2&cuidType=atg_id&reqts=1621582571136&rri=6242838&_=1621582571169", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=229688&msn=webserve-b85027d.use&uid=6440147674919822059&sec=8776374&t=ri&e=1102794&p=1&ve=10133783&va=%5B26094881%5D&ses=f92493d279704577a598f2d3468cebde&expSes=42381&aud=1408117.1362540.1362542&expVisitId=-7338859666925328920&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621582571260&rri=1126890", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_2");
        status = nsApi.ns_web_url ("batch_2",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582571248_715102",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia");
        status = nsApi.ns_web_url ("uia",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582571171",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE,
                "URL=https://idpix.media6degrees.com/orbserv/hbpix?pixId=16873&pcv=70&ptid=66&tpuv=01&tpu=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/dotcom/config.js?v=REL20170123", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F&H=1lm7uu8", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEOzcKGL5lLUrFvFPeBV3ev8&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782&google_gid=CAESEOzcKGL5lLUrFvFPeBV3ev8&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEOzcKGL5lLUrFvFPeBV3ev8&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F&google_gid=CAESEOzcKGL5lLUrFvFPeBV3ev8&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060&google_gid=CAESEOzcKGL5lLUrFvFPeBV3ev8&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmb/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://mpp.vindicosuite.com/sync/?pid=27&fr=1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=903&dpuuid=39b7b682-b183-48f1-a9f9-71eee4300ca4", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/73cde545-49e3-4b30-c59f-6908958798f0?deviceId=73cde545-49e3-4b30-c59f-6908958798f0&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=https://idsync.rlcdn.com/422866.gif?partner_uid=99999999999999&", END_INLINE,
                "URL=https://d9.flashtalking.com/d9core", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=https://servedby.flashtalking.com/map/?key=a74thHgsfK627J6Ftt8sj5ks52bKe&gdpr=0&gdpr_consent=&url=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=[%FT_GUID%]&gdpr=0&gdpr_consent=", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=48891ECCF12A1B&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/xh0;;pixel/?name=Homepage_2019", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE,
                "URL=https://ps.eyeota.net/match?bid=6j5b2cv&uid=89726096032446728673750329446651071084&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=/match/bounce/?bid=6j5b2cv&uid=89726096032446728673750329446651071084&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=48891ECCF12A1B&gdpr=0&gdpr_consent=", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences");
        status = nsApi.ns_web_url ("experiences",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ps.eyeota.net/match/bounce/?bid=6j5b2cv&uid=89726096032446728673750329446651071084&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2OTT1cDfzgS3vXwKhF9lqF2adc__u_SHHay0hcvhdPJQ", END_INLINE,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc");
        status = nsApi.ns_web_url ("lgc",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2OTT1cDfzgS3vXwKhF9lqF2adc__u_SHHay0hcvhdPJQ", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://cms.analytics.yahoo.com/cms?partner_id=ADOBE&_hosted_id=89726096032446728673750329446651071084&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=30646?dpuuid=y-lTevCgNE2pG5F_EKWSl70JSlrJ0DSjFRDzo-~A", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=https://px.owneriq.net/eucm/p/adpq?redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D53196%26dpuuid%3D(OIQ_UUID)", "REDIRECT=YES", "LOCATION=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748689731465519030&uid=Q6748689731465519030&ref=%2Feucm%2Fp%2Fadpq", END_INLINE,
                "URL=https://fei.pro-market.net/engine?site=141472;size=1x1;mimetype=img;du=67;csync=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connect/73cde545-49e3-4b30-c59f-6908958798f0?deviceId=73cde545-49e3-4b30-c59f-6908958798f0&os=html5&devicetype=desktop&loadConfig", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=575&dpuuid=4110083167401953868", END_INLINE,
                "URL=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748689731465519030&uid=Q6748689731465519030&ref=%2Feucm%2Fp%2Fadpq", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748689731465519030", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748689731465519030", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/118811_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4777679_Multi_Color_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3509904_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4887344_Yellow_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047073_Rose_Dye?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047068_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3500577_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4462500?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://ads.scorecardresearch.com/p?c1=9&c2=6034944&c3=2&cs_xi=89726096032446728673750329446651071084&rn=1621582570158&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D89726096032446728673750329446651071084", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3955549?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4528551_Empire_Red?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3874658?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4525714_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:1f1c1ea80b0e48d388d36c2aac50d6f9&seg=xh0", END_INLINE,
                "URL=https://www.googleadservices.com/pagead/conversion_async.js", END_INLINE,
                "URL=https://ads.scorecardresearch.com/p2?c1=9&c2=6034944&c3=2&cs_xi=89726096032446728673750329446651071084&rn=1621582570158&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D89726096032446728673750329446651071084", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=89726096032446728673750329446651071084", END_INLINE
        );

        status = nsApi.ns_end_transaction("lgc", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop");
        status = nsApi.ns_web_url ("floop",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_2");
        status = nsApi.ns_web_url ("floop_2",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_3");
        status = nsApi.ns_web_url ("floop_3",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_4");
        status = nsApi.ns_web_url ("floop_4",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621582573853&cv=9&fst=1621582573853&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621582573861&cv=9&fst=1621582573861&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://bttrack.com/dmp/adobe/user?dd_uuid=89726096032446728673750329446651071084", "REDIRECT=YES", "LOCATION=//dpm.demdex.net/ibs:dpid=49276&dpuuid=3584638e-ccb0-4e56-917c-22a0eff5d3a1", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/c=9828/tp=ADBE/tpid=89726096032446728673750329446651071084?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=49276&dpuuid=3584638e-ccb0-4e56-917c-22a0eff5d3a1", END_INLINE,
                "URL=https://www.googletagservices.com/tag/js/gpt.js", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/ct=y/c=9828/tp=ADBE/tpid=89726096032446728673750329446651071084?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=g8f47s39e399f3fe&google_push&google_sc&google_hm=WUtkaTZnQUFBTUc0dUNhcQ==", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", "METHOD=OPTIONS", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://6249496.collect.igodigital.com/collect.js", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621582573861&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2467601077&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621582573853&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=822768741&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rid");
        status = nsApi.ns_web_url ("rid",
            "URL=https://match.adsrvr.org/track/rid?ttd_pid=casale&fmt=json&p=184399",
            INLINE_URLS,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621582573861&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2467601077&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621582573853&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=822768741&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=121998&dpuuid=532e4fabd045e56b5ef09b05014e6eb2", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://abp.mxptint.net/sn.ashx", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1D530_DD6A22AE_600270B0&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
                "URL=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdi6gAAAMG4uCaq", "REDIRECT=YES", "LOCATION=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdi6gAAAMG4uCaq&C=1", END_INLINE,
                "URL=https://api.rlcdn.com/api/identity?pid=2&rt=envelope", END_INLINE,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=https://ib.adnxs.com/setuid?entity=158&code=YKdi6gAAAMG4uCaq", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", END_INLINE,
                "URL=https://usermatch.krxd.net/um/v2?partner=adobe&id=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?id=537148856&val=YKdi6gAAAMG4uCaq", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1D530_DD6A22AE_600270B0&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D", END_INLINE,
                "URL=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdi6gAAAMG4uCaq&C=1", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_2021051701.js?31061225", END_INLINE,
                "URL=https://aorta.clickagy.com/pixel.gif?ch=124&cm=89726096032446728673750329446651071084&redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D79908%26dpuuid%3D%7Bvisitor_id%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?cc=1&id=537148856&val=YKdi6gAAAMG4uCaq", END_INLINE,
                "URL=https://sync.ipredictive.com/d/sync/cookie/generic?https://dpm.demdex.net/ibs:dpid=2340&dpuuid=${ADELPHIC_CUID}", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=3936ec3d-ba07-11eb-bea9-6b82a1f0ea83", END_INLINE,
                "URL=https://pixel.rubiconproject.com/tap.php?v=7941&nid=2243&put=YKdi6gAAAMG4uCaq&expires=90", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=3936ec3d-ba07-11eb-bea9-6b82a1f0ea83", END_INLINE,
                "URL=https://image2.pubmatic.com/AdServer/Pug?vcode=bz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA=&piggybackCookie=YKdi6gAAAMG4uCaq", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdi6gAAAMG4uCaq&img=1", "REDIRECT=YES", "LOCATION=/partner?adv_id=6409&uid=YKdi6gAAAMG4uCaq&img=1&__user_check__=1&sync_id=3976cfd3-ba07-11eb-85bd-1aa2b20d0507", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdi6gAAAMG4uCaq&img=1&__user_check__=1&sync_id=3976cfd3-ba07-11eb-85bd-1aa2b20d0507", END_INLINE
        );

        status = nsApi.ns_end_transaction("rid", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest");
        status = nsApi.ns_web_url ("bidRequest",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_bottom_728x90&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_9");
        status = nsApi.ns_web_url ("index_9",
            "URL=https://hb.emxdgt.com/?t=1000&ts=1621582574867",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("prebid");
        status = nsApi.ns_web_url ("prebid",
            "URL=https://ib.adnxs.com/ut/v3/prebid",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("prebid", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator");
        status = nsApi.ns_web_url ("translator",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A93218267%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%228%22%2C%22siteID%22%3A%22269247%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%223%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%227%22%2C%22siteID%22%3A%22269241%22%7D%2C%22id%22%3A%224%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%2239b7b682-b183-48f1-a9f9-71eee4300ca4%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T07%3A36%3A14%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("translator", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_2");
        status = nsApi.ns_web_url ("bidRequest_2",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_3");
        status = nsApi.ns_web_url ("bidRequest_3",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_middle&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_4");
        status = nsApi.ns_web_url ("bidRequest_4",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1",
            INLINE_URLS,
                "URL=https://idx.liadm.com/idex/ie/any", END_INLINE
        );

        status = nsApi.ns_end_transaction("bidRequest_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction");
        status = nsApi.ns_web_url ("auction",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_2");
        status = nsApi.ns_web_url ("auction_2",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_bottom_leaderboard_header&lib=ix&size=728x90&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=89726096032446728673750329446651071084", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_3");
        status = nsApi.ns_web_url ("auction_3",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json");
        status = nsApi.ns_web_url ("fastlane_json",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811818&kw=rp.fastlane&tk_flint=index&rand=0.45812087575773464"
        );

        status = nsApi.ns_end_transaction("fastlane_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_2");
        status = nsApi.ns_web_url ("fastlane_json_2",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811822&kw=rp.fastlane&tk_flint=index&rand=0.034639840446528014"
        );

        status = nsApi.ns_end_transaction("fastlane_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_3");
        status = nsApi.ns_web_url ("fastlane_json_3",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811816&kw=rp.fastlane&tk_flint=index&rand=0.8467326802028134"
        );

        status = nsApi.ns_end_transaction("fastlane_json_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_4");
        status = nsApi.ns_web_url ("fastlane_json_4",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=2&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1817898&kw=rp.fastlane&tk_flint=index&rand=0.3512399294220765"
        );

        status = nsApi.ns_end_transaction("fastlane_json_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_4");
        status = nsApi.ns_web_url ("auction_4",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_ZxvkvDMU%22%2C%22site%22%3A%7B%22domain%22%3A%22www.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%225FTJOX8Y%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788162%22%7D%2C%7B%22id%22%3A%22W70Ay1LV%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788161%22%7D%2C%7B%22id%22%3A%221LUMGZuG%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788160%22%7D%2C%7B%22id%22%3A%228KSsOEi9%22%2C%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%7D%2C%22tagid%22%3A%22788159%22%7D%5D%7D", END_INLINE,
                "URL=https://mid.rkdms.com/bct?pid=8bc436aa-e0fc-4baa-9c9a-06fbeca87826&puid=89726096032446728673750329446651071084&_ct=img", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_5");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_5",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=129099&dpuuid=b9b6f09091f2e108115346eb94f229ee", END_INLINE,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("headerstats");
        status = nsApi.ns_web_url ("headerstats",
            "URL=https://as-sec.casalemedia.com/headerstats?s=186355&u=https%3A%2F%2Fwww.kohls.com%2F&v=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://16f5c8077778686046e0cf2942b88de6.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=4438825514018165&correlator=3057759669006294&output=ldjh&impl=fifs&eid=31060789%2C31060854%2C31061225%2C21068030&vrg=2021051701&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1&prev_iu_szs=1x1%2C320x50%7C1024x256%2C970x250%2C728x90%2C300x250%2C300x250%2C300x250%2C320x50%7C1440x630%2C320x50%7C1440x630&fluid=0%2Cheight%2C0%2C0%2C0%2C0%2C0%2Cheight%2Cheight&ists=256&prev_scp=pos%3Dwallpaper%7C%7Cpos%3Dbottom%7Cpos%3Dbottom%7Cpos%3Dbottomleft%7Cpos%3Dbottommiddle%7Cpos%3Dbottomright%7Cpos%3DHP_Desktop_Top_Super_Bulletin%7Cpos%3DHP_Desktop_Bottom_Super_Bulletin&cust_params=pgtype%3Dhome%26channel%3Ddesktop%26env%3Dprod&cookie_enabled=1&bc=31&abxe=1&lmt=1621582575&dt=1621582575799&dlt=1621582567186&idt=7600&frm=20&biw=1349&bih=607&oid=3&adxs=-9%2C-9%2C190%2C311%2C163%2C525%2C887%2C0%2C0&adys=-9%2C-9%2C8193%2C9533%2C9575%2C9575%2C9575%2C2549%2C5490&adks=1341520166%2C1971407845%2C2060426818%2C1304082129%2C4151307844%2C3063740786%2C4276857337%2C3603917613%2C1454221005&ucis=1%7C2%7C3%7C4%7C5%7C6%7C7%7C8%7C9&ifi=1&u_tz=330&u_his=2&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2F&vis=1&dmc=8&scr_x=0&scr_y=0&psz=0x-1%7C0x-1%7C1349x9298%7C1349x9298%7C300x54%7C300x54%7C300x54%7C1349x0%7C1349x9298&msz=0x-1%7C0x-1%7C1349x0%7C1349x0%7C300x0%7C300x0%7C300x0%7C1440x0%7C1440x0&ga_vid=207482848.1621582576&ga_sid=1621582576&ga_hid=545971000&ga_fc=false&fws=2%2C2%2C0%2C0%2C0%2C0%2C0%2C128%2C128&ohw=0%2C0%2C0%2C0%2C0%2C0%2C0%2C0%2C0&btvi=-1%7C-1%7C1%7C2%7C3%7C4%7C5%7C6%7C7", END_INLINE,
                "URL=https://tpc.googlesyndication.com/pagead/js/r20210517/r20110914/client/window_focus.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/15280555526910074681", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/16953867992244551473", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/14906849826852343313", END_INLINE,
                "URL=https://www.kohls.com/search.jsp?submit-search=web-regular&search=jeans&kls_sbp=84795902680038641394405446550017377092", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssDcyFL_SOOY-ueBElLw3ZlJL4vmXH_mHmSYvZ9w5YBoShqMiqsjrmuf-8J9CUexF_qpd5yxGXdt_K8wtyVNbXu_zLm7veMja5_RMVozljVOXcTQxz9DMDJmzJt9pew5rwSbIkUA_czPA-Wlwl74gBwu_dQoBycatSrC4x4c8mh72MJJpSV3IgDGBjOrIZblDH7AUD8mwonPZd-PRV_zKw3viHpdnes7QuYYlFCmhhD61QrP-_6IEhtUfF_LfBOM67MozR3luxh3VwUmGmUwEeA6CaXnelm&sig=Cg0ArKJSzPM9FowUJy6LEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvWAzC1kxoTfY5ZK2PtYstwOhx-ZMwCtldyJNCZ8D4YvLVzapAm-gDI5FEgAeDDC5pxVpTRSSmMcAaXR-5rMCuzHdFMKS5OyqlAiYrtBev_BYP6i85YuPFY7VRq1f6JA_FJf8VnUYXo9QNBakAFNV4cDsa-LtWbp9uH0kybIZJH36hqJ1iPlp8Vbxb7NkPpvU8DtbAZLOIb5_Du97EVMg9JdRM8ekB1UDdGFJMn70muj6SgHz4uxe5HMFTHMExsKE9_PRynmq8NJc5EYkhByb5laqy7yg6K&sig=Cg0ArKJSzIdl2LaEhIgSEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstO-Lc2Ex-kxWvlaO2WHs6kiENRJTHdNZX1kUE5yipttZdKpZi9pxZAQIA9g640czGV3YVEPKO-FJU-tqI5UKvCay7o-3FIFz-b0ksKDbVrlD8kPMmWQuCbfqGbzL_LysrPTVLU6BAsc_-xWMzBC3eiL2NHnKvCA6XkGQkADKJEXICruETjzDnqpCTLOChO8-vS0UGv1VEWCJs_lN1z_YRp3Xi6SQ4ElEW3eT6Wz_P4bbh1-JYa4KbiYupaPh2QCMfBE2moxfjbDhEM9rXTEzN5R_X8GlpG&sig=Cg0ArKJSzIyXfV01YH6NEAE&adurl=", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaRzVtLd9FUevZFspjht94umuTnc6SjjTATO6B14wX7w26KnDwmaNCY_e6qb9gJWh85sxaeMduftWECXwoaQiDnCLmahlg", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaRN7BIIK8vNdMGZEiA9k3Y2GAoyC53HMqhLRsdz9U3vxtGW9jAU1c2rvRaaFY3FAOQbAJzqeV0ZMOmvfvJxwk3EwhPmrg", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaTM1dLFOnF4Qod2qbcrCQAesp_N3gFHbvY3Biuk2--1KmDgr-2mt2tIj81xBoXyzDZU_0YiuwoKPQkiqI6Le4ejVku_SQ", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/osd.js", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/rx_lidar.js?cache=r20110914", END_INLINE
        );

        status = nsApi.ns_end_transaction("headerstats", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_6");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_6",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstcQAkewgbT0XeUtRS6qmEH6Ym5HJpPIViP3wozQ8O34pUTQUJHPn8UODMovdb23sDCW8LwTKhRjQij_5J1b_DlTSOi9KQ5_Q1AD3xaQYPWJlPdaGp94C80hI0s4LfoRtm4Ur2gn70HzWnrVQk69oC-0-PJ3zdeodCLa-jjWzptdtIhMZYc96h726FbHQxW_K9isALIZZZ4L0hmy1g38iZD3HU8_kw3Gw-bit_jlK-d8mBZ8LDng-jjMP4hBx5vZd_Qi3qnkP6o4sVFJOzgQxXObccEWOoksvA&sig=Cg0ArKJSzPsFOwaozqz4EAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsuR243guN6JpCGImYwYXtnqdpokqxOJztlzFvWVryIVRv0S-Xzz7zDvoJBKVnM2-z-3wASE1dAW5bBUxGxD6pFyIu4PHsmxuN2mjbXgvkdLpCfHXBvpNfBRIqzkZSGQhfPOjIaGqO89PJhKsMJPQsbLD0k0E3kE3kAfsliMOe10XP1Ag7DD1QBVlTNdTaBYzOyehKWl-JZURh4rCCuq5LD25yxVQwt5dSW1aYayIA1yMKpLSPG3TmZl0qsm5Bcyuqt1bZxYaBPNoLm6LrF8sDdEeqyNt-v4rzM&sig=Cg0ArKJSzNjGMgVgLqZIEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvaWq17wjyaSdGOn2z3TLrrXWk0a8oDVWc3fP1p0xn33BhYceUKY00H3NxRNu9TFhBDI4d74wQUMqBU0C5M917QhTWpDAzrNZQ_oXILoUEjEMDhF4EPxXzUeeNnJfMV2OlGYEdDLDhrufPFaiuu5sy2YkYgR2D3A2JUhRcN4hMzgMEr6sZNvF0BSkNSSu9jCmd6aHsnBjvKrlpGFfbJ4HM1PuZ9pAuKccBdEsvA4ljkctiFG_TtoH6vnNtq_dnRO4aRtDmb-9vp2-30mcRMxiqC4VYy7nH9ygE&sig=Cg0ArKJSzPZuTaSrFBunEAE&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Amobileapp&adcreativeid=138299836983&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=304968036", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138260694346&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=1985229867", END_INLINE,
                "URL=https://www.kohls.com/sale-event/denim.jsp?&searchTerm=jeans&submit-search=web-regular", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environment.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kjscore3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/ktag.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage1.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/fonts/hfjFonts.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/skava-custom.css", END_INLINE,
                "URL=https://widget.stylitics.com/v2/widget.js", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.base.min.css", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/css/style.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id");
        status = nsApi.ns_web_url ("id",
            "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=84795902680038641394405446550017377092&d_blob=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&ts=1621582578377",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsu6-0gzdsEsE7BJmr8GiYQTQLdaKmdSSx9l1aoO_KmGFSgMvSw235tQYm-lzGFMSHBG2abRmNinf0w6dDGeXo8YuZl1mhEnGGLDXvgkERs&sig=Cg0ArKJSzMHA4UuROrU8EAE&id=lidartos&mcvt=0&p=9501,311,9591,1039&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=1304082129&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621582576600&dlt=0&rpt=550&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjst0h9YJFFc34w1kb32oXnsINpsMJYFOtyhPyfmn3PjpRbGcCfZyEDaNuD0al_pVI0VcIKvvWdQpB1uq084i1RYu3xomoG0TTeCfKXq_W8M&sig=Cg0ArKJSzP1N_7HgCI-pEAE&id=lidartos&mcvt=0&p=9665,163,9915,463&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=4151307844&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621582576607&dlt=0&rpt=544&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjssAqd8GcgppjflQRCkBbZjtsDrAsFdH8HVDQ3VOTmHBJ_UeriGLXnQHgFvBEc8kXOxLX7xuoHr8l3SU_IZ5uegPi2E9uRVw3ViDCGatjJE&sig=Cg0ArKJSzD3AgIh5LlCmEAE&id=lidartos&mcvt=0&p=9665,525,9915,825&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=3063740786&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621582576607&dlt=0&rpt=547&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/js/main.js", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE
        );

        status = nsApi.ns_end_transaction("id", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(15.167);

        status = nsApi.ns_start_transaction("denim_jsp_3");
        status = nsApi.ns_web_url ("denim_jsp_3",
            "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/css/ipadcss.css", END_INLINE,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("denim_jsp_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("json");
        status = nsApi.ns_web_url ("json",
            "URL=https://kohls.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=15e43915071149cda04c993604a7f382&mboxPC=15e43915071149cda04c993604a7f382.31_0&mboxPage=7bd1277a42f34977a44bf57752a32923&mboxRid=ff62ab9c9eed42dc89632264246fe9d6&mboxVersion=1.7.1&mboxCount=1&mboxTime=1621602378424&mboxHost=www.kohls.com&mboxURL=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&mboxReferrer=https%3A%2F%2Fwww.kohls.com%2F&browserHeight=607&browserWidth=1366&browserTimeOffset=330&screenHeight=768&screenWidth=1366&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=ANGLE%20(Intel(R)%20UHD%20Graphics%20620%20Direct3D11%20vs_5_0%20ps_5_0)&at_property=bb529821-b52b-bf89-2022-4492a94a6d05&customerLoggedStatus=false&tceIsRedesign=false&tceIsPDPRedesign=&tceIsCNCRedesign=True&mboxMCSDID=13BD9754284DE169-3FEA97E3C78DE164&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=84795902680038641394405446550017377092&mboxAAMB=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&mboxMCGLH=12",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-main?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-boys?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-09?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-08?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-07?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE
        );

        status = nsApi.ns_end_transaction("json", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_5");
        status = nsApi.ns_web_url ("floop_5",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-womens?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-mens?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-girls?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-m?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-d?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_2");
        status = nsApi.ns_web_url ("mobilemenu_json_2",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_2");
        status = nsApi.ns_web_url ("config_json_2",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405275&v=1.720.0&if=&sl=1&si=61f2d8ee-12b0-4aa8-a8c9-e9373185f865-qtg545&bcn=%2F%2F684fc539.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159"
        );

        status = nsApi.ns_end_transaction("config_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_7");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_7",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_8");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_8",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2F&scriptVersion=1.11.2&dyid_server=6440147674919822059", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_2");
        status = nsApi.ns_web_url ("session_jsp_2",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("session_jsp_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(16.799);

        status = nsApi.ns_start_transaction("jeans_bottoms_clothing_jsp_3");
        status = nsApi.ns_web_url ("jeans_bottoms_clothing_jsp_3",
            "URL=https://www.kohls.com/catalog/jeans-bottoms-clothing.jsp?CN=Product:Jeans+Category:Bottoms+Department:Clothing&icid=denimlp-shopall",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("jeans_bottoms_clothing_jsp_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_9");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_9",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_2");
        status = nsApi.ns_web_url ("uia_2",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582580208",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_3");
        status = nsApi.ns_web_url ("batch_3",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582580310_439863",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits");
        status = nsApi.ns_web_url ("outfits",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_2_200622&return_object=true"
        );

        status = nsApi.ns_end_transaction("outfits", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_2");
        status = nsApi.ns_web_url ("outfits_2",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_1_200622&return_object=true",
            INLINE_URLS,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("outfits_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_3");
        status = nsApi.ns_web_url ("outfits_3",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_4_200622&return_object=true"
        );

        status = nsApi.ns_end_transaction("outfits_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_4");
        status = nsApi.ns_web_url ("outfits_4",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_3_200622&return_object=true",
            INLINE_URLS,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s67378860734058?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A21%205%20-330&d.&nsid=0&jsonv=1&.d&sdid=13BD9754284DE169-3FEA97E3C78DE164&mid=84795902680038641394405446550017377092&aamlh=12&ce=UTF-8&ns=kohls&pageName=denim&g=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=sale%20event%20landing&events=event1&products=%3Bproductmerch1&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&tnt=531276%3A0%3A0%2C545299%3A1%3A0%2C546041%3A0%3A0%2C536856%3A1%3A0%2C536968%3A1%3A0%2C526083%3A0%3A0%2C&c1=no%20taxonomy&c2=no%20taxonomy&c3=no%20taxonomy&v3=browse&c4=sale%20event%20landing&c5=non-search&c7=no%20taxonomy&v8=non-search&v9=homepage&c16=browse&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v23=browse&v24=browse&v25=no%20taxonomy&v26=no%20taxonomy&v27=no%20taxonomy&v28=no%20taxonomy&c39=browse&c40=browse&v40=cloud17&c41=browse&c42=browse&v42=no%20cart&c50=D%3Ds_tempsess&c53=denim&c64=VisitorAPI%20Present&v68=denim&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%3A04c7b2a4-9eb5-41be-bf31-441b23617ef2&c.&a.&activitymap.&page=denim&link=SHOP%20ALL&region=lp-denim&pageIDType=1&.activitymap&.a&.c&pid=denim&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartmen&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("outfits_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_2");
        status = nsApi.ns_web_url ("X349_2",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/kjscoretag3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/pmp_search_loader.gif", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/blank_1x1.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2560266_Secluded_Echo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Stonewash?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3647219_Medium_Indigo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3398441_Twister?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3215192_Heritage_Medium_Wash?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/BestSeller@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pmp_imgs/swatch-blank.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pmp_imgs/plus-circle.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/BuyOneGetOneHalfOff@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/KohlsExclusive@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/TopRated@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/up.svg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Shoestring?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/loading.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/118561_Sleek_Blue?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Majestic?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/ic-left-chevron.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/ic-right-chevron.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/product-rating-stars-sprite.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/plus.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/s_code.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_3");
        status = nsApi.ns_web_url ("config_json_3",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405275&v=1.720.0&if=&sl=1&si=61f2d8ee-12b0-4aa8-a8c9-e9373185f865-qtg545&bcn=%2F%2F684fc539.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/omniture_tracking.js", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/backToTop.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/productlistRevamp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/controller/ctl.pmp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/mobile/mobilemenu.tpl.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("config_json_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_3");
        status = nsApi.ns_web_url ("mobilemenu_json_3",
            "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/pmpSearchPageScriptsV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_10");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_10",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pmpSearchPageTmplV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_2");
        status = nsApi.ns_web_url ("id_2",
            "URL=https://dpm.demdex.net/id?d_visid_ver=5.2.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=84795902680038641394405446550017377092&d_blob=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&ts=1621582583024"
        );

        status = nsApi.ns_end_transaction("id_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_2");
        status = nsApi.ns_web_url ("delivery_2",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20210503-dm-bogo-half-badge?scl=1&amp;fmt=png8", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&scriptVersion=1.11.2&dyid_server=6440147674919822059&ctx=%7B%22type%22%3A%22CATEGORY%22%2C%22data%22%3A%5B%22Clothing%22%2C%22N%2FA%22%5D%7D", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/favorites/favorites.products.css", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_11");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_11",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_4");
        status = nsApi.ns_web_url ("batch_4",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582583555_90436",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_3");
        status = nsApi.ns_web_url ("uia_3",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582583568",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_5");
        status = nsApi.ns_web_url ("batch_5",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582583657_926953",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_3");
        status = nsApi.ns_web_url ("session_jsp_3",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp?CN=Gender:Womens+Product:Jeans+Category:Bottoms+Department:Clothing&icid=jeans-VN-women", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_12");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_12",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s6875927672962?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A24%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cjeans&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&r=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event40%2Cevent38%2Cevent155&c1=clothing&c2=clothing%3Ebottoms&v2=denimlp-shopall&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=jeans%7Cbottoms%7Cclothing%7Cpmp&v9=no%20value&c16=internal%20campaign&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cjeans&c28=denimlp-shopall&v28=bottoms%7Cclothing%7Cjeans&v29=featured%3A1%3A48&v35=denimlp-shopall&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cjeans%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cjeans&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&c75=w%3Ebg%7Cbest_seller&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7C%7C%7C1&c.&a.&activitymap.&page=bottoms%7Cclothing%7Cjeans&link=Women&region=vn-box&pageIDType=1&.activitymap&.a&.c&pid=Bottoms%7CClothing%7CJeans&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCateg&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("storesAvailabilitySearch");
        status = nsApi.ns_web_url ("storesAvailabilitySearch",
            "URL=https://www.kohls.com/snb/storesAvailabilitySearch?zipCode=&_=1621582582939",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/Common/NewBanner/", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/SpotLight/ff021745-7d11-4189-92a1-9afc2eaaa8ef/", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/check-empty.png", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.google.com/adsense/search/async-ads.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-01?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-02?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-03?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-04?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-05?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-sl-hs-20200622-jeans-visnav-06?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20191120-kcash-logo-right-img?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_The_Twist_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Medium_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Clif_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.google.com/afs/ads?adtest=off&channel=PMP_TwoLeftRail&cpp=0&hl=en&client=kohls-search&q=Product%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=7981621582585047&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=4&u_tz=330&dt=1621582585049&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi760sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&referer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular#master-1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.google.com/afs/ads?adsafe=high&adtest=off&cpp=0&client=vert-pla-kohls-srp&q=Product%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=1&pfcrncy=USD&pfmax=360&pfmin=73&theme=walleye&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&nocache=2681621582585061&num=0&output=uds_ads_only&v=3&preload=true&bsl=10&u_his=4&u_tz=330&dt=1621582585069&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=-wi760he265&cont=afshcontainer&inames=slave-0-2&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&referer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular#master-2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Indigo_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Chicago_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Hazelnut_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Madison_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Frisco_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Coffee_Roast_sw?wid=20&hei=20", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCe7a5c80123db4f88ba3a45b88d306fba-source.min.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Vintage_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Timberwolf_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Tumbled_Rigid_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Golden_Top_sw?wid=20&hei=20", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&H=nxouwa&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Black_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("storesAvailabilitySearch", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_6");
        status = nsApi.ns_web_url ("floop_6",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Stonewash_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_2");
        status = nsApi.ns_web_url ("experiences_2",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("experiences_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(21.801);

        status = nsApi.ns_start_transaction("womens_jeans_bottoms_clothin_3");
        status = nsApi.ns_web_url ("womens_jeans_bottoms_clothin_3",
            "URL=https://servedby.flashtalking.com/container/1638;101965;1478;iframe/?spotName=Category_Page&U1=5178,1695762,5172,4967470,4530067,3114244,1070546,2954787,2263298,1731442,3416624,2483201,2953086,396327,2143062,3028316,2982007,4438906,1989834,2310989,3706250,323407,4411494,381312,3264673,3944204,3315119,3416634,2933383,2560270,3022856,2225626,118561,2955090,5001943,1070694,1119487,4342530,4316598,4842207,3741815,3311620,186075,397582,3069612,2244568,3413782,2768739,3000711,2560286,4843744,3537089,1107,3893958,250792,3753184,4411439,3120097,2577481,4381083&U2=1234&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U10=N/A&cachebuster=283401.42850702745",
            INLINE_URLS,
                "URL=https://mon1.kohls.com/test_rum_nv?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=18&d=1|0|-1|2|1768|-1|-1|-1|1401|369|0|2381|0|4951|-1|2363|18|2363|0|1397|https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular|https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall|www.kohls.com|VisitorId%3D04c7b2a4-9eb5-41be-bf31-441b23617ef2%3B%20CavNV%3D4674162754846552467-33333-233048178033-0-9-0-1-145-63556-26487%3B%20AKA_RV2%3D44%3B%20AKA_CNC2%3DTrue%3B%20akacd_www-kohls-com-mosaic-p2%3D2177452799~rv%3D80~id%3De04f395af42c5b548280664818d80ca0%3B%20mosaic%3Dgcpb%3B%20at_check%3Dtrue%3B%20AMCVS_F0EF5E09512D2CD20A490D4D%2540AdobeOrg%3D1%3B%20s_ecid%3DMCMID%25PIPE%2584795902680038641394405446550017377092%3B%20_dy_csc_ses%3Dt%3B%20_dy_c_exps%3D%3B%20s_fid%3D470ED0FAFF43B1EA-3ACBA20E941EA296%3B%20s_cc%3Dtrue%3B%20_dycnst%3Ddg%3B%20s_vi%3D%5BCS%5Dv1%25PIPE%253053B175BA7DF942-600006ECB012BDA4%5BCE%5D%3B%20_dyid%3D6440147674919822059%3B%20_dyjsession%3De520494583b9eb872bf95fb8bcb6fc39%3B%20dy_fs_page%3Dwww.kohls.com%3B%20_dycst%3Ddk.w.c.ms.%3B%20_dy_geo%3DIN.AS.IN_RJ.IN_RJ_Jaipur%3B%20_dy_df_geo%3DIndia..Jaipur%3B%20_mibhv%3Danon-1621582571535-2013629335_8212%3B%20IR_gbd%3Dkohls.com%3B%20_gcl_au%3D1.1.1177027742.1621582573%3B%20SignalSpring2016%3DA%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMDM%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMTA%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMDYyMTU%3DREFZUw%3B%20btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg%3DNjQ0Mzg1Njk3MzExMTQ4NjgwMw%3B|0|101|-1|24|24|WINDOWS|Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F56.0.2924.87%20Safari%2F537.36|en-GB|%5Bobject%20PluginArray%5D|Mozilla|0|PC|56.0|10|33333|1|0|0|%7B-1%7D|2363|-1|8558|1064686|0|0|0|1893|1893|0|0|4.5.0_3aeabf|3b3973&lts=-1&d2=-1|-1|-1|1|100|0|1", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3914103_Naomi?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3416108_Carbon_Bay?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4266674_Atlantic_Ocean_Blue?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3592970_Dark_Indigo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3944204_White?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1840211_ALT51", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1711501", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613333_Dark_Wash", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1757201", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1720904_ALT2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1456050_True_Blue", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1240385_Modern_Reflection", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1474928_Bayside", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3776576_Dark_Wash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1695762_Rosedale", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-830494_ALT5", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-495025_Lights_On", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-854568_Rinse", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1740538", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1768237", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1757535", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/productlistRevamp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/controller/ctl.pmp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/mobile/mobilemenu.tpl.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("womens_jeans_bottoms_clothin_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_4");
        status = nsApi.ns_web_url ("mobilemenu_json_4",
            "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pmpSearchPageTmplV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_3");
        status = nsApi.ns_web_url ("delivery_3",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_13");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_13",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1800593", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613341_Black", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1800593", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613341_Black", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&scriptVersion=1.11.2&dyid_server=6440147674919822059&ctx=%7B%22type%22%3A%22CATEGORY%22%2C%22data%22%3A%5B%22Clothing%22%2C%22Womens%22%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_4");
        status = nsApi.ns_web_url ("session_jsp_4",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("session_jsp_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_7");
        status = nsApi.ns_web_url ("floop_7",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/menu/tpl.accountdropdown.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_14");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_14",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/feature/ESI/Common/NewBanner/", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_14", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("storesAvailabilitySearch_2");
        status = nsApi.ns_web_url ("storesAvailabilitySearch_2",
            "URL=https://www.kohls.com/snb/storesAvailabilitySearch?zipCode=&_=1621582586375"
        );

        status = nsApi.ns_end_transaction("storesAvailabilitySearch_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_4");
        status = nsApi.ns_web_url ("uia_4",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582587036",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s65981031062651?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A27%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cjeans%7Cwomens&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event40%2Cevent81%2Cevent38%2Cevent155&products=%3Bproductmerch3&c1=clothing&c2=clothing%3Ebottoms&v2=jeans-vn-women&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign%20p13n&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=womens%7Cjeans%7Cbottoms%7Cclothing%7Cpmp&v9=bottoms%7Cclothing%7Cjeans&c16=internal%20campaign%20p13n&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cjeans%7Cwomens&c28=jeans-vn-women&v28=bottoms%7Cclothing%7Cjeans%7Cwomens&v29=featured%3A1%3A48&v35=jeans-vn-women&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cjeans%7Cwomens%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cjeans%7Cwomens&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&c75=w%3Ebg%7Cbest_seller&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7Creranked_rt%7C%7C3&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp?CN=Gender:Womens+LegOpening:Flare+Product:Jeans+Category:Bottoms+Department:Clothing&icid=wmsjeans-VN-flares", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_6");
        status = nsApi.ns_web_url ("batch_6",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582587123_706840",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_15");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_15",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/SpotLight/4c6fd148-cac9-4c0b-8461-61c2e90993fa/", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://www.google.com/afs/ads?adtest=off&channel=PMP_TwoLeftRail&cpp=0&hl=en&client=kohls-search&q=Gender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=7671621582588029&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=5&u_tz=330&dt=1621582588033&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi760sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall#master-1", END_INLINE,
                "URL=https://www.kohls.com/media/css/fonts/hfjFonts.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_15", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(24.498);

        status = nsApi.ns_start_transaction("womens_flare_jeans_bottoms_c_3");
        status = nsApi.ns_web_url ("womens_flare_jeans_bottoms_c_3",
            "URL=https://www.google.com/afs/ads?adsafe=high&adtest=off&cpp=0&client=vert-pla-kohls-srp&q=Gender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=1&pfcrncy=USD&pfmax=347.5&pfmin=70.5&theme=walleye&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&nocache=6251621582588044&num=0&output=uds_ads_only&v=3&preload=true&bsl=10&u_his=5&u_tz=330&dt=1621582588048&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=-wi760he265&cont=afshcontainer&inames=slave-0-2&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall#master-2",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-01?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-02?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-03?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-04?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-05?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Clean_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Black_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("womens_flare_jeans_bottoms_c_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_8");
        status = nsApi.ns_web_url ("floop_8",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Gray_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Medium_Blasted_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Modern_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Light_Wash_Destruct_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Light_Tint_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Rinse_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Bewitched_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Inspire_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Authentic_Nordic_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Meridian_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Verona_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Black_Onyx_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Niagara_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Washed_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Expedition_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Inspire_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_3");
        status = nsApi.ns_web_url ("experiences_3",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/pmpSearchPageScriptsV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_4");
        status = nsApi.ns_web_url ("delivery_4",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("delivery_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_16");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_16",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&scriptVersion=1.11.2&dyid_server=6440147674919822059&ctx=%7B%22type%22%3A%22CATEGORY%22%2C%22data%22%3A%5B%22Clothing%22%2C%22Womens%22%5D%7D", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_16", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_7");
        status = nsApi.ns_web_url ("batch_7",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582589362_636045",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_5");
        status = nsApi.ns_web_url ("uia_5",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582589382",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_17");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_17",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_17", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_5");
        status = nsApi.ns_web_url ("session_jsp_5",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("session_jsp_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_8");
        status = nsApi.ns_web_url ("batch_8",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582589464_507206",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("storesAvailabilitySearch_3");
        status = nsApi.ns_web_url ("storesAvailabilitySearch_3",
            "URL=https://www.kohls.com/snb/storesAvailabilitySearch?zipCode=&_=1621582588761",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s61233008833217?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A29%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event81%2Cevent38%2Cevent155&products=%3Bproductmerch4&c1=clothing&c2=clothing%3Ebottoms&v2=wmsjeans-vn-flares&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign%20p13n&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=womens%7Cflare%7Cjeans%7Cbottoms%7Cclothing%7Cpmp&v9=bottoms%7Cclothing%7Cjeans%7Cwomens&c16=internal%20campaign%20p13n&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&c28=wmsjeans-vn-flares&v28=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&v29=featured%3A1%3A48&v35=wmsjeans-vn-flares&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7Creranked_rt%7C%7C3&c.&a.&activitymap.&page=bottoms%7Cclothing%7Cjeans%7Cwomens&link=Flared%20Jeans&region=vn-box&pageIDType=1&.activitymap&.a&.c&pid=bottoms%7Cclothing%7Cjeans%7Cwomens&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFl&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.google.com/afs/ads?adtest=off&channel=PMP_LeftRail_BottomRail&cpp=0&hl=en&client=kohls-search&q=Gender%253AWomens%252BLegOpening%253AFlare%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=5811621582590299&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=6&u_tz=330&dt=1621582590302&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi760sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women#master-1", END_INLINE,
                "URL=https://www.google.com/afs/ads?adsafe=high&adtest=off&cpp=0&client=vert-pla-kohls-srp&q=Gender%253AWomens%252BLegOpening%253AFlare%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=1&pfcrncy=USD&pfmax=280&pfmin=57&theme=walleye&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&nocache=2701621582590312&num=0&output=uds_ads_only&v=3&preload=true&bsl=10&u_his=6&u_tz=330&dt=1621582590315&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=-wi760he265&cont=afshcontainer&inames=slave-0-2&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women#master-2", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;101965;1478;iframe/?spotName=Category_Page&U1=4258008,4211143,4916605,4009813,4634667,4777947,4648985,5121912,3801088,5121924,3877219&U2=1234&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U10=N/A&cachebuster=900930.8604725595", END_INLINE,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8d8f252b2afa4196b5e95fbba1500e81-source.min.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4211143_Dark_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4258008_Dark_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4258008_Medium_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4258008_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=240&hei=240&op_sharpen=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("storesAvailabilitySearch_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_3");
        status = nsApi.ns_web_url ("X349_3",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Black_Coat?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4211143_Dark_Wash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4777947?wid=240&hei=240&op_sharpen=1", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", "METHOD=OPTIONS", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_4");
        status = nsApi.ns_web_url ("experiences_4",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/4211143_Faded_Blue_Jessie_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4916605?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Med_Blue_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4634667_Allison_Dark_Wash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5121912?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4258008_White?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4648985_Annie_Dark_Wash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Dark_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Washed_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Black_Coat_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Ivory_sw?wid=20&hei=20", END_INLINE,
                "URL=https://bat.bing.com/p/action/4024145", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=64fa5544-c969-4033-9683-c7f1c280b333&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=1&pi=0&lg=en-GB&sw=1366&sh=768&sc=24&tl=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&p=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&lt=1384&pt=1621582587345,1009,1009,,,2,2,2,2,2,,16,864,1189,1047,1383,1383,1384,,,&pn=0,0&evt=pageLoad&msclkid=N&sv=1&rn=334818", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=64fa5544-c969-4033-9683-c7f1c280b333&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&prodid=4258008%2C4211143%2C4916605%2C4009813%2C4634667%2C4777947%2C4648985%2C5121912%2C3801088%2C5121924%2C3877219&pagetype=category&ea=custom&en=Y&evt=custom&msclkid=N&rn=764428", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://d9.flashtalking.com/d9core", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_True_Blue?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2954787_Rinse?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Medium_Dark_Destructed?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1731442_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1119487_Rinse_Noir?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3801088_Red_sw?wid=20&hei=20", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&H=s3399n&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_9");
        status = nsApi.ns_web_url ("floop_9",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc_2");
        status = nsApi.ns_web_url ("lgc_2",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE
        );

        status = nsApi.ns_end_transaction("lgc_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_10");
        status = nsApi.ns_web_url ("floop_10",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE,
                "URL=https://s.btstatic.com/lib/d3ba78441d586dd7df57c95bad6ca4771a9d3907.js?v=2", END_INLINE,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621582591421&cv=9&fst=1621582591421&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621582591418&cv=9&fst=1621582591418&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://servedby.flashtalking.com/spot/6/1638;101965;1478/?spotName=Category_Page&U1=4258008,4211143,4916605,4009813,4634667,4777947,4648985,5121912,3801088,5121924,3877219&U2=1234&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U10=N/A&cachebuster=900930.8604725595&ft_trackID=16215825-9149-54BA-0F11-8AA5C5425200", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621582591410&mi_u=anon-1621582571535-2013629335&mi_cid=8212&page_title=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&timezone_offset=-330&event_type=pageview&cdate=1621582591407&ck=host&anon=true&type=category&title=Women%27s%20Flare%20Jeans&id=womens-flare-jeans-bottoms-clothing&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing", END_INLINE,
                "URL=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp?CN=Gender:Womens+LegOpening:Flare+Product:Jeans+Category:Bottoms+Department:Clothing&icid=wmsjeans-VN-flares", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=3&dtm_email_hash=N/A&dtm_user_id=04c7b2a4-9eb5-41be-bf31-441b23617ef2&dtmc_department=clothing&dtmc_category=bottoms&dtmc_sub_category=jeans", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621582591421&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&async=1&fmt=3&is_vtc=1&random=696531391&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621582591418&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&async=1&fmt=3&is_vtc=1&random=1896434026&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_10", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(28.235);

        status = nsApi.ns_start_transaction("womens_lc_lauren_conrad_patc");
        status = nsApi.ns_web_url ("womens_lc_lauren_conrad_patc",
            "URL=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp?prdPV=4",
            INLINE_URLS,
                "URL=https://servedby.flashtalking.com/track/101965;1478;403;16215825-9149-54BA-0F11-8AA5C5425200/?ft_data=d9:1f1c1ea80b0e48d388d36c2aac50d6f9;d9s:1f1c1ea80b0e48d388d36c2aac50d6f9&cachebuster=984214.6512213801", END_INLINE,
                "URL=https://fdz.flashtalking.com/services/kohls/FBI-3059/segment/?id=4258008", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621582591421&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&async=1&fmt=3&is_vtc=1&random=696531391&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621582591418&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Achieve%20On%20Trend%20Style%20with%20Women%27s%20Flare%20Jeans%20%7C%20Kohl%27s&async=1&fmt=3&is_vtc=1&random=1896434026&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://login.dotomi.com/pixel.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("womens_lc_lauren_conrad_patc", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_18");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_18",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=https://fdz.flashtalking.com/services/kohls/FBI-3059/segment/?uid=1f1c1ea80b0e48d388d36c2aac50d6f9&id=4258008", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/xa2-xa211;;pixel/?valuePairs=klsct0;klsurct1&setTime=0;0&granularity=day;day", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/xa2-xa211;;pixel/?valuePairs=klsct0;klsurct1&setTime=0;0&granularity=day;day", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_18", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_10");
        status = nsApi.ns_web_url ("index_10",
            "URL=https://hb.emxdgt.com/?t=1142&ts=1621582592757",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_5");
        status = nsApi.ns_web_url ("bidRequest_5",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_6");
        status = nsApi.ns_web_url ("bidRequest_6",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_2");
        status = nsApi.ns_web_url ("translator_2",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("translator_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_5");
        status = nsApi.ns_web_url ("auction_5",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&v=2.1.2&tmax=1142"
        );

        status = nsApi.ns_end_transaction("auction_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_6");
        status = nsApi.ns_web_url ("auction_6",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_bottom_leaderboard_header&lib=ix&size=728x90&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&v=2.1.2&tmax=1142"
        );

        status = nsApi.ns_end_transaction("auction_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("prebid_2");
        status = nsApi.ns_web_url ("prebid_2",
            "URL=https://ib.adnxs.com/ut/v3/prebid",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A47847127%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%227%22%2C%22siteID%22%3A%22269241%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%223%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%2239b7b682-b183-48f1-a9f9-71eee4300ca4%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T07%3A36%3A14%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("prebid_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X");
        status = nsApi.ns_web_url ("X",
            "URL=https://bat.bing.com/actionp/0?ti=4024145&tm=al001&Ver=2&mid=64fa5544-c969-4033-9683-c7f1c280b333&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&evt=pageHide",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/error-icon.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pdp/play-inactive.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/9-dots.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/warning-glyph-black.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/loading.svg", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pdp/rectangle.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/left-arrow.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/right.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/add-to-list.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/store.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/backToNext.png", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/pdp/pdp.app.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_ALT51?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_ALT5?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/productDetailsPage.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_ALT50?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pdpPageTmplV1.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/css/system.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("X", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_19");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_19",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_19", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_5");
        status = nsApi.ns_web_url ("delivery_5",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_20");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_20",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&scriptVersion=1.11.2&dyid_server=6440147674919822059&ctx=%7B%22type%22%3A%22PRODUCT%22%2C%22data%22%3A%5B%224009813%22%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_20", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_6");
        status = nsApi.ns_web_url ("session_jsp_6",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/templates/clientSidePdpScripts.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_9");
        status = nsApi.ns_web_url ("batch_9",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582594810_396158",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_6");
        status = nsApi.ns_web_url ("uia_6",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582594723",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("index_11");
        status = nsApi.ns_web_url ("index_11",
            "URL=https://www.kohls.com/web/deliveryInfoStandard/",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s64340910062162?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A36%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=pdp&events=prodView%2Cevent3%2Cevent84&products=%3B4009813%3B%3B%3B%3Bevar11%3Dnot%20collection%7Cevar16%3Dn%7Cevar74%3D50.00_29.99&c4=pdp&c9=product%20detail%20page&v9=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&c10=product%20detail%20page&c11=product%20detail%20page&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&v59=product%20page&v63=4&c64=VisitorAPI%20Present&v68=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&v75=pdp20-standard&v86=21may.w2&v87=pdp20&c.&a.&activitymap.&page=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&link=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&region=root_panel1390753&pageIDType=1&.activitymap&.a&.c&pid=bottoms%7Cclothing%7Cflare%7Cjeans%7Cwomens&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC9fa6133381864e24a8e57c6d93d58d24-source.min.js", END_INLINE,
                "URL=https://scontent.webcollage.net/api/v2/product-content", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC55d353ba1b894443bc7ecec777f338d5-source.min.js", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;11970;1478;iframe/?spotName=Product_Pages&U1=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp?prdPV=4&U2=bottoms&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U9=46684264&U10=N/A&cachebuster=129481.93959942312", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_Miramar_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_Peach_Wash_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_Barranca_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4", END_INLINE,
                "URL=https://www.google.com/afs/ads?adpage=1&adtest=off&channel=null&cpp=0&hl=en&client=kohls-pdp&q=Impressive%20style.%20These%20women%27s%20LC%20Lauren%20Conrad%20high-rise%20flare%20pants%20are%20just%20what%20you%27re%20looking%20for.&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=2921621582596624&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=7&u_tz=330&dt=1621582596625&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi800ff2sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares#master-1", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.js", END_INLINE,
                "URL=https://syndi.webcollage.net/site/kohls/tag.js?cv=18768", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/bv.js", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://edge.curalate.com/sites/kohls-dismfc/site/latest/site.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC0c9683a00d4047d1b112bd5cd4e7c71a-source.min.js", END_INLINE,
                "URL=https://content.syndigo.com/site/8f845786-bb87-4065-810b-e52a8a2220e3/syndi.min.mjs?cv=450439", END_INLINE,
                "URL=https://assets.pinterest.com/js/pinit.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("splitTests_json");
        status = nsApi.ns_web_url ("splitTests_json",
            "URL=https://apps.bazaarvoice.com/splitTests.json",
            INLINE_URLS,
                "URL=https://koh-cdns.truefitcorp.com/fitrec/koh/js/fitrec.js?autoCalculate=false", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=90&hei=90&op_sharpen=1", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/api/api-0.7.3.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/curations/curations-1.2.0.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/rating_summary/rating_summary-2.44.0.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/questions/questions-0.2.2.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/review_highlights/review_highlights-3.2.6.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf2b2cd806a554e4da7df299788affb8a-source.min.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/curations-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/rating_summary-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/review_highlights-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/reviews/reviews-0.2.2.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/api-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/questions-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/reviews-config.js", END_INLINE,
                "URL=https://analytics-static.ugc.bazaarvoice.com/prod/static/3/bv-analytics.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("splitTests_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_5");
        status = nsApi.ns_web_url ("outfits_5",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=6&with_item_coords=true&item_number=4009813_White_With_Stitching&return_object=true",
            INLINE_URLS,
                "URL=https://content.syndigo.com/site/common/1.0.187/translations/en.min.mjs?cv=450439&hn=www.kohls.com", END_INLINE,
                "URL=https://edge.curalate.com/sites/kohls-dismfc/experiences/custom-lb-carousel-1593692829557/latest/experience.min.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/v1/config/wishlistconfig.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/static/ss/sstimer.js", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=5&dtm_email_hash=N/A&dtm_user_id=04c7b2a4-9eb5-41be-bf31-441b23617ef2&dtmc_department=clothing&dtmc_category=bottoms&dtmc_sub_category=jeans&dtmc_product_id=4009813", END_INLINE,
                "URL=https://assets.pinterest.com/js/pinit_main.js?0.9535572934799659", END_INLINE
        );

        status = nsApi.ns_end_transaction("outfits_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc_3");
        status = nsApi.ns_web_url ("lgc_3",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://sc-static.net/scevent.min.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=600&hei=600&op_sharpen=1", END_INLINE,
                "URL=https://consumer.truefitcorp.com/fitconfig?callback=tfc.processConfiguration&storeId=koh&clientHandlesBrowserUnsupported=true", END_INLINE,
                "URL=https://servedby.flashtalking.com/spot/6/1638;11970;1478/?spotName=Product_Pages&U1=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp?prdPV=4&U2=bottoms&U3=84795902680038641394405446550017377092&U7=04c7b2a4-9eb5-41be-bf31-441b23617ef2&U9=46684264&U10=N/A&cachebuster=129481.93959942312&ft_trackID=16215825-9758-46C6-DD3A-3038A9118360", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/css/skava-custom.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/skava-pdp-custom.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/skava-pdp.js", END_INLINE,
                "URL=https://servedby.flashtalking.com/track/11970;1478;403;16215825-9758-46C6-DD3A-3038A9118360/?ft_data=d9:1f1c1ea80b0e48d388d36c2aac50d6f9;d9s:1f1c1ea80b0e48d388d36c2aac50d6f9&cachebuster=970669.9256457236", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/en_US/bvapi.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621582597552&mi_u=anon-1621582571535-2013629335&mi_cid=8212&page_title=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&timezone_offset=-330&event_type=pageview&cdate=1621582597547&ck=host&anon=true&type=product&title=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&price=29.99&id=4009813&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&categories=id%3Aclothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fclothing.jsp%253FCN%253DDepartment%253AClothing%2Ctitle%3AClothing%7Cid%3Abottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fbottoms-clothing.jsp%253FCN%253DCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3ABottoms%7Cid%3Ajeans-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fjeans-bottoms-clothing.jsp%253FCN%253DProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3AJeans%7Cid%3Alc-lauren-conrad%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Flc-lauren-conrad.jsp%253FCN%253DBrand%253ALC%252520Lauren%252520Conrad%2Ctitle%3ALC%2520Lauren%2520Conrad&meta=brand%3Alc%2520lauren%2520conrad%2Ccolor%3Awhite%2520with%2520stitching", END_INLINE
        );

        status = nsApi.ns_end_transaction("lgc_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("summary");
        status = nsApi.ns_web_url ("summary",
            "URL=https://api.bazaarvoice.com/data/display/0.2alpha/product/summary?PassKey=9zz78jlr8mloisoz9800sqwo5&productid=4009813&contentType=reviews,questions&reviewDistribution=primaryRating,recommended&rev=0&contentlocale=en_GB,en_US",
            INLINE_URLS,
                "URL=https://tr.snapchat.com/cm/i?pid=8e9bd9e0-8284-4dbd-ab20-145759728098", END_INLINE,
                "URL=https://fm.flashtalking.com/segment/591/view/4009813", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&H=-4dlye7u&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE,
                "URL=https://fm.flashtalking.com/segment/591/view/4009813/?guid=1f1c1ea80b0e48d388d36c2aac50d6f9", END_INLINE,
                "URL=https://tr.snapchat.com/p", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE,
                "URL=https://cdn.truefitcorp.com/store-koh/6.56.1.19/resources/store/koh/css/fitrec.css", END_INLINE,
                "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:1f1c1ea80b0e48d388d36c2aac50d6f9&seg=u6h", END_INLINE,
                "URL=https://cdn.truefitcorp.com/store-koh/6.56.1.19/resources/store/koh/js/tfcapp.js", END_INLINE,
                "URL=https://cdn.truefitcorp.com/consumer-ux/6.56.1.22/resources/fitrec/js/application.js", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/common/static-assets/3.3.3/jquery-bv%403.5.1%2Blodash-bv%404.17.19.js", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/stylesheets/screen.css", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/scripts/bv-primary.js", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=b0d8wh", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=7qwvq6", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=kgwrfu", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/7d2a85cfa13a31455f4199778b05c0c69eae9a35d48ff8", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=co46ia", END_INLINE
        );

        status = nsApi.ns_end_transaction("summary", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_4");
        status = nsApi.ns_web_url ("X349_4",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://content.stylitics.com/images/collage/7d2981c4a63b310c1265d8927cf67b52b207de667bd05e", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=te27un", END_INLINE,
                "URL=https://event.syndigo.cloud/event/p.gif?u=70F29DD4-88C3-44B2-959B-44775BE5AD23&prtnid=8f845786-bb87-4065-810b-e52a8a2220e3&siteid=8f845786-bb87-4065-810b-e52a8a2220e3&pageid=4009813&s=1621582598224&v=v1.0.187&visitid=702C43DD-6C65-42F4-9F4E-8D1F7218A961&dt=0.006&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&r=0.5199436140465215&pageurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/7a288fcdae3f54f13d9dc17bc46f861f5fa1c845ab35", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=Horizontal1%7C15%2CHorizontal2%7C15%2CHorizontal3%7C15", "METHOD=OPTIONS", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json");
        status = nsApi.ns_web_url ("id_json",
            "URL=https://network.bazaarvoice.com/id.json?_=y8xxtf&callback=_bvajsonp1"
        );

        status = nsApi.ns_end_transaction("id_json", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_11");
        status = nsApi.ns_web_url ("floop_11",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_2");
        status = nsApi.ns_web_url ("id_json_2",
            "URL=https://network.bazaarvoice.com/id.json?_=5pauyf&callback=_bvajsonp2"
        );

        status = nsApi.ns_end_transaction("id_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_3");
        status = nsApi.ns_web_url ("id_json_3",
            "URL=https://network.bazaarvoice.com/id.json?_=gzq4q6&callback=_bvajsonp3"
        );

        status = nsApi.ns_end_transaction("id_json_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("wxyfzSQkpEztWWCh");
        status = nsApi.ns_web_url ("wxyfzSQkpEztWWCh",
            "URL=https://edge.curalate.com/v1/media/wxyfzSQkpEztWWCh?appId=curalate&limit=25&productMetadata=availability,custom_label_3,custom_label_4&sort=Latest&fpcuid=17e879e2-c9d4-4466-aa8c-e4a4657e531b&rid=ef47b5b0-c4e8-430d-8e34-27b7f4543e0d&filter=((productId%3A%274009813%27))"
        );

        status = nsApi.ns_end_transaction("wxyfzSQkpEztWWCh", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X009813_json");
        status = nsApi.ns_web_url ("X009813_json",
            "URL=https://content.syndigo.com/page/8f845786-bb87-4065-810b-e52a8a2220e3/4009813.json?u=70F29DD4-88C3-44B2-959B-44775BE5AD23&prtnid=8f845786-bb87-4065-810b-e52a8a2220e3&siteid=8f845786-bb87-4065-810b-e52a8a2220e3&pageid=4009813&s=1621582598224&v=v1.0.187&visitid=3794BFF7-890D-4A0F-A635-843202E7FA79&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&r=0.12727232774174957&pageurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4"
        );

        status = nsApi.ns_end_transaction("X009813_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("batch_json");
        status = nsApi.ns_web_url ("batch_json",
            "URL=https://api.bazaarvoice.com/data/batch.json?passkey=9zz78jlr8mloisoz9800sqwo5&apiversion=5.5&displaycode=9025_10_0-en_us&resource.q0=products&filter.q0=id%3Aeq%3A4009813&stats.q0=questions%2Creviews&filteredstats.q0=questions%2Creviews&filter_questions.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_answers.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviews.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviewcomments.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&resource.q1=questions&filter.q1=productid%3Aeq%3A4009813&filter.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&sort.q1=totalanswercount%3Adesc&stats.q1=questions&filteredstats.q1=questions&include.q1=authors%2Cproducts%2Canswers&filter_questions.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_answers.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&limit.q1=10&offset.q1=0&limit_answers.q1=10&resource.q2=reviews&filter.q2=isratingsonly%3Aeq%3Afalse&filter.q2=productid%3Aeq%3A4009813&filter.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&sort.q2=submissiontime%3Adesc&stats.q2=reviews&filteredstats.q2=reviews&include.q2=authors%2Cproducts%2Ccomments&filter_reviews.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviewcomments.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_comments.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&limit.q2=8&offset.q2=0&limit_comments.q2=3&callback=BV._internal.dataHandler0",
            INLINE_URLS,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", "METHOD=OPTIONS", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_4");
        status = nsApi.ns_web_url ("id_json_4",
            "URL=https://network.bazaarvoice.com/id.json?_=ndxs6w&callback=_bvajsonp4"
        );

        status = nsApi.ns_end_transaction("id_json_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_5");
        status = nsApi.ns_web_url ("id_json_5",
            "URL=https://network.bazaarvoice.com/id.json?_=5t1c6z&callback=_bvajsonp5"
        );

        status = nsApi.ns_end_transaction("id_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_5");
        status = nsApi.ns_web_url ("experiences_5",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=Horizontal1%7C15%2CHorizontal2%7C15%2CHorizontal3%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://log.pinterest.com/?type=pidget&guid=5CWtokN88F6c&tv=2021040501&event=init&sub=www&button_count=0&follow_count=0&pin_count=0&profile_count=0&board_count=0&section_count=0&lang=en&nvl=en-GB&via=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&viaSrc=canonical", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=efk129", END_INLINE,
                "URL=https://scontent.webcollage.net/api/v2/product-content?loadlegacycontent=true", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:bv-loader,bvProductVersion:%2713.2.9%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:%2714.0300%27,endTime:%275042.9900%27,locale:en_US,name:timeToRunScout,startTime:%275028.9600%27,type:Performance))&_=gp27cg", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", END_INLINE,
                "URL=https://s.btstatic.com/lib/d3ba78441d586dd7df57c95bad6ca4771a9d3907.js?v=2", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Embedded&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProduct=questions&subject=Kohls&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621582593281,req:1329,res:160,tot:-1621582591790)&_=ak1a21&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Embedded&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProduct=reviews&subject=Kohls&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621582593281,req:1329,res:160,tot:-1621582591790)&_=y9nku1&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621582599403&cv=9&fst=1621582599403&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621582599401&cv=9&fst=1621582599401&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:RatingSummary,bvProductVersion:%272.44.0%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:0.6299999658949673,locale:en_US,name:bv_rating_summary_render_time,productId:%274009813%27,startTime:5519.245000032242,type:Performance))&_=4iwx37", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Embedded&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProduct=RatingSummary&bvProductVersion=2.44.0&productId=4009813&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621582593281,req:1329,res:160,tot:-1621582591790)&_=ceuv1x&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Embedded&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProduct=ReviewHighlights&bvProductVersion=3.2.6&productId=4009813&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621582593281,req:1329,res:160,tot:-1621582591790)&_=mvj62p&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=ef47b5b0-c4e8-430d-8e34-27b7f4543e0d&fpcuid=17e879e2-c9d4-4466-aa8c-e4a4657e531b&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&e=t%3Apinc%7Cts%3A1621582599168%7Ccut%3A0%7Cdt%3APdp%7Cppid%3Ap_690_85ff8ff9b650ba14a20a8a9008771f23a744bde123c4d6ac9850e6c67c2c6035%7Cpid%3A%274009813%27%7Cpsid%3As_690_e1b83600d959a6f5d1285eda79acd9c797e2591cf0993af99c3292d2a3421ad4&cache=_34f5bccf-b2e1-4f04-ac6f-a151e8940551", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4634667_Allison_Dark_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4218742_Punch?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4211143_Dark_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4438906_Lapis_Dark_Horse?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC988eacf9936b486d87487944f264070b-source.min.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3944204_Cameo_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4648985_Annie_Dark_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4638079_Cedar_Medium_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4381083_Medium_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4899534_Salted_Caramel?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Destructed_Medium_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4856969_Bronze_Peach?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4777947?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4921547_Cream_Silk?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4316598_Ashton_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4206333_Multi_Plaid_Cream?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4657493_Mandarin_Flower?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/scripts/secondary.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_21");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_21",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/4339154_Dark_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_21", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_6");
        status = nsApi.ns_web_url ("id_json_6",
            "URL=https://network.bazaarvoice.com/id.json?_=xmn458&callback=_bvajsonp6",
            INLINE_URLS,
                "URL=https://scontent.webcollage.net/kohls/mosaic-board-meta?ird=true&channel-product-id=4009813", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621582599401&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=4165761424&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621582599403&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=4014024118&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621582599403&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=4014024118&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621582599401&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=4165761424&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://9443bd8761d25e1d2eb569b9fcb8f1a6.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://s.yimg.com/wi/ytc.js", END_INLINE,
                "URL=https://scontent.webcollage.net/kohls/api/js/method/load-content/type/ppp?environment=live&cpi=4009813", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:6392.210000019986,endTime:6392.210000019986,locale:en_US,name:bv-scout-start,startTime:0,type:Performance))&_=hb510s", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_12");
        status = nsApi.ns_web_url ("floop_12",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://scontent.webcollage.net/kohls/power-page?ird=true&channel-product-id=4009813", END_INLINE,
                "URL=https://scontent.webcollage.net/apps/pp/assets/kohls/js/acssite%40s.min.js?ver=R00ZPAJFC00ZBTIKA", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=50153846134490&correlator=3279654365121393&output=ldjh&impl=fifs&eid=31061216%2C31061223%2C31061225%2C21064370%2C21068863%2C31061022%2C31061143&vrg=2021051701&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Cclothing%2Cbottoms%2Cjeans&enc_prev_ius=%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3&prev_iu_szs=320x50%7C1024x45%7C1024x64%7C1024x128%2C728x90%7C1024x45%7C1024x64%7C1024x128%2C728x90%2C300x250%2C300x250%2C300x250&fluid=height%2C0%2C0%2C0%2C0%2C0&prev_scp=pos%3Dmarquee%7Cpos%3Dmiddle%7Cpos%3Dbottom%7Cpos%3Dbottomleft%7Cpos%3Dbottommiddle%7Cpos%3Dbottomright&cust_params=channel%3Ddesktop%26env%3Dprod%26pgtype%3Dpdp&cookie=ID%3D179a88961f5360de%3AT%3D1621582576%3AS%3DALNI_MZIooglrOduaLWOV0A8ZrWZcT7dNg&bc=31&abxe=1&lmt=1621582600&dt=1621582600753&dlt=1621582593215&idt=3447&frm=20&biw=1349&bih=607&oid=3&adxs=163%2C20%2C20%2C185%2C525%2C865&adys=168%2C2864%2C5008%2C5040%2C5040%2C5040&adks=660533235%2C3288486087%2C2283361462%2C4172809658%2C2339037280%2C3950868409&ucis=1%7C2%7C3%7C4%7C5%7C6&ifi=1&u_tz=330&u_his=7&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&vis=1&dmc=8&scr_x=0&scr_y=312&psz=1349x25%7C1309x0%7C1309x0%7C300x32%7C300x32%7C300x32&msz=1024x25%7C1309x0%7C1309x0%7C300x0%7C300x0%7C300x0&ga_vid=1289170567.1621582601&ga_sid=1621582601&ga_hid=527914213&ga_fc=false&fws=0%2C0%2C0%2C0%2C0%2C0&ohw=0%2C0%2C0%2C0%2C0%2C0&btvi=0%7C1%7C2%7C3%7C4%7C5", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:566.179999965243,endTime:6958.389999985229,locale:en_US,name:bv-primary-ready,startTime:6392.210000019986,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:18.220000027213246,endTime:6976.6100000124425,locale:en_US,name:bv-primary-run,startTime:6958.389999985229,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:196.67500001378357,endTime:7155.064999999013,locale:en_US,name:bv-slow-path-ready,startTime:6958.389999985229,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:762.8549999790266,endTime:7155.064999999013,locale:en_US,name:bv-core-app,startTime:6392.210000019986,type:Performance),(brand:LC,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%274283304%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27))&_=jstxj9", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstyTzu7gOavwE8JAG0wIdvoyjc4U4RXqEsmFP6Wy2R51KR93m_JmaFDRftf1Jr7R4rAM50jE3Y8OYdBrG_Blfg5b_VAsI8lB6cqVBjHycqNHzKLazAU5JdNWyuh-PIHqfP5qi39WMfntJVGx6i-uhqeMWifWj7i5jB7U1x6-GdEDAa50ioM9dMfdDyl6mSwqDdXT8tsc1z6yuDIObrtmqq40GfOEx9WOaBXLrs77U3IFKBmegQ-TvyEZvI5CAbwV_lp-Mw0lS1Fl2lm1NRqe2-WhZfyb_kYaZWJJ-cKaz_jSMxDIOw806lo08TIfrlorOPu8d3ha3sj&sig=Cg0ArKJSzEBE38_YZ0F_EAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/js/ext.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/5213575238420075241", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/9820687163340561195?", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaSdOG8LTMkQeyvFjx5kolC60TkW-PlUr7VC-7LgTADcpVeuwN_Oi_SU8ZvnsTo-q1cjA56ZR_ezBIpp8SxYQgtnbR6lpw", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Product&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&label=Default&productId=4009813&bvProduct=AskAndAnswer&categoryId=YOUNG_WMNS_LC_E_AND_J_SVVW&numQuestions=3&numAnswers=2&version=2.0&context=Read&siteId=redesign&bvProductVersion=3.1.12&initial=true&pages=1&subjectType=Product&subjectId=4009813&contentType=Question&brand=LC&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&_=nkkl7f&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=gs0len", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=b9k2v4", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssHDooxYbCUgbgEPJ-5wgMaKQc28zU7D0Bgfv9-ZDYFYvYQk6QUGWWQDh2jmIsTMfm38beg_Li3LdFAVj8fTc5hD5UEwKslrsyHth85VT9hQ2dRSAk2LDsCKH6GjaMIRsDn_kCq4G3k5rBSHrHF0XY9ib5pzJsDU3tI-Z983p5gVXp6nI7zPwVjgr4waPZYNQOP_rCkipxcocaeuIc0t8v23kROIu6QjrHHMq9YnHCCu-DqUiyZto91DwvafPxtEfFEquIV3vk9FAUYp6PgY2x5K4v10uMV9g63gIF4r2ocBR2QPaSEj5LXZjwTfQ&sig=Cg0ArKJSzPSsSp133JcUEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=oqxkt7", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=yinw2j", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=f54231d3b3dbe543fbf&type=Product&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&label=Default&productId=4009813&bvProduct=RatingsAndReviews&categoryId=YOUNG_WMNS_LC_E_AND_J_SVVW&version=2.0&context=Read&siteId=redesign&bvProductVersion=3.1.12&initial=false&pages=1&subjectType=Product&subjectId=4009813&contentType=Review&brand=LC&numReviews=7&numRatingsOnlyReviews=4&percentRecommend=NaN&avgRating=4.3&href=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&canurl=https://www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&_=z7roae&ref=https://www.kohls.com/catalog/womens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BLegOpening:Flare%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Dwmsjeans-VN-flares", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssR02apSvOg4bcKeCucUAL9oxHwIYDv59FUHDPCTJ_0ZqLeUmTtw2caszEGtFKDiIxLJetZDIQSoLRumRsvIRHAKgb_Hv4JJ_mSObd5HW7ncuPbkYdMEa3nUcxLAJDnbPSmpU6TSR6ZJQk2BgeuyukmclI7q1EXTJzQyxMFGl0ogd2MNb1kWyH9NooP6ObYL3tKSNA-ItL1SujkJsBgZoD2F64rwQ289yOQH2496GLbmq0bSkCfx-ESV1-IdLDKxG33tjWZXMqGFvizEJHbUiK1zBihPxtUrKfUpgGicpOziKWIwZJypOQQIdXaSbzF&sig=Cg0ArKJSzGfcM5cXTVU-EAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstmtEMZPmS9uCYkcFP45B4gmI_3AEXBfcjm3MwCAJrJW0ii6PwFTnPtbL6P-u0wn_VHoPuzWWD4Ymb0Ii4Yo5jpqVxF8d9WEjaSLRaqLYn7XseiqT7HOL9d-FRxiXOZf0A_eTmGBhxHE2yaHOapvyq7LbnNsLjfd9jwbQXtD8VIYw9Yfx2c_BX40nj4c59QCpu3NWPQjfhqpOhfmKo47q9mpHY2RD9k-zyllvMQDMynfYoMmyt2KivCmCUTwRedmVkys2kS_dqFtOOHYtcAQccwPp4zWL2oxLxtk1GCuCNO9KJ4aXz4xkXuXnLv&sig=Cg0ArKJSzP2AK9x1f_h7EAE&adurl=", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=r3vume", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=9ap4l9", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=o8a6d", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=nd4a9b", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=1aryqu", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=f54231d3b3dbe543fbf&type=firebird&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=yi0ms9", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((brand:LC,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%274205332%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LC,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%274479720%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%275067514%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%275033521%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LC,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%27178147939%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LC,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%27176114661%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LC,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:YOUNG_WMNS_LC_E_AND_J_SVVW,cl:Impression,contentId:%27175086143%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:1654.1399999987334,endTime:8612.529999983963,locale:en_US,name:bv-qa_show_questions-rendered,startTime:6958.389999985229,type:Performance))&_=l96p9k", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138343070434&adorderid=2774409325&adunitid=16811592&advertiserid=32614632&lineitemid=5641706618&rand=1710414717", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:2217.095000029076,endTime:8620.065000024624,locale:en_US,name:bv-qa_show_questions-completed,startTime:6402.969999995548,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:997.0350000075996,endTime:7399.855000025127,locale:en_US,name:bv-preload,startTime:6402.8200000175275,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:1682.6750000473112,endTime:8641.06500003254,locale:en_US,name:bv-rr_show_reviews-rendered,startTime:6958.389999985229,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:2240.6450000125915,endTime:8644.354999996722,locale:en_US,name:bv-rr_show_reviews-completed,startTime:6403.70999998413,type:Performance))&_=xfhy1c", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307873913&adorderid=2682029031&adunitid=16811592&advertiserid=32614632&lineitemid=5337912127&rand=724066683", END_INLINE,
                "URL=https://rel.webcollage.net/apps/el?e=aplus-no-content&channel-product-id=4009813&partnerid=kohls&page-url=https%3A//www.kohls.com/product/prd-4009813/womens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&content-package=ppp&unique-user-id=&localtimestamp=1621582601453&_sof", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/checkmark.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/down.svg", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_22");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_22",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/swatch_unavail.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_22", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_23");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_23",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_23", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_24");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_24",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621582607065&mi_u=anon-1621582571535-2013629335&mi_cid=8212&page_title=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&timezone_offset=-330&event_type=cart_add&cdate=1621582597547&ck=host&anon=true&title=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&id=4009813&price=29.99&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-jeans.jsp&categories=id%3Aclothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fclothing.jsp%253FCN%253DDepartment%253AClothing%2Ctitle%3AClothing%7Cid%3Abottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fbottoms-clothing.jsp%253FCN%253DCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3ABottoms%7Cid%3Ajeans-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fjeans-bottoms-clothing.jsp%253FCN%253DProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3AJeans%7Cid%3Alc-lauren-conrad%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Flc-lauren-conrad.jsp%253FCN%253DBrand%253ALC%252520Lauren%252520Conrad%2Ctitle%3ALC%2520Lauren%2520Conrad&meta=brand%3ALC%20Lauren%20Conrad%2Ccolor%3AWhite%20With%20Stitching", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_24", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("addItemToCart");
        status = nsApi.ns_web_url ("addItemToCart",
            "URL=https://www.kohls.com/cnc/checkout/cartItems/addItemToCart",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/_sec/cp_challenge/crypto_message-3-4.htm", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/ak-challenge-3-4.htm", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s68665882905152?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A6%3A48%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=pdp&events=event62&products=%3B4009813%3B%3B%3B%3Bevar11%3Dnot%20collection%7Cevar16%3Dn%7Cevar74%3D50.00_29.99&c4=pdp&c9=product%20detail%20page&v9=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&c10=product%20detail%20page&c11=product%20detail%20page&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&v59=product%20page&v63=4&c64=VisitorAPI%20Present&v68=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&v75=pdp20-standard&v86=21may.w2&v87=pdp20&c.&a.&activitymap.&page=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&link=2&region=sbOptions_63915669&pageIDType=1&.activitymap&.a&.c&pid=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-if-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-default-chlge-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/ak-challenge-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-4dlye7u&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&mode=v2&cf=4847505%2C6603093%2C7422632%2C7424035%2C7472048&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE
        );

        status = nsApi.ns_end_transaction("addItemToCart", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://trial-eum-clientnsv4-s.akamaihd.net/eum/getdns.txt?c=pzvyk9zul", "REDIRECT=YES", "LOCATION=https://wzdxpvaxb4swgyfhmmiq-pzvyk9-82ff87bf6-clientnsv4-s.akamaihd.net/eum/results.txt", END_INLINE,
                "URL=https://trial-eum-clienttons-s.akamaihd.net/eum/getdns.txt?c=pzvyk9zul", "REDIRECT=YES", "LOCATION=https://182-71-119-212_s-23-3-70-16_ts-1621582609-clienttons-s.akamaihd.net/eum/results.txt", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_12");
        status = nsApi.ns_web_url ("index_12",
            "URL=https://684fc539.akstat.io/",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_2");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_2",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://182-71-119-212_s-23-3-70-16_ts-1621582609-clienttons-s.akamaihd.net/eum/results.txt", END_INLINE,
                "URL=https://wzdxpvaxb4swgyfhmmiq-pzvyk9-82ff87bf6-clientnsv4-s.akamaihd.net/eum/results.txt", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjstVVWW8eXlOXMNNKsDkHFytvsBlRGg_3wNlvJ9TDTBDHHrUB6DUVuBTf2o9ZggVoUjJz0lQWmFTbN-AHQGR1wB2foNxNiqjNPzA_g9ZwKRcdaA2jlyL&sig=Cg0ArKJSzN0mpzQeLhaNEAE&id=lidar2&mcvt=1293&p=168,163,232,1187&mtos=1212,1255,1293,1310,1339&tos=1212,43,38,17,29&v=20210519&bin=7&avms=nio&bs=1349,607&mc=1&app=0&itpl=3&adk=660533235&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621582601631&dlt=0&rpt=158&isd=317&msd=843&r=v&fum=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_25");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_25",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_25", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_3");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_3",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_4");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_4",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/_sec/cp_challenge/verify", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_26");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_26",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://event.webcollage.net/event/fc.gif?siteCode=kohls&calls=loadProductContent%7C_loadSyndi%7CloadProductContent%7C_loadLegacyWC%7C_loadLegacyContent&r=0.9362776840456879", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=1200&hei=1200&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=90&hei=90&op_sharpen=1", END_INLINE,
            "URL=https://datastream.stylitics.com/api/engagements", "METHOD=OPTIONS", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_26", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("engagements");
        status = nsApi.ns_web_url ("engagements",
            "URL=https://datastream.stylitics.com/api/engagements",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=pdpta0;ord=6281970222192;gtm=2od5c1;auiddc=1177027742.1621582573;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3Fprdpv%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=bottoms;u2=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u23=New%20Customer;u24=;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u3=29.99;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4?", END_INLINE
        );

        status = nsApi.ns_end_transaction("engagements", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("sodar");
        status = nsApi.ns_web_url ("sodar",
            "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2021051701&st=env",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde663c7b16da4ba4947e827dd1a4af31-source.min.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?gdpr=0&google_nid=signal_dmp&google_cm&btt=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?gdpr=0&ttd_pid=signal&ttd_tpi=1&ttd_puid=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&d=Fri%2C%2021%20May%202021%2007%3A37%3A08%20GMT&n=-5&b=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&enc=UTF-8&yv=1.9.2&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2019801-hp-wallet-bar-credit?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2/222/runner.html", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&enc=UTF-8&yv=1.9.2&et=custom&ea=ViewProduct&product_id=4009813&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&enc=UTF-8&yv=1.9.2&et=custom&ea=AddToCart&product_id=4009813&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://www.google.com/recaptcha/api2/aframe", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC1c1b30b08b024ebfa9748b44ff4a21f7-source.min.js", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=COKT44Si2vACFRH-aAodIXkD-Q;src=8632166;type=landi0;cat=pdpta0;ord=6281970222192;gtm=2od5c1;auiddc=1177027742.1621582573;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3Fprdpv%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=bottoms;u2=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u23=New%20Customer;u24=;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u3=29.99;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4?", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCacdd8d53a0eb4a89a0a72c2d6a4147ec-source.min.js", END_INLINE,
                "URL=https://js.cnnx.link/roi/cnxtag-min.js?id=31851", END_INLINE,
                "URL=https://mon1.kohls.com/test_rum_nv?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=3&d=1|0|-1|8|1489|-1|-1|-1|1331|160|2|35057|0|89|36484|1881|3|12893|0|1321|https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares|https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4|www.kohls.com|VisitorId%3D04c7b2a4-9eb5-41be-bf31-441b23617ef2%3B%20CavNV%3D4674162754846552467-33333-233048192974-0-9-0-2-145-28079-26476%3B%20cookieSetting%3DisCookie%3B%20AKA_RV2%3D44%3B%20AKA_CNC2%3DTrue%3B%20akacd_www-kohls-com-mosaic-p2%3D2177452799~rv%3D80~id%3De04f395af42c5b548280664818d80ca0%3B%20mosaic%3Dgcpb%3B%20at_check%3Dtrue%3B%20AMCVS_F0EF5E09512D2CD20A490D4D%2540AdobeOrg%3D1%3B%20s_ecid%3DMCMID%25PIPE%2584795902680038641394405446550017377092%3B%20_dy_csc_ses%3Dt%3B%20_dy_c_exps%3D%3B%20s_fid%3D470ED0FAFF43B1EA-3ACBA20E941EA296%3B%20s_cc%3Dtrue%3B%20_dycnst%3Ddg%3B%20s_vi%3D%5BCS%5Dv1%25PIPE%253053B175BA7DF942-600006ECB012BDA4%5BCE%5D%3B%20_dyid%3D6440147674919822059%3B%20_dyjsession%3De520494583b9eb872bf95fb8bcb6fc39%3B%20dy_fs_page%3Dwww.kohls.com%3B%20_dycst%3Ddk.w.c.ms.%3B%20_dy_geo%3DIN.AS.IN_RJ.IN_RJ_Jaipur%3B%20_dy_df_geo%3DIndia..Jaipur%3B%20_mibhv%3Danon-1621582571535-2013629335_8212%3B%20IR_gbd%3Dkohls.com%3B%20_gcl_au%3D1.1.1177027742.1621582573%3B%20SignalSpring2016%3DA%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMDM%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMTA%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMDYyMTU%3DREFZUw%3B|0|101|-1|24|24|WINDOWS|Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F56.0.2924.87%20Safari%2F537.36|en-GB|%5Bobject%20PluginArray%5D|Mozilla|0|PC|56.0|10|33333|1|0|0|%7B-1%7D|1880|-1|2907|851276|0|0|0|1604|1604|0|0|4.5.0_3aeabf|3b3973&lts=-1&d2=-1|-1|-1|1|100|0|1", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCdc9d7fc5fb9e4d329178721363aaa3f9-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("sodar", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_27");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_27",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde61955ee7f24142bacb5bddee25a9a4-source.min.js", END_INLINE,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=COKT44Si2vACFRH-aAodIXkD-Q;src=8632166;type=landi0;cat=pdpta0;ord=6281970222192;gtm=2od5c1;auiddc=*;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3Fprdpv%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=bottoms;u2=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u23=New%20Customer;u24=;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u3=29.99;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4", END_INLINE,
                "URL=https://secure.adnxs.com/getuid?https%3A%2F%2Fs.thebrighttag.com%2Fcs%3Fbtt%3DGWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A%26uid%3D$UID%26tp%3Dan%26gdpr%3D0", "REDIRECT=YES", "LOCATION=https://s.thebrighttag.com/cs?btt=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A&uid=2174005878281702669&tp=an&gdpr=0", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/bg/zue3njNLpzxGAZrYILNRV_oDQoN1Bf4uoYDHWIdg9NQ.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCecd16bab3edd42838214adeca2f386c4-source.min.js", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-4dlye7u&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&mode=v2&cf=4847715%2C4847939%2C6094051%2C6706303%2C6706308&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=f54231d3b3dbe543fbf&BVBRANDID=8f5894a6-ade9-46c7-956b-e3c011fc6be4&BVBRANDSID=002e05d6-c3fe-4327-a4d4-3d2935a1e86c&BVCRL8ID=17e879e2-c9d4-4466-aa8c-e4a4657e531b&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:30091.789999980014,endTime:36484,locale:en_US,name:bv-host-load,startTime:6392.210000019986,type:Performance))&_=e92whh", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=222&li=gpt_2021051701&jk=50153846134490&rc=", END_INLINE,
                "URL=https://s.pinimg.com/ct/core.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_27", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_13");
        status = nsApi.ns_web_url ("index_13",
            "URL=https://684fc539.akstat.io/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=45ae5150-a754-4f3f-a04d-a5f01e98b39f&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&pi=0&lg=en-GB&sw=1366&sh=768&sc=24&tl=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&p=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&lt=36573&pt=1621582591790,1376,1378,,,2,2,2,2,2,,10,1331,1491,1425,1880,1881,1881,36482,36484,36573&pn=0,0&evt=pageLoad&msclkid=N&sv=1&rn=127758", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=45ae5150-a754-4f3f-a04d-a5f01e98b39f&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&prodid=4009813&pagetype=product&en=Y&evt=custom&msclkid=N&rn=490777", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=45ae5150-a754-4f3f-a04d-a5f01e98b39f&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&prodid=4009813&pagetype=cart&en=Y&evt=custom&msclkid=N&rn=640362", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=gcms&gdpr=0&btt=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A&google_gid=CAESENC5JwCUxXq-mU1Gvgg8epQ&google_cver=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_2");
        status = nsApi.ns_web_url ("test_rum_nv_2",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=3&d=3|0|2&lts=233048557",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/cs?tp=tBLcuKl&btt=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A&uid=39b7b682-b183-48f1-a9f9-71eee4300ca4", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=sodar2&v=222&t=2&li=gpt_2021051701&jk=50153846134490&bg=!tLelt_PNAAZ7hX_Ue4U7ACkAdvg8Wu_NReL8_jiRfIDgG7eGNzu6ENPhwZXPxKZf554Ee1LQXBnJLAIAAAEJUgAAABBoAQcKAEoneMTeFzcNpigJcqTOQvV7up7EYvsvhaYPL_rxgr4HrxrE5yWUGaVFa-A6GauxLjrHzczx0Qt6izepLnqTp2bTYUVggSwhT4SvLpkCCq13mgllr488UlAYZaK7kfnwfY9IHa29NgRBk6OyaP17weIEY48ylYE_pcX86N1EbOFoR7u9BX6RxEAoGoUM-Djmwa6hNlgjC_2_jKWLO7ItbDoC8BV3aUShscmjfsK3wCYJXaogL3DfImzhk2m9cwzKeQ-p739581mWHEiaB_D6MQG1q0nJa5Dl77N6_9nOXulAlPoa5WkH-nc5McJAL90c9M2zyK3SUBD-goxFDWwciWleG-5UNY-vQEAFO4C-LfejLun3KUUNiN9OKschITjxVutL6JDjC-AdCrBMWvMQg6D5TBPVYtVQuYc7trk41dQeGwsPCgf5o8Q00S47jA_XRzKJgP8Ej9yIzIUkeguC2BE-odibcWsDIstnJUUDWD7XXm_X_iY0huFwnTMZfnJbtYyuzTmAnHLYygh7sO6OrCJfVX9-CLr9r1FqyRuvEfXtkBXKeUIm1t04QR9Mf4tvNGNJ04pshbEQ7hBiweoZHkPhz5ivM35eUEnHHsgg8LaA7X792-m5GoFrj4EojrhdG6Rp-nuklJUg-QtE3OeFeVBMXf6FTdaushPMNI_NhRmJyuxOyeYu_fQvXbUaMEufmnN1yEOOClXIW5Qng0SzqMlgFcuvWqdQ-Ap_a_ffxq9VXu1_9zSaa8K8ue3ftyXvDjcEl6W7w4ZvQww_9FT36VOw4ThCGh4Xmg", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?btt=GWAwtOwHOyK5Psg2mVwOPOK-wEQPX9FV4rPxwGWSF6A&uid=2174005878281702669&tp=an&gdpr=0", END_INLINE,
                "URL=https://kohls.sspinc.io/v1/ssp.js?env=prd", END_INLINE,
                "URL=https://s.pinimg.com/ct/lib/main.c8288b79.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_3");
        status = nsApi.ns_web_url ("test_rum_nv_3",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=1|1&lts=233048557&nvcounter=0",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_28");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_28",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_28", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("ssp_json");
        status = nsApi.ns_web_url ("ssp_json",
            "URL=https://kohls.sspinc.io/ssp.json?origin=https%3A%2F%2Fwww.kohls.com&lang=en"
        );

        status = nsApi.ns_end_transaction("ssp_json", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_4");
        status = nsApi.ns_web_url ("test_rum_nv_4",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&op=pagedump&pi=1&CavStore=-1&pid=3&d=3|2|0&lts=233048557&nvcounter=2",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("tag");
        status = nsApi.ns_web_url ("tag",
            "URL=https://tagtracking.vibescm.com/tag",
            INLINE_URLS,
                "URL=https://kohls.sspinc.io/lib/4.39.5/fitpredictor.en.min.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s67622683239598?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A7%3A10%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&events=event38&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&c50=D%3Ds_tempsess&c53=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&c64=VisitorAPI%20Present&v68=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&pe=lnk_o&pev2=Sku%20Selection%20-%20Manual&c.&a.&activitymap.&page=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&link=White%20With%20Stitching&region=panel7212524&pageIDType=1&.activitymap&.a&.c&pid=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&pidt=1&oid=javascript%3Avoid%280%29%3B&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://data.adxcel-ec2.com/pixel/?ad_log=referer&action=misc&value={%22af_revenue%22:%22[value]%22,%22af_order_id%22:%22[order_id]%22}&pixid=6a175b53-c680-4327-aef7-1eb5647a6339", END_INLINE
        );

        status = nsApi.ns_end_transaction("tag", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("index_14");
        status = nsApi.ns_web_url ("index_14",
            "URL=https://ct.pinterest.com/user/?tid=2616391205865&cb=1621582629846",
            INLINE_URLS,
                "URL=https://ct.pinterest.com/v3/?tid=2616391205865&event=init&ad=%7B%22loc%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares%22%2C%22if%22%3Afalse%2C%22sh%22%3A768%2C%22sw%22%3A1366%2C%22mh%22%3A%22c8288b79%22%2C%22floc_enabled%22%3Afalse%7D&cb=1621582629850", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=9780590666709;gtm=2od5c1;auiddc=1177027742.1621582573;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=84795902680038641394405446550017377092;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4?", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8478690a6f32401fb3fe279105e2aa1c-source.min.js", END_INLINE,
                "URL=https://kohls.sspinc.io/skins/kohls/2.0.4/fitpredictor.css", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www47/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://sb.scorecardresearch.com/beacon.js", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=CPT6zYWi2vACFTof1QodUBwM9A;src=8632166;type=landi0;cat=unive0;ord=9780590666709;gtm=2od5c1;auiddc=1177027742.1621582573;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=84795902680038641394405446550017377092;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4?", END_INLINE,
                "URL=https://cdnssl.clicktale.net/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf3953ba39c0e4cda986a27cd34fdd08c-source.min.js", END_INLINE,
                "URL=https://cdnssl.clicktale.net/pcc/d82d7432-724c-4af9-8884-ffab4841f0a1.js?DeploymentConfigName=Release_20210505&Version=4", END_INLINE,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CPT6zYWi2vACFTof1QodUBwM9A;src=8632166;type=landi0;cat=unive0;ord=9780590666709;gtm=2od5c1;auiddc=*;u1=4009813;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4;u11=Women's%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=84795902680038641394405446550017377092;u25=04c7b2a4-9eb5-41be-bf31-441b23617ef2;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www/latest-WR110.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCfde5714c92254dfd820f510d3727aef2-source.min.js", END_INLINE,
                "URL=https://d.agkn.com/pixel/10107/?che=244867575&mcvisid=84795902680038641394405446550017377092", END_INLINE,
            "URL=https://c.sspinc.io/com.snowplowanalytics.snowplow/tp2", "METHOD=OPTIONS", END_INLINE,
                "URL=https://sc-static.net/js-sha256-v1.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC01d660fbc4b44effb3436273c815864f-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_14", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X009813");
        status = nsApi.ns_web_url ("X009813",
            "URL=https://api.kohls.com/v1/marketplace/product/4009813",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC4d3fb96a16274690a8983bbe28d8c339-source.min.js", END_INLINE,
                "URL=https://b-code.liadm.com/a-00oc.min.js", END_INLINE,
                "URL=https://b-code.liadm.com/sync-container.js", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621582631298&cv=9&fst=1621582631298&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621582631304&cv=9&fst=1621582631304&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://www.google-analytics.com/analytics.js", END_INLINE,
                "URL=https://www.google-analytics.com/collect?v=1&_v=j90&a=527914213&t=pageview&_s=1&dl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ul=en-gb&de=UTF-8&dt=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&sd=24-bit&sr=1366x768&vp=1349x607&je=0&_u=YChAgAAB~&jid=1679915476&gjid=1124835175&cid=1289170567.1621582601&tid=UA-45121696-1&_gid=480143324.1621582632&cd2=04c7b2a4-9eb5-41be-bf31-441b23617ef2&cd4=&z=1636855007", END_INLINE
        );

        status = nsApi.ns_end_transaction("X009813", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("prediction_2");
        status = nsApi.ns_web_url ("prediction_2",
            "URL=https://fitpredictor-api.sspinc.io/v1/prediction?type=size&auth_token=kohls%3A5a62d296439256998b4c990b80cad392&page_view_id=3ed40383-3b5f-43c8-8d1c-7465b48b9d93&user_email_hash=&domain_userid=45ca23a3-3c3b-4fe4-910e-66ae57fb479e&mode=prediction&market=US&env=prd&lang=en&product_id=4009813&size=2&size_type=regular&available_size=2&available_size=2%20T%2FL&available_size=4&available_size=4%20T%2FL&available_size=6&available_size=8&available_size=10&available_size=12&available_size=14&available_size=16&available_size=16%20T%2FL&psud_enabled=false"
        );

        status = nsApi.ns_end_transaction("prediction_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X009813_2");
        status = nsApi.ns_web_url ("X009813_2",
            "URL=https://api.kohls.com/v1/marketplace/product/4009813"
        );

        status = nsApi.ns_end_transaction("X009813_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_15");
        status = nsApi.ns_web_url ("index_15",
            "URL=https://ct.pinterest.com/md/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621582631304&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=1727689666&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621582631304&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=1727689666&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621582631298&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=1836287719&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621582631298&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=7&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&tiba=Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans&async=1&fmt=3&is_vtc=1&random=1836287719&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_15", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_16");
        status = nsApi.ns_web_url ("index_16",
            "URL=https://ing-district.clicktale.net/ctn_v2/auth/?pid=24&as=1&323480169&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_16", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("tp2_2");
        status = nsApi.ns_web_url ("tp2_2",
            "URL=https://c.sspinc.io/com.snowplowanalytics.snowplow/tp2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgEoFMA2KCGBnFB9AJgAYCBGIgVkqAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=876383", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsB2D2AEDGBXAzgF2gWwKYCdYEsATIA%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=499448", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsBsCMBmBMATAHAU1QWmvARgBgwBYBDY5DMgYwE4NVqBmAdgemQFZZlJ2gAA&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=432155", END_INLINE,
                "URL=https://c.clicktale.net/pageview?pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&lv=1621582630&lhd=1621582630&hd=1621582630&pn=1&re=1&dw=1349&dh=6594&ww=1366&wh=607&sw=1366&sh=768&dr=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&uc=1&la=en-GB&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22pdp%20(4009813)%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22pdp%20(4009813)%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&v=10.8.6&r=980624", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsAMAsDGDsAjATAQ2gWgJwFNEFYNoBGRHDRAMwGZiiSVqA2Y%2BHS5IAA%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=390615", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=PIOwNg9ghgJgBAHzgZQC4E8wEtVYMYDOicAwhCKgE4RhAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=854917", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=PIOwNg9ghgJgBAHzgZQC4E8wEtVYMYDOicAwlAE4QCuBApmEAAA%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=067015", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=PIAgPiDKCSCaDq4SwKYGcgAA&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=449058", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www/WR1113b.js", END_INLINE,
                "URL=https://c.clicktale.net/dvar?v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&dv=N4IgDgTg9mCMDsIBcIDWUAWAbAzgAgDsoAXPLKAcwoFMATPASwIB9yBPAQy2LcJLMo16TEABoQASXwB5AngDKXashAAzLjmUBfIAAA%3D%3D&r=472484", END_INLINE,
                "URL=https://c.clicktale.net/dvar?v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&dv=N4IgggQg%2BmAqUA1YAIC0yBSAmNyDKApgC7IBqAhgE7IBmA9tRAQHbIASArjTQLbmvoAslQDWyAHIEA7iABcIAKIAPAA4FKASxYBjAsjAgANOGhxEsAAXoAIoOSwASgBYAnAEYAHLiTIH0qgAmAM7IAAoaADZ0JFgADFhuqLEArElOciCxbmGR0chusbEApEYmMPCQuLb2Di5OAOwAbLgAMqF4uKHWocgQ5AEA5npxCUmpbi4ZWWHdvf1DpZDlMBBVdo71WI0uuBAA8gDie3ODerAESjHxiSlJAMxT2W4A9Dh7AGLvi6YVqzbrDg8TliO3QfnIEXsGh4ekIVG0AAswuognRmBCNAAvKaxZAAYTRREodAi32WlX%2BNSasScuDweJauAA0nj9NoAI4cDRBDREDRo5ACrqhDIswIzHqCAhBILkAYaZgDMlmCnIaxhBxZAp0hmdFHcogsEh9Aa%2BAjaEJox72aXGuV%2BC3K35VCR42J3FI6xnoBR8SLIPYqEgASVYeARGhUKgVputYAikKICL0fWYzBRTpWaxqyQKXnQ715YUoBACGm0RAYyAA6hpTiRzkEYlMcLAESW9O8COQiBwS5nVdVwtoRBwVPoAEb8AJomHMBvkCvlkI0pL1VAja2Nkh%2BAIcXST6dooKZnyU4SUETEFQRRcp%2BYx5AjWKoAobtwZPqDGMDv5q13ui4yResydAIhEIR4uKBxRFOkJsN2ATWqEJachCyAtAqIg%2FsYSwqn%2B6qOFgdxOMBPoRAQABuPalviJJVoQFFLgK6AivIhTsCSM4cEQp4oJSfh0GoYaViWGEGhqezqs%2BYxpBkyhqJoOh6AYAC%2BQAA%3D&r=799640", END_INLINE
        );

        status = nsApi.ns_end_transaction("tp2_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("collect");
        status = nsApi.ns_web_url ("collect",
            "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j90&tid=UA-45121696-1&cid=1289170567.1621582601&jid=1679915476&gjid=1124835175&_gid=480143324.1621582632&_u=YChAgAABAAAAAE~&z=1435960289",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=AoEWAIGYgAA%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=756414", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=DIHwBACgIhQAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=702565", END_INLINE
        );

        status = nsApi.ns_end_transaction("collect", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("j");
        status = nsApi.ns_web_url ("j",
            "URL=https://rp.liadm.com/j?tna=v2.0.1&aid=a-00oc&wpn=lc-bundle&pu=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&refr=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&ext_s_ecid=MCMID%7C84795902680038641394405446550017377092&ext_s_vi=%5BCS%5Dv1%7C3053B175BA7DF942-600006ECB012BDA4%5BCE%5D&ext_IXWRAPPERLiveIntentIp=%7B%22t%22%3A1621582575302%2C%22d%22%3A%7B%22response%22%3A%22pass%22%2C%22version%22%3A%221.1.1%22%7D%2C%22e%22%3A1621668975302%7D&duid=0b10d8358f40--01f66xpm9mhnhjgd8c760569v9&se=e30&dtstmp=1621582631314",
            INLINE_URLS,
                "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1289170567.1621582601&jid=1679915476&_u=YChAgAABAAAAAE~&z=1056049096", END_INLINE,
                "URL=https://www.google.co.in/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1289170567.1621582601&jid=1679915476&_u=YChAgAABAAAAAE~&z=1056049096", END_INLINE,
                "URL=https://i.liadm.com/s/c/a-00oc?s=&cim=&ps=true&ls=true&duid=0b10d8358f40--01f66xpm9mhnhjgd8c760569v9&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMkAwIwFYDsA2ALHDCYE4kyAAA&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1&r=164257", END_INLINE
        );

        status = nsApi.ns_end_transaction("j", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_17");
        status = nsApi.ns_web_url ("index_17",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&0&0&0&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://sli.kohls.com/baker?dtstmp=1621582632345", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_17", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_18");
        status = nsApi.ns_web_url ("index_18",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&1&0&1&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_18", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_19");
        status = nsApi.ns_web_url ("index_19",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&2&0&2&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_19", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_29");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_29",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=127444&dpuuid=2f0820c3-f295-4903-a9bb-11e006e21403&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2Fe49dd008e45e447aaebc02f6477aa64b%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", "REDIRECT=YES", "LOCATION=https://i.liadm.com/s/e/a-00oc/0/e49dd008e45e447aaebc02f6477aa64b?mpid=82775&muid=89726096032446728673750329446651071084", END_INLINE,
                "URL=https://trc.taboola.com/sg/liveintent/1/cm/", END_INLINE,
                "URL=https://sync.mathtag.com/sync/img?mt_exid=36&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2Fe49dd008e45e447aaebc02f6477aa64b%3Fmpid%3D7156%26muid%3D%5BMM_UUID%5D&2f0820c3-f295-4903-a9bb-11e006e21403", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1", END_INLINE,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=2f0820c3-f295-4903-a9bb-11e006e21403", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_29", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("tp2_3");
        status = nsApi.ns_web_url ("tp2_3",
            "URL=https://c.sspinc.io/com.snowplowanalytics.snowplow/tp2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=2f0820c3-f295-4903-a9bb-11e006e21403&rd=Y", END_INLINE
        );

        status = nsApi.ns_end_transaction("tp2_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("addItemToCart_2");
        status = nsApi.ns_web_url ("addItemToCart_2",
            "URL=https://www.kohls.com/cnc/checkout/cartItems/addItemToCart",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://i.liadm.com/s/e/a-00oc/0/e49dd008e45e447aaebc02f6477aa64b?mpid=82775&muid=89726096032446728673750329446651071084", END_INLINE
        );

        status = nsApi.ns_end_transaction("addItemToCart_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_20");
        status = nsApi.ns_web_url ("index_20",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&3&1&0&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_20", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("minicart");
        status = nsApi.ns_web_url ("minicart",
            "URL=https://www.kohls.com/snb/staticmessages/minicart",
            INLINE_URLS,
                "URL=https://x.bidswitch.net/syncd?dsp_id=256&user_group=2&user_id=2f0820c3-f295-4903-a9bb-11e006e21403&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=2f0820c3-f295-4903-a9bb-11e006e21403&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", END_INLINE,
                "URL=https://i.liadm.com/s/e/a-00oc/0/e49dd008e45e447aaebc02f6477aa64b?mpid=7156&muid=d85160a7-632a-4b00-bb0b-83bae74b3e35", END_INLINE,
                "URL=https://x.bidswitch.net/sync?ssp=liveintent&user_id=2f0820c3-f295-4903-a9bb-11e006e21403", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=2f0820c3-f295-4903-a9bb-11e006e21403", END_INLINE,
                "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=39b7b682-b183-48f1-a9f9-71eee4300ca4", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=39b7b682-b183-48f1-a9f9-71eee4300ca4", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s6620601325132?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A7%3A14%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=cart%3Aadd%20item&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=cart%20add&events=scAdd%2CscOpen&products=%3B4009813%3B1%3B29.99%3B%3Bevar11%3Dnot%20collection%7Cevar13%3Dkf%7Cevar74%3D50_29.99%7Cevar16%3Dn%7Cevar51%3D46684262%7Cevar77%3Dship%20only%7Cevar107%3Dpdp-regular&c4=cart%20add&c9=cart%7Ccart%20add&v9=pdp%20%284009813%29%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans&c10=cart&c11=cart&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=guest&v48=7207752818350001&c50=D%3Ds_tempsess&c53=cart%3Aadd%20item&v59=product%20page&c64=VisitorAPI%20Present&v68=cart%3Aadd%20item&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&v75=pdp20-standard&v76=s2nog7eutjvstgseuvy9ooj2kw0vguq4f4o46jwdvz4%3D&v86=21may.w2&v87=pdp20&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("minicart", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("errors");
        status = nsApi.ns_web_url ("errors",
            "URL=https://c.clicktale.net/errors?v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=1",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=2f0820c3-f295-4903-a9bb-11e006e21403&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=//i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=fccaa054-e728-41d2-b51c-147933d4c70f", END_INLINE,
                "URL=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=2f0820c3-f295-4903-a9bb-11e006e21403", "REDIRECT=YES", "LOCATION=//cm.g.doubleclick.net/pixel?google_nid=bidswitch_dbm&google_cm&google_sc&ssp=liveintent&bsw_param=fccaa054-e728-41d2-b51c-147933d4c70f&google_hm=ZmNjYWEwNTQtZTcyOC00MWQyLWI1MWMtMTQ3OTMzZDRjNzBm", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/right-slide-black.png", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=PersistentBag%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/right-slide-grey.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/left-slide-grey.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/left.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/icCartG.png", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=bidswitch_dbm&google_cm&google_sc&ssp=liveintent&bsw_param=fccaa054-e728-41d2-b51c-147933d4c70f&google_hm=ZmNjYWEwNTQtZTcyOC00MWQyLWI1MWMtMTQ3OTMzZDRjNzBm", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/kohls_expert_card.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/rightnw.svg", END_INLINE,
                "URL=https://i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=fccaa054-e728-41d2-b51c-147933d4c70f", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/ajax-loader.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("errors", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_21");
        status = nsApi.ns_web_url ("index_21",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&4&1&1&104&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching_sw", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4009813_White_With_Stitching?wid=180&hei=180&op_sharpen=1", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-4dlye7u&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&mode=v2&cf=4847505%2C6603093%2C7422632%2C7424035%2C7472048&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA&errors=%7B%22site%22%3A%224DPyaxM%22%2C%22referrer%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4%22%2C%22errors%22%3A%5B%7B%22type%22%3A%22wait%22%2C%22message%22%3A%22ReferenceError%3A%20pintrk%20is%20not%20defined%22%2C%22tagId%22%3A6603093%2C%22timestamp%22%3A1621582598522%7D%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_21", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_6");
        status = nsApi.ns_web_url ("experiences_6",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=PersistentBag%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://sc-static.net/js-sha256-v1.min.js", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=45ae5150-a754-4f3f-a04d-a5f01e98b39f&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&prodid=4009813&pagetype=cart&en=Y&evt=custom&msclkid=N&rn=693535", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_22");
        status = nsApi.ns_web_url ("index_22",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&5&1&2&104&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=IIHwBACgIhbsATBBTBYAuB7MBhAhgE7pAAA%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=2&r=106247", END_INLINE,
                "URL=https://c.clicktale.net/pageview?pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&lv=1621582630&lhd=1621582630&hd=1621582635&pn=2&re=1&dw=1349&dh=6544&ww=1366&wh=607&sw=1366&sh=768&dr=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3F__cs-popin-add-to-cart%3FprdPV%3D4&uc=1&la=en-GB&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22pdp%20(4009813)%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22pdp%20(4009813)%20women%27s%20lc%20lauren%20conrad%20patch%20pocket%20high%20rise%20flare%20jeans%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&v=10.8.6&r=344437", END_INLINE,
                "URL=https://c.clicktale.net/events?v=10.8.6&str=1415&di=1870&dc=36472&fl=36563&sr=21&mdh=6625&pn=1&re=1&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&lv=1621582630&lhd=1621582630&hd=1621582630&pid=2399&e=NrAMBoEYE4oZgGwPA0B2AuuYkoICywQAckc4%2BATHFsGlMXLJKPhGZcgEQAmAlgDcAxAAUAIiID6ABwBOAex4BXAMYAXSSvkA7AGZ8A5ktkBDNXx0A%2BfgIBcAUwCOAClABKa4Icv3nu0%2BdKDxtvZ0hgrwC4CP8fDxNQ9y5aZEpKfABWcGhoekg03C4%2BbWklNSETHh41eQAjEwMASTEucC5k7Epwahhs6HZKUCI24tLyyuq6hubWyAQ0UnAM0Bh8WnJKNDhcaGIsyBhiEZKyiqqa%2BqaWto6cXE22Y7GzycuZ2i7NjJ29qEPwCDtKAZYiocBwLaUSC0fDdYiULK7fb%2FIoncbnKZXVrtWhZSiMehIv67J6nCYXabXHHYehMXLZX7QZak9GvSnJDBAA%3D%3D&r=658724", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3847629_Modern_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4912037_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://ct.pinterest.com/v3/?event=addtocart&ed=%7B%22currency%22%3A%22USD%22%2C%22line_items%22%3A%5B%7B%22product_name%22%3A%22Women%27s%20LC%20Lauren%20Conrad%20Patch%20Pocket%20High%20Rise%20Flare%20Jeans%22%2C%22product_id%22%3A%224009813%22%2C%22product_category%22%3A%22bottoms%22%2C%22product_price%22%3A%2229.99%22%2C%22product_quantity%22%3A%22%22%7D%5D%7D&tid=2616391205865&pd=%7B%22pin_unauth%22%3A%22dWlkPVltTTNPR0pqT0RrdE5XVmtZaTAwTUdJMExXSXlZamd0TW1ZMU9USXlaRGM1Wm1ZMg%22%7D&ad=%7B%22loc%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-flare-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BLegOpening%3AFlare%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Dwmsjeans-VN-flares%22%2C%22if%22%3Afalse%2C%22sh%22%3A768%2C%22sw%22%3A1366%2C%22mh%22%3A%22c8288b79%22%2C%22floc_enabled%22%3Afalse%7D&cb=1621582635848", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_22", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_13");
        status = nsApi.ns_web_url ("floop_13",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_5");
        status = nsApi.ns_web_url ("test_rum_nv_5",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=3&d=3|0|2&lts=233048557",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_23");
        status = nsApi.ns_web_url ("index_23",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&6&1&3&105&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/3944205_Pink_Stripe?wid=300&hei=300&op_sharpen=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_23", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_24");
        status = nsApi.ns_web_url ("index_24",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&7&2&0&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_24", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_25");
        status = nsApi.ns_web_url ("index_25",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&8&3&0&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_25", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_26");
        status = nsApi.ns_web_url ("index_26",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&9&4&0&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_26", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_27");
        status = nsApi.ns_web_url ("index_27",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&11&0&3&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_27", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_28");
        status = nsApi.ns_web_url ("index_28",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&12&0&4&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_28", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_29");
        status = nsApi.ns_web_url ("index_29",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&10&5&0&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_29", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_6");
        status = nsApi.ns_web_url ("test_rum_nv_6",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=1|1&lts=233048569&nvcounter=3",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_30");
        status = nsApi.ns_web_url ("index_30",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&13&0&5&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_30", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_31");
        status = nsApi.ns_web_url ("index_31",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&14&0&6&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_31", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_30");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_30",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_30", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(79.575);

        status = nsApi.ns_start_transaction("shopping_cart_jsp");
        status = nsApi.ns_web_url ("shopping_cart_jsp",
            "URL=https://www.kohls.com/checkout/shopping_cart.jsp",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=IIHwwgNglgxg1gUwCYAIBuUEHcUwIYBOALkAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&pn=2&r=070153", END_INLINE
        );

        status = nsApi.ns_end_transaction("shopping_cart_jsp", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_32");
        status = nsApi.ns_web_url ("index_32",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&15&0&7&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_32", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_33");
        status = nsApi.ns_web_url ("index_33",
            "URL=https://684fc539.akstat.io/",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_33", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("events");
        status = nsApi.ns_web_url ("events",
            "URL=https://c.clicktale.net/events?v=10.8.6&str=1415&di=1870&dc=36472&fl=36563&sr=12&mdh=6625&pn=2&re=1&uu=4b543957-c26c-a015-bd94-50f9e9367d73&sn=1&lv=1621582630&lhd=1621582630&hd=1621582635&pid=2399&eu=%5B%5B1%2C405%2C0%2C0%2C810%5D%2C%5B6%2C885%2C985%2C385%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(9)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C1408%2C985%2C385%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(9)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%22%5D%2C%5B6%2C1410%2C985%2C385%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(9)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Eimg%3Aeq(0)%22%2C%22%22%5D%2C%5B2%2C6300%2C985%2C387%2C0%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(9)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Eimg%3Aeq(0)%22%2C3397%2C14311%5D%2C%5B7%2C6484%2C971%2C386%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(9)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Eimg%3Aeq(0)%22%5D%2C%5B6%2C6567%2C949%2C373%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(8)%3Ediv%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C6598%2C941%2C367%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(8)%3Ediv%3Aeq(0)%22%5D%2C%5B6%2C6600%2C941%2C367%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(8)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Eimg%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C6687%2C923%2C344%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ea%3Aeq(8)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Eimg%3Aeq(0)%22%5D%2C%5B2%2C6711%2C918%2C338%2C0%2C%22div%23bd_rec_PersistentBag%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%22%2C48351%2C51492%5D%2C%5B6%2C6980%2C684%2C213%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C7014%2C654%2C204%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%5D%2C%5B2%2C7115%2C643%2C199%2C0%2C%22ul%23sliderSection%3Eli%3Aeq(0)%22%2C63981%2C30427%5D%2C%5B6%2C7352%2C672%2C208%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%2C%5B2%2C7518%2C681%2C207%2C0%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C14522%2C21845%5D%2C%5B3%2C7766%2C681%2C206%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%2C%5B12%2C7784%2C%22input%23addtobagID%22%5D%2C%5B2%2C7926%2C681%2C206%2C0%2C%22%22%2C14522%2C17476%5D%2C%5B4%2C7962%2C681%2C206%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%2C%5B5%2C7980%2C681%2C206%2C%22div%23persistent_bar_container%3Ediv%3Aeq(1)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(0)%3Ediv%3Aeq(1)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%5D",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("events", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_7");
        status = nsApi.ns_web_url ("test_rum_nv_7",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=el&pi=1&CavStore=-1&pid=3&d=3&lts=233048569&nvcounter=4",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_8");
        status = nsApi.ns_web_url ("test_rum_nv_8",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=xhrdata&pi=1&CavStore=-1&d=3|1|0||132|-1|-1|-1&lts=233048569&nvcounter=5",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_9");
        status = nsApi.ns_web_url ("test_rum_nv_9",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=jserror&pi=1&CavStore=-1&d=1|3&pid=3&lts=233048569&nvcounter=6",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_10");
        status = nsApi.ns_web_url ("test_rum_nv_10",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=0|1&lts=233048571&nvcounter=7",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_11");
        status = nsApi.ns_web_url ("test_rum_nv_11",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=0|1&lts=233048571&nvcounter=8",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_12");
        status = nsApi.ns_web_url ("test_rum_nv_12",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=3&d=3|0|2&lts=233048569",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_14");
        status = nsApi.ns_web_url ("floop_14",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_14", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X_2");
        status = nsApi.ns_web_url ("X_2",
            "URL=https://bat.bing.com/actionp/0?ti=4024145&tm=al001&Ver=2&mid=45ae5150-a754-4f3f-a04d-a5f01e98b39f&sid=426f0290ba0711eb95091595b023fa85&vid=4270a490ba0711ebae47c5355fd6fce6&vids=0&evt=pageHide",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_13");
        status = nsApi.ns_web_url ("test_rum_nv_13",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000935772041251446&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=0|1&lts=233048572&nvcounter=9",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/skava-custom.css", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjstVVWW8eXlOXMNNKsDkHFytvsBlRGg_3wNlvJ9TDTBDHHrUB6DUVuBTf2o9ZggVoUjJz0lQWmFTbN-AHQGR1wB2foNxNiqjNPzA_g9ZwKRcdaA2jlyL&sig=Cg0ArKJSzN0mpzQeLhaNEAE&id=lidartos&mcvt=16215&p=168,163,232,1187&mtos=16134,16176,16215,16231,16260&tos=31638,78,63,54,66&v=20210519&bin=7&avms=nio&bs=1349,607&mc=1&app=0&itpl=3&adk=660533235&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621582601631&dlt=0&rpt=158&isd=317&msd=1713&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjstg-kY_mm4SFcJZmSos4X9AVHBov-18yXj3CURZ-mp630iVwOv-8jorIXE4EVJmUczKaBftKHenGb4nJgGH47VkbbmQqxzty8hl9-_9di7-LJlUbTCo&sig=Cg0ArKJSzPQm2xtOMCohEAE&id=lidartos&mcvt=0&p=5103,485,5353,785&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=0,0&mc=0&if=1&app=0&itpl=19&adk=4172809658&rs=4&met=ie&la=0&cr=0&osd=1&vs=3&rst=1621582601636&dlt=17&rpt=247&isd=317&msd=1713&r=u&fum=1", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/ipad.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/environment.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/shoppingcart.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/header.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/gwpModal.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_34");
        status = nsApi.ns_web_url ("index_34",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301576414150970&24&11&16&0&8&9&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/tr_gwp.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/sub_v1.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/boss_checkout.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/eliteLoyalty.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/saveForLater.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/fonts/hfjFonts.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/framework.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/jquery.validate.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/jquery.maskedinput.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/js/kohls.restrictCharacter.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/shoppingtrcartR51.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/js/kohlsutility.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/omniture/s_code.js", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/omniture_tracking.js", END_INLINE,
                "URL=https://www.kohls.com/media/images/error-icon.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/js/csspopup.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/shoppingcart.js", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_34", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_5");
        status = nsApi.ns_web_url ("mobilemenu_json_5",
            "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/mobile/mobilemenu.tpl.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/pb.module.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_31");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_31",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/fancybox/fancybox.png", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/v1/config/wishlistconfig.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/static/ss/sstimer.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_31", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_6");
        status = nsApi.ns_web_url ("delivery_6",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=15e43915071149cda04c993604a7f382&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=6440147674919822059&jsession=e520494583b9eb872bf95fb8bcb6fc39&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&scriptVersion=1.11.2&dyid_server=6440147674919822059&ctx=%7B%22type%22%3A%22CART%22%2C%22data%22%3A%5B%2246684262%22%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_32");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_32",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_32", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("shopping");
        status = nsApi.ns_web_url ("shopping",
            "URL=https://www.kohls.com/snb/staticmessages/shopping",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/app/shoppingbag/saveforlater/tpl.saveForLater.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/tpl.gwp_addtogift.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/tpl.elite_loyalty_banner.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/tpl.shoppingcart.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/tpl.common_checkout.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/R51/javascript/tmpl/tr/tpl.order_summary.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s6526661726655?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A7%3A27%205%20-330&d.&nsid=0&jsonv=1&.d&fid=470ED0FAFF43B1EA-3ACBA20E941EA296&ce=UTF-8&ns=kohls&pageName=cart&g=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&r=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=cart&events=scView&products=%3B4009813%3B1%3B29.99%3Bevar51%3D46684262%7Cevar77%3Dship%20only%7Cevar6%3Dstd&c4=cart&v4=n&c9=cart&v9=cart%3Aadd%20item&c10=cart&c11=cart&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C02%3A00%20am&v18=fri%7Cweekday%7C02%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=guest&v48=7207752818350001&c50=D%3Ds_tempsess&c53=cart&c64=VisitorAPI%20Present&v68=cart&v70=04c7b2a4-9eb5-41be-bf31-441b23617ef2&v71=klsbrwcki%7C04c7b2a4-9eb5-41be-bf31-441b23617ef2&v73=no%20loyalty%20id&c75=w%3Efsrec%7C%3C%2445.01%3E&c.&a.&activitymap.&page=cart%3Aadd%20item&link=View%20Cart&region=persistent_bar_container&pageIDType=1&.activitymap&.a&.c&pid=cart%3Aadd%20item&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("shopping", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_7");
        status = nsApi.ns_web_url ("uia_7",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621582647033",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_10");
        status = nsApi.ns_web_url ("batch_10",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621582647113_818674",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/images/myStore.png", END_INLINE,
                "URL=https://www.kohls.com/media/images/enrollment_help.jpg", END_INLINE,
                "URL=https://www.kohls.com/media/images/kohls-cash.png", END_INLINE,
                "URL=https://www.kohls.com/media/images/kohls_expert_card.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/tr-red-circle.jpg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/tr_ckbox_default.jpg", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=ShoppingBag&plids=Horizontal%7C3", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/37.0.0-842/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC6ce36fbfea7943ac9ff55501e2f0bcb4-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_5");
        status = nsApi.ns_web_url ("X349_5",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_7");
        status = nsApi.ns_web_url ("experiences_7",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=ShoppingBag&plids=Horizontal%7C3",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("experiences_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_15");
        status = nsApi.ns_web_url ("floop_15",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&H=2r8nsxd&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NjQ0Mzg1Njk3MzExMTQ4NjgwMw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTM2MDY2MzU2NTQwMjQ2MjMwOA", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=6&dtm_email_hash=N/A&dtm_user_id=04c7b2a4-9eb5-41be-bf31-441b23617ef2&dtmc_product_id=46684262", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_15", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_16");
        status = nsApi.ns_web_url ("floop_16",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621582648943&cv=9&fst=1621582648943&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621582648945&cv=9&fst=1621582648945&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635470%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621582648943&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&async=1&fmt=3&is_vtc=1&random=3753891539&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621582648943&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&async=1&fmt=3&is_vtc=1&random=3753891539&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621582649152&mi_u=anon-1621582571535-2013629335&mi_cid=8212&page_title=Shopping%20Bag%20-%20Kohls.com&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&timezone_offset=-330&event_type=pageview&cdate=1621582649151&ck=host&anon=true", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621582648945&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&async=1&fmt=3&is_vtc=1&random=907071092&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621582648945&cv=9&fst=1621580400000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=8&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&tiba=Shopping%20Bag%20-%20Kohls.com&async=1&fmt=3&is_vtc=1&random=907071092&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", "METHOD=OPTIONS", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/73cde545-49e3-4b30-c59f-6908958798f0", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://f8d4029ffad39e3c054d8c97e2100337.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=1768233516289477&correlator=3401829940819099&output=ldjh&impl=fifs&eid=31060853%2C31061216%2C31061225%2C21064365%2C31061004&vrg=2021051701&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2CROS&enc_prev_ius=%2F0%2F1&prev_iu_szs=320x50%7C1024x45%7C1024x64%7C1024x128&fluid=height&prev_scp=pos%3Dmarquee&cust_params=channel%3Ddesktop%26env%3Dprod&cookie=ID%3D179a88961f5360de%3AT%3D1621582576%3AS%3DALNI_MZIooglrOduaLWOV0A8ZrWZcT7dNg&bc=31&abxe=1&lmt=1621582650&dt=1621582650377&dlt=1621582645104&idt=5150&frm=20&biw=1349&bih=607&oid=3&adxs=163&adys=168&adks=4275178073&ucis=1&ifi=1&u_tz=330&u_his=8&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2Fcheckout%2Fshopping_cart.jsp&ref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-4009813%2Fwomens-lc-lauren-conrad-patch-pocket-high-rise-flare-pants.jsp%3FprdPV%3D4&vis=1&dmc=8&scr_x=0&scr_y=0&psz=1349x70&msz=1024x70&ga_vid=1289170567.1621582601&ga_sid=1621582650&ga_hid=608038332&ga_fc=true&fws=0&ohw=0&btvi=0", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssJxBgVXdS2v4KCbgq8LVORDMHVEyFw4hhcpLyCvIJXax47AAYW6kH9IQ58bU_AAmoboLj-AR1sD7Um8WcqFz509y7aHtOkS-cYDFOqW_WZTZ2go4IVsDopM5fiNBXf-8a08tOqx0pR_29Y2yGZaSIUWrFnSpTa8WJUQ9dE3essvV7sghna8QxxKgftsZaM1zyYx16eYRaWeNc7VyRa0szIWta2ktKW0AH5t_ds0kra-l-h3uogDDy0Gqf-UrpJZbdtBQlNo_tvT0wTTz6sg0Iw0s-wHO_R1zngAVUJ-BRh&sig=Cg0ArKJSzFe-NtZeyndGEAE&adurl=", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaTn6V8QDqsVmyhhwW-_ckK5Tua1XxI_VQzr4lzK4CUNQ5Ie1awWoIB0hRdtA4W6Oj6LXakPPuI3zFkOpC3tVcj1ZVEcBA", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvz3Iid8ecTo-KtwWrkSW2T9thIVtVaWtVqBL2aDZTjRi7CRgpZ_cIfXYrDuyMc_1hv8FZ90wbsbzkkc71ytJiegpPxIAscWjAHs3SWu5pf6EDTWa3bz3_Xb09mKREFuy_FO4_PPMK4yQcafFwyhHp-vbjf7khJ-5rM218HIVxTQl489OFqTNOrjw62mJz9-JFEePOWYjePxAy2fYItRGHLE9XRK-MpnKioioq_QEai4OiAwe_oNyOFZ6UnA4u0_ntEu5KQY6LeS2145YcW79eRW1Ig&sig=Cg0ArKJSzFvrWSgY8hAZEAE&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138343070434&adorderid=2774409325&adunitid=40820232&advertiserid=32614632&lineitemid=5641706618&rand=1398864601", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsvTMA67m1kxykDrFnYvv-S9cIgpM3ePyF2vd5WyNbPfEhXq0zdv8XWVcB1twFo5oJ1we1hGau5dbNT2MeU-HMUOhEA9P8-lMpxyEHnnrbQ&sig=Cg0ArKJSzLKWQtr0Kn-pEAE&id=lidar2&mcvt=1001&p=168,163,232,1187&mtos=1001,1001,1001,1001,1001&tos=1001,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=1&app=0&itpl=3&adk=4275178073&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621582651014&dlt=0&rpt=101&isd=0&msd=0&r=v&fum=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_16", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.958);

        return status;
    }
}
