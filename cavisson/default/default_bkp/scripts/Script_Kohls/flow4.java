/*-----------------------------------------------------------------------------
    Name: flow4
    Recorded By: cavisson
    Date of recording: 05/21/2021 01:36:46
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_Kohls;
import pacJnvmApi.NSApi;

public class flow4 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_35");
        status = nsApi.ns_web_url ("index_35",
            "URL=http://www.kohls.com/",
            "REDIRECT=YES",
            "LOCATION=https://www.kohls.com/",
            INLINE_URLS,
                "URL=https://www.kohls.com/", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/newHomepagestyle.css", END_INLINE,
                "URL=https://www.kohls.com/akam/11/7d34ecfc", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environ3.js", END_INLINE,
                "URL=https://www.kohls.com/media/images/global-header-refresh-icons/order-status-icon.png", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://s.go-mpulse.net/boomerang/4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/global-header-refresh-icons/search-icon.svg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/gh-test-shoppingcart?scl=1&fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_2", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.default.theme.min.css", END_INLINE,
                "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621584287147", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/id/rd?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621584287147", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-kc-bkg02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20190729-hp-od-kcash-offer-bg?fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_3", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_35", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rd_2");
        status = nsApi.ns_web_url ("rd_2",
            "URL=https://dpm.demdex.net/id/rd?d_visid_ver=4.3.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621584287147",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_3", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_4", END_INLINE,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_4", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn10?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn20?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_09", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-dads-rail-bkg1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-golf-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210401-beauty-half", END_INLINE,
                "URL=https://ww8.kohls.com/id?d_visid_ver=4.3.0&d_fieldgroup=A&mcorgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&mid=76403998605952159830891476251030620443&ts=1621584287572", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-bopus-utility", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-bopus-utility", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-2020w1226-rewardsplate", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-social2", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-social2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-kck", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/AppStore-qr_code2020?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_01", END_INLINE
        );

        status = nsApi.ns_end_transaction("rd_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_33");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_33",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_02", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_04", END_INLINE,
                "URL=https://cm.everesttech.net/cm/dd?d_uuid=71429088129042532550393988433173938227", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_hero", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp-dt-e12-20190408-leaf?fmt=png8&scl=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210405-kck-top", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_33", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("json_2");
        status = nsApi.ns_web_url ("json_2",
            "URL=https://kohls.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=951b4c6c934e4b4e8d1242dc5796231d&mboxPC=&mboxPage=4f34bb7b7b4545d4adf20d6c69837a0e&mboxRid=b71d95302c5b4242be527a1204a27e0a&mboxVersion=1.7.1&mboxCount=1&mboxTime=1621604087187&mboxHost=www.kohls.com&mboxURL=https%3A%2F%2Fwww.kohls.com%2F&mboxReferrer=&browserHeight=607&browserWidth=1366&browserTimeOffset=330&screenHeight=768&screenWidth=1366&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=ANGLE%20(Intel(R)%20UHD%20Graphics%20620%20Direct3D11%20vs_5_0%20ps_5_0)&at_property=bb529821-b52b-bf89-2022-4492a94a6d05&customerLoggedStatus=false&tceIsRedesign=false&tceIsPDPRedesign=&tceIsCNCRedesign=False&mboxMCSDID=61AA88EAC7FF0014-1977AA3964EA1951&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=76403998605952159830891476251030620443&mboxAAMB=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&mboxMCGLH=12"
        );

        status = nsApi.ns_end_transaction("json_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_6");
        status = nsApi.ns_web_url ("mobilemenu_json_6",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("pixel_7d34ecfc");
        status = nsApi.ns_web_url ("pixel_7d34ecfc",
            "URL=https://www.kohls.com/akam/11/pixel_7d34ecfc",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("pixel_7d34ecfc", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_34");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_34",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://aa.agkn.com/adscores/g.pixel?sid=9211132908&aam=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://idsync.rlcdn.com/365868.gif?partner_uid=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=21&dpuuid=164570203793000788456", END_INLINE,
                "URL=https://ib.adnxs.com/getuid?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D358%26dpuuid%3D%24UID", "REDIRECT=YES", "LOCATION=https://ib.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fdpm.demdex.net%252Fibs%253Adpid%253D358%2526dpuuid%253D%2524UID", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://ib.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fdpm.demdex.net%252Fibs%253Adpid%253D358%2526dpuuid%253D%2524UID", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=358&dpuuid=4030825338917790404", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=358&dpuuid=4030825338917790404", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_34", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_4");
        status = nsApi.ns_web_url ("config_json_4",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405281&v=1.720.0&if=&sl=0&si=a7154f7b-0cd9-4c63-9a62-c2a459f02e84-qtg6fx&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159",
            INLINE_URLS,
                "URL=https://idsync.rlcdn.com/1000.gif?memo=CKyqFhIxCi0IARCYEhomNzE0MjkwODgxMjkwNDI1MzI1NTAzOTM5ODg0MzMxNzM5MzgyMjcQABoNCKHTnYUGEgUI6AcQAEIASgA", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=477&dpuuid=b201840b42797d932539188f9c3852e988231dce162c53e745951b91633de064b0da87c991749652", END_INLINE
        );

        status = nsApi.ns_end_transaction("config_json_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_35");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_35",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm&gdpr=0&gdpr_consent=&google_hm=NzE0MjkwODgxMjkwNDI1MzI1NTAzOTM5ODg0MzMxNzM5MzgyMjc=", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm=&gdpr=0&gdpr_consent=&google_hm=NzE0MjkwODgxMjkwNDI1MzI1NTAzOTM5ODg0MzMxNzM5MzgyMjc=&google_tc=", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=771&dpuuid=CAESEKFqB1UCWoJ5sBHMqEnkBq0&google_cver=1?gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://navdmp.com/req?adID=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.everesttech.net%2F1x1%3F", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://dp2.33across.com/ps/?pid=897&random=109239567", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_35", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_7");
        status = nsApi.ns_web_url ("session_jsp_7",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537072980%26val%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=601&dpuuid=77336275474980&random=1621584290", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fib.adnxs.com%2Fpxj%3Faction%3Dsetuid(%27__EFGSURFER__.__EFGCK__%27)%26bidder%3D51%26seg%3D2634060der%3D51%26seg%3D2634060", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_static.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fexpires%3D30%26nid%3D2181%26put%3D__EFGSURFER__.__EFGCK__%26v%3D11782", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F&google_gid=CAESEP8Gq91M7_G536evEDxc_FM&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%3D%26piggybackCookie%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEP8Gq91M7_G536evEDxc_FM&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkcG9BQUFBTC0xU0NFLQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060&google_gid=CAESEP8Gq91M7_G536evEDxc_FM&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cdn.navdmp.com/req?adID=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782&google_gid=CAESEP8Gq91M7_G536evEDxc_FM&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEP8Gq91M7_G536evEDxc_FM&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=0&jsession=&ref=&scriptVersion=1.11.2&dyid_server=Dynamic%20Yield&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cdn.dynamicyield.com/scripts/1.11.2/dy-coll-nojq-min.js", END_INLINE,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp?isHome=true", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmb/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://mpp.vindicosuite.com/sync/?pid=27&fr=1", END_INLINE,
                "URL=https://idpix.media6degrees.com/orbserv/hbpix?pixId=16873&pcv=70&ptid=66&tpuv=01&tpu=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=903&dpuuid=4846d55a-5c43-4d54-80d8-5ad9e7d701b6", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCe9bf84b227de49e19116aff6da1d63ec-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC45c202be87f24f4b869276b8ad2213dd-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC7d3594a211b44b26a76f4645d4b39315-source.min.js", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s6323237557576?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A34%3A50%205%20-330&d.&nsid=0&jsonv=1&.d&sdid=61AA88EAC7FF0014-1977AA3964EA1951&mid=76403998605952159830891476251030620443&aamlh=12&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-false&mcidcto=-false&aidcto=-false&.mcid&.c&cc=USD&pageType=homepage&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&tnt=536856%3A1%3A0%2C524571%3A1%3A0%2C545299%3A1%3A0%2C531276%3A0%3A0%2C545701%3A1%3A0%2C536968%3A1%3A0%2C545988%3A0%3A0%2C546041%3A0%3A0%2C526083%3A0%3A0%2C&c4=homepage&c9=homepage%7Chomepage&c10=homepage&c11=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=homepage&c64=VisitorAPI%20Present&v68=homepage&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%3A196815f0-9900-44f0-adaf-570fe8cace1a&v86=68&v87=hp19&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities_3");
        status = nsApi.ns_web_url ("userAffinities_3",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=7656119531360184738",
            INLINE_URLS,
                "URL=https://px.owneriq.net/eucm/p/adpq?redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D53196%26dpuuid%3D(OIQ_UUID)", "REDIRECT=YES", "LOCATION=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748706911756151093&uid=Q6748706911756151093&ref=%2Feucm%2Fp%2Fadpq", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf6d45831b2294615923f29a2042b366f-source.min.js", END_INLINE,
                "URL=https://cms.analytics.yahoo.com/cms?partner_id=ADOBE&_hosted_id=71429088129042532550393988433173938227&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://ps.eyeota.net/match?bid=6j5b2cv&uid=71429088129042532550393988433173938227&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=/match/bounce/?bid=6j5b2cv&uid=71429088129042532550393988433173938227&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;12462;1480;iframe/?spotName=Homepage&U3=76403998605952159830891476251030620443&U7=196815f0-9900-44f0-adaf-570fe8cace1a&U10=N/A&cachebuster=237150.20159178923", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/cav_nv.js?v=REL20170123", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC6e9a66d466e14e7595ac7a2d79c6ca6d-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8f39aead6ce84f38a17325c1cde458bc-source.min.js", END_INLINE,
                "URL=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748706911756151093&uid=Q6748706911756151093&ref=%2Feucm%2Fp%2Fadpq", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748706911756151093", END_INLINE,
                "URL=https://servedby.flashtalking.com/map/?key=a74thHgsfK627J6Ftt8sj5ks52bKe&gdpr=0&gdpr_consent=&url=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=[%FT_GUID%]&gdpr=0&gdpr_consent=", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=48898D5BDA80F0&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=30646?dpuuid=y-eK5aJnpE2pHCGmps6iszRC9XBSwBy5kq_6w-~A", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748706911756151093", END_INLINE,
                "URL=https://ps.eyeota.net/match/bounce/?bid=6j5b2cv&uid=71429088129042532550393988433173938227&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=20blqEmBTd9TpvC7dYUZi1x5AzkbMp5QCsoDwDQWJycQ", END_INLINE,
                "URL=https://fei.pro-market.net/engine?site=141472;size=1x1;mimetype=img;du=67;csync=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=48898D5BDA80F0&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities_4");
        status = nsApi.ns_web_url ("userAffinities_4",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=7656119531360184738",
            INLINE_URLS,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=575&dpuuid=7488938104547395823", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=20blqEmBTd9TpvC7dYUZi1x5AzkbMp5QCsoDwDQWJycQ", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621584290948&mi_u=anon-1621584290947-3383135952&mi_cid=8212&page_title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&timezone_offset=-330&event_type=pageview&cdate=1621584290947&ck=false&anon=true", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=aam=10263239", END_INLINE,
                "URL=https://ads.scorecardresearch.com/p?c1=9&c2=6034944&c3=2&cs_xi=71429088129042532550393988433173938227&rn=1621584288167&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D71429088129042532550393988433173938227", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/dotcom/config.js?v=REL20170123", END_INLINE,
                "URL=https://d.impactradius-event.com/A375953-1cd4-4523-a263-b5b3c8c11fb81.js", END_INLINE,
                "URL=http://localhost:9222/devtools/inspector.html?ws=localhost:9222/devtools/page/8BAF88586AA7C7F52025ED75E215B906", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/root.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/shell.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/xh0;;pixel/?name=Homepage_2019", END_INLINE,
                "URL=https://async-px.dynamicyield.com/id?cnst=1&msn=webserve-0124a8f.use&uid=7656119531360184738&sec=8776374&cuid=196815f0-9900-44f0-adaf-570fe8cace1a&cuidType=atg_id&reqts=1621584290430&rri=2189246&_=1621584290454", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_11");
        status = nsApi.ns_web_url ("batch_11",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621584290624_230049",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=523379&msn=webserve-0124a8f.use&uid=7656119531360184738&sec=8776374&t=ri&e=1100747&p=1&ve=10123479&va=%5B26082590%5D&ses=19cf0b920e454e8be1bd2722b02b15e9&expSes=27551&aud=1408117.1362540.1362542&expVisitId=3489474023055476054&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621584290489&rri=5161693", END_INLINE,
                "URL=http://localhost:9222/devtools/devtools_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=960436&msn=webserve-0124a8f.use&uid=7656119531360184738&sec=8776374&t=ri&e=1096558&p=1&ve=10092862&va=%5B26047526%5D&ses=19cf0b920e454e8be1bd2722b02b15e9&expSes=27551&aud=1408117.1362540.1362542&expVisitId=3489474022567181572&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621584290462&rri=3286839", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=259399&msn=webserve-0124a8f.use&uid=7656119531360184738&sec=8776374&t=ri&e=1102794&p=1&ve=10133783&va=%5B26094881%5D&ses=19cf0b920e454e8be1bd2722b02b15e9&expSes=27551&aud=1408117.1362540.1362542&expVisitId=3489474022774047223&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621584290494&rri=9574842", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_8");
        status = nsApi.ns_web_url ("uia_8",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621584290456",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/inspector.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Runtime.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/platform/utilities.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/dom_extension/DOMExtension.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/common.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/host.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/protocol.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://idsync.rlcdn.com/422866.gif?partner_uid=99999999999999&", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F&H=1lm7uu8", END_INLINE,
                "URL=https://d9.flashtalking.com/d9core", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ui.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/services.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/workspace.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/bindings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_6");
        status = nsApi.ns_web_url ("X349_6",
            "URL=https://kohls.sjv.io/xc/385561/362119/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/components/components.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/persistence.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/browser_sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/extensions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/console_counters.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/EventTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Object.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/UIString.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/App.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/AppProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/CharacterIdMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Color.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://bttrack.com/dmp/adobe/user?dd_uuid=71429088129042532550393988433173938227", "REDIRECT=YES", "LOCATION=//dpm.demdex.net/ibs:dpid=49276&dpuuid=87e63924-ac9c-4adf-a6e1-903171ee4472", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Console.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/JavaScriptMetaData.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ParsedURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Progress.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=http://localhost:9222/devtools/common/QueryParamHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ResourceType.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Revealer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Runnable.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/SegmentedRange.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Settings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_8");
        status = nsApi.ns_web_url ("experiences_8",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/common/StaticContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/StringOutputStream.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/TextDictionary.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Throttler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Trie.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Worker.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHostAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHost.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/ResourceLoader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/UserMetrics.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/Platform.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/InspectorBackend.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/NodeURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/InspectorBackendCommands.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/LiveLocation.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BlackboxManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BreakpointManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/CompilerScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ContentProviderBasedProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/CSSWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DebuggerWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DefaultScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/FileUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/NetworkProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/PresentationConsoleMessageHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/SASSSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/StylesSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/TempFile.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SDKModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMetadata.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/SupportedCSSProperties.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Target.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ProfileTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkRequest.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RuntimeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ChildTargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CompilerSourceMappingContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Connections.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ConsoleModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieParser.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfileDataModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMatchedStyles.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMedia.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSProperty.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSRule.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleDeclaration.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleSheetHeader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMDebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/EmulationModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/FilmStripModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HARLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HeapProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/IsolateManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LayerTreeBase.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LogModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/OverlayModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PaintProfiler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PerformanceMetricsModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RemoteObject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Resource.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ResourceTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ScreenCaptureModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Script.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SecurityOriginManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServerTiming.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerCacheModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMapManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TracingManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TracingModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Widget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/GlassPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Action.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionDelegate.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ARIAUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Context.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextFlavorListener.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Dialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/DropTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/EmptyWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterBar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterSuggestionBuilder.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ForwardedInputEventHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Fragment.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Geometry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/HistoryInput.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Icon.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Infobar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InplaceEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InspectorView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/KeyboardShortcut.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListControl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Panel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/PopoverHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ProgressIndicator.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RemoteDebuggingTerminatedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ReportView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ResizerWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RootView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SearchableView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SegmentedButton.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SettingsUI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutsScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftDropDown.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SplitWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SuggestBox.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SyntaxHighlighter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TabbedPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TargetCrashedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextPrompt.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ThrottledWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Toolbar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Tooltip.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Treeoutline.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/UIUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/View.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ViewManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XElement.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XLink.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ZoomManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/ServiceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/FileManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/UISourceCode.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/WorkspaceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/DockController.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/ImagePreview.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/JSPresentationUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Reload.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/TargetDetachedDialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PlatformFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/Automapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/EditFileSystemView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/FileSystemWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystemManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/NetworkPersistenceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceActions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/WorkspaceSettingsTab.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/LogManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionPanel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionServer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionTraceProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/WarningErrorCounter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/inline_editor/inline_editor_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/color_picker/color_picker_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/formatter/formatter_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/largeIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/navigationControls.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/object_ui/object_ui_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/event_listeners/event_listeners_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/118811_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=http://localhost:9222/devtools/help/help_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4525714_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3955549?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=http://localhost:9222/devtools/elements/elements_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4528551_Empire_Red?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4462500?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3509904_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3500577_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047068_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4777679_Multi_Color_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047073_Rose_Dye?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4887344_Yellow_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3874658?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/touchCursor.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.googletagservices.com/tag/js/gpt.js", END_INLINE,
                "URL=https://www.kohls.com/", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/newHomepagestyle.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_17");
        status = nsApi.ns_web_url ("floop_17",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_17", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_18");
        status = nsApi.ns_web_url ("floop_18",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_18", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_19");
        status = nsApi.ns_web_url ("floop_19",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_19", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_20");
        status = nsApi.ns_web_url ("floop_20",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_2021051801.js?31061259", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_20", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rid_2");
        status = nsApi.ns_web_url ("rid_2",
            "URL=https://match.adsrvr.org/track/rid?ttd_pid=casale&fmt=json&p=184399",
            INLINE_URLS,
                "URL=https://api.rlcdn.com/api/identity?pid=2&rt=envelope", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/checker.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.googleadservices.com/pagead/conversion_async.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("rid_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_7");
        status = nsApi.ns_web_url ("bidRequest_7",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_bottom_728x90&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_8");
        status = nsApi.ns_web_url ("bidRequest_8",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_36");
        status = nsApi.ns_web_url ("index_36",
            "URL=https://hb.emxdgt.com/?t=1000&ts=1621584295585",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_36", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_9");
        status = nsApi.ns_web_url ("bidRequest_9",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_10");
        status = nsApi.ns_web_url ("bidRequest_10",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_middle&secure=1",
            INLINE_URLS,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621584295785&cv=9&fst=1621584295785&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE
        );

        status = nsApi.ns_end_transaction("bidRequest_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_7");
        status = nsApi.ns_web_url ("auction_7",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_bottom_leaderboard_header&lib=ix&size=728x90&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_8");
        status = nsApi.ns_web_url ("auction_8",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621584295758&cv=9&fst=1621584295758&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_3");
        status = nsApi.ns_web_url ("translator_3",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("translator_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("prebid_3");
        status = nsApi.ns_web_url ("prebid_3",
            "URL=https://ib.adnxs.com/ut/v3/prebid",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("prebid_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_5");
        status = nsApi.ns_web_url ("fastlane_json_5",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=2&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1817898&kw=rp.fastlane&tk_flint=index&rand=0.18236336492831695",
            INLINE_URLS,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A3184054%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%228%22%2C%22siteID%22%3A%22269247%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%223%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%227%22%2C%22siteID%22%3A%22269241%22%7D%2C%22id%22%3A%224%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%224846d55a-5c43-4d54-80d8-5ad9e7d701b6%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T08%3A04%3A55%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_6");
        status = nsApi.ns_web_url ("fastlane_json_6",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811822&kw=rp.fastlane&tk_flint=index&rand=0.2323283184459921"
        );

        status = nsApi.ns_end_transaction("fastlane_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_7");
        status = nsApi.ns_web_url ("fastlane_json_7",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811816&kw=rp.fastlane&tk_flint=index&rand=0.6809128973061718"
        );

        status = nsApi.ns_end_transaction("fastlane_json_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_8");
        status = nsApi.ns_web_url ("fastlane_json_8",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811818&kw=rp.fastlane&tk_flint=index&rand=0.3329813012734717",
            INLINE_URLS,
                "URL=https://6249496.collect.igodigital.com/collect.js", END_INLINE,
                "URL=https://idx.liadm.com/idex/ie/any", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_9");
        status = nsApi.ns_web_url ("auction_9",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=258416355", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621584295785&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2660147801&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621584295785&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2660147801&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621584295758&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3779998710&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=49276&dpuuid=87e63924-ac9c-4adf-a6e1-903171ee4472", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621584295758&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635470%2C2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3779998710&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://ads.scorecardresearch.com/p2?c1=9&c2=6034944&c3=2&cs_xi=71429088129042532550393988433173938227&rn=1621584288167&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D71429088129042532550393988433173938227", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=258416355&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_10");
        status = nsApi.ns_web_url ("auction_10",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("headerstats_2");
        status = nsApi.ns_web_url ("headerstats_2",
            "URL=https://as-sec.casalemedia.com/headerstats?s=186355&u=https%3A%2F%2Fwww.kohls.com%2F&v=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_abCSgpkD%22%2C%22site%22%3A%7B%22domain%22%3A%22www.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22IPRVVM6Y%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788162%22%7D%2C%7B%22id%22%3A%22oFs9ugBj%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788161%22%7D%2C%7B%22id%22%3A%22zu5uuw8U%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788160%22%7D%2C%7B%22id%22%3A%22ibtxX2O9%22%2C%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%7D%2C%22tagid%22%3A%22788159%22%7D%5D%7D", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/c=9828/tp=ADBE/tpid=71429088129042532550393988433173938227?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/ct=y/c=9828/tp=ADBE/tpid=71429088129042532550393988433173938227?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=g8f47s39e399f3fe&google_push&google_sc&google_hm=WUtkcG9BQUFBTC0xU0NFLQ==", END_INLINE,
                "URL=https://usermatch.krxd.net/um/v2?partner=adobe&id=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=121998&dpuuid=7773c4d14673990ee06ee3105eba7ee2", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D", END_INLINE,
                "URL=https://49bb6a531a78dd3c620b78b1215a602a.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?id=537148856&val=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://aorta.clickagy.com/pixel.gif?ch=124&cm=71429088129042532550393988433173938227&redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D79908%26dpuuid%3D%7Bvisitor_id%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1", END_INLINE,
                "URL=https://abp.mxptint.net/sn.ashx", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_DD6A65FD_26DB42BBF&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0", END_INLINE,
                "URL=https://ib.adnxs.com/setuid?entity=158&code=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://pixel.rubiconproject.com/tap.php?v=7941&nid=2243&put=YKdpoAAAAL-1SCE-&expires=90", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?cc=1&id=537148856&val=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B342_DD6A65FD_26DB42BBF&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=1010968053479494&correlator=1070772538927044&output=ldjh&impl=fifs&eid=31061259%2C31060508%2C44740387%2C44743003&vrg=2021051801&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1&prev_iu_szs=1x1%2C320x50%7C1024x256%2C970x250%2C728x90%2C300x250%2C300x250%2C300x250%2C320x50%7C1440x630%2C320x50%7C1440x630&fluid=0%2Cheight%2C0%2C0%2C0%2C0%2C0%2Cheight%2Cheight&ists=256&prev_scp=pos%3Dwallpaper%7C%7Cpos%3Dbottom%7Cpos%3Dbottom%7Cpos%3Dbottomleft%7Cpos%3Dbottommiddle%7Cpos%3Dbottomright%7Cpos%3DHP_Desktop_Top_Super_Bulletin%7Cpos%3DHP_Desktop_Bottom_Super_Bulletin&cust_params=pgtype%3Dhome%26channel%3Ddesktop%26env%3Dprod&cookie_enabled=1&bc=31&abxe=1&lmt=1621584296&dt=1621584296703&dlt=1621584286940&idt=8527&frm=20&biw=1349&bih=607&oid=3&adxs=-9%2C-9%2C190%2C311%2C163%2C525%2C887%2C0%2C0&adys=-9%2C-9%2C8193%2C9533%2C9575%2C9575%2C9575%2C2549%2C5490&adks=1341520166%2C1971407845%2C2060426818%2C1304082129%2C4151307844%2C3063740786%2C4276857337%2C3603917613%2C1454221005&ucis=1%7C2%7C3%7C4%7C5%7C6%7C7%7C8%7C9&ifi=1&u_tz=330&u_his=2&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2F&vis=1&dmc=8&scr_x=0&scr_y=0&psz=0x-1%7C0x-1%7C1349x9298%7C1349x9298%7C300x54%7C300x54%7C300x54%7C1349x0%7C1349x9298&msz=0x-1%7C0x-1%7C1349x0%7C1349x0%7C300x0%7C300x0%7C300x0%7C1440x0%7C1440x0&ga_vid=2045363663.1621584297&ga_sid=1621584297&ga_hid=1909693234&ga_fc=false&fws=2%2C2%2C0%2C0%2C0%2C0%2C0%2C128%2C128&ohw=0%2C0%2C0%2C0%2C0%2C0%2C0%2C0%2C0&btvi=-1%7C-1%7C1%7C2%7C3%7C4%7C5%7C6%7C7", END_INLINE,
                "URL=https://sync.ipredictive.com/d/sync/cookie/generic?https://dpm.demdex.net/ibs:dpid=2340&dpuuid=${ADELPHIC_CUID}", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=3c28b7c4-ba0b-11eb-abef-5d701e584744", END_INLINE,
                "URL=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdpoAAAAL-1SCE-", END_INLINE
        );

        status = nsApi.ns_end_transaction("headerstats_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc_4");
        status = nsApi.ns_web_url ("lgc_4",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=3c28b7c4-ba0b-11eb-abef-5d701e584744", END_INLINE,
                "URL=https://image2.pubmatic.com/AdServer/Pug?vcode=bz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA=&piggybackCookie=YKdpoAAAAL-1SCE-", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssInAzZXSLiJ9e_xaHJp2wafB4WOOVwL4XZHU34FjOy6VMfpCxim01EJJ-3cH6sSTQKrKZzb5kDg6swG-KNMpuJ28HqvM5udAVStcZgtCQgUp94MqC8csErNCFLGE3gVoqLusqFm9Agn9oocDo11o15YDPYR9wE-gsj9AQSYhgMXZS9BrpizeNDh7rrs-nDEjtkknMLAYv2ecHezu-md8b4mJi5nltBhyfedbzuf29gyqe3TcTGRVwgDKgY0Go428vD5QxXdSa4zFpBDS4NCGAiGNsUOQFY&sig=Cg0ArKJSzCtHEDW1Sg6_EAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsu10C1uUK6ntlWmh9fQlojI78ZYFIV5TZjl3bqD-2ify1vUCWODvkNHGtHZzJTJT_dghSkVDz2WnNt1qVXiB57rOrVDFIMrZyqlg398bMmMW-iCkXCEkorkeCNCwiwPqjhgGsyC9yhVa_QROUSVLgqSbME_il9hvYiFJ0nSXW63a6cSLiOkB5xu9bY6e2Hfru02zr2KrNpYs6dTbp6VG31jhG_t9-BC-P9sC_R-z2OLE3VcwwHl1I_tf5m0cYb0vg6Ncubo1v0ZsFwx9OIxcmcZVTZTILsV&sig=Cg0ArKJSzGVToLtENc6OEAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/pagead/js/r20210517/r20110914/client/window_focus.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/15280555526910074681", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/16953867992244551473", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstGjDAhdLf9o11M2f9uv7QsaCtITLwfwZKFrLBGlHjTkXsy_R6DK2NbhypbSYly01Dl85ujUB6M6_x452ssn61APKApE5zolhe6WszG73qB7TPh1lRh-jjybXOxBHU0Veom-mkA591ul1tJQqIlr18zlZ_eT4bXRs8aYDxOIB9_2YKMcJH2M0eYlPv9HaTZHJE1601ZGKR8jcdQ-sCZPCP5o-xAu46cgio2Bj4x_LBEJhB7Y4HJyqQwFR5YzrnHh6WL2JDL-ZuAifnLWt0nNO3NIgRyoHIm&sig=Cg0ArKJSzNXi6QsoQO5oEAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/14906849826852343313", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/rx_lidar.js?cache=r20110914", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/osd.js", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=gfp_cw_status&domain=kohls.com&host=www.kohls.com&success=1", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaRRh8rTKXu5Q2VabEfwlHYiCaX26b_ruKMsbybWFv-q8zMc1C8Dh00y-j9FNX53_EJNbxp6rJhNPHlkMIGpf-i4w9iU1Q", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaS2OovFSdcX9J9mQBcv4mR14ZAzdJFoGkOf9tA46FNiWN8V77YQI6ceWgLxa1LtSZ054d561Kg5ZY4DGC19kNFb3iy6dA", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaT4uCW0YFX8gNmzf_h4votGEoOZexwncWmp_S286uRYRfKgqtij9q7EJx7703CHXlzUNTHk8pfK2G7Rra0R6-GBUhIMEw", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdpoAAAAL-1SCE-&img=1", "REDIRECT=YES", "LOCATION=/partner?adv_id=6409&uid=YKdpoAAAAL-1SCE-&img=1&__user_check__=1&sync_id=3c8acb46-ba0b-11eb-8e57-1f8227250407", END_INLINE,
                "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:1f1c1ea80b0e48d388d36c2aac50d6f9&seg=xh0", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdpoAAAAL-1SCE-&img=1&__user_check__=1&sync_id=3c8acb46-ba0b-11eb-8e57-1f8227250407", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvR_cCVQUMbjRzKJOd-R-gX1Jgsj2D9wL515qeqZRFq5MWpsu2w5pWZqQaEcVb2NS_I3A3RulMpdTOykk7YEdfWzKWpGfXkSo8f0kaU6GU6KVzXMQBHMfhoqHZuWPmLJDT3l3Z7c64HZrls_QJCyrAUC0-4ZFdCS5mQk11l1ss-uR1VNk_glOsON3Rw4_f0K2K4bJ7jcI6W57JN8zC51Kh8sg5nEQYwCj8A2RRZ8Y2Ak5nThJZgbOo5AvLhujHYZ4G3yaWKOK9jUolDWglUtSiGMSnuL6S554o&sig=Cg0ArKJSzC8j8xF_FZTHEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssfFpAY51dTUKoOJyQP501ydGNWDys77ket_8MbMbhO2OZOiG6FNnAFsx1U7eY1M2WqPGhJuyKDNhgg3NLv0ik1L99JH1TYFgWlCQ2fVYj4ATEUP1euJ1BbHBR-QBA6JYerteTremIZQNkQ5c39CBnWuI7AizuG1SHxKO5gdi8Fgnj2_KIxIabIxtOOCd8Z54IrTA31j40s46xN1s_OJmEp68tHkigXKNDSdecNaQCFI6MPMoHIs08sl30NTpQqvHqvKB_5O9poL3zUhFASd0r8eK-pg-YsHfY&sig=Cg0ArKJSzDLdJpVaqjXjEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsuGdGu4HChhS8Nd0oN-oBH8AY5UdmiPN1h6pNzho2gq5d2QF-nJQjNmSGFQw88M4qklf80rDM2sm268GcrFUVkelNICfZnqI0oeumyn1TWp31mFT1Dr_FdbvTZo_TXWHEXCRw0l9i9ZpUhoYeyC4-EamR6Ibh_dw4QogQWmRcOL8Z0SpaX8XIdV1RfcKerVCfwbtyRA4Clx5BCzL0-sofkE8Ha9n4DRFXJkvbvjflE-0RCLJBzcOKvGEpV0iBD6oyhZD-z3Lk-BHEQCUQWlj9QmcGlNMtgstFg&sig=Cg0ArKJSzF1QS37ynjfaEAE&adurl=", END_INLINE,
                "URL=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=71429088129042532550393988433173938227", END_INLINE,
                "URL=http://localhost:9222/devtools/cm/cm_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/search/search_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/diff/diff_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/data_grid/data_grid_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://mid.rkdms.com/bct?pid=8bc436aa-e0fc-4baa-9c9a-06fbeca87826&puid=71429088129042532550393988433173938227&_ct=img", END_INLINE,
                "URL=http://localhost:9222/devtools/har_importer/har_importer_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace_diff/workspace_diff_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/cookie_table/cookie_table_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Amobileapp&adcreativeid=138299836983&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=1636489974", END_INLINE,
                "URL=http://localhost:9222/devtools/text_editor/text_editor_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/source_frame/source_frame_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138260694346&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=1663942695", END_INLINE,
                "URL=http://localhost:9222/devtools/perf_ui/perf_ui_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/network/network_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/resourcePlainIconSmall.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/resourceDocumentIconSmall.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/product_registry_impl/product_registry_impl_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=129099&dpuuid=7647dc03f7a3583e70d597923a6fa45d", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/mediumIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/cm_modes/cm_modes_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/popoverArrows.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("lgc_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_36");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_36",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_36", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_37");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_37",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_37", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_38");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_38",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_38", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(33.716);

        status = nsApi.ns_start_transaction("denim_jsp_4");
        status = nsApi.ns_web_url ("denim_jsp_4",
            "URL=https://www.kohls.com/search.jsp?submit-search=web-regular&search=jeans&spa=3&kls_sbp=76403998605952159830891476251030620443"
        );

        status = nsApi.ns_end_transaction("denim_jsp_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_39");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_39",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_39", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("manifest_json");
        status = nsApi.ns_web_url ("manifest_json",
            "URL=https://www.kohls.com/manifest.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/sale-event/denim.jsp?&searchTerm=jeans&submit-search=web-regular", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjstwxMLALwQZUvntEli3bkDKPSUagS-rmUuM6GVb68cE6PwYn_Z7UjsXZAjODK1Z6zOLsCMN8r-mVYkimUvghjXWCUs8XuYKtafJdrp5bMQ&sig=Cg0ArKJSzOA1EQvd5ep1EAE&id=lidartos&mcvt=0&p=9665,525,9915,825&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=3063740786&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621584297887&dlt=0&rpt=575&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjss03fQS8NIoYZrMhyCqh_3hkWcsr3UHJ_-PTV9CuiD4fP1TdZO5fDyAcw0d9ZEDXtBMIYtY9lm7_CpZT56vI3dM2fNMLU65fuKVZZWVDc8&sig=Cg0ArKJSzGpjRoln1uuAEAE&id=lidartos&mcvt=0&p=9665,163,9915,463&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=4151307844&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621584297882&dlt=0&rpt=524&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsujDllhmf4cr0df4WDYSBYlQmB9RoUw6bZeaH_XnnoAD_9iAxg4n-_8vH7-DhptC5MM8Zz-uxjvnUisDydidg6mX93Gjrhi7ANgCuxn4bw&sig=Cg0ArKJSzKmeBW_8UJckEAE&id=lidartos&mcvt=0&p=9501,311,9591,1039&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=1304082129&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621584297753&dlt=0&rpt=721&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/css/style.css", END_INLINE,
                "URL=https://widget.stylitics.com/v2/widget.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environment.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/ktag.js", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/js/main.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kjscore3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/fonts/hfjFonts.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/skava-custom.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage1.css", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.base.min.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("json_3");
        status = nsApi.ns_web_url ("json_3",
            "URL=https://mboxedge31.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=951b4c6c934e4b4e8d1242dc5796231d&mboxPC=951b4c6c934e4b4e8d1242dc5796231d.31_0&mboxPage=ebb9724a171c4605ba5c3c03f6e74212&mboxRid=7adab2af39804f32a090adf0aaad8c22&mboxVersion=1.7.1&mboxCount=1&mboxTime=1621604123734&mboxHost=www.kohls.com&mboxURL=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&mboxReferrer=https%3A%2F%2Fwww.kohls.com%2F&browserHeight=607&browserWidth=1366&browserTimeOffset=330&screenHeight=768&screenWidth=1366&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=ANGLE%20(Intel(R)%20UHD%20Graphics%20620%20Direct3D11%20vs_5_0%20ps_5_0)&at_property=bb529821-b52b-bf89-2022-4492a94a6d05&customerLoggedStatus=false&tceIsRedesign=false&tceIsPDPRedesign=&tceIsCNCRedesign=False&mboxMCSDID=00A6278DEFF0BC64-5FF5CB4EAC831D0A&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=76403998605952159830891476251030620443&mboxAAMB=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&mboxMCGLH=12",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-main?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-07?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-08?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-09?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-m?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-mens?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-girls?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-boys?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-womens?scl=1&fmt=png8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/ipadcss.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-d?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE
        );

        status = nsApi.ns_end_transaction("json_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_21");
        status = nsApi.ns_web_url ("floop_21",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_21", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_7");
        status = nsApi.ns_web_url ("mobilemenu_json_7",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_5");
        status = nsApi.ns_web_url ("config_json_5",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405281&v=1.720.0&if=&sl=1&si=bfdc9235-a445-41ad-ae8b-b91b34b48d10-qtg6fx&bcn=%2F%2F684fc53d.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159"
        );

        status = nsApi.ns_end_transaction("config_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_40");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_40",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_40", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_41");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_41",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_41", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_8");
        status = nsApi.ns_web_url ("session_jsp_8",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=7656119531360184738&jsession=8e1ca56c544bf696269b143792eba19f&ref=https%3A%2F%2Fwww.kohls.com%2F&scriptVersion=1.11.2&dyid_server=7656119531360184738", END_INLINE,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s66752640530175?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A27%205%20-330&d.&nsid=0&jsonv=1&.d&sdid=00A6278DEFF0BC64-5FF5CB4EAC831D0A&mid=76403998605952159830891476251030620443&aamlh=12&ce=UTF-8&ns=kohls&pageName=denim&g=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=sale%20event%20landing&events=event1&products=%3Bproductmerch1&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&tnt=536856%3A1%3A0%2C545299%3A1%3A0%2C531276%3A0%3A0%2C536968%3A1%3A0%2C546041%3A0%3A0%2C526083%3A0%3A0%2C&c1=no%20taxonomy&c2=no%20taxonomy&c3=no%20taxonomy&v3=browse&c4=sale%20event%20landing&c5=non-search&c7=no%20taxonomy&v8=non-search&v9=homepage&c16=browse&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=browse&v24=browse&v25=no%20taxonomy&v26=no%20taxonomy&v27=no%20taxonomy&v28=no%20taxonomy&c39=browse&c40=browse&v40=cloud17&c41=browse&c42=browse&v42=no%20cart&c50=D%3Ds_tempsess&c53=denim&c64=VisitorAPI%20Present&v68=denim&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%3A196815f0-9900-44f0-adaf-570fe8cace1a&c.&a.&activitymap.&page=homepage&link=web-regular&region=site-search&pageIDType=1&.activitymap&.a&.c&pid=homepage&pidt=1&oid=web-regular&oidt=3&ot=SUBMIT&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_12");
        status = nsApi.ns_web_url ("batch_12",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621584327341_894143",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_9");
        status = nsApi.ns_web_url ("uia_9",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621584326926",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&docReferrer=https%3A%2F%2Fwww.kohls.com%2F&H=cajn1lv&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC49eca514a17d4f4eb2269e814b3eb342-source.min.js", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_7");
        status = nsApi.ns_web_url ("X349_7",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621584328218&cv=9&fst=1621584328218&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621584328256&cv=9&fst=1621584328256&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://www.kohls.com/catalog/jeans-bottoms-clothing.jsp?CN=Product:Jeans+Category:Bottoms+Department:Clothing&icid=denimlp-shopall", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=2&dtm_email_hash=N/A&dtm_user_id=196815f0-9900-44f0-adaf-570fe8cace1a&dtmc_department=clothing&dtmc_category=&dtmc_sub_category=not%20set", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_7", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(44.28);

        status = nsApi.ns_start_transaction("jeans_bottoms_clothing_jsp_4");
        status = nsApi.ns_web_url ("jeans_bottoms_clothing_jsp_4",
            "URL=https://www.kohls.com/catalog/jeans-bottoms-clothing.jsp?CN=Product:Jeans+Category:Bottoms+Department:Clothing&icid=denimlp-shopall&kls_sbp=76403998605952159830891476251030620443"
        );

        status = nsApi.ns_end_transaction("jeans_bottoms_clothing_jsp_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_42");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_42",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_42", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_43");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_43",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
            "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=https://login.dotomi.com/pixel.gif", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621584328256&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=194431027&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621584328218&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1205785857&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621584328256&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=194431027&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621584328218&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1205785857&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_43", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_44");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_44",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621584328822&mi_u=anon-1621584290947-3383135952&mi_cid=8212&referrer=https%3A%2F%2Fwww.kohls.com%2F&timezone_offset=-330&event_type=pageview&cdate=1621584328819&ck=host&anon=true", END_INLINE,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?guid=ON&;script=0&data=aam=10263239", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=51313360", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?guid=ON&data=aam=10263239&is_vtc=1&random=51313360&ipr=y", END_INLINE,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A56980896%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A160%2C%22h%22%3A600%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%225%22%2C%22siteID%22%3A%22269244%22%7D%2C%22id%22%3A%223%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%224846d55a-5c43-4d54-80d8-5ad9e7d701b6%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T08%3A04%3A55%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_44", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_37");
        status = nsApi.ns_web_url ("index_37",
            "URL=https://hb.emxdgt.com/?t=1560&ts=1621584329603",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_37", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_11");
        status = nsApi.ns_web_url ("bidRequest_11",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("prebid_4");
        status = nsApi.ns_web_url ("prebid_4",
            "URL=https://ib.adnxs.com/ut/v3/prebid",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("prebid_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_12");
        status = nsApi.ns_web_url ("bidRequest_12",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_left_160x600&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_13");
        status = nsApi.ns_web_url ("bidRequest_13",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_4");
        status = nsApi.ns_web_url ("translator_4",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/kjscoretag3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/pmp_search_loader.gif", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2560266_Secluded_Echo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3647219_Medium_Indigo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Stonewash?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3398441_Twister?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3215192_Heritage_Medium_Wash?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pmp_imgs/plus-circle.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/blank_1x1.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/BestSeller@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pmp_imgs/swatch-blank.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/BuyOneGetOneHalfOff@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/loading.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/ic-left-chevron.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/product-rating-stars-sprite.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/ic-right-chevron.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/118561_Sleek_Blue?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Majestic?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/TopRated@2x.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/up.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/plus.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/new_product_badges/KohlsExclusive@2x.png", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/backToTop.png", END_INLINE
        );

        status = nsApi.ns_end_transaction("translator_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_6");
        status = nsApi.ns_web_url ("config_json_6",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405281&v=1.720.0&if=&sl=1&si=bfdc9235-a445-41ad-ae8b-b91b34b48d10-qtg6fx&bcn=%2F%2F684fc53d.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Shoestring?wid=500&hei=500&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/omniture_tracking.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/s_code.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/controller/ctl.pmp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/productlistRevamp.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("config_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_45");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_45",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/mobile/mobilemenu.tpl.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_45", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_8");
        status = nsApi.ns_web_url ("mobilemenu_json_8",
            "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/pmpSearchPageScriptsV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_46");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_46",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pmpSearchPageTmplV1.js", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/javascript/deploy/pb.module.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_46", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_3");
        status = nsApi.ns_web_url ("id_3",
            "URL=https://dpm.demdex.net/id?d_visid_ver=5.2.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=76403998605952159830891476251030620443&d_blob=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&ts=1621584332835"
        );

        status = nsApi.ns_end_transaction("id_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_7");
        status = nsApi.ns_web_url ("delivery_7",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=951b4c6c934e4b4e8d1242dc5796231d&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/favorites/favorites.products.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20210503-dm-bogo-half-badge?scl=1&amp;fmt=png8", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=7656119531360184738&jsession=8e1ca56c544bf696269b143792eba19f&ref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&scriptVersion=1.11.2&dyid_server=7656119531360184738&ctx=%7B%22type%22%3A%22CATEGORY%22%2C%22data%22%3A%5B%22Clothing%22%2C%22N%2FA%22%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_9");
        status = nsApi.ns_web_url ("session_jsp_9",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("storesAvailabilitySearch_4");
        status = nsApi.ns_web_url ("storesAvailabilitySearch_4",
            "URL=https://www.kohls.com/snb/storesAvailabilitySearch?zipCode=&_=1621584332005"
        );

        status = nsApi.ns_end_transaction("storesAvailabilitySearch_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_10");
        status = nsApi.ns_web_url ("uia_10",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621584333868",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/menu/tpl.accountdropdown.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_13");
        status = nsApi.ns_web_url ("batch_13",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621584333959_191694",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s68924907850344?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A34%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cjeans&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&r=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event40%2Cevent38%2Cevent155&products=%3Bproductmerch2&c1=clothing&c2=clothing%3Ebottoms&v2=denimlp-shopall&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign%20p13n_control&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=jeans%7Cbottoms%7Cclothing%7Cpmp&v9=denim&c16=internal%20campaign%20p13n_control&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cjeans&c28=denimlp-shopall&v28=bottoms%7Cclothing%7Cjeans&v29=featured%3A1%3A48&v35=denimlp-shopall&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cjeans%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cjeans&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&c75=w%3Ebg%7Cbest_seller&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7C%7C%7C3&c.&a.&activitymap.&page=denim&link=SHOP%20ALL&region=lp-denim&pageIDType=1&.activitymap&.a&.c&pid=denim&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartmen&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s68924907850344?AQB=1&pccr=true&vidn=3053B4E704A78657-60001E7130290817&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A34%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cjeans&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&r=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event40%2Cevent38%2Cevent155&products=%3Bproductmerch2&c1=clothing&c2=clothing%3Ebottoms&v2=denimlp-shopall&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign%20p13n_control&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=jeans%7Cbottoms%7Cclothing%7Cpmp&v9=denim&c16=internal%20campaign%20p13n_control&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cjeans&c28=denimlp-shopall&v28=bottoms%7Cclothing%7Cjeans&v29=featured%3A1%3A48&v35=denimlp-shopall&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cjeans%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cjeans&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&c75=w%3Ebg%7Cbest_seller&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7C%7C%7C3&c.&a.&activitymap.&page=denim&link=SHOP%20ALL&region=lp-denim&pageIDType=1&.activitymap&.a&.c&pid=denim&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartmen&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/Common/NewBanner/", END_INLINE,
                "URL=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp?CN=Gender:Womens+Product:Jeans+Category:Bottoms+Department:Clothing&icid=jeans-VN-women", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_47");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_47",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_47", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_48");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_48",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_48", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_49");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_49",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/check-empty.png", END_INLINE,
                "URL=https://www.google.com/adsense/search/async-ads.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20191120-kcash-logo-right-img?scl=1&fmt=png8-alpha", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Stonewash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Light_Blue?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Majestic_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_49", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_22");
        status = nsApi.ns_web_url ("floop_22",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Rustic_Fern_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Latana_Mauve_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Destructed_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Authentic_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Clean_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Cascade_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Open_Sea_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Shy_Gray_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Destructed_Light_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Medium_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Clif_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Indigo_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Chicago_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Hazelnut_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Madison_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Frisco_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Coffee_Roast_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_The_Twist_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Vintage_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Iris_Moon_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Timberwolf_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Tumbled_Rigid_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Golden_Top_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/SpotLight/ff021745-7d11-4189-92a1-9afc2eaaa8ef/", END_INLINE,
                "URL=https://www.google.com/afs/ads?adtest=off&channel=PMP_TwoLeftRail&cpp=0&hl=en&client=kohls-search&q=Product%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=2781621584335796&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=4&u_tz=330&dt=1621584335801&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi760sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&referer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular#master-1", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_22", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(51.318);

        status = nsApi.ns_start_transaction("womens_jeans_bottoms_clothin_4");
        status = nsApi.ns_web_url ("womens_jeans_bottoms_clothin_4",
            "URL=https://www.google.com/afs/ads?adsafe=high&adtest=off&cpp=0&client=vert-pla-kohls-srp&q=Product%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=1&pfcrncy=USD&pfmax=360&pfmin=73&theme=walleye&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&nocache=601621584335818&num=0&output=uds_ads_only&v=3&preload=true&bsl=10&u_his=4&u_tz=330&dt=1621584335823&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=-wi760he265&cont=afshcontainer&inames=slave-0-2&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&referer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular#master-2"
        );

        status = nsApi.ns_end_transaction("womens_jeans_bottoms_clothin_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_9");
        status = nsApi.ns_web_url ("experiences_9",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/check-empty.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20191120-kcash-logo-right-img?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Stonewash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Light_Blue?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Rustic_Fern_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Destructed_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Authentic_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Clean_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Open_Sea_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Shy_Gray_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Destructed_Light_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Medium_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3914103_Naomi?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4266674_Atlantic_Ocean_Blue?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3592970_Dark_Indigo?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3944204_White?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3416108_Carbon_Bay?wid=125&hei=125&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Clif_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Chicago_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Hazelnut_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Madison_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Frisco_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Coffee_Roast_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5172_The_Twist_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Vintage_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4530067_Iris_Moon_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Timberwolf_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Dark_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Tumbled_Rigid_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Golden_Top_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Stonewash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/5178_Bleach_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1711501", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1840211_ALT51", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1757201", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1720904_ALT2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3776576_Dark_Wash?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1474928_Bayside", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1456050_True_Blue", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1240385_Modern_Reflection", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-830494_ALT5", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1695762_Rosedale", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-854568_Rinse", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-495025_Lights_On", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1768237", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1740538", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613333_Dark_Wash", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1757535", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/mobile/mobilemenu.tpl.js", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_9");
        status = nsApi.ns_web_url ("mobilemenu_json_9",
            "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1800593", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613341_Black", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/pmpSearchPageScriptsV1.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pmpSearchPageTmplV1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_50");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_50",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1800593", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613341_Black", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_50", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_8");
        status = nsApi.ns_web_url ("delivery_8",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=951b4c6c934e4b4e8d1242dc5796231d&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1811911", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716153", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1716124", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/favorites/favorites.products.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1755110", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1734847_Barham", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://media2.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1735827", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1730526", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1817071", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1800593", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/JeansProductGuide-20141215-1613341_Black", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_51");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_51",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=7656119531360184738&jsession=8e1ca56c544bf696269b143792eba19f&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&scriptVersion=1.11.2&dyid_server=7656119531360184738&ctx=%7B%22type%22%3A%22CATEGORY%22%2C%22data%22%3A%5B%22Clothing%22%2C%22Womens%22%5D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_51", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_10");
        status = nsApi.ns_web_url ("session_jsp_10",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/cnc/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/menu/tpl.accountdropdown.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("storesAvailabilitySearch_5");
        status = nsApi.ns_web_url ("storesAvailabilitySearch_5",
            "URL=https://www.kohls.com/snb/storesAvailabilitySearch?zipCode=&_=1621584337218",
            INLINE_URLS,
                "URL=https://www.kohls.com/feature/ESI/Common/NewBanner/", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s68014649616145?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A38%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=bottoms%7Cclothing%7Cjeans%7Cwomens&g=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=product%20matrix&events=event40%2Cevent81%2Cevent38%2Cevent155&products=%3Bproductmerch3&c1=clothing&c2=clothing%3Ebottoms&v2=jeans-vn-women&c3=clothing%3Ebottoms%3Ejeans&v3=internal%20campaign%20p13n&c4=pmp&c5=non-search&c7=clothing%3Ebottoms%3Ejeans&v8=non-search&c9=womens%7Cjeans%7Cbottoms%7Cclothing%7Cpmp&v9=bottoms%7Cclothing%7Cjeans&c16=internal%20campaign%20p13n&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=no%20refinement&v24=no%20refinement&v27=bottoms%7Cclothing%7Cjeans%7Cwomens&c28=jeans-vn-women&v28=bottoms%7Cclothing%7Cjeans%7Cwomens&v29=featured%3A1%3A48&v35=jeans-vn-women&v39=no%20customer%20id&c40=bottoms%7Cclothing%7Cjeans%7Cwomens%3Eno%20refinement&v40=cloud17&c42=bottoms%7Cclothing%7Cjeans%7Cwomens&v42=no%20cart&c50=D%3Ds_tempsess&c53=D%3Dv28&c64=VisitorAPI%20Present&v68=D%3Dv28&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&c75=w%3Ebg%7Cbest_seller&v86=21may.w2&v87=pmp20&v96=browse%7C%7C%7Creranked_rt%7C%7C3&c.&a.&activitymap.&page=bottoms%7Cclothing%7Cjeans&link=Women&region=vn-box&pageIDType=1&.activitymap&.a&.c&pid=bottoms%7Cclothing%7Cjeans&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCateg&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("storesAvailabilitySearch_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_11");
        status = nsApi.ns_web_url ("uia_11",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621584338608",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_14");
        status = nsApi.ns_web_url ("batch_14",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621584338850_30552",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://www.kohls.com/feature/ESI/SpotLight/4c6fd148-cac9-4c0b-8461-61c2e90993fa/", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4967470?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Modern_White?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Bewitched?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3114244_Black?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Modern_White?wid=240&hei=240&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Super_Light_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Potassium_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Potassium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Gray_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Light_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Medium_Blasted_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Dark_Blast_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Clean_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Rinse_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Bewitched_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Inspire_Blue_sw?wid=20&hei=20", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_14", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_23");
        status = nsApi.ns_web_url ("floop_23",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Authentic_Nordic_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Light_Wash_Destruct_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Meridian_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Modern_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Verona_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.google.com/afs/ads?adtest=off&channel=PMP_TwoLeftRail&cpp=0&hl=en&client=kohls-search&q=Gender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=5861621584340003&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=5&u_tz=330&dt=1621584340019&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi760sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443#master-1", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_23", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_10");
        status = nsApi.ns_web_url ("experiences_10",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PMP&plids=Horizontal1%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.google.com/afs/ads?adsafe=high&adtest=off&cpp=0&client=vert-pla-kohls-srp&q=Gender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing&r=m&type=1&pfcrncy=USD&pfmax=347.5&pfmin=70.5&theme=walleye&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&nocache=6481621584340044&num=0&output=uds_ads_only&v=3&preload=true&bsl=10&u_his=5&u_tz=330&dt=1621584340062&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=-wi760he265&cont=afshcontainer&inames=slave-0-2&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443#master-2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Black_Onyx_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Niagara_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Light_Tint_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Washed_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Expedition_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Inspire_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Rinse_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3315119_Modern_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.kohls.com/media/css/fonts/hfjFonts.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-01?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-02?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-03?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-04?scl=1&fmt=pjpeg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/sl-womens-denim-hs-20210404-fits-05?scl=1&fmt=pjpeg", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1695762_Rinse_Noir?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_True_Blue?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Medium_Dark_Destructed?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2954787_Rinse?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCe7a5c80123db4f88ba3a45b88d306fba-source.min.js", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1731442_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1119487_Rinse_Noir?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fjeans-bottoms-clothing.jsp%3FCN%3DProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Ddenimlp-shopall%26kls_sbp%3D76403998605952159830891476251030620443&H=-1kncj81&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_24");
        status = nsApi.ns_web_url ("floop_24",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Charcoal_Grey_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Anchor_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Lagoon_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Light_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Serene_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Bewitched_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Dark_Wash_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp?CN=Gender:Womens+Product:Jeans+Category:Bottoms+Department:Clothing&icid=jeans-VN-women", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_True_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Darkest_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Dark_Med_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Med_Light_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Dark_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_True_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Dark_Denim_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Black_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp?color=Vista&prdPV=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_24", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_52");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_52",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_52", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(57.404);

        status = nsApi.ns_start_transaction("womens_lee_legendary_straigh");
        status = nsApi.ns_web_url ("womens_lee_legendary_straigh",
            "URL=https://servedby.flashtalking.com/container/1638;101965;1478;iframe/?spotName=Category_Page&U1=3833748,2263298,2953086,4967470,3315119,3114244,2933383,3741815,1875948,2305734,3416624,3100229,3966197,4463543,3406702,3028316,4463650,4438906,4272437,3741638,3958279,4256003,3416634,4258008,4272438,3673262,4471204,2560270,4885110,4842207,4473136,4885138,1695762,2953334,5001943,3854539,3114256,3344199,3028316,3413782,4922997,3343029,4868477,3413782,2216712,3281586,4843497,2423467,3045155,4530067,2954787,1731442,4462012,4646721,4462288,2982007,5046671,4944580,3944204,4946895&U2=1234&U3=76403998605952159830891476251030620443&U7=196815f0-9900-44f0-adaf-570fe8cace1a&U10=N/A&cachebuster=664065.5768426264",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8d8f252b2afa4196b5e95fbba1500e81-source.min.js", END_INLINE,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("womens_lee_legendary_straigh", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_8");
        status = nsApi.ns_web_url ("X349_8",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X349_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X_3");
        status = nsApi.ns_web_url ("X_3",
            "URL=https://bat.bing.com/actionp/0?ti=4024145&tm=al001&Ver=2&mid=83d6fd0e-fcf3-49fb-a448-f433ae7b27a5&sid=566077a0ba0b11ebae8161853fa4c31d&vid=56611880ba0b11ebbbad61c4d19e4215&vids=0&evt=pageHide",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Charcoal_Grey_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Anchor_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Lagoon_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Light_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Serene_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Bewitched_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Dark_Wash_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_True_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Darkest_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Dark_Med_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Med_Light_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Dark_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_True_Blue_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Dark_Denim_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2933383_Black_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Black_Destructed_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_White_sw?wid=20&hei=20", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3741815_Medium_Wash_sw?wid=20&hei=20", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.css", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/media/digital/ecom/size-charts/css_2020/css/size-guides.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/error-icon.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/9-dots.png", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/warning-glyph-black.svg", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/loading.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/pdp/rectangle.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_ALT5?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_ALT6?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_ALT4?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/left-arrow.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/right.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/add-to-list.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/store.svg", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/backToNext.png", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_ALT3?wid=96&hei=96&op_sharpen=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/app/pdp/pdp.app.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/productDetailsPage.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/tpl/tpl.pdpPageTmplV1.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/css/system.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("X_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_53");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_53",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_53", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_9");
        status = nsApi.ns_web_url ("delivery_9",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=951b4c6c934e4b4e8d1242dc5796231d&version=2.5.0",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("delivery_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_54");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_54",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_54", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_11");
        status = nsApi.ns_web_url ("session_jsp_11",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=7656119531360184738&jsession=8e1ca56c544bf696269b143792eba19f&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&scriptVersion=1.11.2&dyid_server=7656119531360184738&ctx=%7B%22type%22%3A%22PRODUCT%22%2C%22data%22%3A%5B%223833748%22%5D%7D", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/templates/clientSidePdpScripts.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("index_38");
        status = nsApi.ns_web_url ("index_38",
            "URL=https://www.kohls.com/web/deliveryInfoStandard/",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Stripe_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Black_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Expedition_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Inspire_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Rinse_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_White_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Chestnut_Cord_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Washed_Black_sw?wid=30&hei=30&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Hazelnut_Cord_sw?wid=30&hei=30&op_sharpen=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_38", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_6");
        status = nsApi.ns_web_url ("outfits_6",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=6&with_item_coords=true&item_number=3833748_Vista&return_object=true"
        );

        status = nsApi.ns_end_transaction("outfits_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_15");
        status = nsApi.ns_web_url ("batch_15",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621584344996_321890",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("batch_15", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_12");
        status = nsApi.ns_web_url ("uia_12",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621584344875",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista?wid=600&hei=600&op_sharpen=1", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s62730887805241?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A45%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=pdp&events=prodView%2Cevent3%2Cevent84&products=%3B3833748%3B%3B%3B%3Bevar11%3Dnot%20collection%7Cevar16%3Dn%7Cevar74%3D50.00_30.00&c4=pdp&c9=product%20detail%20page&v9=bottoms%7Cclothing%7Cjeans%7Cwomens&c10=product%20detail%20page&c11=product%20detail%20page&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&v59=product%20page&v63=1&c64=VisitorAPI%20Present&v68=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&v75=pdp20-standard&v86=21may.w2&v87=pdp20&c.&a.&activitymap.&page=bottoms%7Cclothing%7Cjeans%7Cwomens&link=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&region=root_panel7422537&pageIDType=1&.activitymap&.a&.c&pid=bottoms%7Cclothing%7Cjeans%7Cwomens&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26pr&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://scontent.webcollage.net/api/v2/product-content", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC9fa6133381864e24a8e57c6d93d58d24-source.min.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/pdn/wishlist.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC55d353ba1b894443bc7ecec777f338d5-source.min.js", END_INLINE,
                "URL=https://www.google.com/afs/ads?adpage=1&adtest=off&channel=null&cpp=0&hl=en&client=kohls-pdp&q=Show%20off%20your%20sense%20of%20style%20with%20these%20women%27s%20Lee%20Legendary%20jeans&r=m&type=0&oe=UTF-8&ie=UTF-8&fexp=21404%2C17300494%2C17300495&format=n5&ad=n5&nocache=2661621584346462&num=0&output=uds_ads_only&v=3&preload=true&adext=as1%2Csr1&bsl=10&u_his=6&u_tz=330&dt=1621584346465&u_w=1366&u_h=768&biw=1349&bih=607&psw=1349&psh=607&frm=0&uio=va1sr1sl1-wi800ff2sd12sv12st12&cont=adcontainer1&csize=%7C%7C&inames=slave-0-1%7Cmaster-a-1%7Cmaster-b-1&jsv=16128&rurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&referer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women#master-1", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/bv.js", END_INLINE,
                "URL=https://syndi.webcollage.net/site/kohls/tag.js?cv=18768", END_INLINE,
                "URL=https://edge.curalate.com/sites/kohls-dismfc/site/latest/site.min.js", END_INLINE,
                "URL=https://koh-cdns.truefitcorp.com/fitrec/koh/js/fitrec.js?autoCalculate=false", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/7d2886cea23339882f2cd6bf7fad6877c86425a34321ef", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;11970;1478;iframe/?spotName=Product_Pages&U1=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp?color=Inspire&U2=bottoms&U3=76403998605952159830891476251030620443&U7=196815f0-9900-44f0-adaf-570fe8cace1a&U9=18899638&U10=N/A&cachebuster=931331.9367930513", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/7d2f8fcca3393f9e44fe2a4d97e5b1afacf5ff5d510d82", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/7d2b85cda1393de13b3c56301f6eba96bfaa8b05887262", END_INLINE,
                "URL=https://assets.pinterest.com/js/pinit.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("uia_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("splitTests_json_2");
        status = nsApi.ns_web_url ("splitTests_json_2",
            "URL=https://apps.bazaarvoice.com/splitTests.json",
            INLINE_URLS,
                "URL=https://apps.bazaarvoice.com/apps/api/api-0.7.3.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/review_highlights/review_highlights-3.2.6.js", END_INLINE,
                "URL=https://content.syndigo.com/site/8f845786-bb87-4065-810b-e52a8a2220e3/syndi.min.mjs?cv=450440", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/rating_summary/rating_summary-2.44.0.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/questions/questions-0.2.2.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/reviews/reviews-0.2.2.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/questions-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/curations-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/api-config.js", END_INLINE,
                "URL=https://analytics-static.ugc.bazaarvoice.com/prod/static/3/bv-analytics.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/rating_summary-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/curations/curations-1.2.0.js", END_INLINE,
                "URL=https://edge.curalate.com/sites/kohls-dismfc/experiences/custom-lb-carousel-1593692829557/latest/experience.min.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/reviews-config.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/deployments/kohls/redesign/production/en_US/review_highlights-config.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/v1/config/wishlistconfig.js", END_INLINE,
                "URL=https://cdnassets-kohls.skavaone.com/static/ss/sstimer.js", END_INLINE,
                "URL=https://assets.pinterest.com/js/pinit_main.js?0.9689208134620815", END_INLINE,
                "URL=https://d9.flashtalking.com/d9core", END_INLINE,
                "URL=https://consumer.truefitcorp.com/fitconfig?callback=tfc.processConfiguration&storeId=koh&clientHandlesBrowserUnsupported=true", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC0c9683a00d4047d1b112bd5cd4e7c71a-source.min.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/skava-pdp-custom.js", END_INLINE,
                "URL=https://apps.bazaarvoice.com/apps/review_highlights/review_highlights/assets/d0f062c8b795f68ce73e.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/javascript/skava-pdp.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/css/skava-custom.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("splitTests_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc_5");
        status = nsApi.ns_web_url ("lgc_5",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://servedby.flashtalking.com/spot/6/1638;11970;1478/?spotName=Product_Pages&U1=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp?color=Inspire&U2=bottoms&U3=76403998605952159830891476251030620443&U7=196815f0-9900-44f0-adaf-570fe8cace1a&U9=18899638&U10=N/A&cachebuster=931331.9367930513&ft_trackID=16215843-4792-462E-2E83-45A82A9A588B", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&H=-2d3p6wz&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE
        );

        status = nsApi.ns_end_transaction("lgc_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("summary_2");
        status = nsApi.ns_web_url ("summary_2",
            "URL=https://api.bazaarvoice.com/data/display/0.2alpha/product/summary?PassKey=9zz78jlr8mloisoz9800sqwo5&productid=3833748&contentType=reviews,questions&reviewDistribution=primaryRating,recommended&rev=0&contentlocale=en_GB,en_US",
            INLINE_URLS,
                "URL=https://servedby.flashtalking.com/track/11970;1478;403;16215843-4792-462E-2E83-45A82A9A588B/?ft_data=d9:1f1c1ea80b0e48d388d36c2aac50d6f9;d9s:1f1c1ea80b0e48d388d36c2aac50d6f9&cachebuster=605148.8932884694", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/en_US/bvapi.js", END_INLINE,
                "URL=https://content.syndigo.com/site/common/1.0.187/translations/en.min.mjs?cv=450440&hn=www.kohls.com", END_INLINE,
                "URL=https://fm.flashtalking.com/segment/591/view/3833748/?guid=1f1c1ea80b0e48d388d36c2aac50d6f9", END_INLINE,
                "URL=https://fm.flashtalking.com/segment/591/view/3833748", END_INLINE
        );

        status = nsApi.ns_end_transaction("summary_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("wxyfzSQkpEztWWCh_2");
        status = nsApi.ns_web_url ("wxyfzSQkpEztWWCh_2",
            "URL=https://edge.curalate.com/v1/media/wxyfzSQkpEztWWCh?appId=curalate&limit=25&productMetadata=availability,custom_label_3,custom_label_4&sort=Latest&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&filter=((productId%3A%273833748%27))",
            INLINE_URLS,
                "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:1f1c1ea80b0e48d388d36c2aac50d6f9&seg=u6h", END_INLINE,
                "URL=https://cdn.truefitcorp.com/store-koh/6.56.1.19/resources/store/koh/js/tfcapp.js", END_INLINE,
                "URL=https://cdn.truefitcorp.com/store-koh/6.56.1.19/resources/store/koh/css/fitrec.css", END_INLINE,
                "URL=https://cdn.truefitcorp.com/consumer-ux/6.56.1.22/resources/fitrec/js/application.js", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf2b2cd806a554e4da7df299788affb8a-source.min.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621584348662&mi_u=anon-1621584290947-3383135952&mi_cid=8212&page_title=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&timezone_offset=-330&event_type=pageview&cdate=1621584348653&ck=host&anon=true&type=product&title=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&price=30.00&id=3833748&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp&categories=id%3Awomens%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens.jsp%253FCN%253DGender%253AWomens%2Ctitle%3AWomens%7Cid%3Awomens-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-clothing.jsp%253FCN%253DGender%253AWomens%252BDepartment%253AClothing%2Ctitle%3AClothing%7Cid%3Awomens-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-bottoms-clothing.jsp%253FCN%253DGender%253AWomens%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3ABottoms%7Cid%3Awomens-jeans-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-jeans-bottoms-clothing.jsp%253FCN%253DGender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3AJeans%7Cid%3Awomens-lee%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-lee.jsp%253FCN%253DGender%253AWomens%252BBrand%253ALee%2Ctitle%3ALee&meta=brand%3Alee%2Ccolor%3Avista", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=atyqtk", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=jpc5tj", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=o0au47", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=va4gme", END_INLINE
        );

        status = nsApi.ns_end_transaction("wxyfzSQkpEztWWCh_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X833748");
        status = nsApi.ns_web_url ("X833748",
            "URL=https://rh.nexus.bazaarvoice.com/highlights/v3/1/kohls/3833748",
            INLINE_URLS,
                "URL=https://display.ugc.bazaarvoice.com/common/static-assets/3.3.3/jquery-bv%403.5.1%2Blodash-bv%404.17.19.js", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=mkt6p2", END_INLINE
        );

        status = nsApi.ns_end_transaction("X833748", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_55");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_55",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/scripts/bv-primary.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_55", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_56");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_56",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=5&dtm_email_hash=N/A&dtm_user_id=196815f0-9900-44f0-adaf-570fe8cace1a&dtmc_department=clothing&dtmc_category=bottoms&dtmc_sub_category=jeans&dtmc_product_id=3833748", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/stylesheets/screen.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_56", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_57");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_57",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=Horizontal1%7C15%2CHorizontal2%7C15%2CHorizontal3%7C15", "METHOD=OPTIONS", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_57", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X833748_json");
        status = nsApi.ns_web_url ("X833748_json",
            "URL=https://content.syndigo.com/page/8f845786-bb87-4065-810b-e52a8a2220e3/3833748.json?u=1D2282C0-0574-49FE-A7C2-D2C8F8DBDD24&prtnid=8f845786-bb87-4065-810b-e52a8a2220e3&siteid=8f845786-bb87-4065-810b-e52a8a2220e3&pageid=3833748&s=1621584349604&v=v1.0.187&visitid=42895612-B2C5-410F-8B90-39D2A322A5A0&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&r=0.4211417436511675&pageurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1",
            INLINE_URLS,
                "URL=https://fm.flashtalking.com/segment/591/view/3833748/?guid=1f1c1ea80b0e48d388d36c2aac50d6f9", END_INLINE,
                "URL=https://fm.flashtalking.com/segment/591/view/3833748", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=https://event.syndigo.cloud/event/p.gif?u=1D2282C0-0574-49FE-A7C2-D2C8F8DBDD24&prtnid=8f845786-bb87-4065-810b-e52a8a2220e3&siteid=8f845786-bb87-4065-810b-e52a8a2220e3&pageid=3833748&s=1621584349604&v=v1.0.187&visitid=6424BBB6-ACE8-44CD-B793-34BCFA12ED32&dt=0.012&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&r=0.8367316809818031&pageurl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1", END_INLINE,
                "URL=https://sc-static.net/scevent.min.js", END_INLINE,
                "URL=https://scontent.webcollage.net/api/v2/product-content?loadlegacycontent=true", END_INLINE
        );

        status = nsApi.ns_end_transaction("X833748_json", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("batch_json_2");
        status = nsApi.ns_web_url ("batch_json_2",
            "URL=https://api.bazaarvoice.com/data/batch.json?passkey=9zz78jlr8mloisoz9800sqwo5&apiversion=5.5&displaycode=9025_10_0-en_us&resource.q0=products&filter.q0=id%3Aeq%3A3833748&stats.q0=questions%2Creviews&filteredstats.q0=questions%2Creviews&filter_questions.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_answers.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviews.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviewcomments.q0=contentlocale%3Aeq%3Aen_GB%2Cen_US&resource.q1=questions&filter.q1=productid%3Aeq%3A3833748&filter.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&sort.q1=totalanswercount%3Adesc&stats.q1=questions&filteredstats.q1=questions&include.q1=authors%2Cproducts%2Canswers&filter_questions.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_answers.q1=contentlocale%3Aeq%3Aen_GB%2Cen_US&limit.q1=10&offset.q1=0&limit_answers.q1=10&resource.q2=reviews&filter.q2=isratingsonly%3Aeq%3Afalse&filter.q2=productid%3Aeq%3A3833748&filter.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&sort.q2=submissiontime%3Adesc&stats.q2=reviews&filteredstats.q2=reviews&include.q2=authors%2Cproducts%2Ccomments&filter_reviews.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_reviewcomments.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&filter_comments.q2=contentlocale%3Aeq%3Aen_GB%2Cen_US&limit.q2=8&offset.q2=0&limit_comments.q2=3&callback=BV._internal.dataHandler0"
        );

        status = nsApi.ns_end_transaction("batch_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("jquery_bv_403_5_1_2Blodash_b");
        status = nsApi.ns_web_url ("jquery_bv_403_5_1_2Blodash_b",
            "URL=https://display.ugc.bazaarvoice.com/common/static-assets/3.3.3/jquery-bv%403.5.1%2Blodash-bv%404.17.19.js.map"
        );

        status = nsApi.ns_end_transaction("jquery_bv_403_5_1_2Blodash_b", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_11");
        status = nsApi.ns_web_url ("experiences_11",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=Horizontal1%7C15%2CHorizontal2%7C15%2CHorizontal3%7C15",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("experiences_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("summary_3");
        status = nsApi.ns_web_url ("summary_3",
            "URL=https://api.bazaarvoice.com/data/display/0.2alpha/product/summary?PassKey=9zz78jlr8mloisoz9800sqwo5&productid=3833748&contentType=reviews&reviewDistribution=primaryRating,recommended&rev=0&contentlocale:eq=,en_GB,en_US",
            INLINE_URLS,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&e=t%3Api%7Cts%3A1621584349169%7Cdt%3APdp%7Cppid%3Ap_690_c5f6d41fb29baea1b0fc55987a56afb06c5dd938128b2a3bb78c237d59d18322%7Cpid%3A%273833748%27%7Cpsid%3As_690_a0a172245a78878f102e2129680aea0c0071a9f2b4f7657658cb713296e9f075&cache=_186a40f5-e872-4458-b76c-eba39fda530c", END_INLINE
        );

        status = nsApi.ns_end_transaction("summary_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_7");
        status = nsApi.ns_web_url ("id_json_7",
            "URL=https://network.bazaarvoice.com/id.json?_=258cn1&callback=_bvajsonp1",
            INLINE_URLS,
                "URL=https://apps.zineone.com/c3/api/v1/connect/c40e0cae-ff21-45b8-df48-c47aed252895?deviceId=c40e0cae-ff21-45b8-df48-c47aed252895&os=html5&devicetype=desktop&loadConfig", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_8");
        status = nsApi.ns_web_url ("id_json_8",
            "URL=https://network.bazaarvoice.com/id.json?_=6vy5um&callback=_bvajsonp2",
            INLINE_URLS,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_9");
        status = nsApi.ns_web_url ("id_json_9",
            "URL=https://network.bazaarvoice.com/id.json?_=7pm2h5&callback=_bvajsonp3"
        );

        status = nsApi.ns_end_transaction("id_json_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_10");
        status = nsApi.ns_web_url ("id_json_10",
            "URL=https://network.bazaarvoice.com/id.json?_=i7doqt&callback=_bvajsonp4",
            INLINE_URLS,
                "URL=https://servedby.flashtalking.com/segment/modify/u6h;;pixel/?valuePairs=&setTime=0&granularity=day&f=pwr_591_VIEW", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_11");
        status = nsApi.ns_web_url ("id_json_11",
            "URL=https://network.bazaarvoice.com/id.json?_=qu8ja2&callback=_bvajsonp5",
            INLINE_URLS,
                "URL=https://log.pinterest.com/?type=pidget&guid=t_7tME1MUbUw&tv=2021040501&event=init&sub=www&button_count=0&follow_count=0&pin_count=0&profile_count=0&board_count=0&section_count=0&lang=en&nvl=en-GB&via=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp&viaSrc=canonical", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_25");
        status = nsApi.ns_web_url ("floop_25",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/2263298_Bewitched?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3045160_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2912729_Soft_Khaki?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3811392_Ash_Heather?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/1875948_Bewitched?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3045155_Rayne?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3406702_El_Paso?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4462012_Compass?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4538852_Orland_Signature?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2953086_Modern_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4471204_Anchor?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4462288_Nightshade?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4380113_Light_Wash?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2423467_Inspire_Blue?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4463543_Nightshade?wid=300&hei=300&op_sharpen=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_25", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_26");
        status = nsApi.ns_web_url ("floop_26",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/images/pdp/checkmark.svg", END_INLINE,
                "URL=https://tr.snapchat.com/cm/i?pid=8e9bd9e0-8284-4dbd-ab20-145759728098", END_INLINE,
                "URL=https://scontent.webcollage.net/kohls/mosaic-board-meta?ird=true&channel-product-id=3833748", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_26", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_58");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_58",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_58", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_59");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_59",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/images/pmp_imgs/down.svg", END_INLINE,
                "URL=https://network.bazaarvoice.com/sid.gif?_=3lt32h", END_INLINE,
                "URL=https://tr.snapchat.com/p", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://scontent.webcollage.net/kohls/api/js/method/load-content/type/ppp?environment=live&cpi=3833748", END_INLINE,
                "URL=https://54243aa124c7306ff7ac879892e2596c.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=2523404050276736&correlator=2736264517518685&output=ldjh&impl=fifs&eid=31060790%2C31061259%2C31061003%2C31061143%2C44743003&vrg=2021051801&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Cclothing%2Cbottoms%2Cjeans&enc_prev_ius=%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3%2C%2F0%2F1%2F2%2F3&prev_iu_szs=320x50%7C1024x45%7C1024x64%7C1024x128%2C728x90%7C1024x45%7C1024x64%7C1024x128%2C728x90%2C300x250%2C300x250%2C300x250&fluid=height%2C0%2C0%2C0%2C0%2C0&prev_scp=pos%3Dmarquee%7Cpos%3Dmiddle%7Cpos%3Dbottom%7Cpos%3Dbottomleft%7Cpos%3Dbottommiddle%7Cpos%3Dbottomright&cust_params=channel%3Ddesktop%26env%3Dprod%26pgtype%3Dpdp&cookie=ID%3D0cbc4e7a3c1946ae%3AT%3D1621584297%3AS%3DALNI_MYF0kCRQonfBjJt2_PacK9ToC9Ndg&bc=31&abxe=1&lmt=1621584352&dt=1621584352294&dlt=1621584342249&idt=4546&frm=20&biw=1349&bih=607&oid=3&adxs=163%2C20%2C20%2C185%2C525%2C865&adys=168%2C2849%2C3819%2C3851%2C3851%2C3851&adks=660533235%2C3288486087%2C2283361462%2C4172809658%2C2339037280%2C3950868409&ucis=1%7C2%7C3%7C4%7C5%7C6&ifi=1&u_tz=330&u_his=6&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&vis=1&dmc=8&scr_x=0&scr_y=243&psz=1349x25%7C1309x0%7C1309x0%7C300x32%7C300x32%7C300x32&msz=1024x25%7C1309x0%7C1309x0%7C300x0%7C300x0%7C300x0&ga_vid=1759937978.1621584352&ga_sid=1621584352&ga_hid=2046197025&ga_fc=false&fws=0%2C0%2C0%2C0%2C0%2C0&ohw=0%2C0%2C0%2C0%2C0%2C0&btvi=0%7C1%7C2%7C3%7C4%7C5", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:bv-loader,bvProductVersion:%2713.2.9%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:%2730.0200%27,endTime:%275255.9750%27,locale:en_US,name:timeToRunScout,startTime:%275225.9550%27,type:Performance))&_=hqjjq0", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:RatingSummary,bvProductVersion:%272.44.0%27,cl:Diagnostic,deploymentZone:redesign,displaySegment:baseline,elapsedMs:2.2250000038184226,locale:en_US,name:bv_rating_summary_render_time,productId:%273833748%27,startTime:6099.220000032801,type:Performance))&_=j4gp41", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Embedded&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&displaySegment=baseline&bvProduct=RatingSummary&bvProductVersion=2.44.0&productId=3833748&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621584342382,req:581,res:168,tot:-1621584341631)&_=twyqw5&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC988eacf9936b486d87487944f264070b-source.min.js", END_INLINE,
                "URL=https://scontent.webcollage.net/kohls/power-page?ird=true&channel-product-id=3833748", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Embedded&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&displaySegment=baseline&bvProduct=questions&subject=Kohls&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621584342382,req:581,res:168,tot:-1621584341631)&_=fij1gx&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Embedded&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&displaySegment=baseline&bvProduct=ReviewHighlights&bvProductVersion=3.2.6&productId=3833748&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621584342382,req:581,res:168,tot:-1621584341631)&_=81hn00&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Embedded&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&displaySegment=baseline&bvProduct=reviews&subject=Kohls&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&r_t=(con:0,dns:0,load:-1621584342382,req:581,res:168,tot:-1621584341631)&_=vk4ov8&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_59", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_60");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_60",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:ReviewHighlights,bvProductVersion:%273.2.6%27,cl:Impression,collectionId:28,collectionName:comfort,contentId:%27163127301-sr:0:60%27,contentType:Snippet,deploymentZone:redesign,displaySegment:baseline,locale:en_US,productId:%273833748%27,type:UGC))&_=4wqd4q", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/swatch_unavail.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_60", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_61");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_61",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/images/badgeImages/incentivizedreview.png", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstKkvyrQQBPuX61lslk6hAHIJ8pBRFDjuJzc-XhZ3_57ZJC8RT2oEWqouUuDdKXQUR3zVdJY-rUupOEGEtM2yJo9KJFihe9Gw5lOMoqo15T32V2lLkoQsGdiTsIkEClMc2hgJsjMI5iaS84kzAEBi25m0l4Q5aIqs4u0zF70MjaSx4e-odxw9nZFYl1idNk_XzJIUtMjelm53HzKnRd3AfBMub1dCCxbeerbqjqqHPBvTp7EH177rI5BsKtaWd6dIWptH2J-gjUogShAEET3CXjlMkowsXLKm5vYsAw2JnSiBtOCqaBdr_vIC7If3GrTPs-Uw&sig=Cg0ArKJSzPZZ-KVOVUiAEAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/js/ext.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/610125684829382582", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/9820687163340561195?", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsuphYdmBCYQMNUWXhDOxatBj9IEv0CWRUQ93zS4HHn3daHQitCMeeN0UxUAtveyAjUijph0mybAc5nArYFZKLo3miLDYbECqxLt900tAnEJ4tLdQlklTgewN4GcjGwEr-4PogdnxAaVxTfV1DKepujPw8DDE_vP2xnlNjaMB4OiAxqKf0aJS4SYFfUgS8z9pjKRwj1Pdz3-0ZmqaMp_gmX_6j3CjTNU_1J0jtkYIOH_XMlyrNSRCA6oCYdrZ2TsBwLGpSmrnneaWyHLv-lLdsXtY_4K1lphNMyvVxU-ptaoxYn2IURC9FI&sig=Cg0ArKJSzNfRTSCyD6HjEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=bv-loader&environment=prod&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProduct:ReviewHighlights,bvProductVersion:%273.2.6%27,cl:Impression,collectionId:22,collectionName:satisfaction,contentId:%27155701268-sr:0:34%27,contentType:Snippet,deploymentZone:redesign,displaySegment:baseline,locale:en_US,productId:%273833748%27,type:UGC),(bvProduct:ReviewHighlights,bvProductVersion:%273.2.6%27,cl:Impression,collectionId:10,collectionName:quality,contentId:%27177452488-sr:0:52%27,contentType:Snippet,deploymentZone:redesign,displaySegment:baseline,locale:en_US,productId:%273833748%27,type:UGC))&_=bvo914", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssA0o6orLyG2asiuP0wt0RUvkrCyUJvXolvsoBjsuGGknSP7ePQJsXAbOBam_fr7vXX3ecxQbJzR5usvSlWkx9pL40lxQKVFNH1gYtX8ybXm3aaXD3Knr8aC2vtw6tmByzj8Jj7x5nZ9kC2LaTi098bc1oEGWTq_WzLH2fQURMDBXeBcMVYUvKFJHLzdK27HtDOWMfbU_-uSqYkkZbW3GdjV0wPtKBVScu5MU5WNruhpF5uH-hsJQmGO3KAhskoUNip-3Bw1ogkdUYhFN4Z2sLXOO_kspXKvS1iJ_-_JvKq_7u9K1TH6cbWcg&sig=Cg0ArKJSzGhexq9S2e8mEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://display.ugc.bazaarvoice.com/static/Kohls/redesign/1292/9025_10_0/en_US/scripts/secondary.js", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138345558598&adorderid=2839226817&adunitid=16811592&advertiserid=32614632&lineitemid=5657461629&rand=387423425", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_61", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_json_12");
        status = nsApi.ns_web_url ("id_json_12",
            "URL=https://network.bazaarvoice.com/id.json?_=w0tslm&callback=_bvajsonp6",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307873913&adorderid=2682029031&adunitid=16811592&advertiserid=32614632&lineitemid=5337912127&rand=1349980403", END_INLINE,
                "URL=https://s.btstatic.com/lib/d3ba78441d586dd7df57c95bad6ca4771a9d3907.js?v=2", END_INLINE,
                "URL=https://scontent.webcollage.net/apps/pp/assets/kohls/js/acssite%40s.min.js?ver=R00ZPAJFC00ZBTIKA", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvR3FAOmBV32Yv9RyDeXK12AcZPqSN1MBgTBjHtjqzq58s1X_sCdTSC5lFxq36Dp1XrhxykbbtjZwd3begxr-Lp1iVBhe32FhB5pk5cvG4nb1IJnGYFItWWZiGSA8rDrF2i0Ru0PHQIrcpQfpHvvjq3x5xd2CkjjIQZaZkmqbk7Tzr-BrSw1Puid2dFjFQtlynaH7GOxHSoeJdqjGE-AcTdrd13gpK257037LsHMB7HHI_tDE39_4MTiWXIQoF5MKmGCX4A4ZMHtbSHP02x_es4NcygxMj0SuIJ7DrxVZLBFTyQGzmd9Q&sig=Cg0ArKJSzOiuItKqBJlwEAE&adurl=", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621584355061&cv=9&fst=1621584355061&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_json_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_9");
        status = nsApi.ns_web_url ("X349_9",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621584355070&cv=9&fst=1621584355070&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:7234.960000030696,endTime:7234.960000030696,locale:en_US,name:bv-scout-start,startTime:0,type:Performance))&_=cuxydc", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=v221jq", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:974.53000000678,endTime:8209.490000037476,locale:en_US,name:bv-primary-ready,startTime:7234.960000030696,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:26.319999946281314,endTime:8235.809999983758,locale:en_US,name:bv-primary-run,startTime:8209.490000037476,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:877.1449999767356,endTime:9086.635000014212,locale:en_US,name:bv-slow-path-ready,startTime:8209.490000037476,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:1851.6749999835156,endTime:9086.635000014212,locale:en_US,name:bv-core-app,startTime:7234.960000030696,type:Performance),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275052102%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27))&_=tf35it", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=obqwxd", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Product&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&label=Default&productId=3833748&bvProduct=AskAndAnswer&categoryId=WMNS_NTNL_BRAND_DENIM&numQuestions=13&numAnswers=9&version=2.0&context=Read&siteId=redesign&bvProductVersion=3.1.12&initial=true&pages=2&subjectType=Product&subjectId=3833748&contentType=Question&brand=LEE&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&_=5barm2&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274669732%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274579360%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274024323%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274019962%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274015322%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%273925751%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%273868283%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274913584%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274884028%27,contentType:question,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275617281%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275614492%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275334508%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275263365%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274854323%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274836280%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274835833%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%274698952%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(bvProduct:AskAndAnswer,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%275000778%27,contentType:answer,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27180072864%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27180059667%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27178545051%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27178384255%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27178203095%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27178047736%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27178030889%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27),(brand:LEE,bvProduct:RatingsAndReviews,bvProductVersion:%273.1.12%27,categoryId:WMNS_NTNL_BRAND_DENIM,cl:Impression,contentId:%27177677781%27,contentType:review,context:Read,deploymentZone:redesign,initialContent:!t,locale:en_US,siteId:redesign,type:UGC,version:%272.0%27))&_=pl0bow", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=k1rgf3", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=xj7pi0", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=sw56kc", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=pj2gaf", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=7kczr0", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=ggd028", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621584355061&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=1092311787&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=7rd5ko", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=tzgjh4", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=taeaf0", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=qj2ge4", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=29vti3", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621584355070&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=524189679&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=enmk7b", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621584355070&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=524189679&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621584355061&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=1092311787&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://rel.webcollage.net/apps/el?e=aplus-no-content&channel-product-id=3833748&partnerid=kohls&page-url=https%3A//www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&content-package=ppp&unique-user-id=&localtimestamp=1621584356130&_sof", END_INLINE,
                "URL=https://s.yimg.com/wi/ytc.js", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=yjz9db", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=PageView&loadId=9f0c4d362f3928567&type=Product&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&label=Default&productId=3833748&bvProduct=RatingsAndReviews&categoryId=WMNS_NTNL_BRAND_DENIM&version=2.0&context=Read&siteId=redesign&bvProductVersion=3.1.12&initial=false&pages=6&subjectType=Product&subjectId=3833748&contentType=Review&brand=LEE&numReviews=164&numRatingsOnlyReviews=15&percentRecommend=NaN&avgRating=4.3&href=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&canurl=https://www.kohls.com/product/prd-3833748/womens-lee-legendary-straight-leg-jeans.jsp&res=1366x768&lang=en-gb&charset=UTF-8&geo=1&cookies=1&_=ynr33a&ref=https://www.kohls.com/catalog/womens-jeans-bottoms-clothing.jsp%3FCN%3DGender:Womens%2BProduct:Jeans%2BCategory:Bottoms%2BDepartment:Clothing%26icid%3Djeans-VN-women", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=bpfikl", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=w0u6kq", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=b4m9u7", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=gpj07p", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=r1w6lm", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=bmzoap", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=bserie", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=i7o490", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=i6ehg", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=dukj4z", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=j501d5", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_62");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_62",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=c0posc", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=ibd2hi", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=osu70", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=q87m0t", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=dh25w8", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_62", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_63");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_63",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621584357345&mi_u=anon-1621584290947-3383135952&mi_cid=8212&page_title=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&referrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&timezone_offset=-330&event_type=cart_add&cdate=1621584348653&ck=host&anon=true&title=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&id=3833748&price=30.00&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp&categories=id%3Awomens%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens.jsp%253FCN%253DGender%253AWomens%2Ctitle%3AWomens%7Cid%3Awomens-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-clothing.jsp%253FCN%253DGender%253AWomens%252BDepartment%253AClothing%2Ctitle%3AClothing%7Cid%3Awomens-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-bottoms-clothing.jsp%253FCN%253DGender%253AWomens%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3ABottoms%7Cid%3Awomens-jeans-bottoms-clothing%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-jeans-bottoms-clothing.jsp%253FCN%253DGender%253AWomens%252BProduct%253AJeans%252BCategory%253ABottoms%252BDepartment%253AClothing%2Ctitle%3AJeans%7Cid%3Awomens-lee%2Curl%3Ahttps%253A%252F%252Fwww.kohls.com%252Fcatalog%252Fwomens-lee.jsp%253FCN%253DGender%253AWomens%252BBrand%253ALee%2Ctitle%3ALee&meta=brand%3ALee%2Ccolor%3AVista", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=rkp256", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_63", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("addItemToCart_3");
        status = nsApi.ns_web_url ("addItemToCart_3",
            "URL=https://www.kohls.com/cnc/checkout/cartItems/addItemToCart",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=8jumxm", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/crypto_message-3-4.htm", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/ak-challenge-3-4.htm", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?cl=Error&loadId=9f0c4d362f3928567&type=firebird&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&locale=en_US&deploymentZone=redesign&bvProductVersion=3.1.12&_=h80i2v", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:4502.804999996442,endTime:12712.295000033919,locale:en_US,name:bv-qa_show_questions-rendered,startTime:8209.490000037476,type:Performance))&_=xwrvzp", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:5475.399999995716,endTime:12719.244999985676,locale:en_US,name:bv-qa_show_questions-completed,startTime:7243.84499998996,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:3089.8400000296533,endTime:10332.800000032876,locale:en_US,name:bv-preload,startTime:7242.960000003222,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:4577.824999985751,endTime:12787.315000023227,locale:en_US,name:bv-rr_show_reviews-rendered,startTime:8209.490000037476,type:Performance),(bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:5551.144999975804,endTime:12795.444999996107,locale:en_US,name:bv-rr_show_reviews-completed,startTime:7244.300000020303,type:Performance))&_=u8272p", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s61734408620383?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A35%3A57%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=pdp&events=event62&products=%3B3833748%3B%3B%3B%3Bevar11%3Dnot%20collection%7Cevar16%3Dn%7Cevar74%3D50.00_30.00&c4=pdp&c9=product%20detail%20page&v9=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&c10=product%20detail%20page&c11=product%20detail%20page&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&v59=product%20page&v63=1&c64=VisitorAPI%20Present&v68=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&v75=pdp20-standard&v86=21may.w2&v87=pdp20&c.&a.&activitymap.&page=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&link=8%20SHORT&region=sbOptions_98616616&pageIDType=1&.activitymap&.a&.c&pid=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&pidt=1&oid=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26pr&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-if-3-4.css", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-2d3p6wz&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&mode=v2&cf=4847505%2C6603093%2C7422632%2C7424035%2C7472048&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-if-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-default-chlge-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183", END_INLINE,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&e=t%3Aivi%7Cts%3A1621584358214%7Cet%3A1503%7Ciid%3Ae2db0995-93d6-4a14-9ffa-e5839d048991%7Cpos%3A1&cache=_3cd40c39-6f59-422a-bccf-d78becd58ee0", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/ak-challenge-3-4.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("addItemToCart_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_5");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_5",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_6");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_6",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_39");
        status = nsApi.ns_web_url ("index_39",
            "URL=https://684fc53d.akstat.io/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjssCw7TDKWAYsO7QzD8lCoxLSlIY2oFLppTpyD6q2VWYAFPCIikl2Jrn-QcpLm8l2ZZN1OhVFN_ew_jXCWrmvPu8PrENyNfXSxFBE5VvyPY&sig=Cg0ArKJSzG2zDlJM90z2EAE&id=lidar2&mcvt=1237&p=168,163,232,1187&mtos=1141,1141,1237,1262,1332&tos=1141,0,96,25,70&v=20210519&bin=7&avms=nio&bs=1349,607&mc=1&app=0&itpl=3&adk=660533235&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621584354093&dlt=0&rpt=1255&isd=307&msd=807&r=v&fum=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_39", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_64");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_64",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_64", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3fno1917698e839c90363_7");
        status = nsApi.ns_web_url ("X8a664a3fno1917698e839c90363_7",
            "URL=https://www.kohls.com/clientlibs/58a664a3fno1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/_sec/cp_challenge/verify", END_INLINE,
                "URL=https://event.webcollage.net/event/fc.gif?siteCode=kohls&calls=loadProductContent%7C_loadSyndi%7CloadProductContent%7C_loadLegacyWC%7C_loadLegacyContent&r=0.5226214164824796", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3fno1917698e839c90363_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_65");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_65",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_65", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_66");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_66",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_66", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_67");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_67",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_67", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_68");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_68",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_68", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_69");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_69",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_69", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_70");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_70",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s61116920788304?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A36%3A12%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&events=event38&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&c50=D%3Ds_tempsess&c53=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&c64=VisitorAPI%20Present&v68=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&pe=lnk_o&pev2=Sku%20Selection%20-%20Manual&c.&a.&activitymap.&page=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&link=Vista&region=panel4060351&pageIDType=1&.activitymap&.a&.c&pid=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&pidt=1&oid=javascript%3Avoid%280%29%3B&ot=A&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_70", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_71");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_71",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_71", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("addItemToCart_4");
        status = nsApi.ns_web_url ("addItemToCart_4",
            "URL=https://www.kohls.com/cnc/checkout/cartItems/addItemToCart",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("addItemToCart_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("minicart_2");
        status = nsApi.ns_web_url ("minicart_2",
            "URL=https://www.kohls.com/snb/staticmessages/minicart",
            INLINE_URLS,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s61509960373112?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A36%3A16%205%20-330&d.&nsid=0&jsonv=1&.d&fid=41BB0DC3469F1B52-1F7FADAAB59D1B58&ce=UTF-8&ns=kohls&pageName=cart%3Aadd%20item&g=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=cart%20add&events=scAdd%2CscOpen&products=%3B3833748%3B1%3B30%3B%3Bevar11%3Dnot%20collection%7Cevar13%3Dkf%7Cevar74%3D50_30%7Cevar16%3Dn%7Cevar51%3D18899633%7Cevar77%3Dship%20only%7Cevar107%3Dpdp-regular&c4=cart%20add&c9=cart%7Ccart%20add&v9=pdp%20%283833748%29%20women%27s%20lee%C2%AE%20legendary%20straight-leg%20jeans&c10=cart&c11=cart&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v39=no%20customer%20id&v40=cloud17&v42=guest&v48=8949752835760001&c50=D%3Ds_tempsess&c53=cart%3Aadd%20item&v59=product%20page&c64=VisitorAPI%20Present&v68=cart%3Aadd%20item&v70=196815f0-9900-44f0-adaf-570fe8cace1a&v71=klsbrwcki%7C196815f0-9900-44f0-adaf-570fe8cace1a&v73=no%20loyalty%20id&v75=pdp20-standard&v76=n%2Fa&v86=21may.w2&v87=pdp20&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/icCartG.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/right-slide-black.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/left-slide-grey.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/right-slide-grey.png", END_INLINE,
                "URL=https://www.kohls.com/cnc/media/images/kohls_expert_card.png", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-2d3p6wz&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&mode=v2&cf=4847505%2C6603093%2C7422632%2C7424035%2C7472048&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE,
                "URL=https://www.kohls.com/snb/media/44.0.0-1535/images/ajax-loader.gif", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/pdp/left.svg", END_INLINE
        );

        status = nsApi.ns_end_transaction("minicart_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_27");
        status = nsApi.ns_web_url ("floop_27",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/images/pdp/rightnw.svg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista?wid=180&hei=180&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3833748_Vista_sw", END_INLINE,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&e=t%3Aivi%7Cts%3A1621584368658%7Cet%3A9607%7Ciid%3Ae2db0995-93d6-4a14-9ffa-e5839d048991%7Cpos%3A1&cache=_82494518-b658-45bb-ac5b-95730dad5d70", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=pdpta0;ord=4144957925322;gtm=2od5c1;auiddc=608256523.1621584295;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3Dvista%26prdpv%3D1;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=bottoms;u2=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u23=New%20Customer;u24=;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u3=30.00;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1?", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde663c7b16da4ba4947e827dd1a4af31-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_27", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("sodar_2");
        status = nsApi.ns_web_url ("sodar_2",
            "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2021051801&st=env",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=-2d3p6wz&referrer=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&docReferrer=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&mode=v2&cf=4847715%2C4847939%2C6094051%2C6706303%2C6706308&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NTU0NDkwNTg2MjMzMDkyNjg0NA&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MTc1NDUzNTQ4MjEyNjc1ODU5Ng", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=COaVlMmo2vACFUaGaAod14UA_Q;src=8632166;type=landi0;cat=pdpta0;ord=4144957925322;gtm=2od5c1;auiddc=608256523.1621584295;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3Dvista%26prdpv%3D1;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=bottoms;u2=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u23=New%20Customer;u24=;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u3=30.00;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1?", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?gdpr=0&ttd_pid=signal&ttd_tpi=1&ttd_puid=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ", END_INLINE,
                "URL=https://network.bazaarvoice.com/st.gif?loadId=9f0c4d362f3928567&BVBRANDID=d4f1bccb-acdc-4a08-9e49-bd582519c696&BVBRANDSID=fab96e3b-45e9-4817-95fe-9217eaf411d0&BVCRL8ID=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&tz=-330&sourceVersion=3.13.4&magpieJsVersion=3.13.4&source=firebird&client=Kohls&dc=9025_10_0&host=www.kohls.com&r_batch=!((bvProductVersion:%273.1.12%27,cl:Diagnostic,deploymentZone:redesign,elapsedMs:33642.0399999693,endTime:40877,locale:en_US,name:bv-host-load,startTime:7234.960000030696,type:Performance))&_=8twkr0", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?gdpr=0&google_nid=signal_dmp&google_cm&btt=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&d=Fri%2C%2021%20May%202021%2008%3A06%3A22%20GMT&n=-5&b=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&enc=UTF-8&yv=1.9.2&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&enc=UTF-8&yv=1.9.2&et=custom&ea=AddToCart&product_id=3833748&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://sp.analytics.yahoo.com/sp.pl?a=10000&b=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&.yp=26892&f=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&e=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&enc=UTF-8&yv=1.9.2&et=custom&ea=ViewProduct&product_id=3833748&tagmgr=gtm%2Cadobe%2Csignal", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/2019801-hp-wallet-bar-credit?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=COaVlMmo2vACFUaGaAod14UA_Q;src=8632166;type=landi0;cat=pdpta0;ord=4144957925322;gtm=2od5c1;auiddc=*;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3Dvista%26prdpv%3D1;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=bottoms;u2=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u23=New%20Customer;u24=;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u3=30.00;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC1c1b30b08b024ebfa9748b44ff4a21f7-source.min.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2/222/runner.html", END_INLINE,
                "URL=https://www.google.com/recaptcha/api2/aframe", END_INLINE,
                "URL=https://js.cnnx.link/roi/cnxtag-min.js?id=31851", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCacdd8d53a0eb4a89a0a72c2d6a4147ec-source.min.js", END_INLINE,
                "URL=https://mon1.kohls.com/test_rum_nv?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=3&d=1|0|-1|26|749|-1|-1|-1|583|168|1|40259|1|132|40877|1578|3|35161|0|555|https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women|https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1|www.kohls.com|kohls_cart%3DajJLAJVgtAZD9tl7F33ow1D5dF26w453V2%2BRxYFKBDvT%2FDjTvekmOn%2FsZJqHCIpwTCCv6S5%2FpPF0kdAXRWONHQ%3D%3D%3B%20VisitorBagTotals%3D%2441.29%25PIPE%251%25PIPE%25%248.95%3B%20VisitorId%3D196815f0-9900-44f0-adaf-570fe8cace1a%3B%20CavNV%3D4673177592873995636-33333-233049919949-0-9-0-1-145-28077-26473%3B%20CavSF%3DcavnvComplete%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C1%2C%2C%2C%2C%2C%2C%3B%20cookieSetting%3DisCookie%3B%20AKA_RV2%3D72%3B%20AKA_CNC2%3DFalse%3B%20akacd_www-kohls-com-mosaic-p2%3D2177452799~rv%3D78~id%3D04860d579c73ae10187b51255ad0b3d0%3B%20check%3Dtrue%3B%20AMCVS_F0EF5E09512D2CD20A490D4D%2540AdobeOrg%3D1%3B%20s_ecid%3DMCMID%25PIPE%2576403998605952159830891476251030620443%3B%20mosaic%3Dgcpb%3B%20mboxEdgeCluster%3D31%3B%20_dy_csc_ses%3Dt%3B%20_dy_c_exps%3D%3B%20_dycnst%3Ddg%3B%20_dyid%3D7656119531360184738%3B%20_dyjsession%3D8e1ca56c544bf696269b143792eba19f%3B%20dy_fs_page%3Dwww.kohls.com%3B%20_dycst%3Ddk.w.c.ms.%3B%20_dy_df_geo%3DIndia..Jaipur%3B%20_dy_geo%3DIN.AS.IN_RJ.IN_RJ_Jaipur%3B%20spa%3D3%3B%20s_cc%3Dtrue%3B%20_dyid_server%3D7656119531360184738%3B%20aam_uuid%3D71429088129042532550393988433173938227%3B%20_mibhv%3Danon-1621584290947-3383135952_8212%3B%20IR_gbd%3Dkohls.com%3B%20_gcl_au%3D1.1.608256523.1621584295%3B%20SignalSpring2016%3DA%3B|0|101|-1|24|24|WINDOWS|Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F56.0.2924.87%20Safari%2F537.36|en-GB|%5Bobject%20PluginArray%5D|Mozilla|0|PC|56.0|10|33333|1|0|0|%7B-1%7D|1578|-1|2668|929473|1|41|0|1121|1121|0|0|4.5.0_3aeac6|3b3973&lts=-1&d2=-1|-1|-1|1|100|0|1", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=tBLcuKl&btt=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ&uid=4846d55a-5c43-4d54-80d8-5ad9e7d701b6", END_INLINE,
                "URL=https://secure.adnxs.com/getuid?https%3A%2F%2Fs.thebrighttag.com%2Fcs%3Fbtt%3DM67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ%26uid%3D$UID%26tp%3Dan%26gdpr%3D0", "REDIRECT=YES", "LOCATION=https://s.thebrighttag.com/cs?btt=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ&uid=4030825338917790404&tp=an&gdpr=0", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCdc9d7fc5fb9e4d329178721363aaa3f9-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde61955ee7f24142bacb5bddee25a9a4-source.min.js", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/bg/zue3njNLpzxGAZrYILNRV_oDQoN1Bf4uoYDHWIdg9NQ.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCecd16bab3edd42838214adeca2f386c4-source.min.js", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=gcms&gdpr=0&btt=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ&google_gid=CAESEMUVdUNhK0XOcO3_StZ9Y58&google_cver=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pagead/sodar?id=sodar2&v=222&li=gpt_2021051801&jk=2523404050276736&rc=", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=8b58e64d-bd48-4e11-8fc9-a15018e04a8b&sid=566077a0ba0b11ebae8161853fa4c31d&vid=56611880ba0b11ebbbad61c4d19e4215&vids=0&prodid=3833748&pagetype=cart&en=Y&evt=custom&msclkid=N&rn=593011", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=8b58e64d-bd48-4e11-8fc9-a15018e04a8b&sid=566077a0ba0b11ebae8161853fa4c31d&vid=56611880ba0b11ebbbad61c4d19e4215&vids=0&pi=0&lg=en-GB&sw=1366&sh=768&sc=24&tl=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&p=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&r=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&lt=41009&pt=1621584341631,590,591,,,2,2,2,2,2,,28,583,751,618,1578,1578,1579,40877,40877,41009&pn=0,0&evt=pageLoad&msclkid=N&sv=1&rn=928462", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=8b58e64d-bd48-4e11-8fc9-a15018e04a8b&sid=566077a0ba0b11ebae8161853fa4c31d&vid=56611880ba0b11ebbbad61c4d19e4215&vids=0&prodid=3833748&pagetype=cart&en=Y&evt=custom&msclkid=N&rn=708888", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=8b58e64d-bd48-4e11-8fc9-a15018e04a8b&sid=566077a0ba0b11ebae8161853fa4c31d&vid=56611880ba0b11ebbbad61c4d19e4215&vids=0&prodid=3833748&pagetype=product&en=Y&evt=custom&msclkid=N&rn=278055", END_INLINE,
                "URL=https://bat.bing.com/p/action/4024145", END_INLINE
        );

        status = nsApi.ns_end_transaction("sodar_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_40");
        status = nsApi.ns_web_url ("index_40",
            "URL=https://684fc53d.akstat.io/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=sodar2&v=222&t=2&li=gpt_2021051801&jk=2523404050276736&bg=!fX6lfjrNAAZ7hX_Ue4U7ACkAdvg8WrtorF4y2uvF672XAvFepuw8-PL8mOn7IcE_oPS6g3g_2LzFXgIAAADRUgAAAA9oAQcKAGrNyeCP6uC086akgijVruuSENoi-uPzha_Cb6W-Mt42h0lBXZpms0wf2R3lGBDUoCk4uYAiGfzpcAVl4CgHQpVbIiFFR8MhHqRTnJPnOteqe_qYXyjw2MjEUMTpLODJ6O0PfR43m7gCcVX7mQIKRRX1Th-vObxNcloMC_Is68kW3POY0kZDLxC4UA7vnBrQ_e7IyKWlo9qWDacen6GhDMSCjAguQxXx0SBfl7J6gNPJgXZiU3fq5HtMhKpufHXNYw3xeJGAyULIoxmpU3kFpaCtbFnEfGe2zpSazYKUKadoVD_w5g9Yn0LwmrQ3AArmWjNQ9xlZWhAoATUpYfOxFdxR493BOFN--2NmgxgTa7zXelgweKN7Yg0-W2yuiPr1pJ7v3GA-3ap0atl3spwvWmNJgMaoKrTqzyW3XPeTWr5tC0japfsk--QtkCOOccOSiCUDLxSBykMD8ftucQIOcdaCqpeJT2XhTkNzZDpSZf54D3fQAAtvmCIkZQFXfWJZjcYl6w1HzmyTWIlWRRWz5vTmsgjITeEoc6V2ZCCuuogtU8mGEnCDrkYq-VpD3u2GZV4_RyGMzQOPtayKd3LbJC0VUkGvBvAt34v9N7gTJR2kEvSXxa_QHF6fGDy5MfqAi2Le1MvnJzuGgRV0ip1BbLAFVa01zVgHhtQqEK2sApjuESW185SfmRsBJ7fyPNiFZ1RHzalsTzgyx4RIZE4dklw7N4NcBWbyF06xlhn04RD3DYsZjNTdTMacmjoT4AEnba33IHByU2JGEQ64a2lxO2AJiqGcT1eu5f6BtgYU4J0w6VcfvlHDVYVATPnT4bfnOEgL3I8BXszK", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?btt=M67cMI1zCnPMa5zRkvy6AM7sX8T90ZvoXNYm_tJ3gDQ&uid=4030825338917790404&tp=an&gdpr=0", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_40", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_14");
        status = nsApi.ns_web_url ("test_rum_nv_14",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000941785232445961&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=1|1&lts=233049961&nvcounter=0",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_14", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_15");
        status = nsApi.ns_web_url ("test_rum_nv_15",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000941785232445961&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=3&d=3|0|2&lts=233049961",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_15", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("tag_2");
        status = nsApi.ns_web_url ("tag_2",
            "URL=https://tagtracking.vibescm.com/tag",
            INLINE_URLS,
                "URL=https://cdnssl.clicktale.net/www47/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=586254776683;gtm=2od5c1;auiddc=608256523.1621584295;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DInspire;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=76403998605952159830891476251030620443;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1?", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8478690a6f32401fb3fe279105e2aa1c-source.min.js", END_INLINE,
                "URL=https://sb.scorecardresearch.com/beacon.js", END_INLINE,
                "URL=https://data.adxcel-ec2.com/pixel/?ad_log=referer&action=misc&value={%22af_revenue%22:%22[value]%22,%22af_order_id%22:%22[order_id]%22}&pixid=6a175b53-c680-4327-aef7-1eb5647a6339", END_INLINE,
                "URL=https://cdnssl.clicktale.net/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf3953ba39c0e4cda986a27cd34fdd08c-source.min.js", END_INLINE,
                "URL=https://cdnssl.clicktale.net/pcc/d82d7432-724c-4af9-8884-ffab4841f0a1.js?DeploymentConfigName=Release_20210505&Version=4", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www/latest-WR110.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCfde5714c92254dfd820f510d3727aef2-source.min.js", END_INLINE,
                "URL=https://kohls.sspinc.io/v1/ssp.js?env=prd", END_INLINE,
                "URL=https://sc-static.net/js-sha256-v1.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC01d660fbc4b44effb3436273c815864f-source.min.js", END_INLINE,
                "URL=https://d.agkn.com/pixel/10107/?che=243735697&mcvisid=76403998605952159830891476251030620443", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=CKKFj8qo2vACFVAb1Qodb-EHFA;src=8632166;type=landi0;cat=unive0;ord=586254776683;gtm=2od5c1;auiddc=608256523.1621584295;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DInspire;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=76403998605952159830891476251030620443;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1?", END_INLINE,
                "URL=https://b-code.liadm.com/a-00oc.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC4d3fb96a16274690a8983bbe28d8c339-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("tag_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("ssp_json_2");
        status = nsApi.ns_web_url ("ssp_json_2",
            "URL=https://kohls.sspinc.io/ssp.json?origin=https%3A%2F%2Fwww.kohls.com&lang=en",
            INLINE_URLS,
                "URL=https://b-code.liadm.com/sync-container.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("ssp_json_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_16");
        status = nsApi.ns_web_url ("test_rum_nv_16",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000941785232445961&p=1&op=pagedump&pi=1&CavStore=-1&pid=3&d=3|2|0&lts=233049961&nvcounter=2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CKKFj8qo2vACFVAb1Qodb-EHFA;src=8632166;type=landi0;cat=unive0;ord=586254776683;gtm=2od5c1;auiddc=*;u1=3833748;u10=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DInspire;u11=Women's%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=76403998605952159830891476251030620443;u25=196815f0-9900-44f0-adaf-570fe8cace1a;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1", END_INLINE,
                "URL=https://kohls.sspinc.io/lib/4.39.5/fitpredictor.en.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_16", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("X833748_2");
        status = nsApi.ns_web_url ("X833748_2",
            "URL=https://api.kohls.com/v1/marketplace/product/3833748",
            INLINE_URLS,
                "URL=https://www.google-analytics.com/analytics.js", END_INLINE,
                "URL=https://kohls.sspinc.io/skins/kohls/2.0.4/fitpredictor.css", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621584385246&cv=9&fst=1621584385246&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621584385239&cv=9&fst=1621584385239&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=PersistentBag%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.google-analytics.com/collect?v=1&_v=j90&a=2046197025&t=pageview&_s=1&dl=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ul=en-gb&de=UTF-8&dt=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&sd=24-bit&sr=1366x768&vp=1349x607&je=0&_u=YChAgAAB~&jid=2018219036&gjid=1617840623&cid=1759937978.1621584352&tid=UA-45121696-1&_gid=1692875223.1621584386&cd2=196815f0-9900-44f0-adaf-570fe8cace1a&cd4=&z=1909138376", END_INLINE,
                "URL=https://c.clicktale.net/pageview?pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&lv=1621584384&lhd=1621584384&hd=1621584384&pn=1&re=1&dw=1349&dh=8817&ww=1366&wh=607&sw=1366&sh=768&dr=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&uc=1&la=en-GB&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22cart%3Aadd%20item%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22cart%3Aadd%20item%22%5D%2C%222%22%3A%5B%22Department%20Name%22%2C%22clothing%22%5D%2C%223%22%3A%5B%22Category%20Name%22%2C%22bottoms%22%5D%2C%224%22%3A%5B%22Subcategory%20Name%22%2C%22jeans%22%5D%7D&v=10.8.6&r=759654", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=PIOwNg9ghgJgBAHzgZQC4E8wEtVYMYDOicAwhCKgE4RhAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=739602", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsBGBmAmDGBOATAZgKwFoCGAWXWuAjNrDgKYAckmkiRkqukulsRADEA%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=243581", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgEoFMA2KCGBnFB9AJgAYCBGIgVkqAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=245225", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsCMCcBsAckCsAzADAWmtNmAse6GAhgCbEoZIDsaKApvAMbFP2TFAAA%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=485412", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsB2D2AEDGBXAzgF2gWwKYCdYEsATIA%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=428956", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=PIAgPiDKCSCaDq4SwKYGcgAA&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=700395", END_INLINE,
                "URL=https://c.clicktale.net/dvar?v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&dv=N4IgDgTg9mCMDsIBcIDWUAWAbAzgAgDsoAXPLKAcwoFMATPASwIB9yBPAQy2LcJLMo16TEABoQASXwB5AngDKXashAAzLjmXiAggCEA%2BtoAq%2BgBpG8AWjwARALJ47HCKmrEwWDgGNqeXR1oKJgo8ACYABgjLWHDLUNgVf0DgsRA9QxM9K1s8AAUAJXDYGOz5AGEAGWzc6ggcBhxiagJSfxD86i98KAIVIrwjaka%2FDgoOrtT040NdbJs8ADky8IBmcIBWUsrsgFEAWw4GLDxpMFIJOXkMBjAwYLw%2B2DxtLGPiDF9%2FAgJanEmDaZZaz2PIMLyoACuYGeACMOARaD09s1SEZvMQwfhwgAWSzheBxSKPAZDUgdWgQnyw%2BGIgh%2FHQAzKzYEOIz5dYxAAc2QAYgxSLkIHQwcQoBA8AB1BiBNwk4ahPrhPBlHrEaBYf4ZGZzAb5UIrbGbaw7LDUABuHCa9BV5HF8mopq8GJ61UVeAAElAsIiIcRNdNzAAClm67EATlg3Os5jwHQA7s5aPhckd%2BBF4nj1njscSU%2BRSDFwgBSf0mGPWABSoVKsoAas48KoxX5mh6IapVAc5NYnC5FtQ4yodgAPMC1BjNKnaUvakNs%2BAANhxWyq1gA0mVnl4AI4Qhr8hgul25Gy5FQbxN5U%2BOIY4UYpBlaoE5JarMNGhTbdeYXDKy8AcXIOFjndagAlzIVdy4PAKiYVAHzSRlZ1sVl8k5bFwjDbIOmgowGGRBQwIgLwMDyX4ei4BgAC83RVFp1RnZ8QTZMMF3iUooC8BhoLsYUODwCQDhoZ4KiMfB01o1UGMfQFmRQ3V4FCBcsOsXRpH%2FaQRhlElh1IdNYEzPEVmJWAAHpq2kHkeRnct5I6GBW3kUUhRghoBXyaR5nTWINmzIdR3HSdfGnABfIAA%3D%3D&r=172210", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621584385239&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=2877503331&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621584385239&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=2877503331&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621584385246&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=86337862&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621584385246&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=6&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&tiba=Women%27s%20Lee%C2%AE%20Legendary%20Straight-Leg%20Jeans&async=1&fmt=3&is_vtc=1&random=86337862&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("X833748_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_41");
        status = nsApi.ns_web_url ("index_41",
            "URL=https://ing-district.clicktale.net/ctn_v2/auth/?pid=24&as=1&295868465&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_41", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("collect_2");
        status = nsApi.ns_web_url ("collect_2",
            "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j90&tid=UA-45121696-1&cid=1759937978.1621584352&jid=2018219036&gjid=1617840623&_gid=1692875223.1621584386&_u=YChAgAABAAAAAE~&z=523681550",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://cdnssl.clicktale.net/www/WR1113b.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("collect_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_12");
        status = nsApi.ns_web_url ("experiences_12",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=PDP&plids=PersistentBag%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.google.co.in/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1759937978.1621584352&jid=2018219036&_u=YChAgAABAAAAAE~&z=2095790854", END_INLINE,
                "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1759937978.1621584352&jid=2018219036&_u=YChAgAABAAAAAE~&z=2095790854", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("j_2");
        status = nsApi.ns_web_url ("j_2",
            "URL=https://rp.liadm.com/j?tna=v2.0.1&aid=a-00oc&wpn=lc-bundle&pu=https%3A%2F%2Fwww.kohls.com%2Fproduct%2Fprd-3833748%2Fwomens-lee-legendary-straight-leg-jeans.jsp%3Fcolor%3DVista%26prdPV%3D1&refr=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&ext_s_ecid=MCMID%7C76403998605952159830891476251030620443&ext_s_vi=%5BCS%5Dv1%7C3053B4E704A78657-60001E7130290817%5BCE%5D&ext_IXWRAPPERLiveIntentIp=%7B%22t%22%3A1621584296241%2C%22d%22%3A%7B%22response%22%3A%22pass%22%2C%22version%22%3A%221.1.1%22%7D%2C%22e%22%3A1621670696241%7D&duid=0b10d8358f40--01f66zc4ymt7j6ynazp96jmyw9&se=e30&dtstmp=1621584385197",
            INLINE_URLS,
            "URL=https://c.sspinc.io/com.snowplowanalytics.snowplow/tp2", "METHOD=OPTIONS", END_INLINE,
                "URL=https://i.liadm.com/s/c/a-00oc?s=&cim=&ps=true&ls=true&duid=0b10d8358f40--01f66zc4ymt7j6ynazp96jmyw9&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", END_INLINE,
                "URL=https://sli.kohls.com/baker?dtstmp=1621584386123", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4231668_White_Palm_Jungle?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3510365_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4015111_Maui_Ocean_Depths?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AoEWAIGYgAA%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=136350", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=DIHwBACgIhQAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=707836", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMkAwIwDYYFY7IExNRoAA%3D&isETR=false&v=10.8.6&pid=2399&uu=7c7846f4-c26c-ab00-ae90-565b5a7c7a49&sn=1&pn=1&r=275391", END_INLINE
        );

        status = nsApi.ns_end_transaction("j_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_28");
        status = nsApi.ns_web_url ("floop_28",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_28", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("prediction_3");
        status = nsApi.ns_web_url ("prediction_3",
            "URL=https://fitpredictor-api.sspinc.io/v1/prediction?type=size&auth_token=kohls%3A5a62d296439256998b4c990b80cad392&page_view_id=90c63632-5078-4d74-a9d7-0d5f12130b41&user_email_hash=&domain_userid=61c03f53-ca3f-402f-9fb0-8f2b21bc5571&mode=prediction&market=US&env=prd&lang=en&product_id=3833748&size=8%20SHORT&size_type=regular&available_size=8%20SHORT&available_size=14%20SHORT&available_size=18%20SHORT&available_size=4%20AVG%2FREG&available_size=6%20AVG%2FREG&available_size=8%20AVG%2FREG&available_size=16%20AVG%2FREG&available_size=8%20T%2FL&available_size=10%20T%2FL&available_size=16%20T%2FL&available_size=18%20T%2FL&psud_enabled=false"
        );

        status = nsApi.ns_end_transaction("prediction_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_42");
        status = nsApi.ns_web_url ("index_42",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&0&0&0&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_42", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("tp2_4");
        status = nsApi.ns_web_url ("tp2_4",
            "URL=https://c.sspinc.io/com.snowplowanalytics.snowplow/tp2",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("tp2_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_43");
        status = nsApi.ns_web_url ("index_43",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&1&0&1&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=127444&dpuuid=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2Fff3c5938b6574c91abb343a6a9145409%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", "REDIRECT=YES", "LOCATION=https://i.liadm.com/s/e/a-00oc/0/ff3c5938b6574c91abb343a6a9145409?mpid=82775&muid=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://trc.taboola.com/sg/liveintent/1/cm/", END_INLINE,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5", END_INLINE,
                "URL=https://sync.mathtag.com/sync/img?mt_exid=36&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2Fff3c5938b6574c91abb343a6a9145409%3Fmpid%3D7156%26muid%3D%5BMM_UUID%5D&b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5", END_INLINE,
                "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=4846d55a-5c43-4d54-80d8-5ad9e7d701b6", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=4846d55a-5c43-4d54-80d8-5ad9e7d701b6", END_INLINE,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5&rd=Y", END_INLINE,
                "URL=https://x.bidswitch.net/syncd?dsp_id=256&user_group=2&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", END_INLINE,
                "URL=https://i.liadm.com/s/e/a-00oc/0/ff3c5938b6574c91abb343a6a9145409?mpid=82775&muid=71429088129042532550393988433173938227", END_INLINE,
                "URL=https://x.bidswitch.net/sync?ssp=liveintent&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5", END_INLINE,
                "URL=https://i.liadm.com/s/e/a-00oc/0/ff3c5938b6574c91abb343a6a9145409?mpid=7156&muid=7f9b60a7-6a04-4400-91dc-803a0e4b67ed", END_INLINE,
                "URL=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=//i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=75b83688-dbb5-4755-8c9f-a0aa26f52cdf", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_43", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_44");
        status = nsApi.ns_web_url ("index_44",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&3&1&0&104&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=b8735bb0-b3c0-4e19-a30d-1aaaf1daf3e5", "REDIRECT=YES", "LOCATION=//pr-bh.ybp.yahoo.com/sync/iponweb?bidswitch_ssp_id=liveintent&ssp_user_id=9741949b-27d8-42e8-828f-1b9432cee027", END_INLINE,
                "URL=https://i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=75b83688-dbb5-4755-8c9f-a0aa26f52cdf", END_INLINE,
                "URL=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=4846d55a-5c43-4d54-80d8-5ad9e7d701b6", END_INLINE,
                "URL=https://pr-bh.ybp.yahoo.com/sync/iponweb?bidswitch_ssp_id=liveintent&ssp_user_id=9741949b-27d8-42e8-828f-1b9432cee027", END_INLINE,
                "URL=https://x.bidswitch.net/sync?dsp_id=74&&user_id=181371292&expires=5&ssp=liveintent", "REDIRECT=YES", "LOCATION=//i.liadm.com/s/52164?bidder_id=5298&licd=&bidder_uuid=9741949b-27d8-42e8-828f-1b9432cee027", END_INLINE,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&e=t%3Aivi%7Cts%3A1621584378658%7Cet%3A9949%7Ciid%3Ae2db0995-93d6-4a14-9ffa-e5839d048991%7Cpos%3A1&cache=_de74fe01-416a-4d79-9cc5-c82551c62adc", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_44", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_45");
        status = nsApi.ns_web_url ("index_45",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&4&1&1&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_45", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_46");
        status = nsApi.ns_web_url ("index_46",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&5&1&2&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_46", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_47");
        status = nsApi.ns_web_url ("index_47",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&6&1&3&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_47", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_48");
        status = nsApi.ns_web_url ("index_48",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&7&1&4&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_48", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_49");
        status = nsApi.ns_web_url ("index_49",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&2&2&0&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_49", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_50");
        status = nsApi.ns_web_url ("index_50",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301605152605152&24&11&8&0&2&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_50", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_17");
        status = nsApi.ns_web_url ("test_rum_nv_17",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000941785232445961&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=3&d=1|1&lts=233049970&nvcounter=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://edge.curalate.com/api/v1/metrics/experience/DOfefMiv/events.png?xp=crl8-custom-product-custom-lb-carousel-1593692829557&rid=568dda44-4c1a-40c8-b7e1-e8f6c2130e90&fpcuid=2cffef57-8f12-44c6-9a92-8264ae3e6ec9&ref=https%3A%2F%2Fwww.kohls.com%2Fcatalog%2Fwomens-jeans-bottoms-clothing.jsp%3FCN%3DGender%3AWomens%2BProduct%3AJeans%2BCategory%3ABottoms%2BDepartment%3AClothing%26icid%3Djeans-VN-women&e=t%3Aivi%7Cts%3A1621584388659%7Cet%3A9946%7Ciid%3Ae2db0995-93d6-4a14-9ffa-e5839d048991%7Cpos%3A1&cache=_62c10fbe-c064-451c-be26-dafa4a15b163", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_17", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(19.935);

        return status;
    }
}
