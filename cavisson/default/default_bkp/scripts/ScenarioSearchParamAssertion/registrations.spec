nsl_web_find(TEXT/IC="Mercury Tours", PAGE=Home, FAIL=NOTFOUND, ActionOnFail=CONTINUE, Search_IN=BODY);
nsl_web_find(TEXT/IC="mercury toURS", PAGE=Home, FAIL=NOTFOUND, ActionOnFail=CONTINUE, Search_IN=BODY);
nsl_web_find(TEXT/IC="mercuytyui", PAGE=Home, FAIL=FOUND, ActionOnFail=CONTINUE, Search_IN=BODY);
nsl_web_find(TEXT/IC="Keep-Alive", PAGE=Home, FAIL=NOTFOUND, ActionOnFail=CONTINUE, Search_IN=HEADER);
nsl_web_find(TEXT/IC="/html", PAGE=Login, FAIL=FOUND, ActionOnFail=CONTINUE, Search_IN=HEADER);
nsl_web_find(TEXT="Keep-alive", PAGE=Login, FAIL=FOUND, ActionOnFail=CONTINUE, Search_IN=HEADER);
nsl_web_find(TEXT/RE="([h|t|m|l|>]+)", PAGE=Reservation, FAIL=NOTFOUND, ActionOnFail=CONTINUE, Search_IN=ALL);
nsl_web_find(TEXT/RE="name=userSession value=([0-9|a-z|A-Z|.]+)", PAGE=Reservation, FAIL=FOUND, ActionOnFail=CONTINUE, Search_IN=ALL);
nsl_search_var(FirstSearch, PAGE=Home, RE="value=([0-9|a-z|A-Z|.]+)", LBMATCH=FIRST, SaveOffset=0, ORD=1, Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SearchANy, PAGE=Home, RE="value=([0-9|a-z|A-Z|.]+)", LBMATCH=FIRST, SaveOffset=0, ORD=ANY, Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SearchLast, PAGE=Home, RE="value=([0-9|a-z|A-Z|.]+)", LBMATCH=FIRST, SaveOffset=0, ORD=LAST,Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SearchVar, PAGE=Home, LB="userSession value=", RB=">", LBMATCH=FIRST, SaveOffset=0, ORD=1, Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SearchVarAny, PAGE=Home, LB="value=", RB=">", LBMATCH=FIRST, SaveOffset=0, ORD=ANY, Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_search_var(SearchVarLast, PAGE=Home, LB="value=", RB=">", LBMATCH=FIRST, SaveOffset=0, ORD=LAST,Search=BODY, RETAINPREVALUE="NO", EncodeMode=None);
nsl_json_var(jsonfirst, PAGE=JsonPage, ORD=1, OBJECT_PATH="root.store.book.category", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_json_var(JsonANy, PAGE=JsonPage, ORD=1, OBJECT_PATH="root.store.book.category", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_json_var(Jsonlast, PAGE=JsonPage, ORD=last, OBJECT_PATH="root.store.book.category", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_xml_var(XMLVarFirst, PAGE=XMLPage, NODE=<bookstore><book><author>, VALUE=<>, ORD=1, EncodeMode=None);
nsl_xml_var(XMLAny, PAGE=XMLPage, NODE=<bookstore><book><author>, VALUE=<>, ORD=ANY, EncodeMode=None);
nsl_xml_var(XMLLast, PAGE=XMLPage, NODE=<bookstore><book><author>, VALUE=<>, ORD=LAST, EncodeMode=None);
