package com.cavisson.scripts.ScenarioSearchParamAssertion;

import pacJnvmApi.NSApi;

public class runlogic
{

      // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
      //Start - List of used flows in the runlogic
      //Initialise the flow class
      XML_Json flowObj0 = new XML_Json();
      Tours flowObj1 = new Tours();
      //End - List of used flows in the runlogic

      public void execute(NSApi nsApi) throws Exception
      {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence blocik - Block1
        //Executing flow - XML_Json
        flowObj0.execute(nsApi);

        //Executing flow - Tours
        flowObj1.execute(nsApi);

        //logging
        nsApi.ns_end_session();

      }
}