/*-----------------------------------------------------------------------------
    Name: flow2
    Recorded By: cavisson
    Date of recording: 05/21/2021 01:49:32
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.script_kohls_checkProxy;
import pacJnvmApi.NSApi;

public class flow2 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_112");
        status = nsApi.ns_web_url ("index_112",
            "URL=http://www.kohls.com/",
            "REDIRECT=YES",
            "LOCATION=https://www.kohls.com/",
            INLINE_URLS,
                "URL=https://www.kohls.com/", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environ3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/newHomepagestyle.css", END_INLINE,
                "URL=https://www.kohls.com/akam/11/7d34ecfc", END_INLINE,
                "URL=https://www.kohls.com/media/images/global-header-refresh-icons/order-status-icon.png", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/images/global-header-refresh-icons/search-icon.svg", END_INLINE,
                "URL=https://s.go-mpulse.net/boomerang/4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-kc-bkg02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/td-d-20210504-default-rewards-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_1", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.default.theme.min.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/gh-test-shoppingcart?scl=1&fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_3", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_3", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210519-lps-hero_4", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210519-lps-hero_4", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-lps-rail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r1_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn10?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-kcash-bug_earn20?scl=1&fmt=png8-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-LPS-offer-r2_05", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_06", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_07", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_08", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-shoprail_09", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-dads-rail-bkg1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210519-dads-split_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210517-golf-bkg", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/20190729-hp-od-kcash-offer-bg?fmt=png-alpha", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210401-beauty-half", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-bopus-utility", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-bopus-utility", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-2020w1226-rewardsplate", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210506-social2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-m-20210506-social2", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-kck", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/AppStore-qr_code2020?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_01", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_02", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_03", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210517-brandrail_04", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-dm-20210506-outfitbar_hero", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp2-d-20210405-kck-top", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/hp-dt-e12-20190408-leaf?fmt=png8&scl=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_112", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_4");
        status = nsApi.ns_web_url ("config_json_4",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405283&v=1.720.0&if=&sl=0&si=d05f114a-1bcc-4add-a812-a64ac6b6e55b-qtg6zy&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159"
        );

        status = nsApi.ns_end_transaction("config_json_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("pixel_7d34ecfc");
        status = nsApi.ns_web_url ("pixel_7d34ecfc",
            "URL=https://www.kohls.com/akam/11/pixel_7d34ecfc",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("pixel_7d34ecfc", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_32");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_32",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_32", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_33");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_33",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_33", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_34");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_34",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_34", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_5");
        status = nsApi.ns_web_url ("mobilemenu_json_5",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://dpm.demdex.net/id?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621585011747", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/id/rd?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621585011747", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rd_2");
        status = nsApi.ns_web_url ("rd_2",
            "URL=https://dpm.demdex.net/id/rd?d_visid_ver=5.2.0&d_fieldgroup=MC&d_rtbd=json&d_ver=2&d_verify=1&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&ts=1621585011747",
            INLINE_URLS,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE,
                "URL=https://ww8.kohls.com/id?d_visid_ver=5.2.0&d_fieldgroup=A&mcorgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&mid=89552655867376930334316022663672800555&ts=1621585012020", END_INLINE
        );

        status = nsApi.ns_end_transaction("rd_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("delivery_6");
        status = nsApi.ns_web_url ("delivery_6",
            "URL=https://kohls.tt.omtrdc.net/rest/v1/delivery?client=kohls&sessionId=d9ac3f9e28844074a6efd431d5a469f3&version=2.5.0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_dynamic.js", END_INLINE,
                "URL=https://cdn.dynamicyield.com/api/8776374/api_static.js", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=https://aa.agkn.com/adscores/g.pixel?sid=9211132908&aam=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://cm.everesttech.net/cm/dd?d_uuid=84622417935889388173660587809767566339", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdsdQAAAFxEsCaq", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=21&dpuuid=164570703793000811338", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=411&dpuuid=YKdsdQAAAFxEsCaq", END_INLINE,
                "URL=https://idsync.rlcdn.com/365868.gif?partner_uid=84622417935889388173660587809767566339", END_INLINE
        );

        status = nsApi.ns_end_transaction("delivery_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_6");
        status = nsApi.ns_web_url ("session_jsp_6",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm&gdpr=0&gdpr_consent=&google_hm=ODQ2MjI0MTc5MzU4ODkzODgxNzM2NjA1ODc4MDk3Njc1NjYzMzk=", END_INLINE,
                "URL=https://idsync.rlcdn.com/1000.gif?memo=CKyqFhIxCi0IARCYEhomODQ2MjI0MTc5MzU4ODkzODgxNzM2NjA1ODc4MDk3Njc1NjYzMzkQABoNCPXYnYUGEgUI6AcQAEIASgA", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=477&dpuuid=2fc4a3a5c3b917c9825e1ef7ae2a7e9d21a9c1d2f32eaa7aea5b5e32fd0f4321b0da87c991749652", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=adobe_dmp&google_cm=&gdpr=0&gdpr_consent=&google_hm=ODQ2MjI0MTc5MzU4ODkzODgxNzM2NjA1ODc4MDk3Njc1NjYzMzk=&google_tc=", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=771&dpuuid=CAESEBZs5gmJdXa9oNchbmFHckw&google_cver=1?gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://navdmp.com/req?adID=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://dp2.33across.com/ps/?pid=897&random=1247778806", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=601&dpuuid=116994383386967&random=1621585014", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.everesttech.net%2F1x1%3F", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537072980%26val%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cdn.navdmp.com/req?adID=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fib.adnxs.com%2Fpxj%3Faction%3Dsetuid(%27__EFGSURFER__.__EFGCK__%27)%26bidder%3D51%26seg%3D2634060der%3D51%26seg%3D2634060", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fexpires%3D30%26nid%3D2181%26put%3D__EFGSURFER__.__EFGCK__%26v%3D11782", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.everesttech.net%252F1x1%253F&google_gid=CAESEP54vRti3gpuJmORHJTZrwE&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782", END_INLINE,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=0&jsession=&ref=&scriptVersion=1.11.2&dyid_server=Dynamic%20Yield&ctx=%7B%22type%22%3A%22HOMEPAGE%22%7D", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fus-u.openx.net%252Fw%252F1.0%252Fsd%253Fid%253D537072980%2526val%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEP54vRti3gpuJmORHJTZrwE&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cdn.dynamicyield.com/scripts/1.11.2/dy-coll-nojq-min.js", END_INLINE,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp?isHome=true", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fib.adnxs.com%252Fpxj%253Faction%253Dsetuid(%2527__EFGSURFER__.__EFGCK__%2527)%2526bidder%253D51%2526seg%253D2634060der%253D51%2526seg%253D2634060&google_gid=CAESEP54vRti3gpuJmORHJTZrwE&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1/gr?url=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%3D%26piggybackCookie%3D__EFGSURFER__.__EFGCK__", "REDIRECT=YES", "LOCATION=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fpixel.rubiconproject.com%252Ftap.php%253Fexpires%253D30%2526nid%253D2181%2526put%253D__EFGSURFER__.__EFGCK__%2526v%253D11782&google_gid=CAESEP54vRti3gpuJmORHJTZrwE&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=everest&google_cm&google_sc&ev_rs=1&google_hm=WUtkc2RRQUFBRnhFc0NhcQ&url=/1/gr%3furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://idpix.media6degrees.com/orbserv/hbpix?pixId=16873&pcv=70&ptid=66&tpuv=01&tpu=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmb/generic?ttd_pid=aam&gdpr=0&gdpr_consent=&ttd_tpi=1", END_INLINE,
                "URL=https://cm.everesttech.net/cm/ax?cookieid=&ev_rs=1&url=/1/gr%3Furl=https%253A%252F%252Fimage2.pubmatic.com%252FAdServer%252FPug%253Fvcode%253Dbz0yJnR5cGU9MSZjb2RlPTI2NjgmdGw9NDMyMDA%253D%2526piggybackCookie%253D__EFGSURFER__.__EFGCK__&google_gid=CAESEP54vRti3gpuJmORHJTZrwE&google_cver=1", "REDIRECT=YES", "LOCATION=https://pixel.everesttech.net/1x1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=903&dpuuid=e54a98e6-19ed-4f55-873b-8d02b6eb0282", END_INLINE,
                "URL=https://pixel.everesttech.net/1x1", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities_2");
        status = nsApi.ns_web_url ("userAffinities_2",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=-2688648636616184714",
            INLINE_URLS,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=867481&msn=webserve-ad97e76.use&uid=-2688648636616184714&sec=8776374&t=ri&e=1096558&p=1&ve=10092862&va=%5B26047526%5D&ses=286b9c143f91ee140b395f32e68da38f&expSes=35310&aud=1408117.1362540.1362542&expVisitId=-3388314025234332241&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621585014297&rri=4419868", END_INLINE,
                "URL=https://async-px.dynamicyield.com/id?cnst=1&msn=webserve-ad97e76.use&uid=-2688648636616184714&sec=8776374&cuid=&cuidType=atg_id&reqts=1621585014269&rri=9841642&_=1621585014290", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=494051&msn=webserve-ad97e76.use&uid=-2688648636616184714&sec=8776374&t=ri&e=1100747&p=1&ve=10123479&va=%5B26082590%5D&ses=286b9c143f91ee140b395f32e68da38f&expSes=35310&aud=1408117.1362540.1362542&expVisitId=-3388314027303639923&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621585014405&rri=4144416", END_INLINE,
                "URL=https://async-px.dynamicyield.com/var?cnst=1&_=444644&msn=webserve-ad97e76.use&uid=-2688648636616184714&sec=8776374&t=ri&e=1102794&p=1&ve=10133783&va=%5B26094881%5D&ses=286b9c143f91ee140b395f32e68da38f&expSes=35310&aud=1408117.1362540.1362542&expVisitId=-3388314025051653141&mech=1&smech=null&eri=1&tsrc=Direct&reqts=1621585014409&rri=9261022", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_7");
        status = nsApi.ns_web_url ("uia_7",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621585014291",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_8");
        status = nsApi.ns_web_url ("batch_8",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621585014401_891875",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://cms.analytics.yahoo.com/cms?partner_id=ADOBE&_hosted_id=84622417935889388173660587809767566339&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://servedby.flashtalking.com/map/?key=a74thHgsfK627J6Ftt8sj5ks52bKe&gdpr=0&gdpr_consent=&url=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=[%FT_GUID%]&gdpr=0&gdpr_consent=", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=488960363EDB50&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=30646?dpuuid=y-w5Dt10VE2pF5T0z0lE1cxLWUL0bOKGnKDIw-~A", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("userAffinities_3");
        status = nsApi.ns_web_url ("userAffinities_3",
            "URL=https://rcom.dynamicyield.com/userAffinities?limit=10&sec=8776374&uid=-2688648636616184714",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=3047&dpuuid=488960363EDB50&gdpr=0&gdpr_consent=", END_INLINE,
                "URL=https://fei.pro-market.net/engine?site=141472;size=1x1;mimetype=img;du=67;csync=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.min.js", END_INLINE,
                "URL=https://px.owneriq.net/eucm/p/adpq?redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D53196%26dpuuid%3D(OIQ_UUID)", "REDIRECT=YES", "LOCATION=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748714151923604758&uid=Q6748714151923604758&ref=%2Feucm%2Fp%2Fadpq", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCe9bf84b227de49e19116aff6da1d63ec-source.min.js", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/cav_nv.js?v=REL20170123", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=575&dpuuid=6328123965509947043", END_INLINE,
                "URL=https://px.owneriq.net/ecc?redir=https%3a%2f%2fdpm.demdex.net%2fibs%3adpid%3d53196%26dpuuid%3dQ6748714151923604758&uid=Q6748714151923604758&ref=%2Feucm%2Fp%2Fadpq", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748714151923604758", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC45c202be87f24f4b869276b8ad2213dd-source.min.js", END_INLINE,
                "URL=https://ps.eyeota.net/match?bid=6j5b2cv&uid=84622417935889388173660587809767566339&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=/match/bounce/?bid=6j5b2cv&uid=84622417935889388173660587809767566339&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=53196&dpuuid=Q6748714151923604758", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/dotcom/config.js?v=REL20170123", END_INLINE,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://ps.eyeota.net/match/bounce/?bid=6j5b2cv&uid=84622417935889388173660587809767566339&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D30064%26dpuuid%3D%7BUUID_6j5b2cv%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2nPMPp-jcd6nhJVn7k8E2UvS1nv0ck6rsOaXfkJJwlIc", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC7d3594a211b44b26a76f4645d4b39315-source.min.js", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=30064&dpuuid=2nPMPp-jcd6nhJVn7k8E2UvS1nv0ck6rsOaXfkJJwlIc", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf6d45831b2294615923f29a2042b366f-source.min.js", END_INLINE,
                "URL=https://servedby.flashtalking.com/container/1638;12462;1480;iframe/?spotName=Homepage&U3=89552655867376930334316022663672800555&U7=d64ec31a-62cb-4949-954e-5859e4a2f695&U10=N/A&cachebuster=31417.515679192707", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC6e9a66d466e14e7595ac7a2d79c6ca6d-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8f39aead6ce84f38a17325c1cde458bc-source.min.js", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=https://d.impactradius-event.com/A375953-1cd4-4523-a263-b5b3c8c11fb81.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F", END_INLINE,
                "URL=https://ads.scorecardresearch.com/p?c1=9&c2=6034944&c3=2&cs_xi=84622417935889388173660587809767566339&rn=1621585012619&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D84622417935889388173660587809767566339", END_INLINE
        );

        status = nsApi.ns_end_transaction("userAffinities_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_6");
        status = nsApi.ns_web_url ("X349_6",
            "URL=https://kohls.sjv.io/xc/385561/362119/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ads.scorecardresearch.com/p2?c1=9&c2=6034944&c3=2&cs_xi=84622417935889388173660587809767566339&rn=1621585012619&r=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D73426%26dpuuid%3D84622417935889388173660587809767566339", END_INLINE,
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15", "METHOD=OPTIONS", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621585015809&mi_u=anon-1621585015807-2168751633&mi_cid=8212&page_title=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&timezone_offset=-330&event_type=pageview&cdate=1621585015807&ck=false&anon=true", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=73426&dpuuid=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://idsync.rlcdn.com/422866.gif?partner_uid=488960363EDB50&", END_INLINE,
                "URL=https://d9.flashtalking.com/d9core", END_INLINE,
                "URL=https://bttrack.com/dmp/adobe/user?dd_uuid=84622417935889388173660587809767566339", "REDIRECT=YES", "LOCATION=//dpm.demdex.net/ibs:dpid=49276&dpuuid=35cd19e3-2b1f-49c4-8b99-42a0dda8fdba", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=https://servedby.flashtalking.com/segment/modify/xh0;;pixel/?name=Homepage_2019", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=49276&dpuuid=35cd19e3-2b1f-49c4-8b99-42a0dda8fdba", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s69524755362955?AQB=1&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A46%3A54%205%20-330&d.&nsid=0&jsonv=1&.d&fid=2720B244721C920B-0ADF8DB34CC7F370&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-false&mcidcto=-false&aidcto=-false&.mcid&.c&cc=USD&pageType=homepage&c4=homepage&c9=homepage%7Chomepage&c10=homepage&c11=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=homepage&c64=VisitorAPI%20Present&v68=homepage&v70=d64ec31a-62cb-4949-954e-5859e4a2f695&v71=klsbrwcki%3Ad64ec31a-62cb-4949-954e-5859e4a2f695&v86=68&v87=hp19&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s69524755362955?AQB=1&pccr=true&vidn=3053B63CFBE99B85-4000109CD0C6191D&ndh=1&pf=1&callback=s_c_il[0].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A46%3A54%205%20-330&d.&nsid=0&jsonv=1&.d&fid=2720B244721C920B-0ADF8DB34CC7F370&ce=UTF-8&ns=kohls&pageName=homepage&g=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=5.2.0&icsmcvid=-false&mcidcto=-false&aidcto=-false&.mcid&.c&cc=USD&pageType=homepage&c4=homepage&c9=homepage%7Chomepage&c10=homepage&c11=homepage&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v40=cloud17&v42=no%20cart&c50=D%3Ds_tempsess&c53=homepage&c64=VisitorAPI%20Present&v68=homepage&v70=d64ec31a-62cb-4949-954e-5859e4a2f695&v71=klsbrwcki%3Ad64ec31a-62cb-4949-954e-5859e4a2f695&v86=68&v87=hp19&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("lgc_6");
        status = nsApi.ns_web_url ("lgc_6",
            "URL=https://d9.flashtalking.com/lgc",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("lgc_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("experiences_7");
        status = nsApi.ns_web_url ("experiences_7",
            "URL=https://api-bd.kohls.com/v1/ede/experiences?cid=WebStore&pgid=Home&plids=RedesignHP1%7C15%2CRedesignHP2%7C15",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://sync-tm.everesttech.net/upi/pid/5w3jqr4k?redir=https%3A%2F%2Fcm.g.doubleclick.net%2Fpixel%3Fgoogle_nid%3Dg8f47s39e399f3fe%26google_push%26google_sc%26google_hm%3D%24%7BTM_USER_ID_BASE64ENC_URLENC%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/h0r58thg?redir=https%3A%2F%2Fsync.search.spotxchange.com%2Fpartner%3Fadv_id%3D6409%26uid%3D%24%7BUSER_ID%7D%26img%3D1", END_INLINE,
                "URL=https://api-bd.kohls.com/v1/ecs/correlation/id", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/c=9828/tp=ADBE/tpid=84622417935889388173660587809767566339?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/UH6TUt9n?redir=https%3A%2F%2Fib.adnxs.com%2Fsetuid%3Fentity%3D158%26code%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ZMAwryCI?redir=https%3A%2F%2Fdsum-sec.casalemedia.com%2Frum%3Fcm_dsp_id%3D88%26external_user_id%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/btu4jd3a?redir=https%3A%2F%2Fpixel.rubiconproject.com%2Ftap.php%3Fv%3D7941%26nid%3D2243%26put%3D%24%7BUSER_ID%7D%26expires%3D90", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/b9pj45k4?redir=https%3A%2F%2Fimage2.pubmatic.com%2FAdServer%2FPug%3Fvcode%3Dbz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA%3D%26piggybackCookie%3D%24%7BUSER_ID%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/ny75r2x0?redir=https%3A%2F%2Fus-u.openx.net%2Fw%2F1.0%2Fsd%3Fid%3D537148856%26val%3D%24%7BTM_USER_ID%7D", END_INLINE,
                "URL=https://sync-tm.everesttech.net/upi/pid/r7ifn0SL?redir=https%3A%2F%2Fwww.facebook.com%2Ffr%2Fb.php%3Fp%3D1531105787105294%26e%3D%24%7BTM_USER_ID%7D%26t%3D2592000%26o%3D0", END_INLINE,
                "URL=https://usermatch.krxd.net/um/v2?partner=adobe&id=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://sync.crwdcntrl.net/map/ct=y/c=9828/tp=ADBE/tpid=84622417935889388173660587809767566339?https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D121998%26dpuuid%3D${profile_id}", END_INLINE,
                "URL=https://abp.mxptint.net/sn.ashx", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B341_DD6A8215_270A2233D&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=121998&dpuuid=27b56a9f8bf46f63b0f27c92939ef79d", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?id=537148856&val=YKdsdQAAAFxEsCaq", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2F&H=1lm7uu8", END_INLINE,
                "URL=https://aorta.clickagy.com/pixel.gif?ch=124&cm=84622417935889388173660587809767566339&redir=https%3A%2F%2Fdpm.demdex.net%2Fibs%3Adpid%3D79908%26dpuuid%3D%7Bvisitor_id%7D", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=75557&dpuuid=R1B341_DD6A8215_270A2233D&redir=https://abp.mxptint.net/sn.ashx?ak=1", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1?deviceId=3fdb02a9-4a8e-4711-cc24-4010cd10d0b1&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?google_nid=g8f47s39e399f3fe&google_push&google_sc&google_hm=WUtkc2RRQUFBRnhFc0NhcQ==", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=79908&dpuuid=c:85e91ca659eb840745c990f2c7919770", END_INLINE,
                "URL=https://image2.pubmatic.com/AdServer/Pug?vcode=bz0yJnR5cGU9MSZqcz0xJmNvZGU9MjE5MSZ0bD0yNTkyMDA=&piggybackCookie=YKdsdQAAAFxEsCaq", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdsdQAAAFxEsCaq&img=1", "REDIRECT=YES", "LOCATION=/partner?adv_id=6409&uid=YKdsdQAAAFxEsCaq&img=1&__user_check__=1&sync_id=e8fc1f8c-ba0c-11eb-930d-168130400507", END_INLINE,
                "URL=https://us-u.openx.net/w/1.0/sd?cc=1&id=537148856&val=YKdsdQAAAFxEsCaq", END_INLINE,
                "URL=https://i.flashtalking.com/ft/?aid=1638&uid=D9:1f1c1ea80b0e48d388d36c2aac50d6f9&seg=xh0", END_INLINE,
                "URL=https://sync.ipredictive.com/d/sync/cookie/generic?https://dpm.demdex.net/ibs:dpid=2340&dpuuid=${ADELPHIC_CUID}", "REDIRECT=YES", "LOCATION=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=e8f6bbee-ba0c-11eb-bc99-218a09dc990d", END_INLINE,
                "URL=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdsdQAAAFxEsCaq", "REDIRECT=YES", "LOCATION=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdsdQAAAFxEsCaq&C=1", END_INLINE,
                "URL=https://sync.search.spotxchange.com/partner?adv_id=6409&uid=YKdsdQAAAFxEsCaq&img=1&__user_check__=1&sync_id=e8fc1f8c-ba0c-11eb-930d-168130400507", END_INLINE,
                "URL=https://pixel.rubiconproject.com/tap.php?v=7941&nid=2243&put=YKdsdQAAAFxEsCaq&expires=90", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=2340&dpuuid=e8f6bbee-ba0c-11eb-bc99-218a09dc990d", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/118811_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3955549?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3874658?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047073_Rose_Dye?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4462500?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4528551_Empire_Red?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3047068_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3500577_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4525714_Black?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/3509904_New_White?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4777679_Multi_Color_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/4887344_Yellow_Floral?wid=300&hei=300&op_sharpen=1", END_INLINE,
                "URL=https://dsum-sec.casalemedia.com/rum?cm_dsp_id=88&external_user_id=YKdsdQAAAFxEsCaq&C=1", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE
        );

        status = nsApi.ns_end_transaction("experiences_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_19");
        status = nsApi.ns_web_url ("floop_19",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_19", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_20");
        status = nsApi.ns_web_url ("floop_20",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_20", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_21");
        status = nsApi.ns_web_url ("floop_21",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("floop_21", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_22");
        status = nsApi.ns_web_url ("floop_22",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://beacon.krxd.net/usermatch.gif?kuid_status=new&partner=adobe&id=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://mid.rkdms.com/bct?pid=8bc436aa-e0fc-4baa-9c9a-06fbeca87826&puid=84622417935889388173660587809767566339&_ct=img", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connect/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1?deviceId=3fdb02a9-4a8e-4711-cc24-4010cd10d0b1&os=html5&devicetype=desktop&loadConfig", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE,
                "URL=https://dpm.demdex.net/ibs:dpid=129099&dpuuid=a62186e477e7c8816c7ab52ec28b8769", END_INLINE,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE,
                "URL=https://mpp.vindicosuite.com/sync/?pid=27&fr=1", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=https://www.googleadservices.com/pagead/conversion_async.js", END_INLINE,
                "URL=https://www.googletagservices.com/tag/js/gpt.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_22", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("rid_2");
        status = nsApi.ns_web_url ("rid_2",
            "URL=https://match.adsrvr.org/track/rid?ttd_pid=casale&fmt=json&p=184399",
            INLINE_URLS,
                "URL=https://api.rlcdn.com/api/identity?pid=2&rt=envelope", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621585018357&cv=9&fst=1621585018357&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621585018343&cv=9&fst=1621585018343&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gpt/pubads_impl_2021051801.js?31061226", END_INLINE,
                "URL=https://6249496.collect.igodigital.com/collect.js", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621585018343&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2623671547&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621585018357&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3338888542&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621585018357&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=3338888542&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621585018343&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059651&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=2&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2F&tiba=Kohl%27s%20%7C%20Shop%20Clothing%2C%20Shoes%2C%20Home%2C%20Kitchen%2C%20Bedding%2C%20Toys%20%26%20More&async=1&fmt=3&is_vtc=1&random=2623671547&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("rid_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_35");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_35",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_35", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_113");
        status = nsApi.ns_web_url ("index_113",
            "URL=https://hb.emxdgt.com/?t=1000&ts=1621585018945",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_113", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_7");
        status = nsApi.ns_web_url ("bidRequest_7",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_middle&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_8");
        status = nsApi.ns_web_url ("bidRequest_8",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_9");
        status = nsApi.ns_web_url ("bidRequest_9",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_10");
        status = nsApi.ns_web_url ("bidRequest_10",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_bottom_728x90&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_10", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_7");
        status = nsApi.ns_web_url ("fastlane_json_7",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=2&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1817898&kw=rp.fastlane&tk_flint=index&rand=0.630761159554212",
            INLINE_URLS,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A31947446%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%228%22%2C%22siteID%22%3A%22269247%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%223%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%227%22%2C%22siteID%22%3A%22269241%22%7D%2C%22id%22%3A%224%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%22e54a98e6-19ed-4f55-873b-8d02b6eb0282%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T08%3A16%3A59%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_7");
        status = nsApi.ns_web_url ("auction_7",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_8");
        status = nsApi.ns_web_url ("fastlane_json_8",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811816&kw=rp.fastlane&tk_flint=index&rand=0.544816557991838",
            INLINE_URLS,
                "URL=https://idx.liadm.com/idex/ie/any", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_9");
        status = nsApi.ns_web_url ("fastlane_json_9",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811822&kw=rp.fastlane&tk_flint=index&rand=0.4428443441892558"
        );

        status = nsApi.ns_end_transaction("fastlane_json_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_10");
        status = nsApi.ns_web_url ("fastlane_json_10",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2F&p_screen_res=1366x768&site_id=343126&zone_id=1811818&kw=rp.fastlane&tk_flint=index&rand=0.5615697363876921"
        );

        status = nsApi.ns_end_transaction("fastlane_json_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_3");
        status = nsApi.ns_web_url ("translator_3",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("translator_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_8");
        status = nsApi.ns_web_url ("auction_8",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_9");
        status = nsApi.ns_web_url ("auction_9",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000"
        );

        status = nsApi.ns_end_transaction("auction_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_10");
        status = nsApi.ns_web_url ("auction_10",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_bottom_leaderboard_header&lib=ix&size=728x90&referrer=https%3A%2F%2Fwww.kohls.com%2F&v=2.1.2&tmax=1000",
            INLINE_URLS,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("headerstats");
        status = nsApi.ns_web_url ("headerstats",
            "URL=https://as-sec.casalemedia.com/headerstats?s=186355&u=https%3A%2F%2Fwww.kohls.com%2F&v=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_nanLt1eQ%22%2C%22site%22%3A%7B%22domain%22%3A%22www.kohls.com%22%2C%22page%22%3A%22%2F%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22WhdHF9HV%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788162%22%7D%2C%7B%22id%22%3A%22QlqS4pB5%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788161%22%7D%2C%7B%22id%22%3A%22EK6MUmPj%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788160%22%7D%2C%7B%22id%22%3A%22yvaCorcG%22%2C%22banner%22%3A%7B%22w%22%3A728%2C%22h%22%3A90%7D%2C%22tagid%22%3A%22788159%22%7D%5D%7D", END_INLINE,
                "URL=https://1a51f1d97ca8f397187fa9a4ef61e0b3.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=2326719547852567&correlator=2557675476714975&output=ldjh&impl=fifs&eid=31060790%2C31061040%2C31061215%2C31061226%2C31061270&vrg=2021051801&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Chomepage&enc_prev_ius=%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1&prev_iu_szs=1x1%2C320x50%7C1024x256%2C970x250%2C728x90%2C300x250%2C300x250%2C300x250%2C320x50%7C1440x630%2C320x50%7C1440x630&fluid=0%2Cheight%2C0%2C0%2C0%2C0%2C0%2Cheight%2Cheight&ists=256&prev_scp=pos%3Dwallpaper%7C%7Cpos%3Dbottom%7Cpos%3Dbottom%7Cpos%3Dbottomleft%7Cpos%3Dbottommiddle%7Cpos%3Dbottomright%7Cpos%3DHP_Desktop_Top_Super_Bulletin%7Cpos%3DHP_Desktop_Bottom_Super_Bulletin&cust_params=pgtype%3Dhome%26channel%3Ddesktop%26env%3Dprod&cookie_enabled=1&bc=31&abxe=1&lmt=1621585019&dt=1621585019972&dlt=1621585007651&idt=11225&frm=20&biw=1349&bih=607&oid=3&adxs=-9%2C-9%2C190%2C311%2C163%2C525%2C887%2C0%2C0&adys=-9%2C-9%2C8193%2C9533%2C9575%2C9575%2C9575%2C2549%2C5490&adks=1341520166%2C1971407845%2C2060426818%2C1304082129%2C4151307844%2C3063740786%2C4276857337%2C3603917613%2C1454221005&ucis=1%7C2%7C3%7C4%7C5%7C6%7C7%7C8%7C9&ifi=1&u_tz=330&u_his=2&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2F&vis=1&dmc=8&scr_x=0&scr_y=0&psz=0x-1%7C0x-1%7C1349x9298%7C1349x9298%7C300x54%7C300x54%7C300x54%7C1349x0%7C1349x9298&msz=0x-1%7C0x-1%7C1349x0%7C1349x0%7C300x0%7C300x0%7C300x0%7C1440x0%7C1440x0&ga_vid=2079472488.1621585020&ga_sid=1621585020&ga_hid=923275651&ga_fc=false&fws=2%2C2%2C0%2C0%2C0%2C0%2C0%2C128%2C128&ohw=0%2C0%2C0%2C0%2C0%2C0%2C0%2C0%2C0&btvi=-1%7C-1%7C1%7C2%7C3%7C4%7C5%7C6%7C7", END_INLINE,
                "URL=https://www.kohls.com/typeahead/jeans.jsp?callback=categoryGenderTypeaheadResult&ta_exp=d&svid=%5BCS%5Dv1%7C3053B63CFBE99B85-4000109CD0C6191D%5BCE%5D&_=1621585011480", END_INLINE,
                "URL=https://tpc.googlesyndication.com/pagead/js/r20210517/r20110914/client/window_focus.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/15280555526910074681", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/14906849826852343313", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/16953867992244551473", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvY-5qS3OHUCD5Iq-g2u9xDlvQgXgt52Nck0b8nZDs0LKamXPf850gJTiSJ0D6QOBZcWxuGrERpB4imxQgXB6x0a6hJstCIfRz1iITcBwRb86IfovE__wiwxtZWjLjcGt1AwSaH7fq9kyUyiiUD5mdMT3FXmg8qHFTDU6D1pIapv8LcGiS_4eYGWTgekamZ5JAAsb0GY5YsJa-bPntoQbOAPu4u6yDQdiytMCTVw3P7o542yjsUUywmFSStBGLNc3ahQow09ODcZPCVkZh9iCrqYi-u0mnH&sig=Cg0ArKJSzBVBRQh1LFetEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsu8XzYCc-J6nprM767F8GvZwpXZgcc2AGdg6_P0RyuSdv3GLCNZ7S2f4Np86RobqzbYusiR2ZbjamnfPLnbR3-PUZFXjBjJJXYxpgr7iqYBA-fSOPJEIUQ_xl4S-bdbBzu8S7kE0MZSLet2_wqnT-kKdXKp0319SMaghgO03kzq_rrHAlwK8OZtFrlR2y07fYEGqo0zI9XUWB8QrM7zsitx6uKvrzL4U7fXXISrI-mKwIe_Kkqh3wTne0Xynzq9pcTyz8GoohLNbyIhMtVqPQYxlwILNI-R&sig=Cg0ArKJSzEyIk4NwEUq8EAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstyfmoxXPSMwrwa3J4eNOkPPW9jMKMmvXnbPfZk6Tk8dZjE2ElCz5Eld7uOUCjhUBAavyEp2VdPW9483OxoWs0WWh-UjNjLwIVEe1ShA-tPscwKL-00F_5tZQCPhyHHhyPvr_uJxB5vtx2XD4wcg3HOwXjN8w6LzEj0wYZYTuoWfwpgoW8pzVWrFSKHaRWzcsK7sZnFwkjTPW41Rj12TClmnxe-NkCEcQQiRARfy-b-IUL22Z_L0lPB-XJ98RaWW_Xz3lzkNa5Z4zWDodK2nF8DkiWWvwow&sig=Cg0ArKJSzAJc8-Z2DoqgEAE&adurl=", END_INLINE,
                "URL=https://www.kohls.com/search.jsp?submit-search=web-regular&search=jeans&kls_sbp=89552655867376930334316022663672800555", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaRh-K8kz1yOu3rr-8E_sNGDNX4177FPVllxkFJnzmka9kFblqFhEfASdPfgknWVnHIHdCWSPf143ZM1iOemv7QD_RI9eg", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaTM7s1UTjosTDtycS9EAF4HxfMMH-6wFqRRvMt-efypVTc3BKGIWRdHhpbrUVomVdSakXuy8q6I8nc833yjkjcvWDv38w", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/rx_lidar.js?cache=r20110914", END_INLINE,
                "URL=https://www.googletagservices.com/activeview/js/current/osd.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("headerstats", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_36");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_36",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssL13PtAVC1XHmDionjaQx55NeiwueyaKx_jDGR8JrP92i0uTE1f5yPd1cl3myGVaKfd7ecIesrh0S7KwywEOBmM-9rnkwLNFwk9vjFeZ1QmOomCYup68AktQdCqRpJkTwmVUQRuXPw6tbMp28-mDD6t3f5arHAFetfA4xH62jcJ91ztFYN3HOrLapscxSWd1Fb3lr1lH4pjXLj1f-e5v4wJT4j0_0LYaCbhVpmHsKiJORs0neavEd2Nlw7AFlH8NVMpTrvcavqwhHLfr-4K2eeeXNvmBtfywY&sig=Cg0ArKJSzIX_Dn1-4GFDEAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsswrHruqjRbDsLeh3F09FvPqE4TGxCPfJThWfUDA57WZPo0V_oJshyQf7ZcYLFLExTHSfpHfEY-wCiH3r6wSf-upyFUcuS7QD-7BMp4lYA_pEvSoFctPN2B_nWlABBduYXDoq0SuvkyTGrPrMpTHcdWRtomAqdBordURwKeFHoG5syiLEJOykY9rxQHKZTAG5YF74XMKDFuyJW1Hhu96Ine97KnvdASksIGG0gpxLpNhVb9WR4FTmITIFCAwE81fzW8xTgWNYt7kC1wkPR3apfwoMbLoknsj-s&sig=Cg0ArKJSzCkaAx-fuhg7EAE&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsukyMPgFSvQIVUJZpzRXLRJFh6cXVzt7OoPICmOtzsSYM8z4okhTUD52zFwwBkkYseV28wA1y8NrO-wLkkbK-cysdAscuNOUG_spWCUGOBclTsGoKu9i9K9NFNKxTv6-6DwE4bpNT9y5elhDvFPWlEI8mMcwftAB-1QnOYmhCNsdP8GxryMrJo5u_0hiwc1RVuXDO8qmkh-I4YU1_5zE0xl7C2ixgvb9pgs6GHeXoZoxZGxoG9BHFvJ2PC8UEH5E_XW7z4_za_dHwu5FeMrJ9HJVEDmEfcdyI0&sig=Cg0ArKJSzBCIZcTb98I8EAE&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Amobileapp&adcreativeid=138299836983&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=1255309708", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138260694346&adorderid=2153217288&adunitid=16769232&advertiserid=32614632&lineitemid=4439169351&rand=1369314460", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_36", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.163);

        //Page Auto split for Button 'web-regular' Clicked by User
        status = nsApi.ns_start_transaction("D035B020EFEA57B6E2D10B7D1877");
        status = nsApi.ns_web_url ("D035B020EFEA57B6E2D10B7D1877",
            "URL=http://localhost:9222/devtools/inspector.html?ws=localhost:9222/devtools/page/D035B020EFEA57B6E2D10B7D1877A4C8",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Sec-Fetch-User:?1",
            "HEADER=Sec-Fetch-Site:none",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/root.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/shell.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/devtools_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Runtime.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/platform/utilities.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/dom_extension/DOMExtension.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/common.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/host.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/protocol.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ui.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/services.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/workspace.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/bindings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/components.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/persistence.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/browser_sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/extensions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/console_counters.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/EventTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Object.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/UIString.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/App.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/sale-event/denim.jsp?&searchTerm=jeans&submit-search=web-regular", END_INLINE,
                "URL=http://localhost:9222/devtools/common/AppProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/CharacterIdMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Color.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Console.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/JavaScriptMetaData.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ParsedURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Progress.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/QueryParamHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ResourceType.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Revealer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Runnable.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/SegmentedRange.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Settings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/StaticContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/StringOutputStream.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/TextDictionary.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Throttler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Trie.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Worker.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHostAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHost.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/ResourceLoader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/UserMetrics.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/Platform.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SDKModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMetadata.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/SupportedCSSProperties.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Target.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ProfileTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkRequest.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.base.min.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage1.css", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RuntimeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ChildTargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CompilerSourceMappingContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Connections.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ConsoleModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/homepage.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/fonts/hfjFonts.css", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieParser.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfileDataModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMatchedStyles.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMedia.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/environment.js", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsu4mSgJvTQQTIPoj49K682fDY1M9q3cAj2DpFKqEA40PuJlHUnUrbgszKsviZceG5PEPRHor17gM5hMn7101cxiBlne1JdDEXuPZzipTNI&sig=Cg0ArKJSzMdf7Hd6WpgoEAE&id=lidartos&mcvt=0&p=9665,525,9915,825&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=3063740786&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621585020702&dlt=0&rpt=557&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSProperty.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSRule.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleDeclaration.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleSheetHeader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjst0ifd7sbjeO2BnzTtua08kj4LtLtHFrBcZ1qQ1VEXd3oMZ9CNBDBbN_K8cl4ebo8P5x91oR7-jWv7C4Iyskqsux0QtgWqtHzTukWZEYaM&sig=Cg0ArKJSzAq5hTvm55KDEAE&id=lidartos&mcvt=0&p=9501,311,9591,1039&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=1304082129&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621585020697&dlt=0&rpt=555&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsvaJIakfmf913gGH4oUcqLQCnduFZGpQ3-__q0Ox4V4It_BaJrvZGFrPB829g1vhATnORvueomahZLborDE9K5wCi34Ff5m6W_V7SwLJyw&sig=Cg0ArKJSzB7jfD7gNdHAEAE&id=lidartos&mcvt=0&p=9665,163,9915,463&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,607&mc=0&app=0&itpl=3&adk=4151307844&rs=4&met=mue&la=0&cr=0&osd=1&vs=3&rst=1621585020702&dlt=0&rpt=568&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/css/style.css", END_INLINE,
                "URL=https://widget.stylitics.com/v2/widget.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/ktag.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kjscore3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/skava-custom.css", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMDebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/EmulationModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/FilmStripModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HARLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HeapProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/IsolateManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LayerTreeBase.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LogModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/OverlayModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PaintProfiler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PerformanceMetricsModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RemoteObject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Resource.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ResourceTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ScreenCaptureModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Script.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SecurityOriginManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServerTiming.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerCacheModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMapManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TracingManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("D035B020EFEA57B6E2D10B7D1877", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("id_2");
        status = nsApi.ns_web_url ("id_2",
            "URL=https://dpm.demdex.net/id?d_visid_ver=4.3.0&d_fieldgroup=AAM&d_rtbd=json&d_ver=2&d_orgid=F0EF5E09512D2CD20A490D4D%40AdobeOrg&d_nsid=0&d_mid=89552655867376930334316022663672800555&d_blob=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&ts=1621585023925",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/sdk/TracingModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/InspectorBackend.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/NodeURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/InspectorBackendCommands.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Widget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/GlassPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Action.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionDelegate.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ARIAUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Context.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextFlavorListener.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://widget.stylitics.com/kohls-mnm-v2/js/main.js", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Dialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/DropTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/EmptyWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterBar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterSuggestionBuilder.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ForwardedInputEventHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Fragment.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Geometry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/HistoryInput.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Icon.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Infobar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InplaceEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InspectorView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/KeyboardShortcut.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListControl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Panel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/PopoverHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ProgressIndicator.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RemoteDebuggingTerminatedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ReportView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ResizerWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RootView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SearchableView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SegmentedButton.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-main?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SettingsUI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutsScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftDropDown.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SplitWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SuggestBox.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("id_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(20.098);

        status = nsApi.ns_start_transaction("denim_jsp");
        status = nsApi.ns_web_url ("denim_jsp",
            "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-boys?scl=1&fmt=png8",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/css/ipadcss.css", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-mens?scl=1&fmt=png8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SyntaxHighlighter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TabbedPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TargetCrashedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextPrompt.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ThrottledWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://kohls.demdex.net/dest5.html?d_nsid=0#https%3A%2F%2Fwww.kohls.com", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-girls?scl=1&fmt=png8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-08?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Toolbar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Tooltip.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Treeoutline.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/UIUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/View.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ViewManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-stylitics-womens?scl=1&fmt=png8", END_INLINE
        );

        status = nsApi.ns_end_transaction("denim_jsp", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("json_2");
        status = nsApi.ns_web_url ("json_2",
            "URL=https://kohls.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=d9ac3f9e28844074a6efd431d5a469f3&mboxPC=d9ac3f9e28844074a6efd431d5a469f3.31_0&mboxPage=ee20d526965d4f1798ba99975bf3ea49&mboxRid=83594b2167444a4a92731a1e5c82f9f4&mboxVersion=1.7.1&mboxCount=1&mboxTime=1621604823993&mboxHost=www.kohls.com&mboxURL=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&mboxReferrer=https%3A%2F%2Fwww.kohls.com%2F&browserHeight=607&browserWidth=1366&browserTimeOffset=330&screenHeight=768&screenWidth=1366&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=ANGLE%20(Intel(R)%20UHD%20Graphics%20620%20Direct3D11%20vs_5_0%20ps_5_0)&at_property=bb529821-b52b-bf89-2022-4492a94a6d05&customerLoggedStatus=false&tceIsRedesign=false&tceIsPDPRedesign=&tceIsCNCRedesign=True&mboxMCSDID=55E9EA00AF760282-4ACAC1466EB67F47&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=89552655867376930334316022663672800555&mboxAAMB=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&mboxMCGLH=12",
            INLINE_URLS,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-01?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-09?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-02?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-04?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-visnav-06?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-03?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-05?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-jeans-hero-panel-07?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-m?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XElement.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XLink.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ZoomManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/ServiceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/FileManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/UISourceCode.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/WorkspaceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/LiveLocation.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BlackboxManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BreakpointManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("json_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_23");
        status = nsApi.ns_web_url ("floop_23",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/bindings/CompilerScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ContentProviderBasedProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/CSSWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DebuggerWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DefaultScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/FileUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/NetworkProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/PresentationConsoleMessageHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/SASSSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/StylesSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/TempFile.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/DockController.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/ImagePreview.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/JSPresentationUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Reload.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://media.kohlsimg.com/is/image/kohls/denim-landing-page-dp-20200810-sourced-d?scl=1&fmt=pjpeg&qlt=80,1", END_INLINE,
                "URL=http://localhost:9222/devtools/components/TargetDetachedDialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PlatformFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/Automapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/EditFileSystemView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/FileSystemWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystemManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/NetworkPersistenceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceActions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/WorkspaceSettingsTab.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/LogManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionPanel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionServer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionTraceProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/WarningErrorCounter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/inspector.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/inline_editor/inline_editor_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/color_picker/color_picker_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/formatter/formatter_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/largeIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/foundation3.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/kohls_v1_m56577569839297458.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/s_code.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/navigationControls.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/object_ui/object_ui_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.view.html", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_23", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("mobilemenu_json_6");
        status = nsApi.ns_web_url ("mobilemenu_json_6",
            "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobilemenu.json",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/javascript/app/mobileheader/mobileaccount.view.html", END_INLINE,
                "URL=https://www.kohls.com/snb/media/css/tr_phase2_common.css", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/pb.module.js", END_INLINE,
                "URL=http://localhost:9222/devtools/event_listeners/event_listeners_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/help/help_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/elements/elements_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("mobilemenu_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_5");
        status = nsApi.ns_web_url ("config_json_5",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405283&v=1.720.0&if=&sl=1&si=3d6fe845-c12e-4034-9bd4-a136fdeb3d3c-qtg6zy&bcn=%2F%2F684d0d38.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159"
        );

        status = nsApi.ns_end_transaction("config_json_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_37");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_37",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/Images/touchCursor.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/sale-event/denim.jsp?&searchTerm=jeans&submit-search=web-regular", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_37", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_38");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_38",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE,
                "URL=http://localhost:9222/devtools/inspector.html?ws=localhost:9222/devtools/page/D035B020EFEA57B6E2D10B7D1877A4C8", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/root.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/shell.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/devtools_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/inspector.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Runtime.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/platform/utilities.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/dom_extension/DOMExtension.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/common.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/host.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/protocol.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ui.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/services.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/workspace.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/bindings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/components.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_38", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_7");
        status = nsApi.ns_web_url ("session_jsp_7",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=-2688648636616184714&jsession=ab65e81d522fdc88f15c6a7229406282&ref=https%3A%2F%2Fwww.kohls.com%2F&scriptVersion=1.11.2&dyid_server=-2688648636616184714", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/persistence.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/browser_sdk.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/extensions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/console_counters.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/EventTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Object.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/snb/media/R51/javascript/tmpl/tr/static_pb_drawerV2.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/menu/tpl.accountdropdown.js", END_INLINE,
                "URL=http://localhost:9222/devtools/common/UIString.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/App.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/AppProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/CharacterIdMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Color.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Console.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/JavaScriptMetaData.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ParsedURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Progress.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/QueryParamHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/ResourceType.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Revealer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Runnable.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/SegmentedRange.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Settings.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/StaticContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/StringOutputStream.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/TextDictionary.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Throttler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Trie.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/common/Worker.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHostAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/InspectorFrontendHost.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/ResourceLoader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/UserMetrics.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/host/Platform.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/InspectorBackend.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/protocol/NodeURL.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/InspectorBackendCommands.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/DockController.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/ImagePreview.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/JSPresentationUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Linkifier.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/Reload.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/components/TargetDetachedDialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SDKModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMetadata.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/SupportedCSSProperties.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Target.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ProfileTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkRequest.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RuntimeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ChildTargetManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CompilerSourceMappingContentProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Connections.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ConsoleModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CookieParser.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfileDataModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CPUProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMatchedStyles.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSMedia.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSProperty.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSRule.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleDeclaration.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/CSSStyleSheetHeader.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMDebuggerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/DOMModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/EmulationModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/FilmStripModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HARLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/HeapProfilerModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/IsolateManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LayerTreeBase.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/LogModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/NetworkLog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/OverlayModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PaintProfiler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/PerformanceMetricsModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/RemoteObject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Resource.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ResourceTreeModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ScreenCaptureModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/Script.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SecurityOriginManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServerTiming.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerCacheModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/ServiceWorkerManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123", END_INLINE,
                "URL=https://cdn.zineone.com/apps/latest/z1m.js", END_INLINE,
                "URL=https://cdns.brsrvr.com/v1/br-trk-5117.js", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMap.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/SourceMapManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TracingManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/sdk/TracingModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=https://s.btstatic.com/tag.js#site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Widget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/GlassPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Action.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionDelegate.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ActionRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ARIAUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Context.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextFlavorListener.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Dialog.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/DropTarget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/EmptyWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterBar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/FilterSuggestionBuilder.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ForwardedInputEventHandler.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Fragment.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Geometry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/HistoryInput.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Icon.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Infobar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InplaceEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/InspectorView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/KeyboardShortcut.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListControl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s62160179355955?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A47%3A8%205%20-330&d.&nsid=0&jsonv=1&.d&sdid=55E9EA00AF760282-4ACAC1466EB67F47&mid=89552655867376930334316022663672800555&aamlh=12&ce=UTF-8&ns=kohls&pageName=denim&g=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=sale%20event%20landing&events=event1&products=%3Bproductmerch1&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&tnt=546041%3A1%3A0%2C545299%3A1%3A0%2C531276%3A0%3A0%2C536856%3A1%3A0%2C536968%3A1%3A0%2C526083%3A0%3A0%2C&c1=no%20taxonomy&c2=no%20taxonomy&c3=no%20taxonomy&v3=browse&c4=sale%20event%20landing&c5=non-search&c7=no%20taxonomy&v8=non-search&v9=homepage&c16=browse&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=browse&v24=browse&v25=no%20taxonomy&v26=no%20taxonomy&v27=no%20taxonomy&v28=no%20taxonomy&c39=browse&c40=browse&v40=cloud17&c41=browse&c42=browse&v42=no%20cart&c50=D%3Ds_tempsess&c53=denim&c64=VisitorAPI%20Present&v68=denim&v70=d64ec31a-62cb-4949-954e-5859e4a2f695&v71=klsbrwcki%3Ad64ec31a-62cb-4949-954e-5859e4a2f695&c.&a.&activitymap.&page=homepage&link=web-regular&region=site-search&pageIDType=1&.activitymap&.a&.c&pid=homepage&pidt=1&oid=web-regular&oidt=3&ot=SUBMIT&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListModel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ListWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Panel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/PopoverHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ProgressIndicator.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RemoteDebuggingTerminatedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ReportView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ResizerWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/RootView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SearchableView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SegmentedButton.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SettingsUI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutRegistry.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ShortcutsScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftContextMenu.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SoftDropDown.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SplitWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_8");
        status = nsApi.ns_web_url ("uia_8",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621585028248",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_9");
        status = nsApi.ns_web_url ("batch_9",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621585028318_496897",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/ui/SuggestBox.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/SyntaxHighlighter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TabbedPane.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TargetCrashedScreen.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextEditor.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/TextPrompt.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ThrottledWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Toolbar.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Tooltip.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/Treeoutline.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/UIUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/View.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ViewManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XElement.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XLink.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/XWidget.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/ui/ZoomManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/services/ServiceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/FileManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/UISourceCode.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace/WorkspaceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/LiveLocation.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BlackboxManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/BreakpointManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/CompilerScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ContentProviderBasedProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/CSSWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connect/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1?deviceId=3fdb02a9-4a8e-4711-cc24-4010cd10d0b1&os=html5&devicetype=desktop&loadConfig", "METHOD=OPTIONS", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DebuggerWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/DefaultScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/FileUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/NetworkProject.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/PresentationConsoleMessageHelper.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC49eca514a17d4f4eb2269e814b3eb342-source.min.js", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceScriptMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/ResourceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/SASSSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/StylesSourceMapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/bindings/TempFile.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PlatformFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/Automapping.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/EditFileSystemView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/FileSystemWorkspaceBinding.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystem.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/IsolatedFileSystemManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/NetworkPersistenceManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceActions.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceImpl.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/PersistenceUtils.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/persistence/WorkspaceSettingsTab.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/browser_sdk/LogManager.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionAPI.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionPanel.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionServer.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionTraceProvider.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/extensions/ExtensionView.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/console_counters/WarningErrorCounter.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/checker.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_7");
        status = nsApi.ns_web_url ("X349_7",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE,
                "URL=http://localhost:9222/devtools/inline_editor/inline_editor_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/color_picker/color_picker_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/formatter/formatter_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/largeIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connect/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1?deviceId=3fdb02a9-4a8e-4711-cc24-4010cd10d0b1&os=html5&devicetype=desktop&loadConfig", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&docReferrer=https%3A%2F%2Fwww.kohls.com%2F&H=cajn1lv&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NDA2NzQ3MzE1NDkzNDAyOTEyNA&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MzIzMjgyNzcyNzM4MDM2MzA0MA", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-2195488", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=DC-8632166", END_INLINE,
                "URL=https://s.btstatic.com/lib/745abcebb4573a60dc1dc7f5d132864d1c23e738.js?v=2", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=2&dtm_email_hash=N/A&dtm_user_id=d64ec31a-62cb-4949-954e-5859e4a2f695&dtmc_department=clothing&dtmc_category=&dtmc_sub_category=not%20set", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1071871169&l=dataLayer&cx=c", END_INLINE,
                "URL=https://www.googletagmanager.com/gtag/js?id=AW-1018012790&l=dataLayer&cx=c", END_INLINE,
                "URL=https://s.btstatic.com/lib/c8c3096e256a91eaf614d7c9433aad0eb1322fcd.js?v=2", END_INLINE,
                "URL=https://login.dotomi.com/pixel.gif", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621585030061&mi_u=anon-1621585015807-2168751633&mi_cid=8212&referrer=https%3A%2F%2Fwww.kohls.com%2F&timezone_offset=-330&event_type=pageview&cdate=1621585030058&ck=host&anon=true", END_INLINE,
                "URL=https://js-sec.indexww.com/ht/p/184399-89471702884776.js", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/cm/cm_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/search/search_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/diff/diff_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/data_grid/data_grid_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://s.btstatic.com/btprivacy.js", END_INLINE,
                "URL=http://localhost:9222/devtools/har_importer/har_importer_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/workspace_diff/workspace_diff_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/cookie_table/cookie_table_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/text_editor/text_editor_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621585031501&cv=9&fst=1621585031501&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621585031511&cv=9&fst=1621585031511&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=http://localhost:9222/devtools/source_frame/source_frame_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/perf_ui/perf_ui_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/network/network_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pagead/js/rum.js", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/resourcePlainIconSmall.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/resourceDocumentIconSmall.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/product_registry_impl/product_registry_impl_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1", "METHOD=OPTIONS", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621585031511&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=483687043&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621585031501&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=2709687490&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621585031501&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=2709687490&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621585031511&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=483687043&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_114");
        status = nsApi.ns_web_url ("index_114",
            "URL=https://hb.emxdgt.com/?t=1461&ts=1621585031967",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_114", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_11");
        status = nsApi.ns_web_url ("auction_11",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_160x600_hdx_header&lib=ix&size=160x600&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=2.1.2&tmax=1461"
        );

        status = nsApi.ns_end_transaction("auction_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_12");
        status = nsApi.ns_web_url ("auction_12",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=2.1.2&tmax=1461"
        );

        status = nsApi.ns_end_transaction("auction_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_11");
        status = nsApi.ns_web_url ("fastlane_json_11",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811818&kw=rp.fastlane&tk_flint=index&rand=0.1373091699413742",
            INLINE_URLS,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A53614623%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A160%2C%22h%22%3A600%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%225%22%2C%22siteID%22%3A%22269244%22%7D%2C%22id%22%3A%223%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%22e54a98e6-19ed-4f55-873b-8d02b6eb0282%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T08%3A16%3A59%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_4");
        status = nsApi.ns_web_url ("translator_4",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("translator_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_12");
        status = nsApi.ns_web_url ("fastlane_json_12",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811816&kw=rp.fastlane&tk_flint=index&rand=0.08808394445738355"
        );

        status = nsApi.ns_end_transaction("fastlane_json_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_11");
        status = nsApi.ns_web_url ("bidRequest_11",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_left_160x600&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_11", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_12");
        status = nsApi.ns_web_url ("bidRequest_12",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_12", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_13");
        status = nsApi.ns_web_url ("bidRequest_13",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_13", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_13");
        status = nsApi.ns_web_url ("fastlane_json_13",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=9&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811828&kw=rp.fastlane&tk_flint=index&rand=0.5831648719008382",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/Images/popoverArrows.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("fastlane_json_13", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_13");
        status = nsApi.ns_web_url ("auction_13",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=2.1.2&tmax=1461",
            INLINE_URLS,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1", END_INLINE,
                "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_Y9ZGwKTl%22%2C%22site%22%3A%7B%22domain%22%3A%22www.kohls.com%22%2C%22page%22%3A%22%2Fsale-event%2Fdenim.jsp%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%22QFQjkyet%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788162%22%7D%2C%7B%22id%22%3A%22xql8PzGa%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788160%22%7D%2C%7B%22id%22%3A%22YRYrHV0s%22%2C%22banner%22%3A%7B%22w%22%3A160%2C%22h%22%3A600%7D%2C%22tagid%22%3A%22788163%22%7D%5D%7D", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("auction_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("headerstats_2");
        status = nsApi.ns_web_url ("headerstats_2",
            "URL=https://as-sec.casalemedia.com/headerstats?s=186355&u=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://744f8d03a455286ffe7938c4520e4eea.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=2243100800065831&correlator=1362855275212717&output=ldjh&impl=fifs&eid=31061226%2C21066613%2C21066615&vrg=2021051801&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Cclothing&enc_prev_ius=%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1&prev_iu_szs=1x1%2C160x600%2C300x250%2C300x250%2C320x50%7C1024x45%7C1024x64%7C1024x128&fluid=0%2C0%2C0%2C0%2Cheight&ists=16&prev_scp=pos%3Dwallpaper%7Cpos%3Dleft%7Cpos%3Dbottomleft%7Cpos%3Dbottomright%7Cpos%3Dmarquee&cust_params=pgtype%3Dsevent%26pgname%3Ddenim.jsp%26channel%3Ddesktop%26env%3Dprod&cookie=ID%3D793e49e62c912a30%3AT%3D1621585020%3AS%3DALNI_MYdNaRVnrPde_Bw_hEDdZzzjfW6hw&bc=31&abxe=1&lmt=1621585033&dt=1621585033455&dlt=1621585023356&idt=8381&frm=20&biw=1349&bih=590&oid=3&adxs=0%2C-12245933%2C-12245933%2C-12245933%2C163&adys=0%2C-12245933%2C-12245933%2C-12245933%2C153&adks=4004583564%2C3033452844%2C636752327%2C2623260883%2C1745468209&ucis=1%7C2%7C3%7C4%7C5&ifi=1&u_tz=330&u_his=3&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&rumc=2243100800065831&rume=1&vis=1&dmc=8&scr_x=0&scr_y=0&psz=1349x4828%7C0x0%7C0x0%7C0x0%7C1349x25&msz=1349x0%7C0x0%7C0x0%7C0x0%7C1024x25&ga_vid=1014052125.1621585033&ga_sid=1621585033&ga_hid=1705331753&ga_fc=false&fws=0%2C128%2C128%2C128%2C0&ohw=0%2C0%2C0%2C0%2C0&btvi=0%7C-1%7C-1%7C-1%7C0", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstUD6UeURYUlF1dR7Gwd52qnSvY9GdKbZ_KqKslIw_qvexs88hypo5q0Y207ozZcsKTJ7QgbU8SKuSImVytTcy_CWnKJXCfUbfn02_ijUNnuvEsB3d8pCWHdYRq9YO7wt7hrVtrd2LIpZCwpAAogpbK-L9nVEXOnxF5qPiHZOQq4pHRPA8dS7PvHTLX-XFp9iK-925wAr6uQpMFp5Qh48rsB_wVf-OGGC88FhHTo8H2w73SDBBpDvcwGMn46FPRxObiUz4uCxXpFwSKhIbbsTeri0oevJCh_lNCGT6ZveoVPxGJ3FA&sig=Cg0ArKJSzC3U4BHQ11CeEAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/safeframe/1-0-38/js/ext.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/9816407454004415507", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/2375489103470079767?", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/9820687163340561195?", END_INLINE,
                "URL=https://www.google.com/ads/measurement/l?ebcid=ALh7CaQVppXX4LV2ZMTGxXivAmmA5gIeCKrSgKB-GvRyFaPm1F0ZkvsYLm4DxVpzqErN9bYQzxCb2_zjBLH0Kj_pwUI-PJllBA", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138342631610&adorderid=2774409325&adunitid=16771032&advertiserid=32614632&lineitemid=5640094776&rand=28762599", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307654931&adorderid=2682029031&adunitid=16771032&advertiserid=32614632&lineitemid=5338295166&rand=1272811488", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsvp1KYGDb31fe6Ep0EZZdRd2okIG5x-Lnr5AtqM8hpRPLguNyPfnBTCa18vNnsUD9F5o2Uqr6wCqJOMYRADR8-dqSZnv1CaLjm4CLVe5tRinpSR46HKquZnGtC3UXoWi7Lf4H9TZI1n09rAwIidoPgLDyRDa2K6aBldqpMWMyG-k6y89eNnDwCFXcKhef7UzAEbkK2S3mtjPzUBoiC89rMmAmXvcfCECvWsHoYgH7-NVilpVf-RYOtsAEahLgAM95KRSYcpdDhH2xHrcMbYAN5UjIrGrl1O3y0&sig=Cg0ArKJSzHRTjDEXfwEhEAE&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307873913&adorderid=2682029031&adunitid=16771032&advertiserid=32614632&lineitemid=5338295166&rand=1591284549", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjst_q64GCM_zvhwTTUlVOD3RlKOKSil7lvqOFrjrhPNbRoB5ImWC2EqJrfaf_3q-SBzgzpyG01zqqRSATZuLFN1pkIsnf4qVI_A07rORV74CZZyMO5sUazCriWMgpZo3fTMlwAwNPGVtxcdRIOwIMEjzr1KkbtJ7PiwLrvFb_WFiV0eAv03IJLjPbt5jgfclitHtcedvayBlxeNizWyttt2WugU1T5piDE79wxauyBgI7OcvZ0GlHLwXDKjQvN7cHF4O3TW1swK1j5mvGEV-SqI3noveyfTZT4EC&sig=Cg0ArKJSzONmGJHY2QCpEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstcNLr4KcnnlNWm88zQ3lQVSXPj8tm13y_Q-NdcPNYCRzsxAA9Z92EPN_OhWhen98xZtQav1M6iCVCWp8ndcLUhJM_s0kBE1c0gOvjXAOLuthtydzgOXNE8xLJM3BlwXoOSIkkFX_G8NRjcfJC4IrVYNwzTKIz71QWWN6nGcJUU5C275jEXiz1fPkEVTMZshVImiBaPcNRURP5TsXpbQiBu4UR3mv48JLk7mP3m6n7jwRb0ocq8-NyQstP26TCQ2jHWuB4co9L5LY3vqmJrJGaMWctQpJ2j3Ac1pc4&sig=Cg0ArKJSzBbXKw6Ws9u1EAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjss-baQCc94RY3qpQf42kT4OSHgbm93A8gyAi06ZDOCqVDszROmjz6ykTiAexXxV2RXjLJNwZjkFVdjllOi99Q1nC4qJbETNOYrY2woBBl7YeW4E_oS7MedIpCBTcPAA3TQvYA_qKgG6mFNnjLkJMogA9yiVRyrXSQskQkIEPObYBUhexyApMq44tmaNLp7uDVXfbGJIjJCgf4qW6aLDFshGiyUnks-Afepxbp22jUm-OOjkH5Ai9_g7dflZzineOP9vVFv27SFmD53oJHF-eUnD4dO7g0TGFmeL&sig=Cg0ArKJSzHgBjPwdTMZOEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstzavu1tINUK4YlG-Xp-JHWt6oipPBVkcmj6smbsdYh33URVLve4K_MZpnZwSur_5yf4D6QyFLmrinWZYgPMpr80gb_W6nxoJk-RwnfInP60b5WW1Uk9rQpbfGVJpFWrZJ9vD-visSHRFb3uSDosg8ebJI-8yxkwrMoAqNpOg-PrefwZZLWvSh_gNmOcP6F01Tq92cKpvUeUOEdcMkzXg_IhRY28ng6PXrX_VqaYImTtNv2iudz6VVCOLX6AAKvxnRFw7MBIYBNoeZdhJrUWhvQVXnDjimn48LR_is&sig=Cg0ArKJSzJHlq41x6DX0EAE&urlfix=1&adurl=", END_INLINE
        );

        status = nsApi.ns_end_transaction("headerstats_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi");
        status = nsApi.ns_web_url ("csi",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=1~koy1v0jr&chm=1&c=2243100800065831&ctx=2&qqid=CO6lv_-q2vACFbo-twAdkpkEZg&met.4=fb.b~lb.4m~ol.d8~idt.5zj~dt.-ha&met.3=734.83~749.bq_i~736.ce~734.cz~735.d6_2~113.ff_b~112.fd_e&met.1=1.koy1v04d~14.1~15.1~16.1~17.1~18.1~19.1~20.d8~21.d9~22.d1~23.d1&met.7=CBsQBCAVOOcC~CB4QChgBIBgoGDBAOChoGnA8gAGiCYgBpRGwAQG4AQE~CCoQChgBIBgoGDAY~CBcQBhgBIKoBKKoBMKQCOHpovwFw7QF42YcCgAGghgKIAaCGArABAbgBAw~CBsQBhgBIKsBKKsBMPoCOM8B~CCgQChgBII8EKI8EMKAEOBFokARwnQSAAcajAYgBlbEDsAEBuAEB",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_2");
        status = nsApi.ns_web_url ("csi_2",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=1~koy1uykb&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&met.9=1.85z~2.8f6~3_1.9r9~7_1.0~7_2.0~7_3.0~7_4.0~7_5.0~4_1.a5m~5_1.a5n~5_4.a66~5_2.a6f~5_3.a6q~5_5.a81~6_5.alh&met.3=831.8fd_3~827.8fm~573.8fv_1~95.8fw_1~573.8g2~95.8g2~573.8g2~95.8g2~573.8g2~95.8g2~73.8g2_5~43.8g7_2~49.8g9_1~74.8ga~43.8ga~49.8ga_6~74.8gj_2~43.8gl~49.8gl~74.8gm~43.8gm_1~49.8gn~74.8go~43.8go_1~49.8gp~1.8gp~1.8gp~1.8gp~1.8gp~14.8gq~6.8gq~91.8gq_3~77.8fr_13~297.8gt_1~894.8gy~724.8gz_3~724.8h2~724.8h3~724.8h3~724.8h3~829.8k7~573.8kn~598.8kn~573.8kn~598.8kn~598.8kn~598.8kn~598.8kn~598.8ko~598.8ko~598.8ko~598.8ko~598.8ko~598.8ko~598.8ko~581.8ko~52.8ko~51.8ko~54.8ko~598.8ko~581.8ko~52.8ko~51.8ko~54.8ko~598.8ko~581.8kp~52.8kp~51.8kp~54.8kp~598.8kp~581.8kp~52.8kp~51.8kp~54.8kp~598.8kp~581.8kp~52.8kp~51.8kp~54.8kp~573.8mk~598.8mk~598.8mk~573.8mk~598.8mk~598.8mk~598.8mk~598.8mk~598.8mk~573.8mk~598.8mk~598.8mk~598.8mk~573.8mk~598.8mk~598.8mk~598.8mk~598.8mk~598.8ml~573.8ml~598.8ml~598.8ml~598.8ml~598.8ml~573.8ml~598.8ml~598.8ml~598.8ml~598.8ml~598.8ml~573.8ml~598.8ml~598.8ml~598.8ml~598.8ml~598.8ml~573.8ml~598.8ml~598.8ml~598.8ml~598.8ml~598.8ml~112.8o6_r~598.9pr~598.9pr~598.9pr~95.9pr_e~95.9q5~95.9q6~95.9q6~95.9q6~872.9q9_1~893.9qa~750.9qq_1~831.9rb~808.9re~808.9re~808.9re~808.9rf~808.9rf~808.9rf~808.9rf~808.9rf~808.9rf~808.9rf~751.9rf~646.a5m_1~646.a5n_e~646.a62_4~646.a66~800.a66~801.a66~831.a66~825.a66~800.a66~800.a66~800.a66~801.a66~825.a66~800.a66~801.a67~355.a67~825.a67~800.a67~800.a67~800.a67~801.a67~355.a67~825.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a67~800.a68~800.a69~801.a78~825.a78~801.a78~825.a78~801.a78~355.a78~825.a78~801.a78~355.a79~825.a79~646.a7m_b~800.a7w~800.a7w~800.a7x~800.a7x~800.a7x~801.acy~825.ad0~801.ad0~355.ad0~825.ad0~647.ad2~680.af2~680.af2~680.af2~824.af2~824.af2~824.af2~824.af2~298.aid_1~298.aif~298.aj9~713.ajd~155.ah7_2f~774.alh~844.alh~844.alh&met.10=1_5.IMJWEIDIAQiAyAEYhsowKAE~1_2.IMJWEAAIABiGyjAoAA~1_4.IMJWEAAIABiGyjAoAA~1_3.IMJWEAAIABiGyjAoAA~1_1.IMJWEAAIABiGyjAoAQ~1_5.IPliEAAIABiGyjAoAA~1_1.IPliEAAIABiGyjAoAA~1_5.II1pEIDoBQiA6AUYhsowKAE&met.7=CBsQCMABidCszQ0~CBsQCiCHFDj4AsABzLWN_AM~CBsQCiCJFDjuA8ABgNmfoQ0~CBsQCiCKFDj7A8AB1rOU9Ak~CBsQByCKFDiPAsABmMCWugo~CBsQByCSFDi7AsABhqyt4wM~CBsQByCSFDihA8ABmYvbjwM~CBsQByCSFDjlA8AB9_Tl2wU~CBsQByCTFDiHAsABlKj3oQg~CBsQByCTFDh8wAGTidAC~CBsQCiCTFDjvAcAB8sGWxw8~CBsQBiCTFMABoInMsQk~CBsQByCTFDifA8AB3eeMpwo~CBsQCiCUFDjoBcAB_vqdoQs~CBsQCiCUFDjbBsABxbrCjQ8~CBsQBiCXFDjWCMAB0u_OzQw~CBsQBiCXFDjhCsABlpLd9QQ~CBsQBiCXFDiPCsABiZLd9QQ~CBsQBiCXFDjkCsABiJLd9QQ~CBsQBiCXFDiNCsABi5Ld9QQ~CBsQBiCXFDjkCsABipLd9QQ~CBsQBiCXFDiOCsABjZLd9QQ~CBsQBiCXFDjkCsABjJLd9QQ~CBsQBiCYFDiMCsABj5Ld9QQ~CBsQBiCYFDjiCsABjpLd9QQ~CBsQBiCYFDiNCsABjN7y3Qw~CBsQBiCYFDjiCsABjd7y3Qw~CBsQBiCYFDiQCsABjt7y3Qw~CBsQBiCYFDjiCsABj97y3Qw~CBsQBiCYFDiNCsABiN7y3Qw~CBsQBiCYFDjjCsABid7y3Qw~CBsQBiCYFDjkCsAB5-2a8AE~CBsQBiCZFDjUCsAB1czJ2Qs~CBsQBiCZFDiiCcABtanp-Q0~CBsQBiC4FDjrCcAB4eurmgc~CBsQBiDNFDi-CMABsPny-Q0~CBsQBiDpFMABurDsyQI~CBsQCiDpFDi7B8AB9Ii_sAI~CBsQByDqFDi3B8AB6YiBGg~CBsQCiDqFDi4B8AB2ovd7AM~CBsQDSCgGDirAcABwIr27QU~CBsQByDSGTilA8ABvr_P0w4~CBsQAiCrHMAB5NObugw~CBsQAiCtHMABiaG_iQ4~CBsQBSDiHDipAcABkOHD6A8~CBsQDSDuHDiJAsABvaHw2w8~CBsQAiDmHziuAcABnuya8AE~CBsQDSCCJjj0AsAB9Ii_sAI~CBsQCiCWJjg0wAGD0zY~CBsQCiCXJji1AcABuN3e5wk~CBsQCiCYJji8AcAB74nomwk~CBsQCiCZJjgtwAGE07TBCA~CBsQCiCaJjgvwAG81N2hCA~CBsQCiDlJzgswAHHof-fBg~CBsQDSDvJzgZwAHO6ObiAQ~CBsQDSDzJzj1B8AB-YqLnwM~CBsQDSD3JzgZwAGi45uKDQ~CBsQByCOKDgGwAHr97nkCg~CBsQCiCVKDgHwAHno8egAQ~CBsQCiCPKjgbwAGGsojXDw~CBsQDSD1KjjBA8AB9Ii_sAI~CBsQCiDzLDgJwAGlz9L3Cg~CBsQBiC0Lzg8wAGFiKHGBA~CBsQDSC7LzjaA8ABtOvgxQs~CBsQCiCyMDj4AsAB57HorQ4~CBsQCiDMMziZAcABpMm1sQ4~CBsQDSDTMzh7wAHO36rwCw~CBsQDSChNDh6wAHpzulx~CBsQCiC0NDjDBMABm5fBPw~CBsQDSCFOjiKB8ABj_i2jwM~CBsQDSDMOjjEBsABnaqW7AU~CBsQCiC6PDgQwAHRlO6FDA~CBsQCiC8PDg-wAH76NOgAQ~CBsQCiC-PDgRwAG1ldWYBA~CBsQCiDBPDgRwAHBmLehAw~CBsQCiDCPDg_wAGGvNylCg~CBsQCiDTPTjbAcABrLngvQI~CBsQCiDpPThhwAHb0tPBCQ~CBsQCiDUPjiKCMABxbmzjws~CBsQCiD5Pjg5wAHBgIt4~CBsQCiCWPzg7wAGHwMKjAQ~CBsQDSDAPzjgAsABo7_XhQQ~CBsQCiCCQDjxB8AB9JSK6w8~CBsQCiCEQDgLwAG9xOWyCg~CBsQCiDcQDgdwAHtyJrrAw~CBsQCiDeQDgcwAHL1oqvCQ~CBsQCiCCQTgXwAGK28H6BQ~CBsQCiCxQTiFAcABwryHnQM~CBsQDSCmQjjqE8ABo7_XhQQ~CBsQChgBILBHKLBHMPJHOELAAYzV2OsE~CBsQChgBIINIKINIMNRIOFHAAYzV2OsE~CBsQBiCaSDjaAsABuLGv1A8~CBsQCiDgSDgMwAH8oKv8Cg~CBsQChgBIJBJKJBJMItKOHvAAYzV2OsE~CBsQChgBIJFJKJFJMI5KOHzAAYzV2OsE~CBsQCiCTSTg_wAGjlejOCg~CBsQBiCZSTizAcABiOmMpww~CBsQCiDbTzhkwAHJiOGHBw~CA0QChgBINxPKNxPMJxQOEFo3U9wi1CAAeemAYgBzvEDsAEBuAEBwAHh2-bdCw~CBsQCiDeUDgwwAH6g8q-BA~CBsQChgBIK5RKK5RMMRROBZor1Fwu1GAAcBtiAGVoAKwAQG4AQHAAe29hLwL~CA4QChgBINlSKNlSMPZSOB1o2lJw3lKAAdriBogBpqETsAEBuAEBwAHc9YuECg~CBsQCiC0UzgowAGcgKzLAQ~CCgQChgBILtTKLtTMPNVOLcCUNtTWOxUYJdUaOxUcOBVeMkMgAGuCIgB4xKwAQG4AQPAAdm-2qYB~CCgQChgBIMdTKMdTMPNVOK0CaO1UcOBVePQIgAGtCIgB4RKwAQG4AQPAAcH-0wU~CCgQChgBIOFVKOFVMM9XOO4BUPBVWI5XYLJWaI5XcLhXePOnAYABxqMBiAGVsQOwAQG4AQPAAZvh-nA~CBsQDSDgVji9AsABvZ-8kQg~CBsQBhgBINtXKNtXMMJZOOcBUN1XWNpYYP1XaNxYcMFZeGyAASqIASqwAQG4AQPAAb_fx5UG~CBsQBhgBINxXKNxXMLxZOOABUN5XWNtYYPxXaNxYcLpZeKgEgAEqiAEqsAEBuAEDwAH6kr78Bw~CBsQBhgBIN5XKN5XMMFZOOMBaNxYcLpZeKgEgAEqiAEqsAEBuAEDwAG6vec4~CBsQBhgBIN5XKN5XMNpZOPsBaNxYcNlZeGyAASqIASqwAQG4AQPAAcrW25EJ~CBsQDSD7VjimA8AB4ZTO3Q8~CBsQDSCIVziQA8AB4ZTO3Q8~CBsQDSCQVzjZAsABgOGgyAs~CBsQDSCZVziTBMAB8bXgGA~CBsQDSCdVziXBMAB27vP_ws~CBsQDSCgVziTBMAB27vP_ws~CBsQDSCoVzi6BMABqdHo7QQ~CBsQDSCrVziABMAB4JjemgU~CBsQDSCiVzjEBcAB27vP_ws~CBsQDSClVziBBcABqdHo7QQ~CBsQDSCmVzjEBMABqdHo7QQ~CBsQDSCGVziiBsAB4ZTO3Q8~CBsQDSDVWjinA8ABvZ-8kQg~CBsQDSCWVziJB8ABwpKj2gI~CC8QBxgBIMdiKMdiMJxkONYBUMtiWL1jYOJiaL9jcJpkeKkEgAFkiAFrsAEBuAEDwAGb_4nHBw~CC8QBxgBIMViKMViMKdkOOIBUMdiWLtjYN9iaL5jcKZkeJ8GgAFkiAFrsAEBuAEDwAHFpqP2DQ~CBsQDSCVYzisAcAB56Pd5Aw~CA8QDRgBIOdiKOdiMPFmOIsEaOhicOZmeKJYgAHOVYgB-MsCsAEBuAEDwAG_3prrBg~CBsQBRgBII5nKI5nMJ9nOBFomWdwnWeAAaQYiAHOL7ABAbgBAcABneHlywY~CBsQBRgBIKBnKKBnMLpnOBpoqGdws2eAAaQYiAHOL7ABAbgBAcABneHlywY~CCoQChgBIKhnKKhnMLtnOBLAAab7gJMH&qqid.1=COqlv_-q2vACFbo-twAdkpkEZg&qqid.2=COulv_-q2vACFbo-twAdkpkEZg&qqid.3=COylv_-q2vACFbo-twAdkpkEZg&qqid.4=CO2lv_-q2vACFbo-twAdkpkEZg&qqid.5=CO6lv_-q2vACFbo-twAdkpkEZg",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjssMVIGb-Oyh3LHyq6XJ2oMuMlYnDRSfrsAyiWPNrO-EzfQqJwQi2CwscJAmRhqjpRh5-9y0ARcnoiTMcPCultautewTApnZgL5oWQ9b0_s&sig=Cg0ArKJSzFWfTSeoYoBBEAE&id=lidar2&mcvt=1005&p=157,163,221,1187&mtos=1005,1005,1005,1005,1005&tos=1005,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,590&mc=1&app=0&itpl=3&adk=1745468209&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621585034250&dlt=0&rpt=227&isd=0&msd=0&r=v&fum=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_3");
        status = nsApi.ns_web_url ("csi_3",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=1~koy1v1f2&chm=1&c=2243100800065831&ctx=2&qqid=COulv_-q2vACFbo-twAdkpkEZg&met.4=fb.2u~lb.tj~ol.x9~idt.614~dt.-fp&met.3=734.12b~734.17l~113.1dv_m~112.1ca_27&met.1=1.koy1v02s~6.4~7.4~8.4~9.4~10.4~12.a~13.e~14.h~15.25~16.ti~17.ti~18.tj~19.x8~20.x8~21.x8&met.7=CBsQCBgBKAUwETitCWgKcA6AAaQYiAHOL7ABAbgBAQ~CBEQChgBIGYoZjDRAjjrAVB3WPgBYKQBaPkBcKYCeIU6gAHuNogB47MBsAEBuAED~CBcQBhgBIGcoZzChAzi5Amj6AXDMAniYsAKAAZKvAogBkq8CsAEBuAED~CBsQBiBoOIsF~CCoQChgBIGgoaDCcATg0~CCgQChgBIPYKKPYKMPwKOAZo-Apw-QqAAcajAYgBlbEDsAEBuAEB",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_4");
        status = nsApi.ns_web_url ("csi_4",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=1~koy1v1ih&chm=1&c=2243100800065831&ctx=2&qqid=COylv_-q2vACFbo-twAdkpkEZg&met.4=fb.2o~lb.w2~ol.14q~idt.60n~dt.-g6&met.3=734.166~734.1bp~113.1fg_4~112.1eu_r&met.1=1.koy1v039~6.4~7.4~8.4~9.4~10.4~12.8~13.j~14.q~15.25~16.w2~17.w2~18.we~19.14p~20.14p~21.14q&met.7=CBsQCBgBKAQwGji6C2gJcBOAAaQYiAHOL7ABAbgBAQ~CBEQChgBIGAoYDDLAjjrAWhhcMECgAHuNogB47MBsAEBuAEB~CBcQBhgBIGEoYTCkAzjDAmjoAXDgAnj6ugKAAfS5AogB9LkCsAEBuAED~CBsQBiBhOP8G~CCoQChgBIGIoYjCTATgy~CCgQChgBILEMKLEMMLMMOANosgxwsgyAAcajAYgBlbEDsAEBuAEB",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde663c7b16da4ba4947e827dd1a4af31-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("sodar_3");
        status = nsApi.ns_web_url ("sodar_3",
            "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2021051801&st=env",
            INLINE_URLS,
                "URL=https://match.adsrvr.org/track/cmf/generic?gdpr=0&ttd_pid=signal&ttd_tpi=1&ttd_puid=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=cajn1lv&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&docReferrer=https%3A%2F%2Fwww.kohls.com%2F&mode=v2&cf=4847939%2C6706303%2C6706308&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NDA2NzQ3MzE1NDkzNDAyOTEyNA&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MzIzMjgyNzcyNzM4MDM2MzA0MA", END_INLINE,
                "URL=https://cm.g.doubleclick.net/pixel?gdpr=0&google_nid=signal_dmp&google_cm&btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA", END_INLINE
        );

        status = nsApi.ns_end_transaction("sodar_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("manifest_json_7");
        status = nsApi.ns_web_url ("manifest_json_7",
            "URL=https://www.kohls.com/manifest.json",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC1c1b30b08b024ebfa9748b44ff4a21f7-source.min.js", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=ama_stats&su=www.kohls.com&doc=complete&pg_h=4897&pg_w=1366&pg_hs=4897&c=1&aa_c=0&av_h=93&av_w=1024&av_a=95232&b=4661&all_b=4661&d=0.019&all_d=0.019&ard=0.014&all_ard=0.014&dt=d", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCacdd8d53a0eb4a89a0a72c2d6a4147ec-source.min.js", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2/222/runner.html", END_INLINE,
                "URL=https://mon1.kohls.com/test_rum_nv?s=000000000000000000000&p=1&op=timing&pi=1&CavStore=-1&pid=10&d=1|1|1660|4|849|-1|-1|-1|2271|240|30|22145|2|31|24680|4867|10|6128|0|605|https%3A%2F%2Fwww.kohls.com%2F|https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular|www.kohls.com|VisitorId%3Dd64ec31a-62cb-4949-954e-5859e4a2f695%3B%20CavNV%3D4679721886262234957-33333-233050621967-0-9-0-1-145-28141-26566%3B%20CavSF%3DcavnvComplete%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C%2C1%2C%2C%2C%2C%2C%2C%3B%20AKA_RV2%3D22%3B%20AKA_CNC2%3DTrue%3B%20akacd_www-kohls-com-mosaic-p2%3D2177452799~rv%3D19~id%3D67b39543b0766db2c63209c66c15bf3d%3B%20mosaic%3Dgcpb%3B%20at_check%3Dtrue%3B%20AMCVS_F0EF5E09512D2CD20A490D4D%2540AdobeOrg%3D1%3B%20s_ecid%3DMCMID%25PIPE%2589552655867376930334316022663672800555%3B%20_dy_csc_ses%3Dt%3B%20_dy_c_exps%3D%3B%20_dycnst%3Ddg%3B%20_dyid%3D-2688648636616184714%3B%20_dyjsession%3Dab65e81d522fdc88f15c6a7229406282%3B%20dy_fs_page%3Dwww.kohls.com%3B%20_dycst%3Ddk.w.c.ms.%3B%20_dy_geo%3DIN.AS.IN_RJ.IN_RJ_Jaipur%3B%20_dy_df_geo%3DIndia..Jaipur%3B%20s_fid%3D2720B244721C920B-0ADF8DB34CC7F370%3B%20s_cc%3Dtrue%3B%20_dyid_server%3D-2688648636616184714%3B%20_mibhv%3Danon-1621585015807-2168751633_8212%3B%20IR_gbd%3Dkohls.com%3B%20s_vi%3D%5BCS%5Dv1%25PIPE%253053B63CFBE99B85-4000109CD0C6191D%5BCE%5D%3B%20_gcl_au%3D1.1.1885231748.1621585018%3B%20SignalSpring2016%3DB%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMDM%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMTA%3DREFZUw%3B|0|101|-1|24|24|WINDOWS|Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F56.0.2924.87%20Safari%2F537.36|en-GB|%5Bobject%20PluginArray%5D|Mozilla|0|PC|56.0|10|33333|1|0|0|%7B-1%7D|4867|-1|2874|1028816|0|0|0|3646|3646|0|0|4.5.0_3aeab9|3b396b&lts=-1&d2=-1|-1|-1|1|100|0|1", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCdc9d7fc5fb9e4d329178721363aaa3f9-source.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCde61955ee7f24142bacb5bddee25a9a4-source.min.js", END_INLINE,
                "URL=https://js.cnnx.link/roi/cnxtag-min.js?id=31851", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=gcms&gdpr=0&btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA&google_gid=CAESEA2_0A5XNLiaWd--xqhS2fc&google_cver=1", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=tBLcuKl&btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA&uid=e54a98e6-19ed-4f55-873b-8d02b6eb0282", END_INLINE,
                "URL=https://secure.adnxs.com/getuid?https%3A%2F%2Fs.thebrighttag.com%2Fcs%3Fbtt%3DoH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA%26uid%3D$UID%26tp%3Dan%26gdpr%3D0", "REDIRECT=YES", "LOCATION=https://secure.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fs.thebrighttag.com%252Fcs%253Fbtt%253DoH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA%2526uid%253D%24UID%2526tp%253Dan%2526gdpr%253D0", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCecd16bab3edd42838214adeca2f386c4-source.min.js", END_INLINE,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://secure.adnxs.com/bounce?%2Fgetuid%3Fhttps%253A%252F%252Fs.thebrighttag.com%252Fcs%253Fbtt%253DoH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA%2526uid%253D%24UID%2526tp%253Dan%2526gdpr%253D0", "REDIRECT=YES", "LOCATION=https://s.thebrighttag.com/cs?btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA&uid=9203284213465667296&tp=an&gdpr=0", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_json_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_39");
        status = nsApi.ns_web_url ("test_rum_nv_39",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=10&d=1|1&lts=233050508&nvcounter=0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/bg/zue3njNLpzxGAZrYILNRV_oDQoN1Bf4uoYDHWIdg9NQ.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_39", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_40");
        status = nsApi.ns_web_url ("test_rum_nv_40",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=10&d=10|0|1&lts=233050508&nvcounter=1",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://bat.bing.com/p/action/4024145", END_INLINE,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=c104aeee-82cb-4e75-aa9a-4288b0972e34&sid=fa3fbb90ba0c11eb8ba183b721734ade&vid=fa3ffd30ba0c11eb9490814ed83fd281&vids=1&pi=0&lg=en-GB&sw=1366&sh=768&sc=24&p=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&lt=24711&pt=1621585020821,2370,2400,2,1662,1662,1662,1662,1662,1662,,1666,2271,2511,2535,4867,4867,4869,24680,24680,24711&pn=0,1&evt=pageLoad&msclkid=N&sv=1&rn=150595", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_40", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_115");
        status = nsApi.ns_web_url ("index_115",
            "URL=https://684d0d38.akstat.io/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/cs?btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA&uid=9203284213465667296&tp=an&gdpr=0", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_115", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_5");
        status = nsApi.ns_web_url ("csi_5",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=2~koy1v0i2&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&met.3=713.an2_1~143.amz_5~143.ar7_9~129.ari~143.auc_3~680.ava~824.ava~824.ava~824.ava~824.ava~680.ava~680.ava~132.ava~680.avb~680.avb~824.avb~824.avc~824.avc~824.avc~680.avc~132.avc~143.ay9_2~129.ayj~143.b19_2~415.b3w_1~844.b3x~844.b3x~713.b3x~130.b3x_1~143.b43_4~132.b4i_2~129.b5h~143.b70_2~713.b7s~132.b7s_1~680.b8x~824.b8x~824.b8x~824.b8x~824.b8x~680.b8x~680.b8x~132.b8x~680.b8z~824.b8z~824.b8z~824.b8z~824.b8z~680.b8z~680.b8z~132.b8z~680.b90~680.b90~824.b90~824.b90~824.b90~824.b90~680.b90~132.b90~680.b91~680.b91~824.b92~824.b92~824.b92~824.b92~680.b92~132.b92~143.ba0_3~415.bbu~844.bbv~844.bbv~130.bbv~129.bck~143.bd3_4~132.be2~713.bga~143.bg9_4~132.bha~143.bj5_2~129.bjk~143.bm0_3~143.bov_1~129.bqi~143.brq_a~143.bus_2~129.bxi~143.bxq_d~143.c0v_3~143.c3t_8~129.c4i~143.c72_h~143.cad_3~129.cbh~143.cdf_d~143.cgm_3~129.cig~143.cji_3~143.cme_1~143.cp8_6~129.cpj_1~143.cs8_2~143.cv2_4~129.cwj~143.cxz_5~143.d0y_4~129.d3k_3~143.d3u_a~143.d6y_6~143.d9x_4~129.dal~143.dcu_f~143.dg1_6~129.dhk_1~143.diz_6~143.dlz_1~129.dok~143.dou_4~143.drq_6~143.dur_a~129.dvj~143.dxu_5~143.e0u_8~129.e2j_1~143.e3w_2~143.e6r_e~129.e9i~143.ea2_b~143.ed7_3~143.eg3_5~129.egh~143.ej1_5~143.ely_8~129.eni_2~143.ep0_3~143.erx_4~129.eui~143.euv_4~143.exu_6~143.f0t_5~129.f1i~143.f3t_5~143.f6t_5~129.f8n_1~143.f9t_3~143.fcq_5~129.ffn~143.ffn_a~143.fiq_6~143.flp_6~129.fmo_1~143.foo_4~143.frl_4~129.fto~143.fui_9~143.fxl_3~143.g0h_8~129.g0s_1~143.g3i_f~143.g6r_5~129.g7r~143.g9q_8~143.gcq_2~129.gep~143.gfl_7~143.gik_7~143.glk_d~129.gm1_1~143.goq_3~143.grv_3~129.gt2_1~143.gus_6~143.gxr_b~129.h01~143.h0v_2~143.h3q_9~143.h6s_1w~129.h9d~143.hbh_5~143.hg1_3~129.hgd~143.hkd_3~143.hn9_1~129.hnd~143.hqq_3~143.htn_4~129.huc~143.hwj_1~143.hzd_5~129.i45~143.i46_n~143.i7n_5~143.ibz_4~129.ic3~143.iex_3~143.iht_7~129.ij2~143.iku_3~143.inq_6~129.iq1~143.iqp_3~143.itl_1~143.iwg_6~129.ix1_1~143.izf_1~94.j2c~130.j2f~130.j2f~154.j2f~143.j4c_3~573.j4q~598.j4q~598.j4q~598.j4q~598.j4q~598.j4q~113.j4m_7~129.j60~153.jai_1~143.jci_2~129.jea~143.jfm_3~143.jja_2~143.jmh_3~143.jpl_2~143.juh_5&met.9=6_2.b3w~6_3.bbv&met.7=CBsQARgBIKdrKKdrMMlzOKIIwAGkoPylBw~CBsQCDiHwQHAAYnQrM0N~CBsQCiDwwAE4QMAB5pKZ2A4~CCcQDRgBIIbBASiGwQEwgcIBOHtoisEBcP3BAXi-N4ABwDaIAdRHsAEBuAEDwAHz8suuCw~CBsQCiD_wAE43wLAAcW5s48L~CBsgpsMBOGzAAenCpkQ~CBwQBhgBIN3BASjdwQEwmMUBOLoDUPfBAVjBwwFg08IBaMHDAXD3xAF4vgOwAQG4AQPAAZSE4rUO~CBsQCiCDxAE4nQHAAYT4v80H~CCcQChgBIK_DASivwwEwtMUBOIYCwAHiwZvaBQ~CBsQCiCtxQE4IMAB1eGangU~CBsQCiCIxgE4N8AB16XFJA~CBsQCiCLxgE4NcAB0YaauQ8~CBsQCiD7wwE4_gLAAeyfo84L~CCcQBRgBIPzFASj8xQEwwcYBOEXAAY3TtMwJ~CBsQBiCUxAE4hQPAAZbzgLYF~CCQQBhgBIKnEASipxAEwmccBOO8CwAGNm6KmCQ~CBsQCiCCxwE4O8AB2oysygM~CBsQCiD_xgE48wHAAc-avYgL&met.1=1.koy1upw5~2.1tu~3.1uo~4.2~5.1a6~6.1a6~7.1a6~8.1a6~9.1a6~10.1a6~12.1aa~13.1r3~14.1xr~15.1yf~16.3r7~17.3r7~18.3r9~19.j1k~20.j1k~21.j2f~22.2tb~23.2tb&met.2=19.9~17.8m1~18.9xe",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=sodar2&v=222&t=2&li=gpt_2021051801&jk=2243100800065831&bg=!-_il-LzNAAZ7hX_Ue4U7ACkAdvg8WvyLcaNmZ5dJvuZt8IXo0TRNO8PG2BooMTp8E6tsdPLFD3koPwIAAADOUgAAAAhoAQcKAY95ZD5iGdbF_nWh2ekQc_SJa1eWKKBih3pe4ez10MCriVS3QMHO7NfWYScwgu_5j03lifU_624KDcpMmL1BIb1brpzhlPMN4X2BHRQd8gqAwHYd2VmcAzm-iEDmabX6ioPbagJQUc4T7FvpUR1HnZNehugwiOYdn7hqdVCJQkj2ZWpcLVubDZaAHU6eShTZ02qjHKMJIpk6bMkOoNur0rm_fBPvlWHQTdtlVm3UzBtB2SmXgQSV5mw1wd1tP9ChYzFOvaR0at_7ciT1uMau4jirceyHh-RhWbgn1LVoPMAdGKaQmByxcoTfga5D5Y-8mCkfAUqfqXPglBJNlKyqdbwop4haZy4MAGI7U_2xWaL-DJwj8l25H8FNqLsGxPXl8YzxiaSSx_NFu77oqpvbtG36DTjcaCfnDsQPUllxOmFYNo3G-KWmzpRjyboSC3nffLfzz1OYzoiKXS_K3k4werygBnUFWv1BgfncQM7KyBFYedheypR6T5Aj9d_Q9g_fBhCXsQzPAAEMkDPgb8CNKaKZAgpPb62aP3RlaKa6ZV0PldpHcc2vi-LfrooLJCsbxM8LdVOIOT2z-ZF9agFSWesOGEAVGVhubMQG8mf5HqOMQrKtlsDzo-5IYK5aZSozRXXcr58XMadt3LfqaMF6q-WGo-3FQ7E9Gi7_kTbtg3MJuUeu5gJ4xiExS8iBFJRc-SNfZ23jmdyLrZRNn1Mz2WXQXci-j9aSopGy-6YXtd7Hbc0BOaNsIzL_HIJbCi7qS_4-4d64hVeFItfyPMwZeAHr5_pvxVdTAUjhQyP02bwTnxIpEcNLCFpBUfJG87G_9kBZJWVAXUzXT9iWViSUSlW5H5FNnWnZH0cgMC9qErR0bw6QJcYDscgtqOo5vxRGAxlEq5q2gn2pimivTAk2zdtYjBDDxHDAUMn3a16CrzuX6Ps1PeRXN3vZiPZ8nNJkuQT65gRwMy0yMYfF7gYLJXTmXxD08sfINzUY46R4p5drQyUfcgKxuOeHThUuF1tdpD-8S2BLdFIZNmJ-v02XC9M59-oWCMxd-RP1dM7BOpOsXwaBz2tGU2Mcl0yUj8A_VbDl6oxizAaStKyud9ZDeCYsKxCETWFQpkz4zB7zgFKZW3c1-5Zitlrwm_NVVM2exGvw47NFw4qf4nc5d1Ipu3G_qOcJn6qfUkTVmgtdTtAToI6fXnH_KdPpkXNIRrYlByKHDZuX7oGia7Y7X7s", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_2");
        status = nsApi.ns_web_url ("outfits_2",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_4_200622&return_object=true"
        );

        status = nsApi.ns_end_transaction("outfits_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("tag_7");
        status = nsApi.ns_web_url ("tag_7",
            "URL=https://tagtracking.vibescm.com/tag",
            INLINE_URLS,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=3196309079341;gtm=2od5c1;auiddc=1885231748.1621585018;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular?", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www47/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC8478690a6f32401fb3fe279105e2aa1c-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("tag_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_3");
        status = nsApi.ns_web_url ("outfits_3",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_3_200622&return_object=true",
            INLINE_URLS,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCf3953ba39c0e4cda986a27cd34fdd08c-source.min.js", END_INLINE,
                "URL=https://data.adxcel-ec2.com/pixel/?ad_log=referer&action=misc&value={%22af_revenue%22:%22[value]%22,%22af_order_id%22:%22[order_id]%22}&pixid=6a175b53-c680-4327-aef7-1eb5647a6339", END_INLINE
        );

        status = nsApi.ns_end_transaction("outfits_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_4");
        status = nsApi.ns_web_url ("outfits_4",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_1_200622&return_object=true"
        );

        status = nsApi.ns_end_transaction("outfits_4", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("outfits_5");
        status = nsApi.ns_web_url ("outfits_5",
            "URL=https://widget-api.stylitics.com/api/outfits?username=kohls&total=10&with_item_coords=true&tags=DenimLandingPage_2_200622&return_object=true",
            INLINE_URLS,
                "URL=https://cdnssl.clicktale.net/ptc/d82d7432-724c-4af9-8884-ffab4841f0a1.js", END_INLINE,
                "URL=https://sb.scorecardresearch.com/beacon.js", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ec8a33906ed82b185f00041045ab6ba4419b781", END_INLINE,
                "URL=https://d.agkn.com/pixel/10107/?che=338602244&mcvisid=89552655867376930334316022663672800555", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ecba539579e1d0186579e70b83723392a9038a9", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ec8a13e8280f899418a37163d7340b0f4b41aba", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ecfae3ec2fef3603aa31538844cbcf3fb146ea9", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RCfde5714c92254dfd820f510d3727aef2-source.min.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("outfits_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_41");
        status = nsApi.ns_web_url ("test_rum_nv_41",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&op=pagedump&pi=1&CavStore=-1&pid=10&d=10|2|0&lts=233050508&nvcounter=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://cdnssl.clicktale.net/pcc/d82d7432-724c-4af9-8884-ffab4841f0a1.js?DeploymentConfigName=Release_20210505&Version=4", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ecfa33ac7449bff7e6560ab5dd8576c7a8c69fc", END_INLINE,
                "URL=https://cdnssl.clicktale.net/www/latest-WR110.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC01d660fbc4b44effb3436273c815864f-source.min.js", END_INLINE,
                "URL=https://content.stylitics.com/images/collage/74258ecfae3920030aa5fb2ff52ae91a028f5a2e1d25", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=CPW2s4ar2vACFVONcAodPEgNaQ;src=8632166;type=landi0;cat=unive0;ord=3196309079341;gtm=2od5c1;auiddc=1885231748.1621585018;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular?", END_INLINE,
                "URL=https://b-code.liadm.com/a-00oc.min.js", END_INLINE,
                "URL=https://assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/1a45067cc1cd/RC4d3fb96a16274690a8983bbe28d8c339-source.min.js", END_INLINE,
                "URL=https://b-code.liadm.com/sync-container.js", END_INLINE,
                "URL=https://sc-static.net/scevent.min.js", END_INLINE,
                "URL=https://tr.snapchat.com/cm/i?pid=8e9bd9e0-8284-4dbd-ab20-145759728098", END_INLINE,
                "URL=https://www.google-analytics.com/analytics.js", END_INLINE,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CPW2s4ar2vACFVONcAodPEgNaQ;src=8632166;type=landi0;cat=unive0;ord=3196309079341;gtm=2od5c1;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular", END_INLINE,
                "URL=https://www.google-analytics.com/collect?v=1&_v=j90&a=1705331753&t=pageview&_s=1&dl=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ul=en-gb&de=UTF-8&sd=24-bit&sr=1366x768&vp=1349x590&je=0&_u=YChAgAAB~&jid=536801860&gjid=318385437&cid=1014052125.1621585033&tid=UA-45121696-1&_gid=64734012.1621585048&cd2=d64ec31a-62cb-4949-954e-5859e4a2f695&cd4=&z=1301845851", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621585048318&cv=9&fst=1621585048318&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621585048323&cv=9&fst=1621585048323&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://sc-static.net/js-sha256-v1.min.js", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621585048323&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=362946233&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621585048318&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=638941738&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621585048318&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=638941738&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621585048323&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=362946233&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_41", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("collect_2");
        status = nsApi.ns_web_url ("collect_2",
            "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=536801860&gjid=318385437&_gid=64734012.1621585048&_u=YChAgAABAAAAAE~&z=1688403667",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("collect_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_42");
        status = nsApi.ns_web_url ("test_rum_nv_42",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=10&d=10|0|2&lts=233050508",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=536801860&_u=YChAgAABAAAAAE~&z=698401472", END_INLINE,
                "URL=https://www.google.co.in/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=536801860&_u=YChAgAABAAAAAE~&z=698401472", END_INLINE,
                "URL=https://tr.snapchat.com/p", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_42", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_116");
        status = nsApi.ns_web_url ("index_116",
            "URL=https://ing-district.clicktale.net/ctn_v2/auth/?pid=24&as=1&1734800455&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgEoFMA2KCGBnFB9AJgAYCBGIgVkqAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=1&r=072846", END_INLINE,
                "URL=https://c.clicktale.net/pageview?pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&lv=1621585048&lhd=1621585048&hd=1621585048&pn=1&re=1&dw=1366&dh=5432&ww=1366&wh=607&sw=1366&sh=768&dr=https%3A%2F%2Fwww.kohls.com%2F&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&uc=1&la=en-GB&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22denim%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22denim%22%5D%7D&v=10.8.6&r=807537", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMAmBsBmBTAHAFgKwFoDGBGATIpqgAySqYCcARtBQIa6QLSLUyTZAAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=1&r=808263", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsAmBsAsCmBjAzARgIYFpoCZECNNYBOEzYgVgUwoA4Lj5Z0cAzaSoAA%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=1&r=130085", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_116", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("events_6");
        status = nsApi.ns_web_url ("events_6",
            "URL=https://c.clicktale.net/events?v=10.8.6&str=869&di=3201&dc=23014&fl=23045&sr=11&mdh=5102&pn=1&re=1&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&lv=1621585048&lhd=1621585048&hd=1621585048&pid=2399&eu=%5B%5B0%2C21%2C1366%2C607%5D%5D",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/dvar?v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=1&dv=N4IgygpghgTgxgCwAQAUEygZwiAXCAOwHsCBabWREAGhAAcYi6BGAdjxAGsiEAbTJMQAuSXkQDm4iABMkASwIAfMQE8ovISsFERYyTPkEaIAIIAhAPomAKlbNJSSACIBZJNYBKADgAsABgBOByQPaF53OQBbCCRISmQUCBhMEnU5AC8OP2Z3CEwhY3MrWwANawACx1d3Dx8A5i9gspCIAHdYaQEUOTERACY%2FPuZSPwBWEZ8snO7epGY%2FPwBSQssbC2bHACk%2B4MgRADVYJAAzIhgkMwgCJAAJAFdj48ioa8cXWE4kADk2jgBRAAedCSciucBiJhWxTswSc3wAwn4AMxjXbwgAywT%2Bzx6SAA8nQRABJa5gBByOh0BTiJBTJAmXjhIQIGJmF4EJKYKFrcywhHIgKjNGYxwAaR4%2FCQ8I6SAA4mIAEbqW7QaR0lAwCAARzuyvRCk41O5pWssLcoSYV1iQjOMX1%2BVQHjxcIGAxG4z8k3wgOBMFBBHB9JAAF8gA&r=942884", END_INLINE
        );

        status = nsApi.ns_end_transaction("events_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("j_6");
        status = nsApi.ns_web_url ("j_6",
            "URL=https://rp.liadm.com/j?tna=v2.0.1&aid=a-00oc&wpn=lc-bundle&pu=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&refr=https%3A%2F%2Fwww.kohls.com%2F&ext_s_ecid=MCMID%7C89552655867376930334316022663672800555&ext_s_vi=%5BCS%5Dv1%7C3053B63CFBE99B85-4000109CD0C6191D%5BCE%5D&ext_IXWRAPPERLiveIntentIp=%7B%22t%22%3A1621585019385%2C%22d%22%3A%7B%22response%22%3A%22pass%22%2C%22version%22%3A%221.1.1%22%7D%2C%22e%22%3A1621671419385%7D&duid=0b10d8358f40--01f6700cpcqvx6swnfx7rby23x&se=e30&dtstmp=1621585048390",
            INLINE_URLS,
                "URL=https://cdnssl.clicktale.net/www/WR1113b.js", END_INLINE,
                "URL=https://i.liadm.com/s/c/a-00oc?s=&cim=&ps=true&ls=true&duid=0b10d8358f40--01f6700cpcqvx6swnfx7rby23x&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMkAwIwDZHwJwwOwpQVnUAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=1&r=723043", END_INLINE,
                "URL=https://sli.kohls.com/baker?dtstmp=1621585049553", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("j_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_117");
        status = nsApi.ns_web_url ("index_117",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&0&0&0&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_117", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_118");
        status = nsApi.ns_web_url ("index_118",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&1&0&1&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=127444&dpuuid=51e452ef-39b5-42e8-b7ed-83178ae8eaea&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2F51c4ea184807492a85b8d8eabe5a8687%3Fmpid%3D82775%26muid%3D%24%7BDD_UUID%7D", "REDIRECT=YES", "LOCATION=https://i.liadm.com/s/e/a-00oc/0/51c4ea184807492a85b8d8eabe5a8687?mpid=82775&muid=84622417935889388173660587809767566339", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://trc.taboola.com/sg/liveintent/1/cm/", END_INLINE,
                "URL=https://match.adsrvr.org/track/cmf/generic?ttd_pid=liveintent&ttd_tpi=1", END_INLINE,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=51e452ef-39b5-42e8-b7ed-83178ae8eaea", END_INLINE,
                "URL=https://sync.mathtag.com/sync/img?mt_exid=36&redir=https%3A%2F%2Fi.liadm.com%2Fs%2Fe%2Fa-00oc%2F0%2F51c4ea184807492a85b8d8eabe5a8687%3Fmpid%3D7156%26muid%3D%5BMM_UUID%5D&51e452ef-39b5-42e8-b7ed-83178ae8eaea", END_INLINE,
                "URL=https://i.liadm.com/s/e/a-00oc/0/51c4ea184807492a85b8d8eabe5a8687?mpid=82775&muid=84622417935889388173660587809767566339", END_INLINE,
                "URL=https://x.dlx.addthis.com/e/live_intent_sync?na_exid=51e452ef-39b5-42e8-b7ed-83178ae8eaea&rd=Y", END_INLINE,
                "URL=https://x.bidswitch.net/sync?ssp=liveintent&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea", END_INLINE,
                "URL=https://x.bidswitch.net/syncd?dsp_id=256&user_group=2&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", END_INLINE,
                "URL=https://i.liadm.com/s/35759?bidder_id=44489&bidder_uuid=e54a98e6-19ed-4f55-873b-8d02b6eb0282", "REDIRECT=YES", "LOCATION=https://i6.liadm.com/s/35759?bidder_id=44489&bidder_uuid=e54a98e6-19ed-4f55-873b-8d02b6eb0282", END_INLINE,
                "URL=https://i.liadm.com/s/e/a-00oc/0/51c4ea184807492a85b8d8eabe5a8687?mpid=7156&muid=4b6160a7-6c9c-4b00-adb0-67cdff2b1c0c", END_INLINE,
                "URL=https://x.bidswitch.net/ul_cb/sync?ssp=liveintent&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea", "REDIRECT=YES", "LOCATION=//a.sportradarserving.com/sync?ssp=bidswitch&bidswitch_ssp_id=liveintent", END_INLINE,
                "URL=https://x.bidswitch.net/ul_cb/syncd?dsp_id=256&user_group=2&user_id=51e452ef-39b5-42e8-b7ed-83178ae8eaea&redir=%2F%2Fi.liadm.com%2Fs%2F52176%3Fbidder_id%3D5298%26bidder_uuid%3D%24%7BBSW_UID%7D", "REDIRECT=YES", "LOCATION=//i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=2d16f1b3-c396-49b0-84d0-afafe08f49d7", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=CPW2s4ar2vACFVONcAodPEgNaQ;src=8632166;type=landi0;cat=unive0;ord=3196309079341;gtm=2od5c1;auiddc=1885231748.1621585018;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular?", END_INLINE,
                "URL=https://i.liadm.com/s/52176?bidder_id=5298&bidder_uuid=2d16f1b3-c396-49b0-84d0-afafe08f49d7", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_118", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_119");
        status = nsApi.ns_web_url ("index_119",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&2&1&0&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_119", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_120");
        status = nsApi.ns_web_url ("index_120",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&3&1&1&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_120", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_121");
        status = nsApi.ns_web_url ("index_121",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&4&1&2&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_121", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_43");
        status = nsApi.ns_web_url ("test_rum_nv_43",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=10&d=1|1&lts=233050519&nvcounter=4",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_43", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_122");
        status = nsApi.ns_web_url ("index_122",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&5&1&3&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_122", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_123");
        status = nsApi.ns_web_url ("index_123",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&6&1&4&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_123", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_124");
        status = nsApi.ns_web_url ("index_124",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&7&1&5&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_124", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_44");
        status = nsApi.ns_web_url ("test_rum_nv_44",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=domwatcher&pi=1&CavStore=-1&pid=10&d=10|0|2&lts=233050519",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/Images/mediumIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/cm_modes/cm_modes_module.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/formatter_worker.js?ws=localhost:9222/devtools/page/D035B020EFEA57B6E2D10B7D1877A4C8", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/platform/utilities.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_44", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_125");
        status = nsApi.ns_web_url ("index_125",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&8&0&2&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/platform/utilities.js", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_125", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("events_7");
        status = nsApi.ns_web_url ("events_7",
            "URL=https://c.clicktale.net/events?v=10.8.6&str=869&di=3201&dc=23014&fl=23045&sr=11&mdh=5102&pn=1&re=1&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&lv=1621585048&lhd=1621585048&hd=1621585048&pid=2399&eu=%5B%5B6%2C38445%2C1266%2C139%2C%22ul%23top-nav-right%3Eli%3Aeq(3)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C38457%2C1250%2C171%2C%22ul%23top-nav-right%3Eli%3Aeq(3)%3Ea%3Aeq(0)%3Espan%3Aeq(0)%22%5D%2C%5B6%2C38552%2C830%2C543%2C%22div%23lp-denim%3Ea%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(3)%3Eimg%3Aeq(0)%22%2C%22%22%5D%2C%5B2%2C38819%2C737%2C507%2C0%2C%22div%23lp-denim%3Ea%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(3)%3Eimg%3Aeq(0)%22%2C22478%2C35735%5D%2C%5B7%2C38823%2C643%2C491%2C%22div%23lp-denim%3Ea%3Aeq(0)%3Ediv%3Aeq(1)%3Ediv%3Aeq(3)%3Eimg%3Aeq(0)%22%5D%2C%5B6%2C38827%2C643%2C491%2C%22div%23lp-denim%3Ea%3Aeq(0)%3Ediv%3Aeq(0)%22%2C%22%22%5D%2C%5B7%2C38889%2C374%2C246%2C%22div%23lp-denim%3Ea%3Aeq(0)%3Ediv%3Aeq(0)%22%5D%2C%5B2%2C39236%2C166%2C2%2C0%2C%22div%23new-equity-banner%3Ediv%3Aeq(0)%22%2C224%2C3277%5D%5D",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("events_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_45");
        status = nsApi.ns_web_url ("test_rum_nv_45",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=el&pi=1&CavStore=-1&pid=10&d=10&lts=233050519&nvcounter=5",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_45", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_6");
        status = nsApi.ns_web_url ("csi_6",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=3~koy1v9v0&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&met.3=143.jyw_4~143.k1u_2~143.k4p_2~143.k7l_2~143.kak_4~143.kdk_5~143.khv_1~143.km5_3~143.kzt_4~143.l2v_2~143.l5s_2~143.l8r_2~143.lbq_2~143.leq_2~143.lm3_3~143.lsq_a~143.lvs_3~143.lyw_66~143.mdr_z~143.mi9_4~143.mlu_3~143.moq_5~143.ms1_3~143.n1f_3~143.n4u_2~143.n8b_2~143.o7m_3~680.o7y~680.o7y~680.o7y~132.o7y~143.oid_4~143.olj_2~143.oog_2~143.ore_2~143.ouf_1~143.oxh_7~143.p0i_7~143.p3i_6~143.p6j_a~143.p9z_4~143.pd8_5~143.pg8_5~143.pjd_4~143.pmc_4~143.ppe_5~143.pse_5~143.pvc_4~143.pyd_2~143.q1e_7~143.q4r_3~143.q7v_4~143.qax_4~143.qdx_5~143.qgx_5~143.qjz_4~143.qn2_6~143.qq2_8~143.qt3_6~143.qw7_3~143.qz2_1~143.r1w_3~143.r50_3~143.r7w_5~143.rax_2~143.rdv_2~143.rgr_3~143.rjr_5~143.rmr_2~143.rpv_4~143.rsy_3~143.rw8_2~143.rzj_4~143.s2n_2~143.s5o_2~143.s8r_9~143.sbt_7~143.seu_2~143.shu_6~143.sku_5~143.snx_2~143.sqy_5~143.suk_2~143.sxg_4~143.t0x_7~143.t42_9~143.t7j_5~143.taj_3~143.tdm_7~143.tgz_8~143.tk4_2~143.tnb_5~143.tqg_5~143.ttg_4~143.tws_9~143.u05_4~143.u3g_5~143.u6f_7~143.u9g_4~143.uch_6~143.ufj_5~143.uii_5~143.ull_8~143.uoz_5~143.ury_3~143.uuz_3~143.uy4_9~143.v19_7~143.v48_e~143.v7f_4~143.val_a~143.ve0_2~143.vgy_6~143.vk9_5~143.vnh_5~143.vqi_6~143.vtl_4~143.vwn_6~143.vzw_5~143.w2x_9~143.w6c_3~143.w9e_3~143.wci_2~143.wfm_6~143.wiy_6~143.wlz_3~143.woz_2~143.ws3_5~143.wve_2~143.wyg_4~143.x1j_r~143.x5c_3~143.x8e_3~143.xbg_2~143.xef_4~143.xhh_2~143.xkm_5~143.xnp_4~143.xqx_5~143.xty_9~143.xxa_5~143.y0d_4~143.y3a_5~143.y6a_6~143.y9o_2~143.ycq_4~143.yfo_2~143.yiq_1~143.ylq_1~143.yoq_1~143.yrp_1~143.yuq_2~143.yxu_2~143.z0u_2~143.z3u_2~143.z6v_2~143.za0_2~143.zcv_2~143.zfq_1~143.zil_2~143.zlx_5~143.zp1_4~143.zs7_5~143.zv7_5~143.zy8_5~143.1017_6~143.1046_4~143.1078_1~143.10a7_4~143.10d7_7~143.10ga_4~143.10jj_4~143.10mu_4~143.10pu_2~143.10su_5~143.10vu_3~143.10yw_4~143.111t_5~143.114x_3~143.117u_2~143.11ap_2~143.11dk_2~143.11gr_1~143.11k0_2~143.11mu_2~143.11pp_2~143.11sr_4~143.11vx_5~143.11yy_4~143.1220_5~143.1253_5~143.1283_7~143.12b4_1~143.12e5_4~143.12h7_3~143.12k6_3~143.12n8_3~143.12q6_2~143.12t5_3~143.12w5_3~143.12z6_4~143.1326_3~143.1353_2~143.1386_4~143.13b6_1~143.13e6_3~143.13h4_5~143.13k6_3~143.13n2_3~143.13py_3~143.13sz_4~143.13w1_3~143.13z9_5~143.1429_2~143.1458_4~143.148a_3~143.14bb_4~143.14ee_3~143.14ha_3~143.14k9_4~143.14nc_3~143.14qa_5~143.14t8_3~143.14w8_4~143.14z9_3~143.1529_3~143.1559_5~143.1589_6~143.15b8_2~143.15e2_1~143.15gw_2~143.15jv_4~143.15mt_2~143.15pt_2~143.15t0_2~143.15w5_5~143.15z7_6~143.1626_3~143.1657_5~143.1687_6~143.16ba_3~143.16e7_2~143.16h9_7~143.16ka_3~143.16n9_2~143.16qb_4~143.16te_4~143.16we_2~143.16zf_2~143.172j_2~143.175j_3~143.178k_3~143.17bh_5~143.17ei_4~143.17hi_1~143.17kj_2~143.17nl_2~143.17qn_2~143.17tp_2~143.17wq_1~143.17zn_8~143.1834_7~143.186j_7~143.189m_7~143.18cm_4~143.18g6_5~143.18jh_c~143.18my_e~143.18qb_6~143.18te_4~143.18wk_6~143.1903_4~143.192z_2~143.1960_3~143.199c_b~143.19co_4~143.19fn_7~143.19is_2~143.19lv_j~143.19p7_2~143.19s9_2~143.19v4_4~143.19y2_2~143.1a1b_4~143.1a4i_7~143.1a7r_1~143.1aat_1~143.1adr_2~143.1agt_2~143.1ajt_4~143.1amt_4~143.1aqu_3~143.1au4_2j~143.1azm_4~143.1b2q_3~143.1b5r_2~143.1b8t_6~143.1bc4_3~143.1bfc_1~143.1big_5~143.1blf_1~143.1bom_3~143.1brx_s~143.1bvu_4~143.1byw_2~143.1c1x_1~143.1c4w_4~143.1c80_5~143.1cb5_2~143.1cei_6~143.1chl_7~143.1ckn_a~143.1co1_6~143.1crc_6~143.1cus_9~143.1cxx_8~143.1d14_5~143.1d4d_4~143.1d7l_3~143.1daq_5~143.1deb_4~143.1dhe_5~143.1dkb_c~143.1dng_2~143.1dqh_3~143.1dtj_b~143.1dwy_7~143.1e0g_d~143.1e3o_6~143.1e6t_6~143.1e9r_7~143.1ed1_5~143.1egd_4~143.1ejq_5~143.1enx_5~143.1er8_3~143.1exn_5~143.1f0t_5~143.1f3s_3~143.1f6o_2~143.1f9u_5~143.1fe8_2~143.1fhm_8~143.1fkn_7~143.1fnz_4~143.1fr6_4~143.1fud_3~143.1fxd_6~143.1g0h_a~143.1g3k_8~143.1g6s_3~143.1g9x_5&met.7=CBsQCiCZyQE4jAHAAfiuqZAI~CBsQBiCjyQE4gwHAAaT-pasP~~CBsQBiDByQE4rALAAa6MwK8F~CBsQARgBIJfKASiXygEwzcwBOLYCwAGkoPylBw~CBwQBhgBIJnMASiZzAEwts0BOJ0BaJ3MAXCxzQF4OLABAbgBA8ABlITitQ4~CBsQDSCvxAE4qwrAAYrjnZYL~CBsQCiDQxwE4vAjAAYzAo74D~CBsQCiDQ0AE4E8ABvJ2_3wQ~CBsQDSClxAE42AzAAYrjnZYL~CBsQCiCW0AE4fMABn-38yws~CBsQDSDsyAE45gjAAYrjnZYL~CBsQDSD2yAE43QjAAYrjnZYL~CBsQBiDUxwE42QvAAfWe4q0P~CBsQBiC1zwE4sQTAAbTAz-oG~CBsQBiC1zwE4sgTAAaDwn9MJ~CBsQBiC1zwE44APAAfuotsoF~CBsQBiDq0AE4kgLAAcDXh0c~CBsQCiDs0AE4NMABjLqg-wQ~CBsQCiDC0AE4qgLAAdGYoZcJ~CBsQCiCT0gE4WMABvcuD8As~CBsQCiD31AE4HMABtvLD1wk~CBsQBiC91AE4aMABjL_B2gE~CBsQCiCO1QE4IcAB-Puz5gc~CBsQBiC_1AE4ggHAAf-s-NwK~CBsQCiCP1QE4LsAB7qyO1ww~CBsQBiDA1AE4nwHAAeeV_ckL~CBsQCiCj1QE4J8ABnI32oQQ~CBsQCiCf1QE4Z8AB7u3W6AQ~CBsQBRgBILTQASiA1QEw7dUBOLkFaILVAXDl1QF4rAyAAYoEiAHfBZABtNABmAH91AGwAQG4AQPAAZON2awD~CBsQCiCZ1gE4DsABnrW11w8~CBsQCiCp1gE4E8AB_dznng0~CBsQChgBIL_WASi_1gEwxdYBOAbAAYzV2OsE~CBsQCiDy1AE4ngLAAabOoOsI~CBsQCiDx1gE4mwHAAdnrsPMK~CBsQBiCu2AE4GcABhuy-9gw~CCgQChgBIOrWASjq1gEwy9gBOOEBUOzWAVjl1wFgh9cBaOXXAXDH2AF4yQyAAa4IiAHhErABAbgBA8AB2b7apgE~CCgQChgBIO_WASjv1gEw0NgBOOEBaObXAXDL2AF48wiAAawIiAHhErABAbgBA8ABwf7TBQ~CBsQCiDA1wE4vQLAAebz9ckH~CCgQDRgBIK3YASit2AEwktsBOOUCwAHisoC5Aw~CBsQBhgBIM7YASjO2AEw99oBOKkCUNHYAVjv2QFg8tgBaPXZAXDx2gF4qASAASqIASqwAQG4AQPAAb_fx5UG~CBsQBhgBIM_YASjP2AEw8NoBOKICUNLYAVjs2QFg8tgBaPLZAXDv2gF4bIABKogBKrABAbgBA8AB-pK-_Ac~CBsQBhgBINTYASjU2AEw-NoBOKUCaPbZAXDy2gF4bIABKogBKrABAbgBA8ABur3nOA~CBsQBhgBINTYASjU2AEw79oBOJsCaPXZAXDu2gF4qASAASqIASqwAQG4AQPAAcrW25EJ~CBsQBhgBILfbASi32wEwt9wBOIABaMDbAXCk3AF4a4ABKogBKrABAbgBA8AB_-bozAs~CBsQBhgBILjbASi42wEwt9wBOIABaMLbAXCl3AF4a4ABKogBKrABAbgBA8ABpJietgY~CBsQDSCG1gE4_gbAAYjK3_8F~CBsQBiCX1gE4oQfAAe-hvOgF~CBsQBiCH1gE4swfAAbiXo7YD~CBsQBiDR2gE46QLAAe-hvOgF~CBsQBiDe2gE43QLAAe-hvOgF~~CBsQBiCo2wE4uwLAAeyoiMIG~CBsQDSCz1wE4qQfAAab929oD~CBsQCiCk3QE42QHAAZPC1pUB~CBsQBiCB4QE4sQLAAe-hvOgF~CBsQBiDm4AE4lgPAAZKLyqQL~CBsQDSCg4wE4lgfAAdG1_NkL~CBsQBSC64AE43QzAAaf63LMJ~CBsQDSCV6wE4uwLAAdG1_NkL~CBsQDSC09QE4hBLAAdG1_NkL~CBsQDSC_hwI44AvAAdG1_NkL~CBsQDSC-kwI4nAnAAdG1_NkL~CBsQDSDonAI48QbAAdG1_NkL~CBsQDSD_owI44wbAAdG1_NkL~CBsQDSCKqwI4qgLAAdG1_NkL~CBsQDSC17wM4rgLAAdG1_NkL",
            "METHOD=POST",
            INLINE_URLS,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/smallIcons.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_6", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(86.665);

        status = nsApi.ns_start_transaction("denim_jsp_2");
        status = nsApi.ns_web_url ("denim_jsp_2",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=10&d=0|1&lts=233050558&nvcounter=6",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("denim_jsp_2", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_126");
        status = nsApi.ns_web_url ("index_126",
            "URL=https://684d0d38.akstat.io/",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/sale-event/denim.jsp?&searchTerm=jeans&submit-search=web-regular", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_126", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X_6");
        status = nsApi.ns_web_url ("X_6",
            "URL=https://bat.bing.com/actionp/0?ti=4024145&tm=al001&Ver=2&mid=c104aeee-82cb-4e75-aa9a-4288b0972e34&sid=fa3fbb90ba0c11eb8ba183b721734ade&vid=fa3ffd30ba0c11eb9490814ed83fd281&vids=1&evt=pageHide",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_7");
        status = nsApi.ns_web_url ("csi_7",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=4~koy1w66c&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&uet=2&met.3=143.1gg0_3~143.1gj9_7~143.1gmc_7~143.1gpr_5~143.1gsr_6~143.1gvs_b~143.1gz9_3~143.1h27_2~143.1h59_1~143.1h88_9~143.1hba_b~143.1hes_2~143.1hhr_1~143.1hla_2~143.1ho6_7~143.1hr7_8~143.1hzh_5~143.1i2i_9~143.1i60_3~143.1i9d_3~143.1im4_1u~143.1irf_3~143.1iuj_4~143.1iyc_5~143.1j1a_2~143.1j4d_2~143.1j7d_4~143.1jag_3~143.1jds_b~143.1jh5_1~143.1jkl_4~143.1jni_4~143.1jqm_8~143.1jxp_a~143.1k0x_2~143.1k4a_8~143.1k7k_5~143.1kb0_5~143.1ke3_3~143.1kh0_d~143.1kkf_5~143.1kns_g~143.1kra_6~143.1kup_9~143.1ky9_8~143.1l1e_4~143.1l4o_7~143.1l84_2~143.1lb7_5~143.1lee_6~143.1li0_i~143.1llh_2~143.1lol_5~143.1lrq_2~143.1luu_5~143.1lxs_3~143.1m13_9~143.1m4g_f~143.1m7t_5~143.1mar_m~143.1mea_6~143.1mha_4&met.7=~CBsQARgBIK6RBCiukQQw-ZgEOMsHwAGkoPylBw~&met.1=24.1mk1",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_127");
        status = nsApi.ns_web_url ("index_127",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301616019079957&24&11&9&0&3&9&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_127", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_8");
        status = nsApi.ns_web_url ("csi_8",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=5~koy1wcg7&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&met.3=713.1mke_2~142.1mk9_8",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_9");
        status = nsApi.ns_web_url ("csi_9",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&top=1&puid=6~koy1wcgo&c=2243100800065831&e=31061226%2C21066613%2C21066615&ctx=1&uet=1&met.11=1.CITRBBAC&met.1=24.1mks",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_46");
        status = nsApi.ns_web_url ("test_rum_nv_46",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=1&CavStore=-1&pid=10&d=0|1&lts=233050558&nvcounter=7",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-3-4.css", END_INLINE,
                "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183", END_INLINE,
                "URL=https://www.kohls.com/_sec/cp_challenge/sec-cpt-3-4.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_46", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_10");
        status = nsApi.ns_web_url ("csi_10",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=2~koy1v0kf&chm=1&c=2243100800065831&ctx=2&qqid=CO6lv_-q2vACFbo-twAdkpkEZg&uet=2&met.3=734.14c_3~735.14y_1&met.7=CBsQARgBIMEEKMEEMP8LOL0H~CCAQBBgBIK8LKK8LMJEOOOICULALWPkMYOgLaPkMcI8OePUDgAEqiAEqsAEBuAED&met.1=24.1cj7",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_11");
        status = nsApi.ns_web_url ("csi_11",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=3~koy1wcnl&chm=1&c=2243100800065831&ctx=2&qqid=CO6lv_-q2vACFbo-twAdkpkEZg&uet=1&met.1=24.1cjz",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjssMVIGb-Oyh3LHyq6XJ2oMuMlYnDRSfrsAyiWPNrO-EzfQqJwQi2CwscJAmRhqjpRh5-9y0ARcnoiTMcPCultautewTApnZgL5oWQ9b0_s&sig=Cg0ArKJSzFWfTSeoYoBBEAE&id=lidartos&mcvt=62455&p=157,163,221,1187&mtos=62455,62455,62455,62455,62455&tos=62455,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,590&mc=1&app=0&itpl=3&adk=1745468209&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621585034250&dlt=0&rpt=227&isd=0&msd=0&esd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsthV6j7bn5iLqmJpkmftMTwZsxd6-sPMPBzLHJnJEwI-CzxNBmssppFfyR83c-1BxIAdo5CU8bbYek7X1ZA-rvg3t2PQrx3eVHi5jAMMzk&sig=Cg0ArKJSzF2OSFwkewVQEAE&id=lidartos&mcvt=0&p=0,0,0,0&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=0,0&mc=0&if=1&app=0&itpl=19&adk=3033452844&rs=4&met=ce&la=0&cr=0&osd=1&vs=3&rst=1621585034044&dlt=53&rpt=1174&isd=0&msd=0&r=u&fum=1", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjssWaN1exkA2IyJuzENqtnO_asHr-XaNlcD_QrqM-IskpS4p_nmtVjCZJ1gAxCTAsidCLZqzaFaYI5U6aGaSlkBcWBasUa4uGBGUiPE3CyE&sig=Cg0ArKJSzBOCXCADFbo7EAE&id=lidartos&mcvt=0&p=0,0,0,0&mtos=0,0,0,0,0&tos=0,0,0,0,0&v=20210519&bin=7&avms=nio&bs=0,0&mc=0&if=1&app=0&itpl=19&adk=636752327&rs=4&met=ce&la=0&cr=0&osd=1&vs=3&rst=1621585034048&dlt=66&rpt=1308&isd=0&msd=0&r=u&fum=1", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_11", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_12");
        status = nsApi.ns_web_url ("csi_12",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=2~koy1v1hr&chm=1&c=2243100800065831&ctx=2&qqid=COulv_-q2vACFbo-twAdkpkEZg&uet=2&met.7=CBsQARgBIKsOKKsOMN8QOLMC&met.1=24.1cn0",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_12", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_13");
        status = nsApi.ns_web_url ("csi_13",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=3~koy1wcwt&chm=1&c=2243100800065831&ctx=2&qqid=COulv_-q2vACFbo-twAdkpkEZg&uet=1&met.1=24.1cuj",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_13", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_14");
        status = nsApi.ns_web_url ("csi_14",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=2~koy1v1j2&chm=1&c=2243100800065831&ctx=2&qqid=COylv_-q2vACFbo-twAdkpkEZg&uet=2&met.7=CBsQARgBIMkOKMkOMP0QOLQC&met.1=24.1cwc",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("csi_14", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("csi_15");
        status = nsApi.ns_web_url ("csi_15",
            "URL=https://csi.gstatic.com/csi?v=2&s=pagead&action=csi_pagead&dmc=8&puid=3~koy1wczn&chm=1&c=2243100800065831&ctx=2&qqid=COylv_-q2vACFbo-twAdkpkEZg&uet=1&met.1=24.1cwk",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/public/58a664a3f9d4347948c85e668a2ed087f723b7604ced", END_INLINE,
                "URL=https://p11.techlab-cdn.com/e/65257_1825232252.js", END_INLINE,
                "URL=https://p11.techlab-cdn.com/e/65319_1825232283.js", END_INLINE,
                "URL=https://p11.techlab-cdn.com/e/64885_1825232283.js", END_INLINE,
                "URL=https://p11.techlab-cdn.com/e/65226_1825232283.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("csi_15", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("json_3");
        status = nsApi.ns_web_url ("json_3",
            "URL=https://kohls.tt.omtrdc.net/m2/kohls/mbox/json?mbox=target-global-mbox&mboxSession=d9ac3f9e28844074a6efd431d5a469f3&mboxPC=d9ac3f9e28844074a6efd431d5a469f3.31_0&mboxPage=3be4e4dc836049499c9f82e081a69a02&mboxRid=8d19164b02bc40cc908497b648664d50&mboxVersion=1.7.1&mboxCount=1&mboxTime=1621604898181&mboxHost=www.kohls.com&mboxURL=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&mboxReferrer=https%3A%2F%2Fwww.kohls.com%2F&browserHeight=607&browserWidth=1366&browserTimeOffset=330&screenHeight=768&screenWidth=1366&colorDepth=24&devicePixelRatio=1&screenOrientation=landscape&webGLRenderer=ANGLE%20(Intel(R)%20UHD%20Graphics%20620%20Direct3D11%20vs_5_0%20ps_5_0)&at_property=bb529821-b52b-bf89-2022-4492a94a6d05&customerLoggedStatus=false&tceIsRedesign=false&tceIsPDPRedesign=&tceIsCNCRedesign=True&mboxMCSDID=21B24757CD107289-4C47680D2245CA3B&vst.trk=ww9.kohls.com&vst.trks=ww8.kohls.com&mboxMCGVID=89552655867376930334316022663672800555&mboxAAMB=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&mboxMCGLH=12"
        );

        status = nsApi.ns_end_transaction("json_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("floop_24");
        status = nsApi.ns_web_url ("floop_24",
            "URL=https://api-bd.kohls.com/v1/ecs/topics/floop",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/snb/media/omniture/SkavaOmnitureCode.js", END_INLINE,
                "URL=https://www.kohls.com/snb/media/javascript/deploy/homepageR51.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("floop_24", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_39");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_39",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_39", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("config_json_6");
        status = nsApi.ns_web_url ("config_json_6",
            "URL=https://c.go-mpulse.net/api/config.json?key=4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T&d=www.kohls.com&t=5405284&v=1.720.0&if=&sl=1&si=3d6fe845-c12e-4034-9bd4-a136fdeb3d3c-qtg6zy&bcn=%2F%2F684d0d38.akstat.io%2F&plugins=AK,ConfigOverride,Continuity,PageParams,IFrameDelay,AutoXHR,SPA,History,Angular,Backbone,Ember,RT,CrossDomain,BW,PaintTiming,NavigationTiming,ResourceTiming,Memory,CACHE_RELOAD,Errors,TPAnalytics,UserTiming,Akamai,Early,EventTiming,LOGN&acao=&ak.ai=225159",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_tab_icon_retina.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("config_json_6", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_40");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_40",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://st.dynamicyield.com/st?sec=8776374&inHead=true&id=-2688648636616184714&jsession=ab65e81d522fdc88f15c6a7229406282&ref=https%3A%2F%2Fwww.kohls.com%2F&scriptVersion=1.11.2&dyid_server=-2688648636616184714", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_40", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("session_jsp_8");
        status = nsApi.ns_web_url ("session_jsp_8",
            "URL=https://www.kohls.com/web/session.jsp?lpf=v2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/wcs-internal/OmnitureAkamai.jsp", END_INLINE
        );

        status = nsApi.ns_end_transaction("session_jsp_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("uia_9");
        status = nsApi.ns_web_url ("uia_9",
            "URL=https://async-px.dynamicyield.com/uia?cnst=1&_=1621585102137",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("uia_9", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("batch_10");
        status = nsApi.ns_web_url ("batch_10",
            "URL=https://async-px.dynamicyield.com/batch?cnst=1&_=1621585102322_281598",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.kohls.com/onlineopinionV5/oo_desktop.js", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/mediumIcons.svg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ww8.kohls.com/b/ss/kohlscomprod/10/JS-2.0.0/s68216175656216?AQB=1&ndh=1&pf=1&callback=s_c_il[1].doPostbacks&et=1&t=21%2F4%2F2021%2013%3A48%3A23%205%20-330&d.&nsid=0&jsonv=1&.d&sdid=21B24757CD107289-4C47680D2245CA3B&mid=89552655867376930334316022663672800555&aamlh=12&ce=UTF-8&ns=kohls&pageName=denim&g=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&c.&k.&pageDomain=www.kohls.com&.k&mcid.&version=4.3.0&icsmcvid=-null&mcidcto=-null&aidcto=-null&.mcid&.c&cc=USD&pageType=sale%20event%20landing&products=%3Bproductmerch2&aamb=RKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y&tnt=546041%3A1%3A0%2C545299%3A1%3A0%2C531276%3A0%3A0%2C536856%3A1%3A0%2C536968%3A1%3A0%2C526083%3A0%3A0%2C&c1=no%20taxonomy&c2=no%20taxonomy&c3=no%20taxonomy&v3=browse&c4=sale%20event%20landing&c5=non-search&c7=no%20taxonomy&v8=non-search&v9=denim&c16=browse&c17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&v17=kohls%20not%20logged%20in%7Cloyalty%20not%20logged%20in&c18=fri%7Cweekday%7C03%3A00%20am&v18=fri%7Cweekday%7C03%3A00%20am&c22=2021-05-21&v22=desktop&v23=browse&v24=browse&v25=no%20taxonomy&v26=no%20taxonomy&v27=no%20taxonomy&v28=no%20taxonomy&c39=browse&c40=browse&v40=cloud17&c41=browse&c42=browse&v42=no%20cart&c50=D%3Ds_tempsess&c53=denim&c64=VisitorAPI%20Present&v68=denim&v70=d64ec31a-62cb-4949-954e-5859e4a2f695&v71=klsbrwcki%3Ad64ec31a-62cb-4949-954e-5859e4a2f695&s=1366x768&c=24&j=1.6&v=N&k=Y&bw=1366&bh=607&AQE=1", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/js/1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("batch_10", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X349_8");
        status = nsApi.ns_web_url ("X349_8",
            "URL=https://kohls.sjv.io/cur/5349",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&docReferrer=https%3A%2F%2Fwww.kohls.com%2F&H=cajn1lv&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NDA2NzQ3MzE1NDkzNDAyOTEyNA&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MzIzMjgyNzcyNzM4MDM2MzA0MA", END_INLINE,
            "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1", "METHOD=OPTIONS", END_INLINE,
                "URL=https://tjxbfc1n.micpn.com/p/cp/-1/track.gif?t=1621585104070&mi_u=anon-1621585015807-2168751633&mi_cid=8212&referrer=https%3A%2F%2Fwww.kohls.com%2F&timezone_offset=-330&event_type=pageview&cdate=1621585104066&ck=host&anon=true", END_INLINE,
                "URL=https://login.dotomi.com/ucm/UCMController?dtm_com=28&dtm_cid=2683&dtm_cmagic=8420d3&dtm_fid=101&dtm_format=6&cli_promo_id=2&dtm_email_hash=N/A&dtm_user_id=d64ec31a-62cb-4949-954e-5859e4a2f695&dtmc_department=clothing&dtmc_category=&dtmc_sub_category=not%20set", END_INLINE,
                "URL=https://apps.zineone.com/c3/api/v1/connectwebsocket/3fdb02a9-4a8e-4711-cc24-4010cd10d0b1", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621585104595&cv=9&fst=1621585104595&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621585104612&cv=9&fst=1621585104612&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621585104595&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=2916129320&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621585104612&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=3122923944&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621585104595&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=2916129320&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621585104612&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=3122923944&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://htlb.casalemedia.com/cygnus?v=7.2&s=186355&fn=headertag.IndexExchangeHtb.adResponseCallback&r=%7B%22id%22%3A68954440%2C%22site%22%3A%7B%22page%22%3A%22https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular%22%2C%22ref%22%3A%22https%3A%2F%2Fwww.kohls.com%2F%22%7D%2C%22imp%22%3A%5B%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%224%22%2C%22siteID%22%3A%22269243%22%7D%2C%22id%22%3A%221%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%223%22%2C%22siteID%22%3A%22269246%22%7D%2C%22id%22%3A%222%22%7D%2C%7B%22banner%22%3A%7B%22w%22%3A160%2C%22h%22%3A600%2C%22topframe%22%3A1%7D%2C%22ext%22%3A%7B%22sid%22%3A%225%22%2C%22siteID%22%3A%22269244%22%7D%2C%22id%22%3A%223%22%7D%5D%2C%22ext%22%3A%7B%22source%22%3A%22ixwrapper%22%7D%2C%22user%22%3A%7B%22eids%22%3A%5B%7B%22source%22%3A%22adserver.org%22%2C%22uids%22%3A%5B%7B%22id%22%3A%22e54a98e6-19ed-4f55-873b-8d02b6eb0282%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID%22%7D%7D%2C%7B%22id%22%3A%22TRUE%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_LOOKUP%22%7D%7D%2C%7B%22id%22%3A%222021-04-21T08%3A16%3A59%22%2C%22ext%22%3A%7B%22rtiPartner%22%3A%22TDID_CREATED_AT%22%7D%7D%5D%7D%5D%7D%7D", END_INLINE
        );

        status = nsApi.ns_end_transaction("X349_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_128");
        status = nsApi.ns_web_url ("index_128",
            "URL=https://hb.emxdgt.com/?t=1348&ts=1621585105306",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_128", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_14");
        status = nsApi.ns_web_url ("bidRequest_14",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_right&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_14", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("translator_5");
        status = nsApi.ns_web_url ("translator_5",
            "URL=https://hbopenbid.pubmatic.com/translator?source=index-client",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("translator_5", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("prebid_3");
        status = nsApi.ns_web_url ("prebid_3",
            "URL=https://ib.adnxs.com/ut/v3/prebid",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("prebid_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_14");
        status = nsApi.ns_web_url ("auction_14",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_300x250_hdx_header&lib=ix&size=300x250&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=2.1.2&tmax=1348"
        );

        status = nsApi.ns_end_transaction("auction_14", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_14");
        status = nsApi.ns_web_url ("fastlane_json_14",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811818&kw=rp.fastlane&tk_flint=index&rand=0.05865449272468237"
        );

        status = nsApi.ns_end_transaction("fastlane_json_14", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("auction_15");
        status = nsApi.ns_web_url ("auction_15",
            "URL=https://tlx.3lift.com/header/auction?inv_code=kohls_d_160x600_hdx_header&lib=ix&size=160x600&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=2.1.2&tmax=1348"
        );

        status = nsApi.ns_end_transaction("auction_15", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_15");
        status = nsApi.ns_web_url ("fastlane_json_15",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=9&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811828&kw=rp.fastlane&tk_flint=index&rand=0.8949670435425714"
        );

        status = nsApi.ns_end_transaction("fastlane_json_15", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("fastlane_json_16");
        status = nsApi.ns_web_url ("fastlane_json_16",
            "URL=https://fastlane.rubiconproject.com/a/api/fastlane.json?account_id=22726&size_id=15&rp_floor=0.01&rf=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&p_screen_res=1366x768&site_id=343126&zone_id=1811816&kw=rp.fastlane&tk_flint=index&rand=0.8391789757228707"
        );

        status = nsApi.ns_end_transaction("fastlane_json_16", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_15");
        status = nsApi.ns_web_url ("bidRequest_15",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=d_btf_left_160x600&secure=1"
        );

        status = nsApi.ns_end_transaction("bidRequest_15", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("bidRequest_16");
        status = nsApi.ns_web_url ("bidRequest_16",
            "URL=https://c2shb.ssp.yahoo.com/bidRequest?cmd=bid&dcn=8a9690cf017272e27aa0e2b5cbe0000d&pos=ros_300x250_mrec_left&secure=1",
            INLINE_URLS,
                "URL=https://ap.lijit.com/rtb/bid?callback=window.headertag.SovrnHtb.adResponseCallback&br=%7B%22id%22%3A%22_ZLgXG9Zt%22%2C%22site%22%3A%7B%22domain%22%3A%22www.kohls.com%22%2C%22page%22%3A%22%2Fsale-event%2Fdenim.jsp%22%7D%2C%22imp%22%3A%5B%7B%22id%22%3A%224qpz1p2q%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788162%22%7D%2C%7B%22id%22%3A%22P26oRsst%22%2C%22banner%22%3A%7B%22w%22%3A300%2C%22h%22%3A250%7D%2C%22tagid%22%3A%22788160%22%7D%2C%7B%22id%22%3A%22B0GcKJbb%22%2C%22banner%22%3A%7B%22w%22%3A160%2C%22h%22%3A600%7D%2C%22tagid%22%3A%22788163%22%7D%5D%7D", END_INLINE,
                "URL=https://adservice.google.co.in/adsid/integrator.js?domain=www.kohls.com", END_INLINE,
                "URL=https://adservice.google.com/adsid/integrator.js?domain=www.kohls.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("bidRequest_16", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("headerstats_3");
        status = nsApi.ns_web_url ("headerstats_3",
            "URL=https://as-sec.casalemedia.com/headerstats?s=186355&u=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&v=3",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://securepubads.g.doubleclick.net/gampad/ads?gdfp_req=1&pvsid=1272302160397166&correlator=2467886272188376&output=ldjh&impl=fifs&eid=31061226%2C44741898%2C31060840&vrg=2021051801&ptt=17&sc=1&sfv=1-0-38&ecs=20210521&iu_parts=17763952%2Cclothing&enc_prev_ius=%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1%2C%2F0%2F1&prev_iu_szs=1x1%2C160x600%2C300x250%2C300x250%2C320x50%7C1024x45%7C1024x64%7C1024x128&fluid=0%2C0%2C0%2C0%2Cheight&ists=16&prev_scp=pos%3Dwallpaper%7Cpos%3Dleft%7Cpos%3Dbottomleft%7Cpos%3Dbottomright%7Cpos%3Dmarquee&cust_params=pgtype%3Dsevent%26pgname%3Ddenim.jsp%26channel%3Ddesktop%26env%3Dprod&cookie=ID%3D793e49e62c912a30%3AT%3D1621585020%3AS%3DALNI_MYdNaRVnrPde_Bw_hEDdZzzjfW6hw&bc=31&abxe=1&lmt=1621585106&dt=1621585106230&dlt=1621585097034&idt=8162&frm=20&biw=1349&bih=590&oid=3&adxs=0%2C-12245933%2C-12245933%2C-12245933%2C163&adys=0%2C-12245933%2C-12245933%2C-12245933%2C153&adks=4004583564%2C3033452844%2C636752327%2C2623260883%2C1745468209&ucis=1%7C2%7C3%7C4%7C5&ifi=1&u_tz=330&u_his=3&u_java=false&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_sd=1&flash=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&vis=1&dmc=8&scr_x=0&scr_y=0&psz=1349x4828%7C0x0%7C0x0%7C0x0%7C1349x25&msz=1349x0%7C0x0%7C0x0%7C0x0%7C1024x25&ga_vid=1014052125.1621585033&ga_sid=1621585106&ga_hid=374674568&ga_fc=true&fws=0%2C128%2C128%2C128%2C0&ohw=0%2C0%2C0%2C0%2C0&btvi=0%7C-1%7C-1%7C-1%7C0", END_INLINE,
                "URL=https://ef4d2113e8dbdf2b41b9d571f753ce62.safeframe.googlesyndication.com/safeframe/1-0-38/html/container.html", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjst4vM3F0CqvoVKmpXzItpg2wvptoyqWalB4Bzva4PT9_2EPXlHh65BKop8Ba6VC6KwBIyiy9-9cY17YzTUGkkz-vUcbta7ow58Rio4j9Rr19r0Xj13d11s2pq9hyzu2ujb-yY0B0CLRg7x2vHzcgkhiZAHoE2JqUdmFhPKQ8jkl6AhvK9lDqELxx6WGZLgKdZRT3J4_cGCHS2XZvRDito2xbXrPe7I9cKUiaFvdBKkIgfdNIXQ2oiMO4Yi66A-xbf5pk85WJfAh84MDBvJSu79RxrZ7E528AlTjWK4lhhCKeB1w7Do&sig=Cg0ArKJSzCOPmKpBuK-gEAE&adurl=", END_INLINE,
                "URL=https://tpc.googlesyndication.com/simgad/610125684829382582", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138345558598&adorderid=2839226817&adunitid=16771032&advertiserid=32614632&lineitemid=5657461629&rand=1182586926", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjssmSH_l-np0Vk6PPipH7VncHQI2W6OguH-8RXAb3IR-YxyMoeZB5fIdOQpMBHiskcVFrOK0mOD7MFBiYfp1_lf6Pe-r01BXMatiLNMrvUSLsVqrnAthslRT4q3JKSwLvaVV0BpqCVn1iIYF7aUVhFy8OOB9gPV1G_YpCRjUW-iIlFlMAcJgC3afel8qMrj5iQWeYW8RDvLb5ZzhJJ63EHG54E7CpZisauMqElST8FDXhoEUoqno0Yv3Wk5-1HdgXr-6HWza5eih1XMi60u2K1lAZlK09sY0giG0&sig=Cg0ArKJSzD-xjw7C9g3wEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsuNzNeztoTWHVCgz9kLSZJuPyKBKun32LQVxj0nws0N3JFR9VjygSCIrBY0iTDEJqVFRL8Jen4xMN-KWckDIKALll0nNFxYchAsbsmd5flvOVy4Qfb6pbMXwGbFtqIWK_8LkUn9kc6NLN42OsOFU9Mh_WcaanH7arSlrP1Pmz4EBhWqfXPLVr_X8f01k65ZvWf5GBMb8vr8_RUU_br_ycbnLLHZ5KyUMxQox4CzMXfZWizH562PsF6D83VDqz5Wyt8ZLE7QAdyq858sYalQ4jKrw5BEYlQlAeg&sig=Cg0ArKJSzCRTv_1blwNNEAE&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307654931&adorderid=2682029031&adunitid=16771032&advertiserid=32614632&lineitemid=5338295166&rand=903157975", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjsutzSUc0qsVgD1sHyAWiTIjbN6-BBp7bdJni-DtBpLSw-9BKgYZlfd9xSZfTZW4tZvQ8AE8b5Z9upuAr5DW4C0r8t5IkdkNlabZObQtH2F_RzIS0cE6jTlVsAaj0ZQd89rJuI7iHizSU-YKeWd0oKHNAI93Lnkt8TEK2Rcy-NqcsX2fdBAA0AzjWHVfODoYi7jOwKZmkfVJXKQsFAsBUyxXaS1KWo2OgNdIWHCeVEEyPY0KziGL6ST6iGpurDCAhSsB1SjVqU35r0xH16xGUmglmDNxWdSwevrJFFg&sig=Cg0ArKJSzOE_axIOQAEgEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstlnfg1wkoFB9l23xTiFV5WJpiWMIQETLc5e1KK1EqwfdsypzwX_8RyCS_4MicwkKckExwvp3WHYVm8zdJ1yHIEe9qDczQx1uigCCrczvl7NeVkF3ZwwpxmEOu28wOT-bY9LyD2t-hPwLOTg5wHwE2OL7KoQsR4MFYi1oMxtDLDuyHnFP288jfeOz_dOIEE1P71xb6DW5JpOnmtOX8Wksa3I5CzioAL61Nv816Ss9fBqM0imAqRyITPICsrMvI5lmCCNOhKcveMWW2RbwuZ1BEnMV369ogkIhqm&sig=Cg0ArKJSzIGrpvbOZJSVEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://s.thebrighttag.com/px?site=4DPyaxM&referrer=onsitead%3Adesktop&adcreativeid=138307873913&adorderid=2682029031&adunitid=16771032&advertiserid=32614632&lineitemid=5338295166&rand=1761551266", END_INLINE,
                "URL=https://securepubads.g.doubleclick.net/pcs/view?xai=AKAOjstFQo_0vhVbO1wlbwG-n8uUYVPqcUpE6rsyKWpwTJ24QCOJwo0bLcWX6H4MU8gO7siU-HzE8XNm4ghMfYfvgSTFAzIigoCx_YUHYB_RQdQJgqdU4pAXKqp4kDtgovx_zsEko3dcPJ-D3VSFEXvDdYD71RSb0nkkomxQYDkld6eQzftn938zaA2HsBgEbtm2NUmD91ZRjxygEdAg0aXu_EijHswjrdfC8-tq2YWG58YWnF02J0SiJlvE7w2QiNFPweTqiQ9PsS6Y6ZKwnhV2VuALvdc2FJX-UJ1LTRk&sig=Cg0ArKJSzJ6LC9Bk661sEAE&urlfix=1&adurl=", END_INLINE,
                "URL=https://pagead2.googlesyndication.com/pcs/activeview?xai=AKAOjsvbVVu5w_nc1gsi4f_GFwXt86I-TKK-MBxvF3yav94iGdqd7F0jCkvvUMJwKJbIZj-uQ75NZWplw7Oh5E3Iq6XgCqQ8m4O8URbupg29WO8&sig=Cg0ArKJSzAzSNwjijQE4EAE&id=lidar2&mcvt=1001&p=157,163,221,1187&mtos=1001,1001,1001,1001,1001&tos=1001,0,0,0,0&v=20210519&bin=7&avms=nio&bs=1349,590&mc=1&app=0&itpl=3&adk=1745468209&rs=4&met=mue&la=0&cr=0&osd=1&vs=4&rst=1621585107633&dlt=0&rpt=673&isd=0&msd=0&r=v&fum=1", END_INLINE,
                "URL=http://localhost:9222/devtools/Images/treeoutlineTriangles.svg", "HEADER=Origin:http://localhost:9222", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://bid.g.doubleclick.net/xbbe/pixel?d=KAE", END_INLINE
        );

        status = nsApi.ns_end_transaction("headerstats_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("sodar_4");
        status = nsApi.ns_web_url ("sodar_4",
            "URL=https://pagead2.googlesyndication.com/getconfig/sodar?sv=200&tid=gpt&tv=2021051801&st=env",
            INLINE_URLS,
                "URL=https://dpm.demdex.net/ibs:dpid=399555&dpuuid=b7a8493f-6f43-4a3c-8447-b6cf8de0f8b3&redir=https%3A%2F%2Fs.thebrighttag.com%2Fcs%3Faam_uuid%3D%24%7BDD_UUID%7D%26btt%3D0%26tp%3DdT9Y2Vu%26gdpr%3D0", "REDIRECT=YES", "LOCATION=https://s.thebrighttag.com/cs?aam_uuid=84622417935889388173660587809767566339&btt=0&tp=dT9Y2Vu&gdpr=0", END_INLINE,
                "URL=https://s.thebrighttag.com/tag?site=4DPyaxM&H=cajn1lv&referrer=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&docReferrer=https%3A%2F%2Fwww.kohls.com%2F&mode=v2&cf=4847939%2C6706303%2C6706308&btpdb.4DPyaxM.dGZjLjYyMTAxMDM=REFZUw&btpdb.4DPyaxM.dGZjLjYyMTAxMTA=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gc2Vzc2lvbg=NDA2NzQ3MzE1NDkzNDAyOTEyNA&btpdb.4DPyaxM.dGZjLjYyMDYyMTU=REFZUw&btpdb.4DPyaxM.Y3VzdG9tZXIgfCBzaWduYWwgMXN0IHBhcnR5IGlkIC0gMzY1IGRheXM=MzIzMjgyNzcyNzM4MDM2MzA0MA", END_INLINE,
                "URL=https://sync.mathtag.com/sync/js?gdpr=0&redir=https%3A%2F%2Fs.thebrighttag.com%2Fcs%3Ftp%3Dmm%26uid%3D%5BMM_UUID%5D%26btt%3DoH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA", END_INLINE,
                "URL=https://tpc.googlesyndication.com/sodar/sodar2/222/runner.html", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?aam_uuid=84622417935889388173660587809767566339&btt=0&tp=dT9Y2Vu&gdpr=0", END_INLINE,
                "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&op=timing&pi=1&CavStore=-1&pid=10&d=1|0|-1|5|509|0|53|22|353|163|235|36604|23|155|37303|4159|10|5359|0|265|https%3A%2F%2Fwww.kohls.com%2F|https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular|www.kohls.com|VisitorId%3Dd64ec31a-62cb-4949-954e-5859e4a2f695%3B%20CavNV%3D4679721886262234957-33333-233050621967-0-9-0-1-145-28141-26566%3B%20CavNVC%3D001000944203143842839-1-233050483-1%3B%20CavSF%3D2%2C%2C1%3A0%3A0%2C1621585046310%2C0%25PIPE%25%2C%2C%2C%2C1%2C%2C%2C1%2C%2C%2C%2C%2C%2C0%3B%20AKA_RV2%3D22%3B%20AKA_CNC2%3DTrue%3B%20akacd_www-kohls-com-mosaic-p2%3D2177452799~rv%3D19~id%3D67b39543b0766db2c63209c66c15bf3d%3B%20mosaic%3Dgcpb%3B%20at_check%3Dtrue%3B%20AMCVS_F0EF5E09512D2CD20A490D4D%2540AdobeOrg%3D1%3B%20s_ecid%3DMCMID%25PIPE%2589552655867376930334316022663672800555%3B%20_dy_csc_ses%3Dt%3B%20_dy_c_exps%3D%3B%20_dycnst%3Ddg%3B%20_dyid%3D-2688648636616184714%3B%20_dyjsession%3Dab65e81d522fdc88f15c6a7229406282%3B%20dy_fs_page%3Dwww.kohls.com%3B%20_dycst%3Ddk.w.c.ms.%3B%20_dy_geo%3DIN.AS.IN_RJ.IN_RJ_Jaipur%3B%20_dy_df_geo%3DIndia..Jaipur%3B%20s_fid%3D2720B244721C920B-0ADF8DB34CC7F370%3B%20s_cc%3Dtrue%3B%20_dyid_server%3D-2688648636616184714%3B%20_mibhv%3Danon-1621585015807-2168751633_8212%3B%20IR_gbd%3Dkohls.com%3B%20s_vi%3D%5BCS%5Dv1%25PIPE%253053B63CFBE99B85-4000109CD0C6191D%5BCE%5D%3B%20_gcl_au%3D1.1.1885231748.1621585018%3B%20SignalSpring2016%3DB%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMDM%3DREFZUw%3B%20btpdb.4DPyaxM.dGZjLjYyMTAxMTA%3DREFZUw%3B|0|101|-1|24|24|WINDOWS|Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F56.0.2924.87%20Safari%2F537.36|en-GB|%5Bobject%20PluginArray%5D|Mozilla|0|PC|56.0|10|33333|1|0|1|%7B-1%7D|4159|001000944203143842839|6044|502330|0|0|0|2922|2922|0|0|4.5.0_3aeab9|3b396b&lts=233050483&d2=-1|-1|-1|2|100|0|1", END_INLINE,
                "URL=https://i.liadm.com/s/63989", END_INLINE,
                "URL=https://s.thebrighttag.com/cs?tp=mm&uid=4b6160a7-6c9c-4b00-adb0-67cdff2b1c0c&btt=oH3oRdDtkhb3BUZ2VXTfadFG_ezPX-BeuU5DBBtTeBA&gdpr=0&gdpr_consent=", END_INLINE
        );

        status = nsApi.ns_end_transaction("sodar_4", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_129");
        status = nsApi.ns_web_url ("index_129",
            "URL=https://684d0d38.akstat.io/",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_129", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_47");
        status = nsApi.ns_web_url ("test_rum_nv_47",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=domwatcher&pi=2&CavStore=-1&pid=10&d=10|0|2&lts=233050610",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_47", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("tag_8");
        status = nsApi.ns_web_url ("tag_8",
            "URL=https://tagtracking.vibescm.com/tag",
            INLINE_URLS,
                "URL=https://bat.bing.com/bat.js", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;src=8632166;type=landi0;cat=unive0;ord=5539913513555;gtm=2od5c1;auiddc=1885231748.1621585018;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular?", END_INLINE
        );

        status = nsApi.ns_end_transaction("tag_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_48");
        status = nsApi.ns_web_url ("test_rum_nv_48",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&op=pagedump&pi=2&CavStore=-1&pid=10&d=10|2|0&lts=233050610&nvcounter=2",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://pagead2.googlesyndication.com/pagead/gen_204?id=sodar2&v=222&t=2&li=gpt_2021051801&jk=1272302160397166&bg=!XF-lXxvNAAZ7hX_Ue4U7ACkAdvg8WgeojE4xqBqhZnFx4fDjvWPXlF6lKUqMSSjjiN6JpMLxFVWAcgIAAADiUgAAAAxoAQcKAFpuI3j0gDo7HaBNLuxw6RAfzzhWd_TZDmw3ZPpNB6LCBRuG4AALHndjB29PBHGQOdu2HDElTAWzWe2qgy0cF30sdoOKmLF1RphxXlQqnqw0gc_dlkrg2hI7Bm2ZAgri3VXL8pc_LbhHnfmT8h9QBNB2rYYoy_DZE_SvGKG17Yt9L9bEBRSklrl74-eAUaaoVZp9HO-e-BieTY7yeEwafhDCPqzN03EZuNsapwolLirwV0NNBStAByTuMCEf7ffSFr18idC653Gl9VsBnY0epKov0CT8dI55eyCEZTalMT8nOV4lujhfAp2Sc2iyOf_oHhpUO9lzgtqV0SgRxMxtSYR5siHNxO0nH4IqkNuUL1GbAN-XzI62QTtSsUP2BDQeW1ytKoYX-1MLrxRQlyIVKS4OkPbJw6IjCbbAUMgJ9YQAZrntj2PqdPqpH4pSIgW1ZvNe7qHzdCjCNh2R2fOfKCCOl-aWMN7dVEIjhFwCgDKOsvjSSZ78aGTAnEuC4NNx3vhwxgy4vrdHPMQ3QAuBanlll73b09b2Bj5CRvu5ZS4A4aEYUlGljbXXfC-NV5U_ct1fxZaUHAu9emeKd_ORYVZxcldDQbP9-jb2ItJkPgFgb599hBOzqbJlQW5heUDJPAefshCXnLBeW9TAQ1AB61Cmcp9di6OR5P3YOcJMnqHmBhuxANFr9zS_YWT1rW8MdC033eOpz704DaWVe_GcDM2Da1D_0FrMJS2mVXycB2kNDcZiXUP7oxdpmFc7JHO0X70799T5nV7Xu3yiuFapMtVKL-4r2C4m_TYE3gk4FWAxZylt2Oh-maU", END_INLINE,
                "URL=https://8632166.fls.doubleclick.net/activityi;dc_pre=CIDziLGr2vACFQKdjwodPfYBgQ;src=8632166;type=landi0;cat=unive0;ord=5539913513555;gtm=2od5c1;auiddc=1885231748.1621585018;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular?", END_INLINE,
                "URL=https://adservice.google.com/ddm/fls/z/dc_pre=CIDziLGr2vACFQKdjwodPfYBgQ;src=8632166;type=landi0;cat=unive0;ord=5539913513555;gtm=2od5c1;auiddc=*;u1=not%20set;u10=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp;u11=N%2FA;u13=not%20set;u2=%25AdOps_PDP_All_ProductName%25;u23=New%20Customer;u24=89552655867376930334316022663672800555;u25=d64ec31a-62cb-4949-954e-5859e4a2f695;u5=%25AdOps_Cart_All_NumberOfItems%25;~oref=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_48", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_41");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_41",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_41", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_42");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_42",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_42", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("X8a664a3f8rn1917698e839c9036_43");
        status = nsApi.ns_web_url ("X8a664a3f8rn1917698e839c9036_43",
            "URL=https://www.kohls.com/kkhepdqgay/58a664a3f8rn1917698e839c90363183",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://bat.bing.com/action/0?ti=4024145&tm=al001&Ver=2&mid=c0899387-ff67-45dd-bf23-c6e482369568&sid=fa3fbb90ba0c11eb8ba183b721734ade&vid=fa3ffd30ba0c11eb9490814ed83fd281&vids=0&pi=0&lg=en-GB&sw=1366&sh=768&sc=24&p=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&r=https%3A%2F%2Fwww.kohls.com%2F&lt=37458&pt=1621585096336,400,635,,,7,7,7,12,87,65,88,353,516,698,4159,4159,4182,37302,37303,37458&pn=1,0&evt=pageLoad&msclkid=N&sv=1&rn=852180", END_INLINE,
                "URL=https://sc-static.net/scevent.min.js", END_INLINE,
                "URL=https://tr.snapchat.com/cm/i?pid=8e9bd9e0-8284-4dbd-ab20-145759728098", END_INLINE,
                "URL=https://d.agkn.com/pixel/10107/?che=422964177&mcvisid=89552655867376930334316022663672800555", END_INLINE
        );

        status = nsApi.ns_end_transaction("X8a664a3f8rn1917698e839c9036_43", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_49");
        status = nsApi.ns_web_url ("test_rum_nv_49",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=2&CavStore=-1&pid=10&d=1|1&lts=233050610&nvcounter=0",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://bid.g.doubleclick.net/xbbe/pixel?d=KAE", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1071871169/?random=1621585139247&cv=9&fst=1621585139247&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://sc-static.net/js-sha256-v1.min.js", END_INLINE,
                "URL=https://googleads.g.doubleclick.net/pagead/viewthroughconversion/1018012790/?random=1621585139337&cv=9&fst=1621585139337&num=1&bg=ffffff&guid=ON&resp=GooglemKTybQhCsO&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&ig=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&hn=www.googleadservices.com&async=1&rfmt=3&fmt=4", END_INLINE,
                "URL=https://tr.snapchat.com/p", END_INLINE
        );

        status = nsApi.ns_end_transaction("test_rum_nv_49", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_130");
        status = nsApi.ns_web_url ("index_130",
            "URL=https://ing-district.clicktale.net/ctn_v2/auth/?pid=24&uid=3301616019079957&as=1&1051623733&skiprnd=1&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://c.clicktale.net/pageEvent?value=MIewdgZglg5gXAAgEoFMA2KCGBnFB9AJgAYCBGIgVkqAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=2&r=807320", END_INLINE,
                "URL=https://c.clicktale.net/pageview?pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&lv=1621585048&lhd=1621585048&hd=1621585139&pn=2&re=1&dw=1366&dh=5432&ww=1366&wh=607&sw=1366&sh=768&dr=https%3A%2F%2Fwww.kohls.com%2F&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&uc=1&la=en-GB&cvars=%7B%221%22%3A%5B%22Page%20Name%22%2C%22denim%22%5D%7D&cvarp=%7B%221%22%3A%5B%22Page%20Name%22%2C%22denim%22%5D%7D&v=10.8.6&r=974850", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMAmBsBmBTAHAFgKwFoDGBGATIpqgAySqYCcARtBQIa6QLSLUyTZAAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=2&r=385044", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsAmBsAsCmBjAzARgIYFpoCZECNNYBOEzYgVgUwoA4Lj5Z0cAzaSoAA%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=2&r=185141", END_INLINE,
                "URL=https://www.google-analytics.com/analytics.js", END_INLINE,
                "URL=https://c.clicktale.net/pageEvent?value=AISQIsDMkAwIwDZHwJwwOwpQVnUAAA%3D%3D&isETR=false&v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=2&r=366628", END_INLINE,
                "URL=https://c.clicktale.net/dvar?v=10.8.6&pid=2399&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&pn=2&dv=N4IgygpghgTgxgCwAQAUEygZwiAXCAOwHsCBabWREAGhAAcYi6BGAdjxAGsiEAbTJMQAuSXkQDm4iABMkASwIAfMQE8ovISsFERYyTPkEaIAIIAhAPomAKlbNJSSACIBZJNYBKADgAsABgBOByQPaF53OQBbCCRISmQUCBhMEnU5AC8OP2Z3CEwhY3MrWwANawACx1d3Dx8A5i9gspCIAHdYaQEUOTERACY%2FPuZSPwBWEZ8snO7epGY%2FPwBSQssbC2bHACk%2B4MgRADVYJAAzIhgkMwgCJAAJAFdj48ioa8cXWE4kADk2jgBRAAedCSciucBiJhWxTswSc3wAwn4AMxjXbwgAywT%2Bzx6SAA8nQRABJa5gBByOh0BTiJBTJAmXjhIQIGJmF4EJKYKFrcywhHIgKjNGYxwAaR4%2FCQ8I6SAA4mIAEbqW7QaR0lAwCAARzuyvRCk41O5pWssLcoSYV1iQjOMX1%2BVQHjxcIGAxG4z8k3wgOBMFBBHB9JAAF8gA&r=411804", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1071871169/?random=1621585139247&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1348882922&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_130", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("j_7");
        status = nsApi.ns_web_url ("j_7",
            "URL=https://rp.liadm.com/j?tna=v2.0.1&aid=a-00oc&wpn=lc-bundle&pu=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&refr=https%3A%2F%2Fwww.kohls.com%2F&ext_s_ecid=MCMID%7C89552655867376930334316022663672800555&ext_s_vi=%5BCS%5Dv1%7C3053B63CFBE99B85-4000109CD0C6191D%5BCE%5D&ext_IXWRAPPERLiveIntentIp=%7B%22t%22%3A1621585019385%2C%22d%22%3A%7B%22response%22%3A%22pass%22%2C%22version%22%3A%221.1.1%22%7D%2C%22e%22%3A1621671419385%7D&duid=0b10d8358f40--01f6700cpcqvx6swnfx7rby23x&se=e30&dtstmp=1621585139663",
            INLINE_URLS,
                "URL=https://i.liadm.com/s/c/a-00oc?s=MgUIBhC-DzIFCAoQvg8yBQh6EL0PMgYIiwEQvg8yBQgLEL4PMgUICxC-DzIFCHkQvQ8&cim=&ps=true&ls=true&duid=0b10d8358f40--01f6700cpcqvx6swnfx7rby23x&ppid=0&euns=0&ci=0&version=sc-v0.2.0&nosync=false&monitorExternalSyncs=false&", END_INLINE,
                "URL=https://www.google.co.in/pagead/1p-user-list/1071871169/?random=1621585139247&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1348882922&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE
        );

        status = nsApi.ns_end_transaction("j_7", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("collect_3");
        status = nsApi.ns_web_url ("collect_3",
            "URL=https://stats.g.doubleclick.net/j/collect?t=dc&aip=1&_r=3&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=1476226454&gjid=1477346695&_gid=64734012.1621585048&_u=QCCAgAABAAAAAE~&z=1951024631",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://www.google.co.in/pagead/1p-user-list/1018012790/?random=1621585139337&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1360077532&resp=GooglemKTybQhCsO&rmt_tld=1&ipr=y", END_INLINE,
                "URL=https://www.google.com/pagead/1p-user-list/1018012790/?random=1621585139337&cv=9&fst=1621584000000&num=1&bg=ffffff&guid=ON&eid=376635471%2C2505059650&u_h=768&u_w=1366&u_ah=728&u_aw=1366&u_cd=24&u_his=3&u_tz=330&u_java=false&u_nplug=0&u_nmime=0&gtm=2oa5c1&sendb=1&data=event%3Dgtag.config&frm=0&url=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ref=https%3A%2F%2Fwww.kohls.com%2F&async=1&fmt=3&is_vtc=1&random=1360077532&resp=GooglemKTybQhCsO&rmt_tld=0&ipr=y", END_INLINE,
                "URL=https://www.google.co.in/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=1476226454&_u=QCCAgAABAAAAAE~&z=485276769", END_INLINE,
                "URL=https://www.google.com/ads/ga-audiences?t=sr&aip=1&_r=4&slf_rd=1&v=1&_v=j90&tid=UA-45121696-1&cid=1014052125.1621585033&jid=1476226454&_u=QCCAgAABAAAAAE~&z=485276769", END_INLINE,
                "URL=https://www.google-analytics.com/collect?v=1&_v=j90&a=374674568&t=pageview&_s=1&dl=https%3A%2F%2Fwww.kohls.com%2Fsale-event%2Fdenim.jsp%3F%26searchTerm%3Djeans%26submit-search%3Dweb-regular&ul=en-gb&de=UTF-8&sd=24-bit&sr=1366x768&vp=1349x590&je=0&_u=QCCAgAAB~&jid=1476226454&gjid=1477346695&cid=1014052125.1621585033&tid=UA-45121696-1&_gid=64734012.1621585048&cd2=d64ec31a-62cb-4949-954e-5859e4a2f695&cd4=&z=494642324", END_INLINE,
                "URL=https://sli.kohls.com/baker?dtstmp=1621585140719", END_INLINE
        );

        status = nsApi.ns_end_transaction("collect_3", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_131");
        status = nsApi.ns_web_url ("index_131",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&0&0&0&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://stags.bluekai.com/site/42614?id=51e452ef-39b5-42e8-b7ed-83178ae8eaea", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_131", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_132");
        status = nsApi.ns_web_url ("index_132",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&1&0&1&8&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://aa.agkn.com/adscores/g.pixel?sid=9212291498&_puid=51e452ef-39b5-42e8-b7ed-83178ae8eaea", END_INLINE,
                "URL=https://pixel-sync.sitescout.com/dmp/pixelSync?nid=12&rurl=https%3A%2F%2Fi.liadm.com%2Fs%2F35758%3Fbidder_id%3D2380%26bidder_uuid%3D%7BuserId%7D", END_INLINE,
                "URL=https://loadus.exelator.com/load/?p=204&g=661&j=0", END_INLINE,
                "URL=https://pixel-sync.sitescout.com/dmp/pixelSync?cookieQ=1&nid=12&rurl=https%3A%2F%2Fi.liadm.com%2Fs%2F35758%3Fbidder_id%3D2380%26bidder_uuid%3D%7BuserId%7D", END_INLINE,
                "URL=https://i.liadm.com/s/35637?bidder_id=100905&amp;bidder_uuid=164570703793000811338", END_INLINE,
                "URL=https://mid.rkdms.com/bct?pid=bcccb40a-06d2-44fe-bdd2-a91ef4a5bfd0&&puid=51e452ef-39b5-42e8-b7ed-83178ae8eaea&liid=&_ct=im", END_INLINE,
                "URL=https://i.liadm.com/s/35758?bidder_id=2380&bidder_uuid=2d51d0d9-fb43-4b72-8010-2804946b34bf-60a76cf6-494e", END_INLINE,
                "URL=https://b1sync.zemanta.com/usersync/liveintent/?cb=%2F%2Fi.liadm.com%2Fs%2F35004%3Fbidder_id%3D98254%26bidder_uuid%3D__ZUID__", "REDIRECT=YES", "LOCATION=https://stags.bluekai.com/site/23178?id=UELIl_hC4nHu4t6Bs09O&redir=https%3A%2F%2Fb1sync.zemanta.com%2Fusersync%2Fbluekai%2Fcallback%2F%3Fd%3DF4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5KVCUYSLML5UEGNDOJB2TI5BWIJZTAOKPEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", END_INLINE,
                "URL=https://i.liadm.com/s/19948?bidder_id=178256&bidder_uuid=a62186e477e7c8816c7ab52ec28b8769", "REDIRECT=YES", "LOCATION=https://x.bidswitch.net/sync?dsp_id=42&user_id=", END_INLINE,
                "URL=https://stags.bluekai.com/site/23178?id=UELIl_hC4nHu4t6Bs09O&redir=https%3A%2F%2Fb1sync.zemanta.com%2Fusersync%2Fbluekai%2Fcallback%2F%3Fd%3DF4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5KVCUYSLML5UEGNDOJB2TI5BWIJZTAOKPEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", "REDIRECT=YES", "LOCATION=https://b1sync.zemanta.com/usersync/bluekai/callback/?d=F4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5KVCUYSLML5UEGNDOJB2TI5BWIJZTAOKPEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_132", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_133");
        status = nsApi.ns_web_url ("index_133",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&2&1&0&104&subsid=233441&msgsize=120",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://b1sync.zemanta.com/usersync/bluekai/callback/?d=F4XWSLTMNFQWI3JOMNXW2L3TF4ZTKMBQGQ7WE2LEMRSXEX3JMQ6TSOBSGU2CMYTJMRSGK4S7OV2WSZB5KVCUYSLML5UEGNDOJB2TI5BWIJZTAOKPEZSXQY3IMFXGOZJ5NRUXMZLJNZ2GK3TU", "REDIRECT=YES", "LOCATION=//i.liadm.com/s/35004?bidder_id=98254&bidder_uuid=UELIl_hC4nHu4t6Bs09O", END_INLINE,
                "URL=https://i.liadm.com/s/35004?bidder_id=98254&bidder_uuid=UELIl_hC4nHu4t6Bs09O", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_133", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_134");
        status = nsApi.ns_web_url ("index_134",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&3&1&1&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_134", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_135");
        status = nsApi.ns_web_url ("index_135",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&4&1&2&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_135", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_136");
        status = nsApi.ns_web_url ("index_136",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&5&1&3&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_136", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_137");
        status = nsApi.ns_web_url ("index_137",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&6&1&4&104&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_137", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_138");
        status = nsApi.ns_web_url ("index_138",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&7&1&5&105&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_138", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_50");
        status = nsApi.ns_web_url ("test_rum_nv_50",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=useraction&pi=2&CavStore=-1&pid=10&d=1|1&lts=233050620&nvcounter=3",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_50", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_51");
        status = nsApi.ns_web_url ("test_rum_nv_51",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=el&pi=2&CavStore=-1&pid=10&d=10&lts=233050620&nvcounter=4",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_51", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("test_rum_nv_52");
        status = nsApi.ns_web_url ("test_rum_nv_52",
            "URL=https://mon1.kohls.com/test_rum_nv?s=001000944203143842839&p=1&m=0&op=domwatcher&pi=2&CavStore=-1&pid=10&d=10|0|2&lts=233050620",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("test_rum_nv_52", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("events_8");
        status = nsApi.ns_web_url ("events_8",
            "URL=https://c.clicktale.net/events?v=10.8.6&str=610&di=4071&dc=37214&fl=37370&sr=11&mdh=5418&pn=2&re=1&uu=536e3d87-c26c-a0f6-85d4-773118599bab&sn=1&lv=1621585048&lhd=1621585048&hd=1621585139&pid=2399&eu=%5B%5B0%2C41%2C1366%2C607%5D%2C%5B11%2C25552%2C%22input%23search%22%5D%2C%5B12%2C28186%2C%22input%23search%22%5D%5D",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("events_8", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("index_139");
        status = nsApi.ns_web_url ("index_139",
            "URL=https://ing-district.clicktale.net/ctn_v2/wr/?3301617508516896&24&11&8&0&2&8&subsid=233441&msgsize=120",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("index_139", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("manifest_json_8");
        status = nsApi.ns_web_url ("manifest_json_8",
            "URL=https://www.kohls.com/manifest.json"
        );

        status = nsApi.ns_end_transaction("manifest_json_8", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.195);

        return status;
    }
}
