/*!
 * Bazaarvoice api 0.7.3 config
 * Fri, 12 Mar 2021 16:51:37 GMT
 *
 * http://bazaarvoice.com/
 *
 * Copyright 2021 Bazaarvoice. All rights reserved.
 *
 */
window.performance && window.performance.mark && window.performance.mark('bv_loader_configure_api_start');BV["api"].configure({"client":"Kohls","site":"redesign","siteName":"redesign","features":{"europeanAuthenticity":false}})