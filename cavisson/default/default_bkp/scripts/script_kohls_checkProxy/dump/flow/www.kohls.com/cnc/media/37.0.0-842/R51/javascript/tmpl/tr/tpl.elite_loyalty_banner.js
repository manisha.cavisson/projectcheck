<script id="switchBanner" type="text/x-jQuery-tmpl">
{{if $env.ksLoyaltyV2 && message && message.length}}
{{if action=="eliteFreeShipping"}}
<div class='mvc-banner-messages1'>
<img src="${$env.cncRoot}images/elite/mvc-vertical.png" class="mvc-vertical-elite">
    <div class='mvc-banner-text1'>
	{{html message}}
    </div>
</div>
</div>
{{else}}
<div class='mvc-banner-messages-add-noEvent {{if bgType=="green"}}mvcgreen{{/if}}{{if bgType=="pink"}}mvcpink{{/if}}{{if bgType=="blue"}}mvcblue{{/if}} elite-bannerV2'>
<img src="${$env.cncRoot}images/elite/close@3x.png" class="switchBannerV2Close" alt="cls"></img>
{{if showImage}}
<img src="${$env.cncRoot}images/elite/mvc-vertical.png" class="MVC-Vertical">
{{/if}}
    <div class='mvc-banner-add-text-logo {{if showImage}}mvc-text{{/if}}'>
        <p class="mvc-banner-text-kcc">{{html message}}</p>
    </div>
	{{if action=="addkohlsCharge"}}
    <div class='mvc-add-logo-footer elite-banner-actionV2 {{if showImage}}mvc-footer-text{{/if}}'>
        <a href="#" class='mvc-addkcc-footer-text-link addkccv2 {{if bgType=="green"}}mvcgreentext{{/if}}{{if bgType=="pink"}}mvcpinktext{{/if}}{{if bgType=="blue"}}mvcbluetext{{/if}}'>Add your Kohl’s Charge</a>
    </div>
	{{/if}}
	{{if action=="setKccPrimary"}}
	<div class='mvc-add-logo-footer elite-banner-actionV2 {{if showImage}}mvc-footer-text{{/if}}'>
        <a href="#" class='mvc-addkcc-footer-text-link setkccv2 {{if bgType=="green"}}mvcgreentext{{/if}}{{if bgType=="pink"}}mvcpinktext{{/if}}{{if bgType=="blue"}}mvcbluetext{{/if}}'>Set Kohl’s Charge as primary payment</a>
    </div>
	{{/if}}
</div>
{{/if}}
{{/if}}
</script>

<script class="eliteBanner" id="eliteBanner" type="text/x-jQuery-tmpl">
{{if $env.eliteProgram && $env.isElite}}
	<div class="${actionType}Section elite-banner elite-banner-{{if pageName}}${pageName}{{/if}}" id="elite-banner-{{if pageName}}${pageName}{{/if}}">
		<div class="pink-truck"></div>
		<div class="elite-banner-text">
			<div class="elite-banner-text-1">${trLabels.kls_static_elite_dont_miss}</div>
			<div id="elite-banner-message">
			{{if message}}
				{{html message}}
			{{/if}}
			</div>
			<div class="elite-banner-action">
				<div class="elite-banner-button ${actionType}">
				{{if actionType === "makePrimary"}}
					${trLabels.kls_static_elite_set_primary}
				{{else actionType === "addKcc"}}
					${trLabels.kls_static_elite_add_kc}
				{{/if}}
				</div>
			</div>	
		</div>
		<div class="banner-close"></div>
	</div>
{{/if}}
</script>

<script class="eliteSuccess" id="eliteSuccess" type="text/x-jQuery-tmpl">
	<div>
		<div class="green-tick"></div>
		<div class="elite-success">${trLabels.kls_static_elite_all_set}</div>
	</div>
</script>

<script class="eliteFailure" id="eliteFailure" type="text/x-jQuery-tmpl">
	<div>
		<div class="red-cross"></div>
		<div class="elite-fail">${trLabels.kls_static_elite_something_wrong}</div>
	</div>
</script>