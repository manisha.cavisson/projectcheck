
<script id="tpl-menu-list" type="text/x-jsrender" >
	{{!-- Recursive nested list of menu options --}}
	<div id="{{id:'menu'+~sfx}}" class="mcom-hamburger-menu new-hb" role="menubar">
		<div class="hb-category-title">
			<div class="category-selected">
				<a id="{{id:'category'+~sfx}}" class="hamburger-deadlink" href="#">{{>name}}</a>
			</div>
		</div>
		<div id="{{id:'back'+~sfx}}" class="m-hamburger-menu-bck"><span class="m-back-icon"></span>Back</div>
		<ul id="{{id:'list'+~sfx}}" class="mobilemenu-list">
			{{if seoURL}}
			<li id="{{id:'shopall'+~sfx}}" class="sale-event"><span class="mobilemenu-item-all-title">Shop all {{>name}}</span><span class="mobilemenu-arrow-right"></span></li>
			{{/if}}
		{{for categories}}
			<li id="{{id:'item'+~sfx}}" class="hamburger-item">{{>name}}</li>
		{{/for}}
		</ul>
	</div>
	{{for categories}}
		{{if categories}}
		{{include ~sfx=~sfx+'_'+#getIndex() tmpl="tpl-menu-list" /}}	{{!-- Recursive inclusion --}}
		{{/if}}
	{{/for}}
</script>

<script id="tpl-category-main" type="text/x-jsrender" >
	{{!-- Entry point for rendering nested menus --}}
	<div id="{{id:'mobilemenu'}}" class="mobilemenu hidden">
		{{include ~sfx='' tmpl="tpl-menu-list" /}}
	</div>
</script>


<script id="tpl-hamburger-main" type="text/x-jsrender">
	{{!-- Primary menu that appears when clicking the hamburger menu icon --}}
	<style type="text/css">
	.mcom-hamburger-menu .m-back-icon {
		background-image: url({{:$env.resourceRoot}}images/mobile/mobile-hb-menu-sprite.png);
	}
	.new-hb ul li a span.hb-mid-block {
	    background-image: url({{:$env.resourceRoot}}images/mobile/mobile-hb-mid-menu-sprite.png);
	}
	.hb-new-sprite {
	    background-image: url({{:$env.resourceRoot}}images/mobile/mobile-pilot-menu-sprite.png);
	}
	</style>
	<div id="{{id:true 'mcom-hamburger-main'}}">
		<div id="{{id:true 'mcom-hamburger-overlay'}}"></div>
		<div id="{{id:'menu'}}" class="mcom-hamburger-menu new-hb">
		   <div id="Category-title" class="hb-category-title" style="display: none;">
		      <div class="m-main-catagory"><a href="#"></a></div>
		   </div>
		   <h3 class="m-hamburger-menu-bck" style="display: none;">
		      <span class="m-back-icon"></span>Back
		   </h3>
		   <div id="home-menu-new" class="">
		      <div class="hb_top_block hb-reg-user">
		         <div class="hb_signin">Sign In</div>
		         <div class="hb_createacc">Create Account</div>
		         <div></div>
		      </div>
		      <div>
		         <div class="hb-wallet"><a>WALLET</a></div>
		         <div class="hb-wallet-cnt">
		            <div>
		               <h1 class="hb-img-blk"><span class="yesyourewards hb-new-sprite"></span></h1>
		            </div>
		            <div>
		               <h1 class="hb-img-blk"><span class="kohlscash hb-new-sprite"></span></h1>
		            </div>
		            <div>
		               <h1 class="hb-img-blk"><span class="avail-offers hb-new-sprite"></span></h1>
		            </div>
		         </div>
		      </div>
		      <ul>
		         <li class="hbHome hb-mid-block">
		            <a href="/">
		               <span class="hbHome hb-mid-block"></span>Home
		            </a>
		         </li>
		         <li class="shop-by-category hb-mid-block">
		            <a id="{{id:'shopCategory'}}" href="#">
		               <span class="shop-by-category hb-mid-block"></span>Shop By Category
		            </a>
		         </li>
		         <li class="deals hb-mid-block">
		            <a href="/sale-event/coupons-deals.jsp">
		               <span class="deals hb-mid-block"></span>Kohl&#39;s Coupons
		            </a>
		         </li>
		         <li class="kohls-charge hb-mid-block">
		            <a href="/feature/kohlscharge">
		               <span class="kohls-charge hb-mid-block"></span>Kohl&#39;s Charge
		            </a>
		         </li>
		         <li class="kohls-rewards hb-mid-block">
		            <a href="/feature/rewards.jsp">
		               <span class="kohls-rewards hb-mid-block"></span>
		               <div>Yes2You Rewards</div>
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="#">
		               <span class="hb-btm-block"></span>Account
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="#">
		               <span class="hb-btm-block"></span>Orders
		            </a>
		         </li>
		         <li class="my-store hb-btm-block">
		            <a href="#">
		               <span class="my-store hb-btm-block"></span>Shop My Store
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="/stores/search.m.shtml">
		               <span class="hb-btm-block"></span>Store Locator
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="/catalog/kohl-s-gift-cards-gift-cards.jsp?CN=Product:Kohl%27s%20Gift%20Cards+Department:Gift%20Cards">
		               <span class="hb-btm-block"></span>Gift Cards
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="/upgrade/giftinglisting/wishlist.jsp">
		               <span class="hb-btm-block"></span>Lists
		            </a>
		         </li>
		         <li class="hb-btm-block">
		            <a href="/feature/contactus.jsp">
		               <span class="hb-btm-block"></span>Help
		            </a>
		         </li>
		         <li class="hb-btm-block hb_signout_pilot"></li>
		      </ul>
		   </div>
		</div>
	</div>
</script>
<script id="tpl-mobile-account-main" type="text/x-jsrender">
	<div id="mobile-login-modal" class="">
		<div class="show">
			<div id="signin-section">
				<div id="form-container">
					<div id="mobile-account-title">Account</div>
					<div id="{{id:'close'}}"  class="mobile-signIn-close-btn"><span class="screen-reader-text">close icon</span></div>
					<div id="form-tabs">
						<div id="{{id:'loginTab'}}" class="">SIGN IN</div>
						<div id="{{id:'signupTab'}}" class="">CREATE ACCOUNT</div>
					</div>
				</div>
				<div id="{{id:'form'}}"></div>
				<div class="kc-section">
					<div class="kc-section-charge">
						<p>
							Looking for your
							<br>
							Kohl&#39;s Charge Account?
						</p>
					</div>
					<div class="m-persistenceLogin-charge"><a href="https://credit.kohls.com/eCustService/" target="_blank">Kohl&#39;s Charge Account Sign In</a><span class="m-right-arrow"></span></div>
					<div class="m-persistenceLogin-chargeCard"><a href="https://apply.kohls.com/" target="_blank">Apply for Kohl&#39;s Charge Card</a><span class="m-right-arrow"></span></div>
				</div>
			</div>
		</div>
	</div>
	<div class="t-login-overlay"></div>
</script>

<script id="tpl-mobile-account-login" type="text/x-jsrender" >
	<form name="login_form" action="" method="" novalidate="">
		<div id="sign-in-form">
			<div class="input-field-container ">
				<div class="input-header"></div>
			<input type="email" id="{{id:true 'loginEmail'}}" name="loginEmail" class="" value="" placeholder="Email Address" autocapitalize="none" autocorrect="off" maxlength="40" nvsensitive="">
			</div>
			<div class="input-field-container ">
				<div class="input-header">
					<div id="{{id:'showPassword'}}" class="show-pw">
						<a tabindex="99">Show Password</a>
					</div>
				</div>
			<input id="{{id:true 'loginPassword'}}" type="password" name="loginPassword" class="" value="" placeholder="Password" nvsensitive="">
			</div>
			<div class="forgot-pw"><a href="javascript:void(0);" id="forgot-pw-link">Forgot password?</a></div>
			<div class="m-rememberme">
				<div class="input-field-container ">
					<div class="input-header"></div>
					<input type="checkbox" id="remember_me" name="remember_me" class="" value="">
					<label for="remember_me"></label>
				</div>
				<div class="remember_me_desc">Keep me logged in</div>
			</div>
			<div id="loginCaptchRemedy" class="kmobile-remedy-container"></div>
			<br>
			<div class="hideGoogleCaptchResponseField">
				<div class="input-header"></div>
				<input type="text" id="googleCaptchaHiddenFiled" name="googleCaptchaHiddenFiled" class="" value="">
			</div>
			<button type="submit" id="{{id:true 'sign-in-btn'}}" class="m-signin-disabled">Sign In</button>
		</div>
	</form>
</script>

<script id="tpl-mobile-account-signup" type="text/x-jsrender" >
	<div id="sign-up-form">
		<p>
			With an account you have access to express checkout, easy order tracking and purchase history. By creating an account, you agree to Kohl&#39;s
			<strong><a href="/feature/privacy-policy.jsp">Security &amp; Privacy Policy</a></strong>
			and
			<strong><a href="/feature/legal-notices.jsp">Legal Notices.</a></strong></p>
		<div class="form-container">
			<div class="input-field-container ">
				<div class="input-header"></div>
				<input type="text" id="{{id:true 'firstName'}}" name="firstName" class="" value="" placeholder="First Name" nvsensitive="">
			</div>
			<div class="input-field-container ">
				<div class="input-header"></div>
				<input type="text" id="{{id:true 'lastName'}}" name="lastName" class="" value="" placeholder="Last Name" nvsensitive="">
			</div>
			<div class="input-field-container ">
				<div class="input-header"></div>
				<input type="email" id="{{id:true 'signUpEmail'}}" name="signUpEmail" class="" value="" placeholder="Email Address" autocapitalize="none" autocorrect="off" maxlength="40" nvsensitive="">
			</div>
			<div class="input-field-container ">
				<div class="input-header">
					<div id="{{id:'showPassword'}}" class="show-pw">
						<a tabindex="99">Show Password</a>
					</div>
				</div>
				<input type="password" id="{{id:true 'signUpPassword'}}" name="signUpPassword" class="" value="" placeholder="Password">
			</div>
			<div class="email-opt-in">
				<div class="input-field-container ">
					<div class="input-header"></div>
					<input type="checkbox" id="emailOptIn" name="emailOptIn" class="" value="true">
					<label for="emailOptIn"></label>
				</div>
				<div class="opt-in-desc">
					Yes, sign me up to receive Kohl&#39;s e-mail offers, updates on sales and events and more! US Residents Only. Get 15% off your next purchase. Terms apply;
					<a href="https://cs.kohls.com/app/answers/detail/a_id/691/kw/sales%20alerts" target="_blank">see details</a>.
					<a href="/common/content/securityprivacy" target="_blank">Privacy Policy</a></div>
			</div>
			<button id="{{id:true 'sign-in-btn'}}" class="m-signin-disabled">Continue</button>
		</div>
	</div>
</script>
