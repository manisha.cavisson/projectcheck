
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<title>Shopping Bag - Kohls.com</title>
		
		<link rel="manifest" href="/manifest.json" />
		
		
		
			<script src="/cnc/media/37.0.0-842/javascript/deploy/environment.js"></script>
		
		
		<link rel="shortcut icon" href="https://www.kohls.com/media/images/kohls_ico.ico">
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/skava-custom.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/ipad.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/shoppingcart.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/header.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/gwpModal.css" />
		
		<link rel="stylesheet" href="https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.default.theme.min.css" />
		
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/tr_gwp.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/sub_v1.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/boss_checkout.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/eliteLoyalty.css" />
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/saveForLater.css" />

		
		<link type="text/css" rel="stylesheet" href="/cnc/media/37.0.0-842/css/fonts/hfjFonts.css"/>
		
		<script type="text/javascript">/*scriptCode*/$env = (typeof $env == 'undefined') ? {} : $env;(function() {	var env = {"resourceRoot":"/cnc/media/37.0.0-842/","serviceRoot":"/snb/","isSignalMarketingDataEnabled":"true","enableSmartCart":"true","welcomePilotOverlay":false,"ksCnCLoyaltyWelcomeOverlayEnabled":false,"welcomePilotURL":"/feature/flex/overlay.jsp","ksCnCLoyaltyKccPaymentSwitchMsg":true,"ksCncShowEarnMessageInPaymentBox":true,"ksCncBigDataUpgradeEnabled":true,"ksCncBigDataUpgradeRenderingEnabled":true,"pilotProgram":true,"eliteProgram":true,"shoppingBagSlotPrecedence":"smartcart","kioskSecureSoftTimeoutInSeconds":"30","kioskSecurehardTimeoutInSeconds":"15","kioskNonSecureSiftTimeoutInSeconds":"60","kioskNonSecureHardTimeoutInSeconds":"30","isKioskEnabled":false,"rewardsPilot":false,"isNuDataEnabled":true,"manageOrderEnabled":true,"globalHeaderRefreshEnabled":true,"ksCncEliteRewardsSpendTracker":true,"ksCncEliteBonusSpendTracker":true,"welcomeEliteOverlay":false,"welcomeEliteURL":"/feature/flex/elite-overlay.jsp","ksCncShoppingBagSmartCartEnabled":true,"ksCncEliteSwitchBanner":true,"ksCncEliteAddBanner":true,"ksCncEliteWelcomeOverlay":false,"thresholdEliteBannerValue":3,"sessionStatus":"ANONYMOUS","keepMeSignedIn":false,"preferredPaymentType":false,"eliteSwitchKccBannerEnabled":true,"eliteKccAdBannerEnabled":true,"ksAddressyTypeahead":true,"ksAddressyAddressValidation":true,"addressyKey":"MP45-MA98-ET81-TP29","addressyDefaultSuggestionCount":"6","addressyDefaultKeyStroke":"3","checkoutReviewSlotPrecedence":"norush","eliteKccUpsellEnabled":true,"eliteRewardsSpendTrackerEnabled":true,"eliteSpendAway":false,"verifyAddressDefaultSelection":true,"enablecncSignalSync":false,"preQualServiceEnabled":true,"otpAllowNewPhoneNumberDisabled":false,"nuData3DStimeoutInSec":"30","visaSrcEndPointUrl":"https://secure.checkout.visa.com","ksLoyaltyV2":true,"ksFreeShipMsgShoppingCart":true,"percentOffForNewKC":"30","ksAccurateDeliveryDate":true,"removeRegistry":true,"ksCNCSmartCartExp1Shipping":true,"enableBrowserAjaxThroughAkamai":"true","akamaiAvailabilityPickup":"/web/availabilityPickup/","ksGuestKCCApply":true,"keepMeSignedInEnabled":true,"ksGuestWalletEnabled":true,"ksCnCSaveForLater":true,"SaveforLaterListPageSize":"10","ksLoyaltyFullLaunchEnabled":true,"reCaptchaSecretKey":"6Lc4HyYUAAAAAPbL9wf1TlonG7PSA3lPxfyARjgs","ksGiftCardAsFullPaymentEnabled":true,"ksCurbSideFeature":true,"ksOptimizedUISessionCallEnabled":false,"ksJscramblerEnabled":false,"jscramblerEnvironment":"staging","jscramblerURL":"a.async=1;a.src=\"https://kh-ea.inappprotection.com/cc/1588655105.js\"","page":"shoppingCartV2","environment":"production","jsSubdomainURL":"","cssSubdomainURL":"","imageSubdomainURL":"","htmlSubdomainURL":"","imgFilePath":"images/","cssFilePath":"css/","jsModulePath":"javascript/deploy/","jsScriptPath":"javascript/","htmlFilePath":"javascript/","acmRoot":"/account/media/","cncRoot":"/cnc/media/37.0.0-842/","snbRoot":"/snb/media/","resourcePath":"","reportErrors":false,"reportingLevel":"error","errorLogApiUrl":"PDP_C","expiredSessionUrl":"/common/session_timeout.jsp","correlationDelim":"::","enableInlineCSS":true,"devEnv":false,"PageContainerESIHeader":"/feature/ESI/Common/Header/","PageContainerESIFooter":"/feature/ESI/Common/Footer/","PageContainerESINewHeader":"/feature/ESI/Common/NewHeader/","adobeLaunchScript":"//assets.adobedtm.com/6d2e783bbfe3/c0c47c5c96c6/launch-b2dd4b082ed7.min.js","bazaarvoiceEnabled":true,"bvJSURL":"//kohls.ugc.bazaarvoice.com/static/9025/bvapi.js","BVscript":"//apps.bazaarvoice.com/deployments/kohls/main_site/production/en_US/bv.js","ccpaConfig":"||OptanonConsent|C0004","ccpaItems":"gpt,zine1,hooklogic,richrelevance,asyncads,skava,bazaarvoice,pinit,fitrec,webcollage,trendage,curalate","diginotify_banner":true,"diginotify_kcreminder":true,"diginotify_popover":true,"disableDFP":false,"disableIE11Scripts":false,"enableAdobeLaunch":true,"enableBazaarVoice2":false,"enableBVFastStars":true,"enableDeferHamburger":true,"enableGeoTargeting":true,"enableHookLogicFeaturedProducts":false,"enableIndexExchange":true,"enableLazySwatches":true,"enableLoadNV":true,"enableOpinionLab":true,"enableSignalSync":false,"enableSplitLoadPDP":true,"enableSplitLoadPDPCollection":true,"enableSplitLoadPMP":true,"enableTypeahead":true,"enableZineOne":true,"Fit":true,"fitPredictor":false,"fsr_files":"/media/foresee/","globalRecommendationsEnabled":true,"isNewPDP":true,"isPDataEnabled":true,"isQuickView":false,"isQuickviewOverlay":false,"isSkavaPerformanceEnabled":true,"kccmEnabled":true,"kccmAccountMenuEnabled":true,"kccmDemoEnabled":false,"ksBiEnabled":false,"ksBiAccountMenuEnabled":false,"NVScript":"https://mon1.kohls.com/nv/kohls/nv_bootstrap.js?v=REL20170123","opinionLabScript":"/onlineopinionV5/oo_desktop.js","pbEnabled":true,"recentSearchesEnabled":true,"siteCatalystEnabled":true,"siteSurveyEnabled":false,"solrTypeAheadEnabled":true,"solrTypeAheadURL":"/","testAndTargetUpgradeEnabled":true,"testTargetEnabled":true,"thresholdKohlsMeterValue":40,"trueFit":true,"zineoneAPIkey":"apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357","zineoneSDKurl":"https://cdn.zineone.com/apps/latest/z1m.js","enableBOSS":true,"cncEnableBOSS":true,"snbCatSuggPersonalization":false,"ksRedesignV2":false,"ksRedesignJs":true,"accountDropDownLazyLoadEnable":true,"snbSessionCallAkamai":true,"ksWalletPopOut":false,"acmAddressApiURL":"MP45-MA98-ET81-TP29","acmAddressSearchLimit":6,"acmAddressMinSearch":3,"enableSnbStoreSearch":true,"enablePosteventsWebsession":true,"enableWishList":true,"jQueryUpgrade":true,"releaseNumber":"21MAY.W2","googleTypeAheadMinCount":4,"googleMapsApiKey":"AIzaSyBSu56XY0uIviQMa40WAy8MSxsTPSYeJMA","goopleApiURL":"//maps.googleapis.com/maps/api/js?key=AIzaSyBSu56XY0uIviQMa40WAy8MSxsTPSYeJMA&v=weekly&libraries=places","enableR74changes":true,"kohlsImageMediaPath":"//media.kohlsimg.com/is/image/kohls/"};	if ($env.$ns) {		$env.$ns.extend(true, $env, env);	} else {		$env.$env = $env.$env||[];		$env.$env.push(env);	}})();
	var trJsonData = {"status":"Success","statusMessage":"Cart Retrieved Successfully","cartId":"7207752838830001","promoApplied":false,"version":1,"eligibleForExpeditedCheckout":false,"prescreen":{"showPrescreenPopup":false,"zootUrl":"https://applynow.kohls.zootweb.com","prescreenOfferId":null,"prescreenKohlsChargeNumber":null},"addressDetails":{"multiShippingRequired":false,"shipAddress":[],"billingAddress":[],"storeAddress":null,"hasMoreAddresses":false,"isBillAddressEqualtoShipAddress":false},"cartItems":[{"cartItemId":"7207752838830001","skuId":"53207339","productId":"3847510","upcCode":"400532073396","quantity":1,"active":true,"itemProperties":{"productTitle":"Women's Apt. 9® Tummy Control Shorts","size":"8","color":"Potassium","image":{"url":"https://media.kohlsimg.com/is/image/kohls/3847510_Potassium?wid=180&hei=180&op_sharpen=1"},"productSeoURL":"/product/prd-3847510/womens-apt-9-tummy-control-shorts.jsp?skuId=53207339","swatchURL":"https://media.kohlsimg.com/is/image/kohls/3847510_Potassium_sw","productEditSeoUrl":"/product/prd-3847510/womens-apt-9-tummy-control-shorts.jsp?skuId=53207339&cItemId=7207752838830001&action=edit&fulfillmentType=SHIP","isWebExclusive":false},"itemType":"Normal","minimumAllowedQuantity":4,"maximumAllowedQuantity":99,"storeClearance":false,"fulfillmentType":"kf","eligibility":{"bopusEligible":true,"bossEligible":true,"shipItFaster":true,"mapEligible":false},"giftInfo":null,"registryInfo":null,"inventoryInfo":{"outOfStock":false,"quantityIncAllowed":true,"otherSkuAvailable":false,"shipAvailable":true,"bopusAvailable":false,"bossAvailable":false,"invOnHandQtyShip":55},"itemPriceInfo":{"regularUnitPrice":40,"regularPriceLabel":"ORIGINAL","saleUnitprice":19.99,"salePriceLabel":"SALE","promoAdjustedExtendedPrice":19.99,"promoApplied":false,"grossPrice":19.99,"discount":0,"regularExtendedPrice":0,"saleExtendedPrice":0},"surchargePriceInfo":null,"taxFee":null,"promoInfo":null,"shippingInfo":{"shipmentRefId":"1","availableShippingMethods":[{"methodCode":"STD","minDate":"Wed May 26","maxDate":"Tue Jun 01","showCutoffMessage":false,"shippingCutOffMessage":null,"shippingCutOfMessage":null,"optimizedDeliveryDate":null,"leadTime":0,"cutOffMessageDate":null},{"methodCode":"ODD","minDate":"Mon May 24","maxDate":"Mon May 24","showCutoffMessage":false,"shippingCutOffMessage":null,"shippingCutOfMessage":null,"optimizedDeliveryDate":null,"leadTime":0,"cutOffMessageDate":null},{"methodCode":"TDD","minDate":"Tue May 25","maxDate":"Tue May 25","showCutoffMessage":false,"shippingCutOffMessage":null,"shippingCutOfMessage":null,"optimizedDeliveryDate":null,"leadTime":0,"cutOffMessageDate":null}]},"offerInfo":null,"autoSwitch":false,"freeStandardShipping":false,"getItemSellable":false,"userEnteredQuantity":null,"isSddAvailableInShoppingBag":false,"eGiftInfo":null}],"shipmentInfo":[{"shipmentId":"1","shippingMethod":"STD","shippingMethodDescription":"Standard","shippingAddressRefId":null,"shipmentItemsCount":1,"shipmentPriceInfo":{"originalShippingCharges":8.95,"shippingChargesAfterDiscount":8.95,"freeShipQualificationDelta":55.01,"shipmentItemsTotal":19.99},"shippingOffers":null,"giftInfo":{"giftTogether":true,"isGift":false},"fulfillmentType":"HARDGOOD","curbsideEligible":null,"instorePickupEligible":null}],"purchaseEarnings":{"rewards":{"currentPurchaseEarnPoint":20},"loyaltyPilotUser":false,"elite":false,"elitePreferredPaymentTypeSet":false,"eliteCardOnProfile":false,"kccPreferredPaymentTypeSet":false,"kccCardOnProfile":false,"kohlsCash":{"qualificationAmount":50,"earnAmount":10,"currentPurchaseEarnAmount":0,"awayAmount":30.01},"kohlsCashEarnings":null,"eliteFreeShippingEvent":false,"orderContainsMPItem":true,"service":true,"sessionBean":true},"paymentTypes":{"creditCards":[],"giftCards":null,"payPalList":[],"venmoList":[]},"orderSummary":{"merchandisePromoAmount":19.99,"discountedShippingAmount":8.95,"originalShippingAmount":8.95,"tax":{"estimated":true,"taxRate":6,"tax":1.74},"totalSurcharges":0,"totalTaxFee":0,"totalKohlsCashAndPromoDiscounts":0,"totalTax":1.74,"total":30.68,"savings":20.01,"taxFeeEnabled":"true"},"userData":{"enrollLoyaltyAndSalesAlert":false,"email":""},"fulfillmentTypes":["HARDGOOD"],"isOrderTotalSatisfiedWithGC":false,"kohls_cart":"xA+FaG/b574vw76ixVk3zFD5dF26w453V2+RxYFKBDvT/DjTvekmOn/sZJqHCIpwTCCv6S5/pPF0kdAXRWONHQ==","eliteLoyaltyData":{},"vgcOnly":false,"recoSystemParameters":{"isRecoEnabled":"true","recoCid":"WebStore","recoJsSdkUrl":"https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.min.js","recoNewEnabled":"true","recoPage":"shoppingBag","recoPageNew":"ShoppingBag","recoPlids":"Horizontal","recoSystemAPIKeys":"NQeOQ7owHHPkdkMkKuH5tPpGu0AvIIOu","recoSystemAPILimit":"3","recoSystemAPIType":"smartRecommendation","recoSystemEndPoint":"https://api-atg.kohls.com/v1/ede/recommendations","recoTimeoutMiliSeconds":"10000","recoBDTemplateName":"flatView","recoBDPlacementName":"Horizontal","recoCssSdkUpgradedFile2":"https://api-bd.kohls.com/update/ede/assets/experiences/webstore/bd-experience-rendering-sdk.default.theme.min.css"},"omnitureServerDate":{"curDate":"2021-05-21","hours":"03:00 am","day":"friday","dayAbbreviation":"fri","currentDayInWeek":"weekday"},"isGuest":"true","thresholdKohlsMeterValue":40,"ksCnCLoyaltyWelcomeOverlayEnabled":false,"welcomePilotOverlay":false,"pilotProgram":true,"ksCnCLoyaltyShoppingBagUpsellMsg":true,"ksCnCLoyaltyKccPaymentSwitchMsg":true,"ksCncShowEarnMessageInPaymentBox":true,"brightTag":{"brightTagEnabled":"true","source":"non-common","bt_URL":"//s.btstatic.com/tag.js","bt_siteId":"4DPyaxM"},"brightTagV2PageDetails":{"pageName":"shopping_cart","pageType":"Shopping cart","defaultLanguage":"English"},"OperationalFeatures":{"alternatePickupEnabled":true,"manageOrderEnabled":true,"signalV2":true,"textNotificationEnabled":true},"customerDetails":{"SUID":"N/A","customerCity":"N/A","customerCountry":"US","customerHasKohlsCharge":false,"customerID":"c68333f4-b7d4-42dd-be5f-49d1eb509125","customerPostalCode":"N/A","customerState":"N/A","emailHashMD5":"N/A","emailHashSHA1":"N/A","emailHashSHA256":"N/A","exactTargetID":"N/A","isLoggedIn":"false","myStoreId":"N/A","rewardsPilotService":true,"kohlsLoyalty2018EliteMember":false,"kohlsLoyalty2018Member":"Not known"},"itemsType":"ShipOnly","isRedesignSingleLoginPage":true,"trCheckoutCartErrors":{"invalid_gift_card_num_pin":"Please enter a valid gift card number and PIN.","mismatch_gift_card_num_pin":"We&#039;re sorry, but the Gift Card number and/or PIN you entered do not match any Gift Card we have on record","mismatch_emails":"Your email and confirmation email addresses must match.","invalid_password":"Password must be at least 8 characters in length.","invalid_zipcode":"Your ZIP code must be exactly 5 characters long.","invalid_phone_number":"Please enter a valid phone number.","invalid_rewards_id":"Kohl's Rewards ID is invalid. Please verify your information and try again.","invalid_kc_info":"Please enter Kohl’s cash Coupon/ Please enter PIN for the Kohl’s Cash coupon.","invalid_kc_number":"We&#039;re sorry! You cannot use your Kohl’s Cash to complete this order. Please check the redeem dates.","invalid_promocode":"This promo code didn&#039;t work. But keep them coming. One will eventually stick.","upper_password":"The entered password does not contain the Upper Case Letter.","lower_password":"The entered password does not contain the Lower Case Letter.","number_password":"The entered password does not contain the number.","err_profileadd_firstName_empty":"Please enter your first name.","err_profileadd_lastName_empty":"Please enter your last name.","err_profileadd_street_address_empty":"Please enter your street address.","err_profileadd_city_empty":"Please enter your city.","err_profileadd_zip_empty":"Please enter your ZIP code.","err_profileadd_state_empty":"Please select a state.","err_profileadd_phone_empty":"Please enter your phone number.","err_profileadd_guest_firstName_empty":"Please enter your shipping first name.","err_profileadd_guest_lastName_empty":"Please enter your shipping last name.","err_profileadd_guest_street_address_empty":"Please enter your shipping address.","err_profileadd_guest_city_empty":"Please enter your shipping city.","err_profileadd_guest_zip_empty":"Please enter your shipping ZIP code.","err_profileadd_guest_state_empty":"Please enter your shipping state.","err_profileadd_guest_billing_firstName_empty":"Please enter your billing first name.","err_profileadd_guest_billing_lastName_empty":"Please enter your billing last name.","err_profileadd_guest_billing_street_address_empty":"Please enter your billing address.","err_profileadd_guest_billing_city_empty":"Please enter your billing city.","err_profileadd_guest_billing_zip_empty":"Please enter your billing ZIP code.","err_profileadd_guest_billing_state_empty":"Please enter your billing state.","err_profileadd_guest_billing_phone_empty":"Please verify the phone number.","max_promos_applied1":"You can only apply ","max_promos_applied2":" promos at a time. It&#39;s tempting, we know.","err_valid_billing_error":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_invalid_card":"The credit card number is not valid for the specified credit card type.","err_empty_card":"Please enter a Credit Card Number.","err_invalid_dataType":"Please correct the fields below.","err_invalid_Citystatezipcode":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","address_err_addr_076":"We could not verify your address. Please check the ZIP code entered.","address_err_addr_077":"We could not verify your address. Please check the city, state and ZIP code entered.","address_err_addr_078":"We could not verify your address. Please check the address and ZIP code entered.","address_err_addr_079":"We could not verify your address. Please check the apartment or unit number entered.","address_err_cart_087":"We couldn’t verify this address.<br> Please make changes or confirm below.","err_invalidStatezipcode":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_invalidCitystate":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_invalidZipcode":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_invalidCity":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_invalidZipcodeunderstate":"Please correct the fields below. The city, state and / or ZIP code entered are not a valid combination.","err_cart_level":"One or more items in your bag cannot be purchased at this time. To continue shopping, please see the item callout(s) below.","kls_static_checkout_AFP_ship_method":"APO/FPO – Standard Ground","kls_static_checkout_AKH_ship_method":"Alaska/Hawaii & APO/FPO - Standard Ground","kls_static_checkout_BOPUS_ship_method":"Pick Up in Store","kls_static_checkout_BOSS_ship_method":"Pick Up in Store","kls_static_checkout_LTL_C_ship_method":"Freight Shipping - Curbside","kls_static_checkout_LTL_R_ship_method":"Freight Shipping - Room of Choice","kls_static_checkout_LTL_T_ship_method":"Freight Shipping - Threshold","kls_static_checkout_LTL_W_ship_method":"Freight Shipping - White Glove","kls_static_checkout_ODD_ship_method":"ONE DAY","kls_static_checkout_PRI_ship_method":"Priority","kls_static_checkout_SDD_ship_method":"Same Day Delivery","kls_static_checkout_STD_ship_method":"Standard","kls_static_checkout_TDD_ship_method":"TWO DAY","kls_static_checkout_WF2DA_ship_method":"Wayfair 2 Day Priority","kls_static_checkout_WFSTD_ship_method":"Wayfair Standard Ground","kls_static_checkout_electronicShippingGroup_ship_method":"Continental US - Standard Ground","err_invalidExpairyDateMonthForCreditCard":"This credit card has expired. Verify the expiration date or enter a new card.","err-enteredInvalidGiftCardNumberOrPinNumber":"We're sorry, but the Gift Card number and/or PIN you entered do not match any Gift Card we have on record.","err_notcalculated_actualtax":"Your order contents were updated from a different session. Please proceed again.","kls_static_promo_already_applied":"You have already entered this Promo Code. Please enter a different Promo Code.","kls_static_promo_cannot_applied":"The Promo Code you entered can only be used once and has already been redeemed","kls_static_promo_used_once":"This promo code doesn't cover that shipping method. Don't worry. Either way, we'll make sure it'll find its way to you.","kls_static_promo_unableabletoremove":"Unable to remove promocode","demo_text":"Checkout Development Demo Text for Static Content","err_billing_info":"Please correct your billing information to proceed, note that for the first name and last name  allowable character are a-z, A-Z, `","err_payment_info":"Please correct your selected payment information to proceed, note that for the first name and last name allowable character are a-z, A-Z, `","err_shipping_info":"Please correct your selected shipping information to proceed, note that for the first name and last name allowable character are a-z, A-Z, `","gift_card_balance_error":"We&#039;re not seeing a balance left on the gift card you entered, so the gift card has not been applied.","err_address_fields":"Please correct the fields below.","billing_missing":"Please enter the billing address for the selected card below.","credit_card_missing":"Please enter the card information below.","err_invalid_addressy_addr":"Please correct the fields below. <br />We could not verify your address. Please double check.","err_not_verified_addr":"The address entered could not be verified. Please check it and make changes.","err_invalid_cvv":"Your payment could not be processed. Please check your CVV and try again. For assistance, contact us at (800) 564-5740 to complete your order.","err_invalid_kc_ms_card":"There was an issue processing your payment. Please contact us at (800) 564-5740 to confirm that your order was placed.","phone_missing":"Please enter the phone number."},"source":"/checkout/get_shopping_cart_json.jsp","fisOverlayUrl":"catalog/findinstore/findstore.jsp","quantityCount":"(1)"};	var loyaltyTnTJsonData = {"rewards":{"currentPurchaseEarnPoint":20},"loyaltyPilotUser":false,"elite":false,"elitePreferredPaymentTypeSet":false,"eliteCardOnProfile":false,"kccPreferredPaymentTypeSet":false,"kccCardOnProfile":false,"kohlsCash":{"qualificationAmount":50,"earnAmount":10,"currentPurchaseEarnAmount":0,"awayAmount":30.01},"kohlsCashEarnings":null,"eliteFreeShippingEvent":false,"orderContainsMPItem":true,"service":true,"sessionBean":true,"isElite":false,"elitepreferredPaymentTypeSet":false,"multipleKCCAccounts":false};	var trLabels = {"kls_static_tr_checkout_v2_creditcard_block_title":"&lt;strong&gt;Select a Credit Card&lt;/strong&gt;","kls_static_tr_checkout_v2_giftcard_title":"&lt;strong&gt;Do you have a Kohl&amp;#39;s  Gift Card&lt;/strong&gt;","kls_static_tr_checkout_select_shipping_method":"Select a Shipping Method","kls_static_checkout_international_shipping_link_url":"kls_static_checkout_international_shipping_link_url","kls_static_tr_checkout_international_shipping_link":"Need to ship Internationally?","kls_static_tr_checkout_shipping_method_tip_in_home_delivery":"LTL Shipping:","kls_static_bopus_tooltip_orders_ready_message":"Orders are typically ready within 2 hours after they are placed online.","kls_static_bopus_tooltip_pickup_store":"Pick Up in Store:","kls_static_tr_checkout_ht_email_order_confirmation":"https://cs.kohls.com/app/answers/detail/a_id/917","kls_static_tr_checkout_ht_gift_card":"https://cs.kohls.com/app/answers/detail/a_id/48","kls_static_tr_checkout_ht_payment_method":"https://cs.kohls.com/app/answers/detail/a_id/192","kls_static_tr_checkout_kgift_card":"Kohl&#039;s Gift Card","kls_static_tr_checkout_payment_selected_card":"Selected Card","kls_static_tr_checkout_payment_security_code_link":"https://cs.kohls.com/app/answers/detail/a_id/52/kw/Security%20Code","kls_static_tr_checkout_payment_security_code":"Security Code","kls_static_tr_information_txt":"This is a temporary card.Please remember to update your card information once you receive your official Kohl&#039;s charge card and discard the temporary card","kls_static_tr_checkout_payment_add_new":"Add a new payment method","kls_static_tr_checkout_county":"County","kls_sta_payment_info_state_code":"State Code","kls_static_tr_checkout_all_fields":"All fields are required unless otherwise marked as optional.","kls_static_tr_checkout_kohls":"Kohl&#039;s","kls_static_tr_checkout_gift_card":"Gift Card","kls_static_tr_checkout_cannot_used_gift_card":"We&#039;re sorry, this tender cannot be used to purchase Gift Cards.","kls_static_tr_checkout_payment_gcard_number":"Card Number","kls_static_tr_checkout_payment_pin":"Pin","kls_static_tr_checkout_payment_apply":"Apply","kls_static_tr_checkout_payment_card_type":"Card Type","kls_static_tr_checkout_payment_card_number":"Card Number","kls_static_tr_checkout_payment_exp_date":"Expiration Date","kls_static_checkout_month":"Month","kls_static_checkout_year":"Year","kls_static_tr_dont_see_one":"I don&#039;t see one","kls_static_tr_checkout_payment_billing_address":"Billing Address","kls_tr_checkout_static_same_as_billing_address":"My shipping and billing addresses are the same.","kls_static_tr_checkout_enter_information":"Enter information","kls_static_tr_checkout_chooseState":"Select state","kls_static_tr_checkout_payment_select_an_option":"Select an option","kls_static_tr_checkout_ht_billing_address":"https://cs.kohls.com/app/answers/detail/a_id/43","kls_static_tr_vc_paragraph1":"For security resons, you can&#039;t edit your Visa Checkout here.&lt;a href=&#034;#&#034; class=&#034;v-button&#034; &gt; Return to Visa Checkout&lt;/a&gt; for changes.","kls_static_tr_vc_paragraph2":"To enter diffrent payment information through Kohl&#039;s Checkout, click &lt;a href=&#034;#&#034; class=&#034;tr_remove_vcheckout&#034;&gt;here&lt;/a&gt;.     If you choose to use different  payment method, your Visa Checkout Information will be removed from this order.","kls_static_tr_vc_edit_link":"Edit Visa Checkout","kls_static_tr_mpc_paragraph1":"For security reasons, you can&#039;t edit your MasterPass Checkout here. &lt;a href=&#034;#&#034; class=&#034;masterPass_link&#034;&gt;Return to MasterPass&lt;/a&gt; for change.","kls_static_tr_mpc_paragraph2":"To enter different information through Kohl&#039;s Checkout click &lt;a href=&#034;#&#034; class=&#034;tr_remove_mpcheckout&#034;&gt;here.&lt;/a&gt; If you choose to use different payment method, your MasterPass Information will be removed from this order.","kls_static_tr_mpc_edit_link":"Edit MasterPass","kls_static_tr_checkout_payment_email":"Email for Order Confirmation","kls_static_tr_checkout_payment_provide_email":"Please provide an email address so we can contact you if there are any issues with your order.","kls_static_tr_checkout_payment_email_address":"Email Address","kls_static_tr_order_confirm_email_will_sent":"An order confirmation email will be sent to","kls_static_tr_checkout_payment_confirm_email_address":"Confirm Email Address","kls_static_tr_checkout_payment_your_email_required":"Your email address is required to place an order.","kls_static_tr_checkout_payment_sign_up_email":"Savings straight to your inbox?","kls_static_tr_checkout_payment_save_five_off":"Yes, sign me up to receive Kohl&#039;s e-mail offers, updates on sales and events and more!  US Residents Only. Get 15% off your next purchase.&lt;br&gt;Terms apply; see &lt;a href=&#034;javascript:launch(&#039;https://cs.kohls.com/app/answers/detail/a_id/691/kw/sales%20alerts&#039;)&#034; style=&#034;text-decoration:underline;&#034;&gt;details&lt;/a&gt;.  &lt;a href=&#034;javascript:launch(&#039;https://www.kohls.com/feature/privacy-policy.jsp&#039;)&#034; style=&#034;text-decoration:underline;&#034;&gt;Privacy Policy&lt;/a&gt;.","kls_sta_payment_info_rewards_msg":"Enroll in Yes2You Rewards today!","kls_sta_payment_info_rewards_msg_lfl":"Enroll in Kohl&#039;s Rewards","kls_sta_payment_info_questionmark":"&amp;#63;","kls_sta_payment_info_optional":"Optional","kls_sta_payment_info_reward_msg_lfl":"Earn more Kohl's Cash. Every purchase. No matter how big or small.","kls_sta_payment_info_reward_100_msg":"As a Yes2You Rewards member, you&#039;ll earn 1 point for every dollar spent, and get a $5 reward for every 100 points.","kls_sta_payment_info_enjoy":"You&#039;ll also enjoy:","kls_sta_payment_info_bday_gift":"8 offers a year, guaranteed, plus a birthday gift","kls_sta_payment_info_bonus_points":"Opportunities to earn bonus points","kls_sta_payment_info_earn":"Even more ways to earn","kls_sta_payment_info_potential":"Potential points earned:","kls_sta_payment_info_potential_lfl":"Earn more Kohl's Cash.","kls_sta_payment_info_singned_up":"I already signed up","kls_sta_payment_info_reward_id":"Enter Rewards ID","kls_sta_payment_info_enroll":"I want to enroll","kls_sta_payment_info_enroll_lfl":"I want to enroll in Kohl's Rewards","kls_sta_payment_info_enroll_rewards_lfl":"By enrolling in Kohl’s Rewards, you agree to the &lt;a href='/feature/rewards-terms.jsp' title='Terms and Condition'&gt; terms and conditions&lt;/a&gt; of the program and certify that you are over 16 years old and a US resident.","kls_sta_payment_info_enroll_rewards":"Enroll me in Rewards:","kls_sta_payment_info_agree":"I agree to the","kls_static_profile_loyalty_terms_and_conditions":"Terms and Conditions","kls_sta_payment_info_13years":"and certify that I am over 13 years old.","kls_static_tr_checkout_payment_create_kohls_account":"Create a Kohls.com Shopping Account","kls_static_tr_checkout_optional":"Optional","kls_sta_payment_info_earning_points":"Almost done! Just create a Kohls.com account and you&#039;ll be ready to start earning points.","kls_sta_payment_info_kohls_acc":"When you enroll in rewards, we also need to create a Kohls.com account.","kls_sta_payment_info_create_pwd":"You will need to create a Kohls.com account to enroll in Rewards. We already have your email address from your order notification, so all you have to do is create a password.","kls_static_tr_checkout_payment_create_account":"Create my account","kls_static_tr_cart_password":"Password","kls_static_tr_checkout_payment_confirm_password":"Confirm Password","kls_static_bopus_continue_to_review_order":"Continue to Review Order","kls_static_checkout_question_mark":"?","kls_static_tr_checkout_payment_credit":"Credit","kls_static_tr_checkout_payment_card":"Card","kls_static_tr_checkout_ht_credit_card":"https://cs.kohls.com/app/answers/detail/a_id/192","kls_static_tr_checkout_united_states":"United States","kls_static_tr_checkout_contact_label":"Phone number will be used for shipping communications only","static_make_main_shippingAddress":"Make this my primary shipping address","kls_static_tr_checkout_required_fields":"Please complete all fields unless marked as optional.","kls_static_tr_checkout_firstName":"First Name","kls_static_tr_checkout_second_address":"Address 2 (Optional)","kls_static_tr_checkout_city":"City","kls_sta_payment_info_apo":"APO/FPO/DPO","kls_static_tr_checkout_state":"State","kls_static_tr_checkout_contactPhone":"Phone Number","kls_static_tr_checkout_payment_method":"Payment Method","successMsgForItemRemove":"Item has been removed","successMsgForItemUpdate":"Item successfully updated","kls_static_tr_checkout_confirmation_msg_to_remove_item":"Are you sure you want to remove this item?","kls_static_tr_checkout_helpText_gift_remove_msg_item_will_also_removed_from_shopping_bag":"The gift that comes with this item will also be removed from your shopping bag.","kls_static_tr_checkout_use_this_address":"Use this address","kls_static_tr_checkout_edit_this_address":"Edit this address","kls_static_tr_checkout_remove_this_address":"Remove this address","kls_static_tr_checkout_payment_use_this_card":"Use this card","kls_static_tr_checkout_payment_edit_this_card":"Edit this card","kls_static_tr_checkout_payment_remove_this_card":"Remove this card","kls_static_tr_err_checkout_enter_valid_promoCode":"Enter a promo code or barcode number.","kls_static_tr_err_checkout_invalid_kcc_pin_format":"If you’re using Kohl’s Cash or Rewards, please enter your coupon number and PIN. If you’re using a promo code, check your entry for extra spaces.","kls_static_tr_cart_empty_shoppingBag":"There are no items in your shopping cart.","kls_static_tr_cart_had_items":"If you had items in your shopping cart from a previous visit,&lt;br/&gt;you may need to","kls_static_tr_cart_signIn":"sign in","kls_static_tr_cart_view_this_items":"to view your items.","kls_static_bopus_shopping_bag_checkout":"check out","kls_static_tr_cart_productDescription":"Item","kls_static_tr_cart_quantity":"Quantity","kls_static_tr_cart_item_total":"Item Total","kls_static_tr_cart_color":"Color","kls_static_tr_cart_primary_size":"Size","kls_static_tr_cart_sku":"SKU","kls_static_tr_cart_makeThisAsGift":"Make this a gift","kls_static_bopus_pick_up_at_msg":"Pick up at","kls_static_bopus_change_to_ship":"Change to ship","kls_static_tr_cart_editThisItem":"Edit item","kls_static_tr_cart_remove":"Remove","kls_static_tr_checkout_payment_method_hdr":"Payment Method","kls_static_checkout_payment_ccard_discover":"Discover Network","kls_static_checkout_payment_ccard_amex":"American Express","kls_static_checkout_payment_ccard_visa":"Visa","kls_static_checkout_payment_ccard_master":"Mastercard","kls_static_checkout_payment_ccard_klscharge":"Kohl&#039;s Charge","kls_static_tr_checkout_billing_address_hdr":"Billing Address","kls_static_tr_checkout_email_hdr":"Email for Order Confirmation","kls_boss_static_tr_checkout_email_hdr":"Email for Confirmation","kls_static_tr_reward_user_hdr":"Yes2You Rewards","kls_static_bopus_change_to_instore_pickup":"Change to in-store pick up","kls_static_bopus_gift_Pickup_msg":"Pick up","kls_boss_static_bopus_gift_Pickup_msg":"PICKUP","kls_static_bopus_item_msg":"Item","kls_static_bopus_at_msg":"at:","kls_static_bopus_store_msg":"store","kls_boss_static_bopus_store_msg":"Store","kls_static_bopus_store_pickup_sla_message":"Orders placed after 5pm (local time) will be processed the next business day. Orders are typically ready within 2 hours. You will receive a notification when your order is ready.","kls_static_bopus_quantity_msg":"Quantity","kls_static_bopus_item_total_msg":"Item Total","kls_static_tr_checkout_gift_receipt_included":"Gift receipt included.","kls_static_bopus_free_msg":"FREE","kls_static_tr_checkout_edit":"Edit","kls_static_tr_checkout_remove":"Remove","kls_static_tr_checkout_selected_address":"Selected address","kls_static_bopus_cart_ship":"SHIP","kls_static_bopus_cart_spt":"FREE STORE PICKUP","kls_static_tr_checkout_shipping_method":"Shipping Method","kls_error_checkout_ship_method_invalid_state_exception":"The item cannot be shipped to the shipping address entered. Please provide a different shipping address","mp_kls_static_checkout_continental_united_states_error_message":"Items from Wayfair can only be shipped to addresses in the continental United States. Please use a different delivery address to continue.","mp_kls_static_checkout_apo_fpo_error_message":"Items from Wayfair cannot be shipped to APO/FPO/DPO addresses. Please use a different delivery address to continue.","kls_static_tr_checkout_select_shipping_method_qualifying_item":"Free gifts will be sent with the same shipping method as the qualifying item.","kls_static_tr_checkout_select_shipping_method_URL":"https://cs.kohls.com/app/answers/detail/a_id/1089","kls_static_tr_checkout_shipping":"Shipping","kls_boss_static_tr_checkout_shipping":"SHIPPING","kls_static_tr_checkout_address":"Address","kls_static_tr_checkout_multi_shipping_ship_to_label":"Shipping to","kls_static_tr_checkout_ht_shipping_address":"https://cs.kohls.com/app/answers/detail/a_id/1144","kls_static_tr_checkout_select_shipping_address_and_method":"Select Shipping Address and Method","kls_static_tr_checkout_select_shipping_address":"Select Shipping Address","kls_static_bopus_shipping_pickup_message":"Shipping &amp; Pickup Options","kls_static_bopus_shipping_pickup_multiship_message":"Shipping &amp; Pickup Options","kls_static_tr_checkout_ht_shipping_method":"https://cs.kohls.com/app/answers/detail/a_id/5","kls_static_tr_checkout_shipping_method_tip":"Shipping Method","kls_static_tr_checkout_shipping_method_tip_standard_ground":"Standard Ground Shipping:","kls_static_tr_checkout_shipping_method_tip_ussually_arrives_3_6":"Usually arrives in 3-6 business days.","kls_static_eso_checkout_shipping_method_tip_two_day_delivery":"TWO DAY:","kls_static_eso_checkout_shipping_method_tip_arrives_in_two_day":"If ordered by 1 p.m. CST, arrives in two business days.","kls_static_eso_checkout_shipping_method_tip_arrives_in_one_day":"If ordered by 1 p.m. CST, arrives the next business day.","kls_static_eso_checkout_shipping_method_tip_one_day_delivery":"ONE DAY:","kls_static_tr_checkout_shipping_method_tip_same_day_delivery":"Same Day Delivery","kls_static_tr_checkout_shipping_method_tip_arrives_on_same_day":"If ordered by 1 p.m. local time, arrives by 8 p.m. the same day. If ordered after 1 p.m., delivery will be the next business day.","kls_static_tr_checkout_shipping_method_tip_standard_ground_or_APO":"Standard Ground Shipping to Alaska / Hawaii or APO/FPO/DPO:","kls_static_tr_checkout_shipping_method_tip_ussually_arrives_5_10":"Usually arrives in 10-15 Days","kls_static_tr_checkout_shipping_method_tip_priority_air":".","kls_static_tr_checkout_shipping_method_tip_ussualy_arrives_2_3":".","kls_static_tr_checkout_shipping_method_tip_ussualy_arrives_2_3_weeks":"Usually arrives in 7-14 business days. A contracted freight carrier will contact you to schedule a delivery time.","kls_static_tr_checkout_shipping_method_tip_shipping_rates":"View Shipping Rates and additional shipping information.","kls_static_tr_checkout_orders_placed_after":"Orders placed after 1pm (Central Time) will be processed the next day.","kls_static_tr_checkout_divide_order":"We may need to divide your order into two or more shipments. Shipping charges will not be affected by this.","kls_static_bopus_order_process_message":"Orders placed after 5pm (local time) will be processed the next business day. Orders are typically ready within 2 hours. You will receive a notification when your order is ready.","kls_static_bopus_process_pickup_message":"Orders placed after 5pm (local time) will be processed the next business day.  Orders are typically ready within 2 hours.  You will receive a notification when your order is ready.","kls_static_bopus_order_process_shipping_message":"Items being shipped may need to be divided into two or more shipments. Shipping charges will not be affected by this. Orders placed after 1 p.m. (Central Time) will be processed the next day.","kls_static_sdd_order_process_shipping_message":"Same Day Delivery orders must be placed by 1 pm local time to arrive by 8 pm local time today.  We&#039;ll email you with updates.","kls_static_tr_checkout_gift_options":"Gift Options","kls_static_tr_checkout_gift_options_next_page":"Add gift receipt and message.","kls_static_bopus_gift_unavailable_PickupItem":"Gift Wrap is unavailable for Pick up in Store","kls_static_gift_unavailable_sddItem":"Gift Wrap is not available for Same Day Delivery.","kls_static_gift_unavailable_sddItem_and_pickupitem":"Gift Wrap is not available for Same Day Delivery and store pick up items.","kls_static_tr_checkout_continue_to_gift_options":"Continue to Gift Options","kls_static_tr_checkout_continue_to_payment":"Continue to Payment","kls_static_tr_checkout_add_new_address":"Add new address","kls_static_tr_checkout_shipping_click":"click","kls_static_tr_checkout_remove_address_confirmation":"Are you sure you want to remove this address?","kls_static_tr_checkout_removing_address_cancel":"Removing an address will not cancel any previous orders shipped to this address.","kls_static_tr_checkout_removing_address_assigned":"Removing an address assigned to a payment method will not remove the payment method. A new billing address will need to be assigned before the payment method can be used.","kls_static_tr_checkout_new_billing_address":"A new billing address will need to be assigned before the payment method can be used.","kls_static_tr_checkout_item_and_shipping_method":"Item &amp; Shipping Method","kls_static_tr_checkout_edit_address":"Edit Address","kls_static_tr_checkout_xPO":"APO / FPO / DPO","kls_static_tr_ship_item_info":"Not eligible for Other Seller&#039;s Items","kls_static_tr_checkout_lastName":"Last Name","kls_static_tr_checkout_address_first":"Address","kls_static_tr_checkout_chooseStateCode":"Select State Code","kls_static_tr_checkout_zipCode":"ZIP Code","kls_static_tr_checkout_enter_county":"Enter a County","kls_static_profile_choose_county":"Choose a county","kls_static_tr_checkout_select_county":"Please select a county","kls_static_tr_checkout_zipcode_spans_county":"The ZIP code spans multiple counties.","kls_static_tr_checkout_dropdown_select_county":"Select county","kls_static_checkout_gift_options":"Gift Options","kls_static_tr_checkout_gifts_receipts":"Gift items include a message and gift receipt with hidden prices. Gift wrap is not available.","kls_static_tr_checkout_see_gift_box_wrap":"See Examples","kls_static_tr_checkout_gift_service_hdr":"Gift Service","kls_static_tr_checkout_gift_service_details_hdr":"Details of our Gift Service","kls_static_tr_checkout_gift_box_wrap_hdr":"Gift Box/Wrap","kls_static_tr_checkout_gift_services":"For $5.95 per box/wrap, we&#039;ll make sure your gift is ready for giving. Tucked inside a clean white box or wrapped in high-quality paper with a coordinating solid satin ribbon, Kohl&#039;s packages are available in a variety of sizes. The white box or wrapping is finished off with a crisp blue satin ribbon for everyday gifting.","kls_static_tr_checkout_gift_special_wrap_hdr":"Special-Occasion Wrap","kls_static_tr_checkout_gift_wedding":"Make a purchase from Kohl&#039;s Wedding Wishes Gift Registry or Little Ones Gift Registry and your gift will arrive wrapped with an occasion-specific ribbon!","kls_static_tr_checkout_gift_wedding_hdr":"Kohl&#039;s Wedding Wishes Gift Registry","kls_static_tr_checkout_gift_celebrate":"Celebrate the happy couple with a gift from their Kohl&#039;s Wedding Wishes Gift Registry. We&#039;ll add elegance to your package with a silver satin ribbon.","kls_static_tr_checkout_gift_little_one_hdr":"Kohl&#039;s Little Ones Gift Registry","kls_static_tr_checkout_gift_little_one":"Your gift from the Little Ones Gift Registry will be the best-wrapped gift on the gift table! The white package will be accented with a gender-neutral, green satin ribbon.","kls_static_tr_checkout_gift_mark_bag":"*When making your purchase, remember to mark your item(s) as a gift in your shopping bag.","kls_static_tr_checkout_gift_service":"If you&#039;ve chosen to Gift Box/Wrap multiple items with the same ship-to address, you may choose to wrap the items together or separately. Simply check the appropriate box below the Gift Box/Wrap column on the Gift Services page. (We&#039;ll accommodate this request if at all possible; however, the size, weight and other characteristics of some items may prohibit us from packaging them together.)","kls_static_tr_checkout_gift_receipts_hdr":"Gift Receipts","kls_static_tr_checkout_gift_receipts_tip":"There&#039;s always the chance your recipient won&#039;t like it as well as you do, so we always include a gift receipt. The receipt doesn&#039;t tell your recipient how much you paid, but it makes our easy Return &amp; Exchange Policy even more convenient. The recipient either brings the item and the gift receipt to the Customer Service Desk at any Kohl&#039;s Department Store or mails it back to our Return Center, and we take care of the rest.","kls_static_tr_checkout_gift_unfortunately":"Unfortunately, if your order is already complete, you are not able to change the status or order a gift receipt. However, your gift recipient may use the packing slip as a receipt.","kls_static_bopus_gift_PickupInStore_msg":"PICK UP IN STORE","kls_static_bopus_items_msg":"Items","kls_static_tr_checkout_gift_items":"item(s)","kls_static_tr_checkout_gift_item_to":") to:","kls_static_tr_checkout_ship_fewer_packages":"One gift receipt and message","kls_static_tr_checkout_enjoy_fewer_packages":"Include one gift receipt and message for this order.","kls_static_tr_checkout_ship_individual_packages":"Multiple gift receipts and messages","kls_static_tr_checkout_multiple_shipments":"Include multiple gift receipts and messages. Select items below.","kls_static_tr_checkout_gift_box_wrap_package":"Enter gift message below","kls_config_tr_checkout_gift_wrap_amt":".","kls_static_tr_gift_wrap_together":"Please select the items that need gift messaging/ wrapping","kls_static_tr_checkout_this_is_gift":"Make this a gift.","kls_static_sdd_gift_unavailable_sddItem":"Gift Wrap is not available for Same Day Delivery.","kls_static_tr_checkout_gift_cannot_wrapped":"Add gift receipt and message.","kls_static_tr_checkout_gift_box_wrap":"Gift Box/Wrap this Item","kls_static_tr_checkout_gwp_get_gift_cannot_wrapped":"This item cannot be gift wrapped. It will be grouped with the qualifying item.","kls_static_tr_checkout_gift_to":"To","kls_static_tr_checkout_gift_from":"From","kls_static_tr_checkout_gift_message":"Message","kls_static_tr_checkout_gift_item":"item","kls_static_tr_checkout_gift_gwp_gift_added":"Gift added.","kls_static_tr_checkout_gift_free_Gift_label":"Free Gift","kls_static_tr_checkout_gift_gwp_applied_offers_details_link":"details","kls_static_tr_checkout_ht_gift_options":"https://cs.kohls.com/app/answers/detail/a_id/914","kls_static_tr_checkout_ht_create_account":"https://cs.kohls.com/app/answers/detail/a_id/918","kls_static_omnichannel_concurrent_update_expecption_msg":"Your order contents were updated from a different session. Please proceed again.","kls_static_tr_cart_shoppingBagInfo":"Please review your Shopping Bag before you checkout. &lt;br&gt;If items were saved to your Shopping Bag from a previous visit, product pricing or availability may have changed.","kls_static_bopus_text_notification":"Receive text notifications","kls_static_bopus_mobile_number":"Mobile Number","kls_static_bopus_order_ready_notify":"We&amp;#39;ll send you a text when your order is ready for pickup.","kls_static_bopus_carrier_charge_msg":"Messages sent via auto-dialed text message to your mobile number. Number of messages will vary based upon order status and number of orders. Msg&amp;Data Rates may apply. Participation not required to make a purchase. Kohl&#039;s does not charge you for sending or receiving Text Messages to or from 28008. See Kohls.com/smsorder for other program Terms &amp; Conditions.  You can view Kohl&#039;s Privacy Policy by clicking &lt;a href = &#034;http://www.kohls.com/smsorder&#034; target=&#034;_blank&#034;&gt;here.&lt;/a&gt;","kls_static_bopus_add_pickup_person_msg":"Add an additional pickup person","kls_static_bopus_optional_text":"&amp;#40;optional&amp;#41;","kls_static_bopus_order_pickup_option":"kls_static_bopus_order_pickup_option","kls_static_bopus_order_self_pickup_msg":"You can still pickup this order, even if you select a pickup person","kls_static_bopus_alt_pickup_firstname":"First Name","kls_static_bopus_alt_pickup_lastname":"Last Name","kls_static_bopus_alt_pickup_email":"Additional Pickup Person Email","kls_static_bopus_alt_email_pickup":"This person will receive emails when the order is ready for pickup as well as relevant order updates.","kls_static_bopus_alt_person_tooltip_msg":"Selecting a pickup person allows an additional person to pickup your order.  You and your pickup person will receive notifications when the order is ready.  The pickup person will need to bring a &lt;b&gt;photo ID&lt;/b&gt; and &lt;b&gt;confirmation email&lt;/b&gt; to pick up the order. You can still pick up this order, even if you select a pickup person","kls_static_bopus_alt_text_tooltip_msg":"By providing your mobile number, you agree to the terms available at &lt;a href = &#034;http://www.kohls.com/smsorder&#034; target=&#034;_blank&#034;&gt;Kohls.com/smsorder&lt;/a&gt;. YYou also acknowledge that text messages about your buy online, pick up order(s) will be sent to your mobile number and visible to those with access to your number.","kls_static_bopus_alt_pickup_header_text":"PICKUP OPTIONS","kls_static_bopus_alt_text_palceholder_text":"555-555-5555","kls_static_bopus_alt_email_placeholder_text":"name@email.com","kls_message_bopus_alt_pickup_fields_error_msg":"Please enter a first name, last name and valid email for an additional pickup person","kls_message_bopus_alt_pickup_first_last_name_error_msg":"Please enter a first and last name for an additional pickup person","kls_message_bopus_alt_pickup_first_name_error_msg":"Please enter a first name for an additional pickup person","kls_message_bopus_alt_pickup_last_name_error_msg":"Please enter a last name for an additional pickup person","kls_message_bopus_alt_pickup_email_error_msg":"Please enter a valid email for an additional pickup person","kls_message_bopus_alt_text_mobile_error_msg":"Please enter a valid mobile phone number to receive text alerts","kls_message_bopus_alt_pickup_first_name_email_error_msg":"Please enter a first name and valid email for an additional pickup person","kls_message_bopus_alt_pickup_last_name_email_error_msg":"Please enter a last name and valid email for an additional pickup person","kls_static_bopus_alt_pickup_done_button":"DONE","kls_static_bopus_alt_pickup_edit_button":"Edit","kls_static_bopus_alt_pickup_add_button":"Add","kls_static_bopus_alt_pickup_text_notify":"Text notifications","kls_static_bopus_alt_pickup_add_person":"Additional pickup person","kls_static_shoprunner_empty_shopping_bag_msg":"Please review your Shopping Bag before you checkout.&lt;br/&gt;If items were saved to your Shopping Bag from a previous visit, product pricing or availability may have changed.&lt;br/&gt;For help and details about promotions, refer to the &lt;a href=&#034;#&#034; class = &#034;shoprunner_anchor_style&#034;&gt;Learn More&lt;/a&gt; page.","kls_static_shoprunner_promotions_msg":"Promotions :","kls_static_shoprunner_checkout_with_msg":"Check out with","kls_static_shoprunner_available_to_ship_msg":"kls_static_shoprunner_available_to_ship_msg","kls_static_curbside_option_text":"Use &lt;span class='curbside-option-text-bold'&gt;Drive Up&lt;/span&gt; for &lt;span class='curbside-option-text-bold'&gt;{0}&lt;/span&gt; pickup items","kls_static_bopus_alt_pickup_none_detail":"None","kls_static_bopus_review_alt_pickup_header_text":"Pickup Options","kls_static_manageorder_button":"MANAGE ORDER","kls_static_digital_wallet_giftcard_header_txt":"Apply a Kohl&amp;#39;s Gift Card","kls_static_digital_wallet_giftcard_txt":"KOHL&amp;#39;S GIFT CARD","kls_static_digital_wallet_giftcard_apply_txt":"APPLY","kls_static_digital_wallet_giftcard_applied_txt":"APPLIED","kls_static_digital_wallet_giftcard_unapply_txt":"UNAPPLY","kls_static_digital_wallet_giftcard_number_placeholder":"Enter Barcode Number.","kls_static_digital_wallet_giftcard_pin_placeholder":"PIN #","kls_static_digital_wallet_card_address_selected":"SELECTED","kls_static_digital_wallet_card_address_select":"SELECT","kls_static_digital_wallet_card_expires_text":"Expires","kls_static_pwd_at_least_char_rule":"At Least 8 Characters Long","kls_static_acc_pwd_upper_lower_case_rule":"Upper and Lower case","kls_static_acc_pwd_upper_lower_case_value":"Aa","kls_static_acc_pwd_numbers_letters_rule":"Numbers and Letters","kls_static_acc_pwd_numbers_letters_value":"ABC123","kls_static_acc_pwd_least_char_cant_email_rule":"Should not be same as email or login","kls_static_acc_pwd_rules_succ_msg":"A strong password helps protect your information. Remember to update it regularly.","kls_message_payment_page_credit_length_error_msg":"The credit card number length must be 12 digits.","kls_static_acc_updated_succ_msg":"Account has been updated!","mp_kls_static_cart_checkout_purchase_earning_tooltip_content":"&lt;h3&gt;How are my earnings calculated?&lt;/h3&gt;&lt;p&gt;Earnings are based on the purchase amount after all applicable discounts are applied and before tax is applied. &lt;/p&gt;&lt;a target=&#034;_blank&#034;href=&#034;https://cs.kohls.com/app/answers/help_topic/c/22,1301&#034;&gt;Read more about Kohl&#039;s Cash and Yes2You Rewards&lt;/a&gt;","kls_static_cart_checkout_purchase_earning_tooltip_content":"&lt;h3&gt;How are my earnings calculated?&lt;/h3&gt;&lt;p&gt;Earnings are based on the purchase amount after all applicable discounts are applied and before tax is applied. &lt;/p&gt;&lt;a target=&#034;_blank&#034;href=&#034;https://cs.kohls.com/app/answers/help_topic/c/22,1301&#034;&gt;Learn more about Kohl&#039;s Cash and Kohl&#039;s Rewards&lt;/a&gt;","kls_static_tr_cart_orderSummary":"Order Summary","kls_static_tr_cart_merchandise":"Subtotal:","kls_static_tr_cart_kohls_cash_and_discounts":"Kohl&#039;s Cash &amp; Promos:","kls_static_tr_cart_shippingFee":"Shipping:","kls_static_tr2_away_from_suggest_msg_order_summary_page":"away from","kls_static_tr2_pb_free_shipping_label":"FREE","kls_static_map_pricing_label":"See final price&lt;span class='mapPricingBold'&gt; in checkout&lt;/span&gt;","kls_static_tr_confirm_surcharges":"Surcharges:","kls_static_tr_confirm_tax":"Tax:","kls_static_tr_cart_total":"Total:","kls_static_tr_cart_total_savings":"Your Savings","kls_static_tr_confirm_amount_applied_to_GC":"Amount applied to Gift Card","kls_static_tr_confirm_amount_applied":"Amount applied to","kls_static_tr2_purc_earn_label":"Purchase Earnings","kls_static_tr2_pe_kohls_cash_img":"/media/images/kohls-cash.png","kls_static_tr2_pe_kohls_cash":"Kohl&#039;s Cash","kls_static_tr2_pe_kohls_rewards_img":"/media/images/checkout_yes2u.png","kls_static_tr2_pe_kohls_rewards":"Yes2You Rewards","kls_static_tr2_pe_potential_pts":"Potential Pts","kls_static_suggested_items_clearance_price":"Clearance","kls_static_suggested_items_sale_price":"Sale","kls_static_suggested_items_amount_price":"Amount","kls_static_suggested_items_original_price":"Original","kls_static_suggested_items_regular_price":"Regular","kls_static_tr2_free_shipping_suggest_msg_order_summary_page":"Free Shipping","kls_static_tr2_free_shipping_suggest_msg_order_summary_page_confirmation":"Free shipping for this shipment","marketPlace":{"sold_and_shipped_by":"Sold &amp; Shipped by","mp_kls_static_item_must_be_returned_to":"This item must be returned to","mp_kls_static_see_all_restrictions":"See all restrictions.","mp_kls_static_cart_checkout_tooltip_content":"&lt;li&gt;The seller ships these items. To return, simply contact the seller. Items cannot be returned at Kohls.com or Kohl&#039;s stores. &lt;/li&gt; &lt;li&gt;You can&#039;t earn or redeem Kohl&#039;s Cash or Yes2You Rewards on products sold by Wayfair.&lt;/li&gt; &lt;li&gt;Coupons and other discounts do not apply to products sold by Wayfair.&lt;/li&gt;","mp_kls_static_gift_option_not_available":"Gift option is not available for","mp_kls_static_products":"products."},"placeholders":{"shipping_zipcode":"5 or 9 digits","shipping_address1":"Street address, P.O. box","shipping_address2":"Apartment, suite, unit, building, floor","shipping_phn_no":"Area code and number"},"kls_static_tr_cart_surcharges_gifts_estimated_tax":"Surcharges, Gifts & Estimated Tax:","kls_static_tr_cart_surcharges_gifts_estimated_tax_star":"Surcharges, Gifts & Estimated Tax*:","kls_static_tr_cart_surcharges_gifts_tax":"Surcharges, Gifts & Tax:","kls_static_tr_cart_surcharges_estimated_tax":"Surcharges & Estimated Tax:","kls_static_tr_cart_surcharges_estimated_tax_star":"Surcharges & Estimated Tax*:","kls_static_tr_cart_surcharges_tax":"Surcharges & Tax:","kls_static_tr_cart_gifts_estimated_tax":"Gifts & Estimated Tax:","kls_static_tr_cart_gifts_estimated_tax_star":"Gifts & Estimated Tax*:","kls_static_tr_cart_gifts_tax":"Gifts & Tax:","kls_static_tr_cart_estimated_tax":"Estimated Tax:","kls_static_tr_cart_estimated_tax_star":"Estimated Tax*:","kls_static_tr_cart_estimated_tax_review_description":"*Tax calculation is based on an estimate. Order confirmation email will provide actual tax charged based on the ship to location.","kls_static_tr_cart_tax":"Tax:","kls_static_tr_cart_tax_estimated":"Estimated Tax*","kls_static_tr_cart_giftwrap":"Gift Wrap","kls_static_tr_checkout_ship_tax":"Shipping &amp; Tax Details","Kls_static_tr_cart_shipping":"Shipping","kls_static_checkout_symbol":",","kls_static_tr_checkout_gift":"Gifts","kls_static_tr_checkout_gift_surcharges":"Surcharges","kls_static_tr_checkout_gift_tax":"Tax","kls_static_tr_checkout_gift_to_small":"to","Browse_static_left_bracket":"(","kls_static_tr_checkout_gift_items_comma":"Kohls items(s)","kls_static_tr_checkout_gift_items_comma_wo_kohls":"items(s)","Browse_static_right_bracket":")","kls_static_tr2_free_ship_suggest_message_modal_window":"away from Free shipping for this shipment","kls_static_checkout_minus_sign":"-","kls_static_tr_checkout_gift_kohls_cash":"Kohl&#039;s Cash","kls_static_mp_apply_kohls_only":"Applies only to items shipped by Kohl&#039;s","kls_static_tr_checkout_gift_total_shipping":"Total Shipping","kls_static_tr_checkout_gift_shipping_surcharges":"Shipping Surcharge","kls_static_tr_checkout_gift_total_surcharges":"Total Surcharges","kls_static_tr_checkout_gift_boxes_wrap":"Gift boxes &amp; Wrap","kls_static_tr_cart_surcharges_tax_fee":"Surcharges, Fees &amp; Tax","kls_static_tr_cart_surcharge_tax":"Surcharges &amp; Tax","kls_static_tr_cart_tax_fee":"Fees &amp; Tax","kls_static_tr_checkout_sales_tax":"Sales Tax","kls_static_tr_checkout_multiple_locations":"Multiple Locations","kls_static_tr_checkout_how_ship_rates_calculated":"How are these costs calculated?","kls_static_tr_checkout_shipping_rates":"&lt;a href=&#034;https://cs.kohls.com/app/answers/detail/a_id/5&#034; target=_blank&gt;Shipping rates&lt;/a&gt; and discounts are based on merchandise total after discounts are taken. In some states, shipping may be taxed. See our &lt;a href=&#034;https://cs.kohls.com/app/answers/detail/a_id/12&#034; target=&#034;_blank&#034;&gt;customer service area&lt;/a&gt; for more information about shipping and tax.","kls_static_tr_checkout_shipping_ratesV2":"<a href=\"https://cs.kohls.com/app/answers/detail/a_id/5\" target=\"_blank\">Shipping rates</a> and discounts are based on merchandise total after discounts are taken. In some states, shipping may be taxed. See our <a href=\"https://cs.kohls.com/app/answers/detail/a_id/12\" target=\"_blank\">customer service area</a> for more information about shipping and tax.","kls_static_tr_checkout_shipping_estimated_rates":"*Tax calculation is based on an estimate. Order confirmation email will provide actual tax charged based on the ship to location.","kls_static_gwp_gift_with_purchase":"Gift with Purchase","kls_static_pwp_special_savings_with_purchase":"Special Savings with Purchase","kls_static_cart_default_itemoffers_msg":"item offers applied","kls_static_you_have_a_free_gift":"You have a free gift!","kls_static_cart_offers_applied":"offers applied","kls_static_cart_offers_details":"details","kls_static_gift_card_applied":"applied","kls_static_remaining_card_balance":"remaining","kls_static_bopus_layout_change_order_thank_you":"Thank you for shopping with us ","kls_static_tr_order_placed_id":"Order#","kls_static_tr_order_placed":"Placed","kls_static_tr_order_placed_next":"What&#039;s Next?","kls_static_bopus_layout_change_order_check_status":"Check on the status of your order by signing into your account.","kls_static_sdd_watch_your_inbox_additional_status_update":"Watch your inbox for additional status updates as your order makes it way to you.","kls_static_sdd_order_pickup_order_ready":"You will receive an email when your order is ready for pickup. Orders are typically ready for free store pickup within 2 hours.","kls_static_tr_order_placed_will_receive_email":"You'll receive an email with your","kls_static_tr_order_placed_kohls_cash":"Kohl&#039;s Cash.","kls_static_tr_order_placed_redeem_it":"You can redeem it","kls_static_tr_order_palced_through":"through","kls_static_potential_points":"Potential points earned:","kls_static_rewpoints_user_message":"Your points will appear in your account up to 48 hours after this order is marked Complete in your Purchase History.","kls_static_tr_order_placed_order":"Your order has been placed","kls_static_tr_confirm_your_order":"YOUR ORDER","kls_static_gift_with_purchase":" Gift with Purchase:","kls_static_select_your_free_gift":"Select your Free Gift from the following items:","kls_static_bopus_orderReview_pickupmessage":"You'll receive an email or text when your order is ready for pickup.</br>Bring your <span class=\"text-bbold\">photo ID</span> and <span class=\"text-bbold\">email</span> or <span class=\"text-bbold\">pickup pass</span>  to your selected store.","kls_static_bopus_orderReview_driveupmessage":"You'll receive an email or text when your order is ready for pickup.</br>Bring your <span class=\"text-bbold\">email</span> or <span class=\"text-bbold\">pickup pass</span>  to your selected store.</br> Park in one of the designated Drive Up spots, and tap the I'M HERE button in your pickup pass.</br> An associate will bring your order out and place it in your trunk or back seat.","kls_static_bopus_orderReview_have_questions":"Have questions?","kls_static_bopus_orderReview_view_faqpage":"Visit our FAQ page.","kls_static_profile_loyalty_program_details":"Program Details","kls_static_profile_loyalty_faqs":"FAQs","kls_static_view_rewards":"View my Rewards","kls_static_checkout_loy_error_msg":"<span class=\"sry_link_text\">Sorry! We had trouble linking to your Rewards ID.</span><br><span class=\"to_earn_text\">To earn points on this order, just <a href=\"/myaccount/kohls_rewards.jsp\" class=\"try_again_text\"> head to the Rewards page and try again there</a></span>","kls_static_chect_loy_enroll_error_msg":"<span class='sry_link_text'>Sorry! We had trouble enrolling you in Rewards.</span><br><span class='to_earn_text''>To earn points on this order, just <a href='/myaccount/kohls_rewards.jsp' class='try_again_text'> head to the Rewards page and try again there.</a></span>","kls_static_enroll_rewards_points":"(100 points = a $5 Reward!)","kls_static_try_again_link":"Try Again","kls_static_lolty_enroll_confirm":"Don't miss out! Enroll in Yes2You Rewards in the next 30 minutes to get your points from this purchase!","kls_static_lolty_enroll_get_points1":"Just use the same email address used for this order.","kls_static_lolty_enroll_get_points2":"Get to the points.","kls_static_enroll_today_link":"Enroll today","kls_static_order_confrm_signup":"Sign up","kls_static_prequal_headtext":"See if you prequalify for a kohl's charge today","kls_static_prequal_spantext":"with no impact on your credit score!","kls_static_prequal_actiontext":"Get Started","kls_static_verify_card_number":"Verify Card Number","kls_static_payment_verification":"Your privacy is important.&lt;br/&gt;Please verify one of the credit card numbers below to continue","kls_static_remedy_validation_error":"The card number entered did not match our records.Please try again","kls_static_pay_method_no_change":"Your Payment method won&#039;t change","kls_static_unable_verify_card":"Unable to verify your card? Please contact us at 866-887-8884 for next steps.","kls_static_add_new_card":"Add New Card","help_topics_need_assistance":"https://cs.kohls.com/","help_topics_security_policy":"https://www.kohls.com/feature/privacy-policy.jsp","help_topics_returns":"https://cs.kohls.com/app/answers/help_topic/c/22,4","help_topics_shipping":"https://cs.kohls.com/app/answers/help_topic/c/22,3","terms_and_condition":"/myaccount/common/rewards_content.jsp?pageName=rewardsTerms","prescreenPreQualify":"You're pre-qualified for a Kohl's Charge","prescreenApplyNow":"Apply Now","prescreenNoThanks":"No Thanks","prescreenLater":"Maybe Later","prescreenDisclosure":"Prescreen Disclosure","prescreenDisclosureDesc":"You can choose to stop receiving \"prescreened\" offers of credit from this and other companies by calling tollfree 1-888-5OPT-OUT(1-888-567-8688). See PRESCREEN & OPT-OUT NOTICE below for more information about prescreened offers.","prescreenOptNotice":"PRESCREEN & OPT-OUT NOTICE","prescreenOptNoticeDesc":"This \"prescreened\" offer of credit is based on information in your credit report indicating that you meet certain criteria. This offer is not guaranteed if you do not meet out criteria. If you do not want to recieve prescreened offers of credit from this and other companies, call the consumer reporting agencies toll free, 1-888-5-OPT-OUT (1-888-567-8688); or write them individually at: Experian Target Marketing, P.O. Box 919, 701 Experian Parkway B2,Allen TX 75013; Equifax Options, P.O. Box 740123, Atlanta, GA 30374-01234; Trans Union Corporation, Attn: Marketing Opt Out, P.O Box 5505, Woodlyn, PA 19094-0505; LexisNexis Risk & Information Analytics Group Inc., Attn: FCRA Opt Out, LexisNexis Consumer Center Prescreen Opt Outs, P.O. Box 105108, Atlanta, GA 30348-5108","prescreenViewThisItems":"to view your items.","prescreenAddress":"Billing Address","preScreenDiscountPerc":"WITH A KOHL'S CHARGE","preScreenDiscountAmount":"20","preScreenMessage":"YOU COULD SAVE","preScreenPercent":"%","preScreenStar":"*","prescreenImage":"/media/images/tr3_pre_screening_logo.jpg","prescreenExclusions":"*Some exclusions apply. See","prescreenDetails":"<a href=\"https://cs.kohls.com/app/answers/detail/a_id/1158 \" target=\"_blank\">Details</a>","prescreenLearnMore":"Learn More","prescreenExpdiscountmess":"Apply now and you could save up to","preScreenDot":".","prescreenDisclosure_expd":"Prescreen Disclosure","prescreenDisclosureDesc_expd":"You can choose to stop receiving \"prescreened\" offers of credit from this and other companies by calling tollfree 1-888-5OPT-OUT","kls_sta_payment_info_potential_text":"potential points earned","kls_static_tr_checkout_expected_delivery":"Expected Delivery","kls_static_tr_checkout_shipping_error":"<div class=\"errorMsg  error_display has-error-message\">The item cannot be shipped to the shipping address entered. Please provide a different shipping address.</div>","kls_static_tr_checkout_shipping_error_msg":"The item cannot be shipped to the shipping address entered. Please provide a different shipping address.","kls_static_tr_checkout_sdd_error_msg":"<div class='taberrorsShip has-error-message'>Same Day Delivery is not available for the selected address. Please select another shipping method.</div>","kls_static_smartcard_incentive_eligible":"Get &#036;{0} Kohl's Cash when you pick up this order <a href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_smartcard_incentive_qualified":"Your &#036;{0} Kohl’s Cash® will be sent in your pickup confirmation email. <br/>Limit one offer per customer per day. <a href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_smartcard_loyalty_incentive_qualified":"Your &#036;{0} Kohl’s Cash® will be sent in your pickup confirmation email. <br/>Limit one offer per customer per day. <a href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_norush_qualified":"<b>$5 Kohl’s Cash® will be sent in your shipping confirmation email.</b><div>You can use it right away — limit one offer per customer per day.</div>","bopus_incentive_evar81":"bopus ${0} incentive","ERR_ORDER_040":"Account creation failed","LINK2005":"Loyalty Profile not found in Customer MDM","kls_gift_option_sdd_error":"Item not eligible for gifting.","kls_static_shopping_bag_heading_label":"Shopping Bag","kls_static_shopping_bag_missing_error":"Some information is missing or invalid below","kls_static_shopping_bag_need_assistance":"Need Assistance?","kls_static_shopping_bag_privacy_policy":"Security &amp; Privacy Policy","kls_static_shopping_bag_returns":"Returns","kls_static_shopping_bag_shipping":"Shipping","kls_static_shopping_bag_suggestion":"You may also like","kls_static_payment_amex":"AMERICANEXPRESS","kls_static_review_one_item":"1 item","kls_static_review_items":"items","kls_static_review_gift_card":"Kohl's Gift Card","kls_static_review_value":"value:","kls_static_review_five_dollar":"$5.95","kls_static_review_gift_receipt":"Gift Receipt Included","kls_static_review_gift_qty":"Qty","kls_static_gift_option_select_item":"Select Free Gift(s) item:","kls_static_gift_option_your_gift":"Select your gift","kls_static_gift_option_free_gift":"Select a free gift","kls_static_gift_option_value":"Value","kls_static_gift_option_size":"SIZE","kls_static_gift_option_select_size":"Please Select a Size","kls_static_gift_option_select":"Select","kls_static_gift_option_please_select":"Please Select a Gift","kls_static_gift_option_dont_want_gift":"No,I don't want the gift","kls_static_gift_option_cancel":"Cancel","kls_static_pb_zero":"0","kls_static_pb_zero_dollar":"$0.00","kls_static_pb_change_location":"Change Location","kls_static_pb_view_item":"View Item","kls_static_pb_or":"or","kls_static_pb_orig_smallcase":"orig.","kls_static_pb_reg_smallcase":"reg.","kls_static_pb_ea":"ea","kls_static_pb_orig":"Orig.","kls_static_pb_reg":"Reg.","kls_static_yes_to_you":"Welcome to Yes2You Rewards!","kls_static_yes_to_you_yay":"yay!","kls_static_order_confirm_item_to":"item) to:","kls_static_order_confirm_items_to":"item(s)) to:","kls_static_pb_remove_item_to_checkout":"Remove item to check out","kls_static_pb_change_or_remove_item_to_checkout":"Change or remove item to check out","kls_static_loyalty_spend_message_10percent":"Spend <span class='text-style-1'> ${0} </span> with a Kohl's Charge <br> to get your next ${1} Kohl's Cash® reward.","kls_static_loyalty_spend_message_5percent":"Spend <span class='text-style-1'> ${0} </span> to get your next <br> ${1} Kohl's Cash® reward.","kls_static_loyalty_spend_message_event":"Spend <span class='text-style-1'> ${0} </span> today to get your next <br> ${1} Kohl's Cash®.","kls_static_loyalty_spend_message_event_sb":"Spend <span class='text-style-1'> ${0} </span> today <br> to get your next ${1} Kohl's Cash®.","kls_static_loyalty_spend_message_event_PB_belowThreshold":"Spend <span class='text-style-1'> ${0} </span> today <br><p> to get your next </p> <p> ${1} Kohl's Cash®.</p>","kls_static_loyalty_spend_message_event_PB_belowThreshold_KCC":"Spend <span class='text-style-1'> ${0} </span> <br> with a Kohl's charge<br> to get your next<br> ${1} Kohl's Cash® reward.</p>","kls_static_loyalty_spend_message_event_PB_belowThreshold_NOKCC":"Spend <span class='text-style-1'> ${0} </span> <br> to get your next <br>${1} Kohl's Cash® reward.","kls_static_loyalty_spend_message_event_PB":"Spend <span class='text-style-1'> ${0} </span> with a  Kohl's Charge to get your next <br> ${1} Kohl's Cash®.","kls_static_loyalty_earn_message_event":"<span class='text-style-1'>Earn ${0} Kohl’s Cash® for every ${1} spent today</span><br> ends {2}","kls_static_loyalty_earn_message_event_PB":"<span class='text-style-1'>Earn ${0} Kohl's Cash® for every ${1} <br> spent today</span> <br> ends {2}","kls_static_loyalty_earn_message_noEvent":"Earn {0}% Kohl’s Cash® toward a future <p> purchase with a Kohl's Charge. </p>","kls_static_loyalty_earn_message_noKCC_5percent":"Earn {0}% Kohl’s Cash® toward a future purchase.","kls_static_loyalty_earn_message_noKCC_PB":"<span>Earn {0}% Kohl’s Cash®</span> <p> towards a future </p> <p> purchase.</p>","kls_static_loyalty_earn_message_hasKCC":"<span>Earn {0}% Kohl’s Cash®</span> <p> towards a future </p> <p> purchase with a </p> <p> Kohl’s Charge. </p>","kls_static_loyalty_earn_message_noKCC":"<span class='text-style-1'>Earn</span> <span class='text-style-2'> ${0} Kohl’s Cash®</span> towards a future purchase.","kls_static_loyalty_earn_message_hasKCC_upsell":"Earn an additional <span class='text-style-1'> ${0} Kohl’s Cash®</span> on this purchase with a Kohl’s Charge.","kls_static_loyalty_earn_message_noKCC_upsell":"Earn an additional <span class='text-style-1'> ${0} Kohl’s Cash®</span> towards a future purchase.","kls_static_loyalty_earn_message_event_date":"ends {0}","kls_static_loyalty_earn_message_delta":"<div class='clearfix'>You could earn an additional <br><span class='text-style-1'> ${0} Kohl’s Cash®</span> on today’s purchase with a Kohl’s Charge.</div>","kls_static_loyalty_KCC_meter_start_amount":"$0","kls_static_loyalty_earn_message_hasKCC_redeemNotReached":"<span class='text-style-1'>Earn</span><span class='text-style-2'> {0}% Kohl’s Cash®</span> toward a future purchase with a Kohl's Charge.","kls_static_loyalty_earn_message_noKCC__redeemNotReached":"<span class='text-style-1'>Earn</span> <span class='text-style-2'> {0}% Kohl’s Cash®</span> toward a future purchase.","kls_static_loyalty_earn_message_hasKCC_redeemReached":"<span class='text-style-1'>Earn</span><span class='text-style-2'> ${0} Kohl’s Cash®</span> on this purchase <br>with a Kohl’s Charge to use {2}","kls_static_loyalty_earn_message_noKCC__redeemReached":"<span class='text-style-1'>Earn</span><span class='text-style-2'> ${0} Kohl’s Cash®</span> on this purchase <br>to use {2}","kls_static_loyalty_details_link":"details","kls_static_loyalty_learnMore_link":"learn more","kls_static_loyalty_details_redirectURL":"/feature/flex/kcash.jsp","kls_static_loyalty_miniTracker_event_message":"Spend <span class='text-style-1'>${0}</span> more today to get an <p>additional ${1} Kohl’s Cash®.</P>","kls_static_loyalty_miniTracker_noEvent_message":"Spend <span class='text-style-1'>${0}</span> more with a Kohl’s Charge <p>to get your next ${1} Kohl’s Cash® reward.</p>","kls_static_loyalty_miniTracker_noEvent_message_noKCC":"Spend <span class='text-style-1'>${0}</span> more to get your next <p>${1} Kohl’s Cash® reward.</p>","kls_static_loyalty_earn_message_paymentOrReview_nonElite":"You could earn an additional <p><span class='text-style-2'> {0} Kohl's Cash®</span></p> with your Kohl's Charge.","kls_static_loyalty_earn_message_paymentOrReview_elite":"You could earn an additional <p><span class='text-style-2'> {0} Kohl's Cash®</span></p> with your Kohl's Charge <div class='elite-free-ship'>+ free standard shipping.</div>","kls_static_loyalty_earn_message_paymentInfoBox":"<span class='text-style-1'>Earn</span><span class='text-style-2'> ${0} </span> Kohl’s Cash®.","kls_static_loyalty_Down_message_paymentInfoBox":"<span class='text-style-1'>Earn</span><span class='text-style-2'> {0}% </span> Kohl’s Cash®.","kls_static_loyaltyv2_Down_message_paymentInfoBox":"<span class='inlineMsgPaymentBold'>Earn {0}%</span> in Kohl’s Rewards","kls_static_loyalty_learnMore_terms":"Earn amounts are approximate and may vary if additional discounts are applied to the purchase transaction","kls_static_loyalty_learnMore_header1":"<h4>You could earn</h4> <h5>${0}</h5> <p>on this purchase toward your <br class='brDisplay'>next ${1} Kohl’s Cash® reward.</p>","kls_static_loyalty_learnMore_header2":"<h4>You could earn</h4> <h5>${0}</h5> <p>on this purchase with a Kohl’s <br class='brDisplay'>Charge toward your next ${1} <br class='brDisplay'>Kohl’s Cash® reward.</p>","kls_static_loyalty_learnMore_header3":"<h4>You could earn</h4> <h5>${0}</h5> <p>Kohl’s Cash® on this purchase.</p>","kls_static_loyalty_learnMore_header4":"<h4>You could earn</h4> <h5>${0}</h5> <p>Kohl’s Cash® on this purchase</p><p>with a Kohl’s Charge.</p>","kls_static_loyalty_learnMore_center":"<h5>${0}</h5> <span>Kohl's Cash&reg;</span> <p>to use {2}</p>","kls_static_loyalty_learnMore_bottom":"<span class='kc-plus'>+</span> <h5>${0}</h5> <p>toward your next ${1} Kohl's Cash® reward.</p>","kls_static_tr2_pb_vgc_email_label":"Email","kls_static_orderDetails_fail":"Thanks for your order! We're having technical difficulties so we can't display the details right now but your order has been placed successfully. You'll get your order confirmation email shortly!","kls_static_tr_cart_total_savings_loyalty":"You saved","kls_static_tr_cart_total_savings_loyalty_text":"on your purchase today","kls_static_loyalty_hasKCC":"Spend <span class='text-style-1'> {0} </span> with a Kohl's Charge to get <br> your next ${1} Kohl's Cash® reward.","kls_static_loyalty_noKCC":"Spend <span class='text-style-1'> {0} </span> to get your next <br> ${1} Kohl's Cash® reward.","kls_static_tr_confirm_order_number":"Order Number:","kls_static_tr_confirm_order_earn_message":"<div class='You-Earned'>You earned <span class='Green-Dollars'>${0} Kohl's Cash</span> today</div><div class='To-Use-Date'>to use {2}</div>","kls_static_tr_confirm_order_loyalty_error_message":"The Kohl's Cash system isn't responding right now, but don't worry! Any Kohl's Cash earned on this purchase will be sent to you in a separate email or reflected in your Kohl's Rewards balance.","vgc_email_to":"Email to","vgc_recipient_email":"Recipient Email","vgc_message":"Message","vgc_from_email":"From Email","kls_loyality_minitracker_static_message__hasKCC":"Earn {0}% Kohl’s Cash® toward a future <br> purchase with a Kohl's Charge.","kls_loyality_minitracker_static_message__noKCC":"Earn {0}% Kohl’s Cash® toward a future purchase.","kls_loyality_static_message_submit_date":"Placed on {0}","kls_static_loyalty_earn_message_paymentOrReview_pilot_check_service_is_down":"<div>You could earn an additional</div><span class='text-style-2'> {0}% Kohl's Cash®</span> <br> <div>with your Kohl's Charge</div>","kls_static_tr_whats_next":"What's Next?","kls_static_tr_whats_next_message_01":"You should receive a confirmation of your order at","kls_static_tr_whats_next_message_02":"You should receive another email when your order is shipped with tracking information.","kls_static_tr_whats_next_message_03":"Questions? We are available via Chat.<br> Click the &quot;Ask Us&quot; icon on the <a class='kohls_cash_cs_link' href='//cs.kohls.com' target='_blank'>Customer Service</a> page.","kls_static_tr_whats_next_message_04":"If you've set up your Kohl's Wallet, we'll add your Kohl's Cash for you, otherwise watch your email.","kls_static_tr_whats_next_message_05":"Kohl’s Cash® earned on this purchase will be emailed to you and to the primary cardholder of the Kohl’s Charge used.","kls_static_tr_whats_next_kc_details":"Kohl’s Cash® details","kls_static_tr2_pb_vgc_email_label_shipping_tax_model":"E-mail","kls_static_pb_edit_item":"Edit Item","kls_static_tr_manage_order":"Manage Order","demo_text":"Checkout Development Demo Text for Static Content","kls_static_bopus_store_pickup_order_review_message":"Orders are typically ready within 2 hours.  Your will receive a notification when your order is ready.  Please bring your <span class='bbold'>photo ID</span> and <span class='bbold'>pickup notification email</span> to your selected pickup store. If you have selected an additional pickup person, you can still pick up this order yourself.","kls_static_bopus_orderReview_view_faqpage_link":"<a href='https://cs.kohls.com/app/answers/detail/a_id/1169' target=_blank>Visit our FAQ page.</a>","kls_static_loyalty_upsell_message_LCS_down":"<div class='clearfix'>You could earn an additional <br><span class='text-style-1'> {0}% Kohl’s Cash®</span> on today’s purchase with a Kohl’s Charge</div>","kls_loyality_PB_static_message__hasKCC":"Earn {0}% Kohl’s Cash® <br> toward a future purchase with a <br> Kohl's Charge","kls_loyality_PB_static_message__noKCC":"Earn {0}% Kohl’s Cash® <br> toward a future purchase","kls_static_loyalty_earn_message_hasKCC_redeemNotReached_cnc":"<span class='text-style-1'>You'll earn</span><span class='text-style-2'> {0}% Kohl’s Cash®</span> on <br>this purchase with your Kohl's Charge","kls_static_loyalty_earn_message_noKCC__redeemNotReached_cnc":"<span class='text-style-1'>You'll earn</span> <span class='text-style-2'> {0}% Kohl’s Cash®</span> on <br>this purchase","kls_static_loyalty_earn_message_hasKCC_redeemReached_cnc":"<span class='text-style-1'>You'll earn</span><span class='text-style-2'> ${0} Kohl’s Cash®</span> on <br>this purchase with your Kohl’s Charge to <br>use {2}","kls_static_loyalty_earn_message_noKCC__redeemReached_cnc":"<span class='text-style-1'>You'll earn</span><span class='text-style-2'> ${0} Kohl’s Cash®</span> on <br>this purchase to use {2}","kls_static_order_confirmation_page_normal_items_email_text":"You should receive confirmation of your order at","kls_static_sdd_watch_your_inbox_additional_status_update_lpf":"You should receive another email when your order is shipped with <br>tracking information.","kls_static_order_confirmation_page_bopus_items_email_text":"You will receive an email at <b>{0}</b> from us confirming your order shortly. We'll send additional updates as we have them.","kls_static_tr_confirm_order_link_loyalty_err_msg_for_pilot_user":"Loyalty Profile is already linked with other ecom profile.","kls_static_boss_arrives":"Estimated Arrival","kls_static_arrives_add":"Estimated Arrival","kls_static_boss_change":"Change","kls_static_boss_close":"Close","kls_static_boss_free_store_pickup":"FREE Store Pickup","kls_static_boss_free_ship_to_store":"FREE Ship to Store","kls_static_boss_other_store":"check other stores","kls_static_boss_ship_to_store":"Ship to Store","kls_static_boss_freight_shipping":"Freight Shipping - Curbside","kls_static_boss_alaska_hawaii_apo_fpo":"Alaska/Hawaii & APO/FPO","kls_static_boss_standard_free":"Standard Free","kls_static_boss_ships_free":"Standard free w/$75 purchase","kls_static_boss_ship_it_faster":"ship it faster","kls_static_boss_empty_cart":"Start shopping","kls_static_boss_one_day":"One Day","kls_static_boss_one_day_shipping_charges_apply":"One Day (Shipping Charges Apply)","kls_static_boss_two_day":"Two Day","kls_static_boss_two_day_shipping_charges_apply":"Two Day (Shipping Charges Apply)","kls_static_boss_same_day_delivery":"Same Day Delivery","kls_static_elite_something_wrong":"Something went wrong.","kls_static_elite_all_set":"all set!","kls_static_elite_dont_miss":"Don’t miss out!","kls_static_elite_set_primary":"set as primary payment","kls_static_elite_add_kc":"add your Kohl's Charge","kls_static_elite_percentage_message":"<div class='elite-banner-text-2'>Get free standard shipping and {0}% Kohl’s Cash® <div class='elite-banner-text-2-s'>when you use your Kohl's Charge</div></div>","kls_static_elite_dollar_message":"<div class='elite-banner-text-2 elite-dollar-text'>Get free standard shipping and an additional<div>${0} Kohl’s Cash® <span class='elite-banner-text-2-s'>when you use your Kohl's Charge</span></div></div>","kls_static_boss_shipping_pickup_message":"Pickup &amp; Shipping Options","kls_static_boss_shipping_pickup_multiship_message":"Pickup &amp; Shipping Options","kls_static_boss_order_process_message":"You will receive an email when your \"Free Store Pickup\" order is ready. 'Free Store Pickup ","kls_static_store_pick_heading_v2":"PICK UP IN STORE","kls_static_boss_order_placed_after_v2":"<b>Ship to Me</b> orders placed after 1pm CST will be processed the next day. Your order may be divided into two or more shipments. Shipping charges will not be afffected.","kls_static_boss_order_process_message_v2":"You'll receive an email when your order is ready for pickup. Store pickup orders are ready within 2 hours after they are placed online. <b>Free Store Pickup Today</b> orders placed after 7pm local time will be processed the next business day","kls_static_boss_order_process_message_2":"' orders placed after 5pm (local time) will be processed the next business day. Orders are typically ready within 2 hours.","kls_static_tr_boss_ship":"Ship","kls_static_tr_boss_checkout_orders_placed_after":" orders placed after 1pm (Central Time) will be processed the next day. We may need to divide your order into two or more shipments. Shipping charges will not be affected by this.","kls_static_sdd_boss_order_process_shipping_message":"Same Day Delivery orders must be placed by 1 pm local time to arrive by 8 pm local time today. We'll email you with updates.","kls_static_boss_order_process_shipping_message":"Ship orders placed after 1pm (Central Time) will be processed the next day. We may need to divide your order into two or more shipments. Shipping charges will not be affected by this","kls_static_bopus_free_store_pickup":"FREE Store Pickup <b>Today</b>","kls_static_bopus_today":"Today","kls_static_boss_shopping_bag_heading_label":"Shopping Cart","kls_static_boss_shopping_bag_checkout":"checkout","kls_static_new_smartcard_incentive_eligible":"<span class='Get-5-Kohls-Cash'>Get ${0} Kohl's Cash</span><span class='when-you-pick-up-thi'>when you pick up this order</span><a class='details' href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_new_smartcard_incentive_qualified":"<span class='Get-5-Kohls-Cash'>Your ${0} Kohl's Cash<sup>®</sup> will be sent in your<br/> pickup confirmation email.</span><span class='when-you-pick-up-thi'> Limit one offer per<br/>customer per day.</span><br/><a class='details' href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_pick_up_my_order":"Pick up my order","kls_static_boss_manageorder_button":"Manage Order","kls_static_tr_boss_order_confirmation_payment_email":"Email for Confirmation","kls_static_boss_gift_Pickup_msg":"PICKUP","kls_static_tr_cart_qty":"QTY","kls_static_boss_not_available_for_pickup":"Not available for Pickup","kls_static_boss_store_pickup_sla_message_bopustext":"Free Store Pickup Today orders placed after 5pm (local time) will be processed the next business day. Orders are typically ready within 2 hours. You will receive a notification when your order is ready.","kls_static_boss_store_pickup_sla_message_bosstext":"Free Pickup in store (Ship to Store) orders usually take 3- 6 business days to reach the selected store before being processed for store pickup. You'll receive a notification email when your order is ready for pickup","kls_boss_static_bopus_store_pickup_order_review_message":"Free Store Pickup Today orders are typically ready within 2 hours.  You'll receive a notification when your order is ready.  Please bring your <span class='bbold'>photo ID</span> and <span class='bbold'>pickup notification email</span> to your selected pickup store. If you have selected an additional pickup person, you can still pick up this order yourself.","kls_boss_static_ship_order_review_message":" Free Pickup in store (Ship to Store) orders usually take 3- 6 business days to reach the selected store before being processed for store pickup. You'll receive a notification email when your order is ready for pickup.","kls_set_elite_kohls_charge_as_preferred_failed":"set elite kohls charge as preferred failed","kls_elite_status_check_failed":"elite status check failed","kls_set_elite_kohls_charge_as_preferred_failed_new":"a new preferred payment type could not be set","kls_static_tr_free_standard_shipping":"+ free standard shipping","whatnextShipOnlyFirstText":"You'll receive an email from us at <b> {0}</b> confirming your order shortly.","whatnextShipOnlySecondText":"Watch your inbox for additional status updates.","whatnextBossOnlyorShipandBossFirstText":"You'll receive an email from us at <b>{0} </b> confirming your order shortly. Watch your inbox for additional status updates.","whatnextBossOnlyorShipandBossSecondText":" If there are items in your order with different pickup dates, you'll receive separate email notifications when they are ready.","whatnextMixedItemsFirstText":"You'll receive an email from us at <b>{0} </b> confirming your order shortly. Watch your inbox for additional status updates.","whatnextMixedItemsSecondText":"If there are items in your order with different pickup dates, you'll receive separate email notifications when they are ready. Same day store pick up orders are typically ready for pickup within 2 hours.","kls_boss_static_registry_error_inventry_low":"Unfortunately inventory is getting low and there are only {0} available for purchase. Please change the quantity","kls_boss_static_error_choose_onther_option":"Please choose another option","kls_static_new_smartcard_incentive_err_message":"Sorry, your cart no longer qualifies for the ${0} Kohl's Cash® pickup deal.","kls_static_smartcard_incentive_qualified_new":"Your &#036;{0} Kohl’s Cash  will be sent in your pickup confirmation email.<br/> Limit one offer per customer per day. <a href=\"https://www.kohls.com/ecom/kohls-smartcart-kcash.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_smartcard_incentive_eligible_new":"Get &#036;{0} Kohl's Cash when you pick up this order","kls_static_new_smartcard_incentive_oos_err_message":"Sorry, no item currently available for pickup at <span class='CamelCap'>{0}</span></span>","kls_boss_static_error_choose_onther_option_instore_one":"Please choose another option. Use","kls_boss_static_error_choose_onther_option_instore_two":"check other stores","kls_boss_static_error_choose_onther_option_instore_three":"link below to find free pickup at nearby stores","kls_boss_static_error_no_longer_available":"This item is no longer available","kls_boss_static_error_apo_cant_ship_address":"This item can't be shipped to the address entered. Please provide another shipping address","kls_static_boss_not_available_for_ship":"Not Available to Ship","kls_static_boss_inventory_getting_low":"Unfortunately you can only purchase {0} of this product. Please try again!","kls_One_or_more_items_in_your_cart_has_changed":"One or more item(s) in your cart has changed.","kls_Adjust_items_below_to_continue":"Adjust item(s) below to continue.","kls_boss_static_registry_error_pickup_inventry_low":"Unfortunately inventory is getting low and there are only {0} available for purchase. Please change the quantity or choose another store","kls_static_pick_up_count":"${0} item available to pick up","kls_static_pick_up_count_items":"${0} items available to pick up","kls_static_boss_inventory_getting_low_forship":"Unfortunately we are only able to sell you {0}. Please select a lower amount and try again!","kls_static_boss_max_error":"You are only allowed {0} item(s) in the Shopping Cart. Adjust cart to continue","kls_static_pick_up_update_message":"${0} item updated","kls_static_pick_up_update_items_message":"${0} items updated","kls_boss_static_shoprunner_empty_shopping_bag_msg":"Please review your Shopping Cart before you checkout.&lt;br/&gt;If items were saved to your Shopping Cart from a previous visit, product pricing or availability may have changed.&lt;br/&gt;For help and details about promotions, refer to the &lt;a href=&#034;#&#034; class = &#034;shoprunner_anchor_style&#034;&gt;Learn More&lt;/a&gt; page.","kls_boss_static_tr_cart_shoppingBagInfo":"Please review your Shopping Cart before you checkout. &lt;br&gt;If items were saved to your Shopping Cart from a previous visit, product pricing or availability may have changed.","kls_static_boss_sdd_order_review_error":"Same Day Delivery is not available. Select a different shipping method","kls_static_boss_session_out_error_first_line":"Your order contents were updated from a different session.","kls_static_boss_session_out_error_second_line":"Please proceed again.","kls_static_tr_orderconfirm_regular":"Regular","kls_boss_static_error_select_adress":"Please select a shipping address","kls_static_boss_not_available_for_pickup_anymore":"Not available for pickup anymore. Check ship availability","kls_static_boss_oic_down_error_msg":"There is an error while processing the request","kls_static_addressy_verify_msg":"Choose the verified address to avoid non-delivery or delays.","kls_static_addressy_you_entered":"You entered","kls_static_addressy_verified_address":"Verified Address","kls_static_norush_header":"<span class='no-rush-text'>Not in a rush?</span><span class='no-rush-kohls-text'>Get $5 Kohl’s Cash<sup>®</sup>.</span><div>Limit one offer per customer per day.</div><a class='details no-rush-details' href=\"https://www.kohls.com/ecom/NORUSHKOHLS.html\" title=\"details\" target=\"_blank\">details</a>","kls_static_norush_toggle_text":"No rush shipping","kls_static_norush_qualification_text":"Your $5 Kohl’s Cash<sup>®</sup> will be sent in your shipping confirmation email.","kls_bopus_static_error_out_stock":"Item is not available at selected store. Please select a different store or have it shipped.","kls_sdd_static_error_out_stock":"Same Day Delivery is not available. Select a different shipping method","kls_static_loyaltyv2_loyalty_down":"Rewards earned on this purchase will be added to your Kohl’s Rewards account within 48 hours.","kls_static_loyaltyv2_rewards_added_balance":"added to balance","kls_static_loyaltyv2_title_your_earnings":"YOUR EARNINGS","kls_static_loyaltyv2_title_kohls_rewards":"Kohl's Rewards","kls_static_loyaltyv2_title_kohls_cash":"Kohl's Cash<sup>®</sup>","kls_static_loyaltyv2_kc_to_use_date":"to use <span id='kccUseByDate'>{0}</span>","kls_static_loyaltyv2_spendMsg_NonKcc":"Spend <span class='spendawayAmountLoyalty'>${0}</span> to earn your next <span>${1}</span> in <br/> Kohl’s Rewards.","kls_static_loyaltyv2_spendMsg_NonKcc_checkout":"Spend <span class='spendawayAmountLoyalty'>${0}</span> to earn your next<br/> <span>${1}</span> in Kohl’s Rewards.","kls_static_loyaltyv2_spendMsg_Kcc":"Spend <span class='spendawayAmountLoyalty'>${0}</span> today with your Kohl’s Charge to earn your <br />next <span>${1}</span> in Kohl’s Rewards.","kls_static_loyaltyv2_spendMsg_Kcc_checkout":"Spend <span class='spendawayAmountLoyalty'>${0}</span> today with your Kohl’s Charge <br />to earn your next <span>${1}</span> in Kohl’s Rewards.","kls_static_loyaltyv2_spendAway_msg_generic":"Earn <span class='spendawayAmountLoyalty'>{0}% in Kohl’s Rewards</span> when you use your Kohl’s Charge today.","kls_static_loyaltyv2_spendAway_msg_genericNonKCC":"Earn <span class='spendawayAmountLoyalty'>{0}% in Kohl’s Rewards</span> today.","kls_static_loyaltyv2_rewardsBrkdown_title":"This purchase would add","kls_static_loyaltyv2_rewardsBrkdown_bottomtxt":"Your Kohl’s Rewards balance is converted and issued in {1} Kohl’s Cash<sup>®</sup> increments on the first of the following month, valid for 30 days.","kls_static_loyaltyv2_rewardsBrkdown_bottomtxtAmt":"$5","kls_static_loyaltyv2_rewardsBrkdown_rewardsdetailsLink":"Kohl’s Rewards details","kls_static_loyaltyv2_rewardsBrkdown_amtBottomtxt":"to your Kohl's Rewards balance.","kls_static_loyaltyv2_spend_event_message":"Spend <span class='spendawayAmountLoyalty'> ${0} </span> today to get your next <span>${1} </span> Kohl's Cash<sup>®</sup> </br> to use<span> {2}.</span>","kls_static_loyaltyv2_spend_event_message_checkout":"Spend <span class='spendawayAmountLoyalty'> ${0} </span> today to get your next <br /><span>${1} </span> Kohl's Cash<sup>®</sup> to use<span> {2}.</span>","kls_static_loyaltyv2_spend_event_genericMsg":"Get <span class='spendawayAmountLoyalty'> ${0} Kohl's Cash<sup>®</sup></span> for every ${1} spent<br /> today to use <span class='spendawayDate'>{2}</span>.","kls_static_loyaltyv2_spendMsg_minicart_NonKcc":"<span>Spend</span><br><span class='kc_bonus_event_text_amount'>${0}</span><p class='spendAmountNoKcc'>to earn your<br>next <span id='kc_bonus_event_earn_amount'> ${1} </span> in </br>Kohl’s Rewards.</p>","kls_static_loyaltyv2_spendMsg_minicart_Kcc":"<span class='rewardSpendTextSize'>Spend</span><br><span class='kc_bonus_event_text_amount_reward' id='kc_bonus_event_spend_amount'>${0}</span> <p class='spendAmountNoKcc'>today with your<br>Kohl’s Charge<br> to earn your<br> next <span>${1}</span> in<br> Kohl’s Rewards.</p>","kls_static_loyaltyv2_minicart_spend_message":"<div class='spendTextSize'><span>Spend</span><br> <span class='kc_bonus_event_text_amount'>${0}</span><br><br></div><p class='todayGetAmountText'> today to get<br> your next <span>${1}</span><br> Kohl’s Cash® to<br> use <span class='startEndDate'> {2}</span><span> - </span><br><span>{3}</span>.</p>","kls_static_loyaltyv2_minicart_get_message":"<span class='getTextSize'>Get &nbsp;</span><span class='kc_bonus_event_get_amount' id='kc_bonus_event_get_amount'>${0}</span><br><span class='kc_bonus_event_get_amount'>Kohl's Cash<sup>®</sup></span><br><span class='kc_bonus_event_text_msg'>for every <span id='kc_spend_away_amount'> ${1} </span> spent today to<br> use <span id='kc_spend_away_date'>{2}</span><span> - </span><br><span>{3}</span>.</span>","kls_static_loyaltyv2_minicart_spendAway_msg_genericNonKCC":"<p class='earnPercentageText'>Earn <span class='kc_bonus_event_no_amount' id='kc_bonus_event_no_amount'>{0}%<br>in Kohl’s<br> Rewards</span><br> today.</p>","kls_static_loyaltyv2_minicart_spendAway_msg_generic":"Earn <span class='kc_bonus_event_no_amount' id='kc_bonus_event_no_amount'>{0}%</span><br><span class='kc_bonus_event_no_amount'>in Kohl's<br>Rewards</span><br><span>when you use<br> your Kohl’s<br> Charge today.</span>","kls_static_loyaltyv2_upsell_message_shopping_cart":"Take an extra {0}% off your first </br> Kohl’s Charge purchase.*","kls_static_loyaltyv2_upsell_msg_subtocredit_txt":"*Subject to credit approval. Terms and exclusions apply.","kls_static_free_standard_shipping_messageV2":"<span class='mvc-msg-stpmr'>YOU HAVE FREE STANDARD SHIPPING</span>","kls_static_loyaltyv2_noEliteCard_kccPercentage_switchMsg":"Get <span class='spendawayTracker_earnThreshold'>free standard shipping</span>, plus earn an extra </br><span class='spendawayTracker_earnThreshold'>${0} in Kohl’s Rewards</span> when you use your </br>Kohl’s Charge today.","kls_static_loyaltyv2_noEliteFreeshipping_switchMsg":"Earn an extra <span class='spendawayTracker_earnThreshold'>${0} in Kohl’s Rewards</span> when </br>you use your Kohl’s Charge today.","kls_static_loyaltyv2_noEliteCard_generic_switchMsg":"Get <span class='spendawayTracker_earnThreshold'>free standard shipping</span> when you use your </br>Kohl’s Charge today.","kls_static_loyaltyv2_inlineMsg_extraEvent":"Get<span class='inlineMsgPaymentBold'> free standard shipping</span> <br/> <span class='inlineMsgPaymentBold'>+ ${0}</span> in Kohl’s Rewards","kls_static_loyaltyv2_loyaltyv2_down_msg_inlineMsg_extraEvent":"Get<span class='inlineMsgPaymentBold'> free standard shipping</span> <br/> <span class='inlineMsgPaymentBold'>+ {0}%</span> in Kohl’s Rewards","kls_static_loyaltyv2_inlineMsg_noExtraEvent":"Get<span  class='inlineMsgPaymentBold'> free standard shipping</span> <br/>with your Kohl’s Charge","kls_static_loyaltyv2_inlineMsg_notPreferedElite":"<span class='inlineMsgPaymentBold'>Earn ${0}</span><br/> in Kohl’s Rewards","kls_static_kcc_not_set_primary_generic_message":"Have your Kohl's Charge ready to go at checkout.","kls_static_kcc_not_set_primary_additional_earn_event_calculated_message":"Earn an extra <span class='switchMsgBold'>${0} in Kohl’s Rewards</span> when you use your Kohl’s Charge today.","kls_static_kcc_not_set_primary_additional_earn_event_generic_message":"Earn <span  class='switchMsgBold'>{0}% in Kohl’s Rewards</span> when you use your Kohl’s Charge today.","kls_static_get_free_shipping_earn_extra_dollar_message":"Get <span class='switchMsgBold'>free standard shipping</span>, plus earn an extra <span class='switchMsgBold'>${0} in Kohl’s Rewards</span> when you use your Kohl’s Charge today.","kls_static_get_free_shipping_earn_extra_percent_message":"Get <span class='switchMsgBold'>free standard shipping</span>, plus earn <span class='switchMsgBold'>{0}% in Kohl’s Rewards</span> when you use your Kohl’s Charge today.","kls_static_kcc_free_shipping_message":"YOU HAVE FREE STANDARD SHIPPING","kls_static_kcc_not_primary_payment_method_message":"Get <span  class='switchMsgBold'>free standard shipping</span> when you use your Kohl’s Charge today.","kls_static_loyaltyv2_ocp_Rewards_msg_txt_lpfOff":"Rewards earned on this purchase will be added to your Kohl’s Rewards account within 48 hours.","kls_static_loyaltyv2_ocp_Rewards_msg_txt_lpfOff_conf":"<span>Rewards earned on this purchase will be added to your Kohl’s Rewards account within 48 hours.</span>","kls_static_loyaltyv2_ocp_Rewards_msg_yoursavings_lpfV2":"<span class='orderSummary_lpfV2_yousaved'>You saved<span class='ordersummary_yoursavings_lpfV2_savedAmount'> ${0}</span></span><br><span class='ordersummary_yoursavings_lpfV2_offrtxt'>on your purchase today.</span>","kls_static_loyaltyv2_ocp_Rewards_msg_txt_lpfOn":"<div class='orderConf_updBlnc'>${0} updated balance</div><div class='orderConf_prevBlnc'>${1} previous balance +</div><div class='orderConf_earndBlnc'>${2} earned  on this purchase</div>","kls_static_loyaltyv2_ocp_whats_next_message_rewardadded":"Your Kohl’s Rewards balance is converted and issued in ${0} Kohl’s Cash® increments on the first of the following month, valid for 30 days.","kls_static_loyaltyv2_rewards_details_ocp_redirectURL":"https://www.kohls.com/feature/kohls-rewards.jsp","kls_static_loyaltyv2_tr_confirm_order_earn_message":"<div class='You_Earned_V2'> <span style='text-transform: capitalize;'>{customerName}</span>, you earned ${0} Kohl’s Cash® today!</div><div class='You_Earned_date_V2'>Spend it {2}</div>","kls_static_guest_free_shipping_messageV2":"Looks like you're on a roll! <br /> <span class='fs-bottom-text'>Keep the savings going with these picks:</span>","kls_static_guest_not_free_shipping_messageV2":"Hi, free shipping is just ${0} away! <br /> <span class='fs-bottom-text'>Check out these picks to get you one step closer:</span>","kls_static_loggedin_free_shipping_messageV2":"Hi {0}, you're on a roll! <br /> <span class='fs-bottom-text'>Keep the savings going with these picks:</span>","kls_static_loggedin_not_free_shipping_messageV2":"Hi {0}, free shipping is just ${1} away! <br /> <span class='fs-bottom-text'>Check out these picks to get you one step closer:","kls_static_free_shipping_banner_bg_colorV2":"background: #b90082;","kls_static_kccUpsellMessage_redirectURL":"https://apply.kohls.com/","kls_static_kccUpsellMessage_text":"Apply now","kls_static_you_earned_free_shipping":"You’ve earned free shipping!","kls_static_tr2_free_shipping_suggest_msg_order_summary_pageV20s":"free shipping.","kls_static_birthdayperk_want_more_text":"Want more of what you love?","kls_static_set_prefrences_text":"SET PREFERENCES","kls_static_members_exclusive_text":"MEMBER EXCLUSIVE","kls_static_birthday_text":"Your birthday's<br /> coming up...","kls_static_birthday_link_text":"PICK YOUR PERK","err_invalid_cvv":"Your payment could not be processed. Please check your CVV and try again. For assistance, contact us at (800) 564-5740 to complete your order.","err_invalid_kc_ms_card":"There was an issue processing your payment. Please contact us at (800) 564-5740 to confirm that your order was placed.","kls_save_for_later_text":"Saved for later ({0})","kls_static_error_LIST1004":"You've saved the maximum number of products. Please remove some products to add more.","kls_move_to_cart_text":"Add to cart","kls_more_items_text":"See more","kls_delete_text":"Remove","kls_static_out_of_stock_heading":"Out of stock","kls_static_saveforlater_text":"Save for later","kls_s4l_errortext":"There is an issue processing your request. Please try again after some time.","kls_s4ladd_errortext":"There is an issue processing your request. Please try again after some time.","addressType":"Shipping Address","addressyShippingErrormessage":"We couldn’t verify this address.<br> Please make changes or confirm below.","addressyYouEnteredLable":"You entered","addressySaveChangeLabel":"MAKE CHANGES","addressyCancelLabel":"IT'S CORRECT","kls_static_Rewards_enroll_confirm":"Don't miss out! Enroll in Kohl's Rewards and earn more Kohl's Cash.","kls_static_Rewards_order_confrm_signup":"ENROLL TODAY","kls_static_Rewards_text":"Welcome to Kohl's Rewards!","kls_static_loyalty_Rewards_user_message1":"Rewards earned on this purchase will be added to your Kohl’s Rewards account within 48 hours.","kls_static_loyalty_Rewards_user_message2":"Rewards are converted and issued in $5 Kohl’s Cash increments on the first of each month, valid for 30 days.","kls_static_Rewards_program_details_text":"Learn More","kls_static_Rewards_program_faq_text":"FAQs","kls_static_checkout_loyalty_linking_error_msg":"<span class=\"sry_link_text\">Sorry, we had trouble linking your Kohl's Rewards ID.</span><br><span class=\"to_earn_text\">Contact us at (844) 564-5704 to link your Kohl's Rewards ID to your <a href=\"/myaccount/kohls_rewards.jsp\" class=\"try_again_text\">Kohls.com</a> shopping account.</span>","kls_static_chect_loyalty_enroll_error_msg":"<span class='sry_link_text'>Sorry! We had trouble enrolling you in Kohl's Rewards.</span><br><span class='to_earn_text'>Tap Try Again below or contact us at (844) 564-5704.</a></span>","err_creditCard_needed":"Looks like the gift card is not covering the full amount. Please add your credit card details in payment page and try again.","credit_card_missing":"Please add the credit card information.","payment_ERR_PYMNT_010":"Looks like the gift card is not covering the full amount. Please add your credit card details in payment page and try again.","payment_ERR_PYMNT_013":"Please add the CVV information.","payment_ERR_PYMNT_014":"Currently we have an issue with the tax system. Please add the credit card details in order to place this order.","placeOrder_ERR_PYMNT_013":"Please go back to the payment page and confirm the CVV for your credit card. We'll use your gift card first.","placeOrder_ERR_PYMNT_014":"Please add the credit card information in order to place this order.","kls_static_checkout_billing_missing":"Please enter a billing address so you can check your order status after purchase.","kls_curbside_pickup_text":"Pick up this order at","kls_curbside_pickup_instore_label":"In Store","kls_curbside_pickup_instore_text":"Pick it up at Customer Service.","kls_curbside_pickup_label":"Drive Up","kls_curbside_pickup_label_text":"We'll bring it out to you.","kls_curbside_notavailable":"Not available","kls_curbside_pickup_itemLevel_instore_label":"In Store","kls_curbside_pickup_itemLevel_pickup_label":"Drive Up","kls_orderconfirmation_pickup_label":"Pickup :","kls_place_order_review_text":"By placing your order, you agree to Kohl's <a href=\"/feature/legal-notices.jsp\" target=\"_blank\">Legal Notices</a> and  <a href=\"/feature/privacy-policy.jsp\" target=\"_blank\">Privacy Policy</a>.","kls_static_continue_shipping_error_message":"Sorry, something went wrong. Please try again later.","kls_static_continue_payment_error_message":"Sorry, something went wrong. Please try again later.","kls_static_continue_review_error_message":"Sorry, something went wrong. Please try again later.","kls_static_continue_placeOrder_error_message":"Sorry, something went wrong. Please try again later.","kls_static_global_shipping_text":"","kls_static_global_orderreview_text":"","kls_static_error_OFS_ship_message":"Not available to ship. Check availability to pickup in-store.","kls_static_error_OFS_BOSS_message":"Not available to ship to store. Check availability to pickup in-store."};	var brightTagV2PageDetails = {"pageName":"shopping_cart","pageType":"Shopping cart","defaultLanguage":"English"};	var trBopusLabels = {"cart_ship":"SHIP","cart_spt":"FREE STORE PICKUP","vgc_email_ship":"EMAIL"};	var trStaticKeys = {"only":"Only","points":"potential points","nextReward":"to next reward!","kohlsCashPartOne":"to next","kohlsCashPartTwo":"Kohls Cash"};	var trShoppingCartErrors = {"sdd_place_order_error_header":"The following item(s) are not available for Same Day Delivery.  <br/>Please select another shipping method.","sdd_please_another_shipmethod":"Please select another shipping method","sdd_please_not_available":"Same Day Delivery is no longer available.<br>Please select another shipping method.","sdd_please_out_of_stock":"kls_message_sdd_error_item_is_no_longer_available_because_item_is_out_of_stock","please_storenot_available_fis":"We're sorry, this item is not available for pick up with in {0} miles of {1}. Please try another location.","please_storenot_available_withqty":"We're sorry, but only {0} are available at this store. Please change your quantity or select a different store.","please_shipnot_available_withqty":"We're sorry, but only {0} are available to ship. Please select store pick up.","please_shiperror_cart":"Not available to ship. Check in-store pick up availability","please_store_cart":"Select a different store","please_miles_cart":"Not available within {0} miles of {1}","please_store_abovecart":"Not available at this store. Please select a different store","please_miles_abovecart":"Not available nearby. Select a different store or have it shipped","killSwithError":"kls_message_bopus_cart_killswitcherror","cart_ship":"SHIP","cart_spt":"FREE STORE PICKUP","cartchecking_availability":"Checking availability...","cart_selectthisstore":"kls_static_bopus_cart_selectthisstore","view_other_stores":"VIEW MORE STORES","storeavailability":"Available at","shipavailability":"Available to ship","storenot_available":"Not available at","shipnot_available":"Not available to ship","reserve_inventory_error":"The item is out of stock","cart_selectStore":"Select a store","item_inactive":"Not Available","item_unavailable_withQty":"You can only purchase {0} for this item {1}, please try again.","errormoved_temporarily":"kls_message_eso_error_ship_method_invalid_state","bopus_no_longer_available":"{0} is no longer available for in-store pick up. Please change to ship.","sdd_not_available_selectedaddress":"Same Day Delivery is not available for the selected address. Please select another shipping method.","sdd_available_limitedqty":"The following items have a limited quantity available for Same Day Delivery.<br>Please reduce the quantity or select another shipping method.","bopus_change_to_ship":"Please change to ship.","sdd_available_for_sdd":"Available for Same Day Delivery","store_sdd_availability":"Available for Same Day Delivery","please_only_shiperror_cart":"Not available to ship","please_select_valid_quantity_for_your_item":"Please select a quantity for your item.","item_cannot_shipped_address":"The item cannot be shipped to the shipping address entered. Please provide a different shipping address.","item_inventory_unavailability_withQty":"We are sorry! SKU {1} is no longer available for purchase. To continue your checkout, you must first remove this item from your shopping bag","vgc_email_to":"Email to","vgc_recipient_email":"Recipient Email","vgc_message":"Message","vgc_from_email":"From Email","kls_boss_static_ship_inventory_getting_low":"This item is currently unavailable. Please remove the item in order to proceed with checkout.","kls_boss_static_bopus_inventory_getting_low":"Not available at this store any more. Select a different store or have it shipped","item_unavailable_boss_withQty":"Unfortunately we are only able to sell you {0}. Please select a lower amount and try again!","item_unavailable_bopus_withQty":"Unfortunately inventory is getting low and there are only {0} available for purchase. Please change the quantity or choose another store","kls_boss_static_noship_bossBopus_low":"Not available to ship. Check in-store pickup availability"};	var External = {"omniture":{"omniLoyNotLogin":"loyalty not logged in","omniLoyLogin":"loyalty logged in","omniLoyLogin2018":"loyalty 2018 logged in","omniKohlsNotLogin":"kohls not logged in","omniKohlsLogin":"kohls logged in","trackingCookiePrefix":"klsbrwcki|","orderDetailsMap":"{\"emptyCart\": \"no cart\", \"true\": \"guest\", \"false\": \"registered\"}","omniPlatform":"cloud17","suitesStandard":"kohlscomprod","omniLoyEliteLogin2018":"loyalty 2018 elite logged in","loyaltymvcloggedin":"loyalty mvc logged in","kohlssoftloggedin":"kohls soft logged in","loyaltymvcsoftloggedin":"loyalty mvc soft logged in"},"skavaFileURL":"//cdnassets-kohls.skavaone.com/pdn/"};</script>

		
  <script src="/cnc/media/37.0.0-842/javascript/deploy/framework.js"></script >
	<script src="/cnc/media/37.0.0-842/javascript/jquery.validate.js"></script>
	<script src="/cnc/media/37.0.0-842/javascript/jquery.maskedinput.js"></script>
	<script src="/cnc/media/37.0.0-842/js/kohls.restrictCharacter.js"></script>
	<script src="/cnc/media/37.0.0-842/javascript/deploy/shoppingcart.js"></script>
	<script src="/cnc/media/37.0.0-842/javascript/deploy/shoppingtrcartR51.js"></script>
	<script src="/cnc/media/37.0.0-842/js/kohlsutility.js"></script>
	
	
		
	

	<script src="/cnc/media/37.0.0-842/omniture/s_code.js"></script>
	<script src="/cnc/media/37.0.0-842/javascript/omniture_tracking.js"></script>
	<script src="/cnc/media/37.0.0-842/js/csspopup.js"></script>


	
	<script type="text/javascript">
		$load.css('//cdnassets-kohls.skavaone.com/pdn/wishlist.css');
		$requires(['//cdnassets-kohls.skavaone.com/pdn/wishlist.js' ],function(){
			$env('skava');
		});
		$env.skavaJs = [];
		 (function() {
			$env.skavaJs.push('deploy/kohls_v1_m56577569839297458.js');
			$requires($env.skavaJs, {
                onLoad: function(error) {
                    if (!error) $env('skava');	// Fire any skava dependent code
                },
                topLoad: !$env.isSkavaPerformanceEnabled,
                isLast: $env.isSkavaPerformanceEnabled
            });
		 })();

	</script >
	

<!-- CommonPageScript begin -->

	<script type="text/javascript">
		$requires('https://cdn.zineone.com/apps/latest/z1m.js', $requires.K.foreign, {
			onLoad: function(error) { if (!error) ZineOne.initialize('apps@2b6b5a02-b3e0-4a05-9e3b-d727afbfa357'); }
		});
	</script>

<!-- CommonPageScript end -->

	
  <script>(window.BOOMR_mq=window.BOOMR_mq||[]).push(["addVar",{"rua.upush":"false","rua.cpush":"false","rua.upre":"false","rua.cpre":"false","rua.uprl":"false","rua.cprl":"false","rua.cprf":"false","rua.trans":"","rua.cook":"false","rua.ims":"false","rua.ufprl":"false","rua.cfprl":"false","rua.isuxp":"","rua.texp":""}]);</script>
                          <script>!function(a){var e="https://s.go-mpulse.net/boomerang/",t="addEventListener";if("False"=="True")a.BOOMR_config=a.BOOMR_config||{},a.BOOMR_config.PageParams=a.BOOMR_config.PageParams||{},a.BOOMR_config.PageParams.pci=!0,e="https://s2.go-mpulse.net/boomerang/";if(window.BOOMR_API_key="4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T",function(){function n(e){a.BOOMR_onload=e&&e.timeStamp||(new Date).getTime()}if(!a.BOOMR||!a.BOOMR.version&&!a.BOOMR.snippetExecuted){a.BOOMR=a.BOOMR||{},a.BOOMR.snippetExecuted=!0;var i,_,o,r=document.createElement("iframe");if(a[t])a[t]("load",n,!1);else if(a.attachEvent)a.attachEvent("onload",n);r.src="javascript:void(0)",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="width:0;height:0;border:0;display:none;",o=document.getElementsByTagName("script")[0],o.parentNode.insertBefore(r,o);try{_=r.contentWindow.document}catch(O){i=document.domain,r.src="javascript:var d=document.open();d.domain='"+i+"';void(0);",_=r.contentWindow.document}_.open()._l=function(){var a=this.createElement("script");if(i)this.domain=i;a.id="boomr-if-as",a.src=e+"4BQ4T-P2CA2-RNGSJ-DLGVP-DF78T",BOOMR_lstart=(new Date).getTime(),this.body.appendChild(a)},_.write("<bo"+'dy onload="document._l();">'),_.close()}}(),"600".length>0)if(a&&"performance"in a&&a.performance&&"function"==typeof a.performance.setResourceTimingBufferSize)a.performance.setResourceTimingBufferSize(600);!function(){if(BOOMR=a.BOOMR||{},BOOMR.plugins=BOOMR.plugins||{},!BOOMR.plugins.AK){var e=""=="true"?1:0,t="",n="m7n6kytilmwg4yfhnm2q-f-c93dd74d1-clientnsv4-s.akamaihd.net",i={"ak.v":"31","ak.cp":"225119","ak.ai":parseInt("225159",10),"ak.ol":"0","ak.cr":41,"ak.ipv":4,"ak.proto":"h2","ak.rid":"1edcb5b","ak.r":40773,"ak.a2":e,"ak.m":"dscx","ak.n":"essl","ak.bpcip":"103.219.229.0","ak.cport":32152,"ak.gh":"104.90.5.77","ak.quicv":"","ak.tlsv":"tls1.3","ak.0rtt":"","ak.csrc":"-","ak.acc":"","ak.t":"1621584693","ak.ak":"hOBiQwZUYzCg5VSAfCLimQ==MdAAWhShajXfulO4Y6bEggG8WP7GnFSISDbmK230WixYRFHGvx8AmwLotgZV6Dp1QMziAle3LAseIKDI8LBoZpumsn4bBvc/XvLnAvw3csM2d9y5HXaMJy99+C7S6Rh97ETaWTsw+JjC/GaOIu8mS3xFlQCkB3wlmgvi0tFUVu5l4mnBGYkF18ABaHuRqPeAAIZ2Kbl/rrm1sb6E41lGdWSIqi2z4bXDFhemgcQPZVIqZH5VTZcQwAqX+BDCFU1oE8UPH2/oiouKq9ROwoCcrde4KfaST7hpTFeK/2KIJhJa8nXDuxa0BwzNbdHJPaVftnQu5lQo3Cr833WfOPbgFPObp3s2YX6zXeZBbgFmUYkT0azqsNcHNA8vql9TpUQaTuw0Kg/1eTgEPHDryFNBXoD3iW2rmTfJfos15LZFQLk=","ak.pv":"1073","ak.dpoabenc":""};if(""!==t)i["ak.ruds"]=t;var _={i:!1,av:function(e){var t="http.initiator";if(e&&(!e[t]||"spa_hard"===e[t]))i["ak.feo"]=void 0!==a.aFeoApplied?1:0,BOOMR.addVar(i)},rv:function(){var a=["ak.bpcip","ak.cport","ak.cr","ak.csrc","ak.gh","ak.ipv","ak.m","ak.n","ak.ol","ak.proto","ak.quicv","ak.tlsv","ak.0rtt","ak.r","ak.acc","ak.t"];BOOMR.removeVar(a)}};BOOMR.plugins.AK={akVars:i,akDNSPreFetchDomain:n,init:function(){if(!_.i){var a=BOOMR.subscribe;a("before_beacon",_.av,null,null),a("onbeacon",_.rv,null,null),_.i=!0}return this},is_complete:function(){return!0}}}}()}(window);</script></head>
	<body class="">
		
	<div id="container">
		<div id="header-region">
			
			
	
		


<!-- START ::  Personalization Kills switch -->
		<script type="text/javascript">
			(function(){
				$env = this.$env||{};		
				$env.personalisationWithUserIdSearch = 'true';
				$env.personalisationWithUserIdBrowse = 'true';	
				$env.personalisedAnonymous = 'true';	
			}).call(this);
		</script>	
		<!-- START ::  Personalization Kills switch --><!-- START :: DFP Marquee Ad Library :: dfp_globalheader_marquee_ad -->
			<script type="text/javascript">
				$init(function _$init_dfp_marquee($) {
				
					var PageUrl  = window.location.pathname;
					var pCategory = '/17763952';			
					var pmpSearchJsonDataObject = "";
					var attributeKey1 = "";
					var attributeKey2 = "";
					var attributeKey3 = "";
					var dfpSetTarget = new Object();					
					if(PageUrl.match("/catalog/")){
						function convertGlobalHeaderHTMLDFP(str){
							var decoded = JSON.parse($("<div/>").html(str).text());
							return decoded;
						}
						if(typeof External != 'undefined' && typeof External.dfp != 'undefined' && typeof External.dfp.adUnitPath != 'undefined'){
							var pmppath = External.dfp.adUnitPath;
							pCategory = pCategory+pmppath;
						}else if(typeof pmpSearchJsonData != 'undefined' && typeof pmpSearchJsonData.ProcessedData != 'undefined' && typeof pmpSearchJsonData.ProcessedData.adUnitPath != 'undefined'){
							var pmppath = pmpSearchJsonData.ProcessedData.adUnitPath.adUnit;
							pCategory = pCategory+pmppath;
						}
						if(typeof pmpSearchJsonData != 'undefined' && typeof pmpSearchJsonData.thirdParty != 'undefined' && typeof pmpSearchJsonData.thirdParty.dfp != 'undefined' && typeof pmpSearchJsonData.thirdParty.dfp.adParameterMap != 'undefined'){
							for(var i = 0; i < pmpSearchJsonData.thirdParty.dfp.adParameterMap.length; i++){				
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].act){
									dfpSetTarget.act = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].act);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].age){
									dfpSetTarget.age = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].age);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].arng){
									dfpSetTarget.arng = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].arng);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].bod){
									dfpSetTarget.bod = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].bod);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].brd){
									dfpSetTarget.brd = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].brd);
								}	
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].crng){
									dfpSetTarget.crng = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].crng);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].featr){
									dfpSetTarget.featr = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].featr);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].fit){
									dfpSetTarget.fit = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].fit);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].flav){
									dfpSetTarget.flav = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].flav);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].gen){
									dfpSetTarget.gen = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].gen);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].leg){
									dfpSetTarget.leg = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].leg);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].occ){
									dfpSetTarget.occ = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].occ);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pcat){
									dfpSetTarget.pcat = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pcat);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pgrp){
									dfpSetTarget.pgrp = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pgrp);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].psub){
									dfpSetTarget.psub = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].psub);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pthm){
									dfpSetTarget.pthm = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].pthm);
								}	
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].sil){
									dfpSetTarget.sil = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].sil);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].spl){
									dfpSetTarget.spl = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].spl);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].spt){
									dfpSetTarget.spt = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].spt);
								}
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].srng){
									dfpSetTarget.srng = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].srng);
								}	
								if(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].trnd){
									dfpSetTarget.trnd = convertGlobalHeaderHTMLDFP(pmpSearchJsonData.thirdParty.dfp.adParameterMap[i].trnd);
								}
							}							
						}
					}else if(PageUrl.match("/product/")){
						var pdpEnv = "";
						if($env != 'undefined'){
							
							if($env.category != 'undefined' &&pdpEnv != '' ){
								pdpEnv = $env.category;
								pCategory = pCategory+pdpEnv;
							}else if(typeof productV2JsonData != 'undefined' && typeof productV2JsonData.monetization != 'undefined' && typeof productV2JsonData.monetization.adUnit != 'undefined' && typeof productV2JsonData.monetization.adUnit.value != 'undefined'){
								pCategory = pCategory+productV2JsonData.monetization.adUnit.value;
							}else{
								pCategory = pCategory+"/ROS";
							}
				
							if($env.Fit != 'undefined' && $env.Fit != ''){
								dfpSetTarget.fit = $env.Fit;
							}
						}else{
							pCategory = pCategory+"/ROS";
						}
						
						
						if(typeof External != 'undefined'){
							if(typeof External.adParameterMap != 'undefined'){
								if(External.adParameterMap.act){
									dfpSetTarget.act = External.adParameterMap.act;
								}
								if(External.adParameterMap.age){
									dfpSetTarget.age = External.adParameterMap.age;
								}
								if(External.adParameterMap.arng){
									dfpSetTarget.arng = External.adParameterMap.arng;
								}
								if(External.adParameterMap.bod){
									dfpSetTarget.bod = External.adParameterMap.bod;
								}
								if(External.adParameterMap.brd){
									dfpSetTarget.brd = External.adParameterMap.brd;
								}
								if(External.adParameterMap.crng){
									dfpSetTarget.crng = External.adParameterMap.crng;
								}
								if(External.adParameterMap.featr){
									dfpSetTarget.featr = External.adParameterMap.featr;
								}
								if(External.adParameterMap.flav){
									dfpSetTarget.flav = External.adParameterMap.flav;
								}
								if(External.adParameterMap.gen){
									dfpSetTarget.gen = External.adParameterMap.gen;
								}
								if(External.adParameterMap.leg){
									dfpSetTarget.leg = External.adParameterMap.leg;
								}
								if(External.adParameterMap.occ){
									dfpSetTarget.occ = External.adParameterMap.occ;
								}
								if(External.adParameterMap.pcat){
									dfpSetTarget.pcat = External.adParameterMap.pcat;
								}
								if(External.adParameterMap.pgrp){
									dfpSetTarget.pgrp = External.adParameterMap.pgrp;
								}
								if(External.adParameterMap.psub){
									dfpSetTarget.psub = External.adParameterMap.psub;
								}
								if(External.adParameterMap.pthm){
									dfpSetTarget.pthm = External.adParameterMap.pthm;
								}
								if(External.adParameterMap.sil){
									dfpSetTarget.sil = External.adParameterMap.sil;
								}
								if(External.adParameterMap.spl){
									dfpSetTarget.spl = External.adParameterMap.spl;
								}
								if(External.adParameterMap.spt){
									dfpSetTarget.spt = External.adParameterMap.spt;
								}
								if(External.adParameterMap.trnd){
									dfpSetTarget.trnd = External.adParameterMap.trnd;
								}
							}
						}
					}else if(PageUrl.match("/search.jsp")){
					/*pCategory = pCategory+"/ROS";*/						
					if(typeof pmpSearchJsonData != 'undefined' && typeof pmpSearchJsonData.searchTerm != 'undefined' && pmpSearchJsonData.pageName == 'noSearchResultsPage'){
						pCategory = pCategory+"/zerosearch";
						var searchTermStr = pmpSearchJsonData.searchTerm,searchTerm = searchTermStr.replace("'","");
						dfpSetTarget.searchTerm = searchTermStr;
						}else{
							if(typeof pmpSearchJsonData != 'undefined' && typeof pmpSearchJsonData.searchTerm != 'undefined'){
								pCategory = pCategory+"/ROS";
								var searchTermStr = pmpSearchJsonData.searchTerm,searchTerm = searchTermStr.replace("'","");
								dfpSetTarget.searchTerm = searchTermStr;
								}
						}						
				}else{
						pCategory = pCategory+"/ROS";
					}
					var dfpslots = [];
					var inPageSlots = [];
					var outOfPageSlots = [];
				
				
								
					if(PageUrl.match("/catalog/") || PageUrl.match("/search.jsp")){
						Kjs.dfp_ads.slot("dfp_globalheader_marquee_ad", {
                            type: 'slot',adUnit: pCategory, sizes: [[1024, 45], 'fluid', [1024, 64], [1024, 128]],targeting: {"pos": "marquee"}
							});	
							
					}else{
						Kjs.dfp_ads.slot("dfp_globalheader_marquee_ad", {
                            type: 'slot',adUnit: pCategory, sizes: [[1024, 45], 'fluid', [1024, 64], [1024, 128]],targeting: {"pos": "marquee"}
							});
					}					
					
				if(window.test_and_target_bucketnames) {
					Kjs.dfp_ads.registerTargets({"tnt_buckets":window.test_and_target_bucketnames});
				}	
				Kjs.dfp_ads.registerTargets({"channel":"desktop"});
				Kjs.dfp_ads.registerTargets({"env":'prod'});				
				
					if(PageUrl.match("/catalog/")){
						Kjs.dfp_ads.registerTargets({"pgtype":"pmp"});
					}else if(PageUrl.match("/product/")){
						Kjs.dfp_ads.registerTargets({"pgtype":"pdp"});
					}else if(PageUrl.match("/search.jsp")){
					if(typeof pmpSearchJsonData != 'undefined' && typeof pmpSearchJsonData.pageName != 'undefined' && pmpSearchJsonData.pageName == 'noSearchResultsPage'){
						Kjs.dfp_ads.registerTargets({"pgtype":"zerosearch"});
						Kjs.dfp_ads.registerTargets({"search":dfpSetTarget.searchTerm});
					}
					else{
					Kjs.dfp_ads.registerTargets({"pgtype":"search"});
					Kjs.dfp_ads.registerTargets({"search":dfpSetTarget.searchTerm});
					}
					}
					if(dfpSetTarget.act) {
						Kjs.dfp_ads.registerTargets({"act":dfpSetTarget.act});
					}
					if(dfpSetTarget.age) {
						Kjs.dfp_ads.registerTargets({"age":dfpSetTarget.age});
					}
					if(dfpSetTarget.arng) {
						Kjs.dfp_ads.registerTargets({"arng":dfpSetTarget.arng});
					}	
					if(dfpSetTarget.bod) {
						Kjs.dfp_ads.registerTargets({"bod":dfpSetTarget.bod});
					}
					if(dfpSetTarget.brd) {
						Kjs.dfp_ads.registerTargets({"brd":dfpSetTarget.brd});
					}
					if(dfpSetTarget.crng) {
						Kjs.dfp_ads.registerTargets({"crng":dfpSetTarget.crng});
					}
					if(dfpSetTarget.featr) {
						Kjs.dfp_ads.registerTargets({"featr":dfpSetTarget.featr});
					}
					if(dfpSetTarget.fit) {
						Kjs.dfp_ads.registerTargets({"fit":dfpSetTarget.fit});
					}
					if(dfpSetTarget.flav) {
						Kjs.dfp_ads.registerTargets({"flav":dfpSetTarget.flav});
					}
					if(dfpSetTarget.gen) {
						Kjs.dfp_ads.registerTargets({"gen":dfpSetTarget.gen});
					}
					if(dfpSetTarget.leg) {
						Kjs.dfp_ads.registerTargets({"leg":dfpSetTarget.leg});
					}
					if(dfpSetTarget.occ) {
						Kjs.dfp_ads.registerTargets({"occ":dfpSetTarget.occ});
					}
					if(dfpSetTarget.pcat) {
						Kjs.dfp_ads.registerTargets({"pcat":dfpSetTarget.pcat});
					}
					if(dfpSetTarget.pgrp) {
						Kjs.dfp_ads.registerTargets({"pgrp":dfpSetTarget.pgrp});
					}
					if(dfpSetTarget.psub) {
						Kjs.dfp_ads.registerTargets({"psub":dfpSetTarget.psub});
					}
					if(dfpSetTarget.pthm) {
						Kjs.dfp_ads.registerTargets({"pthm":dfpSetTarget.pthm});
					}
					if(dfpSetTarget.sil) {
						Kjs.dfp_ads.registerTargets({"sil":dfpSetTarget.sil});
					}
					if(dfpSetTarget.spl) {
						Kjs.dfp_ads.registerTargets({"spl":dfpSetTarget.spl});
					}
					if(dfpSetTarget.spt) {
						Kjs.dfp_ads.registerTargets({"spt":dfpSetTarget.spt});
					}
					if(dfpSetTarget.srng) {
						Kjs.dfp_ads.registerTargets({"srng":dfpSetTarget.srng});
					}
					if(dfpSetTarget.trnd) {
						Kjs.dfp_ads.registerTargets({"trnd":dfpSetTarget.trnd});
					}
					
				});
			</script>
			<!-- END :: DFP Marquee Ad Library :: dfp_globalheader_marquee_ad --><!-- Start :: Global Header content --> 
	<div id="header" class="clearfix top-global-header">
	<script>var trJsonData = trJsonData || "";</script>
	<script type="text/javascript">	
		(function(){
		$env = this.$env||{};
		$env.enable_typeahead_search = 'true';
		$env.enable_typeahead_search_product_suggestions = 'true';
		$env.enableTypeaheadCategorySearch = 'true';
		$env.solrTypeAheadURL = '/';
		$env.solrTypeAheadEnabled = true;
		}).call(this);
	</script>
		<!-- START :: Banners -->
		<!-- START :: Banners - 1 -->
			<!-- END :: Banners - 1 --><!-- START :: Banners - 2 --><!-- Start: Calling Element GetAssetsByDate --><!-- End: Calling Element GetAssetsByDate --><!-- gh-b2-20210519-get.html DEV: TF 05/11/2021 -->

<style>
	.top-global-header .new-dc-equity-banner a span {
		text-decoration: underline;
		text-transform: none;
		font-size: 10px;
	}
	a .ghr-ntfTopPanel_price span {
		font-size: 14px !important;
		text-decoration: none !important;
	}
	/* To make the Details of notification link small */
	a.ghr-ntfTopPanel_detailslnk {
		font-size: 10px !important;
	}
	/* Padding & Size Adjustments for long text */
	.top-global-header .new-dc-equity-banner {
		padding: 13px 0 !important;
	}
	.top-global-header .new-dc-equity-banner a {
		font-size: 12px !important;
		padding: 15px 3px 10px 3px !important;
	}
</style>

<div class="clearfix" id="new-equity-banner">
	<div class="new-dc-equity-banner">
		<a href="http://www.kohls.com/ecom/shipping/75_ShippingUPDATED_nodates_bopus.html" target="_blank"
			class="dcp-banner-left"><b>Free shipping</b> with $75 purchase. <span>details</span></a>
		<a href="/catalog.jsp?CN=InStoreOnline:Pick%20Up%20in%20Store&BL=y&icid=sb-bopus" target="_blank"
			class="dcp-banner-center notification-hide-banner"><b>Fast &plus; free</b> store pickup!
			<span>details</span></a>
		<a href="https://www.kohls.com/ecom/Kcash/EarnKohlsCash_20210519_20210523.html" target="_blank"
			class="dcp-banner-right notification-hide-banner"><b>Get</b> $10 Kohl&rsquo;s Cash&reg; for every $50 spent.
			<span>details</span></a>
		<div id="notificationbanner"></div>
	</div>
</div>

			<!-- END :: Banners - 2 --><!-- START :: Banners - 3 -->
			<!-- END :: Banners - 3 --><!-- START :: Banners - 4 -->
			<!-- END :: Banners - 4 -->
		
		<!-- END :: Banners -->
		<!-- persistent Bar Content Starts Here -->
		<div id="persistent_bar_container">
		<input type="hidden" name="isAnonymousUser" id="isAnonymousUser" value="">
		<div class="tr_phase2_headercontainer">
		<div id="header-container">	
		<!-- LOGO Content Starts Here -->
			<!-- WCS location: GlobalHeader > gh-logo-svg --> <!-- FILE NAME: gh-logo-svg.html --> <!-- LAST EDITED BY: MS - 03.11.20 --> <span id="logo"> <div id="kohls_logo"> <a href="/" title="Kohls Logo"> <svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 122 20" height="22" width="147"><g transform="translate(1 1)"><path d="M0 .441h8.193v5.483h.063L14.527.441h9.202l-7.563 6.712 8.729 9.234H14.527l-6.27-6.524h-.064v6.524H0V.44zM36.775 0c9.328 0 11.849 1.922 11.849 8.445 0 6.523-2.521 8.446-11.849 8.446-9.328.031-11.88-1.891-11.88-8.446C24.895 1.922 27.447 0 36.775 0zm0 13.613c3.656 0 3.404-4.002 3.404-5.325 0-1.324.031-4.98-3.404-4.98s-3.403 3.656-3.403 4.98c-.032 1.323-.252 5.325 3.403 5.325zM66.303 10.4h-6.05v6.082h-8.415V.536h8.414v5.483h6.05V.536h8.414V16.48h-8.413V10.4zM78.718.536h8.635v12.132s6.87.032 7.784.032c.913 0 1.45-.82 1.45-.82v4.57H78.717V.536zm37.973 5.294c-1.796-1.418-3.309-1.86-4.947-1.89-1.607-.032-3.089.535-3.089 1.48 0 .756 1.103 1.072 4.948 1.86 3.151.661 5.263 2.237 5.263 5.167 0 3.31-5.641 5.2-10.652 5.2-3.53 0-6.24-.63-8.666-1.702v-4.474c1.513 1.544 4.853 2.363 7.437 2.363 1.418 0 2.143-.22 2.143-1.323 0-.694-.882-1.324-3.592-1.797-5.105-.914-6.839-2.363-6.839-4.821 0-3.53 4.475-5.61 9.958-5.61 2.963 0 5.673.095 8.068 1.198V5.83h-.032zM91.796 7.059l.599-1.009c-1.765-.85-2.3-3.056-1.387-4.317.977-1.355 3.656-1.922 5.137.158 1.229 1.764.063 3.277-.473 3.718-.598.41-3.876 1.45-3.876 1.45zm27.637-5.01c0 .598-.473 1.04-1.04 1.04-.599 0-1.04-.473-1.04-1.04 0-.6.473-1.04 1.04-1.04.567 0 1.04.44 1.04 1.04zm-1.954 0c0 .504.378.913.882.913.505 0 .883-.41.883-.914s-.378-.914-.883-.914c-.504.032-.882.41-.882.914zm.662.63h-.158v-1.23h.473c.283 0 .41.127.41.347 0 .22-.158.315-.316.347l.379.536h-.19l-.346-.536h-.252v.536zm.189-.694c.189 0 .378 0 .378-.22 0-.158-.126-.22-.284-.22h-.283v.409h.189v.031z" vector-effect="non-scaling-stroke" fill="#444" fill-rule="evenodd"/></g></svg> </a> </div> </span>
<!-- PDP fix --> <style>.products .products_grid .product-description .prod_ratingBlock{padding-top:8px !important}body .external{max-width:1440px;min-width:1024px;}</style>

		<!-- LOGO Content Ends Here -->
		<!-- Form Content Starts Here -->
		<form action="/search.jsp" method="get" id="site-search">
			<fieldset>
				<label class="visually-hidden" for="search">Search by
					Keyword or Web ID </label> <span class="blank_tt">&nbsp;</span>
				<input type="submit" value="" class="button-submit button-search" name="submit-search">
				<input type="text" Placeholder="Search" class="input-text" id="search" name="search" autocomplete="off">
			</fieldset>
		</form>	
		<!-- Form Content Ends Here -->
		
			
		<!-- WCS location: GlobalHeader > gh-accountlinks-LoggedOut > Title -->
<!-- gh-accountlinks-notLoggedIn-Title-HTML.html -->
<!-- LAST EDITED BY: Amber G - 9.11.19 -->

<div id="utility-nav" class="not-signed-in">
	<ul class="utility-nav-group">
		<div class="utility-greeting"> 
			<a href="javascript:void(0);" class="utility-item-link account utility-nav-wallet-svg" title="Account"> 
				<svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 34 34" height="32" width="32"><g transform="translate(1 1)"><path d="M24.278 27.291C22.836 24.128 19.628 22 16 22c-3.629 0-6.836 2.128-8.278 5.291A13.937 13.937 0 0 0 16 30c3.097 0 5.96-1.006 8.278-2.709zm1.58-1.35A13.957 13.957 0 0 0 30 16c0-7.732-6.268-14-14-14S2 8.268 2 16c0 3.887 1.584 7.404 4.142 9.94A11.11 11.11 0 0 1 16 20c4.29 0 8.01 2.411 9.858 5.94zM16 32C7.163 32 0 24.837 0 16S7.163 0 16 0s16 7.163 16 16-7.163 16-16 16zm7-20c0 3.865-3.135 7-7 7s-7-3.135-7-7 3.135-7 7-7 7 3.135 7 7zm-2 0c0-2.76-2.24-5-5-5s-5 2.24-5 5 2.24 5 5 5 5-2.24 5-5z" vector-effect="non-scaling-stroke" fill-rule="nonzero" fill="#000"/></g></svg> </a> 
		</div> 
		<a class="utility-item-link account utility-nav-wallet-svg"> 
			<div class="greeting-container nvSensitive"> 
				<span class="greeting"></span> 
				<span class="first-name">Account</span> 
			</div> 
		</a> 
		<li class="utility-item" style="display: list-item;">
			<input type="hidden" name="isRedesignSingleLoginPage" id="isRedesignSingleLoginPage" value="true"> 
			<div class="my-account-overlay" style="display:none;" id='dropDownHook'></div>
		</li>
	</ul>
</div>

		
		<!-- Account Button Content Ends Here -->
		<!-- Mini Shopping Bag Content Starts Here -->
		
			
		<!-- Different Content --> 
<div class="skava-mini-cart"> 
<div id="mini-cart"> 
<h2 title="Proceed to shopping bag and checkout." class="mini-cart-header"> 
<a id="tr_phase2_ShoppingBg" href="/checkout/shopping_cart.jsp" title="Mini Shopping Cart"> 
<span class="shopping-bag">
<span class="shopping-bag-label kas-newpb-shopping-bag-label"></span>
</span>
<span class="number-items">0</span>
<span class="visually-hidden"> item(s), </span> 
<span class="subtotal">$0.00</span> 
</a> 
</h2> 
</div> 
<div id="notificationpopover"></div> 
</div>
		<!-- End :: CheckoutBag -->
		<!-- START :: Checkout -->
		
			
		<!-- WCS location: GlobalHeader > gh-checkout -->
<!-- FILE NAME: gh-checkout-svg.html -->
<!-- LAST EDITED BY: Amber G - 09.11.19 -->
	
<div id="checkout-container"> 
<a> 
<div>
    <svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 34 34" height="32" width="32"><g transform="translate(1 1)"><path d="M21.232 17H9a1 1 0 0 1 0-2h11.839l-3.046-3.046a1 1 0 0 1 1.414-1.414l4.243 4.242a2 2 0 0 1 0 2.829l-4.243 4.243a1 1 0 0 1-1.414-1.415L21.233 17zM16 2C8.268 2 2 8.268 2 16s6.268 14 14 14 14-6.268 14-14S23.732 2 16 2zm0-2c8.837 0 16 7.163 16 16s-7.163 16-16 16S0 24.837 0 16 7.163 0 16 0z" vector-effect="non-scaling-stroke" fill-rule="nonzero" fill="#000"/></g></svg>
</div>
</a> 
<a class="checkout-link" href="javascript:void(0);"> 
<span>check out</span> 
</a> 
</div>
		<!-- End :: Checkout -->	
		<!-- Start :: Checkout Bag dummy info -->
		
		<!-- End :: Checkout Bag dummy info -->
		</div>
		</div>
		<div class="tr_phase2_headerPanel"></div>
		<!-- END :: Persistance Bar -->
		</div>
		<!-- START :: Middle Menu Bar -->
		<div id="top-nav" class="middle-menu-bar">
		<div id="navigation-bar" class="top-nav-inside-container">
		<ul id="top-nav-left" aria-expanded="false">
		<li class="first">
		<!-- START :: HamburgerMenu -->
		
			
		<!-- WCS location: GlobalHeader > gh-top-navigation -->
<!-- FILE NAME: gh-shopbydepartment-svg.html -->
<!-- LAST EDITED BY: Amber G - 9.11.19 -->
	
<a href="javascript:void(0);" class="kohls-charge-link menu-icons-bar">
    <svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 26 16" height="12" width="22" style="margin-top:.15em;"><g transform="translate(1 1)"><path d="M1 0h22a1 1 0 0 1 0 2H1a1 1 0 0 1 0-2zm0 6h22a1 1 0 0 1 0 2H1a1 1 0 0 1 0-2zm0 6h22a1 1 0 0 1 0 2H1a1 1 0 0 1 0-2z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"/></g></svg>
<p style="margin: -1.9em 2em;width: 12em;"><span class="middle-menu-title">Shop by Department</span></p></a>

		<!-- End :: HamburgerMenu -->
		</li>
		</ul>
		<!-- START :: MyStoreSignIn -->
		
		
		<!-- End :: MyStoreSignIn -->
		
		<ul id="store">
		<!-- START :: KLSGlobalMyStoreAnonymous -->
		
			
		<!-- WCS location: GlobalHeader > gh-mystore-anonymous -->
<!-- gh-myStoreLocateanonymous-svg.html -->
<!-- LAST EDITED BY: Amber G - 09.11.19 -->

<ul id="store"> 
<li class="star-icon">
    <svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 34 32" height="16" width="17"><g transform="translate(1 1)"><path d="M16.499 25.937l8.196 3.949a1.15 1.15 0 0 0 1.635-1.199l-1.291-8.8a1.145 1.145 0 0 1 .32-.973l6.308-6.36a1.146 1.146 0 0 0-.623-1.937L22.088 9.1a1.148 1.148 0 0 1-.819-.588L17.011.604a1.147 1.147 0 0 0-2.022 0L10.73 8.51c-.168.312-.47.529-.82.588L.957 10.617a1.145 1.145 0 0 0-.624 1.937l6.309 6.36c.254.256.372.617.32.973l-1.291 8.8a1.149 1.149 0 0 0 1.635 1.199l8.196-3.95a1.15 1.15 0 0 1 .998 0z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"/></g></svg>
</li> 
<li class="setmystorenavlink choose-store">
<a href="javascript:void(0)" class="setmystore">My Store: Select Store</a>
</li> 
</ul>
		<!-- End :: KLSGlobalMyStoreAnonymous -->
		</ul>
		
		<ul id="top-nav-right">
		<!-- START :: TodaysDeals,Store,Help -->
		
			
		<!-- WCS location: GlobalHeader > gh-toprightlinks -->
<!-- gh-KohlsCoupons-svg.html -->
<!-- LAST EDITED BY: Amber G - 9.11.19 -->

<li class="first">
<a class="today-deals-icon" href="/sale-event/coupons-deals.jsp">
<svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 32 32" height="16" width="16"><g transform="translate(1 1)"><path d="M16 30c7.732 0 14-6.268 14-14S23.732 2 16 2 2 8.268 2 16s6.268 14 14 14zm0 2C7.163 32 0 24.837 0 16S7.163 0 16 0s16 7.163 16 16-7.163 16-16 16zm-1.25-9.29a8.041 8.041 0 0 1-3.303-1.544 1.134 1.134 0 0 1-.447-.912c0-.62.484-1.106 1.08-1.106.317 0 .521.097.67.214 1.08.892 2.235 1.396 3.65 1.396 1.416 0 2.31-.698 2.31-1.707v-.039c0-.97-.522-1.493-2.943-2.075-2.774-.699-4.339-1.553-4.339-4.055v-.039c0-1.973 1.338-3.43 3.322-3.83V8.25a1.25 1.25 0 0 1 2.5 0v.77a6.815 6.815 0 0 1 2.763 1.146c.26.155.503.485.503.95a1.087 1.087 0 0 1-1.676.932c-1.006-.68-1.974-1.029-2.998-1.029-1.341 0-2.123.718-2.123 1.61v.04c0 1.047.596 1.513 3.11 2.133C19.585 15.501 21 16.53 21 18.78v.038c0 2.251-1.504 3.695-3.75 3.996v.714a1.25 1.25 0 0 1-2.5 0v-.817z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"/></g></svg>
<span class="middle-menu-title">Kohl&rsquo;s Coupons</span>
</a>
</li>
<li class="first">
<a class="order-status-icon order-status hide" href="/upgrade/myaccount/order_status_login.jsp" title="Order Status">
<img alt="Order Status" src="/media/images/global-header-refresh-icons/order-status-icon.png">
<span class="middle-menu-title">Order Status</span>
</a>
<a class="order-status-icon purchase-history hide" href="/myaccount/v2/purchase-history.jsp" title="Order Status">
<img alt="history" src="/media/images/global-header-refresh-icons/order-status-icon.png">
<span class="middle-menu-title">Order Status</span>
</a>	
</li>
<li>
<a class="help-icon" href="https://cs.kohls.com/">
<svg role="img" focusable="false" overflow="visible" preserveAspectRatio="none" viewBox="0 0 32 32" height="16" width="16"><g transform="translate(1 1)"><path d="M16 30c7.732 0 14-6.268 14-14S23.732 2 16 2 2 8.268 2 16s6.268 14 14 14zm0 2C7.163 32 0 24.837 0 16S7.163 0 16 0s16 7.163 16 16-7.163 16-16 16zm-1.887-8.603c0-.791.634-1.4 1.5-1.4s1.5.609 1.5 1.4v.202c0 .79-.634 1.399-1.5 1.399s-1.5-.608-1.5-1.399v-.202zm1.637-3.414c-.622 0-1.2-.374-1.25-.983v-2.386c-.07-.715.461-1.228 1.1-1.285 2.22-.16 3.478-1.066 3.536-2.726l.002-.053c.051-1.476-.925-2.539-2.62-2.598-1.248-.044-2.278.422-3.252 1.337-.243.203-.555.35-.884.339-.73-.026-1.295-.705-1.268-1.495.014-.396.17-.813.51-1.117 1.294-1.274 2.902-2.089 5.068-2.013 3.295.115 5.46 2.248 5.345 5.543l-.002.052c-.116 3.32-2.405 4.748-5.035 5.158V19c-.114.576-.628.983-1.25.983z" vector-effect="non-scaling-stroke" fill="#000" fill-rule="nonzero"/></g></svg>
<span class="middle-menu-title" style="">Help</span>
</a>
</li>
		<!-- End :: TodaysDeals,Store,Help -->
		</ul>

		
		</div>
		<div id="container_setmystore"></div>
		</div>
		<!-- End :: Middle Menu Bar -->
		<!-- Start :: Start Navigation--> <div class="dropdown-navigation-container"><ul id="navigation" class="dropdown-navigation"></ul></div><script type="text/javascript">
						$init(function _$init_navigation() {
							if (!$env.enableDeferHamburger) {
								Kjs.loadContent('/feature/ESI/Common/NewNavigation/', {
									sel: '#navigation',
									cache: true,
									independent: true
								});
							}	
						});
					</script><!-- End :: Start Navigation-->
			<noscript><div class="error-noscript-su"> <div
			class="leftcorner"></div> <div
			class="error-icon-su"></div> <div class="message">
			<p> We're sorry, JavaScript is required to shop Kohls.com.
			</p> <br> <p class="message_description"> Please
			enable JavaScript in your browser; it's quick and easy! <a
			href="/kohls_javascript.jsp"> Get instructions now. </a>
			</p> </div> <div class="rightcorner"></div>
			<div class="clear"></div> </div></noscript>

		</div>
		<!-- START :: DFP Marquee Ad  :: dfp_globalheader_marquee_ad --><style>
.dfp_globalheader_marquee_ad {
		width: 1024px;
		margin-right: auto;
		margin-left: auto; 
		position: relative; 
		margin-top: -15px;
		padding-bottom: 15px;
		padding-top: 10px;
	}
</style>
<div class="marquee-ad-cnt">
<div id="dfp_globalheader_marquee_ad" class="dfp_globalheader_marquee_ad"></div>
</div><!-- END :: DFP Marquee Ad  :: dfp_globalheader_marquee_ad -->
		<!-- Added for RR -->
		
		<script type="text/javascript">
			function getCookieByNames(D){var C="";if(document.cookie.length>0){var A=document.cookie.indexOf(D+"=");if(A!==-1){A=A+D.length+1;var B=document.cookie.indexOf(";",A);if(B===-1){B=document.cookie.length}C=document.cookie.substring(A,B);try{C=decodeURI(C)}catch(B){}}}return C}
			var prop50_Value = "";
			if(prop50_Value=="null" || prop50_Value==""){
				prop50_Value="";
			}
		</script>
		<!-- Added for RR -->
		
	
	<!-- End: Main Header-->

		</div>
		




<div id="content" rel="share" class="close_miniovwerlay clearfix" style="position:relative;">
	<div class="ajax-loading1" style="display: none;"></div>
    <div class="ajax-loading1_loader" style="display: none;"></div>
	<div id="frame" class="frame cart_content_area_marker bopus_cart_checkout">
		<div id="error_display">
			<p>
				<img class="errorImg" alt="Error" src="/media/images/error-icon.png">
				Some information is missing or invalid below
			</p>
		</div>
		<div id="shopping_Bag_Lpanel">
			<ul id="checkout-assistance"><li><a href="javascript:launchCorporate('https://cs.kohls.com/')">Need Assistance?</a></li><li><a href="javascript:launchCorporate('https://www.kohls.com/feature/privacy-policy.jsp')">Security &amp; Privacy Policy</a></li><li><a href="javascript:launchCorporate('https://cs.kohls.com/app/answers/help_topic/c/22,4')">Returns</a></li><li><a href="javascript:launchCorporate('https://cs.kohls.com/app/answers/help_topic/c/22,3')">Shipping</a></li></ul>
			<div id="elite-cart"></div>
			<h1 id="shopping-bag-heading">

			    Shopping Cart
				  <span class="boss_shopping_cart_quantity">(1)</span>

			</h1>
			<div class="clear"></div>
			<div id="shopping_cart_page_holder">
			</div>
			<div id="saveforlater_container" class="saveforlater_container"></div>
		</div>
		<div id="shopping_Bag_Rpanel">
				<!-- Order summary section starts -->
				<div class="order_summary_wrapper">
					<div class="orderSummary_loyalty"></div>
					<div class="orderSummary_holder"></div>
				</div>
				<span class="spBagTitle">
					You may also like
				</span>
				<span class="ckoBagTitle">
					You may also like
				</span>
				<span class="ocBagTitle">
					You may also like
				</span>
		</div>
	</div>
</div>

			<script type="text/javascript">
			var brightTagScriptCallParam  =  "4DPyaxM";
			var brightTagScriptCallURL  =  "//s.btstatic.com/tag.js"+ "#site="+brightTagScriptCallParam+"&referrer=" + encodeURIComponent(document.location.href);

			// Load the Bright Tag script under KJS shielding
			$requires(brightTagScriptCallURL, {
				text: '{ site:"4DPyaxM" }',
				isLast: true
			});

			</script>

	

		<div id="footer-region">
			
    
        
<div class="lowernav clearfix">
			<script type="text/javascript">
				$init(function _$init_loggeduser($) {			   
					var customerNameCookie = getCookie( 'VisitorUsaFullName' );
					if(customerNameCookie == '' || customerNameCookie == null ){			
						$('div#notLoggedInUser').show();
						$('div#LoggedInUser').hide();
					}
					else{
						$('div#LoggedInUser').show();
						$('div#notLoggedInUser').hide();
					}
				});		    		    
			</script><div id="notLoggedInUser" style='display:none'></div><div id="LoggedInUser" style="display:none"></div><div class="lowernav_col_three">
				<div class="socialstuff"></div>
			</div>
		</div><!-- Footer: Legal Slot -->
<!-- FILE NAME: footer-desktop-2021.html -->
<!-- LAST EDITED BY: MS - 01.04.2021 -->
<style>
	/*! * Footer Renormalize.css -- by Kohl's Digital Creative * * @copyright 2017 Kohl's Corporation * @author David N * @version 3.0.2 * @date 2017-07-27 14:00:58 GMT-0500 (CDT) */
	.footer-slot cite,
	.footer-slot em,
	.footer-slot i {
		font-style: italic;
	}

	#container~div[id^="templates"],
	#container~iframe,
	#container~img,
	#footer~div[id^="templates"],
	#footer~iframe,
	#footer~img,
	#mobile-footer~iframe {
		opacity: 0;
		width: 1px;
		height: 1px;
		position: absolute;
		bottom: -1px;
	}

	iframe.#clickmapAdobeAnalyticsiframe {
		opacity: 1;
	}

	#footer,
	#mobile-footer {
		clear: both;
		width: 100%;
		margin: 2em 0 0;
	}

	.search-visible #mobile-footer,
	.stickyHeader #mobile-footer {
		max-height: none;
	}

	#footer .lowernav {
		display: none;
	}

	.footer-slot,
	.footer-slot *,
	.footer-slot :after,
	.footer-slot :before {
		box-sizing: border-box;
	}

	.footer-slot {
		font-family: "Gotham 4r", GothamBook, Helvetica, Arial, sans-serif;
		font-size: calc(16px);
		font-weight: 400;
		color: #000;
		line-height: 1.5;
		-webkit-text-size-adjust: 100%;
		-ms-text-size-adjust: 100%;
	}

	.footer-slot b,
	.footer-slot h1,
	.footer-slot h2,
	.footer-slot h3,
	.footer-slot h4,
	.footer-slot h5,
	.footer-slot h6,
	.footer-slot strong {
		font-family: "Gotham 7r", GothamBold, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot a {
		background-color: transparent;
		color: #000;
		text-decoration: underline;
	}

	.footer-links a:focus,
	.footer-links a:hover {
		color: #666;
		text-decoration: underline;
	}

	.footer-slot a:focus,
	[role="button"]:focus {
		outline: dotted 1px;
		outline: -webkit-focus-ring-color auto 5px;
	}

	.footer-slot a:active,
	.footer-slot a:focus:hover,
	[role="button"]:active,
	[role="button"]:focus:hover {
		outline: 0;
	}

	.footer-slot h1,
	.footer-slot h2,
	.footer-slot h3,
	.footer-slot h4,
	.footer-slot h5,
	.footer-slot h6 {
		display: block;
		color: #000;
	}

	.footer-slot h1 {
		margin: 0.66667em 0;
		font-size: 2.25em;
	}

	.footer-slot h2 {
		margin: 0.85714em 0;
		font-size: 1.75em;
	}

	.footer-slot h3 {
		margin: 0.91667em 0;
		font-size: 1.5em;
	}

	.footer-slot h4 {
		margin: 1.2em 0;
		font-size: 1.25em;
	}

	.footer-slot h5 {
		margin: 1.44444em 0;
		font-size: 1.125em;
	}

	.footer-slot h6 {
		margin: 2em 0;
		font-size: 1em;
	}

	.footer-slot p {
		margin: 1em 0;
	}

	.footer-slot abbr {
		cursor: help;
		text-decoration: underline;
	}

	@supports ((-webkit-text-decoration-style: dotted) or (text-decoration-style: dotted)) {
		.footer-slot abbr {
			text-decoration: underline dotted;
		}
	}

	.footer-slot code {
		font-family: "Courier New", Courier, monospace;
	}

	.footer-slot q:before {
		content: "\201c";
	}

	.footer-slot q:after {
		content: "\201d";
	}

	.footer-slot sub,
	.footer-slot sup {
		font-size: 75%;
		line-height: 0;
		position: relative;
		vertical-align: baseline;
	}

	.footer-slot sub {
		bottom: -0.25em;
	}

	.footer-slot sup {
		top: -0.5em;
	}

	.footer-slot small {
		font-size: smaller;
	}

	.footer-slot .gotham-book {
		font-family: "Gotham 4r", GothamBook, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot .gotham-medium {
		font-family: "Gotham 5r", GothamMedium, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot .gotham-medium-ss {
		font-family: "Gotham SSm 5r", "Gotham 5r", GothamMedium, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot .gotham-bold {
		font-family: "Gotham 7r", GothamBold, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot .gotham-black {
		font-family: "Gotham 8r", GothamBlack, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot ol,
	.footer-slot ul {
		display: block;
		margin: 1em 0;
		padding-left: 2.5em;
	}

	.footer-slot ul {
		list-style-type: disc;
	}

	.footer-slot ol ol,
	.footer-slot ul ul {
		margin: 0;
	}

	.footer-slot ol li,
	.footer-slot ul li {
		list-style-type: inherit;
	}

	.footer-slot ol {
		list-style-type: decimal;
	}

	#footer li {
		font-size: inherit;
		line-height: normal;
	}

	.footer-slot li {
		display: list-item;
	}

	.footer-slot .list--unstyled {
		margin: 0;
		padding-left: 0;
		list-style-type: none;
	}

	.footer-slot dt {
		font-family: "Gotham 7r", GothamBold, Helvetica, Arial, sans-serif;
		font-weight: 400;
	}

	.footer-slot dd {
		margin-left: 2.5em;
	}

	.footer-slot img {
		float: none;
	}

	.footer-slot figure {
		margin: 0;
	}

	.footer-slot .c-svg-responsive {
		position: relative;
	}

	.footer-slot .c-svg-responsive svg {
		position: absolute;
		top: 0;
		left: 0;
	}

	.footer-slot .c-svg-responsive svg.c-svg-align--center {
		margin-right: auto;
		margin-left: auto;
		right: 0;
	}

	.footer-slot input:focus {
		outline: dotted 1px;
		outline: -webkit-focus-ring-color auto 5px;
	}

	.footer-slot input:focus:hover {
		outline: 0;
	}

	.footer-slot button {
		margin: 0;
		border: 0;
		padding: 0;
		background-color: transparent;
	}

	.footer-slot button:focus {
		outline: dotted 1px;
		outline: -webkit-focus-ring-color auto 5px;
	}

	.footer-slot button:focus:hover {
		outline: 0;
	}

	.footer-slot .btn--faux-native,
	.footer-slot button {
		float: none;
		color: #000;
		font-family: inherit;
		font-size: inherit;
		font-weight: 400;
		line-height: normal;
	}

	.footer-slot .btn--faux-native {
		min-height: 15px;
		margin: 2px;
		border: 2px outset #f0f0f0;
		padding: 3px 6px;
		background: #f0f0f0;
	}

	.footer-slot .btn--faux-native:active {
		border-style: inset;
	}

	.footer-slot .visually-hidden {
		clip: rect(0, 0, 0, 0);
		overflow: hidden;
		border: none;
		width: 1px;
		height: 1px;
		margin: -1px;
		padding: 0;
		position: absolute;
	}

	.footer-slot .visually-hidden.is-focusable:active,
	.footer-slot .visually-hidden.is-focusable:focus {
		overflow: visible;
		width: auto;
		height: auto;
		margin: 0;
		position: static;
		clip: auto;
	}

	.footer-slot .invisible,
	.footer-slot .is-invisible {
		visibility: hidden;
	}

	.footer-slot .hidden,
	.footer-slot .is-hidden {
		display: none;
	}

	.creative-slot .sbr {
		font-size: 1px;
	}

	.creative-slot .sbr-after:after,
	.creative-slot .sbr:after {
		content: "\a";
		white-space: pre;
	}

	/*! * Global Footer * * @copyright 2017 Kohl's Corporation * @author David N * @version 3.0.9 * @date 2017-09-21 15:16:18 GMT-0500 (CDT) */
	.footer-slot .footer-back-to-top__button,
	.footer-slot .footer-desktop__link,
	.footer-slot .footer-top__header {
		font-family: "Gotham 5r", GothamMedium, Helvetica, Arial, sans-serif;
		text-transform: uppercase;
	}

	.footer-global {
		margin-bottom: 0 !important;
		background-color: #f0f0f0;
		text-align: left;
		padding-bottom: 23em;
	}

	.footer-slot {
		width: 100%;
	}

	@media (min-width: 64em) {
		.footer-global .footer-slot {
			width: 64em;
			margin-right: auto;
			margin-left: auto;
		}
	}

	.footer-slot .footer-back-to-top {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
	}

	.footer-slot .footer-back-to-top__button,
	.footer-top__social {
		display: -webkit-flex;
		display: -ms-flexbox;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
	}

	.footer-slot .footer-back-to-top__button {
		display: flex;
		-webkit-flex-direction: column;
		flex-direction: column;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-grid-row-align: center;
		align-items: center;
		padding-top: 2em;
		padding-bottom: 1em;
		position: relative;
		font-size: 0.75em;
		text-decoration: none;
	}

	.footer-slot .footer-back-to-top__button:focus,
	.footer-slot .footer-back-to-top__button:hover {
		color: #000;
		text-decoration: none;
	}

	.footer-slot .footer-back-to-top__button::before {
		content: "";
		width: 1.1667em;
		height: 1.1667em;
		margin-left: -0.5em;
		border: 2px solid transparent;
		border-top-color: #000;
		border-right-color: #000;
		position: absolute;
		top: 1em;
		left: 50%;
		-webkit-transition: -webkit-transform 250ms ease-in-out;
		transition: -webkit-transform 250ms ease-in-out;
		transition: transform 250ms ease-in-out;
		transition: transform 250ms ease-in-out, -webkit-transform 250ms ease-in-out;
		-webkit-transform: rotate(-45deg);
		transform: rotate(-45deg);
	}

	.footer-slot .footer-back-to-top__button:focus::before,
	.footer-slot .footer-back-to-top__button:hover::before {
		-webkit-transform: rotate(-45deg) translate(0.1667em, -0.1667em);
		transform: rotate(-45deg) translate(0.1667em, -0.1667em);
	}

	.footer-top {
		display: none;
	}

	@media (min-width: 48em) {
		.footer-slot .footer-top {
			display: -webkit-flex;
			display: -ms-flexbox;
			display: flex;
			width: 100%;
			height: 7em !important;
			padding-top: 0.75em;
			padding-bottom: 0.75em;
			border-bottom: 1px solid #ccc;
		}

		.footer-top__social {
			padding-right: 0.9375em;
			padding-left: 0.9375em;
		}
	}

	.footer-slot .footer-top__header {
		margin-top: 0;
		margin-bottom: 0.625em;
		font-size: 0.875em;
	}

	.footer-top__social {
		display: flex;
		-webkit-flex-direction: column;
		flex-direction: column;
		width: 56.25%;
		padding-right: 0.9375em;
		padding-left: 0.9375em;
		border-right: 1px solid #ccc;
	}

	@media (min-width: 64em) {
		.footer-slot .footer-top__header {
			margin-bottom: 0;
			line-height: 1.2;
		}

		.footer-top__social {
			-webkit-box-orient: horizontal;
			-webkit-box-direction: normal;
			-webkit-flex-direction: row;
			flex-direction: row;
			-webkit-box-pack: start;
			-webkit-justify-content: flex-start;
			justify-content: flex-start;
			-webkit-box-align: center;
			-webkit-align-items: center;
			-ms-grid-row-align: center;
			align-items: center;
			-webkit-flex-shrink: 0;
			flex-shrink: 0;
			width: 48.05%;
			padding-right: 1.25em;
			padding-left: 1.25em;
		}

		.footer-slot .footer-top__header--social {
			width: 5.7143em;
			margin-right: 1.4286em;
		}
	}

	.footer-slot .footer-top__social-list {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-justify-content: space-between;
		-ms-flex-pack: justify;
		justify-content: space-between;
		width: 98%;
	}

	@media (min-width: 64em) {
		.footer-slot .footer-top__social-list {
			width: 21em;
		}
	}

	.footer-top__social-list__link {
		display: block;
		width: 3em;
		height: 3em;
		font-size: 1rem;
	}

	.footer-top__social-list__network-icon {
		transition: -webkit-transform 150ms ease-in-out;
		transition: transform 150ms ease-in-out;
	}

	.footer-top__social-list__link:focus .footer-top__social-list__network-icon,
	.footer-top__social-list__link:hover .footer-top__social-list__network-icon {
		-webkit-transform: scale(0.8);
		transform: scale(0.8);
	}

	.footer-slot .footer-top__native-app {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		flex-direction: column;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		width: 43.75%;
		flex-wrap: wrap;
	}

	@media (min-width: 64em) {
		.footer-slot .footer-top__native-app {
			-webkit-flex-shrink: 0;
			flex-shrink: 0;
			-webkit-box-orient: horizontal;
			-webkit-box-direction: normal;
			-webkit-flex-direction: row;
			flex-direction: row;
			-webkit-box-pack: space-around;
			-webkit-justify-content: space-around;
			justify-content: space-around;
			width: 50%;
		}

		.footer-slot .footer-top__header--native-app {
			width: 7em;
			padding-left:1.5em;
		}
	}

	.footer-slot .footer-top__native-app__badges {
		display: -webkit-box;
		display: -webkit-flex;
		display: flex;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		justify-content: center;
		-webkit-box-pack: start;
		-webkit-align-items: start;
		align-items:flex-start;
		flex-direction:column;
		width:41%;
	}

	.footer-slot .footer-top__native-app__image,
	.footer-slot .footer-top__native-app__link {
		display: block;
		width: 95%;
		height: 2.5em;
	}

	.footer-slot .footer-links {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		flex-direction: column;
		-webkit-box-pack: justify;
		-webkit-justify-content: space-between;
		justify-content: space-between;
		width: 100%;
		padding-top: 3.375em;
		padding-bottom: 3em;
	}

	@media (min-width: 42.5em) {
		.footer-slot .footer-links {
			-webkit-flex-wrap: wrap;
			flex-wrap: wrap;
			-webkit-box-pack: start;
			-webkit-justify-content: flex-start;
			justify-content: flex-start;
			height: calc(3.375em + 3em + 1.875em + 2.5em * 16);
		}
	}

	@media (min-width: 48em) {
		.footer-slot .footer-links {
			-webkit-box-orient: horizontal;
			-webkit-box-direction: normal;
			-webkit-flex-direction: row;
			flex-direction: row;
			-webkit-flex-wrap: nowrap;
			flex-wrap: nowrap;
			height: auto;
			padding: 2.5em 0.3125em 3.75em;
		}
	}

	@media (min-width: 64em) {
		.footer-links {
			width: 64em;
		}
	}

	@media (min-width: 42.5em) {
		.footer-links__category {
			width: 33.3334%;
			padding-right: 0.9375em;
			padding-left: 0.9375em;
		}
	}

	.footer-links__category:not(:last-child) {
		margin-bottom: 3em;
	}

	.footer-links__category:nth-child(1) {
		-webkit-box-ordinal-group: 4;
		-webkit-order: 3;
		order: 3;
	}

	@media (min-width: 42.5em) {
		.footer-links__category:nth-child(1) {
			-webkit-box-ordinal-group: 6;
			-webkit-order: 5;
			order: 5;
		}
	}

	@media (min-width: 48em) {
		.footer-links__category {
			width: 20%;
		}

		.footer-slot .footer-links__category {
			margin-bottom: 0;
		}

		.footer-links__category:nth-child(1) {
			-webkit-box-ordinal-group: 2;
			-webkit-order: 1;
			order: 1;
		}
	}

	.footer-links__category:nth-child(2) {
		-webkit-box-ordinal-group: 3;
		-webkit-order: 2;
		order: 2;
	}

	.footer-links__category:nth-child(3) {
		-webkit-box-ordinal-group: 2;
		-webkit-order: 1;
		order: 1;
	}

	@media (min-width: 42.5em) {
		.footer-links__category:nth-child(3) {
			-webkit-box-ordinal-group: 4;
			-webkit-order: 3;
			order: 3;
		}
	}

	.footer-links__category:nth-child(4) {
		-webkit-box-ordinal-group: 5;
		-webkit-order: 4;
		order: 4;
	}

	@media (min-width: 42.5em) {
		.footer-links__category:nth-child(4) {
			-webkit-box-ordinal-group: 2;
			-webkit-order: 1;
			order: 1;
		}
	}

	@media (min-width: 48em) {
		.footer-links__category:nth-child(4) {
			-webkit-box-ordinal-group: 5;
			-webkit-order: 4;
			order: 4;
		}
	}

	.footer-links__category:nth-child(5) {
		-webkit-box-ordinal-group: 6;
		-webkit-order: 5;
		order: 5;
	}

	@media (min-width: 42.5em) {
		.footer-links__category:nth-child(5) {
			-webkit-box-ordinal-group: 5;
			-webkit-order: 4;
			order: 4;
		}
	}

	.footer-slot .footer-links__category__header {
		height: 2.5em;
		margin-top: 0;
		margin-bottom: 0;
		padding-right: 1.25em;
		padding-left: 1.25em;
		font-size: 0.75em;
		text-transform: uppercase;
	}

	.footer-slot .footer-links__category-list {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-orient: vertical;
		-webkit-box-direction: normal;
		-webkit-flex-direction: column;
		flex-direction: column;
		-webkit-flex-wrap: wrap;
		flex-wrap: wrap;
		-webkit-align-content: flex-start;
		align-content: flex-start;
		line-height: 1.3;
	}

	.footer-links__category:nth-child(1) .footer-links__category-list {
		height: calc(2.5em * 4);
	}

	@media (min-width: 35.5em) {
		.footer-links__category:nth-child(1) .footer-links__category-list {
			height: calc(2.5em * 3);
		}
	}

	.footer-links__category:nth-child(2) .footer-links__category-list {
		height: calc(2.5em * 4) !important;
	}

	#footer .footer-links__category:nth-child(3) .footer-links__category-list,
	.footer-links__category:nth-child(3) .footer-links__category-list {
		height: calc(2.5em * 3) !important;
	}

	@media (min-width: 35.5em) {
		.footer-links__category:nth-child(2) .footer-links__category-list {
			height: calc(2.5em * 3) !important;
		}

		#footer .footer-links__category:nth-child(3) .footer-links__category-list,
		.footer-links__category:nth-child(3) .footer-links__category-list {
			height: calc(2.5em * 2) !important;
		}
	}

	.footer-links__category:nth-child(4) .footer-links__category-list {
		height: calc(2.5em * 1);
	}

	.footer-links__category:nth-child(5) .footer-links__category-list {
		height: calc(2.5em * 4);
	}

	@media (min-width: 35.5em) {
		.footer-links__category:nth-child(5) .footer-links__category-list {
			height: calc(2.5em * 3);
		}
	}

	@media (min-width: 42.5em) {
		.footer-slot .footer-links__category__header {
			padding-right: 0;
			padding-left: 0;
		}

		.footer-slot .footer-links__category-list {
			-webkit-flex-wrap: nowrap;
			flex-wrap: nowrap;
		}

		#footer .footer-links__category:nth-child(3) .footer-links__category-list,
		.footer-slot .footer-links__category .footer-links__category-list {
			height: auto !important;
		}
	}

	.footer-links__category-list__item {
		width: 50%;
		padding-right: 0.9375em;
		padding-left: 0.9375em;
	}

	@media (min-width: 35.5em) {
		.footer-links__category-list__item {
			width: 33.3334%;
		}
	}

	@media (min-width: 42.5em) {
		.footer-links__category-list__item {
			width: 100%;
			padding-right: 0;
			padding-left: 0;
		}
	}

	.is-logged-in .customer-logged-state {
		display: none;
	}

	#footer .footer-links__category-list__item {
		line-height: normal;
	}

	#footer .footer-links__category-list__item:last-child {
		padding-bottom: 0;
	}

	.footer-slot .footer-links__category-list__link {
		display: -webkit-inline-flex;
		display: -ms-inline-flexbox;
		display: inline-flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-grid-row-align: center;
		align-items: center;
		height: 3.3333em;
		font-size: 0.75em;
		color: #454545;
		text-decoration: none;
	}

	#footer .footer-slot .footer-links__category-list__link {
		padding-top: 0;
		padding-bottom: 0;
	}

	.footer-slot .footer-links__category-list__link:focus,
	.footer-slot .footer-links__category-list__link:hover {
		text-decoration: underline;
	}

	.footer-slot .footer-links__category-list__link br {
		font-size: 1px;
	}

	.footer-desktop {
		display: none;
	}

	@media (min-width: 48em) {
		.footer-links__category:nth-child(5) {
			-webkit-box-ordinal-group: 6;
			-webkit-order: 5;
			order: 5;
		}

		.footer-slot .footer-desktop {
			display: block;
			width: 100%;
			padding-bottom: 3.75em;
		}
	}

	#footer .footer-desktop {
		display: none;
	}

	.footer-slot .footer-desktop__link {
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		width: 21.875em;
		height: 3.125em;
		margin-right: auto;
		margin-left: auto;
		border: 0.0625em solid #000;
		background-color: transparent;
		text-decoration: none;
	}

	.footer-slot .footer-desktop__copy {
		display: inline-block;
		font-size: 0.75em;
		letter-spacing: 0.1667em;
	}

	.footer-slot .footer-desktop__icon {
		display: inline-block;
		width: 1.125em;
		height: 1.125em;
		margin-left: 0.75em;
	}

	.footer-desktop__icon__image {
		width: 1.125em;
		height: 1.125em;
	}

	.footer-slot .footer-legal {
		width: 100%;
		max-width: 64em;
		margin-right: auto;
		margin-left: auto;
		padding: 1.5em 0.9375em;
		border-top: 1px solid #ccc;
		text-align: center;
	}

	@media (min-width: 42.5em) {
		.footer-slot .footer-legal {
			padding-right: 0;
			padding-left: 0;
		}
	}

	@media (max-width: 48em) {
		.footer-slot .footer-legal {
			padding-bottom: 3.5em;
		}
	}

	.footer-legal__copy {
		max-width: 35.5em;
		margin-right: auto;
		margin-left: auto;
	}

	@media (min-width: 64em) {
		.footer-legal__copy {
			max-width: none;
			padding-right: 0.9375em;
			padding-left: 0.9375em;
		}
	}

	#footer .footer-legal__copy p,
	.footer-legal__copy p {
		font-size: 0.6875em;
		color: #454545;
		line-height: 1.4;
	}

	#footer .footer-legal__copy p:first-child,
	.footer-legal__copy p:first-child {
		margin-top: 0;
	}

	#footer .footer-legal__copy p:last-child,
	.footer-legal__copy p:last-child {
		margin-bottom: 0;
	}

	.footer-slot .footer-legal a {
		font-family: "Gotham 7r", GothamBold, Helvetica, Arial, sans-serif;
		font-size: 1em;
		color: #000;
	}

	.footer-slot .footer-legal a:focus,
	.footer-slot .footer-legal a:hover {
		color: #666;
	}

	.footer-slot .qr__app__container {
		width:100%;
		height:38px;
	}

	.footer-slot .qr__app__img__block {
		width:16%;
		height:5em;
	}

	.footer-slot .qr__app__img {
		width:100%;
		height:100%;
		display:block;
	}

	.footer-slot .footer-top__native-app__badges p,
	.footer-top__native-app__badges p{
		font-family: 'Gotham 4r','GothamBook',Helvetica,Arial,sans-serif;
		color:#000;
		font-size:.9em!important;
		line-height:1.2em;
		margin:0 0 .5em 0;
	}

	#footer .footer-slot .footer-top__native-app__badges p.scancode__copy {
		font-family: 'Gotham 4r','GothamBook',Helvetica,Arial,sans-serif;
		color:#000;
		font-size:.9em;
		line-height:1.2em;
		margin:0 0 .5em 0;
	}

	.footer-slot .footer-top__native-app .scancode__button {
		border-radius: 0.3em;
		background-color:#000;
		width:8em;
	}

	.footer-slot .footer-top__native-app p.scancode__button__copy {
		font-family: 'Gotham 4r','GothamBook',Helvetica,Arial,sans-serif;
		color:#fff;
		font-size:.8em;
		text-align:center;
		padding:.4em 0;
		text-decoration:none !important;
		margin:0;
	}


	.footer-slot .footer-top .scancode__button:focus .scancode__button__copy,
	.footer-slot .footer-top .scancode__button:hover .scancode__button__copy {
		font-family: 'Gotham 5r','GothamBook',Helvetica,Arial,sans-serif;
	}


</style>
<script>
	!(function(t) {
		"use strict";

		function o() {
			var o = t.location.hostname;
			return /^(m|m-stage)\./.test(o) ? "mobile" : /^(mobile|tkpreview)\./.test(o) && $("#id_skStudio_Wrapper").length ? "tablet-skava" : /^(mobile|mobile-stage)\./.test(o) ? "tablet-mosaic" : "desktop";
		}

		function e() {
			var e = o();
			return "mobile" === e || "tablet-mosaic" === e ? t.kohlsUtils.isLoggedIn() : "tablet-skava" === e ? "" !== (document.cookie.match(/(?:skVisitorFullName=)(.*?)(?:;|$)/) || ["", ""])[1] : "" !== (document.cookie.match(
				/(?:VisitorUsaFullName=)(.*?)(?:;|$)/) || ["", ""])[1];
		}

		function i() {
			s.addClass("is-logged-in");
		}

		function a() {
			$(".js-footer-back-to-top").on("click", n);
		}

		function n(o) {
			o.preventDefault(), t.requestAnimationFrame(function() {
				$("html, body").animate({
					scrollTop: 0
				}, {
					complete: function() {
						l.focus();
					}
				});
			});
		}
		var s, l;
		$(function() {
			l = $('<div id="footer-top-of-page" tabindex="-1"></div>');
			(s = $(".footer-global")), $("body").prepend(l), a(), e() && i();
		});
	})(this);
</script>
<div class="footer-slot">
	<div class="footer-back-to-top" aria-hidden="true"> <a href="#footer-top-of-page" class="footer-back-to-top__button btn--unstyled js-footer-back-to-top">Back to Top</a> </div>
</div>
<footer id="footer" class="footer-global" role="contentinfo">
	<div class="footer-slot">
		<div class="footer-top">
			<div class="footer-top__social">
				<h3 class="[ footer-top__header footer-top__header--social ]"> Connect With Us </h3>
				<ul class="footer-top__social-list list--unstyled">
					<li class="footer-top__social-list__item"> <a href="https://www.facebook.com/kohls" target="_blank" rel="noopener" class="footer-top__social-list__link"> <svg xmlns="https://www.w3.org/2000/svg" width="48" height="48"
							  viewBox="0 0 48 48" role="img" focusable="false" class="footer-top__social-list__network-icon">
								<title>Facebook</title>
								<path d="M26.013 37V25.14h3.98l.598-4.62h-4.576v-2.952c0-1.34.372-2.25 2.29-2.25h2.448v-4.135c-.422-.057-1.875-.183-3.564-.183-3.53 0-5.945 2.154-5.945 6.11v3.408h-3.99v4.622h3.99V37h4.773z" />
							</svg> </a> </li>
					<li class="footer-top__social-list__item"> <a href="https://twitter.com/kohls" target="_blank" rel="noopener" class="footer-top__social-list__link"> <svg xmlns="https://www.w3.org/2000/svg" width="48" height="48"
							  viewBox="0 0 48 48" role="img" focusable="false" class="footer-top__social-list__network-icon">
								<title>Twitter</title>
								<path
								  d="M37.038 15.955c-.956.425-1.984.71-3.063.84 1.102-.66 1.946-1.706 2.346-2.95-1.03.61-2.17 1.054-3.383 1.293-.974-1.036-2.36-1.685-3.895-1.685-2.945 0-5.333 2.39-5.333 5.334 0 .418.046.825.137 1.215-4.433-.222-8.364-2.346-10.995-5.573-.46.783-.724 1.7-.724 2.68 0 1.85.94 3.48 2.373 4.44-.876-.03-1.7-.27-2.42-.67v.067c0 2.584 1.84 4.74 4.28 5.23-.446.122-.92.187-1.404.187-.344 0-.678-.035-1.003-.097.68 2.12 2.648 3.663 4.982 3.707-1.826 1.43-4.125 2.283-6.625 2.283-.43 0-.854-.026-1.27-.075 2.36 1.514 5.164 2.396 8.176 2.396 9.812 0 15.177-8.125 15.177-15.174 0-.23-.005-.46-.016-.69 1.04-.75 1.945-1.69 2.66-2.76z" />
							</svg> </a> </li>
					<li class="footer-top__social-list__item"> <a href="https://www.pinterest.com/kohls/" target="_blank" rel="noopener" class="footer-top__social-list__link"> <svg xmlns="https://www.w3.org/2000/svg" width="48" height="48"
							  viewBox="0 0 48 48" role="img" focusable="false" class="footer-top__social-list__network-icon">
								<title>Pinterest</title>
								<path
								  d="M32.243 18.003c-.65-4.74-5.383-7.156-10.42-6.59-3.993.445-7.962 3.668-8.13 8.282-.103 2.813.696 4.922 3.377 5.513 1.156-2.05-.375-2.495-.615-3.983-.982-6.078 7.007-10.224 11.19-5.982 2.9 2.943.988 11.98-3.677 11.037-4.474-.894 2.19-8.086-1.376-9.5-2.9-1.15-4.446 3.508-3.07 5.823-.803 3.983-2.54 7.733-1.837 12.73 2.28-1.656 3.047-4.827 3.677-8.13 1.15.695 1.765 1.42 3.22 1.538 5.392.41 8.4-5.382 7.66-10.736zm0 0" />
							</svg> </a> </li>
					<li class="footer-top__social-list__item"> <a href="https://www.instagram.com/kohls" target="_blank" rel="noopener" class="footer-top__social-list__link"> <svg xmlns="https://www.w3.org/2000/svg" width="48" height="48"
							  viewBox="0 0 48 48" role="img" focusable="false" class="footer-top__social-list__network-icon">
								<title>Instagram</title>
								<path
								  d="M24 15.006c1.68 0 3.404-.076 5.078.114 2.297.26 3.655 1.846 3.833 4.11.14 1.705.09 3.43.09 5.14 0 1.635.1 3.358-.15 4.982-.68 4.506-6.21 3.646-9.55 3.645-1.59-.003-3.31.11-4.89-.192-2.32-.447-3.24-2.334-3.34-4.518-.07-1.662-.06-3.33-.05-4.994 0-1.526-.08-3.128.15-4.644.656-4.31 5.6-3.65 8.847-3.65m0-1.98c-3.84 0-8.9-.48-10.474 3.99-.542 1.54-.483 3.262-.493 4.88-.01 1.974-.04 3.955.032 5.93.08 2.177.48 4.26 2.262 5.71 1.52 1.24 3.476 1.36 5.356 1.404 2.06.05 4.12.04 6.17.01 1.74-.027 3.55-.05 5.1-.93 2.507-1.42 2.93-4.1 2.996-6.72.06-2.38.076-4.774-.015-7.15-.11-2.867-1-5.596-3.96-6.638-2.13-.75-4.75-.5-6.974-.5z" />
								<path
								  d="M24 18.367c-3.112 0-5.635 2.523-5.635 5.635s2.523 5.634 5.635 5.634c3.11 0 5.635-2.522 5.635-5.634S27.11 18.367 24 18.367m0 9.292c-2.02 0-3.657-1.64-3.657-3.66 0-2.02 1.637-3.66 3.657-3.66s3.657 1.64 3.657 3.66S26.02 27.66 24 27.66zm7.173-9.52c0 .72-.59 1.31-1.315 1.31-.73 0-1.317-.59-1.317-1.32s.59-1.318 1.32-1.318c.73 0 1.32.59 1.32 1.314" />
							</svg> </a> </li>
					<li class="footer-top__social-list__item"> <a href="https://www.youtube.com/kohls" target="_blank" rel="noopener" class="footer-top__social-list__link"> <svg xmlns="https://www.w3.org/2000/svg" width="48" height="48"
							  viewBox="0 0 48 48" role="img" focusable="false" class="footer-top__social-list__network-icon">
								<title>YouTube</title>
								<path fill="#FFF" d="M20.814 26.88l7.026-3.64-7.026-3.666v7.306z" />
								<path fill-rule="evenodd" d="M20.814 19.575l6.162 4.114.864-.45-7.026-3.668z" opacity=".12" clip-rule="evenodd" />
								<path
								  d="M36.24 18.31s-.255-1.793-1.034-2.582c-.988-1.035-2.097-1.04-2.605-1.1-3.634-.264-9.092-.264-9.092-.264h-.01s-5.46 0-9.097.263c-.51.06-1.616.066-2.604 1.1-.78.79-1.034 2.582-1.034 2.582s-.26 2.1-.26 4.204v1.972c0 2.103.26 4.208.26 4.208s.256 1.79 1.036 2.58c.99 1.035 2.288 1.003 2.867 1.11 2.08.2 8.84.262 8.84.262s5.464-.008 9.102-.27c.51-.063 1.617-.067 2.605-1.103.78-.79 1.032-2.58 1.032-2.58s.26-2.104.26-4.21v-1.97c0-2.104-.26-4.208-.26-4.208zm-15.425 8.57v-7.305l7.025 3.666-7.025 3.64z" />
							</svg> </a> </li>
				</ul>
			</div> <!-- .footer-top__social -->
			<div class="footer-top__native-app">
				<h3 class="[ footer-top__header footer-top__header--native-app ]">get our<br aria-hidden="true"> app</h3>

				<div class="qr__app__img__block">
				<img class="qr__app__img" src="https://media.kohlsimg.com/is/image/kohls/AppStore-qr_code2020?scl=1&fmt=png8" alt="">
				</div>

				<div class="footer-top__native-app__badges">
					<p>Scan the code to download<br aria-hidden="true">the Kohl&rsquo;s App today.</p>
					<a class="scancode__button" href="/feature/app.jsp">
						<p class="scancode__button__copy">Learn More</p>
					</a>
				</div>

			</div> <!-- .footer-top__native-app -->
		</div> <!-- .footer-top -->
		<div class="footer-links">
			<div class="footer-links__category">
				<h3 class="footer-links__category__header gotham-medium"> Customer Service </h3>
				<ul class="footer-links__category-list list--unstyled">
					<li class="footer-links__category-list__item"> <a href="https://cs.kohls.com/" rel="noopener" class="footer-links__category-list__link">Contact Us</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://cs.kohls.com/app/answers/help_topic/c/22,3" rel="noopener" class="footer-links__category-list__link">Shipping</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://cs.kohls.com/app/answers/help_topic/c/22,4" rel="noopener" class="footer-links__category-list__link">Returns</a> </li>
					<li class="footer-links__category-list__item"> <a href="/sale-event/rebates.jsp" class="footer-links__category-list__link">Rebates</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://cs.kohls.com/app/answers/detail/a_id/1013" target="_blank" rel="noopener" class="footer-links__category-list__link">Recalls</a> </li>
					<li class="footer-links__category-list__item"> <a href="/feature/productguides.jsp" target="_blank" class="footer-links__category-list__link">Product Guides</a> </li>
					<li class="footer-links__category-list__item"> <a href="http://www.kohlsfeedback.com" target="_blank" class="footer-links__category-list__link">Store Survey</a> </li>
					<li class="footer-links__category-list__item"> <a href="/feature/kohls-cash.jsp" class="footer-links__category-list__link">Kohl&rsquo;s Cash</a> </li>
				</ul>
			</div>
			<div class="footer-links__category">
				<h3 class="footer-links__category__header gotham-medium"> Shop Kohl&rsquo;s </h3>
				<ul class="footer-links__category-list list--unstyled">
					<li class="footer-links__category-list__item"> <a href="/homepage/sale_alert_signup.jsp" class="footer-links__category-list__link">Get 15% off when you<br />sign up for our emails</a> </li>
					<li class="footer-links__category-list__item"> <a href="/upgrade/giftinglisting/wishlist.jsp" class="footer-links__category-list__link">My Lists</a> </li>
					<li class="footer-links__category-list__item"> <a href="/feature/sitemapmain.jsp" class="footer-links__category-list__link">Site Map</a> </li>
					<li class="footer-links__category-list__item"> <a href="/stores.shtml" class="footer-links__category-list__link">Store Locator</a> </li>
					<li class="footer-links__category-list__item"> <a href="/sale-event/gift-cards.jsp" class="footer-links__category-list__link">Gift Cards</a> </li>
					<li class="footer-links__category-list__item"> <a href="/sale-event/coupons-deals.jsp" class="footer-links__category-list__link">Kohl&rsquo;s Coupons</a> </li>
				</ul>
			</div>
			<div class="footer-links__category">
				<h3 class="footer-links__category__header gotham-medium"> My Account </h3>
				<ul class="footer-links__category-list list--unstyled js-d-footer-customer-login">
					<li class="footer-links__category-list__item customer-logged-state"> <a href="/myaccount/kohls_login.jsp" class="footer-links__category-list__link">Sign In</a> </li>
					<li class="footer-links__category-list__item"> <a href="/myaccount/v2/myinfo.jsp" class="footer-links__category-list__link">My Account</a> </li>
					<li class="footer-links__category-list__item"> <a href="/myaccount/v2/myinfo.jsp" class="footer-links__category-list__link">Update Password</a> </li>
					<li class="footer-links__category-list__item"> <a href="/upgrade/myaccount/order_status_login.jsp" class="footer-links__category-list__link">Order Status</a> </li>
					<li class="footer-links__category-list__item"> <a href="/myaccount/kohls_rewards.jsp" class="footer-links__category-list__link">Rewards</a> </li>
					<li class="footer-links__category-list__item"> <a href="/giftcard/gift_card_check_balance.jsp" class="footer-links__category-list__link">Gift Card Balance &amp;<br />Kohl&rsquo;s Cash Balance</a> </li>
				</ul>
			</div>
			<div class="footer-links__category">
				<h3 class="footer-links__category__header gotham-medium"> Kohl&rsquo;s Card </h3>
				<ul class="footer-links__category-list list--unstyled">
					<li class="footer-links__category-list__item"> <a href="/sale-event/my-kohls-charge.jsp?icid=benefitskccfooter" target="_blank" rel="noopener" class="footer-links__category-list__link">Benefits</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://credit.kohls.com/eCustService/?icid=managekccfooter" onclick="javascript:launchCorporate('https://credit.kohls.com/eCustService/?icid=managekccfooter','', 'kccRedirectionWarning'); return false;"
						  rel="noopener" class="footer-links__category-list__link">Pay & Manage Card</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://apply.kohls.com/?icid=applyfooter" target="_blank" rel="noopener" class="footer-links__category-list__link">Apply for a Kohl&rsquo;s Card</a> </li>
					<li class="footer-links__category-list__item"> <a href="/feature/pre-qual/prequal_inquiry.jsp?icid=prequalfooter" target="_blank" rel="noopener" class="footer-links__category-list__link">See if you prequalify</a> </li>
				</ul>
			</div>
			<div class="footer-links__category">
				<h3 class="footer-links__category__header gotham-medium"> About Kohl&rsquo;s </h3>
				<ul class="footer-links__category-list list--unstyled">
					<li class="footer-links__category-list__item"> <a href="/feature/ourwebsites.jsp" class="footer-links__category-list__link">Our Websites</a> </li>
					<li class="footer-links__category-list__item"> <a href="/sale-event/kohl-s-cares.jsp" class="footer-links__category-list__link">Community</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://corporate.kohls.com/corporate-responsibility/sustainability" class="footer-links__category-list__link">Sustainability</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://careers.kohls.com/" target="_blank" rel="noopener" class="footer-links__category-list__link">Careers</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://careers.kohls.com/seasonal" target="_blank" rel="noopener" class="footer-links__category-list__link">Apply for Seasonal Jobs</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://myhr.kohls.com/" target="_blank" rel="noopener" class="footer-links__category-list__link">Associate Services</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://corporate.kohls.com/investors" target="_blank" rel="noopener" class="footer-links__category-list__link">Investor Relations</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://www.kohls.com/info/affiliate-program.shtml" class="footer-links__category-list__link">Affiliate Program</a> </li>
					<li class="footer-links__category-list__item"> <a href="https://www.kohls.com/advertising" target="_blank" rel="noopener" class="footer-links__category-list__link">Advertise with Us</a> </li>
				</ul>
			</div>
		</div> <!-- .footer-links -->
		<div class="footer-legal">
			<div class="footer-legal__copy">
				<!-- * Do NOT place links on individual lines! HTML whitespace is stripped by the * content management system, causing content properly whitespaced in the * browser prior to now be jammed up against other copy. This will cause SEO, * readability, and accessibility problems if the links are not left in a * single line per paragraph element. -->
				<p> &copy; 2021 Kohl&rsquo;s, Inc.<br aria-hidden="true"> KOHL&rsquo;S&reg; and Kohl&rsquo;s brand names are trademarks owned by KIN, Inc.<br aria-hidden="true"> All rights reserved.</p>
				<p> Android, Google Play and the Google Play logo are trademarks of Google Inc. App Store is a service mark of Apple Inc. </p>
				<p> <a href="/feature/legal-notices.jsp">Legal Notices</a>, <a href="/feature/privacy-policy.jsp">Privacy Policy</a>, <a href="/feature/privacy-policy.jsp#calirights">California Privacy Policy</a>, <a href="/CAStopSale"
					  target="_blank">CA-Do Not Sell My Personal Information</a>, <a href="/feature/about-our-ads.jsp">About Our Ads</a>, <a href="/feature/socially-responsible.jsp">California Transparency in Supply Chains Act</a>. </p>
			</div>
		</div> <!-- .footer-legal -->
	</div>
</footer> <!-- .footer-global -->
<!-- RRN -->
<script type="text/javascript" charset="utf-8">		
	$requires(('https:' == document.location.protocol) ? 'https://cdns.brsrvr.com/v1/br-trk-5117.js' : 'http://cdn.brcdn.com/v1/br-trk-5117.js', $requires.K.standalone);
</script><!-- Footer div closed -->
		<script>
			$init(function _$init_vnload($) {
				var ulsList  = $("ul");
				var lstElement  = ulsList.children("li[replace]");
				if(lstElement !== null || lstElement !== undefined)
				{
					var element = null, child= null, attrValue = "";
					lstElement.each(function(idx, li) 
					{
						element = $(li);
						child = element.find('[child]').first();
						attrValue  = child.attr("child");   
						if(attrValue != null || attrValue !== undefined)
						{
						 element.attr("class",attrValue);
						}			
						child.removeAttr( "child" );
						element.removeAttr("replace");
					});
				}				
					$ready(function _ready_vnload($) {
						Kjs.poll(200, function() {
							return $('.vn-content a').length > 0;
						}, function () {
							var temp=$('.vn-content a');
							$.each(temp,function(i,v){
								var url=$(v).attr('data-url');
								if(window.browseSearchPlatform !=undefined){
									switch(window.browseSearchPlatform){
										case 'E':url = updateParameterByName(url,'srp','e1');
											break;
										case 'S':url = updateParameterByName(url,'srp','e2');
											break;
									}
								}
								$(v).attr('data-url',url);
							}	);
						});
					},{isAfterReady: true});
				
			});
		</script>
    

			
		</div>
	</div>

		<div display="none" id="tmpl_pool">
		
		</div>
	<script type="text/javascript" >var _cf = _cf || [];  _cf.push(['_setFsp', true]);  _cf.push(['_setBm', true]);  _cf.push(['_setAu', '/kkhepdqgay/58a664a3f8rn1917698e839c90363183']); </script><script type="text/javascript"  src="/kkhepdqgay/58a664a3f8rn1917698e839c90363183"></script><link rel="stylesheet" type="text/css" href="/_sec/cp_challenge/sec-3-4.css">
            <script src="/_sec/cp_challenge/sec-cpt-3-4.js" async defer></script>
            <div id="sec-overlay" style="display:none;">
            <div id="sec-container">
            </div>
          </div></body>
	
	</html>
