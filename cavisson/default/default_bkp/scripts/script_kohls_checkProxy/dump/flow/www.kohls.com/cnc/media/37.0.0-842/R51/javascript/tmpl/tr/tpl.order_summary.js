<script id="orderSummaryTemplate" type="text/x-jQuery-tmpl">
	{{if $env.page != 'checkout' && trJsonData.pilotProgram && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser}}
	<div class="purchase_earnings_summary">
	{{if $env.ksLoyaltyV2}}
		{{tmpl "#ksLoyaltyTrackerTemplateV2"}} 
	{{else}}
		{{tmpl "#ksLoyaltyTrackerTemplate"}} 
	{{/if}}	
	</div>
	{{/if}}
	{{if confirmOrder && taxFeeEnabled}}
		{{tmpl "#orderSummaryTemplatenewconfirm"}}
	{{else}}
		{{tmpl "#orderSummaryCheckoutTemplate"}}
	{{/if}}

	{{if confirmOrder && trJsonData.pilotProgram && trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
		<div class="purchase_earnings_summary">	
			{{if $env.ksLoyaltyV2}}
				{{tmpl "#purchaseEarningsLoyaltyTemplateV2"}} 
			{{else}}
				{{tmpl "#purchaseEarningsLoyaltyTemplate"}} 
			{{/if}}	
		</div>

	{{else !confirmOrder && trJsonData.pilotProgram && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser}}
		<div class="purchase_earnings_summary">
			{{if $env.ksLoyaltyV2}}
				{{tmpl "#ksLoyaltyEarningsTemplateV2"}}
	{{else}}
		{{tmpl "#ksLoyaltyEarningsTemplate"}}
			{{/if}}
		</div>
	{{else}}
		<div class="purchase_earnings_summary">
			{{tmpl "#purchaseEarningsTemplate"}}
		</div>
	{{/if}}	
	
	{{if trJsonData.pilotProgram && trJsonData.ksCnCLoyaltyShoppingBagUpsellMsg && !Kjs.trCommonCheckout.isCheckoutPage() && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings && 
			trJsonData.purchaseEarnings.loyaltyPilotUser && !trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc}}
			<div class="purchase_earnings_summary">	
			{{if $env.ksLoyaltyV2}}
					{{tmpl "#kccUpsellMessageTemplateV2"}} 
				{{else}}
					{{tmpl "#kccUpsellMessageTemplate"}} 
				{{/if}}				 
			</div>
	{{else}}
		<div class="preQualTemplate preQualCheck">
			{{if !$env.isKioskEnabled}}
				{{tmpl "#prequalTemplate"}}
			{{/if}}
    	</div>
	{{/if}}
</script>
<script class="ksBdRecSectionTemplate" id="ksBdRecSectionTemplate" type="text/x-jQuery-tmpl">
	{{if trJsonData.recoSystemParameters.isRecoEnabled ==="true" }}
		{{if $env.ksCncBigDataUpgradeRenderingEnabled }}
			<div id="bd_rec_${trJsonData.recoSystemParameters.recoBDPlacementName}" data-template-type="${trJsonData.recoSystemParameters.recoBDTemplateName}"></div>
		{{else}}
			<div class="big_data_recs suggestItems"></div>
		{{/if}}
	{{/if}}
</script>

<script class="freeShippingTemplateV2" id="freeShippingTemplateV2" type="text/x-jQuery-tmpl">
<div class="freeShippingTitleDiv">
<div id="freeShippingTemplatev2" style="${trLabels.kls_static_free_shipping_banner_bg_colorV2}">
     {{if trJsonData.purchaseEarnings && !trJsonData.purchaseEarnings.loyaltyPilotUser}}
        {{if trJsonData.isGuest == "true"}}
            {{if Kjs.trOrderSummary.getFreeShipDelta() > 0}}
                <p class="gnfs-top-text">{{html trLabels.kls_static_guest_not_free_shipping_messageV2.replace('{0}', Kjs.trOrderSummary.getFreeShipDelta().toFixed(2))}}</p>
            {{else}}
                <p class="gfs-top-text">{{html trLabels.kls_static_guest_free_shipping_messageV2}}</p>
            {{/if}}
        {{else}}
            {{if Kjs.trOrderSummary.getFreeShipDelta() > 0}}
                <p class="nfs-top-text">{{html trLabels.kls_static_loggedin_not_free_shipping_messageV2.replace('{0}', Kjs.trCommonCheckout.getUserNameFromCookie()).replace('{1}', Kjs.trOrderSummary.getFreeShipDelta().toFixed(2))}}</p>
            {{else}}
                <p class="fs-top-text">{{html trLabels.kls_static_loggedin_free_shipping_messageV2.replace('{0}', Kjs.trCommonCheckout.getUserNameFromCookie())}}</p>
            {{/if}}
	    {{/if}}
    {{/if}}    
</div>
</div>
</script>

<script class="ksLoyaltyTrackerTemplate" id="ksLoyaltyTrackerTemplate" type="text/x-jQuery-tmpl">
	<!---------- START Adding null check ---------->
		{{if trJsonData.purchaseEarnings && trJsonData.purchaseEarnings != null && trJsonData.purchaseEarnings.kohlsCashEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings != null}}
	<!----------- END Null check ---------->
			{{if trJsonData.purchaseEarnings.loyaltyPilotUser}}
				{{if trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings && !trJsonData.purchaseEarnings.kohlsCashEarnings.loyaltySystem || (trJsonData.cartItems && trJsonData.cartItems.length == 0)}}	
				<div class="TMB---Event-Threshold Clip-85" id="kls_loyality_tracker">
					<div class="im_background">	
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod}}
						<div class="im_kohls_cash_horizontal" id="loyalityRewardEventImg" title="Kohls cash bonus event" ></div>
						{{else}}
						<div class="Kohls-Rewards-Horizontal-Lockup" id="loyalityRewardImg" title="Kohls cash">  </div>		
						{{/if}}
							<div>
								<div class="layer1">
									<span class="text-style-1">${trLabels.kls_static_loyalty_KCC_meter_start_amount}</span>
								</div>
								<div class="Rectangle-25">
									{{if trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod}}
									<div class="im_green-bar" style="${Kjs.trOrderSummary.calculateLoyaltyProgressBarWidth(trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnTrackerBalance)}" title="$${trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnTrackerBalance}"></div>
									{{else}}
									<div class="im_green-bar" style="${Kjs.trOrderSummary.calculateLoyaltyProgressBarWidth(trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal)}" title="$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal}"></div>
									{{/if}}
								</div>
								<div class="layer2">
									{{if trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod}}
									<span class="text-style-1">$${trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc}</span>
									{{else}}
									<span class="text-style-1">$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold}</span>
									{{/if}}
								</div>
								<br style="clear: left;" />
							</div>
							{{if trJsonData.cartItems && trJsonData.cartItems.length == 0 && trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod && trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc}}
								{{if  trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent > trJsonData.thresholdKohlsMeterValue}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerEarnEventText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_event, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendThresholdforEventKc,trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnPeriodEndDate, '')}}
										</span>
									</div>
								{{else}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerSpentAwayEventText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_spend_message_event_sb, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnPeriodEndDate, '')}}
										</span>
									</div>
								{{/if}}
							{{else}}
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerEarnEventText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__hasKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, '', '')}}
										</span>
									</div>
								{{else}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerEarnEventText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__noKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, '', '')}}
										</span>
									</div>
								{{/if}}
							{{/if}}
						</div>
					</div>
				</div>
				{{else}}
					{{if trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod && $env.ksCncEliteBonusSpendTracker}}
						<div class="TMB---Event-Threshold Clip-85" id="kls_loyality_tracker">
							<div class="im_background">
								<!---------- START Adding null check ---------->
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc && trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc != null}}
								<!----------- END Null check ---------->
									<div class="im_kohls_cash_horizontal" id="loyalityRewardEventImg" title="Kohls cash bonus event" ></div>
									<div>
										<div class="layer1">
											<span class="text-style-1">${trLabels.kls_static_loyalty_KCC_meter_start_amount}</span>
										</div>
										<div class="Rectangle-25">
											<div class="im_green-bar" style="${Kjs.trOrderSummary.calculateLoyaltyProgressBarWidth(trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnTrackerBalance)}" title="$${trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnTrackerBalance}"></div>
										</div>
										<div class="layer2">
											<span class="text-style-1">$${trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc}</span>
										</div>
										<br style="clear: left;" />
									</div>
									<!---------- START Adding null check ---------->
									{{if trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent != null && trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent > trJsonData.thresholdKohlsMeterValue}}
									<!----------- END Null check ---------->	
										<div class="Spend-20-to-get-you" id="loyalityTrackerEarnEventText">
											<span>
												{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_event, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendThresholdforEventKc,trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnPeriodEndDate, '')}}
											</span>
										</div>
									{{else}}
										<div class="Spend-20-to-get-you" id="loyalityTrackerSpentAwayEventText">
											<span>
												{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_spend_message_event_sb, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnPeriodEndDate, '')}}
											</span>
										</div>
									{{/if}}
								{{/if}}
							</div>
						</div>
					{{else $env.ksCncEliteRewardsSpendTracker}}
					<div class="TMB---Event-Threshold Clip-85" id="kls_loyality_tracker">
							<div class="im_background">
						<!---------- START Adding null check ---------->
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc && trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc != null}}
						<!----------- END Null check ---------->
							<div class="Kohls-Rewards-Horizontal-Lockup" id="loyalityRewardImg" title="Kohls cash">  </div>
							<div>
								<div class="layer1">
									<span class="text-style-1">${trLabels.kls_static_loyalty_KCC_meter_start_amount}</span>
								</div>
								<div class="Rectangle-25">
									<div class="im_green-bar" style="${Kjs.trOrderSummary.calculateLoyaltyProgressBarWidth(trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal)}" title="$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal}"></div>
								</div>
								<div class="layer2">
									<span class="text-style-1">$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold}</span>
								</div>
								<br style="clear: left;" />
							</div>
							{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc}}
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc > trJsonData.thresholdKohlsMeterValue }}
									<div class="Spend-20-to-get-you" id="loyalityTrackerEarnKCCText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noEvent, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold,'', '') }}
										</span>
									</div>
								{{else}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerSpentAwayKCCText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_spend_message_10percent, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold) }}
										</span>
									</div>
								{{/if}}
							{{else}}
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc > trJsonData.thresholdKohlsMeterValue}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerEarnText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noKCC_5percent, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
										</span>
									</div>
								{{else}}
									<div class="Spend-20-to-get-you" id="loyalityTrackerSpentAwayText">
										<span>
											{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_spend_message_5percent, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
										</span>
									</div>
								{{/if}}
							{{/if}}
						{{/if}}
						</div>
					</div>
					{{/if}}
				{{/if}}
			{{/if}}
		{{/if}}
</script>

<script class="ksLoyaltyTrackerTemplateV2" id="ksLoyaltyTrackerTemplateV2" type="text/x-jQuery-tmpl">
{{if $data.spendtracker}}
	{{if spendtracker.showKohlsCashtracker}}
	<div class="spendaway_tracker_progbar_container">
            <div class="Meter-Background">
				<div class="Meter-Fill" style = "${spendtracker.widthforProgressBar}"></div>
				<div class="pointerright"></div>
            </div>
				<div class="spendawayTracker_progbar_text" style="margin-bottom:0px!important">{{html spendtracker.loyaltyMessage}}</div>
		</div>
		<div class="spendtrackerSeparator"></div>

	{{else}}
		<div class="spendaway_tracker_container">
			<div class="spendawayTracker_text_genericVersionDiv"><span class="spendawayTracker_text_genericVersion">{{html spendtracker.loyaltyMessage}}</span></div>
		</div>
	{{/if}}
{{/if}}
	{{if $data.kohlsRewardTracker}}
	{{if $data.kohlsRewardTracker.showProgressBar}}
		<div class = "spendTrackerBg_progbar">
			<div class = "spendTracker_reward_logocover_rapper">
				<img src="${$env.cncRoot}images/lpf/rewards-lockup-horizontal.png"srcset="${$env.cncRoot}images/lpf/rewards-lockup-horizontal.png" class ="spendTracker_reward_logocover" alt="Kohls Rewards Image">
			</div>
		</div>
		<div class="spendaway_tracker_progbar_container">
			<div class="Meter-Background">
				<div class="Meter-Fill" style = "${kohlsRewardTracker.widthforProgressBar}" ></div>
				<div class="pointerright"></div>
			</div>		
			<div class="spendawayTracker_progbar_text" style="margin-bottom:0px!important">
				{{html kohlsRewardTracker.loyaltyMessage}}
			</div>
			<div class="spendtrackerSeparator"></div>
		</div>
	{{else}}
		<div class="spendaway_tracker_container">
			<span class="spendawayTracker_text">{{html kohlsRewardTracker.loyaltyMessage}}</span>
			<span>
				<img class="spendTracker_RewardsImg" src="${$env.cncRoot}images/lpf/rewards-lockup-d-green.png" alt="Kohls Rewards Image" />
			</span>
		</div>
	{{/if}}
{{/if}}
</script>

<script type="text/javascript">
	var calculateDate = function(message, type, startDate, endDate){
		if((type == "hasKCCAboveThreshold" || "center") && startDate && startDate != null && endDate && endDate != null){
			var objDate = new Date(startDate),
    			locale = "en-us",
    			startMonth = objDate.toLocaleString(locale, { month: "short" });
			var objDate1 = new Date(endDate),
    			endMonth = objDate1.toLocaleString(locale, { month: "short" });
			if(startMonth == endMonth){
				return startMonth +'&nbsp'+ objDate.getDate() +'&nbsp' +'-'+'&nbsp'+objDate1.getDate();
			}else{
				return startMonth +'&nbsp'+ objDate.getDate() +'&nbsp' +'-'+'&nbsp'+ endMonth +'&nbsp'+ objDate1.getDate();
			}
		}else if(type == ("eventAboveThreshold" || "eventBelowThreshold") && startDate && startDate != null){
			var objDate = new Date(startDate),
    			locale = "en-us",
    			startMonth = objDate.toLocaleString(locale, { month: "short" });
				return startMonth +'&nbsp'+ objDate.getDate();
		}
	};
	function showLoyaltyMesage(message, type, startDate, endDate){
		var amount0, amount1, date;
		if(type == "eventAboveThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventKohlsCashEarn;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendThresholdforEventKc;
			date = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.eventEarnPeriodEndDate;
		}else if(type == "eventBelowThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendThresholdforEventKc;
		}else if(type == "miniTrackerEventBelowThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnTrackerThreshold;
		}else if(type == "miniTrackerNoEventBelowThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
		}else if(type == "hasKCCAboveThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
			date = calculateDate(message, type, startDate, endDate);
		}else if(type == "hasKCCBelowThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage;
		}else if(type == "noKCCAboveThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
		}else if(type == "noKCCBelowThreshold"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage;
		}else if(type == "delta"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKCDelta;
		}else if(type == "hasKCC"){
			amount0 = 10;
		}else if(type == "noKCC"){
			amount0 = 5;
		}else if(type == "header1"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnNonKcc;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
		}else if(type == "header2"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
		}else if(type == "header3"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnNonKcc;
		}else if(type == "header4"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc;
		}else if(type == "center"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate;
			date = calculateDate(message, type, startDate, endDate);
		}else if(type == "bottom"){
			amount0 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal;
			amount1 = trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold;
		}

		if(date && (date != null || date != undefined)){
			return  (message.replace('{0}', amount0).replace('{1}', amount1).replace('{2}', date));
		}else if((amount0 && amount0 != null) || (amount1 && amount1 != null)){
			return  (message.replace('{0}', amount0).replace('{1}', amount1));
		}else{
			return	(message);
		}	
	}
</script>

<script id="preQualTemplateEmptyBag" type="text/x-jQuery-tmpl">
	<div class="preQualTemplate preQualCheck">
	    {{if !$env.isKioskEnabled }}
    	   {{tmpl "#prequalTemplate"}}
		{{/if}}
	</div>
</script>

<script id="orderSummaryCheckoutTemplate" type="text/x-jQuery-tmpl">
	<div class="ship_order_summary">
		<div class="order_summary_bg"><h2>${trLabels.kls_static_tr_cart_orderSummary}</h2></div>
		<div class="clearfix">
			<label for="subtotal" class="normalfont">${trLabels.kls_static_tr_cart_merchandise}</label>
			<div id="subtotal">${addCurrency(merchandisePromoAmount)}</div>
		</div>
		{{if taxFeeEnabled}}
			{{if totalGiftWrapAmount}}
				{{if totalGiftWrapAmount > 0}}
					<div class="clearfix">
						<label for="subtotal" class="normalfont">${trLabels.kls_static_tr_cart_giftwrap}</label>
						<div class="rightboldedsubtotal">${addCurrency(totalGiftWrapAmount)}</div>
					</div>
				{{/if}}
			{{/if}}
		{{/if}}	
		{{if confirmOrder}}
			{{each kohlsCashPGs}}
				{{if amountApplied }}		
					<div class="clearfix kohlscash_margin">
						<label class="ordersummary_kohlscash">	
							${trLabels.kls_static_tr_checkout_gift_kohls_cash} x${couponNumber.substr(couponNumber.length-4,4)}				
						</label>
						<div id="kohlscashdiscounts" class="fright discount_value"> -${amountApplied}</div>
					</div>
				{{/if}}	
			{{/each}}
		{{else}}
			{{! if kohlsCash}}
				<div class="clearfix kohlscash_margin">
					<label class="ordersummary_kohlscash">					
						<a class="kohlscashapplylink" href="/checkout/v2/includes/kohlsCash.jsp">${trLabels.kls_static_tr_cart_kohls_cash_and_discounts}</a>					
					</label>
					<div id="kohlscashdiscounts" class="fright discount_value"> - {{if totalKohlsCashAndPromoDiscounts != "0" }}${addCurrency(totalKohlsCashAndPromoDiscounts)}{{/if}}</div>
					<div class="fleft kohlsCashContainer {{if trJsonData.fromShoprunner}}align_apply_button{{/if}}">
						<a class="kohlscashapply" href="/checkout/v2/includes/kohlsCash.jsp">{{html convertHTML(trLabels.kls_static_tr_checkout_payment_apply)}}</a>
					</div>
				</div>
		{{/if}}			
		{{if confirmOrder}}
			{{each promoCodes}}
				{{if name && offerType != "shippingOffers"}}					
					{{if value }}	
						{{if offerType}}	
						<div class="clearfix kohlscash_margin">		
							<label class="ordersummary_kohlscash">
								${name}
							</label>
							<div id="kohlscashdiscounts" class="fright discount_value"> ${value}</div>
						</div>
						{{/if}}
					{{/if}}					
				{{/if}}
			{{/each}}
		{{/if}}	
		<div class="clearfix">
			{{! removed  !trJsonData.fromShoprunner if condition }}

				{{if confirmOrder}}
					<label for="shipcharges" style="width:140px;">
						${trLabels.kls_static_tr_cart_shippingFee}
					</label>
				{{else}}
					<label for="shipcharges" class="freeShipSuggestMessage">
						<a class="shipSurchargeGiftTax" href="#giftsTax">${trLabels.kls_static_tr_cart_shippingFee}</a>
						<!--Start :Mingle user story 1950 -->
							{{if shippingInfo && isEliteFreeShipping() === false}}
								{{if shippingInfo && shippingInfo.shipmentPriceInfo && shippingInfo.shipmentPriceInfo.freeShipQualificationDelta > 0 }}
									{{if trJsonData && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser}}
										<span class="freeShipMessFirst">${addCurrency(shippingInfo.shipmentPriceInfo.freeShipQualificationDelta)}</span>
										<span>${trLabels.kls_static_tr2_away_from_suggest_msg_order_summary_page}</span>
										<span class="freeShipMesslast">${trLabels.kls_static_tr2_free_shipping_suggest_msg_order_summary_page}</span>
									{{else}}
									<div class="freeShipMessFirstV2Os">${addCurrency(shippingInfo.shipmentPriceInfo.freeShipQualificationDelta)}</div>
									<div class="freeShipMessMidV2Os">${trLabels.kls_static_tr2_away_from_suggest_msg_order_summary_page}</div>
									<div class="freeShipMesslastV2Os">${trLabels.kls_static_tr2_free_shipping_suggest_msg_order_summary_pageV20s}</div>
								{{/if}}
								{{/if}}
							{{/if}}
						<!--End :Mingle user story 1950 -->
						</label>
				{{/if}}

			{{! removed  !trJsonData.fromShoprunner &&  if condition }}

			{{! removed if  shippingInfo && shippingInfo.shipmentPriceInfo condition }}

			{{if discountedShippingAmount == 0}}
				{{if isEliteFreeShipping()}}
					<div id="shipcharges" class="free_ship eliteFreeShipping">
					<!--KOSPC-18960 for adding mvc image-->
					{{if $env.ksLoyaltyV2}}
					<img alt="MVC" src="${$env.cncRoot}images/lpf/mvc-color.png" class="purple" />
					<span class='free_mvc'>${trLabels.kls_static_tr2_pb_free_shipping_label}</span>					
					{{else}}
						<img alt="Elite" src="${$env.cncRoot}images/elite/purple.png" class="purple" />
						<span class='free'>${trLabels.kls_static_tr2_pb_free_shipping_label}</span>
						{{/if}}
					</div>
				{{else}}
				{{if trJsonData && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser}}
					<div id="shipcharges" class="free_ship">${trLabels.kls_static_tr2_pb_free_shipping_label}</div>
				{{else}}	
					<div id="shipcharges" class="freeShippingFreeTextBlackOS">${trLabels.kls_static_tr2_pb_free_shipping_label}</div>
				{{/if}}
				{{/if}}
				{{if cartHasShipItem(trJsonData.fulfillmentTypes)}}
				{{if trJsonData && trJsonData.purchaseEarnings && !trJsonData.purchaseEarnings.loyaltyPilotUser}}
					<div class= "freeShippingMsgfreeV2Os">${trLabels.kls_static_you_earned_free_shipping}</div>
				{{/if}}
				{{/if}}
			{{else}}
				<div id="shipcharges">${addCurrency(discountedShippingAmount)}</div>
			{{/if}}
		</div>
		{{! removed  !trJsonData.fromShoprunner if condition }}
			{{if confirmOrder}}
				{{if totalSurcharges != 0}}
					<div class="clearfix">
						<label for="surcharges">
							${trLabels.kls_static_tr_confirm_surcharges}
						</label>
						<div id="surcharges">${addCurrency(totalSurcharges)}</div>
					</div>
				{{/if}}
				{{if totalGiftWrapAmount}}
					{{if totalGiftWrapAmount != 0 }}
						<div class="clearfix">
							<label for="surcharges">
								{{html convertHTML(trLabels.kls_static_tr_cart_giftwrap)}} (${giftWrapBoxCount} box)
							</label>
							<div id="surcharges">${totalGiftWrapAmount}</div>
						</div>
					{{/if}}
				{{/if}}
				<div class="clearfix">
					<label for="surcharges">
						${trLabels.kls_static_tr_confirm_tax}
					</label>
					<div id="surcharges">${tax}</div>
				</div>
			{{else}}
				<div class="clearfix">
					<label for="surcharges">
						<a class="shipSurchargeGiftTax" href="#giftsTax">${surchargesGiftsTaxLabel}</a>						
					</label>
					<div id="surcharges">${addCurrency(totalTax)}</div>
				</div>
			{{/if}}

		<div class="clearfix totalcharges_margin {{if $env.ksLoyaltyV2}}totalcharges_line_top{{else}}totalcharges_line{{/if}}">
			<label for="totalcharges"><h2 class="order_bldtxt">${trLabels.kls_static_tr_cart_total}</h2></label>
			<div class="total_valuesize" id="totalcharges">${addCurrency(total)}</div>
		</div>
		{{! removed  !trJsonData.fromShoprunner if condition }}

			<div class="clearfix amnt_applied_confirm" id="amnt_applied_confirm">
				{{if paymentTypes}}
					{{if paymentTypes.giftCards}}
						{{each paymentTypes.giftCards}}
							{{if applied}}
								<div class="clearfix">
									<label for="surcharges">	
										{{if giftCardNum}}					
											${trLabels.kls_static_tr_confirm_amount_applied_to_GC}x${giftCardNum.substr(giftCardNum.length-4,4)}
										{{/if}}			
									</label>
									<div id="surcharges">
										${addCurrency(appliedAmount)}
									</div>
								</div>
							{{/if}}
						{{/each}}
					{{/if}}
					{{if paymentTypes.creditCards}}
						{{each paymentTypes.creditCards}}
							{{if appliedAmount}}							
								<div class="clearfix">
									<label for="surcharges" style="width:260px;">	
										{{if cardNum}}					
											${trLabels.kls_static_tr_confirm_amount_applied}
											{{if type.toLowerCase() == 'discover'}}
												${trLabels.kls_static_checkout_payment_ccard_discover}
											{{else type.toLowerCase() == 'americanexpress' || type.toLowerCase() == 'amex'}}
												${trLabels.kls_static_checkout_payment_ccard_amex}
											{{else type.toLowerCase() == 'visa'}}
												${trLabels.kls_static_checkout_payment_ccard_visa}
											{{else type.toLowerCase() == 'mastercard'}}
												${trLabels.kls_static_checkout_payment_ccard_master}
											{{else type.toLowerCase() == 'kohlscharge'}}
													${trLabels.kls_static_checkout_payment_ccard_klscharge}
											{{else}}
													${type}
											{{/if}} x${cardNum.substr(cardNum.length-4,4)}
										{{/if}}			
									</label>
									<div id="surcharges" style="width:70px;">
											${addCurrency(appliedAmount)}
									</div>
								</div>			
							{{/if}}					
						{{/each}}
					{{/if}}
				{{/if}}
			</div>	
	</div>
</script>

<script id="orderSummaryTemplatenewconfirm" type="text/x-jQuery-tmpl">
	<div class="ship_order_summary">
		<div class="orderconfirmationcontent">
			<div class="order_summary_bg">
				<h2>${trLabels.kls_static_tr_cart_orderSummary}</h2>
			</div>
			<div class="clearfix">
				<label for="subtotal" class="normalfont"><b>${trLabels.kls_static_tr_cart_merchandise}</b></label>
				<div id="subtotal">${addCurrency(merchandisePromoAmount)}</div>
			</div>

			{{if taxFeeEnabled}}
				{{if totalGiftWrapAmount}}
					{{if totalGiftWrapAmount > 0}}
						<div class="clearfix">
							<label for="subtotal" class="normalfont">${trLabels.kls_static_tr_cart_giftwrap}</label>
							<div class="rightboldedsubtotal">${addCurrency(totalGiftWrapAmount)}</div>
						</div>
					{{/if}}
				{{/if}}
			{{/if}}
			{{if trJsonData.paymentDetails}}
				{{if trJsonData.paymentDetails.kohlsCashDetails}}
					{{if trJsonData.paymentDetails.kohlsCashDetails.length > 0}}
						{{each trJsonData.paymentDetails.kohlsCashDetails}}
							{{if kohlsCashAmountUsed }}		
								<div class="clearfix kohlscash_margin">
									<label class="ordersummary_kohlscash">	
										${trLabels.kls_static_tr_checkout_gift_kohls_cash}${kohlsCashCouponNumber}				
									</label>
									<div id="kohlscashdiscounts" class="fright discount_value"> ${kohlsCashAmountUsed}</div>
								</div>
							{{/if}}	
						{{/each}}
					{{/if}}
				{{/if}}
			{{/if}}
			{{if confirmOrder}}
				{{if promoCodes}}
					{{if promoCodes.length > 0}}
						{{each promoCodes}}
							{{if name}}					
								{{if value }}	
									<div class="clearfix kohlscash_margin">		
										<label class="ordersummary_kohlscash"> ${name} </label>
										<div id="kohlscashdiscounts" class="fright discount_value"> ${value}</div>
									</div>
								{{/if}}					
							{{/if}}
						{{/each}}
					{{/if}}
				{{/if}}
			{{/if}}

			<div class="clearfix">
				{{if confirmOrder}}
					<label for="shipcharges" style="width:140px;">
						<b>${trLabels.kls_static_tr_cart_shippingFee}</b>
					</label>
				{{/if}}					
				
				{{if discountedShippingAmount == 0}}
					{{if isEliteFreeShippingGetOrderDetails()}}
					<!--KOSPC-19401 for adding mvc image-->
						<div id="shipcharges" class="free_ship eliteFreeShipping">
						{{if $env.ksLoyaltyV2}}
					<img alt="MVC" src="${$env.cncRoot}images/lpf/mvc-color.png" class="purple" />
					<span class='free_mvc'>${trLabels.kls_static_tr2_pb_free_shipping_label}</span>
					{{else}}
							<img alt="Elite" src="${$env.cncRoot}images/elite/purple.png" class="purple" />
							<span class='free'>${trLabels.kls_static_tr2_pb_free_shipping_label}</span>
							{{/if}}
						</div>
						
					{{else}}
						{{if trJsonData && trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
							<div id="shipcharges" class="free_ship">${trLabels.kls_static_tr2_pb_free_shipping_label}</div>
							{{else}}
							<div id="shipcharges" class="freeShippingFreeTextBlackOS">${trLabels.kls_static_tr2_pb_free_shipping_label}</div>
						{{/if}}
					{{/if}}
					{{if cartHasShipItem(trJsonData.fulfillmentTypes)}}
					{{if trJsonData && trJsonData.orderSummary.purchaseEarnings && !trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
						<div class="freeShippingMsgfreeV2Os fleft">${trLabels.kls_static_you_earned_free_shipping}</div>
						{{/if}}
					{{/if}}
				{{else}}
					<div id="shipcharges">${addCurrency(discountedShippingAmount)}</div>
				{{/if}}
			</div>

			<div class="clear padt10px"></div>
			{{if trJsonData.shipmentDetails}}
				{{if trJsonData.shipmentDetails.length > 0}}
					{{each trJsonData.shipmentDetails}}
					 
						<div class="shippingTax  mleft20 padt5px">
							{{if shipMethodDetails.shipMethodCode != null}}
							<!--Start: Code changes for Marketplace defect MPP-550 -->
							{{if shipMethodDetails}}
								{{if shipMethodDetails.shipMethodCode}}
									{{if shipMethodDetails.shipMethodCode !='BOPUS'}}
										{{if shipMethodDetails.shipMethodCode !='WFSTD' && shipMethodDetails.shipMethodCode !='WF2DA'}}
											{{if shipmentAddress.postalCode}}
												<span class="fleft">${getShippingDetailLabelByMethod(trJsonData.trCheckoutCartErrors,shipMethodDetails.shipMethodCode,shipMethodDetails.shipMethodName)} 
													${staticLabels.checkoutGiftToSmall} ${shipmentAddress.postalCode}</span>
											{{else}}
												<span class="fleft">${getShippingDetailLabelByMethod(trJsonData.trCheckoutCartErrors,shipMethodDetails.shipMethodCode,shipMethodDetails.shipMethodName)}</span>
											{{/if}}
										{{else}}
											{{if shipmentAddress.postalCode}}
												<span class="fleft">${getShippingDetailLabelByMethod(trJsonData.trCheckoutCartErrors,shipMethodDetails.shipMethodCode,shipMethodDetails.shipMethodName)} ${staticLabels.checkoutGiftToSmall} ${shipmentAddress.postalCode}</span>
											{{else}}
												<span class="fleft">${getShippingDetailLabelByMethod(trJsonData.trCheckoutCartErrors,shipMethodDetails.shipMethodCode,shipMethodDetails.shipMethodName)}</span>
											{{/if}}
										{{/if}}
									{{else}}
										<span class="fleft">${getShippingDetailLabelByMethod(trJsonData.trCheckoutCartErrors,shipMethodDetails.shipMethodCode,shipMethodDetails.shipMethodName)}</span>
									{{/if}}
									<!--End: Code changes for Marketplace defect MPP-550 -->
									<!-- START : BOPUSV-134 -->
									{{if shipMethodDetails.shipMethodCode !='BOPUS'}}
										{{if shipmentPriceInfo.shippingChargesAfterDiscount == 0 || shipmentPriceInfo.shippingChargesAfterDiscount == '$0.00'}}
											<span class="fright free_ship ">${staticLabels.freeShippingLabel}</span>
										{{else}}
											<span class="fright ">${addCurrency(shipmentPriceInfo.shippingChargesAfterDiscount)}</span> 
										{{/if}}
									{{else}}
										<span class="fright free_ship ">${staticLabels.freeShippingLabel}</span>
									{{/if}}   
								{{/if}}
							{{/if}}


							<!-- END : BOPUSV-134 -->

							{{if shipmentPriceInfo}}
								{{if shipmentPriceInfo.freeShipQualificationDelta > 0 }}
								<span class="fright frightShippingGreen">
									{{if trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
										${addCurrency(shipmentPriceInfo.freeShipQualificationDelta)}
										${trLabels.kls_static_tr2_away_from_suggest_msg_order_summary_page}
										${trLabels.kls_static_tr2_free_shipping_suggest_msg_order_summary_page_confirmation}
									{{else}}
										<div class="freeShipMessFirstV2Os">${addCurrency(shipmentPriceInfo.freeShipQualificationDelta)}</div>
										<div class="freeShipMessMidV2Os">${trLabels.kls_static_tr2_away_from_suggest_msg_order_summary_page}</div>
										<div class="freeShipMesslastV2Os">${trLabels.kls_static_tr2_free_shipping_suggest_msg_order_summary_page_confirmation}</div>
									{{/if}}
									</span>	
								{{/if}}
							{{/if}}
                         {{/if}}
						</div>
						<div class="clear margintop5"></div>
						{{if shipmentPriceInfo}}
							{{if shipmentPriceInfo.shippingOffers}}
								{{if shipmentPriceInfo.shippingOffers.length > 0}}
									{{each shipmentPriceInfo.shippingOffers}}
										{{if confirmationMessage && promoCode}}
											<div class="shippingTax  mleft20 padt5px">
												<span class="fleft shippingTxtGreen "> ${confirmationMessage}</span>
												<span class="float_right_free_ship_amount">${staticLabels.checkoutMinusSign} ${discountAmount}</span>
											</div>
											<div class="clear margintop5"></div>
										{{/if}}
									{{/each}}
								{{/if}}
							{{/if}}
						{{/if}}
						
					{{/each}}
				{{/if}}
			 {{/if}}
			{{if vgcFindOrNot(trJsonData.fulfillmentTypes)}}
				<div class="shippingTax  mleft20 padt5px vgc-email-space">
					<span class="fleft">${trLabels.kls_static_tr2_pb_vgc_email_label}</span>
					<span class="fright free_ship ">${staticLabels.freeShippingLabel}</span>
				</div>
			{{/if}}
			<div class="clear"></div>

			<div class="shippingTaxpad firsttab">				 
				<label for="shipcharges"><b>
				{{if tax && tax.estimated}}
					{{if totalSurcharges > 0 && totalTaxFee }}
						${staticLabels.checkoutSurFeeEstimatesTax}
					{{else  totalSurcharges > 0}}
						${staticLabels.checkoutSurTaxEstimatesTax}
					{{else  totalTaxFee}}
						${staticLabels.checkoutFeeEstimatesTax}
					{{else}}
						${staticLabels.orderConfirmEstimatedTax}
					{{/if}}
				{{else}}
					{{if totalSurcharges > 0 && totalTaxFee }}
						${staticLabels.checkoutSurFeeTax}
					{{else  totalSurcharges > 0}}
						${staticLabels.checkoutSurTax}
					{{else  totalTaxFee}}
						${staticLabels.checkoutFeeTax}
					{{else}}
						${staticLabels.checkoutTax}
					{{/if}}
				{{/if}}
				</b></label>
				<p class="fright rightboldedsubtotal"><b>${addCurrency(totalTax)}</b></p>
			</div> 

			<div class="clear"></div>

			{{if totalSurcharges > 0}}
				<div class="shippingTaxpad mtop8 mleft20">
					<p class="fleft">${staticLabels.checkoutGiftShippingSurcharges}</p>
					<p class="fright">${addCurrency(totalSurcharges)}</p>
				</div> 
			{{/if}}

			<div class="clear"></div>		
			{{if trJsonData.shipmentDetails}}
				{{if trJsonData.shipmentDetails.length >0}}
					{{each trJsonData.shipmentDetails}}
						{{if itemsDetails}}
							{{if itemsDetails.itemStatus}}
								{{if itemsDetails.itemStatus.length >0}}
									{{each itemsDetails.itemStatus}}
										{{if lineItems}}{{if lineItems.length >0}}
											{{each lineItems}}
												{{if taxFee}}{{if taxFee.length >0}}
													{{each taxFee}}
													<div class="taxfeecontainer mleft20 mtop10">		
														<div class="clear"></div>
														<div class="shippingTax" >	
															<div class="descitydata">
																<span class="fleft">${feeDescription}</span>
															</div>		
															<span class="fright">${addCurrency(feeAmount)}</span>
														</div>
													</div>
													{{/each}}
												{{/if}}{{/if}}
											{{/each}}
										{{/if}}{{/if}}											
									{{/each}}
								{{/if}}
							{{/if}}
						{{/if}}
					{{/each}}
				{{/if}}
			{{/if}}		
		<div class="clear"></div>

			<div class="taxfeecontainer mtop8 mleft20 mbot_15">
					<div class="shippingTax">	
					<span class="fleft">${staticLabels.checkoutsalesTax}</span></span>	
					<span class="fright">${addCurrency(salesTax)}</span>
				</div> 
			</div>

			<div class="clearfix totalcharges_margin totalcharges_line">
				<label for="totalcharges"><h2 class="order_bldtxt">${trLabels.kls_static_tr_cart_total}</h2></label>
				<div class="total_valuesize" id="totalcharges">${addCurrency(total)}</div>
			</div>
					
			{{if tax && tax.estimated}}
				<div class="clearfix amnt_applied_confirm" id="amnt_applied_confirm">
					<span style="width:260px;">
					${staticLabels.taxwareDown}
					</span>
				</div>
			{{/if}}			
			<div class="clearfix amnt_applied_confirm" id="amnt_applied_confirm">
				{{if trJsonData.paymentDetails}}
					{{if trJsonData.paymentDetails.giftCardDetails}}
						{{if trJsonData.paymentDetails.giftCardDetails.length > 0}}
							{{each trJsonData.paymentDetails.giftCardDetails}}
								{{if giftCardAmountUsed}}
									<div class="clearfix mtop8">
										<label for="surcharges">	
											{{if giftCardNumber}}					
												${trLabels.kls_static_tr_confirm_amount_applied_to_GC}${giftCardNumber}
											{{/if}}			
										</label>
										<div id="surcharges">
											${giftCardAmountUsed}
										</div>
									</div>
								{{/if}}
							{{/each}}
						{{/if}}
					{{/if}}
					{{if trJsonData.paymentDetails.creditCardDetails}}
						{{if trJsonData.paymentDetails.creditCardDetails.length > 0}}
							{{each trJsonData.paymentDetails.creditCardDetails}}		
								{{if paymentAmount}}						
									<div class="clearfix mtop8">
										<label for="surcharges" style="width:260px;">	
											{{if cardNumber}}					
												${trLabels.kls_static_tr_confirm_amount_applied}
												{{if paymentType.toLowerCase() == 'discover'}}
													${trLabels.kls_static_checkout_payment_ccard_discover}
												{{else paymentType.toLowerCase() == 'americanexpress' || paymentType.toLowerCase() == 'amex'}}
													${trLabels.kls_static_checkout_payment_ccard_amex}
												{{else paymentType.toLowerCase() == 'visa'}}
													${trLabels.kls_static_checkout_payment_ccard_visa}
												{{else paymentType.toLowerCase() == 'mastercard'}}
													${trLabels.kls_static_checkout_payment_ccard_master}
												{{else paymentType.toLowerCase() == 'kohlscharge'}}
													${trLabels.kls_static_checkout_payment_ccard_klscharge}
												{{else}}
													${paymentType}
												{{/if}}${cardNumber}
											{{/if}}			
										</label>
										<div id="surcharges" style="width:70px;">
											${paymentAmount}
										</div>
									</div>	
								{{/if}}							
							{{/each}}
						{{/if}}
					{{/if}}	
				{{/if}}		
			</div>
		</div>
	</div>
</script>

<script class="purchaseEarningsTemplate" id="purchaseEarningsTemplate" type="text/x-jQuery-tmpl">
	<!-- START - Code for the mingle story 44,46,2157 TR-Phase 2 -->
	{{if confirmOrder }}
		{{if purchaseEarnings}}
			<div class="tr_phase2_purchase_earnings_box_style ">
				<div class="order_summary_bg">
					<h2>${trLabels.kls_static_tr2_purc_earn_label}</h2>
					<!-- Starts - Code changes for the mingle story 216 in marketplace -->
					{{if orderContainsMPItem}}
						{{tmpl "#purchaseEarningTooltipTmpl"}}
					{{/if}}
					<!-- Ends - Code changes for the mingle story 216 in marketplace -->
				</div>
				<div class="purchase_section">
					<div class="clear">
						<div class="img_block">
							<img src="_{trLabels.kls_static_tr2_pe_kohls_cash_img}" alt="Cash" width="45" height="23" align="middle">
						</div>
						<div class="fl">
							<div class="img_rgtblock tr_phase2_purchase_earnings_box_padding_adjust">
								<div class="fl">
									<b><p>${trLabels.kls_static_tr2_pe_kohls_cash}</p></b>
								</div> 
								<div class="fr">
									<span class="big_dollor_txt">${addCurrency(purchaseEarnings.kohlsCashEarned)}</span>
								</div>
							</div> 				
						</div> 
					</div>
				</div>
				{{if purchaseEarnings.kohlsRewardsEarned && !$env.ksLoyaltyFullLaunchEnabled && trJsonData && ((trJsonData.userData && trJsonData.userData.isEnrolledUser) || (trJsonData.userLoyaltyOptions && (trJsonData.userLoyaltyOptions.isEnrolledUser || trJsonData.userLoyaltyOptions.loyaltyId)))}}
					<div class="purchase_section">
						<div class="clear">
							<div class="img_block">
								<img src="_{trLabels.kls_static_tr2_pe_kohls_rewards_img}" alt="Rewards" width="45" height="23" align="middle">
							</div>
							<div class="fl">
								<div class="img_rgtblock tr_phase2_purchase_earnings_box_padding_adjust">
									<div class="fl">
										<b><p>${trLabels.kls_static_tr2_pe_kohls_rewards}</p></b>
									</div> 
									<div class="fr">
										<span class="big_dollor_txt">${parseInt(purchaseEarnings.kohlsRewardsEarned)} ${trLabels.kls_static_tr2_pe_potential_pts}</span>
									</div>
								</div> 				
							</div> 
						</div>
					</div>
				{{/if}}
			</div>
		{{/if}}
	{{else}}
		{{if kohlsCash}}
			{{if trJsonData.userData && !$env.ksLoyaltyFullLaunchEnabled && (trJsonData.userData.enrollLoyalty || trJsonData.userData.isEnrolledUser || $env.userEnrollLoyalty)}}
				<div class="tr_phase2_purchase_earnings_box_style">
					<div class="order_summary_bg">
						<h2>${trLabels.kls_static_tr2_purc_earn_label}</h2>
						<!-- Starts - Code changes for the mingle story 216 in marketplace -->
						{{if orderContainsMPItem}}
							{{tmpl "#purchaseEarningTooltipTmpl"}}
						{{/if}}
						<!-- Ends - Code changes for the mingle story 216 in marketplace -->
					</div>
					<div class="purchase_section">
						<div class="clear">
							<div class="img_block"><img src="_{trLabels.kls_static_tr2_pe_kohls_cash_img}" alt="Cash" width="45" height="23" align="middle"> </div>
							<div class="fl">
								<div class="img_rgtblock">
									<div class="fl">
										<p>${trLabels.kls_static_tr2_pe_kohls_cash}</p>
									</div>
									<div class="fr">
										<span class="big_dollor_txt">${addCurrency(kohlsCash.currentPurchaseEarnAmount)}</span>
									</div>
								</div>
								<div class="clear">
									<p>${trStaticKeys.only} <span class="small_dollor_txt">${addCurrency(kohlsCash.awayAmount)}</span>${trStaticKeys.kohlsCashPartOne} ${addCurrencyNoDec(kohlsCash.earnAmount)} ${trStaticKeys.kohlsCashPartTwo}</p>
								</div>
							</div>							
						</div>
					</div>
					<!-- Removed check for service object-->
					<div class="purchase_section">
						<div class="clear">
							<div class="img_block">
								<img src="_{trLabels.kls_static_tr2_pe_kohls_rewards_img}" alt="Rewards" width="45" height="23" align="middle">
							</div>
							<div class="fl">
								<div class="img_rgtblock">
									<div class="fl">
										<p>${trLabels.kls_static_tr2_pe_kohls_rewards}</p>
									</div>
									<div class="fr">
										<span class="big_dollor_txt">${rewards.currentPurchaseEarnPoint}${trLabels.kls_static_tr2_pe_potential_pts}</span>
									</div>
								</div>
								<div class="clear">
									<p>${trStaticKeys.only} <span class="small_dollor_txt">${rewards.awayPoints} ${trStaticKeys.points}</span>${trStaticKeys.nextReward}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			{{else}}		
				<div class="tr_phase2_purchase_earnings_box_style">
					<div class="order_summary_bg">
						<h2>${trLabels.kls_static_tr2_purc_earn_label}</h2>
						<!-- Starts - Code changes for the mingle story 216 in marketplace -->
						{{if orderContainsMPItem}}
							{{tmpl "#purchaseEarningTooltipTmpl"}}
						{{/if}}
						<!-- Ends - Code changes for the mingle story 216 in marketplace -->
					</div>
					<!--Removed Yes2You rewards block below -->
					<div class="purchase_section">
						<div class="clear">
							<div class="img_block"><img src="_{trLabels.kls_static_tr2_pe_kohls_cash_img}" alt="Cash" width="45" height="23" align="middle"> </div>
							<div class="fl">
								<div class="img_rgtblock">
									<div class="fl">
										<p>${trLabels.kls_static_tr2_pe_kohls_cash}</p>
									</div> 
									<div class="fr">
										<span class="big_dollor_txt">${addCurrency(kohlsCash.currentPurchaseEarnAmount)}</span>
									</div>
								</div> 
								<div class="clear">
									<p>${trStaticKeys.only} <span class="small_dollor_txt">${addCurrency(kohlsCash.awayAmount)}</span>${trStaticKeys.kohlsCashPartOne} ${addCurrencyNoDec(kohlsCash.earnAmount)} ${trStaticKeys.kohlsCashPartTwo}</p>
								</div>					
							</div> 
						</div>
					</div>
				</div>		   
			{{/if}}
		{{else}}
			{{if rewards.loyaltyId && !$env.ksLoyaltyFullLaunchEnabled}}
				<div class="tr_phase2_purchase_earnings_box_style">
					<div class="order_summary_bg">
						<h2>${trLabels.kls_static_tr2_purc_earn_label}</h2>
						<!-- Starts - Code changes for the mingle story 216 in marketplace -->
						{{if orderContainsMPItem}}
							{{tmpl "#purchaseEarningTooltipTmpl"}}
						{{/if}}
						<!-- Ends - Code changes for the mingle story 216 in marketplace -->
					</div>
					<div class="purchase_section">
						<div class="clear">
							<div class="img_block"><img src="_{trLabels.kls_static_tr2_pe_kohls_rewards_img}" alt="Rewards" width="45" height="23" align="middle"> </div>
							<div class="fl">
								<div class="img_rgtblock">
									<div class="fl">
										<p>${trLabels.kls_static_tr2_pe_kohls_rewards}</p>
									</div>
									<div class="fr">
										<span class="big_dollor_txt">${rewards.currentPurchaseEarnPoint}${trLabels.kls_static_tr2_pe_potential_pts}</span>
									</div>
								</div>
								<div class="clear">
									<p>${trStaticKeys.only} <span class="small_dollor_txt">${rewards.awayPoints} ${trStaticKeys.points}</span>${trStaticKeys.nextReward}</p>
								</div> 
							</div>
						</div> 
					</div>
				</div>
			{{else}}
				<div class="tr_phase2_purchase_earnings_box_style">
					<div class="order_summary_bg">
						<h2>${trLabels.kls_static_tr2_purc_earn_label}</h2>
						<!-- Starts - Code changes for the mingle story 216 in marketplace -->
						{{if orderContainsMPItem}}
							{{tmpl "#purchaseEarningTooltipTmpl"}}
						{{/if}}
						<!-- Ends - Code changes for the mingle story 216 in marketplace -->
					</div>
				</div>
			{{/if}}
		{{/if}}
	{{/if}}
	<div class="ordersummary_yoursavings">
		<!-- START: COTPE-433 -->
		<span>${trLabels.kls_static_tr_cart_total_savings}</span>
		<!-- END : COTPE-433 -->
		<span class="total_valuesize">${addCurrency(savings)}</span>
	</div>
	<!-- END - Code for the mingle story 44,46,2157 TR-Phase 2 -->		
</script>				

<!-- Piyush : Order Confirmation-->		
<script class="purchaseEarningsLoyaltyTemplate" id="purchaseEarningsLoyaltyTemplate" type="text/x-jQuery-tmpl">
	{{if trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
			<div class="TMB---Event-Threshold Clip-85" id="kls_loyality_tracker" title="Kohls Rewards">
				{{if trJsonData.orderSummary.purchaseEarnings.loyaltySystem}}
				<div class="im_background">
						<div class="Kohls-Rewards-Horizontal-Lockup" id="loyalityRewardImg"></div>
						<div>
							<div class="layer1">
								<span class="text-style-1">${trLabels.kls_static_loyalty_KCC_meter_start_amount}</span>
							</div>
							<div class="Rectangle-25">
								<div class="im_green-bar" style="${Kjs.trOrderSummary.calculateLoyaltyProgressBarWidth(trJsonData.orderSummary.purchaseEarnings.earnTrackerThreshold, trJsonData.orderSummary.purchaseEarnings.updatedEarnTrackerBal)}" title="$${trJsonData.orderSummary.purchaseEarnings.updatedEarnTrackerBal}"></div>
							</div>
							<div class="layer2">
								<span class="text-style-1">$${trJsonData.orderSummary.purchaseEarnings.earnTrackerThreshold}</span>
							</div>
							<br style="clear: left;" />
						</div>	
						<div class="Spend-20-to-get-you" id="loyalityTrackerSpentAwayEventText">
							<span>
								{{if trJsonData.orderSummary.purchaseEarnings.hasKcc}}							
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_hasKCC, trJsonData.orderSummary.purchaseEarnings.spendAwayEveryday, trJsonData.orderSummary.purchaseEarnings.earnTrackerThreshold,'','','orderConfirm')}}
								{{else}}
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_noKCC, trJsonData.orderSummary.purchaseEarnings.spendAwayEveryday, trJsonData.orderSummary.purchaseEarnings.earnTrackerThreshold,'','','orderConfirm')}}								
								{{/if}}
							</span>
						</div>
					</div>
				</div>
				{{/if}}
				<div class="Saving-Card">
					<div class="Saving-Card-Star">
						<div class="star-container">
							<div class="five-point-star1"></div>
							<div class="five-point-star2"></div>
							<div class="five-point-star3"></div>
							<div class="five-point-white1"></div>
							<div class="five-point-white2"></div>
							<div class="five-point-white3"></div>
						</div>
					</div>
					<div class="Saving-Card-Text">
						<span class="You-Saved">${trLabels.kls_static_tr_cart_total_savings_loyalty}</span>
						<span class="Dollars-Saving">${addCurrency(trJsonData.orderSummary.savings)}</span>
						<div>${trLabels.kls_static_tr_cart_total_savings_loyalty_text}</div>
					</div>
				</div>
	{{/if}}
</script>

<!-- LoyaltyV2 : Order Confirmation Page Start-->
<script class="purchaseEarningsLoyaltyTemplateV2" id="purchaseEarningsLoyaltyTemplateV2" type="text/x-jQuery-tmpl">
{{if trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser}}
	{{if trJsonData.orderSummary.purchaseEarnings.loyaltySystem}}
	<div class = "orderConf_Rewards_msg_container_outer" >
		<div class="orderConf_Rewards_msg_container">
			<div class = "orderConf_Rewards_logo_ocp">
				<img src="${$env.cncRoot}images/lpf/rewards-lockup-vertical.png"srcset= "${$env.cncRoot}images/lpf/rewards-lockup-vertical.png"  alt="Kohls Rewards Image">
			</div>
			<div class="orderConf_Rewards_msg_txt">
				{{html trLabels.kls_static_loyaltyv2_ocp_Rewards_msg_txt_lpfOn.replace('{0}', parseFloat(trJsonData.orderSummary.purchaseEarnings.updatedEarnTrackerBal).toFixed(2)).replace('{1}', parseFloat(trJsonData.orderSummary.purchaseEarnings.existingEarnTrackerBal).toFixed(2)).replace('{2}', parseFloat(trJsonData.orderSummary.purchaseEarnings.everydayEarn).toFixed(2))}}
			</div>
		</div>
	</div>
	<div class= "orderConf_Rewards_msg_container_border"></div>	
	<div class = "ocp_yoursavingrectangle">
			<div class="ordersummary_yoursavings_lpfV2">
				{{html trLabels.kls_static_loyaltyv2_ocp_Rewards_msg_yoursavings_lpfV2.replace('{0}',parseFloat(trJsonData.orderSummary.savings).toFixed(2))}}
			</div>		
	</div>
	{{else}}				
	<div class="orderConf_Rewards_msg_container_lpfoff">
		<div class = "orderConf_Rewards_logo_ocp_lpfoff">
			<img src="${$env.cncRoot}images/lpf/rewards-lockup-vertical.png"srcset= "${$env.cncRoot}images/lpf/rewards-lockup-vertical.png" alt="Kohls Rewards Image">
		</div>				
		<div class="orderConf_Rewards_msg_txt_lpfOff">
		{{html trLabels.kls_static_loyaltyv2_ocp_Rewards_msg_txt_lpfOff_conf}}
		</div>
	</div>
	<div class= "orderConf_Rewards_msg_container_border"></div>	
	<div class = "ocp_yoursavingrectangle">
			<div class="ordersummary_yoursavings_lpfV2">
				{{html trLabels.kls_static_loyaltyv2_ocp_Rewards_msg_yoursavings_lpfV2.replace('{0}', parseFloat(trJsonData.orderSummary.savings).toFixed(2))}}
			</div>		
	</div>
	{{/if}}
		{{/if}}
</script>
<!-- LoyaltyV2 : Order Confirmation Page End-->

<script id="ksLoyaltyEarningsTemplateV2" type="text/x-jQuery-tmpl">
	{{if showYourEarnings}}
		<div class="cashAndRewards">
		<span class="order_bldtxt">${trLabels.kls_static_loyaltyv2_title_your_earnings}</span>
		{{if $data.kohlsCashSection}}
			<div class="kccToUse">
			<br />
			<span class="earningsRewardsTitleKCC">{{html trLabels.kls_static_loyaltyv2_title_kohls_cash}}</span>
			<span class="earningsAmount"><span id="kccAmount" class="kccAmountTxt">$${kohlsCashSection.kccEarnedAmt}</span></span><br />
			<span class="earningsMessageTxt">{{html kohlsCashSection.dateRangeMsg}}</span>
			</div>
			<br />
		{{/if}}
		{{if $data.kohlsRewardsSection}}
			<div class="rewardsAdded">
			{{if !$data.kohlsCashSection || !loyaltySystem}}<br />{{/if}}
			<span class="earningsRewardsTitleRew">{{if loyaltySystem}}<a id="earningsRewardsTitleRew" class="rewardEarnedEvent">{{/if}}${trLabels.kls_static_loyaltyv2_title_kohls_rewards}{{if loyaltySystem}}</a>{{/if}}</span>
			{{if loyaltySystem}}
				<span class="earningsAmount"><span id="rewardsAmount" class="rewardsAmountTxt">$${kohlsRewardsSection.rewardsEarnedAmt}</span></span><br />
				<span id="rewardsAddedMsg" class="earningsMessageTxt">${trLabels.kls_static_loyaltyv2_rewards_added_balance}</span>
			{{else}}
				<span id="rewardsAddedMsg" class="earningsMessageTxt">${trLabels.kls_static_loyaltyv2_loyalty_down}</span>
			{{/if}}
			</div>
			<br />
		{{/if}}
		</div>
	{{/if}}
	{{if Kjs.trCommonCheckout.isCheckoutPage() && $data.miniTrackerMsg}}
		<div class="spendMessageTxt">{{html miniTrackerMsg}}</div>
	{{/if}}
</script>

<script class="ksLoyaltyEarningsTemplate" id="ksLoyaltyEarningsTemplate" type="text/x-jQuery-tmpl">
	{{if Kjs.trCommonCheckout.isCheckoutPage()}}	
		<div class="ordersummary_yoursavings">
			<!-- START: COTPE-433 -->
			<span>${trLabels.kls_static_tr_cart_total_savings}</span>
			<!-- END : COTPE-433 -->
			<span class="total_valuesize">${addCurrency(savings)}</span>
		</div>		
	{{/if}}
	{{if trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings}}		
		<div class="-Earned-Messaging">
			<fieldset class="Rectangle-5">
				<legend class="Kohls-Rewards-Horizontal-Lockup---B" title="Kohls rewards"></legend>
				<div class="Earn-5-Kohls-Cash">
					<!---------- START Adding null check ---------->
					{{if  Kjs.trCommonCheckout.getPaymentOrReviewPage()}}
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc}}
						<!----------- END Null check ---------->
							{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate || trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate == '0'}}
								{{if Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemNotReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold, '', '')}}
									{{tmpl({"inlineClass": ""}) "#tmpLoyaltySystemLinkSection"}}
								{{else}}
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noKCC__redeemNotReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
									{{tmpl({"inlineClass": "loyalty-learnmore"}) "#tmpLoyaltySystemLinkSection"}}
								{{/if}}
							{{else}}
							<!---------- START Adding null check ---------->
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate > 0}}
									{{if Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
										{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate, '', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
										{{tmpl({"inlineClass": "loyalty-learnmore"}) "#tmpLoyaltySystemLinkSection"}}
									{{else}}
										{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noKCC__redeemReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate, '',trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
										{{tmpl({"inlineClass": ""}) "#tmpLoyaltySystemLinkSection"}}
									{{/if}}
								{{/if}}
							{{/if}}
						{{/if}}
					{{else}}
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc}}
						<!----------- END Null check ---------->
							{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate || trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate == '0'}}
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc}}
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemNotReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold, '', '')}}
								{{else}}
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noKCC__redeemNotReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
								{{/if}}
								{{tmpl({"inlineClass": ""}) "#tmpLoyaltySystemLinkSection"}}
							{{else}}
							<!---------- START Adding null check ---------->
								{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate > 0}}
									{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc }}
										{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate, '', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
									{{else}}
										{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_noKCC__redeemReached_cnc, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate, '',trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
									{{/if}}
									{{tmpl({"inlineClass": ""}) "#tmpLoyaltySystemLinkSection"}}
								{{/if}}
							{{/if}}
						{{/if}}
					{{/if}}
				</div>
			</fieldset>
		</div>
		{{if Kjs.trCommonCheckout.isCheckoutPage()}}
			{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.loyaltySystem}}
				{{if Kjs.trCommonCheckout.getPaymentOrReviewPage()}}
					{{if Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
						<div class="-Mini-Tracker" id="kls_loyality_minitracker">
							<div class="Spend-XXXX-more-wi">
								{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__hasKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, '', '')}}
							</div>
						</div>
					{{else}}
						<div class="-Mini-Tracker" id="kls_loyality_minitracker">
							<div class="Spend-XXXX-more-wi">
								{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__noKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, '', '')}}
							</div>
						</div>
					{{/if}}
				{{else}}
					{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc}}
						<div class="-Mini-Tracker" id="kls_loyality_minitracker">
							<div class="Spend-XXXX-more-wi">
								{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__hasKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKccPercentage, '', '')}}
							</div>
						</div>
					{{else}}
						<div class="-Mini-Tracker" id="kls_loyality_minitracker">
							<div class="Spend-XXXX-more-wi">
								{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_loyality_minitracker_static_message__noKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, '', '')}}
							</div>
						</div>
					{{/if}}	
				{{/if}}
			{{else}}
				{{if trJsonData.purchaseEarnings.kohlsCashEarnings.earnPeriod}}
					{{if trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc}}
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent && trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent <= trJsonData.thresholdKohlsMeterValue}}
							<div class="-Mini-Tracker" id="kls_loyality_minitracker">
								<div class="Spend-XXXX-more-wi">
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_miniTracker_event_message, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.spendAwayEvent, trJsonData.purchaseEarnings.kohlsCashEarnings.eventKc.earnThresholdforEventKc)}}
								</div>
							</div>
						{{/if}}
					{{/if}}
				{{else trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc}}
					{{if Kjs.trCommonCheckout.getPaymentOrReviewPage()}}
						{{if Kjs.trCommonCheckout.isSelectedKCCPaymentType() && trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc <= trJsonData.thresholdKohlsMeterValue}}
							<div class="-Mini-Tracker" id="kls_loyality_minitracker">
								<div class="Spend-XXXX-more-wi">
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_miniTracker_noEvent_message, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
								</div>
							</div>
						{{else trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc <= trJsonData.thresholdKohlsMeterValue}}
							<div class="-Mini-Tracker" id="kls_loyality_minitracker">
								<div class="Spend-XXXX-more-wi">
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_miniTracker_noEvent_message_noKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
								</div>
							</div>
						{{/if}}
					{{else}}
						{{if trJsonData.purchaseEarnings.kohlsCashEarnings.hasKcc && trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc <= trJsonData.thresholdKohlsMeterValue}}
							<div class="-Mini-Tracker" id="kls_loyality_minitracker">
								<div class="Spend-XXXX-more-wi">
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_miniTracker_noEvent_message, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
								</div>
							</div>
						{{else trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc <= trJsonData.thresholdKohlsMeterValue}}
							<div class="-Mini-Tracker" id="kls_loyality_minitracker">
								<div class="Spend-XXXX-more-wi">
									{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_miniTracker_noEvent_message_noKCC, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.spendAwayEverydayNonKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
								</div>
							</div>
						{{/if}}
					{{/if}}
				{{/if}}
			{{/if}}
		{{/if}}		
	{{/if}}
	{{if !Kjs.trCommonCheckout.isCheckoutPage()}}	
		<div class="ordersummary_yoursavings">
			<!-- START: COTPE-433 -->
			<span>${trLabels.kls_static_tr_cart_total_savings}</span>
			<!-- END : COTPE-433 -->
			<span class="total_valuesize">${addCurrency(savings)}</span>
		</div>		
	{{/if}}
</script>
<script type="text/x-jQuery-tmpl" id="tmpLoyaltySystemLinkSection">
	<!---------- Kohl's Cash earn service call failure check ---------->
	<span class='text-style-3 ${inlineClass}'> 
		{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.loyaltySystem}}
			<a href="${trLabels.kls_static_loyalty_details_redirectURL}" target="_blank">${trLabels.kls_static_loyalty_details_link}</a>
		{{else}}
			<a  href="javascript:void(0);" class="kc-learnmore-link" title="learn more">${trLabels.kls_static_loyalty_learnMore_link}</a>
		{{/if}}
	</span>
	<!----------- END Null check ---------->
</script>
<!-- Start: KC LEARN MORE Model -->
<script type="text/x-jQuery-tmpl" id="kc_learnmore_content">
{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && !Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
	<div class="kc-learnmore-modal">
		<div class="kc-learnmore-top">
			<div class="kc-rewards-badge"></div>
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_header1, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
			<!--<h4>You could earn</h4>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc}</h5>
			<p>Kohl's cash on this purchase toward your next $${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold} kohl's cash</p>-->
		</div>
		<div class="kc-terms">${trLabels.kls_static_loyalty_learnMore_terms} <a href="${trLabels.kls_static_loyalty_details_redirectURL}" title="details" target="_blank">${trLabels.kls_static_loyalty_details_link}</a></div>
	</div>
{{/if}}
{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
<div class="kc-learnmore-modal">
		<div class="kc-learnmore-top">
			<div class="kc-rewards-badge"></div>
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_header2, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
			<!-- <h4>You could earn</h4>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc}</h5>
			<p>Kohl's cash on this purchase with a Kohl's Charge toward your next $${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold} kohl's cash</p> -->
		</div>
		<div class="kc-terms">${trLabels.kls_static_loyalty_learnMore_terms} <a href="${trLabels.kls_static_loyalty_details_redirectURL}" title="details" target="_blank">${trLabels.kls_static_loyalty_details_link}</a></div>
	</div>
{{/if}}
{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && !Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
<div class="kc-learnmore-modal">
		<div class="kc-learnmore-top">
			<div class="kc-rewards-badge"></div>
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_header3, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc)}}
			<!-- <h4>You could earn</h4>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc}</h5>
			<p>Kohl's cash on this purchase</p> -->
		</div>
		<div class="kc-learnmore-center">
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_center, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate, '', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
			<!-- <h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate}</h5>
			<span>Kohl's Cash&reg;</span>
			<p>to use {{html calculateDate(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemReached, 'hasKCCAboveThreshold', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}</p> -->
		</div>
		<div class="kc-learnmore-bottom">
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_bottom, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold)}}
			<!-- <span class="kc-plus">+</span>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal}</h5>
			<p>towards your next ${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold} Kohls's Cash</p> -->
		</div>
		<div class="kc-terms">${trLabels.kls_static_loyalty_learnMore_terms} <a href="${trLabels.kls_static_loyalty_details_redirectURL}" title="details" target="_blank">${trLabels.kls_static_loyalty_details_link}</a></div>
	</div>
{{/if}}
{{if trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate && Kjs.trCommonCheckout.isSelectedKCCPaymentType()}}
<div class="kc-learnmore-modal">
		<div class="kc-learnmore-top">
			<div class="kc-rewards-badge"></div>
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_header4, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc)}}
			<!-- <h4>You could earn</h4>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalEarnKcc }</h5>
			<p>Kohl's cash on this purchase with a Kohl's Charge</p> -->
		</div>
		<div class="kc-learnmore-center">
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_center, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate,'', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
			<!-- <h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.totalKcAmountToActivate}</h5>
			<span>Kohl's Cash&reg;</span>
			<p>to use {{html calculateDate(trLabels.kls_static_loyalty_earn_message_hasKCC_redeemReached, 'hasKCCAboveThreshold', trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}</p> -->
		</div>
		<div class="kc-learnmore-bottom">
			{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_learnMore_bottom, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionStartDate, trJsonData.purchaseEarnings.kohlsCashEarnings.totalKc.redemptionEndDate)}}
			<!-- <span class="kc-plus">+</span>
			<h5>$${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.updatedEarnTrackerBal}</h5>
			<p>towards your next ${trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.earnTrackerThreshold} Kohls's Cash</p> -->
		</div>
		<div class="kc-terms">${trLabels.kls_static_loyalty_learnMore_terms} <a href="${trLabels.kls_static_loyalty_details_redirectURL}" title="details" target="_blank">${trLabels.kls_static_loyalty_details_link}</a></div>
	</div>
{{/if}}
</script>
<!-- End: KC LEARN MORE Model -->

<script id="purchaseEarningTooltipTmpl" type="text/x-jQuery-tmpl">
	<div class="purchase_earning_tooltip" style="float:left">
		<img src="./../../../../../../../../www.kohls.com/media/images/enrollment_help.jpg" alt="help" class="purchase_earning_tooltip" />
		<div class="purchase_earning_tooltip_container">
			<div class="purchase_earning_tooltip_arrow bottomtooltip"></div>
			<div class="purchase_earning_close_tooltip"></div>
			<div class="purchase_earning_tooltipcontent">
				{{if $env.ksLoyaltyFullLaunchEnabled}}
					{{html convertHTML(trLabels.kls_static_cart_checkout_purchase_earning_tooltip_content)}}
				{{else}}
					{{html convertHTML(trLabels.mp_kls_static_cart_checkout_purchase_earning_tooltip_content)}}
				{{/if}}
			</div>
		</div>
	</div>
</script>

<script class="kccUpsellMessageTemplate" id="kccUpsellMessageTemplate" type="text/x-jQuery-tmpl">			
	<div class="KUB-1-Exact Rectangle-8" id="kls_loyality_KCCApply">
		<div class="You-could-earn-an-ad" id="loyalityKCCApplyText">
			{{if !trJsonData.purchaseEarnings.kohlsCashEarnings.loyaltySystem || (trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKCDelta===0)}}
				{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_upsell_message_LCS_down, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayNonKccPercentage, '', '')}}
			{{else}}
				{{html Kjs.commonGlobalFunctions.messageForLoyalty(trLabels.kls_static_loyalty_earn_message_delta, trJsonData.purchaseEarnings.kohlsCashEarnings.everyDayKc.everydayKCDelta)}}
			{{/if}}
		</div>
		<div class="ic-creditcard-o" id="loyalityKCCApplyImg"></div>
		<div class="Apply-Now" id="loyalityKCCApplyBtn">
			<a href="/feature/pre-qual/prequal_inquiry.jsp">Apply Now</a>
		</div>
		<br class="text-in-row" />
	</div>
</script>

<script class="kccUpsellMessageTemplateV2" id="kccUpsellMessageTemplateV2" type="text/x-jQuery-tmpl">			
<div  class="kccUpsellMsgV2outer" >
	<div  class = "kccUpsellMsgV2boxfit">
			<span  class="Take-an-extra-30-of-txtmsg">{{html trLabels.kls_static_loyaltyv2_upsell_message_shopping_cart.replace('{0}',$env.percentOffForNewKC)}}</span>
			<img  class="Kohls-Charge-Card-3D-img" src="${$env.cncRoot}images/lpf/kohls-charge-card-3-d@3x.png"srcset="${$env.cncRoot}images/lpf/kohls-charge-card-3-d@3x.png">
	</div>
	<div class="kccUpsellMsgV2boxfit-buttons" >
		  <div class="kccUpsellMsgV2-Apply-Now">
			  <a href="${trLabels.kls_static_kccUpsellMessage_redirectURL}" class="Apply-Now-text" target="_blank"><u>${trLabels.kls_static_kccUpsellMessage_text}</u></a>
		   </div>
		  <div class = "kccUpsellMsgV2-Subject-txt-outer" >
			 <span class= "kccUpsellMsgV2-Subject-to-credit-a">{{html trLabels.kls_static_loyaltyv2_upsell_msg_subtocredit_txt}}</span> 
			 <span class="kccUpsellMsgV2-Subject-to-credit-a-line"><hr></span>
		  </div>	
	</div>
	</div>
</script>

<script id="suggestedItemsTemplate" type="text/x-jQuery-tmpl">
	
		<div class="ap_tabs">
			<div id="tab-1" style="display: block;">
				<div class="contanttext">{{if trJsonData.recoSystemParameters.recoNewEnabled}} ${payload.recommendations[0].displayText} {{else}}${Kjs.trOrderSummary.getSuggestedTitle()} {{/if}}</div>
				<div class="clear"></div>
				{{each(i) payload.recommendations[0].products}}
					{{if i < 3}}
					<div class="suggestedimage">
					<a href="${Kjs.trOrderSummary.shoppingBagReplaceURL(productTitle, id)}" title="${productTitle}"><img src="_{Kjs.trOrderSummary.shoppingBagReplaceImageURL(image.url)}" alt="${productTitle}"/></a>
					{{each prices}}
						{{if isCurrentPrice}}
						{{if salePrice != null}}
						{{if salePrice.statusCode == 30}}
						{{if salePrice.minPrice != null}}
						<div class="saleprice"><!--clearance-->${trLabels.kls_static_suggested_items_clearance_price} $${Kjs.trOrderSummary.recoPriceConvertion(salePrice.minPrice)}</div>
						{{/if}}
				{{else}}
					{{if salePrice.minPrice != null}}
						<div class="saleprice"><!--sale-->${trLabels.kls_static_suggested_items_sale_price} $${Kjs.trOrderSummary.recoPriceConvertion(salePrice.minPrice)}</div>
						{{/if}}
						{{/if}}
						{{/if}}
						{{if regularPriceType == 'Original'}}
						{{if regularPrice.minPrice != null}}
							<div class="regprice"><!--Original-->${trLabels.kls_static_suggested_items_original_price} $${Kjs.trOrderSummary.recoPriceConvertion(regularPrice.minPrice)}</div>
					{{/if}}
				{{else}}
					{{if regularPrice.minPrice != null}}
						<div class="regprice"><!--regular-->${trLabels.kls_static_suggested_items_regular_price} $${Kjs.trOrderSummary.recoPriceConvertion(regularPrice.minPrice)}</div>
						{{/if}}				
						{{/if}}
						{{/if}}
						{{/each}}
						</div>
					{{/if}}
				{{/each}}
				<div class="clear"></div>
			</div>
		</div>
</script>

<script class="prequalTemplate" id="prequalTemplate" type="text/x-jQuery-tmpl">
		<div class="prequal_order_summary">
			<img src="./../../../../../../../../www.kohls.com/media/images/kohls_expert_card.png" alt="">
		    <div class="mid-section">
		    	<h3>${trLabels.kls_static_prequal_headtext}</h3>
		    	<span>${trLabels.kls_static_prequal_spantext}</span>
		        <a href="/feature/pre-qual/prequal_inquiry.jsp" class="get-started">${trLabels.kls_static_prequal_actiontext}</a>
		    </div>
		</div>
</script>
