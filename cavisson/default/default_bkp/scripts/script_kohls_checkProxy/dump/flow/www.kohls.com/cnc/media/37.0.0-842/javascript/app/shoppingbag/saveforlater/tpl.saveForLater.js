<script id="tpl-saveForLaterTemplate" type="text/x-jsrender">

<div id="saveForLaterMessages" class="msgContainer messages"></div>
{{if packageTplDataArray && packageTplDataArray.length}}
	<div class="saveForLaterItemCountDiv">
		<p class="saveForLaterItemCount">{{:saveForLaterTitle}}</p>
	</div>
	<!-- new cart item code started -->
	<div class="saveForLaterItem_box" id="{{id:'cartItems'}}">	
		{{for packageTplDataArray ~trLabels=trLabels}}
			<div class="saveForLater-cart-item-panel">				
					{{for #data.items tmpl = "tpl-cartBlockSaveitLater" /}}	
					{{if #data.isLastItem == false}}	
						<div class="cart-item-panel-hr">
							<hr/>
						</div>
					{{else}}
						<hr style="color:#ffffff"></hr>							
					{{/if}}			
			</div>
		{{/for}}
		{{if showMoreItem}}
			<div class="cart-item-panel-hr">
					<hr/>
			</div>
			<div id="{{id:'saveForLaterMoreItemLink'}}" class="saveForLaterMoreItemLink-attr">
				<input type="button" style="" name="Checkout" id="saveforlater_seemore" value="{{:trLabels.kls_more_items_text}}" class="saveForLaterMoreItemText"></input>
			</div>	
		{{/if}}
	</div>
	<!-- new cart item code ended -->

{{/if}}
</script>
<script id="tpl-cartBlockSaveitLater" type="text/x-jsrender">
	<div class="cart-item-panel-item">
		<div class="img-product-block">
			<div class="product-img">
				<a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}"> <img class="product-img-style" src="{{__data.imageUrl}}"/></a>
			</div>
		</div>
		<div class="product-block">
			<div class="product-name">
				<a href="{{if #data.productSeoURL}}{{:#data.productSeoURL}}{{else}}javascript:void(0);{{/if}}">{{:#data.productTitle}}</a>
			</div>
			<div class="product-item-detail">Color: {{: #data.color}}</div>	
			<div class="product-item-detail">Size: {{: #data.size}}</div>
			<div class="product-item-detail">SKU # {{: #data.skuId}}</div>
			{{if #data.outOfStock == false}}
				<div class="product-item-detail product-item-detail-text">{{if #data.salePriceLabel}}<span class='product-item-saleprice'> {{: #data.salePriceLabel}} {{: #data.salePriceValue}} </span> {{/if}}{{: #data.priceLabel}} {{: #data.priceValue}} </div>								
			{{/if}}	
		</div>	

		<div class="product-footer-block" id="{{id:'saveForLaterMoveToCart'}}">
			<div class="product-footer-gutter"></div>
			
			{{if #data.outOfStock == false}}
				<div class="saveForLaterPrice">{{if #data.salePriceValue}}{{: #data.salePriceValue}}{{else}}{{: #data.priceValue}} {{/if}}</div>
				<div class="saveForLaterMovetoCart"  data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-skuId="{{:#data.skuId}}"  data-prdId="{{:#data.productId}}" data-shippingMethod="{{:#data.fulfillmentType}}" data-storeName="{{:#data.storeId}}" data-itemType="{{:#data.itemType}}" data-cartItemId="{{:#data.cartItemId}}">{{: ~root.trLabels.kls_move_to_cart_text}}</div>
			{{else}}
				<div class="product-item-outofstock-msg">{{:~root.trLabels.kls_static_out_of_stock_heading}}</div>
			{{/if}}
			<div class="deleteSaveForLater" data-quantityId={{: #data.quantity != 0  ? #data.quantity : #data.quantity}} data-skuId="{{:#data.skuId}}"  data-prdId="{{:#data.productId}}" data-shippingMethod="{{:#data.fulfillmentType}}" data-storeName="{{:#data.storeId}}" data-itemType="{{:#data.itemType}}" data-cartItemId="{{:#data.cartItemId}}">{{: ~root.trLabels.kls_delete_text}}</div>
		</div>
	</div>
	<div class="cart-item-panel-error"></div>
</script>
