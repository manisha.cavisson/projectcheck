<script id="tmpl_gwpInfo_content" type="text/x-jQuery-tmpl">
	<div id="frameQV" class="overlay_wrapper clear frameQv gwp_MW_Wrapper">
		<div class="panelWrapper overlay_wrapper clear frameQv">
			<div id="gwpProdErrorMSG"></div>
			<div id="productContent" class="itamWrapper"> 
				{{if giftItems}}
					<div class="itemleft" style="border-right:none;">
						{{if giftItems.product.length > 1}}
							{{if isEditGetItem}}
								<h2><strong>${trLabels.kls_static_gift_option_select_item}</strong></h2>
							{{else}}
								<h2>${trLabels.kls_static_gift_option_your_gift}</h2> 
							{{/if}}
							<div class="gwp_MW_headerWrapper">
								<p class="fleft"></p><p class="fleft gwp_tickmark"></p><h2><b>{{html convertHTML(trLabels.kls_static_gift_with_purchase)}}</b> {{html convertHTML(trLabels.kls_static_select_your_free_gift)}}</h2><p></p>
							</div>
						{{else}}
							<h2>${trLabels.kls_static_gift_option_free_gift}</h2>
							<div class="gwp_MW_headerWrapper">
								<p class="fleft"></p><p class="fleft gwp_tickmark"></p><h2><b>{{html convertHTML(trLabels.kls_static_gift_with_purchase)}}</b> {{html convertHTML(trLabels.kls_static_select_your_free_gift)}}</h2><p></p>
							</div>
						{{/if}}
						{{each($index, item) giftItems.product}}
								
							<div class="gift_offer_tr product_gift_offer_${item.productId}">
								<!---------- Product Image, Name and price need to make Dynamic ---------->
								<div class="giftpro modal_input_label">			     								      	  			
									<div class="gproimage gift_itemBorder">						
											<img src="_{item.imageURL}" class="active" alt="" width="73" height="73"/>
											<input type="hidden" id="gwp_image_url_${item.productId}" value="${item.imageURL}"/>
									</div>	
									<ul>
										<li>${item.displayName}</li>
										<input type="hidden" id="gwp_product_title_${item.productId}" value="${item.displayName}"/>
											{{if item.regularPriceRange && item.regularPriceRange.minPrice}}	  
												<li class="gprovalue" {{if item.sku.length == 1}}style="display:none;"{{/if}}>${trLabels.kls_static_gift_option_value} : $${item.regularPriceRange.minPrice}</li>
												<input type="hidden" id="gwp_product_value_${item.productId}" value="${item.regularPriceRange.minPrice}"/>
											{{else}}
												<li class="gprovalue" {{if item.sku.length == 1}}style="display:none;"{{/if}}>>${trLabels.kls_static_gift_option_value} : $${item.salePriceRange.minPrice}</li>
												<input type="hidden" id="gwp_product_value_${item.productId}" value="${item.regularPriceRange.minPrice}"/>
											{{/if}}
											
											{{if item.availableVariantsCount && item.availableVariantsCount > 0}}
												<input type="hidden" id="selected_sku_color" value="${item.preSelectedColor}"/>	
												<input type="hidden" id="selected_sku_size" value=""/>
											{{else}}
												<input type="hidden" id="selected_sku_color" value="${item.sku[0].color}"/>	
												<input type="hidden" id="selected_sku_size" value="${item.sku[0].size}"/>
											{{/if}}
									</ul>
								</div> 
								<!----------- END Product Image, Name and price need to make Dynamic ---------->
					

							<div class="gift_offer_swatches_col fleft mrgmin25" id="gift_offer_swatches_container_${item.productId}">
								<div class="price-holder clearfix wid350">
									<div id="${item.productId}" class="prod_description1">
										<div class="selection" id="selection">
											<span class="s-color-detail"></span>
											<span class="s-waist-detail"></span>
											<span class="s-size-detail"></span>
											<span class="s-inseem-detail"></span>
										</div>

										<div class="size-color-block">
											{{if productFeatureCode === "20" || productFeatureCode === "40"}}
												<div class="colorblock">
													<div id="s-select-color-here" class="selecttext" dir="${color}">
														Select Color:
													<span class="bold" id="s-color-value-pdt">${item.preSelectedColor}</span>
													</div>
												</div>
											{{/if}}
											<div class="price-holder">
												<div class = "swatch-container-new">
												{{each item.swatchImages}}	
														{{if color}}
															{{if item.preSelectedColor == color }}
																<div id="T${item.productId}_${color}" class="swatch active" style="height:20px;width:20px;" data-skuId="${skuId}" data-color="${color}" data-img="${URL}">
															{{else}}
																<div id="T${item.productId}_${color}" class="swatch" style="height:20px;width:20px;" data-skuId="${skuId}" data-color="${color}" data-img="${URL}">
															{{/if}}
															<a class="swatch-color-pdt" id="${color}"  title="${color}" rel="${item.productId}_${color}_collectionThumbnail" style="background: url('${URL}') repeat scroll 0% 0% transparent;height:20px;width:20px;" data-img="${URL}"></a>	
																</div>
														{{else}}
															<div id="T${item.productId}_${color}" class="swatch no_inventory" style="height:20px;width:20px;" data-skuId="${skuId}" data-color="${color}">
																<a class="swatch-color-pdt" id="${color}" href="javascript:void(0);" title="${color}" rel="${item.productId}_${color}_collectionThumbnail"  style="background: url('${URL}') repeat scroll 0% 0% transparent;height:20px;width:20px;" data-img="${URL}">
																	<img src='./../../../../../../../../www.kohls.com/media/images/swatch_unavail.gif' width='22px' height='22px' />
																</a>	
															</div>
														{{/if}}
												{{/each}}
												</div>
											</div>
										</div>

										<p><div class="price-holder">
											<div id="sSelectWaistSizeBox" dir="${item.size}">
													{{if productFeatureCode === "30" || productFeatureCode === "40"}}
														<p class="margin-bot10">
															<span getStatic key="Browse_static_select_size"> ${trLabels.kls_static_gift_option_size} : </span>
															<!-- <a href="javascript:newWindow('sizeChartGuideLink')">
																sizeChartGuideText
															</a> -->
															<span class="bold" id="s-waist-size">
																<span getStatic key="Browse_static_select"> ${trLabels.kls_static_gift_option_select_size}</span>
																<span getStatic key="Browse_static_size"> </span>
															</span>
														</p>
													{{/if}}	
													</div>
													{{if item.priSizeList}}
														{{each($pind, primarySize) item.priSizeList}}	
															<div id="size-waist_T${item.productId}" class="size-waist_collectionB size-waist_T${item.productId}">
																	
																<div id="T${item.productId}_${primarySize}_waist" class="size_off_left1 border-box">
																	<a  class="size_off1 s-waist-number" id="${primarySize}" >${primarySize} </a>	
																</div>
																	
															</div>
														{{/each}}
													{{/if}}
											</div><p>
											<div class="clear"></div>	


											<p><div class="price-holder">
												{{if item.sku.length>0 && item.sku[0].secondarySize}}
													{{each($sind, secondarySize) item.secSizeList}}	
														<div id="size-waist_T${item.productId}" class="size-inseam_collectionB size-inseam_T${item.productId}">
																
															<div  id="T${item.productId}_${secondarySize}_waist"  class="size_off_left1 border-box">
																<a class="size_off1 s-waist-number" id="${secondarySize}" >${secondarySize}</a>			
															</div>
																
														</div>
													{{/each}}
												{{/if}}
											</div><p>
											<div class="clear"></div>	
									</div>
								</div>
							</div>

							{{if giftItems.product.length > 1}}		       
									<div class="gift_offer_selectchkbx" sel_productimageurl="" sel_productimageurl="" style="float: right;margin: 0 25px 0 0;">
										<form id="product_selection">
											<div align="center"><br>
												<div id="size_error" class="size_error" style="display:none;">${trLabels.kls_static_gift_option_select_size}</div>
												<!------- Need to make gwpProductId dynamic and need to define setProdIdSkuIdToBag function ------->	
												<input sel_productid="${item.productId}" {{if item.sku.length ==1 }} {{each item.sku}} sel_skuid="${skuId}"{{/each}}{{else}} sel_skuid="${item.selectedSkuId}" {{/if}} type="checkbox" name="option1" class="select_chkbx" value="" > ${trLabels.kls_static_gift_option_select}<br>								
											</div>

											<input type="hidden" id="select_product_T${item.productId}" value="${item.productId}"/>
											<!------- Need to make gwpProductId dynamic------->	

										</form>
									</div>
									<div class="clear"></div>
							{{/if}}	
							</div>
							{{if giftItems.product.length > 1}}
								<hr>
							{{/if}}	
							<input type="hidden" id="product_info_${item.productId}" value="${JSON.stringify(item)}"/>
						{{/each}}
						<div class="clear"></div>			
					</div>
	
					<div class="itemright" style="width:225px;">
						<form name="pbAddToBag-gift-form" method="post">
						{{if giftItems.product.length == 1}}	
							{{if giftItems.product[0].sku.length == 1}}
								<input type="hidden" id="selected_skuid_gift_skucode" value="${giftItems.product[0].sku[0].skuId}"/>	
							{{/if}}
						<input type="hidden" id="selected_sku_color" value="${giftItems.product[0].preSelectedColor}"/>	
						<input type="hidden" id="selected_skuid_gift_skucode" value=""/>	
						<input id="addToBagSkuId_gwpQuickView_selected" type="hidden" value="${giftItems.product[0].productId}" />
						
						{{else}}
						<input type="hidden" id="selected_skuid_gift_skucode" value=""/>	
						<input id="addToBagSkuId_gwpQuickView_selected" type="hidden" value="" />

						{{/if}}
						<input type="hidden" id="giftitemcount" value="${giftItems.product.length}"/>	
						<input type="hidden" id="selected_sku_color" value=""/>	
						<input type="hidden" id="selected_sku_size" value=""/>			
					
						<input id="gwpCommerceItemId" type="hidden" value="" />
						<input id="buyProductId" type="hidden" value="" />

						<input id="productimageurl" type="hidden" value="" />
						<input id="productskutitle" type="hidden" value="" />


						<div class="cart_checkout_container"> 
							<div class="button-container right_col">
								<div class="addtobag">								
									<div id="product_addToBag">	
										<div class="co-save-address-loader display-none" style="display: none;"><img class="submit_loader" src="./../../../../../../../../www.kohls.com/snb/media/images/ajax-loader.gif" alt="loading"></div>
										<div class="displayGftErr" style="display: none;">${trLabels.kls_static_gift_option_please_select}</div>												
										<input class="s-addtoBag" type="button" value="" data-cartItemId="${cartItemId}" data-edit="${isEditGetItem}"/>
									</div>								
								</div>								
								<a class="nplft" href="#">
									<input class="no_gift" value="{{if isEditGetItem}}No,I don't want the gift{{else}}Cancel{{/if}}" name="nogift" onclick="redirectOnClick()" type="button"/>
								</a>							
							</div>      
						</div>
						</form>
				
						<div class="clear"></div>
				
				
				
				</div>
			 {{/if}}
			</div>
		</div>
	</div>
</script>




<script id="tmpl_gwpGetItemInfo_content" type="text/x-jQuery-tmpl">
	<div id="frameQV" class="offers_overlay tr_offer_overlay" style="height:200px;">
		<div class="applied_offers applied_org_offers">
			<h2>${applied_offers_lavel}</h2>
			<div class="applied_offers_row">
				<div class="fleft">
					<p>${offerName}</p>
					<span>
						${offerString}
					</span>
				</div>
				<div class="fright">
					<span class="price_black">-${price_black}</span>
				</div>
				<div class="clear"></div>
			</div>
			<link rel="stylesheet" type="text/css" href="./../../../../../../../../www.kohls.com/media/49.0.0-9866/css/global.css" media="all">
		</div>
	</div>
</script>
