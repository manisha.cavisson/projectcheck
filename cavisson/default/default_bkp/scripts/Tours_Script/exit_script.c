/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: netstorm
    Date of recording: 05/06/2017 04:38:46
    Flow details:
    Build details: 4.1.8 (build# 1503)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
