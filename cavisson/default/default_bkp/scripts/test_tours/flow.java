/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: puja
    Date of recording: 04/09/2021 12:01:22
    Flow details:
    Build details: 4.6.0 (build# 48)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.test_tours;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_html");
        status = nsApi.ns_web_url ("index_html",
            "URL=http://10.10.30.102:8111/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9***********************{date}*************",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_html", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(8.249);

        //Page Auto splitted for Image Link 'Login' Clicked by User
        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login",
            "URL=http://10.10.30.102:8111/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=cav&password=cav&login.x=54&login.y=17&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.055);

        //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
            "URL=http://10.10.30.102:8111/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.223);

        //Page Auto splitted for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?depart=Acapulco&departDate=04-10-2021&arrive=Acapulco&returnDate=04-11-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=44&findFlights.y=17",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.169);

        //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C04-10-2021&hidden_outboundFlight_button1=001%7C0%7C04-10-2021&hidden_outboundFlight_button2=002%7C0%7C04-10-2021&hidden_outboundFlight_button3=003%7C0%7C04-10-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=93&reserveFlights.y=13",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.487);

        //Page Auto splitted for Image Link 'buyFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C04-10-2021&advanceDiscount=&buyFlights.x=70&buyFlights.y=12&.cgifields=saveCC",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/bookanother.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.94);

        //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome",
            "URL=http://10.10.30.102:8111/cgi-bin/welcome",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-US,en;q=0.9",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-US,en;q=0.9", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(38.96);

        return status;
    }
}
