/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 04/29/2019 03:40:09
    Flow details:
    Build details: 4.1.14 (build# 109)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow_1()
{
	
ns_start_transaction("index_html_1");
    ns_web_url ("index_html_1",
     "URL=http://10.10.30.41:9011/url_service/url_test.xml?RespSameAsReq=Y",
      //"URL=http://10.10.30.41:9010/tours/index.html",
        // "URL=https://10.10.30.17/test.html",
        //"URL=https://10.10.30.17/tours/index.html",
       // "URL=https://10.10.30.17/protobuftesting.xml",
        // "URL=http://10.10.70.8:9007/url_service/url_test.xml?RespSameAsReq=Y",
     // "URL=http://10.10.30.41:9010/out.html",
    // "URL=http://10.10.30.41:9010/Noida.html",
    //    "HEADER=Content-Type:application/x-protobuf",
    //   "ReqProtoFile=data_2.proto",
    //   "ReqProtoMessageType=Noida",
      //  "RespProtoFile=data_2.proto",
     //  "RespProtoMessageType=Noida",
     // "BODY=$CAVINCLUDE$=Noida.xml",
      );
    ns_end_transaction("index_html_1", NS_AUTO_STATUS);
    ns_page_think_time(2);
}
    