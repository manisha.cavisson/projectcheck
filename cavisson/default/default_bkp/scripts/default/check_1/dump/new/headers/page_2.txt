--Request 
POST https://www.googleapis.com/rpc
Host: www.googleapis.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.12 Safari/537.36
Accept-Encoding: gzip, deflate
Accept-Language: en-us
Content-Type: application/json
----
 POSTDATA {"method":"spelling.check","apiVersion":"v2","params":{"text":"cavisson","language":"en","originCountry":"USA","key":"dummytoken"}}
--Response 
HTTP/1.1 200
date: Thu, 31 Dec 2020 13:17:12 GMT
content-length: 181
server: GSE
expires: Mon, 01 Jan 1990 00:00:00 GMT
content-encoding: gzip
x-frame-options: SAMEORIGIN
pragma: no-cache
content-security-policy: frame-ancestors 'self'
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
content-type: application/json; charset=UTF-8
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
cache-control: no-cache, no-store, max-age=0, must-revalidate
status: 200
----

