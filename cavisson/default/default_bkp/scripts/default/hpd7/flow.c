/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: nibedita
    Date of recording: 03/18/2021 05:46:26
    Flow details:
    Build details: 4.6.0 (build# 32)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
	 ns_start_transaction("XML");
    ns_web_url ("XML_Page",
        "URL=http://10.10.30.41:8080/xml/Myxml.xml",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
        
    );
     ns_end_transaction("XML", NS_AUTO_STATUS);
    ns_page_think_time(0);
    
     ns_start_transaction("JSON");
     ns_web_url ("JSON_Page",
        "URL=http://10.10.30.41:8080/json/CycleBook.json",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
    );

    ns_end_transaction("JSON", NS_AUTO_STATUS);
    ns_page_think_time(0);
   printf("Value of json is --%s--",ns_eval_string("{J-son-param}"));

    
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.7:81/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(9.741);

       ns_start_transaction("AllParameter");
    ns_web_url ("AllParameter",
        "URL=http://10.10.30.41:9055/tours/index.html?{J-son-param}",
        "HEADER=XMLParameter:--xml={xml}--",
        "HEADER=JSONParameter:--J-son-param={J-son-param}--"
        BODY_BEGIN,
        "XMLParameter:--xml={xml}--
        JSONParameter:--J-son-param={J-son-param}--"
         BODY_END,
    );

    ns_end_transaction("AllParameter", NS_AUTO_STATUS);
    ns_page_think_time(0);

    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.7:81/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username={u-name}&password={{u-pass}}&login.x=56&login.y=13&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(2.437);

    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.7:81/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("plugins_win_json");
    ns_web_url ("plugins_win_json",
        "URL=https://www.gstatic.com/chrome/config/plugins_3/plugins_win.json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors"
    );

    ns_end_transaction("plugins_win_json", NS_AUTO_STATUS);
    ns_page_think_time(6.187);

    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?depart={c-ity}&departDate={date-Time}&arrive=Acapulco&returnDate=03-20-2021&numPassengers={ran-number}&seatPref=Window&seatType=Business&findFlights.x=78&findFlights.y=14",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(1.644);

    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C03-19-2021&hidden_outboundFlight_button1=001%7C0%7C03-19-2021&hidden_outboundFlight_button2=002%7C0%7C03-19-2021&hidden_outboundFlight_button3=003%7C0%7C03-19-2021&numPassengers=1&advanceDiscount=&seatType=Business&seatPref=Window&reserveFlights.x=41&reserveFlights.y=17",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(18.367);

    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?firstName={Ran-string}&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=5674+3421+1254&expDate=1139&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=000%7C0%7C03-19-2021&advanceDiscount=&buyFlights.x=81&buyFlights.y=8&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(4.29);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.7:81/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(3.091);

}
