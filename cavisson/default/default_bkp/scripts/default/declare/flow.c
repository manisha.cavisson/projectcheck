/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: nibedita
    Date of recording: 02/04/2021 01:48:21
    Flow details:
    Build details: 4.5.0 (build# 84)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
   ns_start_transaction("XML");
    ns_web_url ("XML_Page",
        "URL=http://10.10.30.41:9055/xml/file.xml",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
    );
     ns_end_transaction("XML", NS_AUTO_STATUS);
    ns_page_think_time(0);
    

     ns_start_transaction("JSON");
    ns_web_url ("JSON_Page",
        "URL=http://10.10.30.41:8080/json/CycleBook.json",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
    );

    ns_end_transaction("JSON", NS_AUTO_STATUS);
    ns_page_think_time(0);
   printf("Value of json is --%s--",ns_eval_string("{para_json}"));

    
    
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.41:9055/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426626669.png",
        "Snapshot=webpage_1612426627154.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(11.12);

    ns_start_transaction("AllParameter");
    ns_web_url ("AllParameter",
        "URL=http://10.10.30.41:9055/tours/index.html?{para_json}{xmlpara}",
        "HEADER=XMLParameter:--xmlpara={xmlpara}--",
        "HEADER=JSONParameter:--ParamJson={ParamJson}--"
        BODY_BEGIN,
        "XMLParameter:--xmlpara={xmlpara}--
        JSONParameter:--ParamJson={ParamJson}--"
         BODY_END,
    );

    ns_end_transaction("AllParameter", NS_AUTO_STATUS);
    ns_page_think_time(0);
    
    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.41:9055/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username={uname}&password={upass}&login.x=39&login.y=7&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426638485.png",
        "Snapshot=webpage_1612426638810.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(3.379);

    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.41:9055/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426642341.png",
        "Snapshot=webpage_1612426642638.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(7.792);


    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?depart=Acapulco&departDate=02-05-2021&arrive=Acapulco&returnDate=02-06-2021&numPassengers=1&seatPref=Window&seatType=Business&findFlights.x=81&findFlights.y=8",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426650612.png",
        "Snapshot=webpage_1612426650890.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(5.848);

    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C02-05-2021&hidden_outboundFlight_button1=001%7C0%7C02-05-2021&outboundFlight=button2&hidden_outboundFlight_button2=002%7C0%7C02-05-2021&hidden_outboundFlight_button3=003%7C0%7C02-05-2021&numPassengers=1&advanceDiscount=&seatType=Business&seatPref=Window&reserveFlights.x=69&reserveFlights.y=11",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426656976.png",
        "Snapshot=webpage_1612426657255.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("plugins_win_json");
    ns_web_url ("plugins_win_json",
        "URL=https://www.gstatic.com/chrome/config/plugins_3/plugins_win.json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "PreSnapshot=NA",
        "Snapshot=NA"
    );

    ns_end_transaction("plugins_win_json", NS_AUTO_STATUS);
    ns_page_think_time(9.638);

    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?firstName=Julia&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=123456&expDate=1224&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=002%7C0%7C02-05-2021&advanceDiscount=&buyFlights.x=73&buyFlights.y=10&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426691449.png",
        "Snapshot=webpage_1612426691758.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(4.227);

    ns_save_string("xy","param1");
    ns_save_string("nss","param1");
    ns_save_string("lm","param1");
    ns_start_transaction("param1");
    ns_web_url ("param1",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=1234&expDate=1224&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C02-05-2021&advanceDiscount=&buyFlights.x=52&buyFlights.y=10&.cgifields=saveCC",
        "HEADER=DeclareParameter:--param1={param1}--",

        BODY_BEGIN,
        "DeclareParameter:--param1={param1}--"
         BODY_END,
    );

    ns_end_transaction("param1", NS_AUTO_STATUS);
    ns_page_think_time(3.000);


    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.41:9055/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1612426696116.png",
        "Snapshot=webpage_1612426696387.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(3.975);

}
