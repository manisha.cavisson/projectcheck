/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: nibedita
    Date of recording: 02/10/2021 01:31:25
    Flow details:
    Build details: 4.5.0 (build# 91)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.41:8080/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
   // ns_page_think_time(5.64);


    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=hello&password=hello&login.x=43&login.y=15&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    //ns_page_think_time(1.792);

    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.41:8080/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    //ns_page_think_time(5.819);

    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=02-11-2021&arrive=Acapulco&returnDate=02-12-2021&numPassengers=1&seatPref=Window&seatType=Business&findFlights.x=84&findFlights.y=10",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
   // ns_page_think_time(3.433);

    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C02-11-2021&outboundFlight=button1&hidden_outboundFlight_button1=001%7C0%7C02-11-2021&hidden_outboundFlight_button2=002%7C0%7C02-11-2021&hidden_outboundFlight_button3=003%7C0%7C02-11-2021&numPassengers=1&advanceDiscount=&seatType=Business&seatPref=Window&reserveFlights.x=72&reserveFlights.y=18",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
   // ns_page_think_time(13.432);

    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=456378905674&expDate=1228&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=001%7C0%7C02-11-2021&advanceDiscount=&buyFlights.x=93&buyFlights.y=6&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
   // ns_page_think_time(4.101);

	ns_start_transaction("findflight_4");
    ns_web_url ("findflight_4",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=456378905674&expDate=1228&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=001%7C0%7C02-11-2021&advanceDiscount=&buyFlights.x=93&buyFlights.y=6&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight_4", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
	
  ns_start_transaction("findflight_5");
    ns_web_url ("findflight_5",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=456378905674&expDate=1228&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=001%7C0%7C02-11-2021&advanceDiscount=&buyFlights.x=93&buyFlights.y=6&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight_5", NS_AUTO_STATUS);

  ns_start_transaction("findflight_6");
    ns_web_url ("findflight_6",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=456378905674&expDate=1228&oldCCOption=&numPassengers=1&seatType=Business&seatPref=Window&outboundFlight=001%7C0%7C02-11-2021&advanceDiscount=&buyFlights.x=93&buyFlights.y=6&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("findflight_6", NS_AUTO_STATUS);

	ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.41:8080/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);

}
