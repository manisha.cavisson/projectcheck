/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: anup
    Date of recording: 11/23/2018 11:38:36
    Flow details:
    Build details: 4.1.12 (build# 190)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
	ns_start_transaction("IndexFileParameter");
	ns_web_url ("IndexFileParameter",
        "URL=http://10.10.30.41:9055/mercuryN.html",
	);
	ns_end_transaction("IndexFileParameter", NS_AUTO_STATUS);
    ns_page_think_time(0);  

     ns_start_transaction("XML");
    ns_web_url ("XML_Page",
        "URL=http://10.10.30.41:9055/xml/xmlfile.xml",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
    );

    ns_end_transaction("XML", NS_AUTO_STATUS);
    ns_page_think_time(0);
    

    ns_start_transaction("JSON");
    ns_web_url ("JSON_Page",
        "URL=http://10.10.30.41:9055/json/jsonfile.json",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
    );

    ns_end_transaction("JSON", NS_AUTO_STATUS);
    ns_page_think_time(0);
    
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.41:9055/tours/index.html",
        "HEADER=Accept-Language:en-us",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1542953272058.png",
        "Snapshot=webpage_1542953272456.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(0);

    ns_save_string("CAVISSON NOIDA","param1");
    ns_start_transaction("AllParameter");
    ns_web_url ("AllParameter",
        "URL=http://10.10.30.41:9055/tours/index.html",
        "HEADER=FileParameter:--uname={uname}--upass={upass}--",
        "HEADER=SearchParameterInHeader:--sp_header={sp_header}--",
        "HEADER=SearchParameterInBody:--sp_body={sp_body}--",
        "HEADER=SearchParameterInAll:--sp_all={sp_all}--",
        "HEADER=Date&TimeParameter:--ParamDateTime={ParamDateTime}--",
        "HEADER=RandomStringParameter:--ParamRandomString={ParamRandomString}--",
        "HEADER=RandomNumberParameter:--ParamRandonNumber={ParamRandonNumber}--",
        "HEADER=UniqueNumberParameter:--ParamUniqueNumber={ParamUniqueNumber}--",
        "HEADER=XMLParameter:--ParamXML={ParamXML}--",
        "HEADER=JSONParameter:--ParamJson={ParamJson}--",
        "HEADER=IndexFileParameter:--ParamIndexFile={ParamIndexFile}--",
        "HEADER=UniqueRangeParameter:--ParamUniqueRange={ParamUniqueRange}--",
        "HEADER=DeclareParameter:--param1={param1}--",
        "HEADER=DeclareArray:--ParamDeclareArray={ParamDeclareArray_1}--{ParamDeclareArray_2}--"
        BODY_BEGIN,
		"FileParameter:--uname={uname}--upass={upass}--
SearchParameterInHeader:--sp_header={sp_header}--
SearchParameterInBody:--sp_body={sp_body}--
SearchParameterInAll:--sp_all={sp_all}--
Date&TimeParameter:--ParamDateTime={ParamDateTime}--
RandomStringParameter:--ParamRandomString={ParamRandomString}--
RandomNumberParameter:--ParamRandonNumber={ParamRandonNumber}--
UniqueNumberParameter:--ParamUniqueNumber={ParamUniqueNumber}--
XMLParameter:--ParamXML={ParamXML}--
JSONParameter:--ParamJson={ParamJson}--
IndexFileParameter:--ParamIndexFile={ParamIndexFile}--
UniqueRangeParameter:--ParamUniqueRange={ParamUniqueRange}--
DeclareParameter:--ParamDeclare={ParamDeclare}--
DeclareArray:--ParamDeclareArray={ParamDeclareArray_1}--{ParamDeclareArray_2}--"
        BODY_END,
    );

    ns_end_transaction("AllParameter", NS_AUTO_STATUS);
    ns_page_think_time(0);
    
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.41:9055/cgi-bin/login",
        "METHOD=POST",
        "HEADER=Origin:http://10.10.30.41:9055",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-us",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "PreSnapshot=webpage_1542953285949.png",
        "Snapshot=webpage_1542953286148.png",
        BODY_BEGIN,
            "userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username={uname}&password={upass}&login.x=45&login.y=16&login=Login&JSFormSubmit=off",
        BODY_END,
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(0);

	


    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.41:9055/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1****{ParamNum}****",
        "HEADER=Accept-Language:en-us****{ParamIndexFile}****",
        "PreSnapshot=webpage_1542953291819.png",
        "Snapshot=webpage_1542953292106.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/continue.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(0);



    
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?depart=Acapulco&departDate={ParamDateTime}&arrive=Acapulco&returnDate=11-25-2018&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=69&findFlights.y=11&findFlights=Submit",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-us",
        "PreSnapshot=webpage_1542953295336.png",
        "Snapshot=webpage_1542953295579.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/startover.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/continue.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(0);
	
	//ns_save_string(ns_eval_string("{ParamXML}"),"ParamDeclareArray_2");
	
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C11-24-2018&hidden_outboundFlight_button1=001%7C0%7C11-24-2018&hidden_outboundFlight_button2=002%7C0%7C11-24-2018&hidden_outboundFlight_button3=003%7C0%7C11-24-2018&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=97&reserveFlights.y=15",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-us",
        "PreSnapshot=webpage_1542953297188.png",
        "Snapshot=webpage_1542953297430.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/startover.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(0);

    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.41:9055/cgi-bin/findflight?firstName={ParamRandomString}&lastName=Scott&address1={ParamUniqueNumber}+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C11-24-2018&advanceDiscount=&buyFlights.x=58&buyFlights.y=16&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-us",
        "PreSnapshot=webpage_1542953301851.png",
        "Snapshot=webpage_1542953302120.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/bookanother.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(0);

    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.41:9055/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-us***{ParamRandonNumber}***",
        "PreSnapshot=webpage_1542953306554.png",
        "Snapshot=webpage_1542953306752.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us", END_INLINE,
            "URL=http://10.10.30.41:9055/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(0);

}
