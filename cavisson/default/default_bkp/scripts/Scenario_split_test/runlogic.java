package com.cavisson.scripts.Scenario_split_test;

import pacJnvmApi.NSApi;

public class runlogic
{

      // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
      //Start - List of used flows in the runlogic
      //Initialise the flow class
      HPD flowObj0 = new HPD();
      //End - List of used flows in the runlogic

      public void execute(NSApi nsApi) throws Exception
      {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence blocik - Block1
        //Executing flow - HPD
        flowObj0.execute(nsApi);

        //logging
        nsApi.ns_end_session();

      }
}