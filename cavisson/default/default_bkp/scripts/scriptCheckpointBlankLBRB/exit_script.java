/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/26/2021 01:17:32
    Flow details:
    Build details: 4.6.0 (build# 73)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.scriptCheckpointBlankLBRB;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
