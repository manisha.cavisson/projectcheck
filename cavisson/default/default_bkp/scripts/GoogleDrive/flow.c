/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 04/28/2021 12:19:03
    Flow details:
    Build details: 4.6.0 (build# 57)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("Xss_1_scc_1_ltmpl_default_lt");
    ns_web_url ("Xss_1_scc_1_ltmpl_default_lt",
        "URL=https://mail.google.com/",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://mail.google.com/mail/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1#", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/googlesans/v14/4UaGrENHsxJlGDuGo1OIlL3Owp4.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/googlesans/v14/4UabrENHsxJlGDuGo1OIlLU94YtzCwY.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4WxKOzY.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu5mxKOzY.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://accounts.youtube.com/accounts/CheckConnection?pmpo=https%3A%2F%2Faccounts.google.com&v=-1951684229&timestamp=1619591418392", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=sy2h,i5dxUd,m9oV,RAnnUd,sy2d,sy2e,sy2f,uu7UOe,soHxf", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7WxKOzY.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://accounts.google.com/_/bscframe", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=__Host-GAPS", END_INLINE,
            "URL=https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=NpD4ec,SF3gsd,O8k1Cd,YLQSd,lCVo3d,o02Jie,rHjpXd,pB6Zqd,QLpTOd,otPmVb,rlNAl", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("Xss_1_scc_1_ltmpl_default_lt", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log");
    ns_web_url ("log",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:1455",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=sy35,sy36,sy37,sy39,sy3a,sy4x,pwd_view", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_2");
    ns_web_url ("log_2",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:668",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-GB"]],558,[["1619591418000",null,[],null,null,null,null,"[[null,[1,\"accounts.google.com/signin/v2/identifier\",null,[\"\"],false]],null,3,[null,\"S-2063066544:1619591418653949\"]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]],["1619591418000",null,[],null,null,null,null,"[null,null,3,[null,\"S-2063066544:1619591418653949\"],[\"Southern Asia\",null,\"/signin/v2/identifier\",3,808,1,\"IN\"]]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1619591418949",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_5");
    ns_web_url ("index_5",
        "URL=https://accounts.google.com/_/common/diagnostics/?hl=en-GB&_reqid=43284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:672",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=__Host-GAPS;NID",
        BODY_BEGIN,
            "[key: "diagnostics"
value: "[[[\"gaia_fe_minutemaid:ss\",null,null,null,1619591417956]]]"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END
    );

    ns_end_transaction("index_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("json");
    ns_web_url ("json",
        "URL=https://update.googleapis.com/service/update2/json?cup2key=9:2393694829&cup2hreq=8d31fafea5ca6cab50d8539ff2d65199ce8f859daaccd4f8f7f0efa27b3f2ff5",
        "METHOD=POST",
        "HEADER=Content-Length:3009",
        "HEADER=X-Goog-Update-AppId:gcmjkmgdlgnkkcocmoeiminaijmmjnii,oimompecagnajdejgnnjijobebaeigek,hfnkpimlhhgieaddgfemjhofmfblmnib,llkgjffcdpffmhiakmfcdcblohccpfmo,khaoiebndkojlmppeemjhbpbandiljpe,bklopemakmnopmghhmccadeonafabnal,giekcmmlnklenlaomppkphknjmnnpneh,jflookgnkcckhobaglndicnbbgbonegd",
        "HEADER=X-Goog-Update-Interactivity:bg",
        "HEADER=X-Goog-Update-Updater:chrome-79.0.3945.130",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        BODY_BEGIN,
            "{"request":{"@os":"win","@updater":"chrome","acceptformat":"crx2,crx3","app":[{"appid":"gcmjkmgdlgnkkcocmoeiminaijmmjnii","cohort":"1:bm1:","cohorthint":"M54ToM99","cohortname":"M54ToM99","enabled":true,"packages":{"package":[{"fp":"1.4dcc255c0d82123c9c4251bb453165672ea0458f0379f3a7a534dc2a666d7c6d"}]},"ping":{"ping_freshness":"{e3e49096-ec13-4a78-bb0d-c12ca3947c70}","rd":5230},"updatecheck":{},"version":"9.22.0"},{"appid":"oimompecagnajdejgnnjijobebaeigek","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.3a7afafe965da76cea59344022f69dcb9eaaef382ce56eb1daa8d20f9ec80c16"}]},"ping":{"ping_freshness":"{e0230855-fdfb-46f0-b89c-4c47ea893c79}","rd":5230},"updatecheck":{},"version":"4.10.1610.0"},{"appid":"hfnkpimlhhgieaddgfemjhofmfblmnib","cohort":"1:jcl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.94b02f6d6bbb3b971f646107d197f7c7ef7783dc4e08df76ac2968d5ad326be0"}]},"ping":{"ping_freshness":"{0c2e294e-85f0-4658-8bce-fc083e8f6ad0}","rd":5230},"updatecheck":{},"version":"6570"},{"appid":"llkgjffcdpffmhiakmfcdcblohccpfmo","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.3d885f0577e4fd9e5b9251ba18576da6b49e80870ceaafcaa996e3b1dc762c01"}]},"ping":{"ping_freshness":"{3dbb2c12-f6bb-46be-b80f-4bf8abbe458b}","rd":5230},"updatecheck":{},"version":"1.0.0.4"},{"appid":"khaoiebndkojlmppeemjhbpbandiljpe","cohort":"1:cux:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.ffd1d2d75a8183b0a1081bd03a7ce1d140fded7a9fb52cf3ae864cd4d408ceb4"}]},"ping":{"ping_freshness":"{213bbb6c-4f3b-4430-bb9d-a11dd7296629}","rd":5230},"updatecheck":{},"version":"43"},{"appid":"bklopemakmnopmghhmccadeonafabnal","cohort":"1:swl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.70497f45af368f6d591eb9b93a097b7b56821b0770ee00f04b2f5901487a0421"}]},"ping":{"ping_freshness":"{7ce9aa7d-b32c-4b0b-aae4-71fdf7471844}","rd":5230},"updatecheck":{},"version":"4"},{"appid":"giekcmmlnklenlaomppkphknjmnnpneh","cohort":"1:j5l:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.fd515ec0dc30d25a09641b8b83729234bc50f4511e35ce17d24fd996252eaace"}]},"ping":{"ping_freshness":"{c14533ed-d230-4b56-9cdf-52827810c16e}","rd":5230},"updatecheck":{},"version":"7"},{"appid":"jflookgnkcckhobaglndicnbbgbonegd","cohort":"1:s7x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.70dba5eed1c56cea56059b5555dea35cec86b8ec84172850567c7f372b41e80b"}]},"ping":{"ping_freshness":"{58dcd508-f0f6-4220-9fc0-786c4b73f201}","rd":5230},"updatecheck":{},"version":"2622"}],"arch":"x64","dedup":"cr","domainjoined":false,"hw":{"physmemory":16},"lang":"en-GB","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.19041.928"},"prodversion":"79.0.3945.130","protocol":"3.1","requestid":"{dac0a9cd-ebf7-4ce7-bb5d-41d2966c1fdd}","sessionid":"{717f4f4e-6c12-4462-b74f-01fd30573fc3}","updaterversion":"79.0.3945.130"}}",
        BODY_END
    );

    ns_end_transaction("json", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("accountlookup");
    ns_web_url ("accountlookup",
        "URL=https://accounts.google.com/_/lookup/accountlookup?hl=en-GB&_reqid=143284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:3251",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=__Host-GAPS;NID",
        BODY_BEGIN,
            "[key: "continue"
value: "https://mail.google.com/mail/"
, key: "service"
value: "mail"
, key: "rm"
value: "false"
, key: "ltmpl"
value: "default"
, key: "scc"
value: "1"
, key: "ss"
value: "1"
, key: "osid"
value: "1"
, key: "emr"
value: "1"
, key: "f.req"
value: "[\"pattanayakmanisha09@gmail.com\",\"AEThLlxTuxEf0wnMI3G2jX3tlYXZcUdyEA1_bNE2z9kyRj_SmaUIW0J7_ALGAwtuP-Wtjzajo6FA_yTZkNE3kWS0_aGoj6cp41QvUSzwP-xZSz0eXjHBP7G0jhlbJJ41syIJJMDJce-LhB0AJCvR9yTsw6eTvY7Kr00MU2bqev1zr1XwiLxH0i4SmBwTbk70e3Pr6LxaOnw9\",[],null,\"IN\",null,null,2,false,true,[null,null,[2,1,null,1,\"https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1\",null,[],4,[],\"GlifWebSignIn\",null,[]],1,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,true],\"pattanayakmanisha09@gmail.com\",null,null,null,true,true,[]]"
, key: "bgRequest"
value: "[\"identifier\",\"!19Sl1JnNAAZVgnmyVYJCG3Igfe1GyBw7ACkAIwj8RknrYqnJgi9jgAPJFh7ORU_1M-rXsjKBeJGsL4O8oQ6H3Sy-0AIAAACGUgAAAEBoAQeZBBIrlccRaSCv76Q99_i1pjd3v3ySQYDvXFR6gF1tDlXT6h1-ndHxA2YN7V6TY8QrGCkDpeNLRWL4lw0bWIYZxcUyyb8N0M0Q1wIiFNfxfFz9ekomClhIHffItfK1FpvuKXauC6eunh7CcaoaoDknmlIKs9rv3ae-vIx1_0fyflG0gDG6vKENajJ9Cq0zGmlfoUG5rf0vWz8FiQK015dW3y1muzUl9ONXsu6nacqS8BmkpcjvJ1El5cDYkuDtJ3tuMIKEIHykHcaf-RwqsgRnGyrY7KRPfHcZUYWFS-K6yM54u850zHDG0q1dD6898sCMKXttOc1S5Imtw4gYmqZTLdLMOQ254DhGBMyQCPDODWFIWjpbAlb56cUWCfjvWaieITZrgP1JzbPgShXybhMN0srZ9krlQi1T51m90jTMrSd23e65PldeSZNASbsQ6VB4MmnN0v9ISMNxk0HJKnXNVzRvlvxkITdTcMpPdnauYGoRGnXAErMkhuAH7KXVtKpB-OepuoiiQGr5r7ijaWvG1opI1HUrtGXDsMHuWuee7jT_wHus6ETEChVkchpLrOXQLf8x6LP1BNQqPoVkClrRcR2hM4tN7hZswbtTBCmYaIfrw4ZzAPS9gzrxXNCyggtLyh1ThrsQMv93ISu2KQY1GwI5dXDaoDmJk133ONG19dI-siW3qpqNGsqF_XdZrhIEUq2lc6qcMKm3xT5HlgFxvI29xcmYNeZGoUHtuF__37Lu-H8LAsJLfWFYhpcSsjEVRekgpCiJ5r38nPxLvycSJWomPdHIs7EYIQRBIF7sE1pOYrYuw5S5y2fIbFFaf4qaulJnG3kkVkvlEy0zAOvvINFkMIzIYGjXEz-RHGou9lPkE0UBulPDuCHH4QvQaUrt0uuN_CUt3HGE-hU-sJ3h5QMYEAScnd4maj7te_ebwWOb7YpBxSdV0kJuvB4C1o0EPXBh5ZVY854wBqPjw5tQwioNv-06ETsfXKYgFbV_ffuko3J52_ZUQadg4724LtP0EWBQjlKBjbptcZsnj08s7Iw3iQ64Opf9rod5zjub0wSPfwKX2BmNzcTRPfSG8r3ibylRXLXus2ANetN_FXY2PsOyswLpDch1UoHSL4PvQVnllwa75lbFa63Fik4crvc7nt5OgP5eHC888GrpmdpzswjESkgwV7xy5fcBszxaCDWZQvD4r6WoAKQHa8EG0P-HJw3GShY0pTSsB8P9u2km_zYx9W5KadG_fbCNL9f6ye3HLz-_pM8FEfpvtcBhiNAP98Y-j61F6tp9AqC6OAyTRuPiHY7QVvfYuK1p7U5P1BhffA5lovAZL7Y4RM0SVHRICDF3JPfJxGAixybtLKUwfEitYVaGO7Bj_SSG7EnP6DkRuoSs\"]"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://accounts.google.com/signin/v2/challenge/az?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin&cid=3&navigationDirection=forward&TL=AM3QAYYEM_GkAPoNzg_fbiCPBHhe3kRSzhp4YLnsiloaWhoZuy_UUPTr1AqlrDDv", END_INLINE
    );

    ns_end_transaction("accountlookup", NS_AUTO_STATUS);
    ns_page_think_time(457.421);

    //Page Auto splitted for 
    ns_start_transaction("m_sy2y_sy2z_sy2x_sy33_G1cEEc");
    ns_web_url ("m_sy2y_sy2z_sy2x_sy33_G1cEEc",
        "URL=https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=sy2y,sy2z,sy2x,sy33,G1cEEc,sy30,dFms7c,sy3d,sy3f,sy3h,sy31,sy3b,sy3e,emb,emc,sy32,sy34,ema,az_view",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/accounts/embedded/signin_googleapp_pulldown.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=IIfQad,Gspf2,QCqtlc,oWOlDb,Pu0pg,ilZBgf", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/mail/gxlu?email=pattanayakmanisha09%40gmail.com&zx=1619591870632", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=NID", END_INLINE,
            "URL=https://apis.google.com/js/base.js", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=NID", END_INLINE,
            "URL=https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4.woff2", "HEADER=Origin:https://accounts.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("m_sy2y_sy2z_sy2x_sy33_G1cEEc", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_3");
    ns_web_url ("log_3",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:411",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID"
    );

    ns_end_transaction("log_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_4");
    ns_web_url ("log_4",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:933",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID",
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/apps-static/_/js/k=oz.gapi.en_US.bSaSBnJo3mU.O/m=client/exm=base/rt=j/sv=1/d=1/ed=1/am=AQ/rs=AGLTcCOlScUDCc6laSimwcYo4nXUQAS-sQ/cb=gapi.loaded_1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=NID", END_INLINE,
            "URL=https://apis.google.com/_/scs/apps-static/_/js/k=oz.gapi.en_US.bSaSBnJo3mU.O/m=base/rt=j/sv=1/d=1/ed=1/am=AQ/rs=AGLTcCOlScUDCc6laSimwcYo4nXUQAS-sQ/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=NID", END_INLINE,
            "URL=https://content.googleapis.com/cryptauth/v1/authzen/awaittx?alt=json&key=AIzaSyCp406mobx24ph_NgrsN9Qp4wrgWJrDL54", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://accounts.google.com", "HEADER=Access-Control-Request-Headers:content-type,x-clientdetails,x-goog-encode-response-if-executable,x-javascript-user-agent,x-requested-with", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("awaittx");
    ns_web_url ("awaittx",
        "URL=https://content.googleapis.com/cryptauth/v1/authzen/awaittx?alt=json&key=AIzaSyCp406mobx24ph_NgrsN9Qp4wrgWJrDL54",
        "METHOD=POST",
        "HEADER=Content-Length:155",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=X-ClientDetails:appVersion=5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36&platform=Win32&userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36",
        "HEADER=X-Goog-Encode-Response-If-Executable:base64",
        "HEADER=Content-Type:application/json",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=X-JavaScript-User-Agent:google-api-javascript-client/1.1.0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        BODY_BEGIN,
            "{"txId":"CkcKIwgBEAIaCLnAcdrzZq8yKhA7TLfNkCoHguvDZ3gEA4GPMgEDEiClvvkUEZV3s7tqj6ugumz_Dwwe0sfiEZ4FsO7ltt7klRIgcniiwQkhvJC2iKgskO6Bmbv3a64AsFYO27h-n2DDF5I="}",
        BODY_END
    );

    ns_end_transaction("awaittx", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_6");
    ns_web_url ("index_6",
        "URL=https://accounts.google.com/_/common/diagnostics/?hl=en-GB&TL=AM3QAYYEM_GkAPoNzg_fbiCPBHhe3kRSzhp4YLnsiloaWhoZuy_UUPTr1AqlrDDv&_reqid=243284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:779",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID;__Host-GAPS",
        BODY_BEGIN,
            "[key: "diagnostics"
value: "[[[\"gaia_fe_minutemaid:signInAttempt\",null,null,null,1619591417956],[\"gaia_fe_minutemaid:identifierPage\",null,null,null,1619591418601,452030]]]"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END
    );

    ns_end_transaction("index_6", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("awaittx_2");
    ns_web_url ("awaittx_2",
        "URL=https://content.googleapis.com/cryptauth/v1/authzen/awaittx?alt=json&key=AIzaSyCp406mobx24ph_NgrsN9Qp4wrgWJrDL54",
        "METHOD=POST",
        "HEADER=Content-Length:155",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=X-ClientDetails:appVersion=5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36&platform=Win32&userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36",
        "HEADER=X-Goog-Encode-Response-If-Executable:base64",
        "HEADER=Content-Type:application/json",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=X-JavaScript-User-Agent:google-api-javascript-client/1.1.0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        BODY_BEGIN,
            "{"txId":"CkcKIwgBEAIaCLnAcdrzZq8yKhA7TLfNkCoHguvDZ3gEA4GPMgEDEiClvvkUEZV3s7tqj6ugumz_Dwwe0sfiEZ4FsO7ltt7klRIgcniiwQkhvJC2iKgskO6Bmbv3a64AsFYO27h-n2DDF5I="}",
        BODY_END
    );

    ns_end_transaction("awaittx_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("selectchallenge");
    ns_web_url ("selectchallenge",
        "URL=https://accounts.google.com/_/signin/selectchallenge?hl=en-GB&TL=AM3QAYYEM_GkAPoNzg_fbiCPBHhe3kRSzhp4YLnsiloaWhoZuy_UUPTr1AqlrDDv&_reqid=343284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:802",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID;__Host-GAPS",
        BODY_BEGIN,
            "[key: "continue"
value: "https://mail.google.com/mail/"
, key: "service"
value: "mail"
, key: "rm"
value: "false"
, key: "ltmpl"
value: "default"
, key: "scc"
value: "1"
, key: "ss"
value: "1"
, key: "osid"
value: "1"
, key: "emr"
value: "1"
, key: "f.req"
value: "[3,null,true,null,[4,null,null,null,null,null,null,null,null,[null,0]]]"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END
    );

    ns_end_transaction("selectchallenge", NS_AUTO_STATUS);
    ns_page_think_time(0.106);

    //Page Auto splitted for 
    ns_start_transaction("gxlu");
    ns_web_url ("gxlu",
        "URL=https://mail.google.com/mail/gxlu?email=pattanayakmanisha09%40gmail.com&zx=1619591915627",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;NID"
    );

    ns_end_transaction("gxlu", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_5");
    ns_web_url ("log_5",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:411",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en-GB"]],558,[["1619591915000",null,[],null,null,null,null,"[[null,[1,\"accounts.google.com/signin/v2/challenge/az\",null,[\"\"],false]],null,3,[null,\"S-2063066544:1619591418653949\"]]",null,null,null,null,null,null,-19800,null,null,null,null,[],6,null,null,null,null,null,[]]],"1619591915702",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_6");
    ns_web_url ("log_6",
        "URL=https://play.google.com/log?format=json&hasfast=true",
        "METHOD=POST",
        "HEADER=Content-Length:930",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID"
    );

    ns_end_transaction("log_6", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("awaittx_3");
    ns_web_url ("awaittx_3",
        "URL=https://content.googleapis.com/cryptauth/v1/authzen/awaittx?alt=json&key=AIzaSyCp406mobx24ph_NgrsN9Qp4wrgWJrDL54",
        "METHOD=POST",
        "HEADER=Content-Length:155",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=X-ClientDetails:appVersion=5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36&platform=Win32&userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36",
        "HEADER=X-Goog-Encode-Response-If-Executable:base64",
        "HEADER=Content-Type:application/json",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=X-JavaScript-User-Agent:google-api-javascript-client/1.1.0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        BODY_BEGIN,
            "{"txId":"CkcKIwgBEAIaCLnAcdrzZq8yKhA7TLfNkCoHguvDZ3gEA4GPMgEDEiClvvkUEZV3s7tqj6ugumz_Dwwe0sfiEZ4FsO7ltt7klRIgcniiwQkhvJC2iKgskO6Bmbv3a64AsFYO27h-n2DDF5I="}",
        BODY_END
    );

    ns_end_transaction("awaittx_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("challenge");
    ns_web_url ("challenge",
        "URL=https://accounts.google.com/_/signin/challenge?hl=en-GB&TL=AM3QAYYEM_GkAPoNzg_fbiCPBHhe3kRSzhp4YLnsiloaWhoZuy_UUPTr1AqlrDDv&_reqid=443284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:1673",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=NID;__Host-GAPS",
        BODY_BEGIN,
            "[key: "continue"
value: "https://mail.google.com/mail/"
, key: "service"
value: "mail"
, key: "rm"
value: "false"
, key: "ltmpl"
value: "default"
, key: "scc"
value: "1"
, key: "ss"
value: "1"
, key: "osid"
value: "1"
, key: "emr"
value: "1"
, key: "f.req"
value: "[\"AEThLlx0-RsBGfIEAQ57vtUfrq5i5M7Ah7RZ0-ZFP73UzbajG3DsCaUW8b8r0yMMMADEghuP12yTkey2j89FtpuD0VFhXxCK3H8G5AcilBl_gKNSmod2R082-XFyFM0As-8ZH2wtVKIoBn89K-T4U9-WJSp6HNh9xJui-p4DF9hTfkGwGXAX-CJnuP-sfLBZngIAldZeCa5RkcjmnkDWFOGYQTd9OOMgKgCJ7oXOcBuEoFlZs5M6EYwkCCxTVkNgPLY_kcpmnMSteZh3bLGxlPn5MrPhUdjzIafNWTQ0dJKTJJyqBjXVmOJCc5_gRmEtaqrFQu74vVZ5lC96Do_oLsaek388dC7bb_sQ2r2UaYo_uG4ma_DUeBW6q8J_ZXbIpTXPSla6G5CKMUEYgVcO4Crb_Iebp0IxIW2q86awJmq0afv-hAvsbCQZePagdpkBysbkHLOdzqmDSfDR9qlQM6RMm6HODCRtypskDJVAgY_ypJH8k5h7SOvnreKPdFR0k3SfSlq6MV_fz7FZqzF15hqCYju77g_CbjPL5ZrQXJk_d3acg9AcWAwlL0JNpGNynafAow_Wf0HcwOvTIRsF2fSmIrvO5diAKr_BjXwP_0xLtzemy3laIfOR9LDkEuwzpG-1eOOl0rvl\",null,3,null,[4,null,null,null,null,null,null,null,null,[\"ClcKIwgBEAIaCLnAcdrzZq8yKhDLEJmgsMmqTEpvN_1h5HdSMgEEEjDFb1WqY9-Xrrw6RoXUWyizUaNIw1RwQ4_Gv0t5zy2DFohwyPma4GCPLmnRDPIvq68SIJoxdvEUKXT1Zl3CApCRyIlNUN5RAWJwPwmQUrb6gYFP\",1]]]"
, key: "bghash"
value: "kk3Gx8dy_aXfeZYlNm-Tyx8oMdaEk_LJaYzS1CAHZVE"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END
    );

    ns_end_transaction("challenge", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_7");
    ns_web_url ("index_7",
        "URL=https://accounts.google.com/_/common/diagnostics/?hl=en-GB&_reqid=543284&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:795",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=Google-Accounts-XSRF:1",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;LSID;__Host-3PLSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;ACCOUNT_CHOOSER;__Host-GAPS;NID",
        BODY_BEGIN,
            "[key: "diagnostics"
value: "[[[\"gaia_fe_minutemaid:challenge/azPage\",null,null,null,1619591870631,44990],[\"gaia_fe_minutemaid:challenge/azPage\",null,null,null,1619591915621,12460]]]"
, key: "azt"
value: "AFoagUXUIcD6ELFQ4xQW_cB6yRIGqjnPeA:1619591418683"
, key: "cookiesDisabled"
value: "false"
, key: "deviceinfo"
value: "[null,null,null,[],null,\"IN\",null,null,null,\"GlifWebSignIn\",null,[null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[],null,null,null,[],[]],null,null,null,null,1,null,false,null,\"\"]"
, key: "gmscoreversion"
value: "undefined"
, key: "checkConnection"
value: "youtube:377:1"
, key: "checkedDomains"
value: "youtube"
, key: "pstMsg"
value: "1"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://mail.google.com/accounts/SetOSID?authuser=0&continue=https%3A%2F%2Faccounts.youtube.com%2Faccounts%2FSetSID%3Fssdc%3D1%26sidt%3DALWU2cvynAVWAA3HvMyblPJUb44Bpgw59utFwjGPSPkKrLHW37OQRc9kaZOsBwUXeUxP1dKzQuthVjW4Xu3D4KK17fhbnwlzrhCFRJTPJlPNQwSPu%252FPacF2alnF6U5mLsYl7gZ9qOabMNn7laeBkr70BkmDjzl%252B4G3wmaiPtrpK07L%252FPP5ACA%252FaRHkGD3Z3OY7ZdX1RV1xHGVWvve4EUbYFSFpdcN0qFVrOqbISBwo9Z49s8FwMLlPRhgjSw5IXmlPgA8LlfjUHinfUEYdKS1LeAzwtVxxNBRuGv7OVbIFkKaUnBtxVMeVOsr62cJ9FcrJil7mc70zgZToS0k7RDdedLu%252FCtXDSssVYrUOwQsxc4oLn6kd5Theg%253D%26continue%3Dhttps%253A%252F%252Fmail.google.com%252Fmail%252F%26dbus%3DIN&osidt=ALWU2csoFmsEyLEMo_KFKjBSUiLwxBtHwn4bPauR7VfBwt9SdIzdyjjOIAfmUnyh7iziai52MFbcZD67TnxXn1sDeZUmUvN4lmQkUbmfO8tqCTmqcGIV-8Ej6nH4CCmIzBN570_SVFaSlRagVBJOm6KHFYKiiRfdOyUO3lG6I6dMx6YH2gijtokiURnXxGP9pOAzlG6kf5mpr8ccVGNvvoFKzvgJVHiC6w", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN", END_INLINE
    );

    ns_end_transaction("index_7", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("awaittx_4");
    ns_web_url ("awaittx_4",
        "URL=https://content.googleapis.com/cryptauth/v1/authzen/awaittx?alt=json&key=AIzaSyCp406mobx24ph_NgrsN9Qp4wrgWJrDL54",
        "METHOD=POST",
        "HEADER=Content-Length:155",
        "HEADER=Origin:https://accounts.google.com",
        "HEADER=X-ClientDetails:appVersion=5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36&platform=Win32&userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36",
        "HEADER=X-Goog-Encode-Response-If-Executable:base64",
        "HEADER=Content-Type:application/json",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=X-JavaScript-User-Agent:google-api-javascript-client/1.1.0",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        BODY_BEGIN,
            "{"txId":"CkcKIwgBEAIaCLnAcdrzZq8yKhCafsPvZAl_Vib9tgz72qp9MgEDEiBBmvIaCuECtJt7u-orqNi4hkFixqdiDOec0WsP19ZwNRIgqR3AaH9duuIS_j07D00U9xAlM--evi1nZ3kXwgDpegE="}",
        BODY_END,
        INLINE_URLS,
            "URL=https://accounts.youtube.com/accounts/SetSID?ssdc=1&sidt=ALWU2cvynAVWAA3HvMyblPJUb44Bpgw59utFwjGPSPkKrLHW37OQRc9kaZOsBwUXeUxP1dKzQuthVjW4Xu3D4KK17fhbnwlzrhCFRJTPJlPNQwSPu/PacF2alnF6U5mLsYl7gZ9qOabMNn7laeBkr70BkmDjzl%2B4G3wmaiPtrpK07L/PP5ACA/aRHkGD3Z3OY7ZdX1RV1xHGVWvve4EUbYFSFpdcN0qFVrOqbISBwo9Z49s8FwMLlPRhgjSw5IXmlPgA8LlfjUHinfUEYdKS1LeAzwtVxxNBRuGv7OVbIFkKaUnBtxVMeVOsr62cJ9FcrJil7mc70zgZToS0k7RDdedLu/CtXDSssVYrUOwQsxc4oLn6kd5Theg%3D&continue=https://mail.google.com/mail/&dbus=IN", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://accounts.google.co.in/accounts/SetSID?ssdc=1&sidt=ALWU2ctNdca5Lz0XxYWE74vkl/kf7pEQXbrWr1ibvfL34otvkRjH0tKsAvcurAeSr0COd42cHr5D/XuvN0kFaP8tFJKrHRV6Pyp%2BesTmftMC3yJPKKYbVHL5lShb85YTExMPfHuELbNIL2zWU0yhnO3KTUo6zB7XWs7vuy966OdE6n6XgUY9mGcvvnnCi70sQuh/Q8UDt0YND5SFbnUz4%2BvASaK6WuvQLAUX6vSngL44uL6YYzWdK1x%2B/gAfaCSRYtYsL4uiEqrNUJp/Q0H29ie4fckjaxH261m2tKGaj/Nmt%2BLaq/vnasp804gs4Ha7g6wri4IitrubaFSJHvKkO6QhNETpKc55kuwER82cU6U/Mh81qhuqfBs%3D&continue=https://mail.google.com/mail/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/mail/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=b", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=b/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=a", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("awaittx_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("logstreamz");
    ns_web_url ("logstreamz",
        "URL=https://mail.google.com/mail/u/0/logstreamz",
        "METHOD=POST",
        "HEADER=Content-Length:487",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryQfIf0R8zIuiBfspx",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=a,b/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=m,m_i,i20jfd,lKrWxc,hkjXJ,gYOl6d,HXLjIb,DL8jZe,xaQcye,oRmHt,E1P0kd,pE92lb,v2eEBc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=DL8jZe,E1P0kd,HXLjIb,a,b,gYOl6d,hkjXJ,i20jfd,lKrWxc,m,m_i,oRmHt,pE92lb,v2eEBc,xaQcye/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=ebdd,sps,l,spit,t,it,lLYctc,anc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("logstreamz", NS_AUTO_STATUS);
    ns_page_think_time(0.007);

    ns_start_transaction("index");
    ns_web_url ("index",
        "URL=https://mail.google.com/mail/u/0/data?sw=2&token=%5B%22cftp%22,%223ea5edf7c4%22,%22gmail_fe_210421.06_p2%22,%22ZyMrc6cBAw-rFZnvtXyj5A%5Cu003d%5Cu003d%22,%227584,7236,7766,7113,7158,6969,7137,7419,7514,7753,7156,7758,7418,7030,7446,7518,7465,7304,7782,7773,7639,6999,7496,7546,7594,7677,7393,7150,7618,6792,7798,7628,7407,7682,7817,7468,7822,7834,7016,7569,7433,7164,7189,7831,7027,7278,7424,6807,7416,7573,7829,7774,7500%22,1%5D&dilte=0&gme=1&sme=1&pt=gwt",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://mail.google.com/mail/u/0/", END_INLINE,
            "URL=https://mail.google.com/mail/u/0/?ui=2&view=lsimp&imp=sw-asst&ik=3ea5edf7c4&itp=SECTIONED&cct=2&mct=1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;GMAIL_LOGIN;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_8");
    ns_web_url ("index_8",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=1006..&view=dd&_reqid=43731&nsc=1&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:0",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/pimages/2/search_white.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/mail/sprites/general_black-2eb471de5e5ea7371fa18ebc5339694d.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/skinnable/skinnable_ltr_light_1x.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/rfr/logo_gmail_lockup_default_1x_r2.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/images/icons/material/system/svg/help_outline_24px.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/images/icons/material/system_gm/svg/settings_24px.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.PoHfeJrvkfI.L.W.O/m=q_sf,qdid,qmd,qcwid/excm=qaaw,qabr,qadd,qaid,qalo,qebr,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhlo,qhmn,qhpc,qhpr,qhsf,qhtt/d=1/ed=1/ct=zgms/rs=AA2YrTtlkmhqEx9CuPzOCQfBhsNqzgdKAw", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.3gGou_DPQGQ.O/rt=j/m=qgl,q_dnp,q_sf,q_pc,qdid,qmd,qdgm,qcwid,qmutsd,qbg,qbd,qapid/exm=qaaw,qabr,qadd,qaid,qalo,qebr,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhlo,qhmn,qhpc,qhpr,qhsf,qhtt/d=1/ed=1/rs=AA2YrTuZTrLZ4SHM1gfcCFFxdZIZ-5oj0Q", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/branding/googlelogo/svg/googlelogo_clr_74x24px.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/ogw/ADGmqu8EX2z5Z73cAPLbqxMvgzYVYc6W2zEyMM9oo-G_tA=s32-c-mo", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system_gm/1x/launch_gm_grey_18dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/check_box_outline_blank_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/arrow_drop_down_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/refresh_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/spinner_18_18.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/more_vert_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/chevron_left_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/chevron_right_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/inbox_gm_googlered600_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/people_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/local_offer_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/info_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/forum_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/drag_indicator_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/star_border_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/star_googyellow500_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/attachment_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/colored_icons/1x/create_32dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/grade_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/watch_later_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/send_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/insert_drive_file_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/expand_more_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/mail/u/0/#inbox", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/wbt/contacts_icon21.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/wbt/hangouts_icon21.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/mail/u/0/images/cleardot.gif", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_3_pdf_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_image_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_spreadsheet_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_powerpoint_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/videocam_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/keyboard_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/companion/icon_assets/calendar_2020q4_2x.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/companion/icon_assets/keep_2020q4v3_2x.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/companion/icon_assets/tasks2_2x.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/companion/icon_assets/contacts_2x.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/setgmail?zx=cvsv23sk1r0z", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://clients2.google.com/availability/?s=gmail&a=viewinbox&c=scs&tm=1619591931647&zx=1h324m9ly00sw", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_8", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_9");
    ns_web_url ("index_9",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=mail%3Ai.1006.0.0&at=AF6bupONdqByPFeqRPgsXc1E6M_lMsnI3A&view=up&act=oestp&_reqid=143731&nsc=1&mb=0&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:3365",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "stped"
value: "[\"stped\",1619591931344000,0,[\"\"],[[null,false,false,\"179168283f9e0fef\",null,0],[null,false,false,\"178f9ec518168ed6\",null,1],[null,false,false,\"178f51d0e05a21a2\",null,2],[null,false,false,\"178e833f3b53492c\",null,3],[null,false,false,\"1788b9a3014a2390\",null,4],[null,false,false,\"17876f502a83e0c4\",null,5],[null,false,false,\"17867c83f0396a14\",null,6],[null,false,false,\"1784096fce93a6de\",null,7],[null,false,true,\"177c05a79f455a4e\",null,8],[null,false,true,\"177525c305e746f4\",null,9],[null,false,false,\"177a0b60c922daed\",null,10],[null,false,false,\"17785cbf9ecf405f\",null,11],[null,false,false,\"17780c19d31e2db5\",null,12],[null,false,true,\"176e7b1335eaf54d\",null,13],[null,false,true,\"176e28144c6012fe\",null,14],[null,false,false,\"1766234b96ebbd70\",null,15],[null,false,false,\"17645a7c9a7d77b8\",null,16],[null,false,false,\"17630800f6eb0959\",null,17],[null,false,false,\"1761782bb3b01662\",null,18],[null,false,true,\"175cc3de1de7e38e\",null,19],[null,false,true,\"175c062ed57f1bbe\",null,20],[null,false,true,\"1758fffa2fb71594\",null,21],[null,false,false,\"174904665f670e44\",null,22],[null,false,true,\"174870712663b0f9\",null,23],[null,false,false,\"174771243f31484f\",null,24],[null,false,false,\"17453aac7b1f695f\",null,25],[null,false,false,\"173f74068cd00eb1\",null,26],[null,false,false,\"173e76eb21adf121\",null,27],[null,false,true,\"173a683682c3c4fd\",null,28],[null,false,true,\"17399ad99b04c1da\",null,29],[null,false,false,\"1736ca4a0f9842ab\",null,30],[null,false,true,\"173658b44722df23\",null,31],[null,false,false,\"1735ce9e13076317\",null,32],[null,false,false,\"17352bf0c4b49fa8\",null,33],[null,false,false,\"1733e40cc74f1b76\",null,34],[null,false,true,\"1733cad54f4070e7\",null,35],[null,false,true,\"1732ddca1b776349\",null,36],[null,false,true,\"17329e6663269b02\",null,37],[null,false,true,\"1731f6180b1cc8c4\",null,38],[null,false,true,\"17319b2e6f611142\",null,39],[null,false,false,\"172db7a57adcde3d\",null,40],[null,false,false,\"172b363680e61ed5\",null,41],[null,false,false,\"1729414d10544744\",null,42],[null,false,false,\"17270968b69be506\",null,43],[null,false,false,\"17270795033a23aa\",null,44],[null,false,false,\"1726fce5db4d1e78\",null,45],[null,false,false,\"171ab144e860e357\",null,46],[null,false,false,\"1719145d16583732\",null,47],[null,false,false,\"1715d5926349fb28\",null,48],[null,false,true,\"1715982e8e335edd\",null,49]],4,null,[],0]"
]",
        BODY_END
    );

    ns_end_transaction("index_9", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("logstreamz_2");
    ns_web_url ("logstreamz_2",
        "URL=https://mail.google.com/mail/u/0/logstreamz#inbox",
        "METHOD=POST",
        "HEADER=Content-Length:538",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:multipart/form-data; boundary=----WebKitFormBoundaryuFT5UrZFJ76Nv9yc",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("logstreamz_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_10");
    ns_web_url ("index_10",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=1006..&at=AF6bupONdqByPFeqRPgsXc1E6M_lMsnI3A&view=up&act=rfa&em=undefined&_reqid=243731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&tz=5954799_34_34__34_&mb=0&rt=c",
        "METHOD=POST",
        "HEADER=Content-Length:0",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;GMAIL_STAT_1006;GMAIL_IMP;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "f.req"
value: "[\"og.botreq\",null,\"\",null,true,0,false]"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=client/exm=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_10", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("get");
    ns_web_url ("get",
        "URL=https://aa.google.com/u/0/_/gog/get?rt=j&sourceid=23",
        "METHOD=POST",
        "HEADER=Content-Length:69",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/mail/sprites/hangouts_black-734a4722bcd0d29f1eba9aaa722c7c76.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://hangouts.google.com/webchat/u/0/host-js?prop=gmail&b=1&zx=1vi9h9lie5h3", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/webchat/u/0/load?client=sm&prop=gmail&nav=true&fid=gtn-roster-iframe-id&os=Win32&stime=1619591932370&xpc=%7B%22cn%22%3A%22xmibq0%22%2C%22tp%22%3A1%2C%22ifrid%22%3A%22gtn-roster-iframe-id%22%2C%22pu%22%3A%22https%3A%2F%2Fhangouts.google.com%2Fwebchat%2Fu%2F0%2F%22%7D&ec=%5B%22ci%3Aec%22%2Ctrue%2Ctrue%2Cfalse%5D&pvt=AMP3uWZGe-dFnFJXpoxSsRxRpsRhS4yWZ3Sl4gsxLDLyarqyrrwn2OrxJDvwwEhxxBb0HhzGH2lMBP0FqHbnRBCzKMvCWQKNcQ%3D%3D&href=https%3A%2F%2Fmail.google.com%2Fmail%2Fu%2F0%2F%23inbox%3Frel%3D1&pos=l&uiv=2&hl=en&hpc=true&hsm=true&hrc=true&pal=1&uqp=false&gooa=false&gusm=true&sl=false&hs=%5B%22h_hs%22%2Cnull%2Cnull%2C%5B2%2C0%5D%5D&moleh=380&mmoleh=36&two=https%3A%2F%2Fmail.google.com&host=1&zx=5o305fggb238", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("get", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_11");
    ns_web_url ("index_11",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&auto=1&view=au&_reqid=343731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&mb=0&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:0",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("index_11", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd");
    ns_web_url ("fd",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=0",
        "METHOD=POST",
        "HEADER=Content-Length:259",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591932538\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1695446174829633732","2":1,"5":1},{"1":"thread-f:1695179215864424980","2":1,"5":1},{"1":"thread-f:1694489735609951966","2":1,"5":1},{"1":"thread-f:1692233777474263630","2":1,"5":1},{"1":"thread-a:r7841676937757512487","2":1,"5":1}],"2":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=Aihl6c,Av2a7c,B00Vac,CTcde,Cmvl0d,DL8jZe,DUNnfe,E1P0kd,EVZC0d,FSHQ1d,HXLjIb,IiBrJf,J03Die,M25vPb,MMhUM,MigGy,N35dyc,NVcOs,OIxRw,PKSrle,PZhDZb,PaBahd,RI4GO,Sz7W7c,Trl7bc,UZdBGe,VOAugd,VeTcob,VtSflc,ZdOxDb,Zxsddf,anc,atv,cs,cv,dFpypf,dLIOJe,eIu7Db,ebdd,f,gYOl6d,gtqvOc,hS6RLb,hkjXJ,i20jfd,igbF5,it,jVZ0pe,kL0rjf,kRtote,kbPIy,l,lKrWxc,lLYctc,lrkTZd,m_i,ml,nXDxbd,o2ajQe,oRmHt,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,puPi7e,rMQdJc,rn4kU,sm,spit,sps,t,tnECjd,u7EXMd,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,wvjFZd,xaQcye,yWJZbc,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=O9Wdw,B6ci9b,HSnlPc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://mail.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/js/bg/-_4qODghdBIY0Ta5c9Mpu0KGcGVAhTZCtcXg18Etdwg.js", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/chat-static/_/js/k=chat.smh.en.xYmaqBHs4wc.O/am=igE/d=0/rs=AGNGyv2KGZwd0304tv1Z02FeIRJlAfD4-Q/m=b", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=Aihl6c,Av2a7c,B00Vac,B6ci9b,CTcde,Cmvl0d,DL8jZe,DUNnfe,E1P0kd,EVZC0d,FSHQ1d,HSnlPc,HXLjIb,IiBrJf,J03Die,M25vPb,MMhUM,MigGy,N35dyc,NVcOs,O9Wdw,OIxRw,PKSrle,PZhDZb,PaBahd,RI4GO,Sz7W7c,Trl7bc,UZdBGe,VOAugd,VeTcob,VtSflc,ZdOxDb,Zxsddf,anc,atv,cs,cv,dFpypf,dLIOJe,eIu7Db,ebdd,f,gYOl6d,gtqvOc,hS6RLb,hkjXJ,i20jfd,igbF5,it,jVZ0pe,kL0rjf,kRtote,kbPIy,l,lKrWxc,lLYctc,lrkTZd,m_i,ml,nXDxbd,o2ajQe,oRmHt,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,puPi7e,rMQdJc,rn4kU,sm,spit,sps,t,tnECjd,u7EXMd,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,wvjFZd,xaQcye,yWJZbc,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=qXzOkf", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/chat-static/_/js/k=chat.wbl.en.4e_PRnB3Gio.O/am=wEPAAYIAQDgCCCgsog/d=1/rs=AGNGyv0ot3qYzkjgfio0IR-kjdzgNPVDvw/m=b,bst,c,ca,cnv,md,r", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("fd", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_12");
    ns_web_url ("index_12",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=1006..&auto=1&view=omni&_reqid=443731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&mb=0&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:0",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;GMAIL_IMP;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=Aihl6c,Av2a7c,B00Vac,B6ci9b,CTcde,Cmvl0d,DL8jZe,DUNnfe,E1P0kd,EVZC0d,FSHQ1d,HSnlPc,HXLjIb,IiBrJf,J03Die,M25vPb,MMhUM,MigGy,N35dyc,NVcOs,O9Wdw,OIxRw,PKSrle,PZhDZb,PaBahd,RI4GO,Sz7W7c,Trl7bc,UZdBGe,VOAugd,VeTcob,VtSflc,ZdOxDb,Zxsddf,anc,atv,cs,cv,dFpypf,dLIOJe,eIu7Db,ebdd,f,gYOl6d,gtqvOc,hS6RLb,hkjXJ,i20jfd,igbF5,it,jVZ0pe,kL0rjf,kRtote,kbPIy,l,lKrWxc,lLYctc,lrkTZd,m_i,ml,nXDxbd,o2ajQe,oRmHt,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,puPi7e,qXzOkf,rMQdJc,rn4kU,sm,spit,sps,t,tnECjd,u7EXMd,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,wvjFZd,xaQcye,yWJZbc,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=ZThjHe,cXmWHe,mib1Lb,s7cQOe,Wwwwbc,pmnVQ,M184fc,ZUhrff,x8nlqe,xivAT,jPErQb,Z16oCd,wS0DLb,wHcxDd,Q8AZuf,MAQNkf,djoBOd,AQzzK", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_12", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_7");
    ns_web_url ("log_7",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:578",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=AQzzK,Aihl6c,Av2a7c,B00Vac,B6ci9b,CTcde,Cmvl0d,DL8jZe,DUNnfe,E1P0kd,EVZC0d,FSHQ1d,HSnlPc,HXLjIb,IiBrJf,J03Die,M184fc,M25vPb,MAQNkf,MMhUM,MigGy,N35dyc,NVcOs,O9Wdw,OIxRw,PKSrle,PZhDZb,PaBahd,Q8AZuf,RI4GO,Sz7W7c,Trl7bc,UZdBGe,VOAugd,VeTcob,VtSflc,Wwwwbc,Z16oCd,ZThjHe,ZUhrff,ZdOxDb,Zxsddf,anc,atv,cXmWHe,cs,cv,dFpypf,dLIOJe,djoBOd,eIu7Db,ebdd,f,gYOl6d,gtqvOc,hS6RLb,hkjXJ,i20jfd,igbF5,it,jPErQb,jVZ0pe,kL0rjf,kRtote,kbPIy,l,lKrWxc,lLYctc,lrkTZd,m_i,mib1Lb,ml,nXDxbd,o2ajQe,oRmHt,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,pmnVQ,puPi7e,qXzOkf,rMQdJc,rn4kU,s7cQOe,sm,spit,sps,t,tnECjd,u7EXMd,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,wHcxDd,wS0DLb,wvjFZd,x8nlqe,xaQcye,xivAT,yWJZbc,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=IcviEd,ngpaqe,oTMABb,jWbVSe,OEaYAf,H6G42e,idosse,laqh9b,rbV53d,PtOM,RqJ0h,aBj2Kc,VxVlrc,sU8HHe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_7", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("data");
    ns_web_url ("data",
        "URL=https://hangouts.google.com/webchat/u/0/_/data?ds.extension=106074807&_reqid=43734&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:59",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "f.req"
value: "[[[106074807,null,null,null,0]]]"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://hangouts.google.com/_/scs/chat-static/_/ss/k=chat.wbl.m49c5aae3fem.L.W.O/am=wEPAAYIAQDgCCCgsog/d=0/rs=AGNGyv14U7mXHz9H9yD763DN5xh8LowCWg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://people-pa.clients6.google.com/v2/people/me/blockedPeople?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-http-method-override", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://chat-pa.clients6.google.com/chat/v1/conversations/syncrecentconversations?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&reqId=cs1276709404905&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://clients6.google.com/voice/v1/users/@me/account?checkHangoutsCallingPermission=false&key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&locale=en&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=AQzzK,Aihl6c,Av2a7c,B00Vac,B6ci9b,CTcde,Cmvl0d,DL8jZe,DUNnfe,E1P0kd,EVZC0d,FSHQ1d,H6G42e,HSnlPc,HXLjIb,IcviEd,IiBrJf,J03Die,M184fc,M25vPb,MAQNkf,MMhUM,MigGy,N35dyc,NVcOs,O9Wdw,OEaYAf,OIxRw,PKSrle,PZhDZb,PaBahd,PtOM,Q8AZuf,RI4GO,RqJ0h,Sz7W7c,Trl7bc,UZdBGe,VOAugd,VeTcob,VtSflc,VxVlrc,Wwwwbc,Z16oCd,ZThjHe,ZUhrff,ZdOxDb,Zxsddf,aBj2Kc,anc,atv,cXmWHe,cs,cv,dFpypf,dLIOJe,djoBOd,eIu7Db,ebdd,f,gYOl6d,gtqvOc,hS6RLb,hkjXJ,i20jfd,idosse,igbF5,it,jPErQb,jVZ0pe,jWbVSe,kL0rjf,kRtote,kbPIy,l,lKrWxc,lLYctc,laqh9b,lrkTZd,m_i,mib1Lb,ml,nXDxbd,ngpaqe,o2ajQe,oRmHt,oTMABb,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,pmnVQ,puPi7e,qXzOkf,rMQdJc,rbV53d,rn4kU,s7cQOe,sU8HHe,sm,spit,sps,t,tnECjd,u7EXMd,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,wHcxDd,wS0DLb,wvjFZd,x8nlqe,xaQcye,xivAT,yWJZbc,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=P8J5Md,bfl9hd,niGKwf,V88ABc,Nzme9", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://chat-pa.clients6.google.com/chat/v1/presence/querypresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("data", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("blockedPeople");
    ns_web_url ("blockedPeople",
        "URL=https://people-pa.clients6.google.com/v2/people/me/blockedPeople?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k",
        "METHOD=POST",
        "HEADER=Content-Length:344",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591933_ad76269790bf4e833b6c579c7d95b83cece3ce57",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=X-HTTP-Method-Override:GET",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("blockedPeople", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("account");
    ns_web_url ("account",
        "URL=https://clients6.google.com/voice/v1/users/@me/account?checkHangoutsCallingPermission=false&key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&locale=en&alt=protojson",
        "HEADER=Authorization:SAPISIDHASH 1619591933_ad76269790bf4e833b6c579c7d95b83cece3ce57",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Accept-Language:en",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://hangouts.google.com/webchat/u/0/frame2?v=1619385420&pvt=AMP3uWZGe-dFnFJXpoxSsRxRpsRhS4yWZ3Sl4gsxLDLyarqyrrwn2OrxJDvwwEhxxBb0HhzGH2lMBP0FqHbnRBCzKMvCWQKNcQ%3D%3D&prop=gmail&hl=en&fid=gtn-roster-iframe-id#e%5B%22wblh0.3512454962586229-0%22,2,1,%5Btrue,%5B%5D%5D%5D", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/webchat/u/0/frame2?v=1619385420&pvt=AMP3uWZGe-dFnFJXpoxSsRxRpsRhS4yWZ3Sl4gsxLDLyarqyrrwn2OrxJDvwwEhxxBb0HhzGH2lMBP0FqHbnRBCzKMvCWQKNcQ%3D%3D&prop=gmail&hl=en&fid=gtn-roster-iframe-id#e%5B%22wblh0.3512454962586229-1%22,2,1,%5Bnull,%5B2,3,4,5%5D%5D%5D", END_INLINE
    );

    ns_end_transaction("account", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("syncrecentconversations");
    ns_web_url ("syncrecentconversations",
        "URL=https://chat-pa.clients6.google.com/chat/v1/conversations/syncrecentconversations?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&reqId=cs1276709404905&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:147",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591933_ad76269790bf4e833b6c579c7d95b83cece3ce57",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("syncrecentconversations", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_13");
    ns_web_url ("index_13",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=1006..&at=AF6bupONdqByPFeqRPgsXc1E6M_lMsnI3A&view=up&act=prefs&_reqid=543731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&mb=0&rt=c",
        "METHOD=POST",
        "HEADER=Content-Length:16",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "p_bx_wyjps"
value: "true"
]",
        BODY_END
    );

    ns_end_transaction("index_13", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("index_14");
    ns_web_url ("index_14",
        "URL=https://mail.google.com/mail/u/0/?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&rid=1006..&auto=1&view=mic&_reqid=643731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&mb=0&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:0",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://chat.google.com/api/get_attachment_url", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/webchat/u/0/frame?v=1619385420&hl=en&pvt=AMP3uWZGe-dFnFJXpoxSsRxRpsRhS4yWZ3Sl4gsxLDLyarqyrrwn2OrxJDvwwEhxxBb0HhzGH2lMBP0FqHbnRBCzKMvCWQKNcQ%3D%3D&prop=gmail#epreld", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/client?cfg=%7B%222%22%3A%22hangouts%22%2C%226%22%3A%22gmail%22%2C%227%22%3A%22babel-chat.frontend_20210425.07_p0%22%2C%228%22%3Afalse%2C%2213%22%3Afalse%7D&ctype=hangouts&xpc=%7B%22cn%22%3A%220gmaBrxzwo%22%2C%22tp%22%3Anull%2C%22osh%22%3Anull%2C%22ppu%22%3A%22https%3A%2F%2Fhangouts.google.com%2Frobots.txt%22%2C%22lpu%22%3A%22https%3A%2F%2F0.client-channel.google.com%2Frobots.txt%22%7D", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/chat-static/_/js/k=chat.wbl.en.4e_PRnB3Gio.O/am=wEPAAYIAQDgCCCgsog/d=0/rs=AGNGyv0ot3qYzkjgfio0IR-kjdzgNPVDvw/m=sy6d,sy89,sy8b,sy8d,sy8e,sy8f,m", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/chat/babble/sprites/common-517bca7f07a37c64c3263a1411e266a5.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://hangouts.google.com/_/hscv?pvt=AMP3uWZGe-dFnFJXpoxSsRxRpsRhS4yWZ3Sl4gsxLDLyarqyrrwn2OrxJDvwwEhxxBb0HhzGH2lMBP0FqHbnRBCzKMvCWQKNcQ%3D%3D&winUrl=https%3A%2F%2Fmail.google.com&authuser=0&origin=https%3A%2F%2Fhangouts.google.com&origin=https%3A%2F%2Fmail.google.com&xpc=%7B%22cn%22%3A%225Dz2W2r36o%22%2C%22tp%22%3Anull%2C%22osh%22%3Anull%2C%22ppu%22%3A%22https%3A%2F%2Fhangouts.google.com%2Frobots.txt%22%2C%22lpu%22%3A%22https%3A%2F%2Fhangouts.google.com%2Frobots.txt%22%7D", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ogs.google.com/u/0/widget/app?bc=1&origin=https%3A%2F%2Fmail.google.com&cn=app&pid=23&spid=23&hl=en", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_14", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_8");
    ns_web_url ("log_8",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1677",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],407,[["1619591934952",null,[],null,null,null,null,"[23,1619591934952000,\"!y8ilyIXNAAZLnZBaS51CjQjs7xqqR1M7ACkAIwj8RoVFQUDFXuFkGSeWInRib4RukL3mtTgeg0IASfqkd0KAqK0dZQIAAAYcUgAAAL5oAQeZA6OAoK_n1nb_06ORkvj-pv9qpPiB9Kn8-5f1WsztcWG-Y8jMX0ox0jqYFqbzB2EdnwWFNgQBneS6CGYSr-GCBx310bDe8-0I2ixfyf8mrYWxpBjOPEiBmLzm7COHK1HhpGTdXE7X6VXh9sON6FMErSjqXxttIj7w9iuC2KCSH3n6_9IznyLJ9F5ufB8SG_D-u_LLNPA6tYmzJ5dUAR-YzCxHiLN4lEnW2AfR6jOrAXvXUSXI_-RRCLz8ZXmnhXQQ1JIXBRanvwD93Ct22rN_0o8Ykdd6zSSGcR0zQXZ3f_M1l8QFIrNIszVEMya4hycn1vePDnmVhffmTbp-T-2EQjdcoAlj_RNtOkYmHl5I9YtLs9gFZObMXDXfFfgXNucOZEPsb1w86bKCLT8ULnhR6a1wVBR7qaMuiKRngvvjdsjNcSFhNEJnov2nM_o_RNAD032offr-aEF2xqjuhGjBa5wSy0NuGdjq5uBP9DDZLBFFTIBHKdhVgzyWRj21tweMscteX_ugTHy1KQTj56O4DypADC6tgUF5znID9X0qYsMzNOLlxbT1mrm2LCNoX5Z3w5WCIMb9VJA5fho142ilENMUZoVT01mKrRSmDSCxAcRDhErHrmv6r9z9gYDhXF5hvpS6V-t9dybhdF_76M0WIlDLTHQ6SsHdv8H31ma0im9ZlLNMflO3mgMnZNvFjIGx1eg0lr6Rh4qK__8Cn-YXuxQt1PiFRM6ZdUF1zEPBHH0_-wN716c7Nk9KLKAnNVrllHJPKd-VpTjemwUcggX9TCsioz9wV9u9MwClkYtN-XWv_kriQ8jAby4G16OtdeBvVq4heoETh3gBPY7YrhuQqTRcntAH_W-wNZlQQj6Sr-Rs_bzxFWbiUxQ5OuY4JaImZGda-uURwEr7Aq9ManE5ZvHDRNR_q6H5ibyrrms-YkMa03pv3880lSF4TmN_qf_A2J5rJDoc5C-78O4u7WKhWIkraqYQJUiMzH_Q0gDQimdc1Rl4d_UF1_b0dSNgtNQTrrV7PjpuQHoxttxB3hHLFO16QRkJF_N5Q2KR54jOo-wNlcCUrrp-Dn1oywRVmPXNL5k424EX7qwKJbp_j5XAVLUtrtN912t0axhjYs72_yeF060IZIr1UNMZ7ST4xwCicy78-xqSBt4R8YoaOFqZ4EnG3pffRD3kcGjIaveP9N9_L0LwVAi8_FLWL8iD7VLrJwJHDpDxTHzRdgpmUf1Vs10u4Qmw\",33785112,false]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591934952",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://lh3.googleusercontent.com/a-/AOh14Gidt0zb8oZ3LOLXhBej3Bck-0_kW1Jr1Lc5JAxnjg=s28-c-k-no", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/a-/AOh14GgsBh5LLw9rGn8jNCnGAF1768WamvazTIIFkWUD9Q=s46-c-k-no", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_8", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_2");
    ns_web_url ("fd_2",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=1",
        "METHOD=POST",
        "HEADER=Content-Length:112",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591935216\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1691677120350313197","3":["msg-f:1691677120350313197","msg-f:1691685575957562684"]}],"2":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://accounts.google.com/ServiceLogin?service=dynamite&passive=1209600&osid=1&continue=https://chat.google.com/api/get_attachment_url&followup=https://chat.google.com/api/get_attachment_url&authuser=0", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;LSID;__Host-3PLSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;ACCOUNT_CHOOSER;__Host-GAPS;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/chat/sounds/incoming_message_eb39ce414e3ffba41a8e173581dc7248.mp3", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=Range:bytes=0-", END_INLINE,
            "URL=https://ssl.gstatic.com/chat/sounds/incoming_video_long_e5df1b5146e9cbdba78af43d60005200.mp3", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=Range:bytes=0-", END_INLINE,
            "URL=https://ssl.gstatic.com/chat/sounds/incoming_video_short_5abdd7c1c7fa8bbd5d6b4733de315c59.mp3", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=Range:bytes=0-", END_INLINE
    );

    ns_end_transaction("fd_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("querypresence");
    ns_web_url ("querypresence",
        "URL=https://chat-pa.clients6.google.com/chat/v1/presence/querypresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:156",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],[null,"8BB52C5AD454C5B7"],null,"en"],[["102932755784749561560"],["112379449936499860056"]],[2,3,10]]",
        BODY_END,
        INLINE_URLS,
            "URL=https://apis.google.com/js/api.js", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=1/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=inmolecalling", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/js/947777316-lcs_client_bin.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail-ads.google.com/mail/u/0/ads/main_jspb?rt=r&client=25", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://mail.google.com", "HEADER=Access-Control-Request-Headers:content-type,x-framework-xsrf-token", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.HQhZbjCF72o.es5.O/am=WABA/d=1/excm=_b,_tp,appwidgetauthview/ed=1/dg=0/wt=2/rs=AM-SdHtEmfkFOyUat8NMdXzGGJP8_zTQug/m=_b,_tp", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/ogw/ADGmqu8EX2z5Z73cAPLbqxMvgzYVYc6W2zEyMM9oo-G_tA=s128-b16-cc-rp-mo", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/gb/images/p1_c9bc74a1.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/gb/images/a/3a1e625196.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://chat.google.com/accounts/SetOSID?authuser=0&continue=https%3A%2F%2Fchat.google.com%2Fapi%2Fget_attachment_url%3Fpli%3D1&osidt=ALWU2cuYekfIFCoi4M2yDHNvSGDvEvlbsV09EhV9D9RNHrAAFJkQOXj6BL2auSQw_ps6jLLHJA1cppBk0FB0GoOeQWXBloqUNW_I6npy0AXSTMF5wPK2njy_Bru7pFEx-rup_W0XEZBu04mg67SGG9XLbLC4PKXOwgrQh20khEjR4mMuKIrY8Shgrc0NVvpypHQa88xfHno80YMx9-ewX98cws3Af_NRog", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=0/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=sy1,hl", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://chat.google.com/api/get_attachment_url?pli=1", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://accounts.google.com/ServiceLogin?service=dynamite&passive=1209600&osid=1&continue=https://chat.google.com/api/get_attachment_url&followup=https://chat.google.com/api/get_attachment_url&authuser=0", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GAPS;LSID;__Host-3PLSID;ACCOUNT_CHOOSER;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://apis.google.com/js/api.js", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.HQhZbjCF72o.es5.O/ck=boq-one-google.OneGoogleWidgetUi.UAsJBnPrmIc.L.B1.O/am=WABA/d=1/exm=_b,_tp/excm=_b,_tp,appwidgetauthview/ed=1/wt=2/rs=AM-SdHuX74zCLFWXk-Vcp1LyB1xUXREaxw/m=byfTOb,lsjVmc,LEikZe", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=0/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=T6U9J,lHoXXe,Z5j79c,ma", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.HQhZbjCF72o.es5.O/ck=boq-one-google.OneGoogleWidgetUi.UAsJBnPrmIc.L.B1.O/am=WABA/d=1/exm=LEikZe,_b,_tp,byfTOb,lsjVmc/excm=_b,_tp,appwidgetauthview/ed=1/wt=2/rs=AM-SdHuX74zCLFWXk-Vcp1LyB1xUXREaxw/m=n73qwf,ws9Tlc,IZT63,e5qFLc,GkRiKb,UUJqVe,O1Gjze,xUdipf,blwjVc,fKUV3e,aurFic,COQbmf,U0aPgd,ZwDk9d,V3dDOb,mI3LFb,O6y8ed,NpD4ec,PrPYRd,iWP1Yb,MpJwZc,O8k1Cd,NwH0H,OmgaI,HLo3Ef,x60fie,xiqEse,lazG7b,XVMNvd,L1AAkb,KUM7Z,rE6Mgd,lKZxSd,s39S4,lwddkf,gychg,w9hDv,RMhBfe,qCSYWe,SdcwHb,aW3pY,YLQSd,PQaYAf,pw70Gc,EFQ78c,Ulmmrd,ZfAoz,mdR7q,CBlRxf,MdUzUe,xQtZb,lPKSwe,QIhFr,JNoxi,MI6k7c,kjKdXe,pB6Zqd,rHjpXd,yDVVkb,SF3gsd,hKSk3e,iTsyac,hc6Ubd,KG2eXe,SpsfSb,tfTN8c,o02Jie,VwDzFe,zbML3c,HDvRde,Uas9Hd,BVgquf,A7fCU,lsPsHb,UgAtXe,aDfbSd,pjICDe", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=config/exm=client,gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_2", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("querypresence", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_3");
    ns_web_url ("fd_3",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=2",
        "METHOD=POST",
        "HEADER=Content-Length:848",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591935988\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1691203638150512735","3":["msg-f:1691203638150512735","msg-f:1691236208898028731"]},{"1":"thread-f:1691114965133307317","3":["msg-f:1691114965133307317","msg-f:1691137459807472555"]},{"1":"thread-f:1688422232749700429","3":["msg-f:1688422232749700429"]},{"1":"thread-f:1688330977956336382","3":["msg-f:1688330977956336382"]},{"1":"thread-f:1686073918058380656","3":["msg-f:1686073918058380656","msg-f:1686073968587910387"]},{"1":"thread-f:1685571651757766584","3":["msg-f:1685571651757766584","msg-f:1685577198138184774"]},{"1":"thread-f:1685199485802318169","3":["msg-f:1685199485802318169","msg-f:1685214239519546720","msg-f:1685246882442026280"]},{"1":"thread-f:1684759864706864738","3":["msg-f:1684759864706864738","msg-f:1684774854340186776"]},{"1":"thread-f:1683435719481615246","3":["msg-f:1683435719481615246"]}],"2":2}",
        BODY_END
    );

    ns_end_transaction("fd_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("main_jspb");
    ns_web_url ("main_jspb",
        "URL=https://mail-ads.google.com/mail/u/0/ads/main_jspb?rt=r&client=25",
        "METHOD=POST",
        "HEADER=Content-Length:297",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://chat.google.com/accounts/SetOSID?authuser=0&continue=https%3A%2F%2Fchat.google.com%2Fapi%2Fget_attachment_url%3Fpli%3D1&osidt=ALWU2cv-F-ehNgm57fr4hSb3XpUGUSfjcWLeTiC8IUIdUeljx1_3moR7IB20HBzy86nR-EQl1E-DRt-Px7s7CbMyO-VBHfg7LK22kiZX8a2NVX_n0Kjlg-Yl6kbbc78QwcZGieKDfI50cg8YCdyOPmOnlali4ALI6NMhZ8tZKMg6YjPVPaMX04pM-8tA8DQ9InyJwiHVkmAuarF2Vq0JuqnWfc2_eLGP7w", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/_/mss/boq-one-google/_/js/k=boq-one-google.OneGoogleWidgetUi.en.HQhZbjCF72o.es5.O/ck=boq-one-google.OneGoogleWidgetUi.UAsJBnPrmIc.L.B1.O/am=WABA/d=1/exm=A7fCU,BVgquf,CBlRxf,COQbmf,EFQ78c,GkRiKb,HDvRde,HLo3Ef,IZT63,JNoxi,KG2eXe,KUM7Z,L1AAkb,LEikZe,MI6k7c,MdUzUe,MpJwZc,NpD4ec,NwH0H,O1Gjze,O6y8ed,O8k1Cd,OmgaI,PQaYAf,PrPYRd,QIhFr,RMhBfe,SF3gsd,SdcwHb,SpsfSb,U0aPgd,UUJqVe,Uas9Hd,UgAtXe,Ulmmrd,V3dDOb,VwDzFe,XVMNvd,YLQSd,ZfAoz,ZwDk9d,_b,_tp,aDfbSd,aW3pY,aurFic,blwjVc,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,iTsyac,iWP1Yb,kjKdXe,lKZxSd,lPKSwe,lazG7b,lsPsHb,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,o02Jie,pB6Zqd,pjICDe,pw70Gc,qCSYWe,rE6Mgd,rHjpXd,s39S4,tfTN8c,w9hDv,ws9Tlc,x60fie,xQtZb,xUdipf,xiqEse,yDVVkb,zbML3c/excm=_b,_tp,appwidgetauthview/ed=1/wt=2/rs=AM-SdHuX74zCLFWXk-Vcp1LyB1xUXREaxw/m=Wt6vjf,_latency,FCpbqb,WhJNk", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=card/exm=client,config,gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_3", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/feedback/js/help/prod/service/lazy.min.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/gsid", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://ogs.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://contacts.google.com/widget/hovercard/v/2?authuser=0&hl=en&origin=https%3A%2F%2Fmail.google.com&usegapi=1&jsh=m%3B%2F_%2Fscs%2Fabc-static%2F_%2Fjs%2Fk%3Dgapi.gapi.en.jcYff4gdSOQ.O%2Fd%3D1%2Fct%3Dzgms%2Frs%3DAHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw%2Fm%3D__features__#id=__HC_94253229&_gfid=__HC_94253229&parent=https%3A%2F%2Fmail.google.com&pfname=&rpctoken=41207972", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=cnsacwor87pg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://addons-pa.clients6.google.com//$rpc/google.internal.apps.addons.v1.AddOnService/ListInstallations", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://mail.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-api-key,x-goog-authuser,x-user-agent", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("main_jspb", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("main_jspb_2");
    ns_web_url ("main_jspb_2",
        "URL=https://mail-ads.google.com/mail/u/0/ads/main_jspb?rt=r&client=25",
        "METHOD=POST",
        "HEADER=Content-Length:428",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"2":{"1":{"1":"gmail_fe_210421.06_p2","3":"","4":"","5":{"2":1,"6":1,"8":1},"6":0,"7":"en","8":370149053,"9":25,"10":0,"11":0,"12":"+05:30","14":0,"15":{"1":2,"2":[{"1":2},{"1":3},{"1":4},{"1":6},{"1":5}],"3":[1,2,3,0]},"16":0,"22":0,"23":0,"24":1,"25":1366,"26":{"1":"","2":0},"27":607,"28":0},"2":0,"3":"","4":[8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610]}}",
        BODY_END
    );

    ns_end_transaction("main_jspb_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_9");
    ns_web_url ("log_9",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1931",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],241,[["1619591936225",null,[],null,null,null,null,"[1619591936217,[[\"2397168675742140944\",null,[[419,1]]],[\"16147638372540442232\",null,[[497,1]]],[\"14719340685975485085\",null,[[660,1]]],[\"17077408715954654437\",null,[[964,1]]],[\"3318688667027929436\",null,[[497,1]]],[\"5790177495296899286\",null,[[11,1]]],[\"16829267986558572790\",null,[[88,1]]],[\"16339156775003354937\",null,[[137,1]]],[\"749851692583976763\",null,[[12,1]]],[\"15419336178855610526\",null,[[287,1]]],[\"17276521865292187132\",null,[[1,1]]],[\"8257051839445688306\",null,[[963,1]]],[\"7792735449360349632\",null,[[963,1]]],[\"7094487270460551484\",null,[[8,1]]],[\"12563104964214410683\",null,[[344,1]]],[\"15605813632677093659\",null,[[118,1]]],[\"17914751415692637656\",null,[[7,1]]],[\"9797767207516844257\",null,[[0,1]]],[\"14906952326733574741\",null,[[4,1]]],[\"4891744519482609478\",null,[[177,1]]],[\"14307859671070593733\",null,[[123,1]]],[\"7494582641517049914\",null,[[3,1]]],[\"6667106912793420619\",null,[[0,1]]],[\"10118692516388306266\",null,[[1,1]]],[\"408159237941253787\",null,[[260,1]]],[\"476083397694989718\",null,[[260,1]]],[\"8791060314450143495\",null,[[1,1]]],[\"6342145065879578001\",null,[[3,1]]],[\"13596961294000664596\",null,[[344,1]]],[\"2107494750385856652\",null,[[62,1]]],[\"1309831198388189068\",null,[[3,1]]],[\"522022639063469804\",null,[[0,1]]],[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[136,1]]],[\"3079121564595244695\",null,[[108,1]]],[\"10652791942255425261\",null,[[11343,1]]],[\"4132870161583308123\",null,[[114,1]]]],null,null,\"[1,\\\"qBzSPd_PL\\\"]\"]",null,null,null,376525489,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1619591936226",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://0.client-channel.google.com/client-channel/channel/cbp?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&MODE=init&zx=h8u9zuafk3sh&t=1", "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54", "HEADER=X-Origin:https://hangouts.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/channel/cbp?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&TYPE=xmlhttp&zx=v361o91scimc&t=1", "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54", "HEADER=X-Origin:https://hangouts.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://contacts.google.com/widget/hovercard/v/2?hl=en&origin=https://mail.google.com&usegapi=1&jsh=m;/_/scs/abc-static/_/js/k%3Dgapi.gapi.en.jcYff4gdSOQ.O/d%3D1/ct%3Dzgms/rs%3DAHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/m%3D__features__#id=__HC_94253229&_gfid=__HC_94253229&parent=https%3A%2F%2Fmail.google.com&pfname=&rpctoken=41207972", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_9", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("bind");
    ns_web_url ("bind",
        "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&RID=67&CVER=5&zx=4xterle5uytq&t=1",
        "METHOD=POST",
        "HEADER=Content-Length:7",
        "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54",
        "HEADER=Origin:https://0.client-channel.google.com",
        "HEADER=X-Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[null,[1,1,null,null,null,null,null,null,null,null,"5954799_34_34__34_",null]]",
        BODY_END
    );

    ns_end_transaction("bind", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("ListInstallations");
    ns_web_url ("ListInstallations",
        "URL=https://addons-pa.clients6.google.com//$rpc/google.internal.apps.addons.v1.AddOnService/ListInstallations",
        "METHOD=POST",
        "HEADER=Content-Length:78",
        "HEADER=X-User-Agent:grpc-web-javascript/0.1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591936_49b47e4cd87b5fa9753c1bd6fe7ef91d336f23eb",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=X-Goog-Api-Key:AIzaSyBs4M8MW-9jrQUbgjuDZPqYs9zSI5aWshw",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://chat.google.com/api/get_attachment_url?pli=1", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;OSID;__Secure-OSID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&RID=rpc&SID=C43899E49E29E5D9&CI=0&AID=0&TYPE=xmlhttp&zx=glmvq4dkphen&t=1", "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54", "HEADER=X-Origin:https://hangouts.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://contacts.google.com/_/scs/social-static/_/js/k=boq.SocialPeopleHovercardUi.en.0XfxSLuRPx0.es5.O/am=FAAI/d=1/excm=_b,_tp,hovercardwidget/ed=1/dg=0/wt=2/rs=AGLTcCObnm-WrpEA0ipH-ZSdVMTQepZk3g/m=_b,_tp", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://contacts.google.com/_/scs/social-static/_/js/k=boq.SocialPeopleHovercardUi.en.0XfxSLuRPx0.es5.O/ck=boq.SocialPeopleHovercardUi.P6bUXTfUthw.L.B1.O/am=FAAI/d=1/exm=_b,_tp/excm=_b,_tp,hovercardwidget/ed=1/wt=2/rs=AGLTcCOKsRakfeYWUgk_QZItr24OohpSag/m=byfTOb,lsjVmc,LEikZe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://contacts.google.com/_/scs/social-static/_/js/k=boq.SocialPeopleHovercardUi.en.0XfxSLuRPx0.es5.O/ck=boq.SocialPeopleHovercardUi.P6bUXTfUthw.L.B1.O/am=FAAI/d=1/exm=LEikZe,_b,_tp,byfTOb,lsjVmc/excm=_b,_tp,hovercardwidget/ed=1/wt=2/rs=AGLTcCOKsRakfeYWUgk_QZItr24OohpSag/m=n73qwf,ws9Tlc,IZT63,e5qFLc,GkRiKb,UUJqVe,O1Gjze,xUdipf,blwjVc,fKUV3e,aurFic,COQbmf,U0aPgd,ZwDk9d,V3dDOb,WO9ee,mI3LFb,Kgsnd,O6y8ed,NpD4ec,PrPYRd,iWP1Yb,MpJwZc,O8k1Cd,NwH0H,OmgaI,HLo3Ef,x60fie,xiqEse,lazG7b,XVMNvd,L1AAkb,KUM7Z,rE6Mgd,s39S4,yDXup,lwddkf,gychg,w9hDv,RMhBfe,qCSYWe,SdcwHb,aW3pY,YLQSd,PQaYAf,pw70Gc,pA3VNb,EFQ78c,Ulmmrd,ZfAoz,mdR7q,CBlRxf,MdUzUe,xQtZb,lPKSwe,QIhFr,JNoxi,MI6k7c,kjKdXe,JUBtjc,pB6Zqd,rHjpXd,yDVVkb,SF3gsd,hKSk3e,rYgfIf,iTsyac,hc6Ubd,KG2eXe,SpsfSb,tfTN8c,o02Jie,VwDzFe,abNXCb,zbML3c,HDvRde,TauLY,Uas9Hd,BVgquf,A7fCU,UgAtXe,pjICDe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/add_grey600_24dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/2x/more_horiz_grey600_24dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=0/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=sy3,hIrTA", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=0/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=sHLrJf", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://hangouts.google.com/_/scs/mss-static/_/js/k=boq-rtc.MeetingsInMoleCalling.en.b6vKj1ve9H8.es5.O/am=CA0hccEB/d=0/rs=AL5CKSHQvpzeEbZTWFmMtCJLmOLsaCaATQ/m=sy2,DWfIKf", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=gapi_iframes/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("ListInstallations", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_4");
    ns_web_url ("fd_4",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=3",
        "METHOD=POST",
        "HEADER=Content-Length:1009",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591937522\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-a:r-438691246049068145","3":["msg-a:r-683700668497511013","msg-a:r-2551475468947294124","msg-f:1683228624807238682"]},{"1":"thread-f:1682375910830314900","3":["msg-f:1682375910830314900"]},{"1":"thread-f:1677877173905985092","3":["msg-f:1677877173905985092"]},{"1":"thread-f:1677714492473192697","3":["msg-f:1677714492473192697","msg-f:1677714499829275445"]},{"1":"thread-f:1677433786711754831","3":["msg-f:1677433786711754831"]},{"1":"thread-f:1676810948739819871","3":["msg-f:1676810948739819871"]},{"1":"thread-f:1675185157886185137","3":["msg-f:1675185157886185137","msg-f:1675185819939548765"]},{"1":"thread-f:1674906863682842913","3":["msg-f:1674906863682842913","msg-f:1674906863949483908"]},{"1":"thread-a:r3116770113039915165","3":["msg-a:r-200720697129466533","msg-f:1673782986513055355","msg-f:1673814470196038655","msg-f:1673823870895844569","msg-f:1673852192069775906","msg-f:1673888376570696994"]},{"1":"thread-f:1673538995944210906","3":["msg-f:1673538995944210906"]}],"2":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://chat-pa.clients6.google.com/chat/v1/conversations/syncallnewevents?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://chat-pa.clients6.google.com/chat/v1/presence/setpresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("fd_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("bind_2");
    ns_web_url ("bind_2",
        "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&SID=C43899E49E29E5D9&RID=68&AID=2&zx=iwqc73cr0nt9&t=1",
        "METHOD=POST",
        "HEADER=Content-Length:1085",
        "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54",
        "HEADER=Origin:https://0.client-channel.google.com",
        "HEADER=X-Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "count"
value: "3"
, key: "ofs"
value: "0"
, key: "req0_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591937575,\"4\":1619591938716,\"5\":\"c1\"},\"3\":{\"1\":{\"1\":\"babel\"}}}"
, key: "req1_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591937576,\"4\":1619591938716,\"5\":\"c2\"},\"3\":{\"1\":{\"1\":\"hangout_invite\"}}}"
, key: "req2_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591937577,\"4\":1619591938716,\"5\":\"c3\"},\"3\":{\"1\":{\"1\":\"babel_presence_last_seen\"}}}"
]",
        BODY_END
    );

    ns_end_transaction("bind_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("syncallnewevents");
    ns_web_url ("syncallnewevents",
        "URL=https://chat-pa.clients6.google.com/chat/v1/conversations/syncallnewevents?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:158",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591937_d860d495ff4c1eadd6f44791b0f05bf468f3d3ee",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],["lcsw_hangouts_6E1D8410","8BB52C5AD454C5B7"],[],"en"],1619591635930000,[],null,null,false,[],1048576]",
        BODY_END,
        INLINE_URLS,
            "URL=https://contacts.google.com/_/scs/social-static/_/js/k=boq.SocialPeopleHovercardUi.en.0XfxSLuRPx0.es5.O/ck=boq.SocialPeopleHovercardUi.P6bUXTfUthw.L.B1.O/am=FAAI/d=1/exm=A7fCU,BVgquf,CBlRxf,COQbmf,EFQ78c,GkRiKb,HDvRde,HLo3Ef,IZT63,JNoxi,JUBtjc,KG2eXe,KUM7Z,Kgsnd,L1AAkb,LEikZe,MI6k7c,MdUzUe,MpJwZc,NpD4ec,NwH0H,O1Gjze,O6y8ed,O8k1Cd,OmgaI,PQaYAf,PrPYRd,QIhFr,RMhBfe,SF3gsd,SdcwHb,SpsfSb,TauLY,U0aPgd,UUJqVe,Uas9Hd,UgAtXe,Ulmmrd,V3dDOb,VwDzFe,WO9ee,XVMNvd,YLQSd,ZfAoz,ZwDk9d,_b,_tp,aW3pY,abNXCb,aurFic,blwjVc,byfTOb,e5qFLc,fKUV3e,gychg,hKSk3e,hc6Ubd,iTsyac,iWP1Yb,kjKdXe,lPKSwe,lazG7b,lsjVmc,lwddkf,mI3LFb,mdR7q,n73qwf,o02Jie,pA3VNb,pB6Zqd,pjICDe,pw70Gc,qCSYWe,rE6Mgd,rHjpXd,rYgfIf,s39S4,tfTN8c,w9hDv,ws9Tlc,x60fie,xQtZb,xUdipf,xiqEse,yDVVkb,yDXup,zbML3c/excm=_b,_tp,hovercardwidget/ed=1/wt=2/rs=AGLTcCOKsRakfeYWUgk_QZItr24OohpSag/m=Wt6vjf,_latency,FCpbqb,WhJNk", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://chat-pa.clients6.google.com/chat/v1/clients/setactiveclient?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://contacts.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("syncallnewevents", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_10");
    ns_web_url ("log_10",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1698",
        "HEADER=Authorization:SAPISIDHASH 092ef09ee5d3dc0bb0d87c6542f62443ca016575",
        "HEADER=Origin:https://contacts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_contactsuiserver_20210425.15_p0"]],241,[["1619591938173",null,[],null,null,null,null,"[1619591938169,[[\"2397168675742140944\",null,[[669,1]]],[\"16147638372540442232\",null,[[675,1]]],[\"14719340685975485085\",null,[[777,1]]],[\"17077408715954654437\",null,[[1282,1]]],[\"3318688667027929436\",null,[[674,1]]],[\"5790177495296899286\",null,[[0,1]]],[\"16829267986558572790\",null,[[0,1]]],[\"16339156775003354937\",null,[[311,1]]],[\"749851692583976763\",null,[[2,1]]],[\"15419336178855610526\",null,[[583,1]]],[\"17276521865292187132\",null,[[264,1]]],[\"8257051839445688306\",null,[[1281,1]]],[\"7792735449360349632\",null,[[1282,1]]],[\"7094487270460551484\",null,[[5,1]]],[\"12563104964214410683\",null,[[519,1]]],[\"15605813632677093659\",null,[[28,1]]],[\"17914751415692637656\",null,[[2,1]]],[\"9797767207516844257\",null,[[0,1]]],[\"14906952326733574741\",null,[[0,1]]],[\"6667106912793420619\",null,[[0,1]]],[\"10118692516388306266\",null,[[0,1]]],[\"6342145065879578001\",null,[[5,1]]],[\"13596961294000664596\",null,[[519,1]]],[\"2107494750385856652\",null,[[28,1]]],[\"1309831198388189068\",null,[[2,1]]],[\"522022639063469804\",null,[[0,1]]],[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[150,1]]],[\"3079121564595244695\",null,[[122,1]]],[\"10652791942255425261\",null,[[2075,1]]],[\"4132870161583308123\",null,[[60107,1]]]],null,null,\"[1,\\\"Xhmznf_PL\\\"]\"]",null,null,null,1522082812,null,null,-19800,[null,[],null,"[[],[],[1757124,1763433,1772879],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1619591938173",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_10", NS_AUTO_STATUS);
    ns_start_transaction("setpresence");
    ns_web_url ("setpresence",
        "URL=https://chat-pa.clients6.google.com/chat/v1/presence/setpresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=OPTIONS",
        "HEADER=Content-Length:122",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591937_d860d495ff4c1eadd6f44791b0f05bf468f3d3ee",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("setpresence", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("setpresence_2");
    ns_web_url ("setpresence_2",
        "URL=https://chat-pa.clients6.google.com/chat/v1/presence/setpresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:122",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591938_14f733b6b38e4431a59206abfaa2c9fc1626d14b",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],["lcsw_hangouts_6E1D8410","8BB52C5AD454C5B7"],null,"en"],[747,40]]",
        BODY_END,
        INLINE_URLS,
            "URL=https://hangouts.google.com/_/scs/chat-static/_/js/k=chat.wbl.en.4e_PRnB3Gio.O/am=wEPAAYIAQDgCCCgsog/d=0/rs=AGNGyv0ot3qYzkjgfio0IR-kjdzgNPVDvw/m=p", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("setpresence_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_5");
    ns_web_url ("fd_5",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=4",
        "METHOD=POST",
        "HEADER=Content-Length:536",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591938554\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1672746731052745387","3":["msg-f:1672746731052745387","msg-f:1672777334004585876"]},{"1":"thread-f:1672621842925543203","3":["msg-f:1672621842925543203","msg-f:1672622150740437850"]},{"1":"thread-f:1672470014957413143","3":["msg-f:1672470014957413143","msg-f:1672480395285182638"]},{"1":"thread-f:1672291149730324392","3":["msg-f:1672291149730324392","msg-f:1672291495516716220"]},{"1":"thread-f:1671930630219176822","3":["msg-f:1671930630219176822","msg-f:1671931395161887979","msg-f:1671950330925895562"]}],"2":2}",
        BODY_END
    );

    ns_end_transaction("fd_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("setactiveclient");
    ns_web_url ("setactiveclient",
        "URL=https://chat-pa.clients6.google.com/chat/v1/clients/setactiveclient?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:132",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591938_14f733b6b38e4431a59206abfaa2c9fc1626d14b",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],["lcsw_hangouts_6E1D8410","8BB52C5AD454C5B7"],null,"en"],true,null,120,true]",
        BODY_END
    );

    ns_end_transaction("setactiveclient", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("setpresence_3");
    ns_web_url ("setpresence_3",
        "URL=https://chat-pa.clients6.google.com/chat/v1/presence/setpresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST"
    );

    ns_end_transaction("setpresence_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("querypresence_2");
    ns_web_url ("querypresence_2",
        "URL=https://chat-pa.clients6.google.com/chat/v1/presence/querypresence?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:150",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591938_14f733b6b38e4431a59206abfaa2c9fc1626d14b",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],["lcsw_hangouts_6E1D8410","8BB52C5AD454C5B7"],null,"en"],[["102932755784749561560"]],[2,3,10]]",
        BODY_END
    );

    ns_end_transaction("querypresence_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_6");
    ns_web_url ("fd_6",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=5",
        "METHOD=POST",
        "HEADER=Content-Length:452",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591939269\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1671902904191054055","3":["msg-f:1671902904191054055"]},{"1":"thread-f:1671642271821816649","3":["msg-f:1671642271821816649","msg-f:1671642283729122292"]},{"1":"thread-f:1671572574295202562","3":["msg-f:1671572574295202562","msg-f:1671572576965910209"]},{"1":"thread-f:1671387519857248452","3":["msg-f:1671387519857248452","msg-f:1671387527797080188"]},{"1":"thread-f:1671287560470597954","3":["msg-f:1671287560470597954"]}],"2":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://mail.google.com/mail/u/0/sw.js?offline_allowed=1", "HEADER=Cache-Control:max-age=0", "HEADER=Service-Worker:script", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("fd_6", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_7");
    ns_web_url ("fd_7",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=6",
        "METHOD=POST",
        "HEADER=Content-Length:396",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591940128\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1670192958183104061","3":["msg-f:1670192958183104061","msg-f:1670193400903471421"]},{"1":"thread-f:1669487694589599445","3":["msg-f:1669487694589599445"]},{"1":"thread-f:1668936936159725380","3":["msg-f:1668936936159725380"]},{"1":"thread-f:1668312532308976902","3":["msg-f:1668312532308976902"]},{"1":"thread-f:1668310523549721514","3":["msg-f:1668310523549721514"]}],"2":2}",
        BODY_END
    );

    ns_end_transaction("fd_7", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_8");
    ns_web_url ("fd_8",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=7",
        "METHOD=POST",
        "HEADER=Content-Length:84",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591941094\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1668298776144322168","3":["msg-f:1668298776144322168"]}],"2":2}",
        BODY_END
    );

    ns_end_transaction("fd_8", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_9");
    ns_web_url ("fd_9",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=8",
        "METHOD=POST",
        "HEADER=Content-Length:409",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591941498\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1664837921781375831","3":["msg-f:1664837921781375831"]},{"1":"thread-f:1664383927329503026","3":["msg-f:1664383927329503026","msg-f:1664383927801502322","msg-f:1664384102402449467","msg-f:1664384104683804260"]},{"1":"thread-f:1663470462091000616","3":["msg-f:1663470462091000616","msg-f:1663477319348949159"]},{"1":"thread-f:1663402963104915165","3":["msg-f:1663402963104915165"]}],"2":2}",
        BODY_END
    );

    ns_end_transaction("fd_9", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_11");
    ns_web_url ("log_11",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:9477",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],793,[["1619591932467",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[]],[19394,null,[],25],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,0]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],1,null,null,null,null,null,[]],["1619591932475",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[6],null,null,null,null,null,null,[]],[38365,null,[],null],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,1]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],2,null,null,null,null,null,[]],["1619591932475",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[6],null,null,null,null,null,null,[]],[85046,null,[38365],null],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,2]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],3,null,null,null,null,null,[]],["1619591932475",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[6],null,null,null,null,null,null,[]],[96537,null,[38365],null],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,3]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],4,null,null,null,null,null,[]],["1619591932475",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[6],null,null,null,null,null,null,[]],[96536,null,[38365],null],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,4]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],5,null,null,null,null,null,[]],["1619591932475",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[],null,false,\"iv.os\"],null,null,null,null,null,null,[null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[6],null,null,null,null,null,null,[],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[-1,1,50]],[82433,null,[38365],null],null,null,[],[],[],[]],[1,1,false,false,6,null,0,[1,1,null,null]],null,null,null,null,null,null,null,null,null,null,[null,null,5]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],6,null,null,null,null,null,[]]],"1619591942474",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_11", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_12");
    ns_web_url ("log_12",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:456",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],932,[["1619591934693",null,[],null,null,null,null,"[[null,null,[1,135,1],null,\"1012\",[],336195648,null,3,null,null,null,4],1,1,[0,6080,5560,null,1],null,null,null,null,[1,null,null,false]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[20090307],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1619591944706",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://mail.google.com/mail/u/0/?sw=2", "HEADER=x-gmail-sw-cache-warming-request:true", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_12", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("flushimpressions");
    ns_web_url ("flushimpressions",
        "URL=https://mail.google.com/mail/u/0/flushimpressions?ui=2&ik=3ea5edf7c4&jsver=N605Puzl050.en.&cbl=gmail_fe_210421.06_p2&_reqid=743731&pcd=1&cfact=7113%2C7766%2C7518%2C7433%2C7465%2C7594%2C7677%2C7514%2C7782%2C7753%2C7424%2C7057%2C7416%2C7758%2C7628%2C7682%2C7030%2C7546%2C7496&cfinact=7085%2C7767%2C7577%2C7595%2C7563%2C7683%2C7640%2C7826%2C7802%2C7574%2C7117%2C7544%2C7086%2C7799%2C7690%2C7521%2C7421%2C7083%2C7580%2C7543%2C7512%2C7689%2C7757%2C7684%2C7058%2C7754%2C7082%2C7056%2C7084&mb=0&rt=j",
        "METHOD=POST",
        "HEADER=Content-Length:22",
        "HEADER=X-Same-Domain:1",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;GMAIL_IMP;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "user_domain"
value: "gmail.com"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://www.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://mail.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=b?sw=2", "HEADER=x-gmail-sw-cache-warming-request:true", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("flushimpressions", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_13");
    ns_web_url ("log_13",
        "URL=https://www.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1609",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],793,[["1619591936288",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,false,\"iv.os\",null,[false,false,0,1],[[[6,1],[2,1],[9,1],[4,1]],false]],null,null,null,null,null,null,[null,null,null,null,null,null,[],[],[],[],null,null,[null,null,[115463760,143205392,0,null,[]],null,0]],[1,1,false,false,6,null,0,[1,1,null,null]]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1619591946309",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=b/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=a?sw=2", "HEADER=x-gmail-sw-cache-warming-request:true", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=a,b/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=m,m_i,i20jfd,lKrWxc,hkjXJ,gYOl6d,HXLjIb,DL8jZe,xaQcye,oRmHt,E1P0kd,pE92lb,v2eEBc?sw=2", "HEADER=x-gmail-sw-cache-warming-request:true", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=DL8jZe,E1P0kd,HXLjIb,a,b,gYOl6d,hkjXJ,i20jfd,lKrWxc,m,m_i,oRmHt,pE92lb,v2eEBc,xaQcye/ed=1/im=1/dg=0/br=1/wt=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=ebdd,sps,l,spit,t,it,lLYctc,anc?sw=2", "HEADER=x-gmail-sw-cache-warming-request:true", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_13", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_14");
    ns_web_url ("log_14",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:308",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],996,[["1619591937140",null,[],null,null,null,null,"[[[66684,null,[],2],[1]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591947150",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_14", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("bind_3");
    ns_web_url ("bind_3",
        "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&SID=C43899E49E29E5D9&RID=69&AID=8&zx=brdm06dfv2n5&t=1",
        "METHOD=POST",
        "HEADER=Content-Length:717",
        "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54",
        "HEADER=Origin:https://0.client-channel.google.com",
        "HEADER=X-Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "count"
value: "2"
, key: "ofs"
value: "3"
, key: "req0_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591951022,\"4\":1619591940366,\"5\":\"c4\"},\"3\":{\"1\":{\"1\":\"tango_web\"}}}"
, key: "req1_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591951029,\"4\":1619591940366,\"5\":\"c5\"},\"3\":{\"1\":{\"1\":\"tango_web\"}}}"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyB1j3mFq6w9iUpl8m8UNezy4TBwy9Eb8b4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyCIMH2ks6VPAfRC2lqU_Snz1Lo76XGdnlc", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("bind_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("subscriptions");
    ns_web_url ("subscriptions",
        "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyCIMH2ks6VPAfRC2lqU_Snz1Lo76XGdnlc",
        "METHOD=POST",
        "HEADER=Content-Length:98",
        "HEADER=Authorization:SAPISIDHASH 1619591951_8318d0bd88ace1766a4eaef88243ec4fee4ecfe0",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[["cGF0dGFuYXlha21hbmlzaGEwOUBnbWFpbC5jb20",null,"MJ3J7Om9d1o6IRLc",11,"DP5tNqjclSw="],false,null]",
        BODY_END
    );

    ns_end_transaction("subscriptions", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("subscriptions_2");
    ns_web_url ("subscriptions_2",
        "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyB1j3mFq6w9iUpl8m8UNezy4TBwy9Eb8b4",
        "METHOD=POST",
        "HEADER=Content-Length:59",
        "HEADER=Authorization:SAPISIDHASH 1619591951_8318d0bd88ace1766a4eaef88243ec4fee4ecfe0",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("subscriptions_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("syncallnewevents_2");
    ns_web_url ("syncallnewevents_2",
        "URL=https://chat-pa.clients6.google.com/chat/v1/conversations/syncallnewevents?key=AIzaSyD7InnYR3VKdb4j2rMUEbTCIr2VyEazl6k&alt=protojson",
        "METHOD=POST",
        "HEADER=Content-Length:159",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591952_1f57cfd1e876e54ff5fa82447cd8cf587d153070",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[[6,3,"babel-chat.frontend_20210425.07_p0",1619385420],["lcsw_hangouts_6E1D8410","8BB52C5AD454C5B7"],[],"en"],"1619591639629000",[],null,null,true,[],1048576]",
        BODY_END
    );

    ns_end_transaction("syncallnewevents_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_15");
    ns_web_url ("log_15",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:7815",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],893,[["1619591951109",null,[],null,null,null,null,"[\"1619591946094\",[[null,null,[[\"0\",\"11\"]],\"-4959446818712997655\"],[null,null,[[\"0\",\"5\"]],\"942790293620930274\"],[null,null,[[\"0\",\"5\"]],\"-6599056016198770034\"],[null,null,[[\"0\",\"5\"]],\"7367863417336259419\"],[null,null,[[\"51680114\",\"1\"]],\"2783112258024556088\"],[null,null,[[\"38560034\",\"1\"]],\"-3967474719957906250\"],[null,null,[[\"1\",\"1\"]],\"8156935523321976108\"],[null,null,[[\"3837\",\"1\"]],\"-4481849414583003945\"],[null,null,[[\"0\",\"1\"]],\"7060951837351133665\"],[null,null,[[\"0\",\"2\"],[\"50\",\"9\"]],\"-2516723497608421612\"],[null,null,[[\"0\",\"1\"]],\"-8116503699014023182\"],[null,null,[[\"0\",\"1\"]],\"2562181559016560727\"],[null,null,[[\"107\",\"3\"]],\"5831036667936232446\"],[null,null,[[\"0\",\"1\"]],\"1034946293555366177\"],[null,null,[[\"107\",\"1\"]],\"-161189394366280054\"],[null,null,[[\"151\",\"1\"]],\"-3070150658141692169\"],[null,null,[[\"1112916\",\"1\"]],\"1872363352292334908\"],[null,null,[[\"0\",\"1\"]],\"-6188756759949672964\"],[null,null,[[\"0\",\"1\"]],\"8668660156681077229\"],[null,null,[[\"0\",\"1\"]],\"-8692212986168768267\"],[null,null,[[\"3803\",\"1\"]],\"3674416918965118215\"],[null,null,[[\"0\",\"1\"]],\"-2626802438754436212\"],[null,null,[[\"50\",\"5\"],[\"0\",\"1\"]],\"-7442833728258606944\"],[null,null,[[\"0\",\"1\"]],\"6064600358658876886\"],[null,null,[[\"3837\",\"1\"]],\"-9071897846402698467\"],[null,null,[[\"0\",\"1\"]],\"-2785542543111349306\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],1,null,null,null,null,null,[]],["1619591951109",null,[],null,null,null,null,"[\"1619591946094\",[[null,null,[[\"13\",\"1\"],[\"1\",\"1\"]],\"8592769406416781108\"],[null,null,[[\"28\",\"1\"],[\"11\",\"1\"]],\"-7558660779622016800\"],[null,null,[[\"1\",\"2\"]],\"-7121550470961355275\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[\\\"ItemsCoordinator.getStoredItemDetailsOrNull\\\",1]]],[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],2,null,null,null,null,null,[]],["1619591951110",null,[],null,null,null,null,"[\"1619591946094\",[[null,null,[[\"0\",\"1\"]],\"8592769406416781108\"],[null,null,[[\"1\",\"1\"]],\"-7558660779622016800\"],[null,null,[[\"1\",\"1\"]],\"-7121550470961355275\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[\\\"ItemsStorageUpdateHintCalculator.getItemsStorageUpdateHint\\\",0]]],[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],3,null,null,null,null,null,[]],["1619591951110",null,[],null,null,null,null,"[\"1619591946094\",[[null,null,[[\"0\",\"1\"]],\"8592769406416781108\"],[null,null,[[\"1\",\"1\"]],\"-7558660779622016800\"],[null,null,[[\"1\",\"1\"]],\"-7121550470961355275\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[\\\"ItemsCoordinator.getCurrentWriteVersion\\\",0]]],[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],4,null,null,null,null,null,[]]],"1619591961114",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=hfwkygewod5o", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/archive_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/delete_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/drafts_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_15", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_16");
    ns_web_url ("log_16",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:459",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],78,[["1619591979222",null,[],null,null,null,null,"[null,null,null,null,null,null,null,null,null,null,null,[[null,1,null,null,null,null,23,837856527]],null,null,[1366,768],null,null,null,null,null,null,null,null,[],null,null,[]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591979222",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_16", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_17");
    ns_web_url ("log_17",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:359",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],729,[["1619591979663",null,[],null,null,null,null,"[[[46975,null,[],null],23,[]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591979663",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_17", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_18");
    ns_web_url ("log_18",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1237",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],373,[["1619591979470",null,[],null,null,null,null,"[69,40400,23,1,\"369134291.0\",\"-wKJYP2_BJaDtQaz-brABg\",null,null,null,\"en\",\"IND\",1,7,51370,null,true,false,null,\"og-cdeb4e78-42c5-4f4f-b957-69e2e1230967\",null,null,null,null,19,true,false,32955,null,null,null,null,null,null,null,null,false,null,1,null,null,298,null,null,null,null,null,null,null,null,null,null,null,null,null,false]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700817,3700830,3700858,3700874,3700875],[]]"],null,null,null,[],1,null,null,null,null,null,[]],["1619591979471",null,[],null,null,null,null,"[63,40400,23,1,\"369134291.0\",\"-wKJYP2_BJaDtQaz-brABg\",null,null,null,\"en\",\"IND\",1,7,51371,null,true,false,null,\"og-cdeb4e78-42c5-4f4f-b957-69e2e1230967\",null,null,null,null,null,true,false,32955,null,null,null,null,null,null,null,null,false,null,2,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,false]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[3700817,3700830,3700858,3700874,3700875],[]]"],null,null,null,[],2,null,null,null,null,null,[]]],"1619591980480",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_18", NS_AUTO_STATUS);
    ns_page_think_time(0.37);

    //Page Auto splitted for 
    ns_start_transaction("cleardot_gif");
    ns_web_url ("cleardot_gif",
        "URL=https://mail.google.com/mail/u/0/images/cleardot.gif?zx=oeko7j85m2fb",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=GMAIL_AT;COMPASS;COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("cleardot_gif", NS_AUTO_STATUS);
    ns_page_think_time(0.045);

    //Page Auto splitted for Link 'image' Clicked by User
    ns_start_transaction("index_2");
    ns_web_url ("index_2",
        "URL=https://drive.google.com/?ogsrc=32&tab=mo&authuser=0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-User:?1",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/drive/?ogsrc=32&tab=mo", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/ss/k=drive_fe.main.TU1p8w7Em-0.L.W.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gszwcnavBz0xaEYc_fKOMywsCuOX1A", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=1/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=b", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=1/exm=b/ed=1/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=RsR2Mc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_19");
    ns_web_url ("log_19",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:689",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=1/exm=RsR2Mc,b/ed=1/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=core", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://fonts.googleapis.com/css?lang=en&family=Product+Sans|Roboto:400,700", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_19", NS_AUTO_STATUS);
    ns_page_think_time(570.176);

    ns_start_transaction("index_3");
    ns_web_url ("index_3",
        "URL=https://drive.google.com/drive/?ogsrc=32&tab=mo",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1bt,sy122,sy1e0,sy1f6,VHjwLe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=syv3,sy1jh,syvp,NpD4ec,ws9Tlc,syxf,syxg,IZT63,sywy,sywz,syxe,xUdipf,ZwDk9d,xiqEse,hJDwEc,syvf,syve,syvg,syx0,syvh,syx1,syx7,syxa,syvi,syx2,syx8,syxb,syx9,syxc,YNjGDd,em1e,PrPYRd,syv8,MpJwZc,Y9atKf,s39S4,sy1k7,Ncyg4c,joc3q,syg,YeEaif,sywx,sy1gl,zwYpSc,syw2,syvz,syw0,syw1,L1AAkb,VybXsf,pw70Gc,syx6,SF3gsd,QIhFr,uTmWc,HyaRXb,bkte1c,zydI6,yFBrU,mJmK0b,sy1kd,sy1lp,sy1ly,m18GNd", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=lgcDZ,XBbrG,rAX3yc,GjvfCe,jZTsEb,MbE88c,sy1am,sy1ar,sy1at,syyq,sy16e,sy1al,sy1aq,sy1as,sy1au,sy1av,sy1aw,sy1ax,apps,syth,syv9,syva,sy160,sy1c2,sy1ce,sy1j5,syyu,sywp,sywr,syv7,sy161,sy1c1,sy1d4,sy1j0,sy1j1,sy1j4,sy1j3,sy1j7,sy1j6,syzf,syzi,syyv,sywq,syzm,sy1jt,sy10w,sy13i,sy16a,sy1be,sy1bh,sy1ci,sy1g8,sy1gm,sy1gn,sy1gp,sy1jn,sy1k5,sy1c3,sy1cf,sy1cq,sy1dh,sy1d6,sy1iz,syzg,sy1js,sy1ed,sy1go,sy18o,uGw66e,sy1jo,syzv,sy12z,sy130,X5fVc,sy1b9,sy1bg,sy1c4,sy1c6,sy1cb,sy1cr,em1j,em1k,em1l,em1m,sy1d5,sy1d9,sy1dp,sy1e2,sy1el,sy1fl,sy1ff,sy1ga,CgsJMb,sy1ja,sy1jx,sy1k6,sy1oy,sy1bx,sy1cg,sy1jp,wuMs4d,TDCeDd,sy1jq,pxqpub,sy14t,sy166,syzc,sy18k,sy18l,sy18m,sy18n,sy1b2,sy129,sy165,sy1b1,sy1ba,sy1bb,sy1bc,sy18j,sy1b7,sy1b5,sy1bd,sy1b8,sy1b0,rWf7ge,G00Z0c,sy8,sy4,sy15n,sy15m,sy1ky,sy1kp,sy1kq,sy1kx,sy1kr,sy1kz,sy1ks,sy1kt,sy1l0,sy13c,sy1kn,sy1ku,sy1lv,sy1m6,sy7,sy1bj,sy10v,sy1cw,sy1m3,sy1m7,sy1mb,sy1md,uNVIRe,sy1mc,ZTp7Ob", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("index_3", NS_AUTO_STATUS);
    ns_page_think_time(570.295);

    ns_start_transaction("index_4");
    ns_web_url ("index_4",
        "URL=https://drive.google.com/drive/",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=r89iMe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=yk73j4qnakrp", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/images/branding/product/1x/drive_2020q4_48dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://clients6.google.com/drive/v2internal/apps?openDrive=true&reason=301&syncType=0&errorRecovery=false&fields=kind%2CdefaultAppIds%2Citems(kind%2Cid%2CuseByDefault%2Cname%2CopenUrlTemplate%2CprimaryMimeTypes%2CsecondaryMimeTypes%2CcreateUrl%2CcreateInFolderTemplate%2CobjectType%2CsupportsCreate%2CsupportsImport%2CsupportsMultiOpen%2CsupportsOfflineCreate%2Cinstalled%2Cauthorized%2CproductUrl%2CprimaryFileExtensions%2CsecondaryFileExtensions%2CshortDescription%2ClongDescription%2CproductId%2Cremovable%2Cicons(iconUrl%2Csize%2Ccategory)%2Ctype%2CchromeExtensionIds%2CrequiresAuthorizationBeforeOpenWith%2ChasDriveWideScope%2CdriveBranded%2CdriveSource%2CsupportsMobileBrowser%2CsupportsTeamDrives%2ChasGsmListing)&languageCode=en&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://clients6.google.com/drive/v2internal/about?openDrive=true&reason=301&syncType=0&errorRecovery=false&fields=kind%2Cuser(kind%2Cid%2CpermissionId%2CemailAddressFromAccount%2Cdomain)%2CquotaBytesTotal%2CquotaBytesUsed%2CquotaBytesUsedAggregate%2CquotaBytesUsedInTrash%2CquotaBytesByService%2CquotaType%2CrootFolderId%2CdomainSharingPolicy%2ClargestChangeId%2CimportFormats%2CgsuiteSubscriptionInfo(trialMillisRemaining%2CtrialEndTime%2Cstatus)%2CteamDashboardCapabilities(canAdministerTeam%2CcanManageInvites)%2CgraceQuotaInfo%2CquotaStatus&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://clients6.google.com/drive/v2internal/settings?openDrive=true&reason=301&syncType=0&errorRecovery=false&namespace=DRIVE_FE&namespace=DRIVE_BE&namespace=SLACK&namespace=TEAMKIT&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sywc,sywh,sy1cn,E49vvd,rjqLAb,JBLWee,sy12c,sy12d,sy135,sy14v,sy159,sy137,sy15d,sywt,sy12e,sy131,sy136,syzd,sy134,sy14w,sy15g,sy158,sy15a,sy15h,sy15e,sy15i,syws,sy15k,syv5,sywu,sy138,sy14r,sy14u,sy14x,sy15c,sy15o,sy15f,sy15j,sy15l,syx4,em1b,em1c,em1d,sy15b,sy15t,sy15u,OPDtvd,syxd,sy1ji,vfuNJf,xs1Gy,syx3,hc6Ubd,syvj,syvk,sy1kl,XIZkGb,sy16h,sy16g,sy16f,FUObwc,sy16d,syxx,sy1d7,sy1kj,db6Kj,sy1ch,kJBlxb,l8jpMe,H3SiYb,v8SDMe,Vpv1ee,d4r7Ed,sy1da,sy1kk,ZYlyzb,SvxIpf,uxrQvc,AEkvQe,oK9xM,sy1ma,zYvxSe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/pdf", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/video/mp4", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.openxmlformats-officedocument.wordprocessingml.document", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/image/jpeg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1la,sy1lb,sy1ld,sy1lc,sy1le,sy1bu,sy1lf,kfsWbe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=dkX98d,Zvu1ic,bb6D7,lnUr8e,qpzqNc,lDtxSe,I0Ibec,sy154,KATmAc,sy1lr,o4UUxd", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy16c,Ly3VJe,F2Fnfd,kxfun", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://fonts.gstatic.com/s/productsans/v13/pxiDypQkot1TnFhsFMOfGShVF9eO.woff2", "HEADER=Origin:https://drive.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.google-apps.document", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.google-apps.spreadsheet", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/zip", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/video/x-matroska", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1jb,k2LvNc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://clients6.google.com/drive/v2beta/apps?openDrive=true&reason=700&syncType=0&errorRecovery=false&fields=items(icons%5Bcategory%3D%27application%27%5D%2Cicons(size%2CiconUrl)%2Cid%2Cname%2CopenUrlTemplate%2CprimaryFileExtensions%2CprimaryMimeTypes%2CproductId%2CrankingInfo%2CsecondaryFileExtensions%2CsecondaryMimeTypes%2Ckind)%2Ckind&appQueryScope=all_webstore&languageCode=en&retryCount=0&key=AIzaSyBc1bLOZpOtg3-qgMjSQ6pmn6HbE2zjzJg", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.google-apps.presentation", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.openxmlformats-officedocument.presentationml.presentation", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_4", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("apps");
    ns_web_url ("apps",
        "URL=https://clients6.google.com/drive/v2internal/apps?openDrive=true&reason=301&syncType=0&errorRecovery=false&fields=kind%2CdefaultAppIds%2Citems(kind%2Cid%2CuseByDefault%2Cname%2CopenUrlTemplate%2CprimaryMimeTypes%2CsecondaryMimeTypes%2CcreateUrl%2CcreateInFolderTemplate%2CobjectType%2CsupportsCreate%2CsupportsImport%2CsupportsMultiOpen%2CsupportsOfflineCreate%2Cinstalled%2Cauthorized%2CproductUrl%2CprimaryFileExtensions%2CsecondaryFileExtensions%2CshortDescription%2ClongDescription%2CproductId%2Cremovable%2Cicons(iconUrl%2Csize%2Ccategory)%2Ctype%2CchromeExtensionIds%2CrequiresAuthorizationBeforeOpenWith%2ChasDriveWideScope%2CdriveBranded%2CdriveSource%2CsupportsMobileBrowser%2CsupportsTeamDrives%2ChasGsmListing)&languageCode=en&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591983_7fcd6b3cd4ff71fc4cd92797e03d12d0c464ce91_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("apps", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("settings");
    ns_web_url ("settings",
        "URL=https://clients6.google.com/drive/v2internal/settings?openDrive=true&reason=301&syncType=0&errorRecovery=false&namespace=DRIVE_FE&namespace=DRIVE_BE&namespace=SLACK&namespace=TEAMKIT&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591983_7fcd6b3cd4ff71fc4cd92797e03d12d0c464ce91_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("settings", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("about");
    ns_web_url ("about",
        "URL=https://clients6.google.com/drive/v2internal/about?openDrive=true&reason=301&syncType=0&errorRecovery=false&fields=kind%2Cuser(kind%2Cid%2CpermissionId%2CemailAddressFromAccount%2Cdomain)%2CquotaBytesTotal%2CquotaBytesUsed%2CquotaBytesUsedAggregate%2CquotaBytesUsedInTrash%2CquotaBytesByService%2CquotaType%2CrootFolderId%2CdomainSharingPolicy%2ClargestChangeId%2CimportFormats%2CgsuiteSubscriptionInfo(trialMillisRemaining%2CtrialEndTime%2Cstatus)%2CteamDashboardCapabilities(canAdministerTeam%2CcanManageInvites)%2CgraceQuotaInfo%2CquotaStatus&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591983_7fcd6b3cd4ff71fc4cd92797e03d12d0c464ce91_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=FEOFHe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1ia,qZWTYd", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("about", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal");
    ns_web_url ("v2internal",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3D2mjbgz39czu%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:2386",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====2mjbgz39czu=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/files?openDrive=false&reason=121&syncType=0&errorRecovery=false&q=trashed%20%3D%20false%20and%20'machineRoot'%20in%20folderFeatures&fields=kind%2CnextPageToken%2Citems(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CincompleteSearch&appDataFilter=NO_APP_DATA&spaces=drive&maxResults=50&supportsTeamDrives=true&includeItemsFromAllDrives=true&corpora=default&orderBy=folder%2Ctitle_natural%20asc&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619591984_6485e1ceb7f237744626208b7e76a0e438829ebb_u
x-goog-authuser: 0

--=====2mjbgz39czu=====--",
        BODY_END,
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=customization", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy18f,account", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("v2internal", NS_AUTO_STATUS);
    ns_page_think_time(571.262);

    ns_start_transaction("my_drive");
    ns_web_url ("my_drive",
        "URL=https://drive.google.com/drive/my-drive",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=LdUV1b,sOXFj,OAj4Sd,aDmR9e,sy162,mQNLKe,sy169,LPQUTd,syx5,q0xTif,sy1l3,sy1l2,sy1l4,sy1l7,sy1l5,sy1l6,syti,sy1l8,sy1l9,sy1m4,sy106,sy13f,sy1cj,sy1lg,sy1lh,sy1m5,sy1m8,sy1ke,sy1kg,sy1m1,sy1m2,sy1m9,sy1me,q2lkmb,Grzhuf,U3dEG,jH2Pd,pxtbwb,gewHbe,aNz4G,zQdOjc,sywo,sy1lj,sy1lk,pAisf,sy1ll,LWRO0e,zNm7zc,f4w9Ve", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=CHaMoc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy16y,sy16x,sy171,sy17t,sy16l,sy16n,sy18r,sy16z,sy170,sy172,sy17q,sy17s,sy17u,sy18p,sy1ay,syyw,sy16w,sy175,sy17r,sy18q,sy195,sy1az,sy1b3,sy1b4,sy1b6,sy1bf,companion", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=syxw,BDbGue,vCpxhb,WCIEHd,dS9ppc,uflob,wEobMe,vkwSxb,kmCYTd", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=RMhBfe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1k8,WAgSub,zgISHf,sy12y,v7L3pd,FlLXHb,sy185,help,sy12x,TwRwKf,UVo7lb,JXvesd,NZLdkc,CCKT8e", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("my_drive", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("apps_2");
    ns_web_url ("apps_2",
        "URL=https://clients6.google.com/drive/v2beta/apps?openDrive=true&reason=700&syncType=0&errorRecovery=false&fields=items(icons%5Bcategory%3D%27application%27%5D%2Cicons(size%2CiconUrl)%2Cid%2Cname%2CopenUrlTemplate%2CprimaryFileExtensions%2CprimaryMimeTypes%2CproductId%2CrankingInfo%2CsecondaryFileExtensions%2CsecondaryMimeTypes%2Ckind)%2Ckind&appQueryScope=all_webstore&languageCode=en&retryCount=0&key=AIzaSyBc1bLOZpOtg3-qgMjSQ6pmn6HbE2zjzJg",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy184,dajgr,sy18s,contacts,sy1j8,cNpMff,sy176,sy1ev,sy1et,sy1es,sy1f0,sy1f2,sy1ex,sy1ez,sy1eu,sy1ew,sy1f9,sy1ey,sy1fg,sy1ak,sy1bn,sy1fs,sy11t,sy1fk,sy1g3,sy1aj,sy1e7,sy1f8,sy1fx,sy1fy,sy1g4,sy1gj,sy1j2,sy1jc,sy1jd,srch", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1hx,sy167,sy1e6,sy1hw,sy1hy,sy1hz,sy1i2,sy1i5,sy1i8,sy1i7,bAcfqb", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=lqbZtc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=gKCChf,sy1cc,sy1ls,NEq59c,sy1ct,kodfyd,arATOc,syxt,syxs,F46aid,sy1dc,y9Qy5d,SHdiIe,sy1by,sy1bw,sy1bz,sy1d0,sy12j,sy1c0,sy1ca,sy1fn,sy1d1,sy1d2,sy1ln,sy1ck,sy1d8,sy1ki,sy1lm,sy1lo,YYGnn", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://people-pa.clients6.google.com/v2/people?person_id=102932755784749561560&person_id=106251321010044521290&request_mask.include_container=PROFILE&request_mask.include_container=DOMAIN_PROFILE&request_mask.include_field.paths=person.metadata.best_display_name&request_mask.include_field.paths=person.photo&request_mask.include_field.paths=person.email&merged_person_source_options.included_profile_states=DASHER_ADMIN_DISABLED&core_id_params.enable_private_names=true&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.3gGou_DPQGQ.O/rt=j/m=qgl,q_dnp,q_sf,q_pc,qdid,qmd,qcwid,qmutsd,qbg,qbd,qapid/exm=qaaw,qabr,qadd,qaid,qalo,qebr,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhlo,qhmn,qhpc,qhpr,qhsf,qhtt/d=1/ed=1/rs=AA2YrTuZTrLZ4SHM1gfcCFFxdZIZ-5oj0Q", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("apps_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("get_2");
    ns_web_url ("get_2",
        "URL=https://aa.google.com/u/0/_/gog/get?rt=j&sourceid=49",
        "METHOD=POST",
        "HEADER=Content-Length:68",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "f.req"
value: "[\"og.botreq\",null,\"\",null,true,0,true]"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=gapi_iframes,googleapis_client/exm=client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy19w,sy1pe,HLvCjc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("get_2", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("people");
    ns_web_url ("people",
        "URL=https://people-pa.clients6.google.com/v2/people?person_id=102932755784749561560&person_id=106251321010044521290&request_mask.include_container=PROFILE&request_mask.include_container=DOMAIN_PROFILE&request_mask.include_field.paths=person.metadata.best_display_name&request_mask.include_field.paths=person.photo&request_mask.include_field.paths=person.email&merged_person_source_options.included_profile_states=DASHER_ADMIN_DISABLED&core_id_params.enable_private_names=true&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591984_6485e1ceb7f237744626208b7e76a0e438829ebb_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy11p,sy13x,sy13z,sy12s,sy11n,sy13y,sy16r,sy11o,sy17a,sy17d,sy16m,sy173,sy174,sy177,sy17e,jVGgL", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.google.com/js/bg/kk3Gx8dy_aXfeZYlNm-Tyx8oMdaEk_LJaYzS1CAHZVE.js", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=bq20Sc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=u0cEBe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sywb,E1sPM", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://clients6.google.com/drive/v2internal/settings?openDrive=true&reason=305&syncType=2&errorRecovery=false&namespace=DRIVE_BE_EX&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=cloudsearch/exm=client,gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_2", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_document_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_presentation_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_2_form_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_audio_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_video_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_2_archive_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_drawing_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_shortcut_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_folder_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_site_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("people", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_20");
    ns_web_url ("log_20",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:591",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],639,[["1619591984871",null,[],null,null,null,null,"[[[null,null,1,1619591984870000,null,null,null,[[1619591984870000],null,1],null,716,null,1,1]],[\"3213a3f1fd236c6a19e322f8\",1619591984870000,null,null,null,108],null,[6],[[null,null,null,null,null,null,null,null,null,null,[null,[[2,1,1,1,2,2,2,2,2,2],null,null,2]]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591984871",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=syvw,i5dxUd,m9oV,syw8,syw9,RAnnUd,syvs,syvt,syvu,uu7UOe,syvv,nKuFpb", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_20", NS_AUTO_STATUS);
    ns_page_think_time(0.017);

    ns_start_transaction("bscframe");
    ns_web_url ("bscframe",
        "URL=https://drive.google.com/_/og/bscframe",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://lh3.google.com/u/0/d/1AuVC7CZwHOC1ZHlv_QMIuLJq-ivaMTA1=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/0B5Wo5b32VY-AYWRRWTVvYzRUeHNhMnRIMldZUGw0Q0RDN2Fj=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1MnBKTYrvgUi9G6O-bendNTsqoR9MvZn9=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/0B5Wo5b32VY-AQ2lkS1hCYThnMlFxR1NpaWJwcUYyM29nSk5n=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1WgPEGRS0etYu9kT1hTZVaF7FFS8821Vi=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1U8GL_EXVup6yjrW2AlKm2OYOC6KF1xHc=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("bscframe", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("settings_2");
    ns_web_url ("settings_2",
        "URL=https://clients6.google.com/drive/v2internal/settings?openDrive=true&reason=305&syncType=2&errorRecovery=false&namespace=DRIVE_BE_EX&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591985_45fa28b0c743b6269adff41ab5980627e4028856_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://lh3.google.com/u/0/d/1--Ft05LHBIoTXD0BPlAInjQXIht5WwaV=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/0B5Wo5b32VY-AU0x1WEVGczc0SzhTcW9xOFBnYXN0THJELVgw=w32-h32-p-k-nu-iv2", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1BNE8mphCfzuGZc0rs22hBFQG23yp9Gal=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://addons-pa.clients6.google.com/$rpc/google.internal.apps.addons.v1.AddOnService/ListInstallations", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-api-key,x-goog-authuser,x-user-agent", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/16/type/application/vnd.google-apps.map", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIpZ4qyVgPcFctVpoP5ULxKG52aYd9OaPpqvndzYDTUUGSCTlVIZdJodXdUCDoKlOd16gf_p7tDZ0g-lOH-MvGPy6PdFzLO41SbMb-ct2g6GYe-_JmzSvgq4q5AfVINbBUPC7lNCQC17ort-mXVkoWf2Et8tHCmbRJN-PtXbmeGo7px6WyNN_7UHS5e2twDNE1cMcBZsx5rAlF0kmAno9hz3atIxHzgolZCX0JP8lScMTm-JvPHJ0sIJeRffePY7mPGXTA3apR0CLkwPjCNS_zU8CPJCUHVzv8eXQPU5bQdSDxnjLOL2rBDsmPgwHgsjp__VnJKhRRm5Qza2BMrrUGUCx132SoYJxzDJB1dPuUhGVLLtTCtyc3gmK10hqDEKsW6lLeqodIIPy7Qjw7XCMiB49tSaKeyB_c4ZCTD3Ojv3J2mNPp13kkCLdqOjzdHm_cz8hY_MzV5A8Z8uFd_Yuv7HKNT3fs2u8qpA9tOnNgF7EqUzoW-oWflQZ0ZwH-GHfssQMEj21UpRx-4VnRiMB0xsKHc4p4zWRRdNATLNSwwIpUYUSIW6O1pnYL-SET_rBmUgWwvgc7-iaCah0MX9POwGeFLHPxa07NChmtx73VyKvxzGWtIipzjhKj1IWqMR912r6Gfe_IoCWIuX6JvH0Ck_8Fvg5-hWqny1AZVJTKOti_4jy6V7s02AGj7X7LE-JXOoI-J5MBM0YEraHDy1cANuPp5P--ykX-SbvWTEjKYm-ZlzbgI=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoGhBMxxtP3UxKJM-you5b3bybHkRug50TyCF9JRm3Bqgd1BzHPlhoYGfqJz76n8ewDxgEOCKtPAd8YNKsX9aMRcexHI5ja9EKBJeaUZjJx5LuajMpYCYLZo71911sUY3O6WtaMiaI5zzenO4Q412UpyOa1yTpMGiTbXgTPzQQ0SsBitJkoheSDPNKdKUgxKcEHnKugnXZJUCzOHPPhtBHlkWy5eEZrr8SJ_N-VpBghCXFdG3NxAEwzCFufzQprMePoGmgCMPIxWNeN-OundC02gJL30AZDpCWv1BleVg7Uu976unMH5a3u1ZNMJApNkAiDuzfj2sCCjdRw-jZH2yGw11LH8mvahox4HXLofAWXFIAvWGX7__el_Rga_kGaGaF0TG5vrI1vbFTSGas2r2K7IL2HdVIk8Y5Xvze6bwUMyq9yGoolmptE0BBQ5QqOThr1rsQfDXa_NcFvZ9OKAQ7gOCWDNesDxc7PMf9gY9Wd-6LDOv0eyPcvY_wrK0E30tEPmpl_YRcQH6JRtvC_74LUkjqArqzO3ndWYWBD7VY5aVn0bNc03qbWYcQvTaev5wPywsFsuN6g6fdGNXYT7iaf8E24njJIPCkW9EGgJZPb28QOS1G6ysIhNk3yuSg8mZ4GI7JEUG6pFNsDciY4-iVfeXhIL3QWwbrUwFwlG55zuIEXRjQsa_IPQc3AqJopjfbBK9JWkJlUYHmtJHqar_wfx1qb0CnKV3g=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIof1OeEc9JwWQKridHxQPuyQ_4HhhmzggJYB6jKeCYxke51FbZQ0gRPkxPFOQpSsIBUh8v3ev7TQ8sJ0RWF4LbNDhifVPOrYT1Y0IGMhq-TV3NwY-Pl7UjWzHdsXR-Xck1kI4wh9up1Jd7Jkexly2QOsbTgXm0DY6drarEnCTE_cvYQUtTwMingELH_Mz3HWE_iSuCYuDyPvYtPEuQcpH6De5BPWAa_4gm43c20Gywk9d_4bVoqHTxJKmvQA-u-4awPA7b7NSU8qAKTNRyNPweZIUhxXzE-HX3WruIH5srI6wUF7z74dMF_2ZekVIWPDVgVHaH6c30HMz--qjgqIG2YVtoZv92cnKaMALgolF6a95ytNO4l76CC_E3j_p4hqYfF3SdjoGzVjA_1GLdaV5FcnaLhukyNx64jWCGXnRk5d5GmHKSxXD9j37XwP4S77HA1UXiv_Akp5zNcfgE7zpCn5UH61N9JnEjXrpmq4Bl4mwpz7p5-pPf43IS-4AUugFIeMEouiUCJcjyVEjkaX6I6PSuXNxtY6jSL14vPphY1p71k5_ltDkMSdXswJD2-Ra8cLpyak1kCqrREY4qrLD5EjNMUSQvt3kaRTuugPN0R_pdHhMvB6wAY7PTf4tWc8J7ynSBGQNy_QSO-hFWF0EkcPC4wNK4BiHAvBJFnBZ83FsJC7wVdDHyYf06u8fHBQ8RpY3yckFuqcxwqCxTslQVUkGcoyCG55hMGQoUHMDmdYNhTlyE=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoEyjVtpL6oZpei80QsVJSNbzgWIK1X_UBdQ2fP_m9cW3JbX1cXUo3GUGY6H23JOFo-mASnRgvIun1rgV7uwPK6PMsC6F1dbiY00A13pHj1pMZW-kpfhpq4lc6B8rH7ed78aFz2RPZuRcjBWVm-ka_1X3h7_Vl9VOFB0u2tlluzj4dZOo4lk2-pc6m2AnyUJG60Js7_SfXiAXrUwUEgSYvfoSeX3e45vZo9CTv_iD6EJKfokYlznsRfejKc0cGMikMOslq-aPmiDxWWXYUcaMyccAu0Zg_LHpcVWxPeh9nUvve0bNVund7zFPTS67Ll9xTk-Gbh2m7SVrha8lCCsSYhNYpbuNkqDIrXO5HrmdXabekjS8sinyKZrkynvs17yo4gFMfMz_9FHRHteX_nCdaWkr6tONC8UkQv5B1e2Bs5wtxlvkTVLcMjtjusKymmZWVRi8GTN3gkypI_axVm64PfKydxiI3FD9LlAzZDPhDZtpcxIeSCgNy2xoKB7YfEQIuXVrTreI583itJwLPu0BYc1NbUkoLF6HPbOw7YMyVLt3llyZUAbRU-lEAH-Qpi6YF3sbECRUTG7N70dED9jILREGsmEq9YnXlw_no4Ci3nt3Jq9Qyau5K5b0m8VM1btiCTqNr9RGsa3FrJH3xGhRD3QBur9XBRDjlUMgi704SRNPmFEX6h2xYN9xVqYvOW5HEOmVQ8-7EeYUNpUFhEIcMKtaelQ0R3qrc=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIrwvwbB2kqdgAH6TppqmOYHWDq1nJFaUvc2_r6V5wsS4rFZVqX_hq8Elc8ipBUlOHbIJqAC6QuLcc2QmpkR-Cc1T9YAmOtaNTIyZm8AvymHd0qyM3otRVrIAVMKpllGToZANAPR1Qz0NzqpKLTRAUR0t25Q7EJZqVhElwsGQ09YfSSPVVQRE_qw-IC9vM4ksVkFT68rMeGoes5x-WeUDjg85VPeMWtRriuBolB6c7f4GdDuIXu9tO-I3ojFUdhtEg-04Xm-kh8r9wTdoKXw5A-KINOSbh_WsHfkab3DtqhCQmdgOpp9kY0Gyd04jnr0utx8-2sxAYMNMERwWL1t5O06IVHO_nmQfqw6UD6lHB9-l_mcvSTon8PzXu1CDLyP7MStr_fR9feYDzvbaODoMnIXUV7M7M4mtgYXHKsRS4fLZpP0RBPOaKXxb2J44q1bsbsv-C-bUAALNfRcLwdYmBJtmx0hWswG7lHMnrZRPX9rjlWdKe_7bGCofxT_O_AWTluYPRBVRn_8ivoSvM_Cl37_rX-aSD062KvtwHd84noBWeWr4PDRfgqJfSgSH-iyYh79bd-04zNAQz2bB5aybsnMDHNmhc5h8c12k_pntNlXuycP0zd3hDdi027HYjkYNR41oEEdZzQOxzQxM4lspl5STm6aK0CdMifvF4HI4e_f9fWG0AIfDD2DA5GpbYffhWdyPIp-fVpeagPYmeaoyAjzVnvr12zjfUo=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIpB_o85YLU5-1zKy82L9-L1Scnyp-KnnrFE3hao6Mye2eyTTVXHlSV9duCi__cBqH54dRooeGrw2jyFU_9vOpOgezQtNo_eb6MdWC-x-Sob7cd-52ABnF_sTJSjiNCeo4r6gvF7oLYIKecxDP7iF3yH2cegrTFW-6WX1E7qUFL5Gz3yaqm8eBFzUDfOWDwIcC2ZuUx75KX95KYSV0-aph_IaKK5s4DXG3nIFv_vI6RRKq7S-lX8qXDfm919LCw7-2a9GzDVC_LbxnwGhIlQPs7oVm7hAtejCNbUaMsap6_kSYZ92qzV6ix-BlY6Tu9L-gVN3qQ872DLNMv4uGYEx2sNlzlPr-wmkrMnkB0GL3jw2KwCJd2280xCgOEwCXHbLJ89M7AoKpKw8cDTJFvnZ3OQj3fI7xu7h9oMheQNeFO4eMaSbyR1ZWewjrdNeViCy9L11X_lEA52BDR_gaxDgxBMTOfUlC4bh9GQLOERML4aX7YCXNy2dIzbSObi4G1vub42BkVaOuyNhJh09Ne1XyJff5ZSbjrkmHlmmY_6ZEbvKdYiHCdFjb8xjz9OKFbdzgm5tx1BM7vzONmtrZNXvilJHNLiXkbRmqQ8pAh7Qnq0kLy4yAFw_V5W3sgXBBsNK3cACydaEZtkEb1_tsMXyfUNAIFAN-9S_-0rQlIgybp5MTcUrxcynCi7TY_P0R_cUAGZOOf3b_9yJX_9xXyW7351B2uqTHJTLUU=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqdrst1zVsVlzKiklsltkuk1aAFp_CcOzgGwcHkOovfoiixkmgiY9hI7tculw7E95HM-mlvvWHNiEqF_qg7Q9r6KcyPOiBnShBSnIA1ouA0uwY2kxTHtYQbOuSp_8koKBjj0xoQ1nH_NfiUtNycuIscFE9CQ7oiQWdBvgyTnHg5d2_isFkAiKfwXySVStGBzOO91UGfGEgENiQsVPP7HkiMu3VgQgA4uvnoairFjbmnBj3UocdnSB5VRahcrv4r35AW70SGukximMxCgrPXe-34S87HM115R0S_W-BLimzzT7xK7KJ_V3eOJS6rL1D7fwq2gNgmVU3WwHrbJGnI4WtKTGCkGBCMTxuoj63s9KZm50VrEVTSiBjkwhHpJkhjbQacNLfVJ0LA2OoifF63iHpws9g3r4KUejWup50ufATV0gG1uVRBqePXE0pYsJKwt1GFcBXAVV_PF9-3sg8Ftk-HtxDuqac_OqUBSexCyHZgyYKHjffoR_pF0WiouuviZ08Seg_zwibaQ3jO70kZHI06d40MmpXLJoXTBoqyVFshnuc382WtWAdi1M2EaZKokEvnW6RPhuBMgx7IXx-AAZB9AaXSujfU40vAJbKFoj-3PYAvPazxBrDJH8TgHw1cxy1ph-XXILlsLwCuKICyRaxOwUp9uNVuo4XwvvwQ_AnBAkBVBWOMrZN-bnzOqSAvNiPe707Wa1elOdN_f3T8FuL34aoIdGWY9WM=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqokx7zicuR6yTinZ28_HAHW0BIbkyVxhXiq3D2n6Ed3-VV4_cq0oYBziAEG3Qpr-gEz8ThmPD0Kbt1rX8ANHrQLf89bIz_9YMPyTxjjK9bDrBvWCJH-_IOmjHVB7db3j_LCmf3idcRJWDFa12txhjIQNnIQXREx-VKtr4ErKchgCNs2VuziTq6agiaxVQmfOJkSaVD5B5hXbTx29U5qGTRpfARyhD9Bsa5SkfFQL3f2IaHFOqs76QRb9-7Sc-Xg_KDm2cuj9S3eI1sK1jY47Oq29BGt8KRR9gIIZlQLiczvTFrwHfM2D8LoIORY4dvABAZYUoUvYvFvTWDlEpNM8Jv1EaKXlK89Uja8DfVQnzckan5FmG7DNHvTIva3v-3aXs3aTn6EhropmHaDOYSJlNjlf2kuO9DhqVyamwPkG_a4E1az3CMEpRbDu-ODgJVHapCEzxik6_z11TjSkkiSBzTKPaU5MGzoZO-YupgA0t1-_aI0FW19ex6qo6vihRZ6B0Jy5MFES7rwJDQcDLVipkTXVQMCpoD9NIEBDZeOJ5DU9qcMlJxCKCUMEwC1iM0KLewpi3yp7tKT_UeT_FtvpmZbQgRS-5VFnEjVxs4evPBgVvchTw45cFgH_pigmSSRNTVc_hTHlml0UtHVWJ-tPtqnmcrg2U8DPZkCsLFMgYs6neVrIr1sTg9cpL5A3Yc4DqU-fjtAg4dOPlCEYrrr4I_v4BJNdB5AHc=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqAIGy46kD7whjQUX3gUOZLFFPFfMwdUEFubLm4Eiq9dNzuYogNASt-g7NveTRNQ0pTNYywjOvwTI2xY-yuc3thWdohdNX5ZbquBxSwhzByGq7u6evQYyAz8R38G8su286KCFutmGpnYaflEox33l_h1w84lbFTvhuQ8VU2S6ilc9MfpDhLdod22W_YBRGllgrNuDQPp_4XHXmQj2h6kdc-Gs362NUk08NT6AJ0BMunP315owmdWghDyfXI8ZqHoCfDlBUqOXYSqGDp8ItWJFOXXQ4RKLX59eZVoHm7yAyC49w-r0PvyUyrrfSELM3Hzpj6b43j1KrRUfBIAVVT5fBsnggy3zBLRNMs8WvHKMmKi8cfuh1azJUkmxpO7L8IcUdABR4ERTzJIgkJrJdgasQs4pGQpphzPJbGhVp8juSPUQRtsg8XX-mC9LhIQq5_r2lMGZvtl3pOOdeQ0uXW-kV4Ut5j9nQMhiLT3II7fgypZl1oUPWx4fBbVsO8F-aclwTj5VQJap5SGqVnSsIXF67JOGesbq59B4fg_UTJkvZ9dcj5A-gXojRMzv_xb6uIjZ-XGLzGD5VZCNVk18UExls9aUPxu-foMVV3xJ54o8c1L0iJ_TS6buEmXWQNemi5_73H_Ii6OoVytzPbeRoAHR6Od2qFsu8bZ8oGeO-498NenhbOrxGVpqlIIHW8-J8KQVFpRk-KZXHXVkpgNVndMRl85EIVN9IaiGAxGRzOgLFiM7Nx_HU=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=xzlOke", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/11LhZKLlmfG2KstMMiTX9U-rbUKes61PhhV_GtWQEsNg=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UIkeEoSX1nJ0VltEvH3IqUUyWyKFopPb=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UUzDKMNSdw7Su1wmvO9lyeIL3GM_BOv2=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1IwcnVXXHZm8mg_gW11QlStdeadHO1Y6U=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/19CWe9D8qne71Au_HcywawI0GrnBuQIef=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1flGeNrmwtbuznbRQFnRcD0a8QtI=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UIfLhQWpC0A7hCphZY7BpskL6eGNfVrC=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UFO0mQAcQeFAphkNXNQHHdIZ3NJyo55F=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1NWs9WzbsdnG7Lq66aGe9dX8Lq-Q72WKb=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1ULxquWPx7Ll1D7BduCG9WopHlTnDVZiP=w300-k-nu", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("settings_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_21");
    ns_web_url ("log_21",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:11586",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_21_url_0_1_1619592544646.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIpO7oWgdCbGLftafIpjKuziN1XWOKj3mdEmjSrKiRzFNqSxaNQQxa0tJXIpJJtcszfqA9Mc2YHadQxA7lXlaU2nCrQ9uxpyEPFLPAabGxqsL29owPukSUF1sFnq_IOuhV0uP4G4sNIece7CXngeiDBlILgHhw3-0EJV-eBJFJzYId6YglJvyh3P3X8_NBUFF2qCMsNqSGdG0f-4h9TAi9ckLXwFdyQko1dVIDkvFx17LhUpBqgxl6JUAZzr3y-vFGDKJULmbtdo_ORkeaxR-283GIvDKai6TT_s5ANngyTcQvO2sA7p2c5EBwvfon_CSjaJh1b5wEVGsugwNVp-RDQEy6IdhF8JEXJFpDDuFNEXdm6T52bOuMgaD_MdzDTbQGkRtzrx5ocsR0qVrLTVWvFq67lgBgZNo3Q4P7qC6aqznr-xsgkxCJRkFYKXSOS74-fI2MfG5sm4BsVOrSjNd3sFExHaVcnPbyjvDKHzI0YbOpgUtNiXmYrEDFsXf8FObLMaVqFXpqMSTi0k8oibiQ4gjoKLSSa85Wm4LTtme5_tCLXSgaZJdBqnyEIs4wHTxMyDqcoBZ4ylSTL90cBSfb2TnxngRBw-m2HCdDky6K_hMDBkVyXl5Ntj4-b1aMYrluuEQx1xDIRTrikp4RrlGyhq7JihzHAtu3cf9vRdlZIAVE229vC-Wc8TN7a34S7TKaZMcqGLQSsrep74Cw1_5ssNgG40svOq=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIp6VJWK61gJN5mMl3AU6aIcIVPxE6EWXhtvCcBOS6aqD0MIXE16iO0bvcZeUgbUkIPliIDqwxQm_hTEmQHR11yCoIHZd-i0W-_mno8qI8DkqGqzu1oD1As9xiyh0NDsAt2Lx4C7cTP0YxRfT0GdCG-skjHCso6Dz8s00pEsqP-5jRJyRz7zWPmhQrMp5Uo2gnHh2UwcruAjseWwXH4CzG0KCYYBo7HBsenjVZg1cxdtkj-J3iGrA4FCIatH48XwVjf3e15gBoLmkqQLkS9pHmLS6mSLc5RHLvepG4_l34zEWxSQfxdJqogrpFu-tdttZ7or9TvueP4CkPKNN9Tgzf44PxnTo-V7CSGn_-pKoJFVwxKFt5qTwlGi6ZQaDoY1HVbU22Zwso5ra4ZIBtcnn7c_RIL5Bqvu-hzxL5-4FT7PpdBQkn2f5vD3fmtatZQkiiEE4TGLG-ulVH_K1Y0SdXJ67hq-LdKB3N-DukFpNJlBeZsJ_tGR157rpqZ5LJ91lDqTzDoK2Ao2xXN08NRI3lfFgOziu3wSR--oP31tnPDsBJwqZxZsi2FAPIocxxb7lyaNlJlWbfzOg2nP59i7Qkw7U3rRPLyoCeBjfAFsZ3NHHqLxc7cSHh2HFuZhW4cBW0iAGstibDGPNkixHU63GQx7pWNjoTh87P2hLGRLcrD3PrCCXZ1BpRAaAvTjSerFZkmwYlQx8zd49gcmozIEKuOXYszRZCSX=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqILAtUtF21JEYfnPFwe_z1NPoIBBwcNY3TB0Cf0Kh56dfwtORng2wUvnPoXiJH7A2kRKAMWI8yAlc8XWulatyi7GHxq3-g7ZKWmgtCp3UI4VsiM-a_p3e_HZxpePNEwJYlq5vnqE1V2pPhSTgij5vAQmMFQetf_ZA0yNsPbvFZkBmM4Ug8zEig1fXNdiEJLS2EppRgA5FxMwtQtX7bvK2X7b7YtqUlKCgqXHIxhk9Q8L66NZNJof_tNkfh23aIS7KRu17UV6XtM3INcMjlzOAb8udBC2tc07VrNK04ZytrjNoyVNZpUxMJd89SVNqWMVTuW6bXSKUNwXDWYAovDMootX3_6QjvZ1q2QUevI14VDcxdW49goOZWs0zhenwfwfo6I060D7UpoxhEj1eINcFOHL547lp0wDHiDoCSjuPzDqTE9EplZRdIbZIKhRLOJi3I-TIQoiUsqdt2-RZI9pNXq3jK2rOleYfsNon5RUwc44kpT7VeMMxIHleYI4MzZLf7lOovzFMX4PRalMsJGQol6OkJzSNcvKdvJSmWVeN9Cs_r-5-rJ_2OdZ7WAV92toCh-6MQB4HwuPMnYp2-e2xYvOH-BXKWYJarXEtrTboIj3bSlk21pDCO8KcJb7H1pR0rHojU_t0ryou-Z67Yq5rVdG1OFd9Ro8OikEo9gCXJvsjomG5d_nIeQDwYusqPxFrnB9-_bMhBpYViw1uB90QZAiLwuyEQjUJrmOf7zfs=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqfchv9IKuue1L6phLV7Xgd83lcA7SOAeUtZwsAN-3ivj_vlOD7aO5G4b6CM_DpbU_okfPzANVVMyACdrVmVaymkbBD8Sj5O3fe6qpUWJeYlPvFJlXK2fcKiTbAmHbj7CspJaB3EN8mMnXmwAWnx_eageZMgornm4m2Gt1XteVqEYkB_TAbQtHjpg6o27Z31uscnuM_QdLmPsFVqZK4eHOZxrzgaAn5uz53lgGxcJ3IIBZ8-HrpEkU_1BYe33fkT3DNlimrwhHchQIneIt7hMp-uzv0q-OuLzxfImOKnD5YxHc4qddsgbDamiWocVnJj4xn6KDYdMND5e0wIORutY-Td8OIs165EHtL2dlnduMYA6d7AnSP682VYdCWgu_4nmDhNUwxISQQqcF_9nGxOkALoOOymgfUgWL6XShuubIRQ4h5gWWAhO6UuSo9S82bNw_PO-g3ersvKxifiDJWpr6mI3BoDZRQA3ioX8nyLjXoIVYNbBjyzpq7TZw7f-Lz8wsM69_klUYcCrdO82441-nZswUh03OvqUppwj76e4Eo8UZcvHzZHurCLgFhNg6OMEOs161wxSQIBgN1ZaLkzG6cjZg7r7m_Q_UI6Q6ONfDlPxdRzPzR5j1rnA3BktTgG963YmaZx2sB0xGIcZycD02Nm4c28oiT6czqb-CikRiwLj2HojmCR_HjetlvKFRBL95bnR-11cSJWE_pzYZP8rZUhi_ezhfY=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIr7FiHihf_MDMQJXoJYYymolur6QtU8MLMdXuAQjwLDCCtkDa8EDZcUrVZ4Gx7RYRGuCzklDhSYjIvPGUlkf9tZk6d4EW3HC1kKB59ciQxycCd0WUnO0b6sufZK7k24kEDTOLAm6OvP6z3N2TFkxo7MeCCy2TAIaG3vmavg9kg3WSLHNjptQUPe5sYvDIj_eIcMcAVTihEMyBHxq7URyUBJEPKjBGW9VSJR44pcZe16qI_f9QE65mJBpL8tPQRftSHwxpYViX2N84nrceURNT6lSNRPIANY7QpUm4El3RxNsyMxXxO2-UBI9hcfkR04_9jJCeh0XGzWvGjFYO2mQR7vc6Z4JjTYIzQsq5nxx4kBoHfQIsw5kc23dARFEFeW5PfEVtZ72NrvmYji2i8Cad8FwANOFfu5FBQbxfxs1ymwEh39PqcEMTQqHJzPuS69T2vey8ybmWxNDAtKMxe-FY5013eQOQSqydwpN7g9PXDV4m9uV9mhdrxl-LPp7tZPJYEDXR2yN3k72lCfjTsYnZf4DTYsdGOphZllfb0hA081g3knXiUSksZRDTjyJP-kPWtCmhZ9eOEF3d5c2mHXlei5JmSDeHFpExnKt1hj32gJW64GQ9wHvs73MQAyKrkFpLYhrftFyXPymMVHgZql0kwMzURXak6b4Q52WprvEyuuXE7JV4e5j2IUC4Zl6MVZO4znQsift0Vz7_SnejqVyYBI41ehYSSQ=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoW1694C8P8Aq3RFVG3mY2m87i0EVprsBsKuGLb5F8IbsTA4ZcKthsxSgHCY6yhoMsdc0zNeA5AG3G-8onaJ6EiPx-O3n1sCv7ybiteowYilG6Jt6CSIKqyYTU6pOg_O_a_A2mohhUM4JESurjsoCKUr9vjy7-ke5E1gCVeBTqg1dwDlIx9RyA9_qtGsA6eTuxMji3esavF0rispHSIP1q5UXPHHkKn4cCcp5TfMBwCJ54O25v3tsXSX33vemuwBzFpOqNaCTTjZ8wECPYt_2Fb6xsH6G8Py_t7Cvq4OSJVLa5R45Qvu3NQFhUf_c4991qAPNsT4B_bjEksTN0And5HteS8dudydrZukfqRlCf-VAM9Wmxzt3vpTeSaYzGXz86VKKC_6Pf9GYw6Q7HkCOFNJxVmryuGaz5ZEuEGtAxb7zdHolSZHAaVxxDkduLJAe9uL6eSGOu_o2VG-R-pR7r6DiiuMjpJpbvEDq1g1oNi-HvBy-oIqIrSbnrAsIng2MnI6a7TYwWujdfdrQ6ufZXLCX2VZTkoSmmRc27hNqGZQF2xYl8un1ZP0Gs4ieDwWPEcvvqy49_OJb-gTS9MDvqIp2MaBC47MeYO_YKLPOKX6ejCykqBN3E30-IRqh2TIKNO7K1gJkrpO-XF1TmSmP4B_is6xYjEFVEw_vdcVIwwmjRIcShtzTRUNrkWsKuXO0UE_-5Rq9akjZ_rNIKiXG5XvLM=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIrtyR_h9VMjIQgtYoYYfQLcgA8az41-Mvlg-ipRE5i1tcazSWk08DT1CeH4w3oO9OJccXyojbSsrqnCyz9r1gcFJ6ahXJ2ul12Eqy9_gzikznWWvHzUpU-hxOkJ_9pK8pdYAKWpSuIIxQr95LiT3IyAhC1pg032QjeuB7NaBjV6djhGfox40mpzvC0Su-YjO7nrWCEtkKHkzJIteLPCVCqWbiD_izcp85o4ATAueNFVdMSJef-60tyqXzGfMAj1Cs8lLGQ4CdI9fQYnmWh8WcQ9VsiRUNeFZYlYErJt7e0vuR0eS7pkTTbvQ7HdrT6Tc0ujF2C2WkxHZrh0Y_1F_oQwLcQX1uckdZhdmlm391sPxS66vg2y3LcIp_brFAqtERzGRqpI8pVGn3oCNP8efxdDeGfyX9Y5yiGyoEOxLdZD7ai4LrodTkcvo_fusrIk7v2JsEerZCGOrFcbvOzkNmV2cB6j_zaEfMnG3xXPCZd4UCLwq9CnQ0WWd7i1Q-_QevYwpqSwogqcqT3JkL-72macIXvxO8b0ItyUYNrMnH_AIKBAlyD-iMjkX26PQ4piBuLcoss1_5d0ae48AQGexRvyg400M6rH9s8FgY1sqEwXdt52YeSeXu8KtRa10eR7FsFDHup5LgHeI3w_81m5txRdVDRJFdrk9yrd8SvG91MbgD7qsOQmOxv-AN4jdzSh4gQRQhKdFKvXiquMG4JZvI4wn_Uvq8Kul8g=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoAGP1DdPLKA35zuKjUuk6pTzbYRQbJ08ELLOZmX5LYZE00v_MqUAYCGl8_KuBTWy7gSnc_Din2honFaX2TdZNSqoI0kvuGypx-LWOoUoi375ok0hedJgLcINUcCdStumd-oRHSNhsZRlMB4F8Zrq_W6fieqImdjNab4qlTn4ruH4Kdv-kQ5JNHxHS41cYepgTYOJQWrF_Ftd_qxeKySoHd2mpNP0kNZGAy8hzJmlgFMDnZb4Y3IOjofVHjCXQURttFCz1lBW13UD1Fnsw9lDWplvkGAI2CWKSTb3opmBnNCZDttmCGVSNuc6IdnV6guhjgKOvlB2NvzimU-4x3qpyynCtyhitoaLeMKvElLFzJ50iUVbDhOuNhK9stBUtq_uOfQj5vbBfR9I8nOK6cZbghEdY0_a3bbvFWyzjak9X6nj8GjXVuezRZ-MghfNM8NQsPVApgsDETKnPLUd__1U4JVrv_4adQhIR2ea7dDYyCjH4lYWLD6XqlaMc-sxIXHHFeCVsLmX4jXnEp4394QN5IXuosQadSYADXxRMOCV3Q1g0crgi68GlUZPphQB311uW_KodKze4XtU7omrTvawNTDO8nMHV0wYS1xxv2te6Rp6fecki5LgMvRtY9a8r5xD8Hucdp_cMsBVblTsOUaXLxMtGs8uU6q8gC6BnEamjivSYai0kZwjhyjXG65Buz_rJNMcc5G1kPzJmCUFc51f1WhlFRjBwo=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqxhk1mRlXgr6bueUizHCHWDKC6XMs0sWlSd__nrWkKy1SPzSVEysUG9sQzG-QZ9bu4KSb4f-X2nhuPr4CUwRFuLvCtzh77wZmsZeeJENXYW7bTQFQaMAJL1DqsItrXh_4wD4LxH4CtXvtq6KqmNTSx0CkO202q1Gf87n8nSTlUFv1KWiyBrxy7bGp6jvWJVOb_1oTyG0beK6IpwiKppqJPrRw-XgAVl4DIHmM3gZqpgJKcXIIU1IYF7GUfi6_mZ_WvBzrsH4QBjlC9nBMh4QlbvvbrL6tyQdkmRrA6Us9z0cGaVEnjEjbbPEi4Bz2hmyAUqK0g6CyDkEk1tQdgKp8SHj_Ty7knjROmBqwH_RjhUZ0Z5GlH8zRoeR7BIBEdaEcl3Unrp_Ub4bHSZ0gMTVsK4rz1jquuf7fIAP-ms1wsson67Nhn4R_xF7-5h1yaNN1ddjstpKmXwx7CAn3PE4hAXfY54F4b6iwQIYC6yl4xDrR_CAuGrcU44J02ZEPb6_zwqP-O3N_yqpXh0ceb1NN_aC8LMpeKGxXZJw2YXCgqYmYi-MYTyvtm7coMmZ71WJo1bkxiEoO4JNZ5tsb_r3EC0rMF7z7nn9yD2sE7tEBZPyb6ZgcPKLS0aHcFUURETGxV4mpX_m_xHg1jqNAq8lZFUJkPczwBAO0Lb0u_6YAq-aa73wO3t8ciA0NkwPhPt1ghOXFxFppJwNurUIicKsCNwAynP6u3=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqFLdt6UOZjaHC_XN0U3wD0cCxzkgrNkb6lwZiVO3erUDk3_ZRxw3s76wqUM6rcrhystCiK-8uxKjl6KJKZvDnA0sMCkmN05BHM2PtnrnPBEEklQ2x8fWTET0wkLR_Pn9tvJ9Nm5xGEgP0E_dtbLR5_wnI5iUltKJ1NVwSsTOIWg8eL3t2-QdbS3kh3HBuCbNnInmaf_bWWALzqdnT3Bvkp378SBAhCHbAUs3oOeTEKeQW2_J9uvIlGPOUfGkPd2Kz_kq08EPmyTYbylxpSElumEkzs0mPxEFwmMbogY4BVf3mHfBHiMhiOmVgb2OlacCMS2EMc4_WxA3SrXUmSF6ZSQ9qj8Ge5bxMu3ktKk_zlBeC1G2GEyOjseG5oIRY5wKcu6AXvA9vus0gFyS2NnUeHa6cyZXD3FDoUOro9rDyyEv_hTtQo0p39qsHA0M6-TqX6IDXQLx4D6mkOpB0GmNNs2b8RInlmJB8oQkwOJZqnlqd1pycRdDEltzEoFB0Pgq2aWy5L3B-mXOdDTnonE4J7TSb3jR_T3peQX1YtXgfEvAx7GYQ5mrrVzAHoevaGF2dcJArNBDyPqbpjCCsy5cxWg3rjFS3lDDUZA-mCJCoR7Dlbg2GSzZv5tiNASqDrOPgY5UOz1x7ri59lbpX5UDChryScTNTMotoL3d6FlrAXv0-ul_C6uLW3ei8B-A4IPV-seKJkvnxQKogPVhDhxl0l2haKmMN7=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_21", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_2");
    ns_web_url ("v2internal_2",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3Dz2hon8qk2sz6%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:21220",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/v2internal_2_url_0_1_1619592544655.body",
        BODY_END
    );

    ns_end_transaction("v2internal_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("ListInstallations_2");
    ns_web_url ("ListInstallations_2",
        "URL=https://addons-pa.clients6.google.com/$rpc/google.internal.apps.addons.v1.AddOnService/ListInstallations",
        "METHOD=POST",
        "HEADER=Content-Length:78",
        "HEADER=X-User-Agent:grpc-web-javascript/0.1",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=Accept-Language:en",
        "HEADER=Authorization:SAPISIDHASH 1619591985_94ba35c01918379579e45287bd166b2e6ae46a65",
        "HEADER=Content-Type:application/json+protobuf",
        "HEADER=X-Goog-Api-Key:AIzaSyD_InbmSFufIEps5UAt2NmB_3LvBH3Sz_8",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("ListInstallations_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_22");
    ns_web_url ("log_22",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1682",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],407,[["1619591986572",null,[],null,null,null,null,"[49,1619591986571000,\"!nJ-ln9LNAAZVgnmyVYJCv8t4ODJSF087ACkAIwj8RiasPrdAofW9K0pKz3VnaZ6bhDws0Mev3EJGjvsPp4bVgmtoywIAAAIjUgAAABRoAQeZA6afbVfruaelxae9FVRwWikwb_o4FovQeMPhuu2YXQQPHn-T6Q2Z68Ts01IW63Pyj8zz_PzfrKFIBt5Ch8Tm_V9EhP0TBqAeM1pbsLxXn3r-pFGu-jumSztatRJgpaooNnJqVkF5eq8Invr_NrRSEDsnufLdqbJg7wHNTeqKkWG-0_LluoHfJPnKd8ghZfjjgkMLortLMAJoNM2Etwb0PnsUKXcD8e96-yrcmkPqXTUOwugLV_0NjgosNp6rKU8xGTpBbKR-g7nlO67S8HInz0lbxeAoLUEGupl6gd90q4aC3e3tiM8VGvZegjfqdLwZDe_Mgrw9PJ6i4wQ1g6nfGOe_wyrO-WKyyo4-jzJ-WDoUfOLY5yV17vAd1EnmG5TjAxgTwbOcLaLK4DMyQDREAOW8eTP1xdYR7HuK8FpNfWcUZE9ntZrdQpL7UsGRA7GamGPFY_3d9_KzYiemo5ns4qCy1ejqTaX6xnGuVhLlygU6iGLZlJb7QzLM12wlL1-cKMFUQoIhL-X35kJqty6vCy5QuojGMRDV1qujk1anUFa4d_rbu22TwcsuC6nhob8tpwbrawgCsJ6axTKxBKV38Lgnq1m7iW2rJWM6mK0vhOSq6UT5YTFZq_Z1sLPncCLrCpEcc1N4yCdmZV-f4Fb5FmwpuvYQJbDGJznp-Lh0yvKqCzYLoSj65RTnQHT3Mh__nEqO9ndSgWGlH-X-jq3JX3CXbTsA5rG2y-9oEU-wc6hJt2Y9Dcsx3l7-BZmV8DxA4s7uRBdE5_NVk4-vOSfgw9sQhxdBFFBjtsqM_jDW56G14svxn9ltDRxZI1lmnzKTnpPqMimowUANOlItskXX8n1Q4p0xf5_KuZbb7rKYmHQ7TWj0wboMDcyAYGqpE5fAJ1p1SC7gCr285g1TWynkipfKjeN7Iwp-IIWiJxuQb7HntuEkM8nEbxAmnd-XI086EmYiaRvS79Gb4rDUbfIkK_c_5vrPH-bC0JOZyq8Zb810VNVo1Ei7JcFJY6I9p3VuFBMTcghvQF2Xp8gtlmH9ZS8vkL00d4ExWM4zG3-RBHSfWPWtBYEsE-r6_9tlWpfy6BptF-ybcpj9nwJBIVjsymSViG60bM79XWLYWNyqHomBbiJhgRyCx4CBr_2gLz6muACgVDSHTwk_q0hfhvbhMScj5eoKZgRDmqLz0wtA81vhP6aEZ058k5fJOxOt0xmyEB8JTRhbdpV0sPVGjiGHi6njhI-VsBge\",1601285751,true]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591986572",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=Y6bHqe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.google.com/images/cleardot.gif", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_22", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("manifest_json");
    ns_web_url ("manifest_json",
        "URL=https://drive.google.com/_/AppsNotifyUi/manifest.json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=pOYMFb", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=Q3Ucxe", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1fu,sy1gt,sy1h2,syyy,sy14k,sy1a1,sy1fe,sy1gv,sy1gx,sy1gy,sy1h4,sy1h5,sy1hf,sy1fa,sy1f5,sy1fh,sy1hi,sy1h6,sy1hg,sy1he,sy1gu,sy1h1,sywg,sy1fp,sy1fr,sy1g0,sy1g2,sy1gw,sy1hn,sy1h3,sy1fi,sy1h9,sy1hb,sy1h0,syvq,sy17c,sy18z,sy1ah,sy1ao,sy1fq,sy1g1,sy1gs,sy1gz,sy1h7,sy1h8,sy1ha,sy1hc,sy1hd,sy1hh,sy1hj,sy1hk,sy1hl,sy1hm,sy1ho,sy1hp,sy1hq,sy1hr,sy1hs,prev", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=tfTN8c,syn,MTuW0d,lsjVmc,sy1iv,HT14Ae", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=change", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=syy3,sy1g5,sy1gq,sy1gr,ol", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=urip", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/feedback/session_load.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.google.com/tools/feedback/chat_load.js", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1ik,sy1im,sy1ij,sy1il,udp", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=syxr,sy1in,P6NtRc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1gb,fup", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=rJpW1b", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=EV1Jzb", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://www.gstatic.com/feedback/js/131swjoe9sl50/chat_load.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/feedback/session_load.js", "HEADER=If-Modified-Since:Thu, 07 Nov 2013 18:35:35 GMT", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("manifest_json", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("operatorParams");
    ns_web_url ("operatorParams",
        "URL=https://ssl.gstatic.com/support/realtime/operatorParams",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/viewer/v3/v-sprite35.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("operatorParams", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_23");
    ns_web_url ("log_23",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:460",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],78,[["1619591987960",null,[],null,null,null,null,"[null,null,null,null,null,null,null,null,null,null,null,[[null,1,null,null,null,null,49,1723230717]],null,null,[1366,768],null,null,null,null,null,null,null,null,[],null,null,[]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591987960",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=auth/exm=client,cloudsearch,gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_3", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_23", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_3");
    ns_web_url ("v2internal_3",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3Dq2qb98b2dw9a%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:3238",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====q2qb98b2dw9a=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/changes?openDrive=false&reason=303&syncType=0&errorRecovery=false&includeDeleted=true&includeSubscribed=true&maxResults=1000&startChangeId=62515&fields=kind%2ClargestChangeId%2CnextPageToken%2Citems(deleted%2CfileId%2Cid%2Ckind%2Cfile(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CteamDriveId%2CteamDrive(kind%2Cid%2Cname%2CcolorRgb%2CbackgroundImageLink%2Ccreator(kind%2CpermissionId%2Cid%2CemailAddressFromAccount)%2CpermissionsSummary(entryCount%2CuserEntryCount%2CgroupEntryCount%2CmemberCount%2CselectPermissions(kind%2Cid%2CuserId%2CemailAddress%2Cdomain%2Crole%2Ctype%2CadditionalRoles%2Cview%2CaudienceId%2CcustomerId))%2Ctrusted%2CprimaryDomainName%2CorganizationDisplayName%2Crestrictions%2CbackgroundImageGridViewLink%2CbackgroundImageListViewLink%2Chidden%2Ccapabilities(canAddChildren%2CcanAddFolderFromAnotherDrive%2CcanChangeTeamDriveBackground%2CcanComment%2CcanCopy%2CcanDeleteTeamDrive%2CcanDownload%2CcanEdit%2CcanManageMembers%2CcanManageVisitors%2CcanRemoveChildren%2CcanRename%2CcanRenameTeamDrive%2CcanShareOutsideDomain%2CcanDeleteChildren%2CcanTrashChildren)))&appDataFilter=NO_APP_DATA&filters=DRIVE_DATASERVICE&spaces=drive%2Cphotos&supportsTeamDrives=true&includeCorpusRemovals=true&includeItemsFromAllDrives=true&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619591988_ae16df8cbeea1b2b1cfdf6b808efb508c7f150c7_u
x-goog-authuser: 0

--=====q2qb98b2dw9a=====--",
        BODY_END,
        INLINE_URLS,
            "URL=https://docs.google.com/offline/iframeapi?ouid=ua3edb31a06cd07aa&sa=9#cd=2", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("v2internal_3", NS_AUTO_STATUS);
    ns_page_think_time(0.203);

    ns_start_transaction("iframeapi");
    ns_web_url ("iframeapi",
        "URL=https://cello.client-channel.google.com/client-channel/client?cfg=%7B%222%22%3A%22cello%22%2C%228%22%3Afalse%2C%2213%22%3Atrue%7D&ctype=cello&sw=true&xpc=%7B%22cn%22%3A%22319AqZapob%22%2C%22ppu%22%3A%22https%3A%2F%2Fdrive.google.com%2Frobots.txt%22%2C%22lpu%22%3A%22https%3A%2F%2Fcello.client-channel.google.com%2Frobots.txt%22%7D",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("iframeapi", NS_AUTO_STATUS);
    ns_page_think_time(1.47);

    //Page Auto splitted for 
    ns_start_transaction("m_core");
    ns_web_url ("m_core",
        "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.dataservice_worker.en.Us1ISN2HQTg.O/d=1/ct=zgms/rs=AFB8gszafTxv_6nkNJ1Oo9o6-p2UXJgsEA/m=core",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:same-origin",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("m_core", NS_AUTO_STATUS);
    ns_page_think_time(575.281);

    ns_start_transaction("client");
    ns_web_url ("client",
        "URL=https://ogs.google.com/u/0/widget/app?bc=1&origin=https%3A%2F%2Fdrive.google.com&cn=app&pid=49&spid=49&hl=en"
    );

    ns_end_transaction("client", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_24");
    ns_web_url ("log_24",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:19202",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_24_url_0_1_1619592544699.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://docs.google.com/static/offline/client/js/3493816217-docs_offline_iframe_api_bin.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://cello.client-channel.google.com/client-channel/client?cfg=%7B%222%22%3A%22cello%22%2C%228%22%3Afalse%2C%2213%22%3Atrue%7D&ctype=cello&sw=true&xpc=%7B%22cn%22%3A%22319AqZapob%22%2C%22ppu%22%3A%22https%3A%2F%2Fdrive.google.com%2Frobots.txt%22%2C%22lpu%22%3A%22https%3A%2F%2Fcello.client-channel.google.com%2Frobots.txt%22%7D", END_INLINE
    );

    ns_end_transaction("log_24", NS_AUTO_STATUS);
    ns_page_think_time(0.127);

    ns_start_transaction("app");
    ns_web_url ("app",
        "URL=https://ssl.gstatic.com/images/branding/product/2x/drive_2020q4_96dp.png",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/js/947777316-lcs_client_bin.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ogs.google.com/u/0/widget/app?bc=1&origin=https%3A%2F%2Fdrive.google.com&cn=app&pid=49&spid=49&hl=en", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;OTZ;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("app", NS_AUTO_STATUS);
    ns_page_think_time(577.189);

    ns_start_transaction("app_2");
    ns_web_url ("app_2",
        "URL=https://ogs.google.com/u/0/widget/app?bc=1&origin=https%3A%2F%2Fdrive.google.com&cn=app&pid=49&spid=49&hl=en",
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/client-auth", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("app_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("el2");
    ns_web_url ("el2",
        "URL=https://mail.google.com/sync/u/0/el2?hl=en&c=9",
        "METHOD=POST",
        "HEADER=Content-Length:232",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619591990653\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":[{"1":3,"3":[{"1":6044,"2":1,"3":[1043]}]},{"1":8,"8":{"1":1619591930563,"2":1619591990639,"3":[{"2":6035,"4":1,"5":1,"7":[6040]},{"2":6068,"4":1,"5":1,"7":[5449,5450]},{"2":6033,"4":1,"5":1},{"2":6068,"4":1,"5":1}]}}]}]}",
        BODY_END
    );

    ns_end_transaction("el2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_25");
    ns_web_url ("log_25",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1920",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],241,[["1619591990664",null,[],null,null,null,null,"[1619591990660,[[\"2397168675742140944\",null,[[472,1]]],[\"16147638372540442232\",null,[[498,1]]],[\"14719340685975485085\",null,[[623,1]]],[\"17077408715954654437\",null,[[917,1]]],[\"3318688667027929436\",null,[[497,1]]],[\"5790177495296899286\",null,[[0,1]]],[\"16829267986558572790\",null,[[235,1]]],[\"16339156775003354937\",null,[[264,1]]],[\"749851692583976763\",null,[[3,1]]],[\"15419336178855610526\",null,[[408,1]]],[\"17276521865292187132\",null,[[0,1]]],[\"8257051839445688306\",null,[[916,1]]],[\"7792735449360349632\",null,[[916,1]]],[\"7094487270460551484\",null,[[8,1]]],[\"12563104964214410683\",null,[[202,1]]],[\"15605813632677093659\",null,[[3,1]]],[\"17914751415692637656\",null,[[8,1]]],[\"9797767207516844257\",null,[[0,1]]],[\"14906952326733574741\",null,[[4,1]]],[\"4891744519482609478\",null,[[6,1]]],[\"14307859671070593733\",null,[[1,1]]],[\"7494582641517049914\",null,[[4,1]]],[\"6667106912793420619\",null,[[0,1]]],[\"10118692516388306266\",null,[[1,1]]],[\"408159237941253787\",null,[[5,1]]],[\"476083397694989718\",null,[[5,1]]],[\"8791060314450143495\",null,[[1,1]]],[\"6342145065879578001\",null,[[3,1]]],[\"13596961294000664596\",null,[[202,1]]],[\"2107494750385856652\",null,[[7,1]]],[\"1309831198388189068\",null,[[3,1]]],[\"522022639063469804\",null,[[0,1]]],[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[187,1]]],[\"3079121564595244695\",null,[[159,1]]],[\"10652791942255425261\",null,[[11343,1]]],[\"4132870161583308123\",null,[[114,1]]]],null,null,\"[1,\\\"qBzSPd_PL\\\"]\"]",null,null,null,1475350117,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879],[]]"],null,null,null,[],1,null,null,null,null,null,[]]],"1619591990665",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_25", NS_AUTO_STATUS);
    ns_page_think_time(0.036);

    //Page Auto splitted for 
    ns_start_transaction("m_sy1gf_sy1gg_sy1gi_sy1gh_sy");
    ns_web_url ("m_sy1gf_sy1gg_sy1gi_sy1gh_sy",
        "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=sy1gf,sy1gg,sy1gi,sy1gh,sy1f7,sy1ge,sy1ix,sy1g9,em1u,em1t,sy1gd,sy1iw,upload",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("m_sy1gf_sy1gg_sy1gi_sy1gh_sy", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_26");
    ns_web_url ("log_26",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:360",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],729,[["1619591981108",null,[],null,null,null,null,"[[[46976,7,[46975],3],23,[49]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1619591995565",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_26", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_27");
    ns_web_url ("log_27",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:3700",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619591995576",null,[],null,null,null,null,"[[[null,null,124,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,3,4,1,0,true]]],1,null,[null,[1619591988583000,1619591989745000],2],[null,66,null,null,106021],106019,null,124,130],[null,null,126,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,3,2,0,0,true]]],1,null,[null,[1619591988589000,1619591989763000],2],[null,66,null,null,106021],106019,null,126,131],[null,null,117,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,2,0,null,\"NaN\",true]]],1,null,[null,[1619591988565000,1619591989766000],2],[null,66,null,null,106021],106023,null,117,132],[null,null,129,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,3,1,1,45,true]]],1,null,[null,[1619591989682000,1619591989775000],2],[null,66,null,null,106021],106019,null,129,134],[null,null,133,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,2,0,0,null,true]]],1,null,[null,[1619591989767000,1619591989811000],2],[null,66,null,null,106021],106019,null,133,135],[null,null,66,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[55,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,true]]],1,null,[null,[1619591988026000,1619591989824000],2],null,106021,null,66,136]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],3,null,null,null,null,null,[]]],"1619591995576",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=u6ndf3uaetn", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://cello.client-channel.google.com/client-channel/js/1606551176-sharedchannelmain_bin.js?ctype=cello&authuser=0", "HEADER=Sec-Fetch-Mode:same-origin", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_2_generic_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_27", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_28");
    ns_web_url ("log_28",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:308",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],996,[["1619591986857",null,[],null,null,null,null,"[[[66684,null,[],2],[3]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]]],"1619591996889",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_28", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_4");
    ns_web_url ("v2internal_4",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3D2dr1py1l6tri%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:2474",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====2dr1py1l6tri=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/files?openDrive=false&reason=203&syncType=0&errorRecovery=false&q=title%20contains%20'UI%20RefreshIssue'%20and%20title%20contains%20'docx'%20and%20trashed%20%3D%20false%20and%20'0AJWo5b32VY-AUk9PVA'%20in%20parents&fields=kind%2CnextPageToken%2Citems(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CincompleteSearch&appDataFilter=NO_APP_DATA&spaces=drive&maxResults=100000&supportsTeamDrives=true&includeItemsFromAllDrives=true&corpora=default&orderBy=folder%2Ctitle_natural%20asc&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619591995_91f879622c0e2e394eea80ad5e7d474f9b541dda_u
x-goog-authuser: 0

--=====2dr1py1l6tri=====--",
        BODY_END
    );

    ns_end_transaction("v2internal_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_5");
    ns_web_url ("v2internal_5",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3Dei7uevrb9d3%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:2684",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====ei7uevrb9d3=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/files?openDrive=false&reason=305&syncType=0&errorRecovery=false&q&fields=kind%2CnextPageToken%2Citems(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CincompleteSearch&appDataFilter=NO_APP_DATA&pageToken=~!!~AI9FV7TsCZyL2hDGz1TTAnTh8Igrlpf46043MXxE0NPVUzro0xLyML8i9mYL2NpQMuTHaUNDD2hQCVUF9S3NyFwpIho2QOTfKnsK0GOjZyeOzRtH4zvbgiR8lLMndLqCmGKkUysVEUBBPEtKMJw4Zd07Aj9caeFpSdedL_wwMtfCXwqShv0RYnankOuGdMMAusWYybPdZBpQbpEvGHsMDg5gS0glA0DIqiLzFr4uwXLidLBrPnsHHaIYepe2qAU2pbW_BWwpkX27wbtDEftd9yoQzIlTl28bxnLXOpsVbtk1Mc-3iGb6TGqVfYegtuf3VezinXEwH_0-kXCHcgMPUbs-dyz8iiWC-REzWR6Epx5kkpVmoUGnqmY4onzhb4JN4-U2QV0gxHVg&maxResults=21000&supportsTeamDrives=true&includeItemsFromAllDrives=true&corpora=default&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619591996_b146deb2bd87193f66385a39c527a38cd58f3a35_u
x-goog-authuser: 0

--=====ei7uevrb9d3=====--",
        BODY_END,
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/client-auth/", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/doclist/images/mediatype/icon_1_word_x16.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://cello.client-channel.google.com/client-channel/gsid", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://clients6.google.com/upload/drive/v2internal/files?uploadType=multipart&supportsTeamDrives=true&pinned=true&convert=false&fields=kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed)&openDrive=false&reason=202&syncType=0&errorRecovery=false&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://www.gstatic.com/images/icons/material/system/1x/mark_as_unread_black_20dp.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("v2internal_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("files");
    ns_web_url ("files",
        "URL=https://clients6.google.com/upload/drive/v2internal/files?uploadType=multipart&supportsTeamDrives=true&pinned=true&convert=false&fields=kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed)&openDrive=false&reason=202&syncType=0&errorRecovery=false&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:341241",
        "HEADER=authorization:SAPISIDHASH 1619591996_b146deb2bd87193f66385a39c527a38cd58f3a35_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=content-type:multipart/related; boundary=\"o3hd2dq01s3w\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        MULTIPART_BODY_BEGIN,
            MULTIPART_BOUNDARY,
            //--o3hd2dq01s3w
                "HEADER=content-type: application/json; charset=UTF-8",
                BODY_BEGIN,
                    "{"title":"UI RefreshIssue.docx","mimeType":"application/vnd.openxmlformats-officedocument.wordprocessingml.document","parents":[{"id":"0AJWo5b32VY-AUk9PVA"}],"modifiedDate":"2020-06-10T16:29:31Z"}",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--o3hd2dq01s3w
                "HEADER=content-transfer-encoding: base64",
                "HEADER=content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                BODY_BEGIN,
                    "$CAVINCLUDE$=http_request_body/files_url_0_1_1619592544755.body",
                BODY_END,
        MULTIPART_BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/2/openhand.cur", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("files", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_29");
    ns_web_url ("log_29",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1910",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619591998588",null,[],null,null,null,null,"[[[null,null,137,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[8,6,null,1,null,true,false,null],[55,null,null,null,null,true]]],1,null,[null,[1619591995749000,1619591995750000],2],null,106001,null,137,138],[null,null,140,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[10,9,null,1,false,true,false,null],[55,null,null,null,null,true]]],1,null,[null,[1619591995775000,1619591995830000],2],null,106001,null,140,141],[null,null,139,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[3,1,null,1,true,false,false,true,false],[55,null,null,null,null,true]]],1,null,[null,[1619591995773000,1619591996389000],2],null,106001,null,139,142]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],4,null,null,null,null,null,[]]],"1619591998588",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/gsid", END_INLINE,
            "URL=https://cello.client-channel.google.com/client-channel/channel/cbp?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&MODE=init&zx=mi0cqdkjsog8&t=1", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://cello.client-channel.google.com/client-channel/channel/cbp?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&TYPE=xmlhttp&zx=hn5mjre3naqt&t=1", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_29", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("bind_4");
    ns_web_url ("bind_4",
        "URL=https://cello.client-channel.google.com/client-channel/channel/bind?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&RID=65885&CVER=5&zx=2rzgtzfkxix5&t=1",
        "METHOD=POST",
        "HEADER=Content-Length:7",
        "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6",
        "HEADER=Origin:https://cello.client-channel.google.com",
        "HEADER=X-Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "count"
value: "0"
]",
        BODY_END,
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/channel/bind?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&RID=rpc&SID=2B5847E73F3DE61&CI=0&AID=0&TYPE=xmlhttp&zx=rbegem6b45dl&t=1", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("bind_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("bind_5");
    ns_web_url ("bind_5",
        "URL=https://cello.client-channel.google.com/client-channel/channel/bind?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&SID=2B5847E73F3DE61&RID=65886&AID=1&zx=txe8pz4kdf1c&t=1",
        "METHOD=POST",
        "HEADER=Content-Length:365",
        "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6",
        "HEADER=Origin:https://cello.client-channel.google.com",
        "HEADER=X-Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[key: "count"
value: "1"
, key: "ofs"
value: "0"
, key: "req0_p"
value: "{\"1\":{\"1\":{\"1\":{\"1\":3,\"2\":2}},\"2\":{\"1\":{\"1\":3,\"2\":2},\"2\":\"10.0\",\"3\":\"JS\",\"4\":\"lcsclient\"},\"3\":1619591998400,\"4\":1619591999253,\"5\":\"c1\"},\"3\":{\"1\":{\"1\":\"tango_web\"}}}"
]",
        BODY_END
    );

    ns_end_transaction("bind_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_30");
    ns_web_url ("log_30",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:3929",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592001596",null,[],null,null,null,null,"[[[null,null,143,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[56,null,null,null,null,true],null,null,null,null,null,[1,true,null,false,false,255613,201326592,0]]],1,null,[null,[1619591996415000,1619591998722000],2],null,106012,null,143,144],[null,null,147,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[8,6,null,1,null,true,false,null],[56,null,null,null,null,true]]],1,null,[null,[1619591999433000,1619591999434000],2],null,106001,null,147,148],[null,null,145,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[8,6,null,1,null,false,false,null],[56,null,null,null,null,true]]],1,null,[null,[1619591998729000,1619591999457000],2],null,106001,null,145,149],[null,null,150,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[56,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,1,0,null,\"NaN\",true]]],1,null,[null,[1619591999497000,1619591999539000],2],[null,146,null,null,106021],106023,null,150,151],[null,null,152,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[56,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,1,0,0,null,true]]],1,null,[null,[1619591999540000,1619591999541000],2],[null,146,null,null,106021],106019,null,152,153],[null,null,146,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[56,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[1,true]]],1,null,[null,[1619591999433000,1619591999563000],2],null,106021,null,146,154],[null,null,155,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[56,null,null,null,null,true],null,[1,true,true,false]]],1,null,[null,[1619592000762000,1619592001240000],2],null,106003,null,155,156]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],5,null,null,null,null,null,[]]],"1619592001597",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://clients6.google.com/drive/v2internal/about?openDrive=false&reason=304&syncType=0&errorRecovery=false&fields=kind%2Cuser(kind%2Cid%2CpermissionId%2CemailAddressFromAccount%2Cdomain)%2CquotaBytesTotal%2CquotaBytesUsed%2CquotaBytesUsedAggregate%2CquotaBytesUsedInTrash%2CquotaBytesByService%2CquotaType%2CrootFolderId%2CdomainSharingPolicy%2ClargestChangeId%2CimportFormats%2CgsuiteSubscriptionInfo(trialMillisRemaining%2CtrialEndTime%2Cstatus)%2CteamDashboardCapabilities(canAdministerTeam%2CcanManageInvites)%2CgraceQuotaInfo%2CquotaStatus&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyAWGrfCCr7albM3lmCc937gx4uIphbpeKQ", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_30", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("about_2");
    ns_web_url ("about_2",
        "URL=https://clients6.google.com/drive/v2internal/about?openDrive=false&reason=304&syncType=0&errorRecovery=false&fields=kind%2Cuser(kind%2Cid%2CpermissionId%2CemailAddressFromAccount%2Cdomain)%2CquotaBytesTotal%2CquotaBytesUsed%2CquotaBytesUsedAggregate%2CquotaBytesUsedInTrash%2CquotaBytesByService%2CquotaType%2CrootFolderId%2CdomainSharingPolicy%2ClargestChangeId%2CimportFormats%2CgsuiteSubscriptionInfo(trialMillisRemaining%2CtrialEndTime%2Cstatus)%2CteamDashboardCapabilities(canAdministerTeam%2CcanManageInvites)%2CgraceQuotaInfo%2CquotaStatus&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619591998_76b318a4b451b77693b7de807dbf48b46220c2c8_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("about_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("subscriptions_3");
    ns_web_url ("subscriptions_3",
        "URL=https://signaler-pa.clients6.google.com/v1/subscriptions?key=AIzaSyAWGrfCCr7albM3lmCc937gx4uIphbpeKQ",
        "METHOD=POST",
        "HEADER=Content-Length:63",
        "HEADER=Authorization:SAPISIDHASH 1619591998_c52f9bc88de9cb306e5caf3f1ee8970d9b94c32b",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[["Drive",null,"v465Uax+V0DN0Rky",11,"pEMKtrCwulg="],true,null]",
        BODY_END,
        INLINE_URLS,
            "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyAWGrfCCr7albM3lmCc937gx4uIphbpeKQ", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("subscriptions_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("acknowledgments");
    ns_web_url ("acknowledgments",
        "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyAWGrfCCr7albM3lmCc937gx4uIphbpeKQ",
        "METHOD=POST",
        "HEADER=Content-Length:250",
        "HEADER=Authorization:SAPISIDHASH 1619592000_c0a858dd4736c2fcdeca4a84e9978f9a7e634cc1",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[null,[["CpYBMJP3l/n6yb6mKzoMOTQ3MzE4OTg5ODAzSnwKFioUVpn0n15L9yf744RuPOUe3WQ3cCAKFioU0Pbm7Uv3meB1PJIAq+WLxxfEtmcKFioU5Iybi8cFYJMJy0jJ9EYz8C4MD34KAlgBCgkIobaYr6qg8AIKFioULTE4MjA4OTM2ODkwNjU4NDI5MzYKCwjth72usJaq5sUBEQ6z6j2uOqDtGAs=",1619592000158000]]]",
        BODY_END
    );

    ns_end_transaction("acknowledgments", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_6");
    ns_web_url ("v2internal_6",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3Dks9uc7wpvl1%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:3236",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====ks9uc7wpvl1=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/changes?openDrive=false&reason=303&syncType=0&errorRecovery=false&includeDeleted=true&includeSubscribed=true&maxResults=1000&startChangeId=62515&fields=kind%2ClargestChangeId%2CnextPageToken%2Citems(deleted%2CfileId%2Cid%2Ckind%2Cfile(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CteamDriveId%2CteamDrive(kind%2Cid%2Cname%2CcolorRgb%2CbackgroundImageLink%2Ccreator(kind%2CpermissionId%2Cid%2CemailAddressFromAccount)%2CpermissionsSummary(entryCount%2CuserEntryCount%2CgroupEntryCount%2CmemberCount%2CselectPermissions(kind%2Cid%2CuserId%2CemailAddress%2Cdomain%2Crole%2Ctype%2CadditionalRoles%2Cview%2CaudienceId%2CcustomerId))%2Ctrusted%2CprimaryDomainName%2CorganizationDisplayName%2Crestrictions%2CbackgroundImageGridViewLink%2CbackgroundImageListViewLink%2Chidden%2Ccapabilities(canAddChildren%2CcanAddFolderFromAnotherDrive%2CcanChangeTeamDriveBackground%2CcanComment%2CcanCopy%2CcanDeleteTeamDrive%2CcanDownload%2CcanEdit%2CcanManageMembers%2CcanManageVisitors%2CcanRemoveChildren%2CcanRename%2CcanRenameTeamDrive%2CcanShareOutsideDomain%2CcanDeleteChildren%2CcanTrashChildren)))&appDataFilter=NO_APP_DATA&filters=DRIVE_DATASERVICE&spaces=drive%2Cphotos&supportsTeamDrives=true&includeCorpusRemovals=true&includeItemsFromAllDrives=true&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619592000_3fc1fffb992fcfac3c334cbed367cb2faf77c7e1_u
x-goog-authuser: 0

--=====ks9uc7wpvl1=====--",
        BODY_END
    );

    ns_end_transaction("v2internal_6", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_31");
    ns_web_url ("log_31",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:2005",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592014473",null,[],null,null,null,null,"[[[null,null,4,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,false],[[],true,[8],null,[5,7],28,1]]],1,null,[null,[1619591983219000,1619592012905000],2],null,106002,null,4,168],[null,null,177,1619592014422000,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,true],null,null,null,null,null,null,null,null,[28,0]]],1,null,[[1619592014422000],null,1],null,106015,null,177,177],[null,null,172,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,3,4,null,0,true]]],1,null,[null,[1619592014393000,1619592014473000],2],[null,157,null,null,106021],106023,null,172,178]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],6,null,null,null,null,null,[]]],"1619592014473",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_31", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_7");
    ns_web_url ("v2internal_7",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3Dd77e1u4n1qua%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:2686",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====d77e1u4n1qua=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/files?openDrive=false&reason=305&syncType=0&errorRecovery=false&q&fields=kind%2CnextPageToken%2Citems(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CincompleteSearch&appDataFilter=NO_APP_DATA&pageToken=~!!~AI9FV7QS8rvCLIH0iZXHyvFYQmkYsZB1R0yKq_vp2rQzC_BOplrFwYOy8s5rKtmXP_NbuOAcT9qTrxxXgr3iCWVNQYikdmnR71hfWCBNMrMonITmMLhVTCUyLRY67WpRSYHIDAiJkcUVqIOFjNn1CBRMJ_LqPULFQJ2Ac85bC4j9r9cpvdYpVmXpC-QW4gH6fLsn3fabVrxi_wsNGsbvpcE0IJE1h6NrzJG5eR_Ybt--KOqDy-Mm25xXe4YiosyBqKE9ZmfxtOl0FkK9q_mn2MeEKiQo79Oh2kcQHUTupARoTs7uPIUNMk89-3pDHP4FLGJgumtW6Cx2RyPbWhk11kAhrkHlHePYwtPPS2CNLuBwhb0fA5qfrhaTJvWKJpNjQu2_IM-QRb9c&maxResults=20000&supportsTeamDrives=true&includeItemsFromAllDrives=true&corpora=default&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619592004_26237d996877e6aeb2c2f5d4e6d659986a78b3fe_u
x-goog-authuser: 0

--=====d77e1u4n1qua=====--",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=283yv4luw7d0", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("v2internal_7", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_8");
    ns_web_url ("v2internal_8",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3D4u6wld21igve%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:2686",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====4u6wld21igve=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/files?openDrive=false&reason=305&syncType=0&errorRecovery=false&q&fields=kind%2CnextPageToken%2Citems(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CincompleteSearch&appDataFilter=NO_APP_DATA&pageToken=~!!~AI9FV7Q_DWmtgee1WsTlg1bLq0ZHxg2kQ4nufFL99pTLE7GmPp_IKYqdsAj4wTuZM1JR5aVrZcEYNskxtKMg2YtNhMswSqO9UwlkjbhvYVtz8hcnfAWzWArCkcHblA9tmv-vDSPrCncxB4hb0OmdC2-rmUV7YWUWpPBcBkAKm7s7SGkCSxIvZQqVwOnF-17yZdlIU2ZN3ScsHGyoQPJ8u-wzBR7xC_-mV7t_kMmyC-F1JfliJutzwxc7frU0C5itT5D0FJy974s9S9gE2b4Wkx7MzkziURxpGwLS3wmPt7-9fzdDbQcOpYB4RlaHazfYK-4Aqk__8V6ieHWgeA2TsIvSqwKFHpJJZ6liop-8cjmCy7fCNa4dgyB2RkasmnZZlfgJoG6bSATN&maxResults=19000&supportsTeamDrives=true&includeItemsFromAllDrives=true&corpora=default&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619592009_487e23532b377771893b9893349097ad2725adde_u
x-goog-authuser: 0

--=====4u6wld21igve=====--",
        BODY_END,
        INLINE_URLS,
            "URL=https://people-pa.clients6.google.com/v2/people?person_id=112379449936499860056&person_id=114403597203212744593&person_id=109945607986663397742&person_id=110354069898457460512&person_id=107746171132689817283&person_id=114536419672752884678&person_id=114840858107054395178&person_id=113823140766129294218&person_id=104816726882095170185&person_id=110623666277524361650&person_id=103475005534909181677&person_id=115183828290246331720&person_id=109441144649591855061&person_id=101683055057116577695&person_id=106066914064155175657&person_id=103870495185862401155&person_id=109399432418701376105&person_id=109101877694441915204&person_id=102189725510412263799&person_id=101632424810107997384&person_id=106991418431646771036&person_id=100154392822650159818&person_id=107020764352471885398&person_id=118231848891495877239&person_id=103969360057359336526&person_id=113287134732058994995&request_mask.include_container=PROFILE&request_mask.include_container=DOMAIN_PROFILE&request_mask.include_field.paths=person.metadata.best_display_name&request_mask.include_field.paths=person.photo&request_mask.include_field.paths=person.email&merged_person_source_options.included_profile_states=DASHER_ADMIN_DISABLED&core_id_params.enable_private_names=true&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,x-goog-authuser,x-goog-drive-client-version", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("v2internal_8", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_9");
    ns_web_url ("v2internal_9",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3D2ti2omxm3crj%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:19694",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/v2internal_9_url_0_1_1619592544777.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://lh3.google.com/u/0/d/19CWe9D8qne71Au_HcywawI0GrnBuQIef=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1flGeNrmwtbuznbRQFnRcD0a8QtI=w300-k-nu-iv1460508356560", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/11LhZKLlmfG2KstMMiTX9U-rbUKes61PhhV_GtWQEsNg=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UIkeEoSX1nJ0VltEvH3IqUUyWyKFopPb=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UIfLhQWpC0A7hCphZY7BpskL6eGNfVrC=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UUzDKMNSdw7Su1wmvO9lyeIL3GM_BOv2=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1ULxquWPx7Ll1D7BduCG9WopHlTnDVZiP=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.google.com/u/0/d/1UFO0mQAcQeFAphkNXNQHHdIZ3NJyo55F=w300-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://drive-thirdparty.googleusercontent.com/128/type/application/pdf", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("v2internal_9", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("people_2");
    ns_web_url ("people_2",
        "URL=https://people-pa.clients6.google.com/v2/people?person_id=112379449936499860056&person_id=114403597203212744593&person_id=109945607986663397742&person_id=110354069898457460512&person_id=107746171132689817283&person_id=114536419672752884678&person_id=114840858107054395178&person_id=113823140766129294218&person_id=104816726882095170185&person_id=110623666277524361650&person_id=103475005534909181677&person_id=115183828290246331720&person_id=109441144649591855061&person_id=101683055057116577695&person_id=106066914064155175657&person_id=103870495185862401155&person_id=109399432418701376105&person_id=109101877694441915204&person_id=102189725510412263799&person_id=101632424810107997384&person_id=106991418431646771036&person_id=100154392822650159818&person_id=107020764352471885398&person_id=118231848891495877239&person_id=103969360057359336526&person_id=113287134732058994995&request_mask.include_container=PROFILE&request_mask.include_container=DOMAIN_PROFILE&request_mask.include_field.paths=person.metadata.best_display_name&request_mask.include_field.paths=person.photo&request_mask.include_field.paths=person.email&merged_person_source_options.included_profile_states=DASHER_ADMIN_DISABLED&core_id_params.enable_private_names=true&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619592012_4184d06cecdb09a59941875a7d69845d52abeb96_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoYMBFrHdRtg527K2WnrTGXxmbJ-6PHW73Cd4-kXVRZY8PMIT26xXf0fBRwhXSNGOyOeWXUGts2jnplqVqc2hthEIZk-cKiuYyLERMSvkMIDZPtvtBxi7x89Tp4KsmICW78S975dupF1rwb_Mms1RdyPC5JrAzHw82w48Vu_Sq53mOYXdmGEqa8KZ88bvuCE8l1F8z4km8DejkpOc5L0LoqmjC0ZBWhHwylPbz6APcfyeoERBlLxBMHncGQOYbrf4tL1mM6u2JJ-dwvZabISdIgLpezRA1cx0FfRR_NJSbHh3IGBIFfaFoZmwI4Y2SPVsVtIlVdnMyzQaudtRN338AzVl8H3nU0iZ0kpS7pvhmKXnmb0UaQmxe--tZf1TY5r0DjgNd1q7AMPX-Zg2xGUhhZf9WkQGIpzxqb6qJSZej6xV4rakooHuCLP9KHagPXsMtdTIMiEMUJVAsY4DjpSdOssgxyoglfLVuBdqdZL0zwet72rX9AH97inltjb-IrRW3genAsEGTYX2DfRIBsNvtSzymc3wcrQkqX3yrm0Ad9eiurp08tQPapa88kEfkhqIDs6zkgLuZhmjgCVQhoI4V5BQPszbFSvKr184G1VLkjva7EJhNc6DBVOJvgpsI5sUfQF3W60BSNVCkLsW3DCRrCxAt8iem26JTR70ARDSoYOrlZv8vQDIWF9wv8b3zCeLO9cfVy5s5Jz1a2cOTeXRhhxJpWd3TndLA=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIrNYE_8CFaps9Z_PF0Rnk9rqgflpJ1sw3cm7tpVIK00zhi-DR9BMvJ3osQ3TSerpYFMMeZxa_-UX1d7fbKob7BbWymoUaBIeJepFzl27Z3vv342DugKTZGuaEeMeccFhahJCRCF2UgZsW4WFoglY9UtOfhhQSEpove_a1EtyXFKBA5tva1sB4K9flkbOQT3wegmVqyxxiYqCYwOY9pqEbGEC8Quv9qVxC8YwDFlaQMDHTPchyfHLX4mH-aqcucGR0Nr0nIlAd73JJiQP_d1ZKq3sL1ovsNTjaW441GVgcMLeBW3bpf-XPJUR_fsxzwTTppsz85x19ccwNGYYOmHx7WKHrmeZPpa2-bPcBrXihjzChbqoCVLfn0yTJ5UmziGcrS6cDJ2-bf1weMmVoBvrTge-0yXMRqaZkh_qGbfwov1S9_oNxwzucbxbalKY72za3rvMnSR31ACPZsXbYRZvOwsqc6c-RqsdbSm2MaV01LljC9CM49TR4XiO_N0bc_WI8OV9msYc7DvcKHG3Q5c03CZIEN2Cin-iogZUWItpeyTBA0SmkwXFhoIrpeaKHP9MC5pyacb8pYFK7alzwcfyH3EDK7E7CIV67mlisvRoqVu8PWlVy22ZIx9EAq-uON2n4ANQ-rE-b7sNGLHzrUmxmJIJau3vlgh1H0o5uAtWn565boE5MHtRINKFqMK7tOhf5KcvM0ZvY4CalX8Ng1DT2dFiMVF4CeJdfuwyKx4BoCkzQ=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIowvN8XsMbZiQcpJpR1HNQHy9fdy0uRIKtw0hnar4PQo4BrnYFVc0Vj9PsqLhni8nDnOEyX1gMUHRr_dbiqYhBH8yiIwAFGM2rAPNqN82-OEv29fALESC0jCIKqFneNxftmebxPJnJtPG-3xxlSQleIlb7GeMWh7NOWgm16uEfpLuZCKIIJV9mt13kxt9umB1zQVFkSyM8lbYK8OLhYFDyWLaT_piRWvrGtPxKSe33B8diaFPqTwzqtG3jHMlsjwdiw9OMZ2O9-pq7v3pAKkgCZigqUigoyGPysf8XOalRlarkaPVbiKOk4A9j3Lmt41_yhX-uH01hKXpMQZv0smBDwPTab-KQA_6MIWuGNXBDlZO8hPthyors8EKYzmMX7LVkh-6WIWEN8sOsWWxmZ0yH7LtgZyAbyGIcpjlkjFle4W32bjgeK1fWKRdgyXbABrlAWA4TiAovDor2E3Us2FcnVepMXFlbgiq-N70Bsyvn4IuRDpVRm_RqSDNopmEiyF3Be68caW-oY0_1jQ557_zDGq79dd6lUwV4RXj5pHEIhNPLzHKURuGLn7rB38FFbfvvI4g_1tIUp6Rnkvut_qIsIiLd20lvj4kCAp4pVuYQecPjn8x0aL2ap1X_dltvjqx1LhgKuq9bxwvO7LzB2dp3pTulrRMBQn915IPoG8ww64oZ5RHPUUX3Th1EGu6T5qhGYNymzRV5q45axhAA7eAHWqI8MTW-4NPU=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqiYlMBHti4OcwjOTCUTeKgu41dPk7Cwj9et8s940NVy6q0qnQBFDSq5Ewws9L8AL1ZHXZSw8OKjgOLa18fQoAyxj4TvNdmrKnjjHc90f1X6i6syUh0nU4UEW34wO_iqbgI5czh9-DN5sJ3W_d212vlh6hT41qpO425anSc2s7cq9zGQX28qZMLkFNwHvOA_a_bbmoSroO7qak6x4h22d70otQBD4DyAwvTqyBCOejG7ThjNL6a4qHIBIxwgtfIedLP5RQfxF9EiLD4QrPO1u5ECmR38eecDtJM8Byl9JlX5dQqsJxGegtTYdGx0ZGoEJIpjEdw1s3FjTsN7iN_HHemDoixkEOkmzkhaeE6GxRc570E2Xf9FsjBEQwUZ5AxlXEsIwo4dsvd-FudISQv9Ruisz-UYx_xYtmzkjzmgc9T9W_ZuZasFSlI62lSLQvX5f26Pb8rLIoZusXxPNk1homqRzj2lg554wfkgsGxVt12RQhDfWMTHSAvCsuooz9MEZ9Qlk38fZs1HsVENyDoy7sMAokqqBBA5GghMyeiG67EJe99gJvr3veFXY2xoNQnCEZA4GJly6cfyFaO45XBaLi_5W1DV2sLPIYj56cIvr11HsK9fhI5yLA1KBN3lPmHtLqXfBTx96KEnBMS8O83UlqUPuYiQsihjzaCxZwXTE3k6D0y8YO7QpOtOKKSyy0Ip3SRxBJEuQ3UKpq-tM4r2FDZN6qggLz8qoA=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIqjGPtEs_F29AfbWMdgTG5aGFW5AtWFCeF1OcDKtn5wfgxZPMpGO_zxatE2Z1_hGoG1kmGPG4Z6q1YzN7CG2SzEviV34FZ3WYX2slF5Mkiipr8LjHIe8iYSNjj4sfV8c2gPMVdWWsjI7j5gE-_BYwIvry6hJIXe9BOYZ-rU2ZedY6CH-eZhlnWWui3d20bdpBNzUQs_PyTfnlE9n0iyNgtO8YfC3jnHPmihH2f2weChQ3XUwlLsDWEWvqZVWL_GpV1Dhv9PZiNlr1ZHgWSHaAsLBzVHbLNmRYYKVJh0Z_dPxLlsyukh1G-QxT3ojSsF5_9Rt_QY8Vr0zg2blYOTEc3QXGc7erw8aFIYJBVreTLS1nmjT6UwxKSuIc2U99HFV2_UEGkJYavIyFlRiQ_omksOkTaBTwszMutVxBfRlyiqCdavOHFfPPwVL30O_2timmfLF5mL0ZSqitCMpm24_D7RqMRBfxzUODn6nWCkuiN_RicJhOe-WLrfOLDV0jivY5OIlqoFTDv1ogoao24JPiszOAG-bmb-ANrD_ag5NlXPVlen1olv91W_Y5Dzaehslf8sITumA17VB-buB-Sm46J7bvsVdXpQRkhcz2JqzE0bR3vpd81MBiRIdzpReF5RdUO-R6mjs_JdKumiCGWczeJB_NpNq9ZD7ncQO7H3tFfMFKE1nHRCVz9cNVGGDgg_a5Xe9i-LytDBWp0FTH5Dm--lNiv5zXtyjdo=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIprJciXFiHEmRmdYrW6Wd-2dee3tiuxBo8DN5EiJYgCAxCTi3dVQXMeq9y_8GolEgbjnQHPaEPIkiaY1xUP-NkqeFBE2NzRcal5CWy5tbwEAiRPdff3ylquAYX5KiS_qU0VOX5elsMMo18s5GV2YV2d6UUf2Uwez5ydbXUCqMEdzfapmFpJR7fO660fcWIsJTESbts9gdMciq8Z_1BHFtbxObYcjcGLlI-B-d7QVJcaDLizJh2o6e0nxJ3IzQJoqH5TnYyTfPkMzCWoqniQ28Cb5ZkmfcKFkC7nYAVeSdKwU9wkVqsCpag6V7wdQSUnDkVXcuan-CvoQOHiREQ_xx5M74_r8NEV6ukAYu3PuOB2GNF3bIPfKdvCNkrEhSnJ6M7R_NH5ozycCmVwrPP42-Y79t-Qfgv5xxV5ESLRn5msloK7CvKxoZR9AYnVv_z0RwHZnbozqFsJZMwdSt-qsivIDxCInjCM9f2wedPg9TqoEG9tKC8Pr63UmBp0Z677SPZrN0USxrvpX4Sy65nilnTRWJs_1BQBBgFDq0kW2GT6LVfGaGWmVDYsuhiWkoE5q4VSiT06T7hh0q8kmtgcXHBj9SVWI8s1fJNT6tkwy4eoJlVK-NZ3RPyiW3H2JFqtO0coSbZNk4YGMa5xLvtQLRdYBWxWP427MUrEF_z2tW4wXAiFE3jJh182mCMXW1nnAN8QVDWb8QPS_SWxcuNal1FZHbfztK4AnZQ=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIo9zNzVkkGSozdlE0WWST996B6kaTuOs1rUMKLvZTuY4ZK4QvYfAKwSQoLlVzUVyxhvF72IP81wPSWkqwQk-LOdEJpV5ABzXt0QMfbyv035LAECzmi0lOl91W5wx9kqi7pjKLEZTKyyPzpe1j0L6X8QKnKbUJvHhEq0qQo6rTwkbtsfcBVp_f81Cay-c8Wr44O_BUhoxWV0llsimu9mJNcYN7cn8ECPOIu3-p_pAvQXzaPqdckIW9H9ol-4ydVzGZud2zCoAF3TXcW68Sn3NGV5etn4bVdChsCAkJhqRcW9miKuimBKc_L_V9Js5xoZoIdAHdkazccoPd_-90LmWcW879f1izdRJNeMII4PyF2mlH6S6VRC84nHKLz73ATEv-U7yGjc7CN_tExtJtCg99iAyOG98hKBzefNdCr5-B6jerQ8a9uLOuYKXVGRqCRXS4JC-vwYMqI6G_KroGh-C9AUjzJUHVhk_E--ypeu4eD9l4K2Dgc7C2enEYf8E4gJlHy5dbUjylId9xIhS13Z6keimb1Volp1O9gMNymG034K8a9lAIH1N3A7o-TTYfC_JcC2BZZT6Lo4W_k7LveGrzIBGvkb5HSEQJOk-L1T2oKujQbwZ27mWlrCrJZ6HOeeqv7r92yp54wHPvKGQQ82_Cf7_MXl4tLYb0cedKdgbrQwTsI9ZNqQJtg2eFs2IygfBEN7IwFFE8znkln3EpHrw3s3DnySsvkoVFE=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIpi-3SnzwRsM799hTcX44lnWkfyXJLMB-HD_v7c8qE5j6l8cBZmTgzdeTQGXOL7h_gTWtUorGf5HpUP90Glm9my3cz2udzrEwrqZYEZy78KmCk6zu2UguPyyYbkQANSHjzj3Kk-h1ZVD0EN9dMKT1JZ7XxkUp_0w91QGCanzRAxWyjCSJwk0VzGKhBhhL7GQwvoAB8mCg-BViCeChthAchtd3zvo9l_4JTnqzFpAzA5ogTp9bL-5mGwkEMGN0RHVOTRMy5anjkRmi2m4nBoAPpZp-3UpftY5btdlJ2Vncs-8wWQfsozrHb5v_I2N2arXDAQijQIx1FbufuFP74s5fBY0_Ms5Yep8yVr3ZQvLzkl7Q9dHZv2U_hJ9zP1QeM-CiksR0R4pbcf9kwJ0ci7ZsYk5ih-v0NWcKqMw2xVPm1RSQbEcmNhYcmi1KfiMl9ErKnxmP_-0OCAdXPSvesq096m_dOnIaS4fXOXso0bz1kGR3dzsHC0wa6pjOdCVX9i9qwUMG4zx8G9kl0WLdmOimHIhXmJPtgzIvom3eamkAgGhS641Ul67cnCqmwcyOR2EB-XrCTCIxKQn-kfkctRFWZ3VjvF2PRkkTkdjMiRuiFU9xp_VtdO-sdebZ3qv6CPVyM7EdJH07qJcPH4LZ-HSWMx9MwDacpmogP1bzl0Pn013-wrfmPgj-npyMo_48QFssMHKXs_gLZpT8HtWo1SkdSCMtvXYr3_nzi7=w300-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("people_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_32");
    ns_web_url ("log_32",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:17629",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE$=http_request_body/log_32_url_0_1_1619592544798.body",
        BODY_END
    );

    ns_end_transaction("log_32", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_33");
    ns_web_url ("log_33",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST"
    );

    ns_end_transaction("log_33", NS_AUTO_STATUS);
    ns_page_think_time(0.201);

    ns_start_transaction("proxy_html");
    ns_web_url ("proxy_html",
        "URL=https://clients6.google.com/static/proxy.html?usegapi=1&jsh=m%3B%2F_%2Fscs%2Fabc-static%2F_%2Fjs%2Fk%3Dgapi.gapi.en.jcYff4gdSOQ.O%2Fd%3D1%2Fct%3Dzgms%2Frs%3DAHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw%2Fm%3D__features__#parent=https%3A%2F%2Fdrive.google.com&rpctoken=723225357",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://apis.google.com/js/googleapis.proxy.js?onload=startup", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.jcYff4gdSOQ.O/m=googleapis_proxy/rt=j/sv=1/d=1/ed=1/rs=AHpOoo_CvAHQybwQAZJQL2tdeysMj0HgHw/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("proxy_html", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("viewerimpressions");
    ns_web_url ("viewerimpressions",
        "URL=https://clients6.google.com/drive/v2internal/viewerimpressions?key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4&alt=json",
        "METHOD=POST",
        "HEADER=Content-Length:362",
        "HEADER=Origin:https://clients6.google.com",
        "HEADER=X-Origin:https://drive.google.com",
        "HEADER=X-ClientDetails:appVersion=5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36&platform=Win32&userAgent=Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20Win64%3B%20x64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F79.0.3945.130%20Safari%2F537.36",
        "HEADER=X-Goog-Encode-Response-If-Executable:base64",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Content-Type:application/json",
        "HEADER=X-Requested-With:XMLHttpRequest",
        "HEADER=X-JavaScript-User-Agent:google-api-javascript-client/1.1.0",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=X-Referer:https://drive.google.com",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"kind":"viewer#impression","impression":"[[[[null,1,null,1619591987839,null,null,null,null,null,16]],[\"fpraziio3kyl\",1619591987806],[[1076,498,1076,498],\"en\",\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",null,1052,386,1,null,null,1],10,true,[\"20210414.00_p2\",\"1\"],false,false]]"}",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=tg69tu0m2ir", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=i6beq31pirs5", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("viewerimpressions", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("logImpressions");
    ns_web_url ("logImpressions",
        "URL=https://drive.google.com/drive/logImpressions",
        "METHOD=POST",
        "HEADER=Content-Length:53134",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://drive.google.com/_/drive_fe/_/js/k=drive_fe.main.en.30EIyNhgiKE.O/am=hM8AcqSwQ3LXgugIKCAU/d=0/ct=zgms/rs=AFB8gsyC3kUyk9wdySerik_hFKaC4GAZGw/m=IIPcyc", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("logImpressions", NS_AUTO_STATUS);
    ns_page_think_time(8.357);

    ns_start_transaction("preload");
    ns_web_url ("preload",
        "URL=https://drive.google.com/drive/preload",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://docs.google.com/document/u/0/preload?source=drive", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/presentation/u/0/preload?source=drive", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/spreadsheets/u/0/preload?source=drive", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/drawings/u/0/preload?source=drive", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:nested-navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("preload", NS_AUTO_STATUS);
    ns_page_think_time(0.418);

    ns_start_transaction("preload_2");
    ns_web_url ("preload_2",
        "URL=https://drive.google.com/file/u/0/preload?source=drive",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:nested-navigate",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC"
    );

    ns_end_transaction("preload_2", NS_AUTO_STATUS);
    ns_page_think_time(636.086);

    ns_start_transaction("preload_3");
    ns_web_url ("preload_3",
        "URL=https://docs.google.com/presentation/u/0/preload?source=drive"
    );

    ns_end_transaction("preload_3", NS_AUTO_STATUS);
    ns_page_think_time(636.257);

    ns_start_transaction("preload_4");
    ns_web_url ("preload_4",
        "URL=https://www.gstatic.com/_/apps-fileview/_/js/k=apps-fileview.v.en.b1MhjE9FHVw.O/d=1/ct=zgms/rs=AO0039vDclgxcFib9tEvd72iDslyRRIK7A/m=v",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://www.gstatic.com/_/apps-fileview/_/ss/k=apps-fileview.v.-YT_M5aTqMc.L.W.O/d=0/ct=zgms/rs=AO0039vZf7cqDTqD8wfQ4AsNf_u1c9mGTw", "HEADER=Origin:https://drive.google.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/presentations/images/themes-29-en.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://docs.google.com/static/presentation/client/css/4269237312-editor_css_ltr.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/drawings/u/0/preload?source=drive", END_INLINE
    );

    ns_end_transaction("preload_4", NS_AUTO_STATUS);
    ns_page_think_time(636.393);

    ns_start_transaction("preload_5");
    ns_web_url ("preload_5",
        "URL=https://docs.google.com/document/u/0/preload?source=drive"
    );

    ns_end_transaction("preload_5", NS_AUTO_STATUS);
    ns_page_think_time(636.311);

    ns_start_transaction("preload_6");
    ns_web_url ("preload_6",
        "URL=https://docs.google.com/static/presentation/client/js/1262026855-editor_js_prod_core.js",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://docs.google.com/static/presentation/client/js/2394217715-editor_js_prod_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/presentation/client/js/894119007-viewer_core.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/presentation/client/css/1047058303-viewer_css_ltr.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/drawings/client/css/846314003-editor_css_ltr.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/spreadsheets/u/0/preload?source=drive", END_INLINE,
            "URL=https://docs.google.com/static/drawings/client/js/1989842512-editor_core.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/drawings/client/js/1585292169-editor_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/document/client/js/2035516118-client_js_prod_kix_core.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/document/client/js/158206670-client_js_prod_kix_app.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/document/client/js/1367374703-client_js_prod_kix_tertiary.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/document/client/js/2400783104-client_js_prod_kix_canvas.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gadgets/smallLogo.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/bg-btn.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/trans.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gadgets/bottombar-right.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gradient-12.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/env.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/share_icons3.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/rows_hidden_hover.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/spinWait.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/button_orange.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/icon-dropdn.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/iphone-button-gradient.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/toastBackFill.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/logo_mobile.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/rows_hidden_clicked.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/hatch-8.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gadgets/bottombar-left.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/menu-downarrow.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/editor_docsslogo.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gradient-on-12.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/ssbar-gradient.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gradient-19.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/hatch-15.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/iphone-gp-back.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/button-bg.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/smallDialogBackground.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/rows_hidden.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/icon-disabled-dropdn.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/modalDlgBack.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/bg-btn.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/cols_hidden_clicked.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/subscribe.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/cols_hidden.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/icon_email.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/button_orange_5x5.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/cols_hidden_hover.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gradient-on-19.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/iphone-google-logo.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gears_icons.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/gadgets/bottombar-fill.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/iphone-view-header-gradient.gif", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/publishheader.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/spreadsheets/toastBackTop.png", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3641775972-combinary_waffle_js_prod_core.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2740061357-combinary_waffle_js_prod_ritzmain.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3292097093-combinary_waffle_js_prod_shell.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3491133861-combinary_waffle_js_prod_inputtools.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3386455023-combinary_waffle_js_prod_postshellbase.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4001681974-combinary_waffle_js_prod_ritzbehavior.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2965590619-combinary_waffle_js_prod_ritzbinaryproto.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/839415600-combinary_waffle_js_prod_ritzcharts.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/799993282-combinary_waffle_js_prod_ritzfunctionhelpcontent.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/62504219-combinary_waffle_js_prod_ritzvisualization.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/622117433-combinary_waffle_js_prod_add_ons_app_finder_populator.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/662709507-combinary_waffle_js_prod_add_ons_host_content_factory.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2671441023-combinary_waffle_js_prod_add_ons_promo_bubble_factory.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3018131817-combinary_waffle_js_prod_addons.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1407397526-combinary_waffle_js_prod_analytics.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3947522898-combinary_waffle_js_prod_answers.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/895995024-combinary_waffle_js_prod_approvals.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1972164357-combinary_waffle_js_prod_assistant.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1758807406-combinary_waffle_js_prod_banding.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/911968261-combinary_waffle_js_prod_charteditor.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2962403615-combinary_waffle_js_prod_charts.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1883831827-combinary_waffle_js_prod_chips.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3548556184-combinary_waffle_js_prod_companion.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3462487990-combinary_waffle_js_prod_conditionalformat.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1688846245-combinary_waffle_js_prod_cse.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1154433823-combinary_waffle_js_prod_dataconnector.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2958792671-combinary_waffle_js_prod_dataprep.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/630170575-combinary_waffle_js_prod_datavalidation.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2838109433-combinary_waffle_js_prod_datepicker.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2976185190-combinary_waffle_js_prod_dbrecord.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3145560168-combinary_waffle_js_prod_dbrefresh.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/347878064-combinary_waffle_js_prod_dbsource.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/828200099-combinary_waffle_js_prod_dialogs.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2970840397-combinary_waffle_js_prod_docos.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3671334148-combinary_waffle_js_prod_drawings.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2133029666-combinary_waffle_js_prod_filterbar.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1090230767-combinary_waffle_js_prod_findreplace.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3681899719-combinary_waffle_js_prod_freemium.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2229995434-combinary_waffle_js_prod_functionhelpcontent.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3887980503-combinary_waffle_js_prod_goto.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1825934799-combinary_waffle_js_prod_hats.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3522291800-combinary_waffle_js_prod_impressions.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/851916023-combinary_waffle_js_prod_links.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/813777245-combinary_waffle_js_prod_meet.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2166945593-combinary_waffle_js_prod_nameboxautocomplete.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4289428746-combinary_waffle_js_prod_namedranges.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4282896416-combinary_waffle_js_prod_officeeditingfileextension.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/216653775-combinary_waffle_js_prod_offline.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4085685758-combinary_waffle_js_prod_organize.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1873162799-combinary_waffle_js_prod_peoplehovercard.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2035174755-combinary_waffle_js_prod_performancetool.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1670317961-combinary_waffle_js_prod_pickerbase.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/515696844-combinary_waffle_js_prod_printing.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3312029732-combinary_waffle_js_prod_protection.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3638037637-combinary_waffle_js_prod_rangeprotection.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/868143019-combinary_waffle_js_prod_revisions.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3136451971-combinary_waffle_js_prod_ritzconversion.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2414527808-combinary_waffle_js_prod_ritzcse.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/776806146-combinary_waffle_js_prod_ritzfilter.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2060278756-combinary_waffle_js_prod_ritzhubembeddingdisabled.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3953019227-combinary_waffle_js_prod_ritzmaestro.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4229948537-combinary_waffle_js_prod_screenmagnifier.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1336659462-combinary_waffle_js_prod_screenreader.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4174551530-combinary_waffle_js_prod_shadowdocs.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/2440151548-combinary_waffle_js_prod_slicereditor.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1952965618-combinary_waffle_js_prod_slicers.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/440025166-combinary_waffle_js_prod_spellcheck.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4242942523-combinary_waffle_js_prod_upsellpromo.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1395614965-combinary_waffle_js_prod_visualization.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/15472957-combinary_waffle_js_prod_workbookthemeeditor.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3728902146-combinary_waffle_js_prod_braille.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/111600264-combinary_waffle_js_prod_images.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/3274946994-combinary_waffle_js_prod_onepick.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/277820730-combinary_waffle_js_prod_queryeditor.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/4154792544-combinary_waffle_js_prod_quickaccess.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1697298108-combinary_waffle_js_prod_ritzhubembedding.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/306458753-combinary_waffle_js_prod_ritzpivottables.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/js/1510078941-combinary_waffle_js_prod_viewer.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/css/413379983-waffle_k_ltr.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://docs.google.com/static/spreadsheets2/client/css/364585578-waffle_k_rtl.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://mail.google.com/_/scs/mail-static/_/js/k=gmail.main.en.N605Puzl050.O/am=rvm_hEcwRrcDPxtGwh0AYDURIAhgEapk8ZaUyv8g-kMC0NdicDAARzwAOofINwAAAAAAAAAAAAAAAAAAANgFlcYg/d=1/exm=AQzzK,Aihl6c,Av2a7c,B00Vac,B6ci9b,BIgGS,CTcde,Cmvl0d,DL8jZe,DN7jVe,DUNnfe,DjaFmc,E1P0kd,EVZC0d,Eaeprc,FSHQ1d,Ff0R9c,FgDqad,H6G42e,HLvCjc,HSnlPc,HXLjIb,IcviEd,IiBrJf,J03Die,J0c0De,Kau0Hf,LMZAzc,M184fc,M25vPb,MAQNkf,MHpzHf,MMhUM,MigGy,N35dyc,NUmjQc,NVcOs,NrTyqe,Nzme9,O9Wdw,OEaYAf,OIxRw,Odkg3b,P8J5Md,PKSrle,PLFcdb,PZhDZb,PaBahd,PtOM,Q8AZuf,RGIyZe,RI4GO,RqJ0h,Sz7W7c,TSMMxc,Trl7bc,UZdBGe,V88ABc,VOAugd,VeTcob,VgcCKb,VoSDvb,VtSflc,VxVlrc,WMHBRb,WQKEXd,WXDMTb,Wwwwbc,XSXB1d,Xg6p3c,YesRdb,Yns7ze,YyKOm,Z16oCd,ZCnZO,ZSB1L,ZThjHe,ZUhrff,ZUkdn,ZdOxDb,ZgCoOd,Zxsddf,a2RK5b,aBj2Kc,aEgFwf,anc,atv,be,bfl9hd,blMNCd,c91q5c,cUTNAd,cXmWHe,cm,coi,cs,cv,dFpypf,dKGjGc,dLIOJe,djoBOd,e,eIu7Db,ebdd,f,f6apvd,gYOl6d,gnUs6d,gr8chf,gtqvOc,gwO7sb,hS6RLb,hkjXJ,i20jfd,idosse,igbF5,it,jPErQb,jVZ0pe,jWbVSe,kL0rjf,kRtote,kbPIy,kx0uyc,l,lC97vf,lKrWxc,lLYctc,laqh9b,lhJ7R,lrkTZd,mDFAuf,m_i,mdFHK,mib1Lb,ml,nORU2b,nXDxbd,ngpaqe,niGKwf,o2ajQe,oRmHt,oTMABb,p59Uie,pA5mjb,pE92lb,pauPV,pk1i4d,pmCKac,pmnVQ,puPi7e,qXzOkf,rMQdJc,rbV53d,rn4kU,s7cQOe,sU8HHe,saV8oc,sm,spit,sps,t,tC0yce,tnECjd,u7EXMd,ua6dje,utMpr,uuoH9c,v2eEBc,vGa3Ad,vXhLBe,w0FYpd,wHcxDd,wS0DLb,wvjFZd,x8nlqe,xaQcye,xgwBDf,xivAT,yWJZbc,zPd59e,zm225/ed=1/im=1/br=1/rs=AHGWq9AL-QDExCHvwExqUW9peCfTiG1FEw/m=spsy", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("preload_6", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("s");
    ns_web_url ("s",
        "URL=https://mail.google.com/sync/u/0/i/s?hl=en&c=10",
        "METHOD=POST",
        "HEADER=Content-Length:82",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592054555\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":{"1":1,"4":0},"3":{"1":1,"5":{"2":25},"7":1},"4":{"2":0,"4":0,"5":257},"5":2}",
        BODY_END
    );

    ns_end_transaction("s", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("s_2");
    ns_web_url ("s_2",
        "URL=https://mail.google.com/sync/u/0/i/s?hl=en&c=11",
        "METHOD=POST",
        "HEADER=Content-Length:77",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592061455\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"3":{"1":1,"2":"2285366","5":{"2":0},"7":1},"4":{"2":0,"4":0,"5":257},"5":2}",
        BODY_END
    );

    ns_end_transaction("s_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_10");
    ns_web_url ("fd_10",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=12",
        "METHOD=POST",
        "HEADER=Content-Length:1717",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592065120\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1698261164786785080","3":["msg-f:1698261164786785080"]},{"1":"thread-f:1698254461567346602","3":["msg-f:1698254461567346602"]},{"1":"thread-f:1698234230791184978","3":["msg-f:1698234230791184978"]},{"1":"thread-f:1698226204198295599","3":["msg-f:1698226204198295599"]},{"1":"thread-f:1698222434570254995","3":["msg-f:1698222434570254995"]},{"1":"thread-f:1698220969016569962","3":["msg-f:1698220969016569962"]},{"1":"thread-f:1698220745989240854","3":["msg-f:1698220745989240854"]},{"1":"thread-f:1698217225653491168","3":["msg-f:1698217225653491168"]},{"1":"thread-f:1698215006529236761","3":["msg-f:1698215006529236761"]},{"1":"thread-f:1698208987830915688","3":["msg-f:1698208987830915688"]},{"1":"thread-f:1698208859129500221","3":["msg-f:1698208859129500221"]},{"1":"thread-f:1698199021106548983","3":["msg-f:1698199021106548983"]},{"1":"thread-f:1698196423443314732","3":["msg-f:1698196423443314732"]},{"1":"thread-f:1698187845916527628","3":["msg-f:1698187845916527628"]},{"1":"thread-f:1698187493357041999","3":["msg-f:1698187493357041999"]},{"1":"thread-f:1698185413450826560","3":["msg-f:1698185413450826560"]},{"1":"thread-f:1698184767059836100","3":["msg-f:1698184767059836100"]},{"1":"thread-f:1698179021135280204","3":["msg-f:1698179021135280204"]},{"1":"thread-f:1698174262635106992","3":["msg-f:1698174262635106992"]},{"1":"thread-f:1698174142620739454","3":["msg-f:1698174142620739454"]},{"1":"thread-f:1698173880280876807","3":["msg-f:1698173880280876807"]},{"1":"thread-f:1698171533111321882","3":["msg-f:1698171533111321882"]},{"1":"thread-f:1698169739362095702","3":["msg-f:1698169739362095702"]},{"1":"thread-f:1698165306563375471","3":["msg-f:1698165306563375471"]}],"2":3}",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=4jp0phv54s9b", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=9crzl7ax78bf", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("fd_10", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_34");
    ns_web_url ("log_34",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:3020",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],893,[["1619592065543",null,[],null,null,null,null,"[\"1619592060536\",[[null,null,[[\"50\",\"1\"]],\"-7442833728258606944\"],[null,null,[[\"50\",\"1\"]],\"-2516723497608421612\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],5,null,null,null,null,null,[]],["1619592071949",null,[],null,null,null,null,"[\"1619592066936\",[[null,null,[[\"50\",\"1\"]],\"-7442833728258606944\"],[null,null,[[\"50\",\"1\"]],\"-2516723497608421612\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],6,null,null,null,null,null,[]]],"1619592075567",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_34", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_35");
    ns_web_url ("log_35",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1073",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592103076",null,[],null,null,null,null,"[[[null,null,227,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[3,true]]],1,null,[null,[1619592053063000,1619592053360000],2],null,106021,null,227,228]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],8,null,null,null,null,null,[]]],"1619592103077",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_35", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("el2_2");
    ns_web_url ("el2_2",
        "URL=https://mail.google.com/sync/u/0/el2?hl=en&c=13",
        "METHOD=POST",
        "HEADER=Content-Length:110",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592110778\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":[{"1":8,"8":{"1":1619592050746,"2":1619592110762,"3":[{"2":6068,"4":1,"5":1,"7":[5449,5450]}]}}]}]}",
        BODY_END,
        INLINE_URLS,
            "URL=https://play.google.com/log?format=json&hasfast=true&u=0&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://mail.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-encoding,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("el2_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_36");
    ns_web_url ("log_36",
        "URL=https://play.google.com/log?format=json&hasfast=true&u=0&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:154",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Content-Encoding:gzip",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/binary",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/log_36_main_url_1_1619592544851.body",
        BODY_END
    );

    ns_end_transaction("log_36", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_37");
    ns_web_url ("log_37",
        "URL=https://play.google.com/log?format=json&hasfast=true&u=0&authuser=0",
        "METHOD=POST",
        "HEADER=Access-Control-Request-Method:POST",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=Access-Control-Request-Headers:authorization,content-encoding,content-type,x-goog-authuser",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
    );

    ns_end_transaction("log_37", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_38");
    ns_web_url ("log_38",
        "URL=https://play.google.com/log?format=json&hasfast=true&u=0&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:948",
        "HEADER=Authorization:SAPISIDHASH e6cc4ac7c4584e2492b89bdeea8610d352eacb5b",
        "HEADER=Content-Encoding:gzip",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/binary",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/log_38_main_url_1_1619592544856.body",
        BODY_END
    );

    ns_end_transaction("log_38", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_39");
    ns_web_url ("log_39",
        "URL=https://play.google.com/log?format=json&hasfast=true&u=0&authuser=0",
        "METHOD=POST",
        INLINE_URLS,
            "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-encoding,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_39", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_40");
    ns_web_url ("log_40",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:625",
        "HEADER=Authorization:SAPISIDHASH e6cc4ac7c4584e2492b89bdeea8610d352eacb5b",
        "HEADER=Content-Encoding:gzip",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/binary",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "$CAVINCLUDE_NOPARAM$=http_request_body/log_40_main_url_1_1619592544862.body",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=l74oz6ro9i5w", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=ob0ko7jzi304", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=uj0ws62scj10", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&RID=rpc&SID=C43899E49E29E5D9&CI=0&AID=18&TYPE=xmlhttp&zx=yn4nhf9l2155&t=1", "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54", "HEADER=X-Origin:https://hangouts.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_40", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("s_3");
    ns_web_url ("s_3",
        "URL=https://mail.google.com/sync/u/0/i/s?hl=en&c=14",
        "METHOD=POST",
        "HEADER=Content-Length:77",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592185283\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"3":{"1":1,"2":"2285366","5":{"2":0},"7":1},"4":{"2":0,"4":0,"5":257},"5":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=fukek1dcwqft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("s_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_41");
    ns_web_url ("log_41",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1528",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],893,[["1619592190942",null,[],null,null,null,null,"[\"1619592185925\",[[null,null,[[\"668\",\"1\"]],\"6377561628952101238\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],7,null,null,null,null,null,[]]],"1619592200946",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://cello.client-channel.google.com/client-channel/channel/bind?authuser=0&ctype=cello&gsessionid=JSdZrBHiWiZZXlHB--Q00u3S_e-7h2a1q-4EIikmCjE&sw=true&VER=8&RID=rpc&SID=2B5847E73F3DE61&CI=0&AID=11&TYPE=xmlhttp&zx=cfr5vke4xajq&t=1", "HEADER=Authorization:SAPISIDHASH 1619591990_7bc2ab06d9c15da921d1835b243ae20d4a471eb6", "HEADER=X-Origin:https://drive.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE
    );

    ns_end_transaction("log_41", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_42");
    ns_web_url ("log_42",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1073",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592223110",null,[],null,null,null,null,"[[[null,null,229,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[3,true]]],1,null,[null,[1619592128372000,1619592128381000],2],null,106021,null,229,230]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],9,null,null,null,null,null,[]]],"1619592223110",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_42", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_43");
    ns_web_url ("log_43",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1684",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],407,[["1619592231152",null,[],null,null,null,null,"[23,1619592231151000,\"!xcalxovNAAZLnZBaS51CjQjs7xqqR1M7ACkAIwj8RoVFQUDFXuFkGSeWInRib4RukL3mtTgeg0IASfqkd0KAqK0dZQIAAAYcUgAAABpoAQeZA6iAoK_n1nb_06ORkvj-pv9qpPiB9Kn8-5f1WsztcWG-Y8jMX0ox0jqYFqbzB2EdnwWFNgQBneS6CGYSr-GCBx310bDe8-0I2ixfyf8mrYWxpBjOPEiBmLzm7COHK1HhpGTdXE7X6VXh9sON6FMErSjqXxttIj7w9iuC2KCSH3n6_9IznyLJ9F5ufB8SG_D-u_LLNPA6tYmzJ5dUAR-YzCxHiLN4lEnW2AfR6jOrAXvXUSXI_-RRCLz8ZXmnhXQQ1JIXBRanvwD93Ct22rN_0o8Ykdd6zSSGcR0zQXZ3f_M1l8QFIrNIszVEMya4hycn1vePDnmVhffmTbp-T-2EQjdcoAlj_RNtOkYmHl5I9YtLs9gFZObMXDXfFfgXNucOZEPsb1w86bKCLT8ULnhR6a1wVBR7qaMuiKRngvvjdsjNcSFhNEJnov2nM_o_RNAD032offr-aEF2xqjuhGjBa5wSy0NuGdjq5uBP9DDZLBFFTIBHKdhVgzyWRj21tweMscteX_ugTHy1KQTj56O4DypADC6tgUF5znID9X0qYsMzNOLlxbT1mrm2LCNoX5Z3w5WCIMb9VJA5fho142ilENMUZoVT01mKrRSmDSCxAcRDhErHrmv6r9z9gYDhXF5hvpS6V-t9dybhdF_76M0WIlDLTHQ6SsHdv8H31ma0im9ZlLNMflO3mgMnZNvFjIGx1eg0lr6Rh4qK__8Cn-YXuxQt1PiFRM6ZdUF1zEPBHH0_-wN716c7Nk9KLKAnNVrllHJPKd-VpTjemwUcggX9TCsioz9wV9u9MwClkYtN-XWv_kriQ8jAby4G16OtdeBvVq4heoETh3gBPY7YrhuQqTRcntAH_W-wNZlQQj6Sr-Rs_bzxFWbiUxQ5OuY4JaImZGda-uURwEr7Aq9ManE5ZvHDRNR_q6H5ibyrrms-YkMa03pv3880lSF4TmN_qf_A2J5rJDoc5C-78O4u7WKhWIkraqYQJUiMzH_Q0gDQimdc1Rl4d_UF1_b0dSNgtNQTrrV7PjpuQHoxttxB3hHLFO16QRkJF_N5Q2KR54jOo-wNlcCUrrp-Dn1oywRVmPXNL5k424EX7qwKJbp_j5XAVLUtrtN912t0axhjYs72_yeF060IZIr1UNMZ7ST4xwCicy78-xqSBt4R8YoaOFqZ4EnG3pffRD3kcGjIaveP9N9_L0LwVAi8_1LWL8qD7XcEROOndS8Ra-Y2Ya-XJBD4iIysl6aTh0kegq8\",33785112,false]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1619592231154",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=82xxvri64kue", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_43", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("s_4");
    ns_web_url ("s_4",
        "URL=https://mail.google.com/sync/u/0/i/s?hl=en&c=15",
        "METHOD=POST",
        "HEADER=Content-Length:77",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592234610\",\"21\":\"2285366\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"3":{"1":1,"2":"2285366","5":{"2":0},"7":1},"4":{"2":0,"4":0,"5":257},"5":2}",
        BODY_END,
        INLINE_URLS,
            "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyCIMH2ks6VPAfRC2lqU_Snz1Lo76XGdnlc", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://hangouts.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("s_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("fd_11");
    ns_web_url ("fd_11",
        "URL=https://mail.google.com/sync/u/0/i/fd?hl=en&c=16",
        "METHOD=POST",
        "HEADER=Content-Length:84",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592235192\",\"21\":\"2285421\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"1":[{"1":"thread-f:1698265546157138820","3":["msg-f:1698265546157138820"]}],"2":3}",
        BODY_END
    );

    ns_end_transaction("fd_11", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("acknowledgments_2");
    ns_web_url ("acknowledgments_2",
        "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyCIMH2ks6VPAfRC2lqU_Snz1Lo76XGdnlc",
        "METHOD=POST",
        "HEADER=Content-Length:242",
        "HEADER=Authorization:SAPISIDHASH 1619592234_1c8c56af4671c1b335923dcdbb30fe5986d25cc5",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[null,[["CpEBMKfC2fOAqZD3FzoLODg4MTM2NzYyODdKeAoWKhQv3IKQHOzCT48U86/KNvbHEed2DwoWKhROy70/gymBTbTgCGOsV+Lz/2Af1AoWKhR50R4k0szYg44O8aLvucTqUyRKJQoCWAEKCQiR4YCfq6DwAgoSKhAzN2RhZmI3N2Y2NmNiOTcyCgsImdbq6u7a7uS/ARFmhKHPreT7NBgL",1619592234541000]]]",
        BODY_END
    );

    ns_end_transaction("acknowledgments_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_44");
    ns_web_url ("log_44",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:750",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],241,[["1619592236248",null,[],null,null,null,null,"[1619592236240,[[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[169,1]]],[\"3079121564595244695\",null,[[161,1]]],[\"14113926844082461540\",null,[[26,1]]],[\"16057581369328409502\",null,[[0,1]]],[\"8359544496734816350\",null,[[0,1]]],[\"7355862829888568636\",null,[[0,1]]],[\"3328225046418674513\",null,[[4,1]]]],null,null,\"[2,\\\"WMove_MS\\\"]\"]",null,null,null,1557300944,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879],[]]"],null,null,null,[],2,null,null,null,null,null,[]]],"1619592236249",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_44", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_45");
    ns_web_url ("log_45",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:558",
        "HEADER=Authorization:SAPISIDHASH 092ef09ee5d3dc0bb0d87c6542f62443ca016575",
        "HEADER=Origin:https://contacts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_contactsuiserver_20210425.15_p0"]],241,[["1619592238199",null,[],null,null,null,null,"[1619592238186,[[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[169,1]]],[\"3079121564595244695\",null,[[162,1]]]],null,null,\"[2,\\\"lSWNlf_MS\\\"]\"]",null,null,null,1879607050,null,null,-19800,[null,[],null,"[[],[],[1757124,1763433,1772879],[]]"],null,null,null,[],2,null,null,null,null,null,[]]],"1619592238200",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_45", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_46");
    ns_web_url ("log_46",
        "URL=https://www.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1611",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],793,[["1619592236317",null,[],null,null,null,null,"[[null,null,null,370149053,null,25,\"gmail_fe_210421.06_p2\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,false,\"iv.os\",null,[false,false,0,1],[[[6,1],[2,1],[9,1],[4,1]],false]],null,null,null,null,null,null,[null,null,null,null,null,null,[],[],[],[],null,null,[null,null,[169306668,177366876,300,null,[]],null,0]],[1,1,false,false,6,null,0,[1,1,null,null]]]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],2,null,null,null,null,null,[]]],"1619592246320",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_46", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_47");
    ns_web_url ("log_47",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:3269",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],893,[["1619592239609",null,[],null,null,null,null,"[\"1619592234603\",[[null,null,[[\"1\",\"1\"]],\"8592769406416781108\"],[null,null,[[\"2\",\"1\"]],\"-7558660779622016800\"],[null,null,[[\"1\",\"1\"]],\"-7121550470961355275\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,[null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[\\\"ClientSyncStateForDebuggingManager.guardedUpdateState\\\",0]]],[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],8,null,null,null,null,null,[]],["1619592239610",null,[],null,null,null,null,"[\"1619592234603\",[[null,null,[[\"626\",\"1\"]],\"6377561628952101238\"],[null,null,[[\"50\",\"1\"]],\"-7442833728258606944\"],[null,null,[[\"50\",\"1\"]],\"-2516723497608421612\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],9,null,null,null,null,null,[]]],"1619592249621",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=dhohn29inko", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_47", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_48");
    ns_web_url ("log_48",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1694",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],407,[["1619592284811",null,[],null,null,null,null,"[49,1619592284810000,\"!CQqlCkfNAAZVgnmyVYJCv8t4ODJSF087ACkAIwj8RiasPrdAofW9K0pKz3VnaZ6bhDws0Mev3EJGjvsPp4bVgmtoywIAAAIjUgAAACJoAQeZA6-fbVfruaelxae9FVRwWikwb_o4FovQeMPhuu2YXQQPHn-T6Q2Z68Ts01IW63Pyj8zz_PzfrKFIBt5Ch8Tm_V9EhP0TBqAeM1pbsLxXn3r-pFGu-jumSztatRJgpaooNnJqVkF5eq8Invr_NrRSEDsnufLdqbJg7wHNTeqKkWG-0_LluoHfJPnKd8ghZfjjgkMLortLMAJoNM2Etwb0PnsUKXcD8e96-yrcmkPqXTUOwugLV_0NjgosNp6rKU8xGTpBbKR-g7nlO67S8HInz0lbxeAoLUEGupl6gd90q4aC3e3tiM8VGvZegjfqdLwZDe_Mgrw9PJ6i4wQ1g6nfGOe_wyrO-WKyyo4-jzJ-WDoUfOLY5yV17vAd1EnmG5TjAxgTwbOcLaLK4DMyQDREAOW8eTP1xdYR7HuK8FpNfWcUZE9ntZrdQpL7UsGRA7GamGPFY_3d9_KzYiemo5ns4qCy1ejqTaX6xnGuVhLlygU6iGLZlJb7QzLM12wlL1-cKMFUQoIhL-X35kJqty6vCy5QuojGMRDV1qujk1anUFa4d_rbu22TwcsuC6nhob8tpwbrawgCsJ6axTKxBKV38Lgnq1m7iW2rJWM6mK0vhOSq6UT5YTFZq_Z1sLPncCLrCpEcc1N4yCdmZV-f4Fb5FmwpuvYQJbDGJznp-Lh0yvKqCzYLoSj65RTnQHT3Mh__nEqO9ndSgWGlH-X-jq3JX3CXbTsA5rG2y-9oEU-wc6hJt2Y9Dcsx3l7-BZmV8DxA4s7uRBdE5_NVk4-vOSfgw9sQhxdBFFBjtsqM_jDW56G14svxn9ltDRxZI1lmnzKTnpPqMimowUANOlItskXX8n1Q4p0xf5_KuZbb7rKYmHQ7TWj0wboMDcyAYGqpE5fAJ1p1SC7gCr285g1TWynkipfKjeN7Iwp-IIWiJxuQb7HntuEkM8nEbxAmnd-XI086EmYiaRvS79Gb4rDUbfIkK_c_5vrPH-bC0JOZyq8Zb810VNVo1Ei7JcFJY6I9p3VuFBMTcghvQF2Xp8gtlmH9ZS8vkL00d4ExWM4zG3-RBHSfWPWtBYEsE-r6_9tlWpfy6BptF-ybcpj9nwJBIVjsymSViG60bM79XWLYWNyqHomBbiJhgRyCx4CBr_2gLz6muACgVDSHTwk_q0hfhvbhMScj5eoKZgRDmqLz0wtAvYGOP6Xjku2Bzr0GWYSl8k9iJIPsQDnO5tIhzWvBD4PqlUlx2syqCofN74OPoKPptYrs\",1601285751,true]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1619592284814",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_48", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_49");
    ns_web_url ("log_49",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:592",
        "HEADER=Authorization:SAPISIDHASH c8daafd5c24a1a5c3afe252a42bcf02e1856f8de",
        "HEADER=Origin:https://ogs.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en",null,"boq_onegooglehttpserver_20210426.03_p0"]],241,[["1619592290681",null,[],null,null,null,null,"[1619592290670,[[\"4950535922500196698\",null,[[4144,1]]],[\"1757184925777806825\",null,[[168,1]]],[\"3079121564595244695\",null,[[163,1]]],[\"14113926844082461540\",null,[[17,1]]]],null,null,\"[2,\\\"WMove_MS\\\"]\"]",null,null,null,648233228,null,null,-19800,[null,[],null,"[[],[],[1763433,1772879],[]]"],null,null,null,[],2,null,null,null,null,null,[]]],"1619592290682",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=p15crp6mxjcr", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_49", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_50");
    ns_web_url ("log_50",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1074",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592343130",null,[],null,null,null,null,"[[[null,null,231,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3088,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[3,true]]],1,null,[null,[1619592278386000,1619592278390000],2],null,106021,null,231,232]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],10,null,null,null,null,null,[]]],"1619592343130",[],null,null,null,null,null,null,null,null,0]",
        BODY_END,
        INLINE_URLS,
            "URL=https://ssl.gstatic.com/ui/v1/icons/mail/images/cleardot.gif?zx=ugvqi5bj5vuh", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("log_50", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("s_5");
    ns_web_url ("s_5",
        "URL=https://mail.google.com/sync/u/0/i/s?hl=en&c=17",
        "METHOD=POST",
        "HEADER=Content-Length:77",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Google-BTD:1",
        "HEADER=X-Framework-Xsrf-Token:AKwhgQoAeRB1R--DnxSE2rzi_fHyJZuGDQ:1619590590512",
        "HEADER=X-Gmail-BTAI:{\"3\":{\"6\":0,\"10\":1,\"13\":1,\"15\":0,\"16\":1,\"17\":1,\"18\":0,\"19\":1,\"22\":1,\"23\":1,\"24\":1,\"25\":1,\"26\":1,\"27\":1,\"28\":1,\"29\":0,\"30\":1,\"31\":1,\"32\":1,\"33\":1,\"34\":1,\"35\":0,\"36\":1,\"37\":\"en\",\"38\":\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36\",\"39\":1,\"40\":0,\"41\":25,\"43\":0,\"44\":1,\"45\":0,\"46\":1,\"47\":1,\"48\":1,\"49\":1,\"50\":1,\"52\":1,\"53\":1,\"54\":0,\"55\":1,\"56\":1,\"57\":0,\"58\":0,\"60\":0,\"61\":0,\"62\":0,\"63\":0,\"64\":0},\"5\":\"3ea5edf7c4\",\"7\":25,\"8\":\"gmail_fe_210421.06_p2\",\"9\":1,\"10\":5,\"11\":\"\",\"12\":19800000,\"13\":\"+05:30\",\"14\":1,\"16\":370149053,\"17\":\"\",\"18\":\"\",\"19\":\"1619592355277\",\"21\":\"2285421\"}",
        "HEADER=Content-Type:application/json",
        "HEADER=Sec-Fetch-Site:same-origin",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=COMPASS;SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;OSID;__Secure-OSID;1P_JAR;SEARCH_SAMESITE;NID;__Host-GMAIL_SCH_GMN;__Host-GMAIL_SCH_GMS;__Host-GMAIL_SCH_GML;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "{"3":{"1":1,"2":"2285421","5":{"2":0},"7":1},"4":{"2":0,"4":0,"5":258},"5":2}",
        BODY_END
    );

    ns_end_transaction("s_5", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("acknowledgments_3");
    ns_web_url ("acknowledgments_3",
        "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyCIMH2ks6VPAfRC2lqU_Snz1Lo76XGdnlc",
        "METHOD=POST",
        "HEADER=Content-Length:242",
        "HEADER=Authorization:SAPISIDHASH 1619592370_d4629a71dd2b5b59ad6c34a1ca7d9538fb9d122b",
        "HEADER=Origin:https://hangouts.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[null,[["CpEBMIDY/7Pd/NPODToLODg4MTM2NzYyODdKeAoWKhQbOp/LrP/YAIay8gZyTCnbtYSkwAoWKhQTafIKN1ZoKTmvp8eQnLRFDxibVwoWKhTdaIq9mVmrl/b3AmBHeAJcGXFviwoCWAEKCQiI5uLfq6DwAgoSKhA2NmYwYjA1ZTJhMWE3YmM4CgsI2sr97IfO2tThARH58zPEHesO3hgL",1619592370337000]]]",
        BODY_END
    );

    ns_end_transaction("acknowledgments_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_51");
    ns_web_url ("log_51",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1529",
        "HEADER=Authorization:SAPISIDHASH 9d2231cb21a5d87db5e2378f9c5f35c29a338813",
        "HEADER=Origin:https://mail.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],893,[["1619592360802",null,[],null,null,null,null,"[\"1619592355792\",[[null,null,[[\"537\",\"1\"]],\"6377561628952101238\"]],null,null,\"[[null,null,null,370149053,null,25,\\\"gmail_fe_210421.06_p2\\\",7,null,null,null,null,null,null,null,[6,2,9,4],8192,0,\\\"iv.os\\\"],null,null,null,null,null,null,null,[1,1,0,0,6,null,0,[1,1]]]\"]",null,null,null,null,null,null,-19800,[null,[],null,"[[],[],[7113,7766,7518,7433,7465,7594,7677,7514,7782,7753,7424,7057,7416,7758,7628,7682,7030,7546,7496,7158,6623,7137,5192,6456,6318,7798,7418,7407,5020,7468,7446,7164,7189,6807,5001,6162,6587,7500,5499,6455,8202316,8203183,8203161,8203028,8203691,8202193,8202009,8202484,8201523,8202805,8203287,8203359,8203555,8202610,8203883,8203699,8203378,8202855,8203222,8203363,8202578,8203587,8203298,8203550,8203570,8202965,8202124,45717508,8202813,8202257,8202891,8203044,8203388,8203582,8203464,8203424,8203244,8202563,8203312,8203310,8203295,8203311,8203291,8203609,8202123,8202721,8203455,45779046,8202126,8203514,8203270,8203664,8202463,45775010,8203292,8203421,45772160,8202709,8203103,8202737,8203643,8202918,8200010,45751720,8202511,8203695,8203075,8203178,8203297,45769960,8203347,8203757,8203451,8202753,8203586,8203381,8202924,8203596,8203427,8203385,8203239,8203543,8202125,8202953,8203054,8203611,45772005,8203624,8202323,8202843,45704154],[]]"],null,null,null,[],10,null,null,null,null,null,[]]],"1619592370806",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_51", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_52");
    ns_web_url ("log_52",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:460",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],78,[["1619592386991",null,[],null,null,null,null,"[null,null,null,null,null,null,null,null,null,null,null,[[null,1,null,null,null,null,49,1723230717]],null,null,[1366,768],null,null,null,null,null,null,null,null,[],null,null,[]]",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]]],"1619592386991",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_52", NS_AUTO_STATUS);
    ns_page_think_time(17.19);

    //Page Auto splitted for 
    ns_start_transaction("cleardot_gif_2");
    ns_web_url ("cleardot_gif_2",
        "URL=https://ssl.gstatic.com/docs/common/cleardot.gif?zx=fnnyuix1opmt",
        "HEADER=Sec-Fetch-Site:cross-site",
        "HEADER=Sec-Fetch-Mode:no-cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=https://0.client-channel.google.com/client-channel/channel/bind?ctype=hangouts&prop=gmail&appver=babel-chat.frontend_20210425.07_p0&gsessionid=5XRL_CVWERs3Iaon28J11LUbDxDW1wG2yevT4lSq81k&VER=8&RID=rpc&SID=C43899E49E29E5D9&CI=0&AID=28&TYPE=xmlhttp&zx=lsdpfzehj6uc&t=1", "HEADER=Authorization:SAPISIDHASH 1619591934_b035b7581508dcf9fd29e426b44eaaa616c6ed54", "HEADER=X-Origin:https://hangouts.google.com", "HEADER=X-Goog-AuthUser:0", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://clients6.google.com/upload/drive/v2internal/files?uploadType=multipart&supportsTeamDrives=true&pinned=true&convert=false&fields=kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed)&openDrive=false&reason=202&syncType=0&errorRecovery=false&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:POST", "HEADER=Origin:https://drive.google.com", "HEADER=Access-Control-Request-Headers:authorization,content-type,x-goog-authuser", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("cleardot_gif_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("files_2");
    ns_web_url ("files_2",
        "URL=https://clients6.google.com/upload/drive/v2internal/files?uploadType=multipart&supportsTeamDrives=true&pinned=true&convert=false&fields=kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CdisplayName%2Cpicture%2CemailAddress%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed)&openDrive=false&reason=202&syncType=0&errorRecovery=false&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:508146",
        "HEADER=authorization:SAPISIDHASH 1619592404_25d65fb72bf5c8aeac7860812433252a5102054f_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=content-type:multipart/related; boundary=\"7jm1l2jpi1y\"",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        MULTIPART_BODY_BEGIN,
            MULTIPART_BOUNDARY,
            //--7jm1l2jpi1y
                "HEADER=content-type: application/json; charset=UTF-8",
                BODY_BEGIN,
                    "{"title":"woman-6173215_1920.jpg","mimeType":"image/jpeg","parents":[{"id":"0AJWo5b32VY-AUk9PVA"}],"modifiedDate":"2021-04-20T07:48:18Z"}",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--7jm1l2jpi1y
                "HEADER=content-transfer-encoding: base64",
                "HEADER=content-type: image/jpeg",
                BODY_BEGIN,
                    "$CAVINCLUDE$=http_request_body/files_2_url_0_1_1619592544956.body",
                BODY_END,
        MULTIPART_BODY_END
    );

    ns_end_transaction("files_2", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_53");
    ns_web_url ("log_53",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:1957",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592407818",null,[],null,null,null,null,"[[[null,null,233,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[7,1,null,1,true,true,false,true,false],[3088,null,null,null,null,true]]],1,null,[null,[1619592404309000,1619592404456000],2],null,106001,null,233,235],[null,null,234,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[10,9,null,1,false,true,false,null],[3088,null,null,null,null,true]]],1,null,[null,[1619592404406000,1619592404460000],2],null,106001,null,234,236],[null,null,237,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3089,null,null,null,null,true],null,null,null,null,null,[1,true,null,false,false,380884,201326592,0]]],1,null,[null,[1619592404475000,1619592407818000],2],null,106012,null,237,238]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],11,null,null,null,null,null,[]]],"1619592407818",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_53", NS_AUTO_STATUS);

    //Page Auto splitted for application/json type
    ns_start_transaction("about_3");
    ns_web_url ("about_3",
        "URL=https://clients6.google.com/drive/v2internal/about?openDrive=false&reason=304&syncType=0&errorRecovery=false&fields=kind%2Cuser(kind%2Cid%2CpermissionId%2CemailAddressFromAccount%2Cdomain)%2CquotaBytesTotal%2CquotaBytesUsed%2CquotaBytesUsedAggregate%2CquotaBytesUsedInTrash%2CquotaBytesByService%2CquotaType%2CrootFolderId%2CdomainSharingPolicy%2ClargestChangeId%2CimportFormats%2CgsuiteSubscriptionInfo(trialMillisRemaining%2CtrialEndTime%2Cstatus)%2CteamDashboardCapabilities(canAdministerTeam%2CcanManageInvites)%2CgraceQuotaInfo%2CquotaStatus&retryCount=0&dsNonce=6qlc7xs5a5ut&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "HEADER=authorization:SAPISIDHASH 1619592407_22aabed47f17fc619ceb8de05760c4c736a20139_u",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=x-goog-authuser:0",
        "HEADER=X-Goog-Drive-Client-Version:drive.web-frontend_20210414.00_p2",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        INLINE_URLS,
            "URL=https://lh3.google.com/u/0/d/1Ibr9MzO8TTTVgTOmDADoQidQebMBy4R7=w32-h32-p-k-nu-iv1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC", END_INLINE,
            "URL=https://lh3.googleusercontent.com/fife/ABSRlIoyjUqNru-WcSw74t3aedmiDZq5PitloIx5rlIxaLKB5GJ8ICE7qfU_CJLYQ87bfEouGpNfB2NzwFQgBKz9zB9jjV27m6tRbgGDhmecHx3GX4AII6QEDtiieaET_j4Z914BZGsZZFvQYmU9cJ44sZvBItk1RP4GElM171M905yxg5yGw1L4zyDTBfhpba0lJX_oe1Zv-B6GkrTho8EwnHlmHGoPfNQeZH_fefE96Q7P3JIDQp3QoTP3eVp_0gXvyC4tSjOqRdXCsg-2Xt-o7FNxHnbYZkqmsfmrDV7MMwOZ_FI3Gfo-u867l8HN6B0DUpj6p0_S5L65SDHBURMg4WZ8XMvXM-WOpuhabF3LGNCfRxi3yxTNSA1Vs1Ac30W6y91YfiAJvvYuTNYasMXNAcpUtEBgHwCL04wjsIG3qYZBWohN8JR39f4APreXPQaJ2kus5IPVnNnxv2PaooG4aHYnJcvczvOJ20Vlli3t0OB9dxbp_IIgg6cHqs88R0vtKqOFoZ_cNcBM_UmjsganymKEIy_S8j8r-kO7pZmzyDR8WezV51OdI0oy9XnMGEruY8p-zwBcDZFVAoZhWDCGRpPqTKcplVw2Xd7c0P3_aEMEwE4OYBGhrGIpXO_4FaynaLkGG1XiVl4Bed-Wvc5unbKla6Y941xknxMOUFv1Cqy_znDeMUC3Swq-1a4OvEKoQESipES4dRKgiwq67yDyWbPCEj8N7MhxTCQ=w32-h32-p-k-nu-ft", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("about_3", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("acknowledgments_4");
    ns_web_url ("acknowledgments_4",
        "URL=https://signaler-pa.clients6.google.com/v1/acknowledgments?key=AIzaSyAWGrfCCr7albM3lmCc937gx4uIphbpeKQ",
        "METHOD=POST",
        "HEADER=Content-Length:250",
        "HEADER=Authorization:SAPISIDHASH 1619592408_3ffde360f835f214820910bcd7411410544beb0d",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-type:application/json+protobuf",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[null,[["CpUBMM/JnfyR6N6tEzoMOTQ3MzE4OTg5ODAzSnsKFioUJrb2gj8OyZ/f6lnJm04FCZBAMFkKFioUdkBWgNVITEKWRVj/it8hevmgXosKFioUiZoClK3Jtw/WvOxvHNQQ8JDEhysKAlgBCgkI0OD28aug8AIKFSoTNjg1OTM2MTM1OTEwNzg5NjM3NwoLCMbpjpCZ3JSgtwERvpgXIBttRk8YCw==",1619592408562000]]]",
        BODY_END
    );

    ns_end_transaction("acknowledgments_4", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("v2internal_10");
    ns_web_url ("v2internal_10",
        "URL=https://clients6.google.com/batch/drive/v2internal?%24ct=multipart%2Fmixed%3B%20boundary%3D%22%3D%3D%3D%3D%3D763xj4j3q3vb%3D%3D%3D%3D%3D%22&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4",
        "METHOD=POST",
        "HEADER=Content-Length:3238",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=content-type:text/plain; charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "--=====763xj4j3q3vb=====
content-type: application/http
content-transfer-encoding: binary

GET /drive/v2internal/changes?openDrive=false&reason=303&syncType=0&errorRecovery=false&includeDeleted=true&includeSubscribed=true&maxResults=1000&startChangeId=62519&fields=kind%2ClargestChangeId%2CnextPageToken%2Citems(deleted%2CfileId%2Cid%2Ckind%2Cfile(kind%2CmodifiedDate%2ChasVisitorPermissions%2CcontainsUnsubscribedChildren%2CmodifiedByMeDate%2ClastViewedByMeDate%2CfileSize%2Cowners(kind%2CpermissionId%2CemailAddressFromAccount%2Cdomain%2Cid)%2ClastModifyingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CcustomerId%2CancestorHasAugmentedPermissions%2ChasThumbnail%2CthumbnailVersion%2Ctitle%2Cid%2CresourceKey%2Cshared%2CsharedWithMeDate%2CuserPermission(role)%2CexplicitlyTrashed%2CmimeType%2CquotaBytesUsed%2Ccopyable%2Csubscribed%2CfolderColor%2ChasChildFolders%2CfileExtension%2CprimarySyncParentId%2CsharingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CflaggedForAbuse%2CfolderFeatures%2Cspaces%2CsourceAppId%2Crecency%2CrecencyReason%2Cversion%2CactionItems%2CteamDriveId%2ChasAugmentedPermissions%2CcreatedDate%2CprimaryDomainName%2CorganizationDisplayName%2CpassivelySubscribed%2CtrashingUser(kind%2CpermissionId%2CemailAddressFromAccount%2Cid)%2CtrashedDate%2Cparents(id)%2Ccapabilities(canMoveItemIntoTeamDrive%2CcanUntrash%2CcanMoveItemWithinTeamDrive%2CcanMoveItemOutOfTeamDrive%2CcanDeleteChildren%2CcanTrashChildren%2CcanAddMyDriveParent%2CcanRemoveMyDriveParent%2CcanShareChildFiles%2CcanShareChildFolders%2CcanRead%2CcanMoveItemWithinDrive%2CcanMoveChildrenWithinDrive%2CcanAddFolderFromAnotherDrive%2CcanBlockOwner%2CcanCopy%2CcanDownload%2CcanEdit%2CcanAddChildren%2CcanDelete%2CcanRemoveChildren%2CcanShare%2CcanTrash%2CcanRename%2CcanReadTeamDrive%2CcanMoveTeamDriveItem)%2CcontentRestrictions(readOnly)%2CshortcutDetails(targetId%2CtargetMimeType%2CtargetLookupStatus%2CtargetFile%2CcanRequestAccessToTarget)%2Clabels(starred%2Ctrashed%2Crestricted%2Cviewed))%2CteamDriveId%2CteamDrive(kind%2Cid%2Cname%2CcolorRgb%2CbackgroundImageLink%2Ccreator(kind%2CpermissionId%2Cid%2CemailAddressFromAccount)%2CpermissionsSummary(entryCount%2CuserEntryCount%2CgroupEntryCount%2CmemberCount%2CselectPermissions(kind%2Cid%2CuserId%2CemailAddress%2Cdomain%2Crole%2Ctype%2CadditionalRoles%2Cview%2CaudienceId%2CcustomerId))%2Ctrusted%2CprimaryDomainName%2CorganizationDisplayName%2Crestrictions%2CbackgroundImageGridViewLink%2CbackgroundImageListViewLink%2Chidden%2Ccapabilities(canAddChildren%2CcanAddFolderFromAnotherDrive%2CcanChangeTeamDriveBackground%2CcanComment%2CcanCopy%2CcanDeleteTeamDrive%2CcanDownload%2CcanEdit%2CcanManageMembers%2CcanManageVisitors%2CcanRemoveChildren%2CcanRename%2CcanRenameTeamDrive%2CcanShareOutsideDomain%2CcanDeleteChildren%2CcanTrashChildren)))&appDataFilter=NO_APP_DATA&filters=DRIVE_DATASERVICE&spaces=drive%2Cphotos&supportsTeamDrives=true&includeCorpusRemovals=true&includeItemsFromAllDrives=true&retryCount=0&key=AIzaSyAy9VVXHSpS2IJpptzYtGbLP3-3_l0aBk4 HTTP/1.1
X-Goog-Drive-Client-Version: drive.web-frontend_20210414.00_p2
authorization: SAPISIDHASH 1619592409_af1a707a6931e1918944baec1e198b8aba28d5ee_u
x-goog-authuser: 0

--=====763xj4j3q3vb=====--",
        BODY_END
    );

    ns_end_transaction("v2internal_10", NS_AUTO_STATUS);

    //Page Auto splitted for Method = POST
    ns_start_transaction("log_54");
    ns_web_url ("log_54",
        "URL=https://play.google.com/log?format=json&hasfast=true&authuser=0",
        "METHOD=POST",
        "HEADER=Content-Length:3461",
        "HEADER=Authorization:SAPISIDHASH 86b6a7c010deb628742cc33cc6af93ab06c4ec9a",
        "HEADER=Origin:https://drive.google.com",
        "HEADER=X-Goog-AuthUser:0",
        "HEADER=Content-Type:application/x-www-form-urlencoded;charset=UTF-8",
        "HEADER=Sec-Fetch-Site:same-site",
        "HEADER=Sec-Fetch-Mode:cors",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "COOKIE=SID;__Secure-3PSID;HSID;SSID;APISID;SAPISID;__Secure-3PAPISID;1P_JAR;SEARCH_SAMESITE;NID;SIDCC;__Secure-3PSIDCC",
        BODY_BEGIN,
            "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,"en"]],906,[["1619592410821",null,[],null,null,null,null,"[[[null,null,241,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[8,6,null,1,null,true,false,null],[3089,null,null,null,null,true]]],1,null,[null,[1619592408391000,1619592408395000],2],null,106001,null,241,242],[null,null,239,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[[8,6,null,1,null,false,false,null],[3089,null,null,null,null,true]]],1,null,[null,[1619592407830000,1619592408463000],2],null,106001,null,239,243],[null,null,244,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3089,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,1,0,null,\"NaN\",true]]],1,null,[null,[1619592408543000,1619592408692000],2],[null,240,null,null,106021],106023,null,244,245],[null,null,246,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3089,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,[true,null,1,0,0,null,true]]],1,null,[null,[1619592408693000,1619592408695000],2],[null,240,null,null,106021],106019,null,246,247],[null,null,240,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3089,null,null,null,null,true],null,null,null,null,null,null,null,null,null,null,null,null,null,null,[1,true]]],1,null,[null,[1619592408388000,1619592408749000],2],null,106021,null,240,248],[null,null,249,null,[null,null,null,null,null,null,null,null,null,null,null,[1],null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,[null,[3089,null,null,null,null,true],null,[1,true,true,false]]],1,null,[null,[1619592409568000,1619592410019000],2],null,106003,null,249,250]],[\"1619591983058_5ssdtc5uokvs\",1619591983058000,null,null,null,109],[null,null,null,null,null,null,null,null,null,[[12,24,9,21,2,6,14,23,28,36,25,26,27,29]]],[6],[[null,null,null,null,null,null,null,null,null,null,null,null,[1,1,1,3,false,false,false]],[\"drive.web-frontend_20210414.00_p2\"]]]",null,null,null,null,null,null,-19800,null,null,null,null,[],12,null,null,null,null,null,[]]],"1619592410821",[],null,null,null,null,null,null,null,null,0]",
        BODY_END
    );

    ns_end_transaction("log_54", NS_AUTO_STATUS);
    ns_page_think_time(3.004);

}
