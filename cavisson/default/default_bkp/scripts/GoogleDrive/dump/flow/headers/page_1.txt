--Request 
GET https://mail.google.com/
Host: mail.google.com
Host: mail.google.com
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 301
status: 301
location: /mail/
expires: Wed, 28 Apr 2021 06:30:18 GMT
date: Wed, 28 Apr 2021 06:30:18 GMT
cache-control: private, max-age=7776000
content-type: text/html; charset=UTF-8
content-encoding: gzip
x-content-type-options: nosniff
x-frame-options: SAMEORIGIN
content-security-policy: frame-ancestors 'self'
x-xss-protection: 1; mode=block
content-length: 156
server: GSE
alt-svc: clear
----
--Request 
GET https://mail.google.com/mail/
Host: mail.google.com
Host: mail.google.com
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 302
status: 302
content-type: text/html; charset=UTF-8
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: Mon, 01 Jan 1990 00:00:00 GMT
date: Wed, 28 Apr 2021 06:30:18 GMT
location: https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1#
content-encoding: gzip
x-content-type-options: nosniff
x-frame-options: SAMEORIGIN
content-security-policy: frame-ancestors 'self'
x-xss-protection: 1; mode=block
content-length: 264
server: GSE
alt-svc: clear
----
--Request 
GET https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1#
Host: accounts.google.com
Host: accounts.google.com
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Sec-Fetch-User: ?1
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: none
Sec-Fetch-Mode: navigate
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
content-type: text/html; charset=utf-8
x-frame-options: DENY
x-auto-login: realm=com.google&args=service%3Dmail%26continue%3Dhttps%253A%252F%252Fmail.google.com%252Fmail%252F
link: <https://www.google.com/gmail/>; rel=\"canonical\"
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: Mon, 01 Jan 1990 00:00:00 GMT
date: Wed, 28 Apr 2021 06:30:18 GMT
content-encoding: gzip
strict-transport-security: max-age=31536000; includeSubDomains
content-security-policy: script-src 'report-sample' 'nonce-l0JBPvahj/BniIG6FOEfcg' 'unsafe-inline' 'unsafe-eval';object-src 'none';base-uri 'self';report-uri /cspreport
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
server: GSE
set-cookie: __Host-GAPS=1:NTOT00EPzc_0ODDqudK2oaLfdlPjSQ:sA7IPmv7DYw9lRVA;Path=/;Expires=Fri, 28-Apr-2023 06:30:18 GMT;Secure;HttpOnly;Priority=HIGH
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/googlesans/v14/4UaGrENHsxJlGDuGo1OIlL3Owp4.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 21464
date: Thu, 22 Apr 2021 02:03:02 GMT
expires: Fri, 22 Apr 2022 02:03:02 GMT
last-modified: Mon, 22 Apr 2019 23:42:59 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534437
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 15344
date: Thu, 22 Apr 2021 02:03:02 GMT
expires: Fri, 22 Apr 2022 02:03:02 GMT
last-modified: Mon, 16 Oct 2017 17:32:55 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534437
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 15552
date: Thu, 22 Apr 2021 02:03:02 GMT
expires: Fri, 22 Apr 2022 02:03:02 GMT
last-modified: Mon, 16 Oct 2017 17:33:02 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534437
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/googlesans/v14/4UabrENHsxJlGDuGo1OIlLU94YtzCwY.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 21700
date: Thu, 22 Apr 2021 02:03:02 GMT
expires: Fri, 22 Apr 2022 02:03:02 GMT
last-modified: Mon, 22 Apr 2019 23:43:33 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534437
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7GxKOzY.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 11936
date: Wed, 21 Apr 2021 17:01:02 GMT
expires: Thu, 21 Apr 2022 17:01:02 GMT
last-modified: Mon, 16 Oct 2017 17:33:01 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 566957
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4WxKOzY.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 7276
date: Thu, 22 Apr 2021 02:03:02 GMT
expires: Fri, 22 Apr 2022 02:03:02 GMT
last-modified: Mon, 16 Oct 2017 17:32:54 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534437
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu5mxKOzY.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 9832
date: Thu, 22 Apr 2021 02:03:03 GMT
expires: Fri, 22 Apr 2022 02:03:03 GMT
last-modified: Mon, 16 Oct 2017 17:32:49 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534436
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://accounts.youtube.com/accounts/CheckConnection?pmpo=https%3A%2F%2Faccounts.google.com&v=-1951684229&timestamp=1619591418392
Host: accounts.youtube.com
Host: accounts.youtube.com
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: nested-navigate
Referer: https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
content-type: text/html; charset=utf-8
x-frame-options: ALLOW-FROM https://accounts.google.com
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: Mon, 01 Jan 1990 00:00:00 GMT
date: Wed, 28 Apr 2021 06:30:19 GMT
cross-origin-resource-policy: cross-origin
content-security-policy: script-src 'report-sample' 'nonce-8ZuHRAZtLG/8r5k6kCeiUA' 'unsafe-inline';object-src 'none';base-uri 'self';report-uri /_/AccountsDomainCookiesCheckConnectionHttp/cspreport;worker-src 'self'
content-security-policy: script-src 'nonce-8ZuHRAZtLG/8r5k6kCeiUA' 'self' https://apis.google.com https://ssl.gstatic.com https://www.google.com https://www.gstatic.com https://www.google-analytics.com;report-uri /_/AccountsDomainCookiesCheckConnectionHttp/cspreport;frame-ancestors https://accounts.google.com
cross-origin-opener-policy-report-only: same-origin; report-to=\"AccountsDomainCookiesCheckConnectionHttp\"
report-to: {\"group\":\"AccountsDomainCookiesCheckConnectionHttp\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/AccountsDomainCookiesCheckConnectionHttp/external\"}]}
content-encoding: gzip
server: ESF
x-xss-protection: 0
x-content-type-options: nosniff
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=sy2h,i5dxUd,m9oV,RAnnUd,sy2d,sy2e,sy2f,uu7UOe,soHxf
Host: ssl.gstatic.com
Host: ssl.gstatic.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://accounts.google.com/ServiceLogin?service=mail&passive=true&rm=false&continue=https://mail.google.com/mail/&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
vary: Accept-Encoding, Origin
content-encoding: gzip
content-type: text/javascript; charset=UTF-8
cross-origin-resource-policy: cross-origin
content-length: 6735
date: Tue, 27 Apr 2021 01:24:16 GMT
expires: Wed, 27 Apr 2022 01:24:16 GMT
last-modified: Sat, 17 Apr 2021 02:34:15 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
cache-control: public, max-age=31536000
age: 104763
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----


--Request 
GET https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu7WxKOzY.woff2
Host: fonts.gstatic.com
Host: fonts.gstatic.com
Connection: keep-alive
Origin: https://accounts.google.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://accounts.google.com/
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
content-type: font/woff2
access-control-allow-origin: *
timing-allow-origin: *
content-length: 5224
date: Thu, 22 Apr 2021 02:03:03 GMT
expires: Fri, 22 Apr 2022 02:03:03 GMT
last-modified: Mon, 16 Oct 2017 17:32:55 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
age: 534436
cache-control: public, max-age=31536000
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://accounts.google.com/_/bscframe
Host: accounts.google.com
Host: accounts.google.com
Connection: keep-alive
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: nested-navigate
Referer: https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: __Host-GAPS=1:NTOT00EPzc_0ODDqudK2oaLfdlPjSQ:sA7IPmv7DYw9lRVA
----
--Response 
HTTP/1.1 200
status: 200
content-type: text/html; charset=utf-8
vary: Sec-Fetch-Dest, Sec-Fetch-Mode, Sec-Fetch-Site
x-frame-options: SAMEORIGIN
cache-control: no-cache, no-store, max-age=0, must-revalidate
pragma: no-cache
expires: Mon, 01 Jan 1990 00:00:00 GMT
date: Wed, 28 Apr 2021 06:30:20 GMT
content-security-policy: script-src 'unsafe-eval';require-trusted-types-for 'script';object-src 'none'
p3p: CP=\"This is not a P3P policy! See g.co/p3phelp for more info.\"
strict-transport-security: max-age=31536000
cross-origin-opener-policy-report-only: same-origin; report-to=\"AccountsSignInSignUpUi\"
cross-origin-resource-policy: same-site
report-to: {\"group\":\"AccountsSignInSignUpUi\",\"max_age\":2592000,\"endpoints\":[{\"url\":\"https://csp.withgoogle.com/csp/report-to/AccountsSignInSignUpUi/external\"}]}
content-encoding: gzip
server: ESF
x-xss-protection: 0
x-content-type-options: nosniff
set-cookie: NID=214=OWfUjs7If-3roI7DRoR8JvWNMfuQIh_7B7OO2Y9EsGs6WpuhGyFbkXWUakd9o_T9UNSr2-HRWmKA6U7AYomcqQtbJn2C8hYuuPHIM2eoIpds3-uSswlUhjPPQcCsSHzswS8DKptKQ3KfKzmMK8O0jgWxEBKwtwASXBAgeUuYn2A; expires=Thu, 28-Oct-2021 06:30:20 GMT; path=/; domain=.google.com; Secure; HttpOnly; SameSite=none
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----
--Request 
GET https://ssl.gstatic.com/accounts/static/_/js/k=gaia.gaiafe_glif.en_GB.mhSbGivJrrg.O/am=A4O4YYMCNAAIQAAAAAAAAgCAESRAIfU6CP8/d=0/ct=zgms/rs=ABkqax0mWxuzRef7O-yle0zdfyWeSNMiFA/m=NpD4ec,SF3gsd,O8k1Cd,YLQSd,lCVo3d,o02Jie,rHjpXd,pB6Zqd,QLpTOd,otPmVb,rlNAl
Host: ssl.gstatic.com
Host: ssl.gstatic.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://accounts.google.com/signin/v2/identifier?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
 POSTDATA [key: "[[1,null,null,null,null,null,null,null,null,null,[null,null,null,null,\"en-GB\"]],558,[[\"1619591418000\",null,[],null,null,null,null,\"[[null,[1,\\\"accounts.google.com/ServiceLogin\\\",null,[\\\"\\\"],false]],null,3,[null,\\\"S-2063066544:1619591418653949\\\"]]\",null,null,null,null,null,null,-19800,null,null,null,null,[],1,null,null,null,null,null,[]],[\"1619591418000\",null,[],null,null,null,null,\"[[[223,42108,\\\"invalid-device-id\\\",null,null,null,null,null,null,null,null,null,\\\"accounts.google.com/ServiceLogin\\\"]],null,3,[null,\\\"S-2063066544:1619591418653949\\\"]]\",null,null,null,null,null,null,-19800,null,null,null,null,[],2,null,null,null,null,null,[]],[\"1619591418000\",null,[],null,null,null,null,\"[[[224,42076,\\\"invalid-device-id\\\",null,null,null,null,null,null,null,null,null,\\\"accounts.google.com/ServiceLogin\\\"]],null,3,[null,\\\"S-2063066544:1619591418653949\\\"]]\",null,null,null,null,null,null,-19800,null,null,null,null,[],3,null,null,null,null,null,[]],[\"1619591418000\",null,[],null,null,null,null,\"[[null,[1,\\\"accounts.google.com/signin/v2/identifier?service"
value: "mail"
, key: "passive"
value: "true"
, key: "rm"
value: "false"
, key: "continue"
value: "https://mail.google.com/mail/"
, key: "ss"
value: "1"
, key: "scc"
value: "1"
, key: "ltmpl"
value: "default"
, key: "ltmplcache"
value: "2"
, key: "emr"
value: "1"
, key: "osid"
value: "1"
, key: "flowName"
value: "GlifWebSignIn"
, key: "flowEntry"
value: "ServiceLogin\\\",null,[\\\"\\\"],true]],null,3,[null,\\\"S-2063066544:1619591418653949\\\"]]\",null,null,null,null,null,null,-19800,null,null,null,null,[],4,null,null,null,null,null,[]]],\"1619591418664\",[],null,null,null,null,null,null,null,null,0]"
]
--Response 
HTTP/1.1 200
status: 200
accept-ranges: bytes
vary: Accept-Encoding, Origin
content-encoding: gzip
content-type: text/javascript; charset=UTF-8
cross-origin-resource-policy: cross-origin
content-length: 913
date: Tue, 27 Apr 2021 01:24:24 GMT
expires: Wed, 27 Apr 2022 01:24:24 GMT
last-modified: Sat, 17 Apr 2021 02:34:15 GMT
x-content-type-options: nosniff
server: sffe
x-xss-protection: 0
cache-control: public, max-age=31536000
age: 104756
alt-svc: h3-29=\":443\"; ma=2592000,h3-T051=\":443\"; ma=2592000,h3-Q050=\":443\"; ma=2592000,h3-Q046=\":443\"; ma=2592000,h3-Q043=\":443\"; ma=2592000,quic=\":443\"; ma=2592000; v=\"46,43\"
----

