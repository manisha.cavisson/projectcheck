/*-----------------------------------------------------------------------------
    Name: flow2
    Recorded By: cavisson
    Date of recording: 04/07/2021 07:30:01
    Flow details:
    Build details: 4.6.0 (build# 46)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_check_U20;
import pacJnvmApi.NSApi;

public class flow2 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=http://www.kohls.com/",
            INLINE_URLS,
                "URL=http://www.kohls.com/squid-internal-static/icons/SN.png", END_INLINE
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.636);

        status = nsApi.ns_start_transaction("index_2");
        status = nsApi.ns_web_url ("index_2",
            "URL=http://www.google.com/",
            INLINE_URLS,
                "URL=http://www.google.com/squid-internal-static/icons/SN.png", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(126.841);

        return status;
    }
}
