/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Rajesh
    Date of recording: 05/18/2021 05:49:47
    Flow details:
    Build details: 4.6.0 (build# 69)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
    //URLId = 1, TimeStamp (Start = 0.981 secs, End = 1.193 secs, Resp Time = 0.212 secs)
        "URL=http://10.10.30.102:8333/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            //URLId = 2, TimeStamp (Start = 1.225 secs, End = 1.375 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 3, TimeStamp (Start = 1.229 secs, End = 1.449 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 4, TimeStamp (Start = 1.37 secs, End = 1.618 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 5, TimeStamp (Start = 1.373 secs, End = 1.62 secs)
            "URL=http://10.10.30.102:8333/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(7.703);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
    //URLId = 6, TimeStamp (Start = 9.323 secs, End = 9.588 secs, Resp Time = 0.265 secs)
        "URL=http://10.10.30.102:8333/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=53&login.y=14&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            //URLId = 7, TimeStamp (Start = 9.63 secs, End = 9.77 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 8, TimeStamp (Start = 9.634 secs, End = 9.908 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 9, TimeStamp (Start = 9.638 secs, End = 9.916 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 10, TimeStamp (Start = 9.642 secs, End = 9.918 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 11, TimeStamp (Start = 9.772 secs, End = 10.1 secs)
            "URL=http://10.10.30.102:8333/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 12, TimeStamp (Start = 9.774 secs, End = 10.103 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 13, TimeStamp (Start = 9.912 secs, End = 10.251 secs)
            "URL=http://10.10.30.102:8333/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(1.09);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
    //URLId = 14, TimeStamp (Start = 11.341 secs, End = 11.589 secs, Resp Time = 0.248 secs)
        "URL=http://10.10.30.102:8333/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            //URLId = 15, TimeStamp (Start = 11.709 secs, End = 11.946 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 16, TimeStamp (Start = 11.72 secs, End = 12.149 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 17, TimeStamp (Start = 11.727 secs, End = 12.155 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 18, TimeStamp (Start = 11.73 secs, End = 12.153 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 19, TimeStamp (Start = 11.733 secs, End = 12.157 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 20, TimeStamp (Start = 11.736 secs, End = 12.158 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 21, TimeStamp (Start = 12.166 secs, End = 12.355 secs)
            "URL=http://10.10.30.102:8333/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 22, TimeStamp (Start = 12.168 secs, End = 12.361 secs)
            "URL=http://10.10.30.102:8333/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(1.747);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
    //URLId = 23, TimeStamp (Start = 14.108 secs, End = 14.299 secs, Resp Time = 0.191 secs)
        "URL=http://10.10.30.102:8333/cgi-bin/findflight?depart=Acapulco&departDate=05-19-2021&arrive=Acapulco&returnDate=05-20-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=57&findFlights.y=19",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            //URLId = 24, TimeStamp (Start = 14.373 secs, End = 14.534 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 25, TimeStamp (Start = 14.385 secs, End = 14.531 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 26, TimeStamp (Start = 14.398 secs, End = 14.528 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 27, TimeStamp (Start = 14.402 secs, End = 14.678 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 28, TimeStamp (Start = 14.408 secs, End = 14.683 secs)
            "URL=http://10.10.30.102:8333/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 29, TimeStamp (Start = 14.415 secs, End = 14.688 secs)
            "URL=http://10.10.30.102:8333/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 30, TimeStamp (Start = 14.538 secs, End = 14.78 secs)
            "URL=http://10.10.30.102:8333/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 31, TimeStamp (Start = 14.665 secs, End = 14.813 secs)
            "URL=http://10.10.30.102:8333/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            //URLId = 32, TimeStamp (Start = 14.705 secs, End = 14.907 secs)
            "URL=http://10.10.30.102:8333/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(2.467);

}
