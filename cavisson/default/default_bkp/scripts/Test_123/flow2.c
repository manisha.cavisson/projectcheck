/*-----------------------------------------------------------------------------
    Name: flow2
    Recorded By: cavisson
    Date of recording: 05/18/2021 06:44:28
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow2()
{
    ns_start_transaction("index_html_2");
    ns_web_url ("index_html_2",
        "URL=http://10.10.30.41:8080/tours/index.html"
    );

    ns_end_transaction("index_html_2", NS_AUTO_STATUS);
    ns_page_think_time(76.026);

    ns_start_transaction("reservation_2");
    ns_web_url ("reservation_2",
        "URL=http://10.10.30.41:8080/tours/index.html",
        "HEADER=Cache-Control:max-age=0",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "REDIRECT=YES",
        "LOCATION=http://10.10.30.41:8080/cgi-bin/reservation"
    );

    ns_end_transaction("reservation_2", NS_AUTO_STATUS);

    //Page Auto split for application/json type
    ns_start_transaction("plugins_win_json");
    ns_web_url ("plugins_win_json",
        "URL=https://www.gstatic.com/chrome/config/plugins_3/plugins_win.json",
        "HEADER=Sec-Fetch-Site:none",
        "HEADER=Sec-Fetch-Mode:no-cors",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/cgi-bin/reservation", "HEADER=Cache-Control:max-age=0", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("plugins_win_json", NS_AUTO_STATUS);
    ns_page_think_time(1.661);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=05-19-2021&arrive=Acapulco&returnDate=05-20-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=71&findFlights.y=10",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(1.429);

    //Page Auto split for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.41:8080/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(1.31);

    //Page Auto split for Image Link 'Login' Clicked by User
    ns_start_transaction("login_2");
    ns_web_url ("login_2",
        "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=46&login.y=11&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("login_2", NS_AUTO_STATUS);
    ns_page_think_time(1.54);

    //Page Auto split for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation_3");
    ns_web_url ("reservation_3",
        "URL=http://10.10.30.41:8080/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("reservation_3", NS_AUTO_STATUS);
    ns_page_think_time(2.15);

    //Page Auto split for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=05-19-2021&arrive=Acapulco&returnDate=05-20-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=46&findFlights.y=3",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(2.156);

    //Page Auto split for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_4");
    ns_web_url ("findflight_4",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-19-2021&hidden_outboundFlight_button1=001%7C0%7C05-19-2021&hidden_outboundFlight_button2=002%7C0%7C05-19-2021&hidden_outboundFlight_button3=003%7C0%7C05-19-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=30&reserveFlights.y=21",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_4", NS_AUTO_STATUS);
    ns_page_think_time(4.679);

}
