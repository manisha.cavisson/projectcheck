/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: nibedita
    Date of recording: 01/28/2021 05:54:28
    Flow details:
    Build details: 4.5.0 (build# 83)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.41:8080/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836634654.png",
        "Snapshot=webpage_1611836635073.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(11.164);

    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=hello&password=hello&login.x=51&login.y=13&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836646174.png",
        "Snapshot=webpage_1611836646583.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(2.375);

    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.41:8080/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836648810.png",
        "Snapshot=webpage_1611836649204.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("reservation", NS_AUTO_STATUS);
    ns_page_think_time(4.158);

    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=02-01-2021&arrive=Acapulco&returnDate=02-02-2021&numPassengers=1&seatPref=None&seatType=Business&findFlights.x=87&findFlights.y=16",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836653204.png",
        "Snapshot=webpage_1611836653724.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(3.948);

    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C02-01-2021&outboundFlight=button1&hidden_outboundFlight_button1=001%7C0%7C02-01-2021&hidden_outboundFlight_button2=002%7C0%7C02-01-2021&hidden_outboundFlight_button3=003%7C0%7C02-01-2021&numPassengers=1&advanceDiscount=&seatType=Business&seatPref=None&reserveFlights.x=81&reserveFlights.y=18",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836657576.png",
        "Snapshot=webpage_1611836657967.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    ns_page_think_time(3.785);

    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Business&seatPref=None&outboundFlight=001%7C0%7C02-01-2021&advanceDiscount=&buyFlights.x=88&buyFlights.y=7&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836661568.png",
        "Snapshot=webpage_1611836661932.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(3.273);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.41:8080/cgi-bin/welcome{city2}",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1611836665046.png",
        "Snapshot=webpage_1611836665366.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(2.461);

}
