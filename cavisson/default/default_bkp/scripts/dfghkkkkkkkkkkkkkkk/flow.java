/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 05/12/2021 01:15:28
    Flow details:
    Build details: 4.6.0 (build# 67)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.dfghkkkkkkkkkkkkkkk;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=http://www.wwt.com/",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "REDIRECT=YES",
            "LOCATION=https://www.wwt.com/",
            INLINE_URLS,
                "URL=https://www.wwt.com/", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://use.typekit.net/pfc2dbt.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.google.com/recaptcha/api.js?onload=gRecaptchaOnLoad&render=explicit", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.gstatic.com/recaptcha/releases/npGaewopg1UaB8CNtYfx-y1j/recaptcha__en.js", "HEADER=Origin:https://www.wwt.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/c312f24.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/03a9aaf.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/7b6c232.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/769d21d.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/66c9921.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/be0bdf7.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/7e33ef3.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/97ac832.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/246f9a8.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://p.typekit.net/p.css?s=1&k=pfc2dbt&ht=tk&f=139.175.176&a=13200372&app=typekit&e=css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("gated_content");
        status = nsApi.ns_web_url ("gated_content",
            "URL=https://www.wwt.com/api/auth/gated-content",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("gated_content", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("gated_content_2");
        status = nsApi.ns_web_url ("gated_content_2",
            "URL=https://www.wwt.com/api/auth/gated-content",
            "METHOD=POST",
            "HEADER=Content-Length:56",
            "HEADER=Origin:https://www.wwt.com",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Content-Type:application/json;charset=UTF-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            BODY_BEGIN,
                "{"correlationId":"a8bfb0ed-644a-4415-8a00-42735e80c114"}",
            BODY_END,
            INLINE_URLS,
                "URL=https://use.typekit.net/af/705e94/00000000000000003b9b3062/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n4&v=3", "HEADER=Origin:https://www.wwt.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://use.typekit.net/af/949f99/00000000000000003b9b3068/27/l?primer=7cdcb44be4a7db8877ffa5c0007b8dd865b3bbc383831fe2ea177f62257a9191&fvd=n7&v=3", "HEADER=Origin:https://www.wwt.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/cf569de.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/36aad48.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f04db51.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE
        );

        status = nsApi.ns_end_transaction("gated_content_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("banners");
        status = nsApi.ns_web_url ("banners",
            "URL=https://www.wwt.com/api/corpsite/banners?active=true&inflateAuthor=false",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("banners", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages");
        status = nsApi.ns_web_url ("pages",
            "URL=https://www.wwt.com/api/corpsite/pages?page=1&pageSize=8&select=id,category.title,topic.title,headerImage.attachmentId,pageType.name,pageType.slug,slug,title,video.image.link,output.inflatedAttributes.playlist.count,restrictToGroups,totalDownloads,topology.attachmentId,diagram.attachmentId&corpsiteDependent=false&pageTypes=article,assessment,atc-insight,briefing,case-study,lab,playlist,technology,training,video,white-paper,workshop&sort=totalDownloads&order=desc",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id",
            INLINE_URLS,
                "URL=https://www.wwt.com/favicon-32x32.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/cdn/wwt-icon-font/style.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", "REDIRECT=YES", "LOCATION=https://cdn.apps.wwt.com/static/wwt-icon-font/style.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("pages", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_2");
        status = nsApi.ns_web_url ("pages_2",
            "URL=https://www.wwt.com/api/corpsite/pages?page=1&pageSize=8&select=id,category.title,topic.title,headerImage.attachmentId,pageType.name,pageType.slug,slug,title,video.image.link,output.inflatedAttributes.playlist.count,restrictToGroups,totalDownloads,topology.attachmentId,diagram.attachmentId&corpsiteDependent=false&pageTypesDistinct=article,assessment,atc-insight,briefing,case-study,lab,playlist,technology,training,video,white-paper,workshop&sort=totalDownloads&order=desc",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id",
            INLINE_URLS,
                "URL=https://www.wwt.com/favicon-32x32.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=If-None-Match:W/\"54c-17939727140\"", "HEADER=If-Modified-Since:Tue, 04 May 2021 22:14:32 GMT", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/favicon-32x32.png", END_INLINE,
                "URL=https://res.cloudinary.com/wwt/image/upload/q_70,f_auto/v1/shared/homepage/skylinebg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/dc32750.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f9f30ae.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/075ad42.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f01ffab.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://cdn.apps.wwt.com/static/wwt-icon-font/style.css", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/site.webmanifest", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/dbb9e25.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/50f7416.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/070d3a2.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/8256d4d.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/1322bd4.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/8545965.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/603cafd.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/0fe3419.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/296be1f.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/2017987.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/8a5ecb5.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/a5d16ff.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/ea30d44.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/7a335d8.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/597f20f.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/e9997e8.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/a4ed3b4.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/6ad05b7.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/723c38b.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/b3f03a3.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/0485216.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/bd8c463.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/ccd736b.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f7fe6c8.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/28d283f.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/38f170d.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f813ab0.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/f01a270.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/atc/labs", END_INLINE
        );

        status = nsApi.ns_end_transaction("pages_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_3");
        status = nsApi.ns_web_url ("pages_3",
            "URL=https://www.wwt.com/api/corpsite/categories/automation-and-orchestration/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_3", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_4");
        status = nsApi.ns_web_url ("pages_4",
            "URL=https://www.wwt.com/api/corpsite/categories/carrier-networking/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_4", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.69);

        //Page Auto split for Link 'A' Clicked by User
        status = nsApi.ns_start_transaction("d01f84f_js");
        status = nsApi.ns_web_url ("d01f84f_js",
            "URL=https://www.wwt.com/_nuxt/d01f84f.js",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id",
            INLINE_URLS,
                "URL=https://www.wwt.com/_nuxt/98dd30f.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/6b71e22.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/_nuxt/46c56d2.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE
        );

        status = nsApi.ns_end_transaction("d01f84f_js", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_5");
        status = nsApi.ns_web_url ("pages_5",
            "URL=https://www.wwt.com/api/corpsite/categories/data-center/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_5", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_6");
        status = nsApi.ns_web_url ("pages_6",
            "URL=https://www.wwt.com/api/corpsite/categories/digital/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_6", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_7");
        status = nsApi.ns_web_url ("pages_7",
            "URL=https://www.wwt.com/api/corpsite/categories/digital-workspace/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_7", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_8");
        status = nsApi.ns_web_url ("pages_8",
            "URL=https://www.wwt.com/api/corpsite/categories/multi-cloud-architecture/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_8", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_9");
        status = nsApi.ns_web_url ("pages_9",
            "URL=https://www.wwt.com/api/corpsite/categories/networking/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id"
        );

        status = nsApi.ns_end_transaction("pages_9", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("pages_10");
        status = nsApi.ns_web_url ("pages_10",
            "URL=https://www.wwt.com/api/corpsite/categories/security-transformation/pages?pageTypes=lab&select=category.id,category.title,creationDate,id,launchAccess.onDemand,slug,title,topic.title,totalDownloads,totalLaunches,output.inflatedAttributes.technologies,output.inflatedAttributes.oems&pageSize=10000&page=1",
            "HEADER=Wwt-Com-Session-Id:a8c0c7d9-d04e-4c44-9512-fe69be0016cc",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=wwt_correlation_id",
            INLINE_URLS,
                "URL=https://www.wwt.com/api/attachments/5d4b75a864713d0083005c6b/thumbnail?width=600", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/api/attachments/5ce564e891f9d5004cc2dc58/thumbnail?width=600", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/api/attachments/5d28acf7dca53e008b527f8d/thumbnail?width=600", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/api/attachments/5d4b741164713d0083005c4e/thumbnail?width=600", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE,
                "URL=https://www.wwt.com/api/attachments/5f11b9a45091b000801be8e2/thumbnail?width=600", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=wwt_correlation_id", END_INLINE
        );

        status = nsApi.ns_end_transaction("pages_10", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.664);

        return status;
    }
}
