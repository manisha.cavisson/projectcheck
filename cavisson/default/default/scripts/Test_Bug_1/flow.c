/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Rajesh
    Date of recording: 03/09/2021 04:49:13
    Flow details:
    Build details: 4.5.1 (build# 16)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{

char *message;
char *md_type[100];
int msg_len;
int encode_mode;

char *HMAC= ns_hmac("This is cavisson" , 14, "1234", 4, "MD5", 1);
ns_save_string(HMAC,"Hmac_encode");
    printf("new************ %s", ns_eval_string("{Hmac_encode}"));

    ns_start_transaction("Homepage");
    ns_web_url ("Homepage",
        "URL=http://10.10.30.41:9000/tours/index.html",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
        "PreSnapshot=webpage_1615288740235.png",
        "Snapshot=webpage_1615288740389.png",
        INLINE_URLS,
            "URL=http://10.10.30.41:9000/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9000/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9000/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=http://10.10.30.41:9000/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
    );

    ns_end_transaction("Homepage", NS_AUTO_STATUS);
    ns_page_think_time(2.259);
}
