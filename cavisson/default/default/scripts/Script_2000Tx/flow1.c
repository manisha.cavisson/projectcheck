10.10.30.7/work)/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: vinay
    Date of recording: 04/30/2019 11:28:34
    Flow details:
    Build details: 4.1.14 (build# 110)
    Modification History:
-----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"
void flow()
{
    ns_start_transaction("index_html");
    ns_web_url ("index_html",
        "URL=http://10.10.30.7:81/tours/index.html",
        "HEADER=Accept-Language:en-in",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1556603877150.png",
        "Snapshot=webpage_1556603878262.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("index_html", NS_AUTO_STATUS);
    ns_page_think_time(11.992);
    ns_start_transaction("login");
    ns_web_url ("login",
        "URL=http://10.10.30.7:81/cgi-bin/login?userSession={Session_Id}&username={user}&password={passwd}&login.x=48&login.y=17&login=Login&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603889973.png",
        "Snapshot=webpage_1556603891075.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("login", NS_AUTO_STATUS);
    ns_page_think_time(2.188);
    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation");
    ns_web_url ("reservation",
        "URL=http://10.10.30.7:81/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603893212.png",
        "Snapshot=webpage_1556603894092.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("reservation", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight");
    ns_web_url ("findflight",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?depart=Acapulco&departDate=05-01-2019&arrive=Acapulco&returnDate=05-02-2019&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=37&findFlights.y=8&findFlights=Submit",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603895786.png",
        "Snapshot=webpage_1556603896649.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight", NS_AUTO_STATUS);
    ns_page_think_time(2.278);
    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_2");
    ns_web_url ("findflight_2",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C05-01-2019&hidden_outboundFlight_button1=001%7C0%7C05-01-2019&hidden_outboundFlight_button2=002%7C0%7C05-01-2019&outboundFlight=button3&hidden_outboundFlight_button3=003%7C0%7C05-01-2019&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=38&reserveFlights.y=17",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603898892.png",
        "Snapshot=webpage_1556603900564.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight_2", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_3");
    ns_web_url ("findflight_3",
        "URL=http://10.10.30.7:81/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=003%7C0%7C05-01-2019&advanceDiscount=&buyFlights.x=44&buyFlights.y=10&.cgifields=saveCC",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603906439.png",
        "Snapshot=webpage_1556603907025.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/bookanother.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("findflight_3", NS_AUTO_STATUS);
    ns_page_think_time(2.74);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome");
    ns_web_url ("welcome",
        "URL=http://10.10.30.7:81/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603909748.png",
        "Snapshot=webpage_1556603910147.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("welcome", NS_AUTO_STATUS);
    ns_page_think_time(2.436);
   ns_start_transaction("index_html1");
    ns_web_url ("index_html1",
        "URL=http://10.10.30.7:81/tours/index.html",
        "HEADER=Accept-Language:en-in",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1556603877150.png",
        "Snapshot=webpage_1556603878262.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("index_html1", NS_AUTO_STATUS);
    ns_page_think_time(11.992);
    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation1");
    ns_web_url ("reservation1",
        "URL=http://10.10.30.7:81/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603893212.png",
        "Snapshot=webpage_1556603894092.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("reservation1", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight1");
    ns_web_url ("findflight1",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603895786.png",
        "Snapshot=webpage_1556603896649.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight1", NS_AUTO_STATUS);
    ns_page_think_time(2.278);
    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_21");
    ns_web_url ("findflight_21",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603898892.png",
        "Snapshot=webpage_1556603900564.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight_21", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_31");
    ns_web_url ("findflight_31",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603906439.png",
        "Snapshot=webpage_1556603907025.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/bookanother.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("findflight_31", NS_AUTO_STATUS);
    ns_page_think_time(2.74);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome1");
    ns_web_url ("welcome1",
        "URL=http://10.10.30.7:81/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603909748.png",
        "Snapshot=webpage_1556603910147.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("welcome1", NS_AUTO_STATUS);
    ns_page_think_time(2.436);
   ns_start_transaction("index_html2");
    ns_web_url ("index_html2",
        "URL=http://10.10.30.7:81/tours/index.html",
        "HEADER=Accept-Language:en-in",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1556603877150.png",
        "Snapshot=webpage_1556603878262.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("index_html2", NS_AUTO_STATUS);
    ns_page_think_time(11.992);
    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login2");
    ns_web_url ("login2",
        "URL=http://10.10.30.7:81/cgi-bin/login?userSession={Session_Id}&username={user}&password={passwd}&login.x=48&login.y=17&login=Login&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603889973.png",
        "Snapshot=webpage_1556603891075.png",
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("login2", NS_AUTO_STATUS);
    ns_page_think_time(2.188);
    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation2");
    ns_web_url ("reservation2",
        "URL=http://10.10.30.7:81/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603893212.png",
        "Snapshot=webpage_1556603894092.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("reservation2", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight2");
    ns_web_url ("findflight2",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603895786.png",
        "Snapshot=webpage_1556603896649.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight2", NS_AUTO_STATUS);
    ns_page_think_time(2.278);
    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_22");
    ns_web_url ("findflight_22",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603898892.png",
        "Snapshot=webpage_1556603900564.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight_22", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_32");
    ns_web_url ("findflight_32",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603906439.png",
        "Snapshot=webpage_1556603907025.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/bookanother.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("findflight_32", NS_AUTO_STATUS);
    ns_page_think_time(2.74);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome2");
    ns_web_url ("welcome2",
        "URL=http://10.10.30.7:81/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603909748.png",
        "Snapshot=webpage_1556603910147.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("welcome2", NS_AUTO_STATUS);
    ns_page_think_time(2.436);
-----------------------------
   ns_start_transaction("index_html3");
    ns_web_url ("index_html3",
        "URL=http://10.10.30.7:81/tours/index.html",
        "HEADER=Accept-Language:en-in",
        "HEADER=Upgrade-Insecure-Requests:1",
        "PreSnapshot=webpage_1556603877150.png",
        "Snapshot=webpage_1556603878262.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("index_html3", NS_AUTO_STATUS);
    ns_page_think_time(11.992);
    //Page Auto splitted for Image Link 'Login' Clicked by User
    ns_start_transaction("login3");
    ns_web_url ("login3",
        "URL=http://10.10.30.7:81/cgi-bin/login?userSession={Session_Id}&username={user}&password={passwd}&login.x=48&login.y=17&login=Login&JSFormSubmit=off",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603889973.png",
        "Snapshot=webpage_1556603891075.png",
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("login2", NS_AUTO_STATUS);
    ns_page_think_time(2.188);
    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("reservation3");
    ns_web_url ("reservation3",
        "URL=http://10.10.30.7:81/cgi-bin/reservation",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603893212.png",
        "Snapshot=webpage_1556603894092.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("reservation3", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("findflight3");
    ns_web_url ("findflight3",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603895786.png",
        "Snapshot=webpage_1556603896649.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/continue.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight3", NS_AUTO_STATUS);
    ns_page_think_time(2.278);
    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("findflight_23");
    ns_web_url ("findflight_23",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603898892.png",
        "Snapshot=webpage_1556603900564.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/startover.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );
    ns_end_transaction("findflight_23", NS_AUTO_STATUS);
    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("findflight_33");
    ns_web_url ("findflight_33",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603906439.png",
        "Snapshot=webpage_1556603907025.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/bookanother.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("findflight_33", NS_AUTO_STATUS);
    ns_page_think_time(2.74);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("welcome3");
    ns_web_url ("welcome3",
        "URL=http://10.10.30.7:81/cgi-bin/welcome",
        "HEADER=Upgrade-Insecure-Requests:1",
        "HEADER=Accept-Language:en-in",
        "PreSnapshot=webpage_1556603909748.png",
        "Snapshot=webpage_1556603910147.png",
        INLINE_URLS,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-in", END_INLINE,
            "URL=http://10.10.30.7:81/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-in", END_INLINE
    );

    ns_end_transaction("welcome3", NS_AUTO_STATUS);
    ns_page_think_time(2.436);
}
