/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/24/2021 05:23:38
    Flow details:
    Build details: 4.6.0 (build# 72)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_Build460_72;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
