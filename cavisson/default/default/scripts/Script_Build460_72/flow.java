/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 05/24/2021 05:23:39
    Flow details:
    Build details: 4.6.0 (build# 72)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_Build460_72;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_html");
        status = nsApi.ns_web_url ("index_html",
        //URLId = 1, TimeStamp (Start = 11.386 secs, End = 11.452 secs, Resp Time = 0.066 secs)
            "URL=http://10.10.30.41:8080/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 2, TimeStamp (Start = 11.632 secs, End = 11.654 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 3, TimeStamp (Start = 11.641 secs, End = 11.801 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 4, TimeStamp (Start = 11.689 secs, End = 11.811 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 5, TimeStamp (Start = 11.779 secs, End = 11.864 secs)
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_html", NS_AUTO_STATUS);
        
        status = nsApi.ns_page_think_time(3.23);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login",
        //URLId = 7, TimeStamp (Start = 15.32 secs, End = 15.349 secs, Resp Time = 0.029 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username={Uname}&password={Upass}&login.x=49&login.y=20&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1*",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 8, TimeStamp (Start = 15.437 secs, End = 15.5 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 9, TimeStamp (Start = 15.449 secs, End = 15.51 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 10, TimeStamp (Start = 15.457 secs, End = 15.53 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 11, TimeStamp (Start = 15.465 secs, End = 15.539 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 12, TimeStamp (Start = 15.487 secs, End = 15.565 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 13, TimeStamp (Start = 15.494 secs, End = 15.631 secs)
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 14, TimeStamp (Start = 15.524 secs, End = 15.679 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(4.756);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
        //URLId = 15, TimeStamp (Start = 20.435 secs, End = 20.457 secs, Resp Time = 0.022 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1*******{SearchParam}*********",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 16, TimeStamp (Start = 20.613 secs, End = 20.691 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 17, TimeStamp (Start = 20.635 secs, End = 20.709 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 18, TimeStamp (Start = 20.648 secs, End = 20.733 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 19, TimeStamp (Start = 20.657 secs, End = 20.722 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 20, TimeStamp (Start = 20.668 secs, End = 20.745 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 21, TimeStamp (Start = 20.678 secs, End = 20.88 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 22, TimeStamp (Start = 20.765 secs, End = 20.925 secs)
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 23, TimeStamp (Start = 20.772 secs, End = 20.936 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.937);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
        //URLId = 24, TimeStamp (Start = 22.873 secs, End = 22.913 secs, Resp Time = 0.04 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=05-25-2021&arrive=Acapulco&returnDate=05-26-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=50&findFlights.y=7",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 25, TimeStamp (Start = 23.015 secs, End = 23.062 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 26, TimeStamp (Start = 23.02 secs, End = 23.08 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 27, TimeStamp (Start = 23.033 secs, End = 23.098 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 28, TimeStamp (Start = 23.04 secs, End = 23.09 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 29, TimeStamp (Start = 23.048 secs, End = 23.122 secs)
                "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 30, TimeStamp (Start = 23.054 secs, End = 23.142 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 31, TimeStamp (Start = 23.169 secs, End = 23.217 secs)
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 32, TimeStamp (Start = 23.183 secs, End = 23.206 secs)
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 33, TimeStamp (Start = 23.193 secs, End = 23.248 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.677);

        //Page Auto split for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
        //URLId = 34, TimeStamp (Start = 26.925 secs, End = 26.955 secs, Resp Time = 0.03 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C05-25-2021&hidden_outboundFlight_button1=001%7C0%7C05-25-2021&outboundFlight=button2&hidden_outboundFlight_button2=002%7C0%7C05-25-2021&hidden_outboundFlight_button3=003%7C0%7C05-25-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=65&reserveFlights.y=9",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 35, TimeStamp (Start = 27.014 secs, End = 27.079 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 36, TimeStamp (Start = 27.021 secs, End = 27.089 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 37, TimeStamp (Start = 27.031 secs, End = 27.098 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 38, TimeStamp (Start = 27.039 secs, End = 27.106 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 39, TimeStamp (Start = 27.059 secs, End = 27.147 secs)
                "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 40, TimeStamp (Start = 27.067 secs, End = 27.158 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 41, TimeStamp (Start = 27.138 secs, End = 27.267 secs)
                "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 42, TimeStamp (Start = 27.181 secs, End = 27.297 secs)
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 43, TimeStamp (Start = 27.19 secs, End = 27.311 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(4.039);

        //Page Auto split for Image Link 'buyFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
        //URLId = 44, TimeStamp (Start = 31.35 secs, End = 31.371 secs, Resp Time = 0.021 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=002%7C0%7C05-25-2021&advanceDiscount=&buyFlights.x=48&buyFlights.y=13&.cgifields=saveCC",
            "HEADER=Upgrade-Insecure-Requests:1*{SearchBlankRB}*********************",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 45, TimeStamp (Start = 31.44 secs, End = 31.501 secs)
                "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 46, TimeStamp (Start = 31.449 secs, End = 31.516 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 47, TimeStamp (Start = 31.456 secs, End = 31.527 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 48, TimeStamp (Start = 31.464 secs, End = 31.539 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 49, TimeStamp (Start = 31.472 secs, End = 31.586 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 50, TimeStamp (Start = 31.483 secs, End = 31.596 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 51, TimeStamp (Start = 31.553 secs, End = 31.617 secs)
                "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 52, TimeStamp (Start = 31.57 secs, End = 31.63 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.723);

        //Page Auto split for Image Link 'SignOff Button' Clicked by User
        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome",
        //URLId = 53, TimeStamp (Start = 35.353 secs, End = 35.37 secs, Resp Time = 0.017 secs)
            "URL=http://10.10.30.41:8080/cgi-bin/welcome",
            "HEADER=Upgrade-Insecure-Requests:1******{SearchBlankRB}******",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                //URLId = 54, TimeStamp (Start = 35.42 secs, End = 35.466 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 55, TimeStamp (Start = 35.43 secs, End = 35.477 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 56, TimeStamp (Start = 35.436 secs, End = 35.487 secs)
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 57, TimeStamp (Start = 35.451 secs, End = 35.529 secs)
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                //URLId = 58, TimeStamp (Start = 35.459 secs, End = 35.537 secs)
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(2.45);

        return status;
    }
}
