/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Manisha
    Date of recording: 04/13/2021 05:23:30
    Flow details:
    Build details: 4.6.0 (build# 49)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_checkkScript_C;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
