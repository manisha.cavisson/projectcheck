/*-----------------------------------------------------------------------------
    Name: flow
    Generated By: cavisson
    Date of generation: 04/28/2021 02:14:35
    Flow details:
    Build details: 4.6.0 (build# 57)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.scriptcheckcompilation;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;
    status = nsApi.ns_start_transaction("Xbatch_8");
        status = nsApi.ns_web_url ("Xbatch_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:456",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_82db-a0db-5a3d",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3636303938443636443646454534343738384136443545443630413630413233362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,  
            MULTIPART_BOUNDARY,
            //--batch_82db-a0db-5a3d
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjEngagementsSet?sap-client=100&$skip=0&$top=8&$orderby=ChangedOn%20desc&$expand=ProjectRoleSet&$inlinecount=allpages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject&/Display/ProjEngagementsSet/567FHK", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/library-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        return status;
    }
}