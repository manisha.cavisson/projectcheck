/*-----------------------------------------------------------------------------
    Name: flow3
    Recorded By: cavisson
    Date of recording: 05/31/2021 11:38:49
    Flow details:
    Build details: 4.6.1 (build# 4)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.copied123;
import pacJnvmApi.NSApi;

public class flow3 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_web_url ("index_html_3",
            "URL=http://10.10.30.102:8111/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "PreSnapshot=webpage_1622484522699.png",
            "Snapshot=webpage_1622484523069.png",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_page_think_time(1.378);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_web_url ("login_3",
            "URL=http://10.10.30.102:8111/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=75&login.y=13&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            "PreSnapshot=webpage_1622484524703.png",
            "Snapshot=webpage_1622484524772.png",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_page_think_time(0.991);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_web_url ("reservation_2",
            "URL=http://10.10.30.102:8111/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            "PreSnapshot=webpage_1622484525906.png",
            "Snapshot=webpage_1622484525955.png",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_page_think_time(2.052);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://10.10.30.102:8111/cgi-bin/findflight?depart=Acapulco&departDate=06-01-2021&arrive=Acapulco&returnDate=06-02-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=58&findFlights.y=10",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            "PreSnapshot=webpage_1622484528211.png",
            "Snapshot=webpage_1622484528253.png",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

 

        return status;
    }
}
