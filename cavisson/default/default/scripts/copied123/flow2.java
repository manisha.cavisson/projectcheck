/*-----------------------------------------------------------------------------
    Name: flow2
    Recorded By: cavisson
    Date of recording: 05/31/2021 11:38:10
    Flow details:
    Build details: 4.6.1 (build# 4)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.copied123;
import pacJnvmApi.NSApi;

public class flow2 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_web_url ("index_html_2",
            "URL=http://10.10.30.102:8111/tours/index.html",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_page_think_time(1.622);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_web_url ("login_2",
            "URL=http://10.10.30.102:8111/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=48&login.y=9&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=SL_Cookie",
            INLINE_URLS,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE,
                "URL=http://10.10.30.102:8111/favicon.ico", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=SL_Cookie", END_INLINE
        );

        status = nsApi.ns_page_think_time(1.365);

        return status;
    }
}
