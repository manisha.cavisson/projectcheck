/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/31/2021 11:37:33
    Flow details:
    Build details: 4.6.1 (build# 4)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.copied123;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
