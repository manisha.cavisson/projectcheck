/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 05/28/2021 11:04:25
    Flow details:
    Build details: 4.6.0 (build# 74)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.ScriptCheck123;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=http://www.google.com/",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "REDIRECT=YES",
            "LOCATION=https://www.google.com/?gws_rd=ssl",
            INLINE_URLS,
                "URL=https://www.google.com/?gws_rd=ssl", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0qVbB-nooiM.O/m=cdos,dpf,hsm,jsa,d,csi/am=QBFAAAAAAAAAAAIFAAAAAGMAAAAAAABQCACQDA4EAMDQPDIAABAogCOgQAoBAQAAABPYDxDw3wQAXMBGGAAAAAAAIACXQEapAYmCAAQAAAAgU6unACEABA/d=1/ed=1/dg=2/br=1/rs=ACT90oHzy807Ssr1SV7Q1M3oRQd2YMmogg", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.google.com/images/searchbox/desktop_searchbox_sprites318_hr.webp", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.gstatic.com/og/_/js/k=og.qtm.en_US.-ThjTyCG6l8.O/rt=j/m=qabr,q_d,qcwid,qmutsd,qapid,qald/exm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/rs=AA2YrTtqUwmHCYezZsbgFhyf-tyVHNCx7A", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://www.gstatic.com/og/_/ss/k=og.qtm.rGzT29-BEU0.L.W.O/m=qcwid/excm=qaaw,qadd,qaid,qein,qhaw,qhbr,qhch,qhga,qhid,qhin,qhpr/d=1/ed=1/ct=zgms/rs=AA2YrTuK7JjpgAQwJqDGAD6h0l_MVATosw", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("gen_204");
        status = nsApi.ns_web_url ("gen_204",
            "URL=https://www.google.com/gen_204?s=webhp&t=aft&atyp=csi&ei=3oCwYJCRIPDaz7sPiZmVkAo&rt=wsrt.563,aft.359,prt.247&imn=1&ima=1&imad=0&aftp=607&bl=ckvK",
            "METHOD=POST",
            "HEADER=Origin:https://www.google.com",
            "HEADER=Content-Type:text/plain;charset=UTF-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=1P_JAR;NID"
        );

        status = nsApi.ns_end_transaction("gen_204", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("search");
        status = nsApi.ns_web_url ("search",
            "URL=https://www.google.com/complete/search?q&cp=0&client=gws-wiz&xssi=t&gs_ri=gws-wiz&hl=en-IN&authuser=0&psi=3oCwYJCRIPDaz7sPiZmVkAo.1622180062579&nolsbt=1&dpr=1",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=1P_JAR;NID",
            INLINE_URLS,
                "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0qVbB-nooiM.O/ck=xjs.s.mfBdg5x0pyM.L.W.O/am=QBFAAAAAAAAAAAIFAAAAAGMAAAAAAABQCACQDA4EAMDQPDIAABAogCOgQAoBAQAAABPYDxDw3wQAXMBGGAAAAAAAIACXQEapAYmCAAQAAAAgU6unACEABA/d=1/exm=cdos,csi,d,dpf,hsm,jsa/ed=1/dg=2/br=1/rs=ACT90oEVZWLkc4psU6xfUuIXhOmNn1p7Pg/m=BLvsRb,NBZ7u,aa,abd,async,dvl,fEVMic,mUpTid,mu,sb_wiz,sf,sonic,spch,xz7cCd?xjs=s1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.google.com/client_204?&atyp=i&biw=1366&bih=607&ei=3oCwYJCRIPDaz7sPiZmVkAo", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://apis.google.com/_/scs/abc-static/_/js/k=gapi.gapi.en.vQiXRrxCe40.O/m=gapi_iframes,googleapis_client/rt=j/sv=1/d=1/ed=1/rs=AHpOoo-wBrxoiAJAWI5rmBYJxS8iVgTdFg/cb=gapi.loaded_0", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://ogs.google.com/widget/app/so?bc=1&origin=https%3A%2F%2Fwww.google.com&cn=app&pid=1&spid=538&hl=en", "HEADER=Origin:https://www.google.com", "HEADER=Purpose:prefetch", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0qVbB-nooiM.O/ck=xjs.s.mfBdg5x0pyM.L.W.O/am=QBFAAAAAAAAAAAIFAAAAAGMAAAAAAABQCACQDA4EAMDQPDIAABAogCOgQAoBAQAAABPYDxDw3wQAXMBGGAAAAAAAIACXQEapAYmCAAQAAAAgU6unACEABA/d=1/exm=BLvsRb,NBZ7u,aa,abd,async,cdos,csi,d,dpf,dvl,fEVMic,hsm,jsa,mUpTid,mu,sb_wiz,sf,sonic,spch,xz7cCd/ed=1/dg=2/br=1/rs=ACT90oEVZWLkc4psU6xfUuIXhOmNn1p7Pg/m=HFyn5c?xjs=s2", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE,
                "URL=https://www.google.com/xjs/_/js/k=xjs.s.en_GB.0qVbB-nooiM.O/ck=xjs.s.mfBdg5x0pyM.L.W.O/am=QBFAAAAAAAAAAAIFAAAAAGMAAAAAAABQCACQDA4EAMDQPDIAABAogCOgQAoBAQAAABPYDxDw3wQAXMBGGAAAAAAAIACXQEapAYmCAAQAAAAgU6unACEABA/d=1/exm=BLvsRb,HFyn5c,NBZ7u,aa,abd,async,cdos,csi,d,dpf,dvl,fEVMic,hsm,jsa,mUpTid,mu,sb_wiz,sf,sonic,spch,xz7cCd/ed=1/dg=2/br=1/rs=ACT90oEVZWLkc4psU6xfUuIXhOmNn1p7Pg/m=wkrYee?xjs=s2", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE
        );

        status = nsApi.ns_end_transaction("search", NS_AUTO_STATUS);
    
    //Page Auto split for Method = POST
        status = nsApi.ns_start_transaction("gen_204_2");
        status = nsApi.ns_web_url ("gen_204_2",
            "URL=https://www.google.com/gen_204?atyp=csi&ei=3oCwYJCRIPDaz7sPiZmVkAo&s=webhp&t=all&bl=ckvK&imn=1&ima=1&imad=0&aftp=607&adh=&conn=onchange&ime=1&imex=1&imeh=0&imea=0&imeb=0&wh=607&scp=0&net=dl.1600,ect.4g,rtt.100&mem=ujhs.15,tjhs.21,jhsl.4345,dm.8&sto=&sys=hc.4&rt=aft.359,prt.247,iml.359,xjses.442,xjsee.510,xjs.511,dcl.512,ol.802,wsrt.563,cst.108,dnst.0,rqst.266,rspt.110,sslt.102,rqstt.407,unt.274,cstt.298,dit.858&zx=1622180062829",
            "METHOD=POST",
            "HEADER=Origin:https://www.google.com",
            "HEADER=Content-Type:text/plain;charset=UTF-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=1P_JAR;NID",
            INLINE_URLS,
                "URL=https://adservice.google.com/adsid/google/ui", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=1P_JAR;NID", END_INLINE
        );

        status = nsApi.ns_end_transaction("gen_204_2", NS_AUTO_STATUS);
    
    //Page Auto split for application/json type
        status = nsApi.ns_start_transaction("manifest");
        status = nsApi.ns_web_url ("manifest",
            "URL=https://www.google.com/manifest?pwa=webhp",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=1P_JAR;NID"
        );

        status = nsApi.ns_end_transaction("manifest", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.982);

        return status;
    }
}
