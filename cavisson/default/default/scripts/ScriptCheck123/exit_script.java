/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/28/2021 11:04:25
    Flow details:
    Build details: 4.6.0 (build# 74)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.ScriptCheck123;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
