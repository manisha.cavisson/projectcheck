/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: Rajesh
    Date of recording: 05/25/2021 11:12:26
    Flow details:
    Build details: 4.6.0 (build# 73)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Test_param;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome",
            "URL=http://fvwctup/",
            "METHOD=HEAD"
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
    
    //Page Auto split for Method = HEAD
        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=http://sorqvjtrxwq/",
            "METHOD=HEAD"
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
    
    //Page Auto split for Method = HEAD
        status = nsApi.ns_start_transaction("index_2");
        status = nsApi.ns_web_url ("index_2",
            "URL=http://zbbsaayiyckwyth/",
            "METHOD=HEAD",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/cgi-bin/welcome", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.026);

        //Page Auto split for Image Link 'Login' Clicked by User
        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login",
            "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=32&login.y=19&JSFormSubmit=off",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.118);

        //Page Auto split for Image Link 'Search Flights Button' Clicked by User
        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
            "URL=http://10.10.30.41:8080/cgi-bin/reservation",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.616);

        //Page Auto split for Image Link 'findFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=05-26-2021&arrive=Acapulco&returnDate=05-27-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=65&findFlights.y=13",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.848);

        //Page Auto split for Image Link 'reserveFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-26-2021&hidden_outboundFlight_button1=001%7C0%7C05-26-2021&hidden_outboundFlight_button2=002%7C0%7C05-26-2021&hidden_outboundFlight_button3=003%7C0%7C05-26-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=51&reserveFlights.y=18",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(3.408);

        //Page Auto split for Image Link 'buyFlights' Clicked by User
        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C05-26-2021&advanceDiscount=&buyFlights.x=45&buyFlights.y=13&.cgifields=saveCC",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.818);

        return status;
    }
}
