/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 04/07/2021 07:26:39
    Flow details:
    Build details: 4.6.0 (build# 46)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_check_U20;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_html_2");
        status = nsApi.ns_web_url ("index_html_2",
            "URL=http://10.10.30.41:8080/tours/index.html",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_html_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(63.649);

        status = nsApi.ns_start_transaction("login");
        status = nsApi.ns_web_url ("login",
            "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=66&login.y=8&JSFormSubmit=off",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE
        );

        status = nsApi.ns_end_transaction("login", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(91.351);

        status = nsApi.ns_start_transaction("reservation");
        status = nsApi.ns_web_url ("reservation",
            "URL=http://10.10.30.41:8080/cgi-bin/reservation",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(104.988);

        status = nsApi.ns_start_transaction("findflight");
        status = nsApi.ns_web_url ("findflight",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=04-08-2021&arrive=Acapulco&returnDate=04-09-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=62&findFlights.y=14",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(111.154);

        status = nsApi.ns_start_transaction("findflight_2");
        status = nsApi.ns_web_url ("findflight_2",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?hidden_outboundFlight_button0=000%7C0%7C04-08-2021&hidden_outboundFlight_button1=001%7C0%7C04-08-2021&outboundFlight=button2&hidden_outboundFlight_button2=002%7C0%7C04-08-2021&hidden_outboundFlight_button3=003%7C0%7C04-08-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=59&reserveFlights.y=16",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(114.768);

        status = nsApi.ns_start_transaction("findflight_3");
        status = nsApi.ns_web_url ("findflight_3",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=002%7C0%7C04-08-2021&advanceDiscount=&buyFlights.x=74&buyFlights.y=11&.cgifields=saveCC",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(117.872);

        status = nsApi.ns_start_transaction("welcome");
        status = nsApi.ns_web_url ("welcome",
            "URL=http://10.10.30.41:8080/cgi-bin/welcome",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(134.379);

        status = nsApi.ns_start_transaction("login_2");
        status = nsApi.ns_web_url ("login_2",
            "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=43&login.y=21&JSFormSubmit=off",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("login_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(222.901);

        status = nsApi.ns_start_transaction("reservation_2");
        status = nsApi.ns_web_url ("reservation_2",
            "URL=http://10.10.30.41:8080/cgi-bin/reservation",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(223.845);

        status = nsApi.ns_start_transaction("findflight_4");
        status = nsApi.ns_web_url ("findflight_4",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=04-08-2021&arrive=Acapulco&returnDate=04-09-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=38&findFlights.y=3",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_4", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(225.674);

        status = nsApi.ns_start_transaction("findflight_5");
        status = nsApi.ns_web_url ("findflight_5",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C04-08-2021&hidden_outboundFlight_button1=001%7C0%7C04-08-2021&hidden_outboundFlight_button2=002%7C0%7C04-08-2021&hidden_outboundFlight_button3=003%7C0%7C04-08-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=35&reserveFlights.y=18",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_5", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(226.871);

        status = nsApi.ns_start_transaction("findflight_6");
        status = nsApi.ns_web_url ("findflight_6",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C04-08-2021&advanceDiscount=&buyFlights.x=28&buyFlights.y=21&.cgifields=saveCC",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_6", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(230.337);

        status = nsApi.ns_start_transaction("welcome_2");
        status = nsApi.ns_web_url ("welcome_2",
            "URL=http://10.10.30.41:8080/cgi-bin/welcome",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.61);

        return status;
    }
}
