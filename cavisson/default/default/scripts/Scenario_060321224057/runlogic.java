package com.cavisson.scripts.Scenario_060321224057;

import pacJnvmApi.NSApi;

public class runlogic
{

      // Note: Following extern declaration is used to find the list of used flows. Do not delete/edit it
      //Start - List of used flows in the runlogic
      //Initialise the flow class
      Flow_1 flowObj0 = new Flow_1();
      //End - List of used flows in the runlogic

      public void execute(NSApi nsApi) throws Exception
      {
        //Logging
        int initStatus = init_script.execute(nsApi);

        //Executing sequence blocik - Block1
        //Executing flow - Flow_1
        flowObj0.execute(nsApi);

        //logging
        nsApi.ns_end_session();

      }
}