//file parameter
nsl_static_var(u-name:1,u-pass:2, File=.ran, Refresh=SESSION, Mode=RANDOM, FirstDataLine=2, EncodeMode=All);
//search parameter
nsl_search_var(c-ity, PAGE=reservation, LB="=\"", RB="\">", LBMATCH=FIRST, ORD=29, SaveOffset=0, RETAINPREVALUE="NO", EncodeMode=None);
//declare parameter
nsl_decl_var(d-param);
//date and time variable
nsl_date_var(date-Time, Format="%m/%d/%Y", Offset=1.00:00:00, Refresh=SESSION);
//indexfile parameter
nsl_index_file_var(In-dex-param, File=cavi, indexVar=c-ity, EncodeMode=All);
//json parameter
nsl_json_var(J-son-param, PAGE=JSON_Page, OBJECT_PATH="root.store.book.price", ORD=1, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
//declarearray
nsl_decl_array(dec-array,Size=10,DefaultValue="cavisson");
//randomstring
nsl_random_string_var(Ran-string, Min=1, Max=5, CharSet="a-zA-Z0-9", Refresh=SESSION);
//randomnumber
nsl_random_number_var(ran-number, Min=1, Max=5, Format=%01lu, Refresh=SESSION);
//uniquenumber
nsl_unique_number_var(Uni-number, Format=%09lu, Refresh=SESSION);
//xmlparameter
nsl_xml_var(xml, PAGE=XML_Page, NODE=<bookstore><year>, VALUE=<>, ORD=1, EncodeMode=None);
