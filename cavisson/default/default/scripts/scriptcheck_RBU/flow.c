/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 05/05/2021 03:28:42
    Flow details:
    Build details: 4.6.0 (build# 59)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{

    ns_start_transaction("index_html");
    ns_browser("index_html",
        "url=http://10.10.30.41:8080/tours/index.html",
        "browserurl=http://10.10.30.41:8080/tours/index.html",
        "action=Home",
        "title=Mercury Tours",
        "Snapshots=webpage_1620208622754.png");
    ns_end_transaction("index_html", NS_AUTO_STATUS);

    ns_page_think_time(5.607);



    ns_start_transaction("index_html_2");
    ns_link("index_html_2",
        "url=http://10.10.30.41:8080/tours/index.html",
        "action=click",
        attributes=["border=0","name=login","src=Merc10-dev/images/login.gif","alt=Login","type=image","width=95","height=25","value=Login"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > form > center > table > tbody > tr:nth-child(5) > td > input[type=\"image\"]",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[5]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshots=webpage_1620208628362.png");
    ns_end_transaction("index_html_2", NS_AUTO_STATUS);

    ns_page_think_time(2.933);



    ns_start_transaction("login");
    ns_link("login",
        "url=http://10.10.30.41:8080/cgi-bi/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=68&login.y=19&JSFormSubmit=off",
        "type=IMAGE_LINK",
        "action=click",
        attributes=["src=/tours/Merc10-dev/images/flights.gif","alt=Search Flights Button","border=0","width=95","height=25"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > center > table > tbody > tr:nth-child(1) > td > a > img",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/A[1]/IMG[1]",
        "tagName=IMG",
        "Snapshots=webpage_1620208631294.png");
    ns_end_transaction("login", NS_AUTO_STATUS);

    ns_page_think_time(1.655);

ns_db_connect(char *conn_str);

    ns_start_transaction("reservation");
    ns_link("reservation",
        "url=http://10.10.30.41:8080/cgi-bin/reservation",
        "action=click",
        attributes=["src=/tours/images/continue.gif","border=0","type=IMAGE","value=Submit","name=findFlights"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr > td > table > tbody > tr > td > form > table > tbody > tr:nth-child(7) > td > input[type=\"IMAGE\"]",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/TABLE[1]/TBODY[1]/TR[7]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshots=webpage_1620208632949.png");
    ns_end_transaction("reservation", NS_AUTO_STATUS);

    ns_page_think_time(2.984);



    ns_start_transaction("findflight");
    ns_link("findflight",
        "url=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=05-06-2021&arrive=Acapulco&returnDate=05-07-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=63&findFlights.y=9",
        "action=click",
        attributes=["src=/tours/images/continue.gif","border=0","type=image","name=reserveFlights"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr > td > table > tbody > tr > td > form > center > center > table > tbody > tr > td:nth-child(1) > input[type=\"image\"]",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/FORM[1]/CENTER[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/INPUT[1]",
        "tagName=INPUT",
        "Snapshots=webpage_1620208635933.png");
    ns_end_transaction("findflight", NS_AUTO_STATUS);

    ns_page_think_time(2.724);



    ns_start_transaction("findflight_2");
    ns_link("findflight_2",
        "url=http://10.10.30.41:8080/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C05-06-2021&hidden_outboundFlight_button1=001%7C0%7C05-06-2021&hidden_outboundFlight_button2=002%7C0%7C05-06-2021&hidden_outboundFlight_button3=003%7C0%7C05-06-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=31&reserveFlights.y=6",
        "action=click",
        attributes=["src=/tours/images/purchaseflight.gif","border=0","type=image","name=buyFlights"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(2) > table > tbody > tr > td > table > tbody > tr > td:nth-child(1) > blockquote > form > input[type=\"image\"]:nth-child(7)",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[2]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/BLOCKQUOTE[1]/FORM[1]/INPUT[6]",
        "tagName=INPUT",
        "Snapshots=webpage_1620208638657.png");
    ns_end_transaction("findflight_2", NS_AUTO_STATUS);

    ns_page_think_time(77.368);



    ns_start_transaction("findflight_3");
    ns_link("findflight_3",
        "url=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C05-06-2021&advanceDiscount=&buyFlights.x=56&buyFlights.y=17&.cgifields=saveCC",
        "type=IMAGE_LINK",
        "action=click",
        attributes=["src=/tours/Merc10-dev/images/signoff.gif","alt=SignOff Button","border=0","width=95","height=25"],
        "csspath=html > body > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(1) > center > table > tbody > tr:nth-child(3) > td > a > img",
        "xpath=HTML/BODY[1]/TABLE[1]/TBODY[1]/TR[2]/TD[1]/TABLE[1]/TBODY[1]/TR[1]/TD[1]/CENTER[1]/TABLE[1]/TBODY[1]/TR[3]/TD[1]/A[1]/IMG[1]",
        "tagName=IMG",
        "Snapshots=webpage_1620208716025.png");
    ns_end_transaction("findflight_3", NS_AUTO_STATUS);

    ns_page_think_time(6.098);


}
