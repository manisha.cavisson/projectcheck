/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: cavisson
    Date of recording: 04/28/2021 12:38:41
    Flow details:
    Build details: 4.6.0 (build# 57)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.script_clientMultipart;
import pacJnvmApi.NSApi;

public class flow implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("ui");
        status = nsApi.ns_web_url ("ui",
            "URL=https://ctfproda25d5a988.hana.ondemand.com/ctf-core/backend/redirect/v1/TSS1000/P2003858757",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Sec-Fetch-User:?1",
            "HEADER=Sec-Fetch-Site:none",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "REDIRECT=YES",
            "LOCATION=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:none", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://arwzjcavz.accounts.ondemand.com/saml2/idp/sso/arwzjcavz.accounts.ondemand.com", END_INLINE
        );

        status = nsApi.ns_end_transaction("ui", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("arwzjcavz_accounts_ondemand_");
        status = nsApi.ns_web_url ("arwzjcavz_accounts_ondemand_",
            "URL=https://arwzjcavz.accounts.ondemand.com/saml2/idp/sso/arwzjcavz.accounts.ondemand.com",
            "METHOD=POST",
            "HEADER=Content-Length:2419",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Content-Type:application/x-www-form-urlencoded",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "REDIRECT=YES",
            "LOCATION=https://saptrial.accounts.ondemand.com/saml2/idp/sso/saptrial.accounts.ondemand.com?SAMLRequest=jZLRTsIwFIZfZanXW7tuEGwGCUqMJKhE0AvvSlegZjudPR1Tn96BGLgBTHrVfH%2Fy%2FeecbFj7NTzrj1qjD4aI2nlj4dYC1qV2M%2B02RumX50mfrL2vUFAqXfP9ruTmO5JK2Ro8RhZyXUrII2VLirIsODV5RaXCSzQJps56q2xxYyA3sOqT2oGwEg0KkKVG4ZWYDR8mgkdMLH4hFPfz%2BTScPs3mJBi15gbkVvsgibLyzsjioiOivQCTYDzqk1lPaxX3GA8XKumEaZx0w143ZiHvSpXqlOcyTVoUsdZjQC%2FB9wlnPA5ZGvLenHVFp3086lynbyR41Q53wm0rEnyWBeD55tV%2BTHtYAPLzAfm3y0Mi%2FZ1PO56maaImiaxbUc5YTFlKW0iDujrQyQmaUXa9pXM0qysyyFoTsavtBv88kYweZbLHVn08mtrCqK%2FgzrpS%2BtPN4ije%2FZg8XO5QUQNWWpml0Tmhg4weH%2FTgBw%3D%3D&RelayState=arcb43ab0&SigAlg=http%3A%2F%2Fwww.w3.org%2F2001%2F04%2Fxmldsig-more%23rsa-sha256&Signature=UTfSbBwSOBAPJaK16tvAj7tb6SeUtLJuTraMIThPd%2FSwC1C29SEjzmz2SpdhMtYMvAUbiL4fLpW%2FteSN5icPooZ8sQNxNHCLrpkVkrzOlHWel6CUb5QX%2B1GqAdwVEL9dh8t5n7l9tINzTica8KiEmPkNiRoGlPCPxAOoOYhX%2BHOS5Ha4aIFSC0TmG2FlXd3GOGkEDc29neq1uP7EQfDugRsksjnnqVXx4R5CSLsFeiLhQCXlzDWp8H39io%2FjUuKPqYAZjEoFSBfqGeFRPiKEqjqOvG0uL%2F0bFmj5ACxzRECsNpb%2BPgqbHDL2OK2uBc9X1kxP2j6RkYQl9DLgdfFdTQ%3D%3D",
            BODY_BEGIN,
                "[key: "SAMLRequest"
value: "PHNhbWxwOkF1dGhuUmVxdWVzdCBJRD0iU2ZhMTYzZTAyLTU4Y2EtMWVlYi1hOWZkLWRkMGUwMjI4YTU4YyIgVmVyc2lvbj0iMi4wIiBJc3N1ZUluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTY6NTBaIiBEZXN0aW5hdGlvbj0iaHR0cHM6Ly9hcnd6amNhdnouYWNjb3VudHMub25kZW1hbmQuY29tL3NhbWwyL2lkcC9zc28vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbSIgRm9yY2VBdXRobj0iZmFsc2UiIElzUGFzc2l2ZT0iZmFsc2UiIEFzc2VydGlvbkNvbnN1bWVyU2VydmljZUluZGV4PSIwIiB4bWxuczpzYW1scD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIj48c2FtbDpJc3N1ZXIgeG1sbnM6c2FtbD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiI+aHR0cHM6Ly9teTMwNDg3NC5zNGhhbmEub25kZW1hbmQuY29tPC9zYW1sOklzc3Vlcj48ZHM6U2lnbmF0dXJlIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48ZHM6U2lnbmVkSW5mbz48ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjxkczpTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjcnNhLXNoYTEiLz48ZHM6UmVmZXJlbmNlIFVSST0iI1NmYTE2M2UwMi01OGNhLTFlZWItYTlmZC1kZDBlMDIyOGE1OGMiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPkVJYTZUZ2JaM0VBUW5EUGVoM2M0WG1KRlRNYz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+UU1UelNRVmVVNTNwUHZ5aEhUeGtEU1liZVhWQjZTZzM2MWZLQ1oxMTZvT2FMSkN1Rm5HWWdGcXFhbDloZjhibFdxLzJ4QlMrL0h2Ngp3RW5oMDVIeHVGZWk2czB4VzBhNHU4MDdlYUhBVUVpR2psU3RPZ2daUnVyOTdlWnE4dVlMa3o2TGw5dHBQUVA4L0NlYjV5MkQ4cHVqCjBSTndVNkppZFdNM0UwaDJxRTBWSXlPWUhLU25JcWc3bkNwenVMOEVPVFdlcjA3bUdDbHd6UVNVdnBXK2p5S1RrNE56ZlRwMENJeGQKY1BwTG1EVHFXY2FBanBaeXJ5RjFqTklMMFRka1pNMWhtdi9NWEQ0Zlh5cnVJejV2dkF4dmlsblhoQmN6eVhvWjdGdXpQcGkyV0hsKwpFUTRIVy8xdWRTUHV0YjBZL0xyNllvUmFKZHgzbEp6ZXRLd3RLV1hRNnlpOElVMWJrejJ0eTQwWnRPWVM5cmliSlpSSHU5ZjZ3bE10CmRZUjNaSFRIVVJzUzhkdnFJaTVIbW5YVjZkN1hyR2h4eWY5cmlscmlYR096NTErVGVTdHBTQ0F3aE0rejRncXNHMXZ2WkNMcE9leHoKaVVTalRGUEU0YkhJc1VQV1lTWklJS3Qybk1MQTg4cC9wS2tlTjkxQnhYMEs3dGFqY1I5U2NZYkI8L2RzOlNpZ25hdHVyZVZhbHVlPjwvZHM6U2lnbmF0dXJlPjxzYW1scDpOYW1lSURQb2xpY3kgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCIvPjwvc2FtbHA6QXV0aG5SZXF1ZXN0Pg=="
, key: "RelayState"
value: "oucfarwteqsovycaoreeboazfdoddqeqssyavyc"
]",
            BODY_END,
            INLINE_URLS,
                "URL=https://saptrial.accounts.ondemand.com/saml2/idp/sso/saptrial.accounts.ondemand.com?SAMLRequest=jZLRTsIwFIZfZanXW7tuEGwGCUqMJKhE0AvvSlegZjudPR1Tn96BGLgBTHrVfH%2Fy%2FeecbFj7NTzrj1qjD4aI2nlj4dYC1qV2M%2B02RumX50mfrL2vUFAqXfP9ruTmO5JK2Ro8RhZyXUrII2VLirIsODV5RaXCSzQJps56q2xxYyA3sOqT2oGwEg0KkKVG4ZWYDR8mgkdMLH4hFPfz%2BTScPs3mJBi15gbkVvsgibLyzsjioiOivQCTYDzqk1lPaxX3GA8XKumEaZx0w143ZiHvSpXqlOcyTVoUsdZjQC%2FB9wlnPA5ZGvLenHVFp3086lynbyR41Q53wm0rEnyWBeD55tV%2BTHtYAPLzAfm3y0Mi%2FZ1PO56maaImiaxbUc5YTFlKW0iDujrQyQmaUXa9pXM0qysyyFoTsavtBv88kYweZbLHVn08mtrCqK%2FgzrpS%2BtPN4ije%2FZg8XO5QUQNWWpml0Tmhg4weH%2FTgBw%3D%3D&RelayState=arcb43ab0&SigAlg=http%3A%2F%2Fwww.w3.org%2F2001%2F04%2Fxmldsig-more%23rsa-sha256&Signature=UTfSbBwSOBAPJaK16tvAj7tb6SeUtLJuTraMIThPd%2FSwC1C29SEjzmz2SpdhMtYMvAUbiL4fLpW%2FteSN5icPooZ8sQNxNHCLrpkVkrzOlHWel6CUb5QX%2B1GqAdwVEL9dh8t5n7l9tINzTica8KiEmPkNiRoGlPCPxAOoOYhX%2BHOS5Ha4aIFSC0TmG2FlXd3GOGkEDc29neq1uP7EQfDugRsksjnnqVXx4R5CSLsFeiLhQCXlzDWp8H39io%2FjUuKPqYAZjEoFSBfqGeFRPiKEqjqOvG0uL%2F0bFmj5ACxzRECsNpb%2BPgqbHDL2OK2uBc9X1kxP2j6RkYQl9DLgdfFdTQ%3D%3D", "HEADER=Cache-Control:max-age=0", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("arwzjcavz_accounts_ondemand_", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.987);

        status = nsApi.ns_start_transaction("sso");
        status = nsApi.ns_web_url ("sso",
            "URL=https://accounts.sap.com/saml2/idp/sso",
            "METHOD=POST",
            "HEADER=Content-Length:2490",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Origin:https://saptrial.accounts.ondemand.com",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Content-Type:application/x-www-form-urlencoded",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            BODY_BEGIN,
                "[key: "utf8"
value: "\342\234\223"
, key: "authenticity_token"
value: "HovZOBzXzrBEoEE6KGwRdz42CdAd3PhtSThXSLg+Cqvav68Q9AmQ4fGcNTW3BrRWJZIyKx1XJH4h1sDHLbNJiw=="
, key: "SAMLRequest"
value: "PEF1dGhuUmVxdWVzdCB4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiB4bWxuczpuczI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIHhtbG5zOm5zMz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgeG1sbnM6bnM0PSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyMiIEFzc2VydGlvbkNvbnN1bWVyU2VydmljZVVSTD0iaHR0cHM6Ly9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20vc2FtbDIvaWRwL2Fjcy9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20iIERlc3RpbmF0aW9uPSJodHRwczovL2FjY291bnRzLnNhcC5jb20vc2FtbDIvaWRwL3NzbyIgSUQ9IlNhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1Njo1My42MDlaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLVBPU1QiIFZlcnNpb249IjIuMCI+PG5zMjpJc3N1ZXI+c2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tPC9uczI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz48ZHM6UmVmZXJlbmNlIFVSST0iI1NhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIvPjxkczpEaWdlc3RWYWx1ZT5kWXVSSHdXTkRwQkR2YlBMUVNmL3lsVmF0ampzakRuN3ZaNmNmaDJ3a1dRPTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5aUCtjeTRnTURsTTdqeXlEV3BGcGRxTWdUcGw2RzVvc3hJVmRmSGprUzJ3QVUyNmVMQUJjYzlXbmZiNTkrRGFFdDFOUjRxdlJMWW9IVkNVMzBQeEJlWnFkaHFFV2RSamdpQkpINGJMNkMrWk1VQWpnRXY1VHpQYW9mTHlPSVh0NiszQTViZGMrK0Y2L2dDZFNLOXBoUVh6MVNLeFFQM1g2ckxLVEZKeUpOZ0xrUTlRb0J1QUR1SUxyK2t6OHphQzlLVEN6MXpya2p3cXRKbm56cEVUdlZlTG1OejdGUzVKN3I2YUNUa043YlB5Zk1qbDMweXIxR01rRnVjVjZwWEhzSnZYNkM3N0c0TmtJb0JhUndSUFdKOCs2SW5kRDdSOThWUm1mK1JXUE9IZ09pUXV0ZndpSzQyK0R6ck9lb0Z1Z3gwNU9zZXZBUmxNcXAwNDNSL0ovRUE9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PC9kczpTaWduYXR1cmU+PE5hbWVJRFBvbGljeSBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIi8+PC9BdXRoblJlcXVlc3Q+"
, key: "RelayState"
value: "arc140a26"
]",
            BODY_END,
            INLINE_URLS,
                "URL=https://accounts.sap.com/universalui/assets/ids-e5ed7a2b74cd2c9f45ca91c664e3b34e1b1fc0f61be70f582556f0696f730bdf.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE,
                "URL=https://accounts.sap.com/universalui/assets/application-83b300a5431cef948be7755e94288d14c332fc6ee2986d8115a8f0f9337d3cc4.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE,
                "URL=https://accounts.sap.com/universalui/assets/login-384852ea058b1adc769ba64e693e01b4cd7bd5f427385eb7ee4af867456b711b.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE,
                "URL=https://accounts.sap.com/universalui/assets/cursor_focus-cba94039b8a27fc28e2ab27b0a264a1a6a92531ae21b701c31dfc2f48b82eea6.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE,
                "URL=https://accounts.sap.com/ui/public/cached/tenant/v/2/tenant_logo", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE,
                "URL=https://accounts.sap.com/universalui/assets/favicon-1c12ac7a083ca6c4ce998b405e396ffa8977858e43e1506a0de381b74f7a6804.ico", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=XSRF_COOKIE;JSESSIONID", END_INLINE
        );

        status = nsApi.ns_end_transaction("sso", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(15.963);

        //Page Auto splitted for Button '' Clicked by User
        status = nsApi.ns_start_transaction("sso_2");
        status = nsApi.ns_web_url ("sso_2",
            "URL=https://accounts.sap.com/saml2/idp/sso",
            "METHOD=POST",
            "HEADER=Content-Length:2877",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Origin:https://accounts.sap.com",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Content-Type:application/x-www-form-urlencoded",
            "HEADER=Sec-Fetch-User:?1",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=XSRF_COOKIE;JSESSIONID",
            BODY_BEGIN,
                "utf8=%E2%9C%93&authenticity_token=bNEG6IlbCYoYJLLU1mXm1J%2FEdXeVjUw9GYqg%2F3uMauXHnjWoeBXRIbL3zoeY%2FwprYUcEJpVjozhRBJ3P8cXFdQ%3D%3D&xsrfProtection=77%2B977%2B9ch7vv73vv70R77%2B977%2B9c%2B%2B%2FvR02Wzzvv70V77%2B9SO%2B%2Fve%2B%2Fve%2B%2Fve%2B%2Fve%2B%2FvUtqO21LQF86MTYxOTU5MzAxNDgxNw%3D%3D&method=POST&idpSSOEndpoint=https%3A%2F%2Faccounts.sap.com%2Fsaml2%2Fidp%2Fsso&SAMLRequest=PEF1dGhuUmVxdWVzdCB4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiB4bWxuczpuczI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIHhtbG5zOm5zMz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgeG1sbnM6bnM0PSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyMiIEFzc2VydGlvbkNvbnN1bWVyU2VydmljZVVSTD0iaHR0cHM6Ly9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20vc2FtbDIvaWRwL2Fjcy9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20iIERlc3RpbmF0aW9uPSJodHRwczovL2FjY291bnRzLnNhcC5jb20vc2FtbDIvaWRwL3NzbyIgSUQ9IlNhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1Njo1My42MDlaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLVBPU1QiIFZlcnNpb249IjIuMCI%2BPG5zMjpJc3N1ZXI%2Bc2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tPC9uczI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8%2BPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz48ZHM6UmVmZXJlbmNlIFVSST0iI1NhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM%2BPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIvPjxkczpEaWdlc3RWYWx1ZT5kWXVSSHdXTkRwQkR2YlBMUVNmL3lsVmF0ampzakRuN3ZaNmNmaDJ3a1dRPTwvZHM6RGlnZXN0VmFsdWU%2BPC9kczpSZWZlcmVuY2U%2BPC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5aUCtjeTRnTURsTTdqeXlEV3BGcGRxTWdUcGw2RzVvc3hJVmRmSGprUzJ3QVUyNmVMQUJjYzlXbmZiNTkrRGFFdDFOUjRxdlJMWW9IVkNVMzBQeEJlWnFkaHFFV2RSamdpQkpINGJMNkMrWk1VQWpnRXY1VHpQYW9mTHlPSVh0NiszQTViZGMrK0Y2L2dDZFNLOXBoUVh6MVNLeFFQM1g2ckxLVEZKeUpOZ0xrUTlRb0J1QUR1SUxyK2t6OHphQzlLVEN6MXpya2p3cXRKbm56cEVUdlZlTG1OejdGUzVKN3I2YUNUa043YlB5Zk1qbDMweXIxR01rRnVjVjZwWEhzSnZYNkM3N0c0TmtJb0JhUndSUFdKOCs2SW5kRDdSOThWUm1mK1JXUE9IZ09pUXV0ZndpSzQyK0R6ck9lb0Z1Z3gwNU9zZXZBUmxNcXAwNDNSL0ovRUE9PTwvZHM6U2lnbmF0dXJlVmFsdWU%2BPC9kczpTaWduYXR1cmU%2BPE5hbWVJRFBvbGljeSBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIi8%2BPC9BdXRoblJlcXVlc3Q%2B&RelayState=arc140a26&targetUrl=&sourceUrl=&org=&spId=554ca6bbe4b0c68c14258d47&spName=saptrial.accounts.ondemand.com&mobileSSOToken=&tfaToken=&css=&j_username=P2003858757",
            BODY_END,
            INLINE_URLS,
                "URL=https://accounts.sap.com/universalui/assets/ids-e5ed7a2b74cd2c9f45ca91c664e3b34e1b1fc0f61be70f582556f0696f730bdf.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("sso_2", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(18.735);

        //Page Auto splitted for Button '' Clicked by User
        status = nsApi.ns_start_transaction("sso_3");
        status = nsApi.ns_web_url ("sso_3",
            "URL=https://accounts.sap.com/saml2/idp/sso",
            "METHOD=POST",
            "HEADER=Content-Length:2897",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Origin:https://accounts.sap.com",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Content-Type:application/x-www-form-urlencoded",
            "HEADER=Sec-Fetch-User:?1",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=XSRF_COOKIE;authIdentifierDataTemporary;JSESSIONID",
            "REDIRECT=YES",
            "LOCATION=https://accounts.sap.com/saml2/idp/sso?redirect=true",
            BODY_BEGIN,
                "utf8=%E2%9C%93&authenticity_token=WY7aW4WsZkdwa6rxAQ%2FbNfcbcXHJifQ80vAB5x6WqguZDCPLQD4EZOzpIq4ohnC35mytuRap40d3ULd9n5eLIw%3D%3D&xsrfProtection=77%2B977%2B9ch7vv73vv70R77%2B977%2B9c%2B%2B%2FvR02Wzzvv70V77%2B9SO%2B%2Fve%2B%2Fve%2B%2Fve%2B%2Fve%2B%2FvUtqO21LQF86MTYxOTU5MzAxNDgxNw%3D%3D&method=POST&idpSSOEndpoint=https%3A%2F%2Faccounts.sap.com%2Fsaml2%2Fidp%2Fsso&SAMLRequest=PEF1dGhuUmVxdWVzdCB4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnByb3RvY29sIiB4bWxuczpuczI9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIHhtbG5zOm5zMz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgeG1sbnM6bnM0PSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyMiIEFzc2VydGlvbkNvbnN1bWVyU2VydmljZVVSTD0iaHR0cHM6Ly9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20vc2FtbDIvaWRwL2Fjcy9zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb20iIERlc3RpbmF0aW9uPSJodHRwczovL2FjY291bnRzLnNhcC5jb20vc2FtbDIvaWRwL3NzbyIgSUQ9IlNhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1Njo1My42MDlaIiBQcm90b2NvbEJpbmRpbmc9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpiaW5kaW5nczpIVFRQLVBPU1QiIFZlcnNpb249IjIuMCI%2BPG5zMjpJc3N1ZXI%2Bc2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tPC9uczI6SXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8%2BPGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiLz48ZHM6UmVmZXJlbmNlIFVSST0iI1NhNWEwMTI2Yi03YWUyLTQ5MGUtOGJiYi0xODkyOTQyMTM4OGUiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM%2BPGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiIvPjxkczpEaWdlc3RWYWx1ZT5kWXVSSHdXTkRwQkR2YlBMUVNmL3lsVmF0ampzakRuN3ZaNmNmaDJ3a1dRPTwvZHM6RGlnZXN0VmFsdWU%2BPC9kczpSZWZlcmVuY2U%2BPC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5aUCtjeTRnTURsTTdqeXlEV3BGcGRxTWdUcGw2RzVvc3hJVmRmSGprUzJ3QVUyNmVMQUJjYzlXbmZiNTkrRGFFdDFOUjRxdlJMWW9IVkNVMzBQeEJlWnFkaHFFV2RSamdpQkpINGJMNkMrWk1VQWpnRXY1VHpQYW9mTHlPSVh0NiszQTViZGMrK0Y2L2dDZFNLOXBoUVh6MVNLeFFQM1g2ckxLVEZKeUpOZ0xrUTlRb0J1QUR1SUxyK2t6OHphQzlLVEN6MXpya2p3cXRKbm56cEVUdlZlTG1OejdGUzVKN3I2YUNUa043YlB5Zk1qbDMweXIxR01rRnVjVjZwWEhzSnZYNkM3N0c0TmtJb0JhUndSUFdKOCs2SW5kRDdSOThWUm1mK1JXUE9IZ09pUXV0ZndpSzQyK0R6ck9lb0Z1Z3gwNU9zZXZBUmxNcXAwNDNSL0ovRUE9PTwvZHM6U2lnbmF0dXJlVmFsdWU%2BPC9kczpTaWduYXR1cmU%2BPE5hbWVJRFBvbGljeSBGb3JtYXQ9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjEuMTpuYW1laWQtZm9ybWF0OnVuc3BlY2lmaWVkIi8%2BPC9BdXRoblJlcXVlc3Q%2B&RelayState=arc140a26&targetUrl=&sourceUrl=&org=&spId=554ca6bbe4b0c68c14258d47&spName=saptrial.accounts.ondemand.com&mobileSSOToken=&tfaToken=&css=&j_username=P2003858757&j_password=saptest12%2B",
            BODY_END,
            INLINE_URLS,
                "URL=https://accounts.sap.com/saml2/idp/sso?redirect=true", "HEADER=Cache-Control:max-age=0", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Sec-Fetch-User:?1", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=authIdentifierDataTemporary;IDP_USER;IDP_J_COOKIE;IDP_SESSION_MARKER_accounts;XSRF_COOKIE;authIdentifierData", END_INLINE,
            "URL=https://saptrial.accounts.ondemand.com/saml2/idp/acs/saptrial.accounts.ondemand.com", "METHOD=POST", "HEADER=Content-Length:6967", "HEADER=Cache-Control:max-age=0", "HEADER=Origin:https://accounts.sap.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=arc140a26;XSRF_COOKIE;JSESSIONID",
                BODY_BEGIN,
                "[key: "utf8"
value: "\342\234\223"
, key: "authenticity_token"
value: "2De+dbmY5usApMUiYeGwhG4FIV60CR7EoPSQJ2rRdJreTt1jtNgj+RNbb3RvMIjdF3CwGHhe6NKjvl1H81nFCQ=="
, key: "SAMLResponse"
value: "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiIgeG1sbnM6bnMzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIiB4bWxuczpuczQ9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jIyIgRGVzdGluYXRpb249Imh0dHBzOi8vc2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tL3NhbWwyL2lkcC9hY3Mvc2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tIiBJRD0iUkVTLVNTTy04NWViYTcwYy1iYmM4LTQ2YWMtYjcyYy0yMjY2MDllNWQwOTEiIEluUmVzcG9uc2VUbz0iU2E1YTAxMjZiLTdhZTItNDkwZS04YmJiLTE4OTI5NDIxMzg4ZSIgSXNzdWVJbnN0YW50PSIyMDIxLTA0LTI4VDA2OjU3OjMxLjA0NFoiIFZlcnNpb249IjIuMCI+PG5zMjpJc3N1ZXI+aHR0cHM6Ly9hY2NvdW50cy5zYXAuY29tPC9uczI6SXNzdWVyPjxTdGF0dXM+PFN0YXR1c0NvZGUgVmFsdWU9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpzdGF0dXM6U3VjY2VzcyIvPjwvU3RhdHVzPjxBc3NlcnRpb24geG1sbnM9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDphc3NlcnRpb24iIHhtbG5zOm5zMj0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyIgeG1sbnM6bnMzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyMiIElEPSJBLWM5ZGMxYzZhLTc5YTItNDg5NC05YTUwLWFiZmY0OGY5MzZlZiIgSXNzdWVJbnN0YW50PSIyMDIxLTA0LTI4VDA2OjU3OjMxLjA0NFoiIFZlcnNpb249IjIuMCI+PElzc3Vlcj5odHRwczovL2FjY291bnRzLnNhcC5jb208L0lzc3Vlcj48ZHM6U2lnbmF0dXJlIHhtbG5zOmRzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48ZHM6U2lnbmVkSW5mbz48ZHM6Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjxkczpTaWduYXR1cmVNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjcnNhLXNoYTEiLz48ZHM6UmVmZXJlbmNlIFVSST0iI0EtYzlkYzFjNmEtNzlhMi00ODk0LTlhNTAtYWJmZjQ4ZjkzNmVmIj48ZHM6VHJhbnNmb3Jtcz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI2VudmVsb3BlZC1zaWduYXR1cmUiLz48ZHM6VHJhbnNmb3JtIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PC9kczpUcmFuc2Zvcm1zPjxkczpEaWdlc3RNZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjc2hhMSIvPjxkczpEaWdlc3RWYWx1ZT5aMkZnRXBoVEkzbUlWMCtlclAyeWZESUVMa1U9PC9kczpEaWdlc3RWYWx1ZT48L2RzOlJlZmVyZW5jZT48L2RzOlNpZ25lZEluZm8+PGRzOlNpZ25hdHVyZVZhbHVlPm1TdzVHcUhlbjJIejdLYWM0WDBnZkRoUjVFK2RXRVVWQmhFS3ZvKzFKd2xKVVR4cEJlQ1lnNGJNVnNhdnFtdGtVak5HUzduTHVTa0k5M1QvWks3YzFaZDlSTVRzSXBlQnBwOUVxNmZpcDVZY1lGRlV5REFDZ0xCelNPM1FNbXltQ1JQdFFucEZ2RHBZMlZFWVcrWHJtbTdZWTVvY1RvTGovT1ZHL3ZCUkc5UzJUbkN2QU44Y21IbUczUHdBTzl1WlhLSFl4M3lBNFpVUVNQM3FTZlBnSnF3OXRxcWVPejJDeW9wdjRYVnlTK1R4QUNDWDhMclp3NEltaERFSkFnc3Vrb3FuVHVFRGRvLzFFSElMMHAvYWRWM2trTzR4MWRBbXVaMk1VYzVzUWJHSVdhVDM3NnJLOEFJSUNwZlEzYXorVFVHc2lwM3hOZXVzbm5MTGRDb2lqZz09PC9kczpTaWduYXR1cmVWYWx1ZT48ZHM6S2V5SW5mbz48ZHM6WDUwOURhdGE+PGRzOlg1MDlDZXJ0aWZpY2F0ZT5NSUlDOURDQ0FkeWdBd0lCQVFJR0FXK2tLeHI5TUEwR0NTcUdTSWIzRFFFQkN3VUFNRGt4Q3pBSkJnTlZCQVlUQWtSRk1ROHdEUVlEVlFRS0V3WlRRVkF0VTBVeEdUQVhCZ05WQkFNVEVHRmpZMjkxYm5SekxuTmhjQzVqYjIwd0hoY05NakF3TVRFME1UTXdPREEyV2hjTk16QXdNVEUwTVRNd09EQTJXakE1TVFzd0NRWURWUVFHRXdKRVJURVBNQTBHQTFVRUNoTUdVMEZRTFZORk1Sa3dGd1lEVlFRREV4QmhZMk52ZFc1MGN5NXpZWEF1WTI5dE1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBdDVUYkgzSFpoRTQ0QStsVE0xbFI3eWZ3emMxVWVnT1pGV1R5dlZJSUJvcHMrMXhad05EdDdra1V3cWpqYTUwVEkwMGZJU0NBVlFRMFRES2lWWnQvaktFL1R0MjQzQ3dBNVk1cDBZeFE1RmNMc2xOVzNHZzVFM2VFaFBPamdiSmFhTG82OUYyeDRpMVZJd2FINGhVNHhZL0VLakZjQ3p1RkZkMEJMN0ZCODFWMGdKbmpRSFRhVDFoaGJuWTdJOERXS24wU204cDA4ZktPbW52SitXRDBPTC9Dek51VlFoOUNUeWRvNWM2ZHk0STdYb0ZjOGUwdVVubnJ0VDhESFRmSnF1MlJaVE5JNnBEWEFjdncwUERPeEVVdkZDaW1SSThSL1J2UE1LeUxUUzZGcDZTTDMrb0FWVVZTaHhXYVRKeG1NWGRLN25wU2cvSmZyMUU4azArclB3SURBUUFCZ2dJQUFEQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFjYndmR2dRSFpYR1NUVi9vTkp6UmRYdEpBNzBYcUc2RzMyNXNySmZMdGV4dlBXd1lkSzZtQldXWHB5dFROR1FVVWZ4L0cxdVhSZDVZVGVwNXNXTWN0RVVTZjhOaHRUeGMxNGUzVFcwTDcvKzE2SjNncnVadHlLeTkxcEpkL2ZQNlNjVEU4UVljTnArajU1VElzd2FoNnFHYld2anlKcC9uMytLb012YWtNeFkwbEJuTGtNTjFwVHU0UWVJaTZtWEkvNUtUeFQrMGIvMW54cFIveit0aWVyTlRmQzMxV0g0M093QWNaQzBVL1EvdG5lRWhacmUwdkRKR1dNOWVEK2VsalgybTZMd0p4d1Q2SWp2ZENBZTRnbVV2a2pqUXdYQVN2M1U5SXh6TVIycU9USGpSMTQ3emNHdlhqZTA2SmI4Yjk0R3BLZWJuTEpWa3hJU3AzN0oyMWc9PTwvZHM6WDUwOUNlcnRpZmljYXRlPjwvZHM6WDUwOURhdGE+PGRzOktleVZhbHVlPjxkczpSU0FLZXlWYWx1ZT48ZHM6TW9kdWx1cz50NVRiSDNIWmhFNDRBK2xUTTFsUjd5Znd6YzFVZWdPWkZXVHl2VklJQm9wcysxeFp3TkR0N2trVXdxamphNTBUSTAwZklTQ0FWUVEwVERLaVZadC9qS0UvVHQyNDNDd0E1WTVwMFl4UTVGY0xzbE5XM0dnNUUzZUVoUE9qZ2JKYWFMbzY5RjJ4NGkxVkl3YUg0aFU0eFkvRUtqRmNDenVGRmQwQkw3RkI4MVYwZ0pualFIVGFUMWhoYm5ZN0k4RFdLbjBTbThwMDhmS09tbnZKK1dEME9ML0N6TnVWUWg5Q1R5ZG81YzZkeTRJN1hvRmM4ZTB1VW5ucnRUOERIVGZKcXUyUlpUTkk2cERYQWN2dzBQRE94RVV2RkNpbVJJOFIvUnZQTUt5TFRTNkZwNlNMMytvQVZVVlNoeFdhVEp4bU1YZEs3bnBTZy9KZnIxRThrMCtyUHc9PTwvZHM6TW9kdWx1cz48ZHM6RXhwb25lbnQ+QVFBQjwvZHM6RXhwb25lbnQ+PC9kczpSU0FLZXlWYWx1ZT48L2RzOktleVZhbHVlPjwvZHM6S2V5SW5mbz48L2RzOlNpZ25hdHVyZT48U3ViamVjdD48TmFtZUlEIEZvcm1hdD0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6MS4xOm5hbWVpZC1mb3JtYXQ6dW5zcGVjaWZpZWQiPlAyMDAzODU4NzU3PC9OYW1lSUQ+PFN1YmplY3RDb25maXJtYXRpb24gTWV0aG9kPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6Y206YmVhcmVyIj48U3ViamVjdENvbmZpcm1hdGlvbkRhdGEgSW5SZXNwb25zZVRvPSJTYTVhMDEyNmItN2FlMi00OTBlLThiYmItMTg5Mjk0MjEzODhlIiBOb3RPbk9yQWZ0ZXI9IjIwMjEtMDQtMjhUMDc6MDc6MzEuMDQ0WiIgUmVjaXBpZW50PSJodHRwczovL3NhcHRyaWFsLmFjY291bnRzLm9uZGVtYW5kLmNvbS9zYW1sMi9pZHAvYWNzL3NhcHRyaWFsLmFjY291bnRzLm9uZGVtYW5kLmNvbSIvPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q+PENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDIxLTA0LTI4VDA2OjUyOjMxLjA0NFoiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMS4wNDRaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U+c2FwdHJpYWwuYWNjb3VudHMub25kZW1hbmQuY29tPC9BdWRpZW5jZT48L0F1ZGllbmNlUmVzdHJpY3Rpb24+PC9Db25kaXRpb25zPjxBdXRoblN0YXRlbWVudCBBdXRobkluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTc6MzEuMDQ0WiIgU2Vzc2lvbkluZGV4PSJTLVNQLTFiZDFiMGMyLTQ1MDgtNGUxYy1iOGU5LTFkOGM0Y2RhYzM4NCIgU2Vzc2lvbk5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQxODo1NzozMS4wNDRaIj48QXV0aG5Db250ZXh0PjxBdXRobkNvbnRleHRDbGFzc1JlZj51cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZFByb3RlY3RlZFRyYW5zcG9ydDwvQXV0aG5Db250ZXh0Q2xhc3NSZWY+PC9BdXRobkNvbnRleHQ+PC9BdXRoblN0YXRlbWVudD48QXR0cmlidXRlU3RhdGVtZW50PjxBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIj48QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5KYWluPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48QXR0cmlidXRlIE5hbWU9ImZpcnN0X25hbWUiPjxBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPk5lZXJhajwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PEF0dHJpYnV0ZSBOYW1lPSJlbWFpbCI+PEF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+bmVlcmFqQGNhdmlzc29uLmNvbTwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PC9BdHRyaWJ1dGVTdGF0ZW1lbnQ+PC9Bc3NlcnRpb24+PC9SZXNwb25zZT4="
, key: "RelayState"
value: "arc140a26"
]",
            BODY_END,
             END_INLINE,
            "URL=https://arwzjcavz.accounts.ondemand.com/saml2/idp/acs/arwzjcavz.accounts.ondemand.com", "METHOD=POST", "HEADER=Content-Length:7145", "HEADER=Cache-Control:max-age=0", "HEADER=Origin:https://saptrial.accounts.ondemand.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=arcb43ab0",
                BODY_BEGIN,
                "[key: "utf8"
value: "\342\234\223"
, key: "authenticity_token"
value: "BQaAgmkiefPDstRvfrXX/E1kWPYIcPCA5rBu+UnhakaFdY0qdJrHd79GT/y730mzgsirdTj0WWgm7sFRGBV1EQ=="
, key: "SAMLResponse"
value: "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiIgeG1sbnM6bnMzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIiB4bWxuczpuczQ9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jIyIgRGVzdGluYXRpb249Imh0dHBzOi8vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbS9zYW1sMi9pZHAvYWNzL2Fyd3pqY2F2ei5hY2NvdW50cy5vbmRlbWFuZC5jb20iIElEPSJSRVMtU1NPLTE4YzhkYTEwLThlZWMtNGUyZC1iNzhhLTVkYzk2M2U0OTVlYyIgSW5SZXNwb25zZVRvPSJTOGVlYzE4MDItYmMzNS00MTM2LTg2MTAtMjZhYzRlNDJkYTQzIiBJc3N1ZUluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTc6MzEuOTg4WiIgVmVyc2lvbj0iMi4wIj48bnMyOklzc3Vlcj5zYXB0cmlhbC5hY2NvdW50cy5vbmRlbWFuZC5jb208L25zMjpJc3N1ZXI+PFN0YXR1cz48U3RhdHVzQ29kZSBWYWx1ZT0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOnN0YXR1czpTdWNjZXNzIi8+PC9TdGF0dXM+PEFzc2VydGlvbiB4bWxucz0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiIgeG1sbnM6bnMyPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIiB4bWxuczpuczM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jIyIgSUQ9IkEtNDY3ZjBlMTctOGE4Zi00ODYyLWI1MDgtODg0YmNhMzg1YzExIiBJc3N1ZUluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTc6MzEuOTg4WiIgVmVyc2lvbj0iMi4wIj48SXNzdWVyPnNhcHRyaWFsLmFjY291bnRzLm9uZGVtYW5kLmNvbTwvSXNzdWVyPjxkczpTaWduYXR1cmUgeG1sbnM6ZHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxkczpTaWduZWRJbmZvPjxkczpDYW5vbmljYWxpemF0aW9uTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8xMC94bWwtZXhjLWMxNG4jIi8+PGRzOlNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNyc2Etc2hhMSIvPjxkczpSZWZlcmVuY2UgVVJJPSIjQS00NjdmMGUxNy04YThmLTQ4NjItYjUwOC04ODRiY2EzODVjMTEiPjxkczpUcmFuc2Zvcm1zPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSIvPjxkczpUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48L2RzOlRyYW5zZm9ybXM+PGRzOkRpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNzaGExIi8+PGRzOkRpZ2VzdFZhbHVlPjR4bndYeVIwVVBjdWdJdkdjckEvc25CNVlndz08L2RzOkRpZ2VzdFZhbHVlPjwvZHM6UmVmZXJlbmNlPjwvZHM6U2lnbmVkSW5mbz48ZHM6U2lnbmF0dXJlVmFsdWU+UGJhZ3k0MFQwM2psNzlLekEyWUVFS1AxL2pMQUxzbTJwYWM0UUVKTlNKb0lUOXZuaVB6ckRDcmZxbXJ1ZVhtYTVUV2VDZ3l5VHpKT2tuc29hTEEvcy8yWncvcVFGQVV0eVdyVTdTV24rSTA0MkFNM2ZGWFpuOWx0WUZqSXJ2SkpvdnhZSHNmRlhkbEZzVEs5WDU2UkFIbk5kekxHcEVMVGpKWXZiU0lVQ0FRTnJNT1lLUTl3aTdXbHFOeHFFQUhUNmNXUU1LaE9zcXJKNExFcyt6YjU2WEw4eWo1MitiYkpSUWszMTVZRy9CRnVDeGVjVUhSOTNDc2pUWlpMUUloRk9kY0tBRmxjUWVMQm9vWGhwY1ZJTEhGUjZvMzdqS055T3FESVBGT1dEWE00V2hkT1pOU09vK3FsOFZUSGFFd1dhTW9OTDVjdUNMV0dZS0FzOE94LzVBPT08L2RzOlNpZ25hdHVyZVZhbHVlPjxkczpLZXlJbmZvPjxkczpYNTA5RGF0YT48ZHM6WDUwOUNlcnRpZmljYXRlPk1JSURFRENDQWZpZ0F3SUJBUUlHQVV6Y2toMUhNQTBHQ1NxR1NJYjNEUUVCQlFVQU1FY3hDekFKQmdOVkJBWVRBa1JGTVE4d0RRWURWUVFLRXdaVFFWQXRVMFV4SnpBbEJnTlZCQU1USG5OaGNIUnlhV0ZzTG1GalkyOTFiblJ6TG05dVpHVnRZVzVrTG1OdmJUQWVGdzB4TlRBME1qRXhOVEl4TkRWYUZ3MHlOVEEwTWpFeE5USXhORFZhTUVjeEN6QUpCZ05WQkFZVEFrUkZNUTh3RFFZRFZRUUtFd1pUUVZBdFUwVXhKekFsQmdOVkJBTVRIbk5oY0hSeWFXRnNMbUZqWTI5MWJuUnpMbTl1WkdWdFlXNWtMbU52YlRDQ0FTSXdEUVlKS29aSWh2Y05BUUVCQlFBRGdnRVBBRENDQVFvQ2dnRUJBSWFBSnR2dVFPRVdUVnpNR3drTDdIQUZNemxRZExoZXFXMmZjYTFhNlJpb2p4YVZROTZYempIRFhod1huRW52WjVncUdCeUZzSTNPdDU2Q3lWVWgxdGQxNHB4VG1ZZjR2WFh2TXpuUmpaR3UvQW1RWCs5dmNRSG03ZDNYYjZ0QU1CUkF3Zk1zMnppYTlxTjdaWUNIajQ0QkFhUkZkVXpQSHRnVzc1cGoveVllNmxNUlg4VjNkRDBiVjN6d2dPdGNJQXdTMlhVKzFNZ3JOV3hFL3hjVks0RE82UEFkVjQ4eTRGRzB1R3NXanI0dkRjblcwZ2g0WjZQeVhqWXJucG1BdTU2MFNDLzNVK3hadUxrUlYzQW1LQUVydEQ1OUlSYkZVc3NVWjV1MTNCWVJuZmN4eU9DVCtpcWJqY2lsQ1BvbEFydEF5S0lQNUlSdTV0MXFOWEJLbXJjQ0F3RUFBWUlDQUFBd0RRWUpLb1pJaHZjTkFRRUZCUUFEZ2dFQkFFK2J0RFVGcVUwK0VRNVVCK1F6akVnVUtNNGlUSXNydGVSQ0Y4T21IUFAyM3VqMmZIcVl3QkZnb05RVHFSTXdwN1JvakdnaTBIRDM4VlNNTVAxcm9sMVI1TUcvQ1lyL3FnbS9LWk91WDBMci9xTkIxZXA5MWpGRnZJNUVQL1NubklWTnRRSjc2UEcxekRXMkRqUlJ2SUs0emMvbVduWk90a2d0UXVNY3BETUY2QUpoY2ZVcFhZWnZXb1FXNGNhQkpvL0kweWRSNFpTMHV3b0hpMlFJNVRVWlA5c0ZEODNFZFVFZFFiU2NVUWplYnBKRUVzKzNxM1hrV3NaQ1FWMVhzcFRHdnlFbUtOYzZxV05peHFiaFgwdVU0QUhySGl6UjJPbEZzOUhoK1I2UzM3SithemNrWmx2cGNVc3p0Y045TUNoWTZBWXpRWlB1OUdpOUx4MGRmczQ9PC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48ZHM6S2V5VmFsdWU+PGRzOlJTQUtleVZhbHVlPjxkczpNb2R1bHVzPmhvQW0yKzVBNFJaTlhNd2JDUXZzY0FVek9WQjB1RjZwYlo5eHJWcnBHS2lQRnBWRDNwZk9NY05lSEJlY1NlOW5tQ29ZSElXd2pjNjNub0xKVlNIVzEzWGluRk9aaC9pOWRlOHpPZEdOa2E3OENaQmY3Mjl4QWVidDNkZHZxMEF3RkVEQjh5emJPSnIybzN0bGdJZVBqZ0VCcEVWMVRNOGUyQmJ2bW1QL0poN3FVeEZmeFhkMFBSdFhmUENBNjF3Z0RCTFpkVDdVeUNzMWJFVC9GeFVyZ003bzhCMVhqekxnVWJTNGF4YU92aThOeWRiU0NIaG5vL0plTml1ZW1ZQzduclJJTC9kVDdGbTR1UkZYY0NZb0FTdTBQbjBoRnNWU3l4Um5tN1hjRmhHZDl6SEk0SlA2S3B1TnlLVUkraVVDdTBESW9nL2toRzdtM1dvMWNFcWF0dz09PC9kczpNb2R1bHVzPjxkczpFeHBvbmVudD5BUUFCPC9kczpFeHBvbmVudD48L2RzOlJTQUtleVZhbHVlPjwvZHM6S2V5VmFsdWU+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxTdWJqZWN0PjxOYW1lSUQgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCI+UDIwMDM4NTg3NTc8L05hbWVJRD48U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPjxTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBJblJlc3BvbnNlVG89IlM4ZWVjMTgwMi1iYzM1LTQxMzYtODYxMC0yNmFjNGU0MmRhNDMiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMS45ODhaIiBSZWNpcGllbnQ9Imh0dHBzOi8vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbS9zYW1sMi9pZHAvYWNzL2Fyd3pqY2F2ei5hY2NvdW50cy5vbmRlbWFuZC5jb20iLz48L1N1YmplY3RDb25maXJtYXRpb24+PC9TdWJqZWN0PjxDb25kaXRpb25zIE5vdEJlZm9yZT0iMjAyMS0wNC0yOFQwNjo1MjozMS45ODhaIiBOb3RPbk9yQWZ0ZXI9IjIwMjEtMDQtMjhUMDc6MDc6MzEuOTg4WiI+PEF1ZGllbmNlUmVzdHJpY3Rpb24+PEF1ZGllbmNlPmh0dHBzOi8vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbTwvQXVkaWVuY2U+PC9BdWRpZW5jZVJlc3RyaWN0aW9uPjwvQ29uZGl0aW9ucz48QXV0aG5TdGF0ZW1lbnQgQXV0aG5JbnN0YW50PSIyMDIxLTA0LTI4VDA2OjU3OjMxLjk4OFoiIFNlc3Npb25JbmRleD0iUy1TUC04YWU0NTA2MC0wYmUxLTQxNjktOWY5Ny1iMWQ3M2IxMDZjNjQiIFNlc3Npb25Ob3RPbk9yQWZ0ZXI9IjIwMjEtMDQtMjhUMTg6NTc6MzEuOTg4WiI+PEF1dGhuQ29udGV4dD48QXV0aG5Db250ZXh0Q2xhc3NSZWY+dXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFjOmNsYXNzZXM6UGFzc3dvcmRQcm90ZWN0ZWRUcmFuc3BvcnQ8L0F1dGhuQ29udGV4dENsYXNzUmVmPjxBdXRoZW50aWNhdGluZ0F1dGhvcml0eT5odHRwczovL2FjY291bnRzLnNhcC5jb208L0F1dGhlbnRpY2F0aW5nQXV0aG9yaXR5PjwvQXV0aG5Db250ZXh0PjwvQXV0aG5TdGF0ZW1lbnQ+PEF0dHJpYnV0ZVN0YXRlbWVudD48QXR0cmlidXRlIE5hbWU9Imxhc3RfbmFtZSI+PEF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+SmFpbjwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PEF0dHJpYnV0ZSBOYW1lPSJmaXJzdF9uYW1lIj48QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5OZWVyYWo8L0F0dHJpYnV0ZVZhbHVlPjwvQXR0cmlidXRlPjxBdHRyaWJ1dGUgTmFtZT0iZW1haWwiPjxBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPm5lZXJhakBjYXZpc3Nvbi5jb208L0F0dHJpYnV0ZVZhbHVlPjwvQXR0cmlidXRlPjwvQXR0cmlidXRlU3RhdGVtZW50PjwvQXNzZXJ0aW9uPjwvUmVzcG9uc2U+"
, key: "RelayState"
value: "arcb43ab0"
]",
            BODY_END,
             END_INLINE,
            "URL=https://my304874.s4hana.ondemand.com/sap/saml2/sp/acs/100", "METHOD=POST", "HEADER=Content-Length:7153", "HEADER=Cache-Control:max-age=0", "HEADER=Origin:https://arwzjcavz.accounts.ondemand.com", "HEADER=Upgrade-Insecure-Requests:1", "HEADER=Content-Type:application/x-www-form-urlencoded", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:navigate", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=oucfarwteqsovycaoreeboazfdoddqeqssyavyc;sap-usercontext;oucfarwteqsovycaoreeboazfdoddqeqssyavyc_anchor",
                BODY_BEGIN,
                "[key: "utf8"
value: "\342\234\223"
, key: "authenticity_token"
value: "4ZchHx1Q9akZuikaUnpxhCAKo/o4ZVx9Hs747AFyEEKbjbZbUHW+00WmcTgYkFBjJQdS41lKrG8ayxvefj44tA=="
, key: "SAMLResponse"
value: "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiIgeG1sbnM6bnMzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIiB4bWxuczpuczQ9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jIyIgRGVzdGluYXRpb249Imh0dHBzOi8vbXkzMDQ4NzQuczRoYW5hLm9uZGVtYW5kLmNvbS9zYXAvc2FtbDIvc3AvYWNzLzEwMCIgSUQ9IlJFUy1TU08tMmEzYmM1YmUtMTA5Yi00MzlhLTg0ZGUtYTg4ZmMzNDk5YWUyIiBJblJlc3BvbnNlVG89IlNmYTE2M2UwMi01OGNhLTFlZWItYTlmZC1kZDBlMDIyOGE1OGMiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1NzozMi44MzVaIiBWZXJzaW9uPSIyLjAiPjxuczI6SXNzdWVyPmh0dHBzOi8vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbTwvbnMyOklzc3Vlcj48U3RhdHVzPjxTdGF0dXNDb2RlIFZhbHVlPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6c3RhdHVzOlN1Y2Nlc3MiLz48L1N0YXR1cz48QXNzZXJ0aW9uIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiB4bWxuczpuczI9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiIHhtbG5zOm5zMz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjIiBJRD0iQS0yNmQ4Zjg1Ny1kMzg4LTQ4N2MtOTc4MS1jZmViNjQ0MWY3NmUiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1NzozMi44MzVaIiBWZXJzaW9uPSIyLjAiPjxJc3N1ZXI+aHR0cHM6Ly9hcnd6amNhdnouYWNjb3VudHMub25kZW1hbmQuY29tPC9Jc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNBLTI2ZDhmODU3LWQzODgtNDg3Yy05NzgxLWNmZWI2NDQxZjc2ZSI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+eHB5eG1HYlFrUkJkN3lOVEF5alNiUm5uU2F3PTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5kSmlaL282dXhySzBkY1E0a0pnd0VBbEtWam5lZnBpNUN0Q1ZOLzVGVVU4ZVFWeWhaazhOTi9tUThlYXYrS1JubUt5NEpLL0wwOGlmYk42Zlc2c1V1eEQrMGZqeHBvU21jczI3U3BwZTN2cS9VM052WldrLzZOUEZZKzlJaTRCdHBwYm1zQXBUekhZb2I0QW5JbDhPYWVoMmF6WTJJZmVaTFI4ckxkdGNwd3E3NEdtNEFVKzhoeXhoUm9yMW9WMFBNYm9GSUdjdWdjUHhlTzdlRWF6SUYzY0R1blBHa0NFZndmN2ZlaitCeVRua0lMclhOMlgxeTI5RlUyNDFiTUgxUThXc05CSDNGNkJ4Nkd5TGNwOUpYTTI2aGR5NzBvdWw5eU01TWs5ZXlhNjJMUWtZd3Q2N0xmcTZyc1JMZS94Ty9NVm9JSnZLUzF3WTdYUHMxS0pWWHc9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJREVqQ0NBZnFnQXdJQkFRSUdBWFRTWkh6N01BMEdDU3FHU0liM0RRRUJDd1VBTUVneEN6QUpCZ05WQkFZVEFrUkZNUTh3RFFZRFZRUUtFd1pUUVZBdFUwVXhLREFtQmdOVkJBTVRIMkZ5ZDNwcVkyRjJlaTVoWTJOdmRXNTBjeTV2Ym1SbGJXRnVaQzVqYjIwd0hoY05NakF3T1RJNE1ERTBOekUxV2hjTk16QXdPVEk0TURFME56RTFXakJJTVFzd0NRWURWUVFHRXdKRVJURVBNQTBHQTFVRUNoTUdVMEZRTFZORk1TZ3dKZ1lEVlFRREV4OWhjbmQ2YW1OaGRub3VZV05qYjNWdWRITXViMjVrWlcxaGJtUXVZMjl0TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFqRkVJdm40TW5YTG44c0ZTREZuSHl6RkMwd2lnWVEzS09SSjFJU1oxM3hGcjhUZVNiWXNOZDJ0L1ZMRzR4Rlo2RTRIUkZRTmZaSGttTGFxVWx3bnBESy9yVC9yUlRyWnU5WGVBVTVyTWhSWWdzL0VYSHlIYlI4RElkK3lWZklIbkxkVlR2Z3E3VE1jU0pqYUxrOWcwaXgvY1lLcXpFWXZIZWc4MDNjdVpLZTdHd0VRcmRKZVVFVTBTZ0FSRmErU2JyU3Rwa3FFcGNpNHRQaEo3WjFsUzlWaU8zZHBGK3p5Y24yWVVaOHhGLzVhMklVNW1UVTcrYkh0N3YyeVdoRnprY3lEcFppcCtSNzJ5aHcxaGdFaENoMEQwb2ZLU0lwck94SXRhMVVDYjhFcFRGd29YYTR6NWhYaWpxRi9OZVQyQ29PZEt0Vk8zUTRRRjhnYVl6RXV5VHdJREFRQUJnZ0lBQURBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQURBU0pqSTR1cC9KUWVDWEptQVVSc1EzZWJLNjlHdFo3Qi9YVmh6WVI3RTJ2eGJWNjhwRXJFR1kzdlFFZGVNTmdGWUZ0MSt0REV3V2NJQmRSakF1VWIyUHdpREtLT2lPZXRzbGlVUG1rTitpL2laMkR2cUV6RUlQQm5acGpKWmt4T1NqSDNzTUtGWVQ2ajdkMWthMkJaVDdsd2hZRWtya1N5NkxvalNwYUZFaWRiU3JpcE9HNWJ3c3FpbVVIQUNUTUVVaWJPTHV6R2ZTTGs1WUpFVnRySytkeFNsQXVFZDVsVzRxU043QjMwWnBTT3ZZV2RVc2xFZUQyeVhLZ3BNS2R2Z3FzS2IxSDBzTk5vemk5QzJ2Z1ZsaFVKQUdKVTYxcWdPTTM4SkhCbC8wSE9HWFlnWG9hb0JQdW5Jc2x2K0tydWpmdUFPVVRNTVdwdmt3M0FZR1JUQT09PC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48ZHM6S2V5VmFsdWU+PGRzOlJTQUtleVZhbHVlPjxkczpNb2R1bHVzPmpGRUl2bjRNblhMbjhzRlNERm5IeXpGQzB3aWdZUTNLT1JKMUlTWjEzeEZyOFRlU2JZc05kMnQvVkxHNHhGWjZFNEhSRlFOZlpIa21MYXFVbHducERLL3JUL3JSVHJadTlYZUFVNXJNaFJZZ3MvRVhIeUhiUjhESWQreVZmSUhuTGRWVHZncTdUTWNTSmphTGs5ZzBpeC9jWUtxekVZdkhlZzgwM2N1WktlN0d3RVFyZEplVUVVMFNnQVJGYStTYnJTdHBrcUVwY2k0dFBoSjdaMWxTOVZpTzNkcEYrenljbjJZVVo4eEYvNWEySVU1bVRVNytiSHQ3djJ5V2hGemtjeURwWmlwK1I3MnlodzFoZ0VoQ2gwRDBvZktTSXByT3hJdGExVUNiOEVwVEZ3b1hhNHo1aFhpanFGL05lVDJDb09kS3RWTzNRNFFGOGdhWXpFdXlUdz09PC9kczpNb2R1bHVzPjxkczpFeHBvbmVudD5BUUFCPC9kczpFeHBvbmVudD48L2RzOlJTQUtleVZhbHVlPjwvZHM6S2V5VmFsdWU+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxTdWJqZWN0PjxOYW1lSUQgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCI+UDIwMDM4NTg3NTc8L05hbWVJRD48U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPjxTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBJblJlc3BvbnNlVG89IlNmYTE2M2UwMi01OGNhLTFlZWItYTlmZC1kZDBlMDIyOGE1OGMiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMi44MzVaIiBSZWNpcGllbnQ9Imh0dHBzOi8vbXkzMDQ4NzQuczRoYW5hLm9uZGVtYW5kLmNvbS9zYXAvc2FtbDIvc3AvYWNzLzEwMCIvPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q+PENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDIxLTA0LTI4VDA2OjUyOjMyLjgzNVoiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMi44MzVaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U+aHR0cHM6Ly9teTMwNDg3NC5zNGhhbmEub25kZW1hbmQuY29tPC9BdWRpZW5jZT48L0F1ZGllbmNlUmVzdHJpY3Rpb24+PC9Db25kaXRpb25zPjxBdXRoblN0YXRlbWVudCBBdXRobkluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTc6MzIuODM1WiIgU2Vzc2lvbkluZGV4PSJTLVNQLWNlOGNjNmY5LTdhNWItNDQzOC1iNzUzLWY0MWFkZGM3OGFhOSIgU2Vzc2lvbk5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQxODo1NzozMi44MzVaIj48QXV0aG5Db250ZXh0PjxBdXRobkNvbnRleHRDbGFzc1JlZj51cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZFByb3RlY3RlZFRyYW5zcG9ydDwvQXV0aG5Db250ZXh0Q2xhc3NSZWY+PEF1dGhlbnRpY2F0aW5nQXV0aG9yaXR5PnNhcHRyaWFsLmFjY291bnRzLm9uZGVtYW5kLmNvbTwvQXV0aGVudGljYXRpbmdBdXRob3JpdHk+PC9BdXRobkNvbnRleHQ+PC9BdXRoblN0YXRlbWVudD48QXR0cmlidXRlU3RhdGVtZW50PjxBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIj48QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5KYWluPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48QXR0cmlidXRlIE5hbWU9ImZpcnN0X25hbWUiPjxBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPk5lZXJhajwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PEF0dHJpYnV0ZSBOYW1lPSJlbWFpbCI+PEF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+bmVlcmFqQGNhdmlzc29uLmNvbTwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PC9BdHRyaWJ1dGVTdGF0ZW1lbnQ+PC9Bc3NlcnRpb24+PC9SZXNwb25zZT4="
, key: "RelayState"
value: "oucfarwteqsovycaoreeboazfdoddqeqssyavyc"
]",
            BODY_END,
             END_INLINE
        );

        status = nsApi.ns_end_transaction("sso_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.141);

        status = nsApi.ns_start_transaction("X00");
        status = nsApi.ns_web_url ("X00",
            "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter",
            "METHOD=POST",
            "HEADER=Content-Length:7038",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Content-Type:application/x-www-form-urlencoded",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=oucfarwteqsovycaoreeboazfdoddqeqssyavyc;sap-usercontext;oucfarwteqsovycaoreeboazfdoddqeqssyavyc_anchor",
            BODY_BEGIN,
                "[key: "SAMLResponse"
value: "PFJlc3BvbnNlIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6cHJvdG9jb2wiIHhtbG5zOm5zMj0idXJuOm9hc2lzOm5hbWVzOnRjOlNBTUw6Mi4wOmFzc2VydGlvbiIgeG1sbnM6bnMzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIiB4bWxuczpuczQ9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jIyIgRGVzdGluYXRpb249Imh0dHBzOi8vbXkzMDQ4NzQuczRoYW5hLm9uZGVtYW5kLmNvbS9zYXAvc2FtbDIvc3AvYWNzLzEwMCIgSUQ9IlJFUy1TU08tMmEzYmM1YmUtMTA5Yi00MzlhLTg0ZGUtYTg4ZmMzNDk5YWUyIiBJblJlc3BvbnNlVG89IlNmYTE2M2UwMi01OGNhLTFlZWItYTlmZC1kZDBlMDIyOGE1OGMiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1NzozMi44MzVaIiBWZXJzaW9uPSIyLjAiPjxuczI6SXNzdWVyPmh0dHBzOi8vYXJ3empjYXZ6LmFjY291bnRzLm9uZGVtYW5kLmNvbTwvbnMyOklzc3Vlcj48U3RhdHVzPjxTdGF0dXNDb2RlIFZhbHVlPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6c3RhdHVzOlN1Y2Nlc3MiLz48L1N0YXR1cz48QXNzZXJ0aW9uIHhtbG5zPSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YXNzZXJ0aW9uIiB4bWxuczpuczI9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiIHhtbG5zOm5zMz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS8wNC94bWxlbmMjIiBJRD0iQS0yNmQ4Zjg1Ny1kMzg4LTQ4N2MtOTc4MS1jZmViNjQ0MWY3NmUiIElzc3VlSW5zdGFudD0iMjAyMS0wNC0yOFQwNjo1NzozMi44MzVaIiBWZXJzaW9uPSIyLjAiPjxJc3N1ZXI+aHR0cHM6Ly9hcnd6amNhdnouYWNjb3VudHMub25kZW1hbmQuY29tPC9Jc3N1ZXI+PGRzOlNpZ25hdHVyZSB4bWxuczpkcz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnIyI+PGRzOlNpZ25lZEluZm8+PGRzOkNhbm9uaWNhbGl6YXRpb25NZXRob2QgQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzEwL3htbC1leGMtYzE0biMiLz48ZHM6U2lnbmF0dXJlTWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3JzYS1zaGExIi8+PGRzOlJlZmVyZW5jZSBVUkk9IiNBLTI2ZDhmODU3LWQzODgtNDg3Yy05NzgxLWNmZWI2NDQxZjc2ZSI+PGRzOlRyYW5zZm9ybXM+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyNlbnZlbG9wZWQtc2lnbmF0dXJlIi8+PGRzOlRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMTAveG1sLWV4Yy1jMTRuIyIvPjwvZHM6VHJhbnNmb3Jtcz48ZHM6RGlnZXN0TWV0aG9kIEFsZ29yaXRobT0iaHR0cDovL3d3dy53My5vcmcvMjAwMC8wOS94bWxkc2lnI3NoYTEiLz48ZHM6RGlnZXN0VmFsdWU+eHB5eG1HYlFrUkJkN3lOVEF5alNiUm5uU2F3PTwvZHM6RGlnZXN0VmFsdWU+PC9kczpSZWZlcmVuY2U+PC9kczpTaWduZWRJbmZvPjxkczpTaWduYXR1cmVWYWx1ZT5kSmlaL282dXhySzBkY1E0a0pnd0VBbEtWam5lZnBpNUN0Q1ZOLzVGVVU4ZVFWeWhaazhOTi9tUThlYXYrS1JubUt5NEpLL0wwOGlmYk42Zlc2c1V1eEQrMGZqeHBvU21jczI3U3BwZTN2cS9VM052WldrLzZOUEZZKzlJaTRCdHBwYm1zQXBUekhZb2I0QW5JbDhPYWVoMmF6WTJJZmVaTFI4ckxkdGNwd3E3NEdtNEFVKzhoeXhoUm9yMW9WMFBNYm9GSUdjdWdjUHhlTzdlRWF6SUYzY0R1blBHa0NFZndmN2ZlaitCeVRua0lMclhOMlgxeTI5RlUyNDFiTUgxUThXc05CSDNGNkJ4Nkd5TGNwOUpYTTI2aGR5NzBvdWw5eU01TWs5ZXlhNjJMUWtZd3Q2N0xmcTZyc1JMZS94Ty9NVm9JSnZLUzF3WTdYUHMxS0pWWHc9PTwvZHM6U2lnbmF0dXJlVmFsdWU+PGRzOktleUluZm8+PGRzOlg1MDlEYXRhPjxkczpYNTA5Q2VydGlmaWNhdGU+TUlJREVqQ0NBZnFnQXdJQkFRSUdBWFRTWkh6N01BMEdDU3FHU0liM0RRRUJDd1VBTUVneEN6QUpCZ05WQkFZVEFrUkZNUTh3RFFZRFZRUUtFd1pUUVZBdFUwVXhLREFtQmdOVkJBTVRIMkZ5ZDNwcVkyRjJlaTVoWTJOdmRXNTBjeTV2Ym1SbGJXRnVaQzVqYjIwd0hoY05NakF3T1RJNE1ERTBOekUxV2hjTk16QXdPVEk0TURFME56RTFXakJJTVFzd0NRWURWUVFHRXdKRVJURVBNQTBHQTFVRUNoTUdVMEZRTFZORk1TZ3dKZ1lEVlFRREV4OWhjbmQ2YW1OaGRub3VZV05qYjNWdWRITXViMjVrWlcxaGJtUXVZMjl0TUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FROEFNSUlCQ2dLQ0FRRUFqRkVJdm40TW5YTG44c0ZTREZuSHl6RkMwd2lnWVEzS09SSjFJU1oxM3hGcjhUZVNiWXNOZDJ0L1ZMRzR4Rlo2RTRIUkZRTmZaSGttTGFxVWx3bnBESy9yVC9yUlRyWnU5WGVBVTVyTWhSWWdzL0VYSHlIYlI4RElkK3lWZklIbkxkVlR2Z3E3VE1jU0pqYUxrOWcwaXgvY1lLcXpFWXZIZWc4MDNjdVpLZTdHd0VRcmRKZVVFVTBTZ0FSRmErU2JyU3Rwa3FFcGNpNHRQaEo3WjFsUzlWaU8zZHBGK3p5Y24yWVVaOHhGLzVhMklVNW1UVTcrYkh0N3YyeVdoRnprY3lEcFppcCtSNzJ5aHcxaGdFaENoMEQwb2ZLU0lwck94SXRhMVVDYjhFcFRGd29YYTR6NWhYaWpxRi9OZVQyQ29PZEt0Vk8zUTRRRjhnYVl6RXV5VHdJREFRQUJnZ0lBQURBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQURBU0pqSTR1cC9KUWVDWEptQVVSc1EzZWJLNjlHdFo3Qi9YVmh6WVI3RTJ2eGJWNjhwRXJFR1kzdlFFZGVNTmdGWUZ0MSt0REV3V2NJQmRSakF1VWIyUHdpREtLT2lPZXRzbGlVUG1rTitpL2laMkR2cUV6RUlQQm5acGpKWmt4T1NqSDNzTUtGWVQ2ajdkMWthMkJaVDdsd2hZRWtya1N5NkxvalNwYUZFaWRiU3JpcE9HNWJ3c3FpbVVIQUNUTUVVaWJPTHV6R2ZTTGs1WUpFVnRySytkeFNsQXVFZDVsVzRxU043QjMwWnBTT3ZZV2RVc2xFZUQyeVhLZ3BNS2R2Z3FzS2IxSDBzTk5vemk5QzJ2Z1ZsaFVKQUdKVTYxcWdPTTM4SkhCbC8wSE9HWFlnWG9hb0JQdW5Jc2x2K0tydWpmdUFPVVRNTVdwdmt3M0FZR1JUQT09PC9kczpYNTA5Q2VydGlmaWNhdGU+PC9kczpYNTA5RGF0YT48ZHM6S2V5VmFsdWU+PGRzOlJTQUtleVZhbHVlPjxkczpNb2R1bHVzPmpGRUl2bjRNblhMbjhzRlNERm5IeXpGQzB3aWdZUTNLT1JKMUlTWjEzeEZyOFRlU2JZc05kMnQvVkxHNHhGWjZFNEhSRlFOZlpIa21MYXFVbHducERLL3JUL3JSVHJadTlYZUFVNXJNaFJZZ3MvRVhIeUhiUjhESWQreVZmSUhuTGRWVHZncTdUTWNTSmphTGs5ZzBpeC9jWUtxekVZdkhlZzgwM2N1WktlN0d3RVFyZEplVUVVMFNnQVJGYStTYnJTdHBrcUVwY2k0dFBoSjdaMWxTOVZpTzNkcEYrenljbjJZVVo4eEYvNWEySVU1bVRVNytiSHQ3djJ5V2hGemtjeURwWmlwK1I3MnlodzFoZ0VoQ2gwRDBvZktTSXByT3hJdGExVUNiOEVwVEZ3b1hhNHo1aFhpanFGL05lVDJDb09kS3RWTzNRNFFGOGdhWXpFdXlUdz09PC9kczpNb2R1bHVzPjxkczpFeHBvbmVudD5BUUFCPC9kczpFeHBvbmVudD48L2RzOlJTQUtleVZhbHVlPjwvZHM6S2V5VmFsdWU+PC9kczpLZXlJbmZvPjwvZHM6U2lnbmF0dXJlPjxTdWJqZWN0PjxOYW1lSUQgRm9ybWF0PSJ1cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoxLjE6bmFtZWlkLWZvcm1hdDp1bnNwZWNpZmllZCI+UDIwMDM4NTg3NTc8L05hbWVJRD48U3ViamVjdENvbmZpcm1hdGlvbiBNZXRob2Q9InVybjpvYXNpczpuYW1lczp0YzpTQU1MOjIuMDpjbTpiZWFyZXIiPjxTdWJqZWN0Q29uZmlybWF0aW9uRGF0YSBJblJlc3BvbnNlVG89IlNmYTE2M2UwMi01OGNhLTFlZWItYTlmZC1kZDBlMDIyOGE1OGMiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMi44MzVaIiBSZWNpcGllbnQ9Imh0dHBzOi8vbXkzMDQ4NzQuczRoYW5hLm9uZGVtYW5kLmNvbS9zYXAvc2FtbDIvc3AvYWNzLzEwMCIvPjwvU3ViamVjdENvbmZpcm1hdGlvbj48L1N1YmplY3Q+PENvbmRpdGlvbnMgTm90QmVmb3JlPSIyMDIxLTA0LTI4VDA2OjUyOjMyLjgzNVoiIE5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQwNzowNzozMi44MzVaIj48QXVkaWVuY2VSZXN0cmljdGlvbj48QXVkaWVuY2U+aHR0cHM6Ly9teTMwNDg3NC5zNGhhbmEub25kZW1hbmQuY29tPC9BdWRpZW5jZT48L0F1ZGllbmNlUmVzdHJpY3Rpb24+PC9Db25kaXRpb25zPjxBdXRoblN0YXRlbWVudCBBdXRobkluc3RhbnQ9IjIwMjEtMDQtMjhUMDY6NTc6MzIuODM1WiIgU2Vzc2lvbkluZGV4PSJTLVNQLWNlOGNjNmY5LTdhNWItNDQzOC1iNzUzLWY0MWFkZGM3OGFhOSIgU2Vzc2lvbk5vdE9uT3JBZnRlcj0iMjAyMS0wNC0yOFQxODo1NzozMi44MzVaIj48QXV0aG5Db250ZXh0PjxBdXRobkNvbnRleHRDbGFzc1JlZj51cm46b2FzaXM6bmFtZXM6dGM6U0FNTDoyLjA6YWM6Y2xhc3NlczpQYXNzd29yZFByb3RlY3RlZFRyYW5zcG9ydDwvQXV0aG5Db250ZXh0Q2xhc3NSZWY+PEF1dGhlbnRpY2F0aW5nQXV0aG9yaXR5PnNhcHRyaWFsLmFjY291bnRzLm9uZGVtYW5kLmNvbTwvQXV0aGVudGljYXRpbmdBdXRob3JpdHk+PC9BdXRobkNvbnRleHQ+PC9BdXRoblN0YXRlbWVudD48QXR0cmlidXRlU3RhdGVtZW50PjxBdHRyaWJ1dGUgTmFtZT0ibGFzdF9uYW1lIj48QXR0cmlidXRlVmFsdWUgeG1sbnM6eHM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hIiB4bWxuczp4c2k9Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvWE1MU2NoZW1hLWluc3RhbmNlIiB4c2k6dHlwZT0ieHM6c3RyaW5nIj5KYWluPC9BdHRyaWJ1dGVWYWx1ZT48L0F0dHJpYnV0ZT48QXR0cmlidXRlIE5hbWU9ImZpcnN0X25hbWUiPjxBdHRyaWJ1dGVWYWx1ZSB4bWxuczp4cz0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEiIHhtbG5zOnhzaT0iaHR0cDovL3d3dy53My5vcmcvMjAwMS9YTUxTY2hlbWEtaW5zdGFuY2UiIHhzaTp0eXBlPSJ4czpzdHJpbmciPk5lZXJhajwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PEF0dHJpYnV0ZSBOYW1lPSJlbWFpbCI+PEF0dHJpYnV0ZVZhbHVlIHhtbG5zOnhzPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYSIgeG1sbnM6eHNpPSJodHRwOi8vd3d3LnczLm9yZy8yMDAxL1hNTFNjaGVtYS1pbnN0YW5jZSIgeHNpOnR5cGU9InhzOnN0cmluZyI+bmVlcmFqQGNhdmlzc29uLmNvbTwvQXR0cmlidXRlVmFsdWU+PC9BdHRyaWJ1dGU+PC9BdHRyaWJ1dGVTdGF0ZW1lbnQ+PC9Bc3NlcnRpb24+PC9SZXNwb25zZT4="
, key: "RelayState"
value: "oucfarwteqsovycaoreeboazfdoddqeqssyavyc"
, key: "sap-language"
value: "EN"
]",
            BODY_END
        );

        status = nsApi.ns_end_transaction("X00", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.016);

        status = nsApi.ns_start_transaction("ui_2");
        status = nsApi.ns_web_url ("ui_2",
            "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter",
            "HEADER=Cache-Control:max-age=0",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/library.css", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/abap.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/m/themes/sap_fiori_3/library.css", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/sap_fiori_3/library.css", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-0.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-1.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("ui_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("start_up");
        status = nsApi.ns_web_url ("start_up",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/start_up?so=ProductTrialOffer&action=displayTrialCenter&systemAliasesFormat=object&formFactor=desktop&sap-language=EN&shellType=FLP&depth=0&sap-cache-id=D466DA148DBD3A24354FD8CE2AA56322",
            "HEADER=sap-language:EN",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-client:100",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-contextid;sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("start_up", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("start_up_2");
        status = nsApi.ns_web_url ("start_up_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/start_up?so=%2A&action=%2A&systemAliasesFormat=object&sap-language=EN&shellType=FLP&depth=0&sap-cache-id=D466DA148DBD3A24354FD8CE2AA56322",
            "HEADER=sap-language:EN",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-client:100",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-contextid;sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-2.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-3.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/renderers/fiori2/resources/resources_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/m/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/layout/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("start_up_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("BusinessSuiteInAppSymbols_js");
        status = nsApi.ns_web_url ("BusinessSuiteInAppSymbols_js",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/fonts/BusinessSuiteInAppSymbols.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("BusinessSuiteInAppSymbols_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("SAP_icons_TNT_json");
        status = nsApi.ns_web_url ("SAP_icons_TNT_json",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/tnt/themes/base/fonts/SAP-icons-TNT.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("SAP_icons_TNT_json", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("manifest_appdescr");
        status = nsApi.ns_web_url ("manifest_appdescr",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/lrep/content/~20201023072411.4493340~/apps/nw.core.cdt.trialcenter/app/sap/nw_aps_cdt_tc/manifest.appdescr?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043443838364236354139394634303336383141313836324345333241424537412020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-0.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-1.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-2.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-3.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_appdescr", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("SAP_icons_TNT_json_2");
        status = nsApi.ns_web_url ("SAP_icons_TNT_json_2",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/tnt/themes/base/fonts/SAP-icons-TNT.json",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/CanvasShapesManager.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("SAP_icons_TNT_json_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__P__");
        status = nsApi.ns_web_url ("PersContainers_category__P__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='P',id='flp.settings.FlpSettings')?$expand=PersContainerItems&sap-cache-id=F592CF41BD57C267A659FE68345CE67D",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202031343731463134434139454434323741413239414543413844344436434437322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/fonts/72-Regular.woff2", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/base/fonts/SAP-icons.woff2", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("PersContainers_category__P__", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json");
        status = nsApi.ns_web_url ("library_parameters_json",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_2");
        status = nsApi.ns_web_url ("library_parameters_json_2",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/m/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_3");
        status = nsApi.ns_web_url ("library_parameters_json_3",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_4");
        status = nsApi.ns_web_url ("library_parameters_json_4",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/layout/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/nw_aps_cdt_tc/~1304FCF72EECB290151D30F63CBB3536~5/Component-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/img/SAPLogo.svg", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("library_parameters_json_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("en_json");
        status = nsApi.ns_web_url ("en_json",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/cldr/en.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("en_json", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata");
        status = nsApi.ns_web_url ("Xmetadata",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_NAVOPTBLOCKEDCTNT_SRV/$metadata?sap-client=100&sap-language=EN&sap-context-token=20200724145647",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042413730453946453242374334304336413742304544454435413841453837372020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/library.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/nw_aps_cdt_tc/~1304FCF72EECB290151D30F63CBB3536~5/css/custom.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageLayout.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageSectionBase.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/library.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageSection.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageSubSection.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageLazyLoader.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/BlockBase.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/BlockBaseMetadata.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageSubSectionRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageSectionRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageHeaderContent.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectImageHelper.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageHeaderContentRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageHeaderRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/LazyLoading.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageLayoutABHelper.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/AnchorBar.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/HierarchicalSelect.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/HierarchicalSelectRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/AnchorBarRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/library-preload.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ThrottledTaskHelper.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageLayoutRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/img/launchpad_favicon.ico", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageDynamicHeaderTitle.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageDynamicHeaderContent.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageDynamicHeaderContentRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/DynamicPageHeaderRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/DynamicPageHeader.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/ObjectPageDynamicHeaderTitleRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/DynamicPageTitleRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/DynamicPageTitle.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/fonts/72-Bold.woff2", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xmetadata", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__U__");
        status = nsApi.ns_web_url ("PersContainers_category__U__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='U',id='sap.ushell.services.UserRecents')?$expand=PersContainerItems",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046463830344241323544453834424143423833333432373434313535374334332020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter&/s4hana_us/tours", END_INLINE
        );

        status = nsApi.ns_end_transaction("PersContainers_category__U__", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_2");
        status = nsApi.ns_web_url ("Xmetadata_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$metadata?sap-client=100&sap-language=EN&sap-context-token=20200724145647",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035463342413846343537434234384341384634334637343546323841303642322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("PersContainers");
        status = nsApi.ns_web_url ("PersContainers",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers",
            "METHOD=POST",
            "HEADER=Content-Length:3537",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Accept-Language:EN",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=sap-client:100",
            "HEADER=DataServiceVersion:1.0",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037374237383335424342323734423936393344343146313644423737433031312020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"__metadata":{"id":"https://my304874.s4hana.ondemand.com:443/sap/opu/odata/UI2/INTEROP/PersContainers(id='sap.ushell.services.UserRecents',category='U')","uri":"https://my304874.s4hana.ondemand.com:443/sap/opu/odata/UI2/INTEROP/PersContainers(id='sap.ushell.services.UserRecents',category='U')","type":"INTEROP.PersContainer"},"id":"sap.ushell.services.UserRecents","category":"U","validity":0,"clientExpirationTime":"\/Date(253373423400000)\/","component":"","appName":"","PersContainerItems":[{"value":"{\"recentDay\":\"2021/4/28\",\"recentUsageArray\":[{\"oItem\":{\"title\":\"Trial Center\",\"appType\":\"Application\",\"url\":\"#ProductTrialOffer-displayTrialCenter\",\"appId\":\"#ProductTrialOffer-displayTrialCenter\",\"timestamp\":1619593058963,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619593058963,\"aUsageArray\":[1,16,1],\"iCount\":18},{\"oItem\":{\"title\":\"Customer Projects\",\"appType\":\"Application\",\"url\":\"#CustomerProject-maintainCustomerProject\",\"appId\":\"#CustomerProject-maintainCustomerProject\",\"timestamp\":1619535376109,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619535376109,\"aUsageArray\":[10,0],\"iCount\":10},{\"oItem\":{\"title\":\"Manage Sales Orders\",\"appType\":\"Application\",\"url\":\"#SalesOrder-manageSalesOrder?preferredMode=create\",\"appId\":\"#SalesOrder-manageSalesOrder\",\"timestamp\":1619522537819,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619522537819,\"aUsageArray\":[3,0],\"iCount\":3},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"customer activity repository\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"customer activity repository\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508376201,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508376201,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"IDE Full-Stack\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"IDE Full-Stack\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508141324,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508141324,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"Full\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"Full\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508111215,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508111215,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"title\":\"Trial Center\",\"appType\":\"Application\",\"url\":\"#ProductTrialOffer-displayTrialCenterDesktop&/s4hana_us/tours\",\"appId\":\"#ProductTrialOffer-displayTrialCenterDesktop\",\"timestamp\":1619507760105,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619507760105,\"aUsageArray\":[2,0],\"iCount\":2}]}","id":"RecentActivity","category":"I","containerId":"sap.ushell.services.UserRecents","containerCategory":"U"}]}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("PersContainers", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = HEAD
        status = nsApi.ns_start_transaction("index");
        status = nsApi.ns_web_url ("index",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/?sap-client=100",
            "METHOD=HEAD",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=x-csrf-token:Fetch",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042453846433043413344443334324143383843383832344337354646463833452020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("index", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch");
        status = nsApi.ns_web_url ("Xbatch",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:416",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_2fb2-988e-c394",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043324531353731334244393734354234393642324435463643454339373044302020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_2fb2-988e-c394
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Datasets('s4hana_us')?sap-client=100&$expand=Pages%2cIntroLink%2cIntroLink%2fImage%2cInfoPopupLink%2cSupportedLanguages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("sap_ui_version_json");
        status = nsApi.ns_web_url ("sap_ui_version_json",
            "URL=https://ui5.sap.com/1.81.6/resources/sap-ui-version.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("sap_ui_version_json", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_2");
        status = nsApi.ns_web_url ("Xbatch_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:4991",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_0da9-ac56-e586",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032453445393034333338423534363842413842354635323036334531414338392020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: multipart/mixed; boundary=changeset_9553-1d39-0eed",
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                "HEADER=Content-Type: application/json",
                "HEADER=Content-Length: 61",
                    MULTIPART_BODY_BEGIN,
                MULTIPART_BOUNDARY,
                //--changeset_9553-1d39-0eed
                    BODY_BEGIN,
                        "POST AssetUsage?sap-client=100 HTTP/1.1
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==

{"Event":"openRolePicker","Asset":"","DatasetId":"s4hana_us"}",
                    BODY_END,
                MULTIPART_BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='tours')/Sections/$count?sap-client=100 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='tours')/Sections?sap-client=100&$skip=0&$top=100&$orderby=Ranking%20asc&$expand=Image%2cVisionaryContentDefinition%2cVisionaryContentDefinition%2fLink%2cCarouselDefinition%2cFrequentlyAskedQuestions%2cLinks%2cLinks%2fImage%2cLinks%2fCurrentAdditionalTranslation%2cGuidedTours%2cGuidedTours%2fImage%2cGuidedTours%2fGuidedTourLinks%2cGuidedTours%2fGuidedTourLinks%2fLink HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='learning')/Sections/$count?sap-client=100 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='learning')/Sections?sap-client=100&$skip=0&$top=100&$orderby=Ranking%20asc&$expand=Image%2cVisionaryContentDefinition%2cVisionaryContentDefinition%2fLink%2cCarouselDefinition%2cFrequentlyAskedQuestions%2cLinks%2cLinks%2fImage%2cLinks%2fCurrentAdditionalTranslation%2cGuidedTours%2cGuidedTours%2fImage%2cGuidedTours%2fGuidedTourLinks%2cGuidedTours%2fGuidedTourLinks%2fLink HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='media')/Sections/$count?sap-client=100 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='media')/Sections?sap-client=100&$skip=0&$top=100&$orderby=Ranking%20asc&$expand=Image%2cVisionaryContentDefinition%2cVisionaryContentDefinition%2fLink%2cCarouselDefinition%2cFrequentlyAskedQuestions%2cLinks%2cLinks%2fImage%2cLinks%2fCurrentAdditionalTranslation%2cGuidedTours%2cGuidedTours%2fImage%2cGuidedTours%2fGuidedTourLinks%2cGuidedTours%2fGuidedTourLinks%2fLink HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='support')/Sections/$count?sap-client=100 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0da9-ac56-e586
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET Pages(DatasetId='s4hana_us',PageId='support')/Sections?sap-client=100&$skip=0&$top=100&$orderby=Ranking%20asc&$expand=Image%2cVisionaryContentDefinition%2cVisionaryContentDefinition%2fLink%2cCarouselDefinition%2cFrequentlyAskedQuestions%2cLinks%2cLinks%2fImage%2cLinks%2fCurrentAdditionalTranslation%2cGuidedTours%2cGuidedTours%2fImage%2cGuidedTours%2fGuidedTourLinks%2cGuidedTours%2fGuidedTourLinks%2fLink HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==",
                BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='s4hana_us-s4hana_us-intro')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_3");
        status = nsApi.ns_web_url ("Xbatch_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:587",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_5427-25ea-1037",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030464538444445304532463434464335383737413543354333333031303646322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_5427-25ea-1037
                "HEADER=Content-Type: multipart/mixed; boundary=changeset_f560-3152-7052",
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                "HEADER=Content-Type: application/json",
                "HEADER=Content-Length: 71",
                    MULTIPART_BODY_BEGIN,
                MULTIPART_BOUNDARY,
                //--changeset_f560-3152-7052
                    BODY_BEGIN,
                        "POST AssetUsage?sap-client=100 HTTP/1.1
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==

{"Event":"visitSection","Asset":"Tour_Section","DatasetId":"s4hana_us"}",
                    BODY_END,
                MULTIPART_BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_4");
        status = nsApi.ns_web_url ("Xbatch_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:597",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_903a-2c1f-126c",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600006E772E636F72652E6364742E747269616C63656E74657240302E302E3020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F6974656D392D5F5F6C697374312D305F636C69636B5F3120202020202020202020202020202000056E772E636F72652E6364742E747269616C63656E74657240302E302E3020202034463332453335413841393934413737424336353139433132433334453936432020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_903a-2c1f-126c
                "HEADER=Content-Type: multipart/mixed; boundary=changeset_4960-7037-a33f",
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                "HEADER=Content-Type: application/json",
                "HEADER=Content-Length: 81",
                    MULTIPART_BODY_BEGIN,
                MULTIPART_BOUNDARY,
                //--changeset_4960-7037-a33f
                    BODY_BEGIN,
                        "POST AssetUsage?sap-client=100 HTTP/1.1
sap-contextid-accept: header
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==

{"Event":"openTour","Asset":"Tour_Plan_Customer_Project","DatasetId":"s4hana_us"}",
                    BODY_END,
                MULTIPART_BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/GridList.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/GridListRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Notifications/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Component_preload_js");
        status = nsApi.ns_web_url ("Component_preload_js",
            "URL=https://my304874.s4hana.ondemand.com/copilot/ui/Component-preload.js",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030304431464431413739393134393638393341424143353931393935413242362020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/utils/adapters/fiori/Component-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("Component_preload_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("manifest_appdescr_2");
        status = nsApi.ns_web_url ("manifest_appdescr_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/lrep/content/~20201023072435.1735870~/apps/sap.wdr.UIPluginAdaptUI/app/sap/wdr_adapt_ui/manifest.appdescr?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039334146443330453838304634363637384332374442383131374333314136352020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/rta/Component.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/BaseRTAPlugin.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/BaseRTAPluginStatus.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_appdescr_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("manifest_json");
        status = nsApi.ns_web_url ("manifest_json",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/rta/manifest.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("manifest_json", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("GetBadgeNumber__");
        status = nsApi.ns_web_url ("GetBadgeNumber__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/GetBadgeNumber()",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045363043363733344431413134433738394445324243323237423531424646442020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("GetBadgeNumber__", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount");
        status = nsApi.ns_web_url ("Xcount",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Notifications/$count",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045323130454239324536363134423042393034344233433135353038313439362020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Notifications");
        status = nsApi.ns_web_url ("Notifications",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Notifications?$expand=Actions,NavigationTargetParams&$orderby=CreatedAt%20desc&$filter=IsGroupHeader%20eq%20false&$skip=0&$top=10",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-CSRF-Token:fetch",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036433830344535363831413434303741384642333932353044333034433436342020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Notifications", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__P___2");
        status = nsApi.ns_web_url ("PersContainers_category__P___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='P',id='sap.ushell.services.Notifications')?$expand=PersContainerItems&sap-cache-id=F592CF41BD57C267A659FE68345CE67D",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046304139334143324546363534453430393132413830423231313439454241442020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__P___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NotificationTypePersonalizat");
        status = nsApi.ns_web_url ("NotificationTypePersonalizat",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/NotificationTypePersonalizationSet",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032363534314131344646343334313437393042433535363846334331303846322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("NotificationTypePersonalizat", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_SMP_");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_SMP_",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_SMP')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030313835314638443245303834393246393942363034454245304241423041412020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_SMP_", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_EMAI");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_EMAI",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_EMAIL')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044344143323644464532343434423642383939304238394342363836433443332020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/UserImage/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/rta/i18n/i18n_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/rta/i18n/i18n_en.properties", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-planproject')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-eventrevrec')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-reviewpro')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-analyzeproj')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-finstatement')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-monrecievabl')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-anapaybles')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-postpayments')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-predaccount')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-cash-monitorcash')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pur-proc2invoice')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pur-earlypayment')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-sofulfill')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-analyzesales')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-sellstock')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-mngmatrequir')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-qualityinspe')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-extwarehouse')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-incrstock')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-extend-inappext')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-highlights-inappext')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-highlights-fiori3')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-highlights-feedback')/$value", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Search/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/Component-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/MeArea/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Settings/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_EMAI", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("FeedbackLegalTexts__1__");
        status = nsApi.ns_web_url ("FeedbackLegalTexts__1__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/FeedbackLegalTexts('1')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039373744454531393245313034373933384130423241414242434441434642382020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("FeedbackLegalTexts__1__", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NotificationTypePersonalizat_2");
        status = nsApi.ns_web_url ("NotificationTypePersonalizat_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/NotificationTypePersonalizationSet",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032374336373041433530394534464538393539373937334338323645454132382020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("NotificationTypePersonalizat_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_SMP__2");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_SMP__2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_SMP')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037453745444142313933363234373135414133343042373335364335324441322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_SMP__2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_EMAI_2");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_EMAI_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_EMAIL')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042354539303330313344323034373439423441374444384441433534334643322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_EMAI_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Capabilities");
        status = nsApi.ns_web_url ("Capabilities",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/ServerInfos?$expand=Services/Capabilities",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:fetch",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034393446334546313938393334353130423944344533453331314143393142312020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Capabilities", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels__SAP_WEBSOCKET__");
        status = nsApi.ns_web_url ("Channels__SAP_WEBSOCKET__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels('SAP_WEBSOCKET')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039464337344236413633384334373545384634323146433634323444324538442020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='s4hana_us-s4hana_us-intro')/$value", END_INLINE
        );

        status = nsApi.ns_end_transaction("Channels__SAP_WEBSOCKET__", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_3");
        status = nsApi.ns_web_url ("Xmetadata_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/$metadata",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/xml",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033343539453737323537334334423533384235303644463741413746414546442020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("i18n_en_properties");
        status = nsApi.ns_web_url ("i18n_en_properties",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/i18n/i18n_en.properties",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045454345453439424241453234454341383736424645423337313144323041322020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-planproject')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-eventrevrec')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-analyzeproj')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pm-reviewpro')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-finstatement')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-monrecievabl')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-anapaybles')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-postpayments')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-fin-predaccount')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-cash-monitorcash')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pur-proc2invoice')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-pur-earlypayment')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-sofulfill')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-analyzesales')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-sal-sellstock')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-qualityinspe')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-incrstock')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-mngmatrequir')/$value", END_INLINE
        );

        status = nsApi.ns_end_transaction("i18n_en_properties", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Component_js");
        status = nsApi.ns_web_url ("Component_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/Component.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030424638393243303532354534434543383432443239443230394234303636302020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-prod-extwarehouse')/$value", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/Images(DatasetId='s4hana_us',ImageId='tours-extend-inappext')/$value", END_INLINE
        );

        status = nsApi.ns_end_transaction("Component_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Annotations__filter_Type_20e");
        status = nsApi.ns_web_url ("Annotations__filter_Type_20e",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/DataSources?$expand=Annotations,Attributes/UIAreas,Attributes/Annotations&$filter=Type%20eq%20%27View%27%20and%20IsInternal%20eq%20false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034384645383532454545383734433244413433384344443642394643423542422020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Annotations__filter_Type_20e", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("manifest_json_2");
        status = nsApi.ns_web_url ("manifest_json_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/manifest.json",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202031384331443232383746363834423235423334424230304233344543394630362020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/wpb/less/quartz.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/wpb/less/text_styles_quartz.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_json_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersonalizedSearchMainSwitch");
        status = nsApi.ns_web_url ("PersonalizedSearchMainSwitch",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/PersonalizedSearchMainSwitches?$filter=Selected%20eq%20true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032373632423441344644324334384432423942343144424337463333303334412020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersonalizedSearchMainSwitch", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Users___3Ccurrent_3E__");
        status = nsApi.ns_web_url ("Users___3Ccurrent_3E__",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/Users('%3Ccurrent%3E')",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034354330444333304632463234304245424333334346324232313445343737432020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors,x-csrf-token", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Users___3Ccurrent_3E__", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("catalogue");
        status = nsApi.ns_web_url ("catalogue",
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=IDP_SESSION_MARKER_accounts",
            INLINE_URLS,
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("catalogue", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("InteractionEventLists");
        status = nsApi.ns_web_url ("InteractionEventLists",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/InteractionEventLists",
            "METHOD=POST",
            "HEADER=Content-Length:154",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041373546313839364632454234353732423346324631363235384334303934342020200007223A08037BDC4C6BBD222D0143D799920000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ID":1,"SessionID":"2EE53B91C9E646FC9254D4D44C53D658","Events":[{"ID":1,"Type":"SESSION_START","Timestamp":"\\/Date(1619593082508)\\/","Parameters":[]}]}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("InteractionEventLists", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("Xcatalogue");
        status = nsApi.ns_web_url ("Xcatalogue",
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22ProductTrialOffer-displayTrialCenter%22%2C%22ProductTrialOffer-displayTrialCenter!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("Xcatalogue", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("flp_sap_fesr_only");
        status = nsApi.ns_web_url ("flp_sap_fesr_only",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/flp;sap-fesr-only",
            "METHOD=POST",
            "HEADER=Content-Length:286",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Content-Type:application/x-www-form-urlencoded;charset=utf-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("flp_sap_fesr_only", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("json");
        status = nsApi.ns_web_url ("json",
            "URL=https://update.googleapis.com/service/update2/json?cup2key=9:2602016094&cup2hreq=0b13581bafe657ddbffb8a1771b7a76314bb0ec11a417cf28c830fc57cff9bee",
            "METHOD=POST",
            "HEADER=Content-Length:3009",
            "HEADER=X-Goog-Update-AppId:oimompecagnajdejgnnjijobebaeigek,gcmjkmgdlgnkkcocmoeiminaijmmjnii,llkgjffcdpffmhiakmfcdcblohccpfmo,hfnkpimlhhgieaddgfemjhofmfblmnib,khaoiebndkojlmppeemjhbpbandiljpe,bklopemakmnopmghhmccadeonafabnal,jflookgnkcckhobaglndicnbbgbonegd,giekcmmlnklenlaomppkphknjmnnpneh",
            "HEADER=X-Goog-Update-Interactivity:bg",
            "HEADER=X-Goog-Update-Updater:chrome-79.0.3945.130",
            "HEADER=Content-Type:application/json",
            "HEADER=Sec-Fetch-Site:none",
            "HEADER=Sec-Fetch-Mode:no-cors",
            BODY_BEGIN,
                "{"request":{"@os":"win","@updater":"chrome","acceptformat":"crx2,crx3","app":[{"appid":"oimompecagnajdejgnnjijobebaeigek","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.3a7afafe965da76cea59344022f69dcb9eaaef382ce56eb1daa8d20f9ec80c16"}]},"ping":{"ping_freshness":"{78e4baab-aa8e-4228-9eae-0d3fd5e1a20a}","rd":5230},"updatecheck":{},"version":"4.10.1610.0"},{"appid":"gcmjkmgdlgnkkcocmoeiminaijmmjnii","cohort":"1:bm1:","cohorthint":"M54ToM99","cohortname":"M54ToM99","enabled":true,"packages":{"package":[{"fp":"1.4dcc255c0d82123c9c4251bb453165672ea0458f0379f3a7a534dc2a666d7c6d"}]},"ping":{"ping_freshness":"{d78ebd90-7d62-4fd4-af16-16fccd1257a5}","rd":5230},"updatecheck":{},"version":"9.22.0"},{"appid":"llkgjffcdpffmhiakmfcdcblohccpfmo","cohort":"1::","enabled":true,"packages":{"package":[{"fp":"1.3d885f0577e4fd9e5b9251ba18576da6b49e80870ceaafcaa996e3b1dc762c01"}]},"ping":{"ping_freshness":"{cfaac548-fdfe-4ab3-afdb-c2ded7c77b04}","rd":5230},"updatecheck":{},"version":"1.0.0.4"},{"appid":"hfnkpimlhhgieaddgfemjhofmfblmnib","cohort":"1:jcl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.94b02f6d6bbb3b971f646107d197f7c7ef7783dc4e08df76ac2968d5ad326be0"}]},"ping":{"ping_freshness":"{ad19a2cc-0485-4e5c-9000-9d200c1f7b38}","rd":5230},"updatecheck":{},"version":"6570"},{"appid":"khaoiebndkojlmppeemjhbpbandiljpe","cohort":"1:cux:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.ffd1d2d75a8183b0a1081bd03a7ce1d140fded7a9fb52cf3ae864cd4d408ceb4"}]},"ping":{"ping_freshness":"{d0d0b67b-f589-48bb-b39a-112a63a58931}","rd":5230},"updatecheck":{},"version":"43"},{"appid":"bklopemakmnopmghhmccadeonafabnal","cohort":"1:swl:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.70497f45af368f6d591eb9b93a097b7b56821b0770ee00f04b2f5901487a0421"}]},"ping":{"ping_freshness":"{c479c1c1-9beb-4542-a753-203eb72c7ba9}","rd":5230},"updatecheck":{},"version":"4"},{"appid":"jflookgnkcckhobaglndicnbbgbonegd","cohort":"1:s7x:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.70dba5eed1c56cea56059b5555dea35cec86b8ec84172850567c7f372b41e80b"}]},"ping":{"ping_freshness":"{9e0665b3-6887-4b83-bc8b-f9f3effb268a}","rd":5230},"updatecheck":{},"version":"2622"},{"appid":"giekcmmlnklenlaomppkphknjmnnpneh","cohort":"1:j5l:","cohorthint":"Auto","cohortname":"Auto","enabled":true,"packages":{"package":[{"fp":"1.fd515ec0dc30d25a09641b8b83729234bc50f4511e35ce17d24fd996252eaace"}]},"ping":{"ping_freshness":"{0e82c366-c46a-46cf-9d80-f5f9ad8d48a9}","rd":5230},"updatecheck":{},"version":"7"}],"arch":"x64","dedup":"cr","domainjoined":false,"hw":{"physmemory":16},"lang":"en-GB","nacl_arch":"x86-64","os":{"arch":"x86_64","platform":"Windows","version":"10.0.19041.928"},"prodversion":"79.0.3945.130","protocol":"3.1","requestid":"{25acb0e8-7bc2-48e2-b1cd-4ca9428cdd61}","sessionid":"{cf95c84f-ac3c-4b40-a14b-057d9074ee09}","updaterversion":"79.0.3945.130"}}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("json", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(8.747);

        //Page Auto splitted for Link 'image' Clicked by User
        status = nsApi.ns_start_transaction("ui_3");
        status = nsApi.ns_web_url ("ui_3",
            "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0",
            "HEADER=Upgrade-Insecure-Requests:1",
            "HEADER=Sec-Fetch-User:?1",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:navigate",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ui_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_5");
        status = nsApi.ns_web_url ("Xbatch_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/$batch?sap-client=100",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/library.css", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/m/themes/sap_fiori_3/library.css", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/sap_fiori_3/library.css", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-0.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-1.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ChipInstanceProperties_Assig");
        status = nsApi.ns_web_url ("ChipInstanceProperties_Assig",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/PAGE_BUILDER_PERS/PageSets('%2FUI2%2FFiori2LaunchpadHome')?$expand=Pages/PageChipInstances/Chip/ChipBags/ChipProperties,Pages/PageChipInstances/RemoteCatalog,Pages/PageChipInstances/ChipInstanceBags/ChipInstanceProperties,AssignedPages,DefaultPage&sap-cache-id=FA163E0258CA1EEBA9D66DC0F8E6384B",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:fetch",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ChipInstanceProperties_Assig", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("start_up_3");
        status = nsApi.ns_web_url ("start_up_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/start_up?so=%2A&action=%2A&tm-compact=true&sap-language=EN&shellType=FLP&depth=0&sap-cache-id=D466DA148DBD3A24354FD8CE2AA56322",
            "HEADER=sap-language:EN",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-client:100",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-contextid;sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("start_up_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("start_up_4");
        status = nsApi.ns_web_url ("start_up_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/start_up?so=%2A&action=%2A&systemAliasesFormat=object&sap-language=EN&shellType=FLP&depth=0&sap-cache-id=D466DA148DBD3A24354FD8CE2AA56322",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-2.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-3.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/messagebundle_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/m/messagebundle_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/renderers/fiori2/resources/resources_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/layout/themes/sap_fiori_3/library.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("start_up_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("BusinessSuiteInAppSymbols_js_2");
        status = nsApi.ns_web_url ("BusinessSuiteInAppSymbols_js_2",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/fonts/BusinessSuiteInAppSymbols.json",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/CanvasShapesManager.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("BusinessSuiteInAppSymbols_js_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("PersContainers_category__P___3");
        status = nsApi.ns_web_url ("PersContainers_category__P___3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='P',id='flp.settings.FlpSettings')?$expand=PersContainerItems&sap-cache-id=F592CF41BD57C267A659FE68345CE67D"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__P___3", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_5");
        status = nsApi.ns_web_url ("library_parameters_json_5",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/library-parameters.json"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_6");
        status = nsApi.ns_web_url ("library_parameters_json_6",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/m/themes/sap_fiori_3/library-parameters.json"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_7");
        status = nsApi.ns_web_url ("library_parameters_json_7",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/sap_fiori_3/library-parameters.json"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_8");
        status = nsApi.ns_web_url ("library_parameters_json_8",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/layout/themes/sap_fiori_3/library-parameters.json"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("en_json_2");
        status = nsApi.ns_web_url ("en_json_2",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/core/cldr/en.json",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#Shell-home", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/flp-controls.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/homepage/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-0.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-1.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-2.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/fiori/core-ext-light-3.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("en_json_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("applauncher_chip_xml");
        status = nsApi.ns_web_url ("applauncher_chip_xml",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/ui2/ushell/~2CF890645D9577A22F4D461AF9D912E7~C/chips/applauncher.chip.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033413144324642373641353934374646384234454132364133423844423335322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("applauncher_chip_xml", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("applauncher_dynamic_chip_xml");
        status = nsApi.ns_web_url ("applauncher_dynamic_chip_xml",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/ui2/ushell/~2CF890645D9577A22F4D461AF9D912E7~C/chips/applauncher_dynamic.chip.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038304346393137343933393834363732413437424242423132433544333530422020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("applauncher_dynamic_chip_xml", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ContributionTileChip_xml");
        status = nsApi.ns_web_url ("ContributionTileChip_xml",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtiless1/~2CF890645D9577A22F4D461AF9D912E7~C/ssuite/smartbusiness/tiles/contribution/ContributionTileChip.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037383639303636383631393334313736393044463033453643394243373642302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ContributionTileChip_xml", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NumericTileChip_xml");
        status = nsApi.ns_web_url ("NumericTileChip_xml",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtiless1/~2CF890645D9577A22F4D461AF9D912E7~C/ssuite/smartbusiness/tiles/numeric/NumericTileChip.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039423944394436333337303934363041424138443446313039434334383542302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Notifications/Component-preload.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("NumericTileChip_xml", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Component_preload_js_2");
        status = nsApi.ns_web_url ("Component_preload_js_2",
            "URL=https://my304874.s4hana.ondemand.com/copilot/ui/Component-preload.js",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030463332433634414145344434434545423241373546333535433842384234392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/utils/adapters/fiori/Component-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=If-Modified-Since:Mon, 22 Mar 2021 11:46:16 GMT", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/lrep/content/~20201023072435.1735870~/apps/sap.wdr.UIPluginAdaptUI/app/sap/wdr_adapt_ui/manifest.appdescr?sap-language=EN&sap-client=100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/rta/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/Component-preload.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("Component_preload_js_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("GetBadgeNumber___2");
        status = nsApi.ns_web_url ("GetBadgeNumber___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/GetBadgeNumber()",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030353843453733323044364434383539393034334246303035463935383737462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("GetBadgeNumber___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_2");
        status = nsApi.ns_web_url ("Xcount_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Notifications/$count",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041394636373142393630443034304530423434383131433341383641373838392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Notifications_2");
        status = nsApi.ns_web_url ("Notifications_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Notifications?$expand=Actions,NavigationTargetParams&$orderby=CreatedAt%20desc&$filter=IsGroupHeader%20eq%20false&$skip=0&$top=10",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-CSRF-Token:fetch",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032333741323931383344464134334233393045353742423631443530463641312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Notifications_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("PersContainers_category__P___4");
        status = nsApi.ns_web_url ("PersContainers_category__P___4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='P',id='sap.ushell.services.Notifications')?$expand=PersContainerItems&sap-cache-id=F592CF41BD57C267A659FE68345CE67D"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__P___4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NotificationTypePersonalizat_3");
        status = nsApi.ns_web_url ("NotificationTypePersonalizat_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/NotificationTypePersonalizationSet",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035334439344446453239414234363739413938323834373344363442434437302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("NotificationTypePersonalizat_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_SMP__3");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_SMP__3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_SMP')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041364331323845424137414634433842394337413634313834353246453943302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_SMP__3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_EMAI_3");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_EMAI_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_EMAIL')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035383531333442454233344434353630384231434637373333374133303830432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/BaseRTAPlugin.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/BaseRTAPluginStatus.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/UserImage/Component-preload.js", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/i18n/i18n_en.properties", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/Component.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_EMAI_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("manifest_json_3");
        status = nsApi.ns_web_url ("manifest_json_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/wdr_adapt_ui/~617A0355F8855EFBBB985B76FFD7077B~5/manifest.json",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Search/Component-preload.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/MeArea/Component-preload.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/components/shell/Settings/Component-preload.js", END_INLINE
        );

        status = nsApi.ns_end_transaction("manifest_json_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("FeedbackLegalTexts__1___2");
        status = nsApi.ns_web_url ("FeedbackLegalTexts__1___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/FeedbackLegalTexts('1')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032453238454230433945453434453543413144393531363435303736453442462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("FeedbackLegalTexts__1___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Component_preload_js_3");
        status = nsApi.ns_web_url ("Component_preload_js_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtiless1/~2CF890645D9577A22F4D461AF9D912E7~C/ssuite/smartbusiness/tiles/contribution/Component-preload.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045303841333335423043384334384232414133313735343734304138334136392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Component_preload_js_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Util_js");
        status = nsApi.ns_web_url ("Util_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/Util.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030464633324243373333324234454545413237303945444142364430464134372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Util_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("TileLoadManager_js");
        status = nsApi.ns_web_url ("TileLoadManager_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/TileLoadManager.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034393330394541444642304334453736393938334143363532334535453441462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("TileLoadManager_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("AnnotationHelper_js");
        status = nsApi.ns_web_url ("AnnotationHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/AnnotationHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041353031453042443731433334324430393443373533324132463945424430382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("AnnotationHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("TileNavigation_js");
        status = nsApi.ns_web_url ("TileNavigation_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/TileNavigation.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034314645453631393533343534453030384438323641383937443237383639392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("TileNavigation_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RequestManager_js");
        status = nsApi.ns_web_url ("RequestManager_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/RequestManager.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039393146353743364644303934433338423438333843374241444444393143332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RequestManager_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("AppStateManager_js");
        status = nsApi.ns_web_url ("AppStateManager_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/AppStateManager.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043423831324337413144363334343236384134303538453235363343353732442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("AppStateManager_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("TimeStampControl_js");
        status = nsApi.ns_web_url ("TimeStampControl_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/TimeStampControl.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039303037383331454134383734454536413030464630434539303438333837342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/ComparisonMicroChart.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/library.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/MicroChartUtils.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/ComparisonMicroChartRenderer.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/MicroChartRenderUtils.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/ComparisonMicroChartData.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtiless1/~2CF890645D9577A22F4D461AF9D912E7~C/css/style.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/microchart/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("TimeStampControl_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Component_preload_js_4");
        status = nsApi.ns_web_url ("Component_preload_js_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtiless1/~2CF890645D9577A22F4D461AF9D912E7~C/ssuite/smartbusiness/tiles/numeric/Component-preload.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034383131433736374635414634344342423742374244323841444343393434362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/thirdparty/hyphenopoly/hyphenopoly.bundle.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("Component_preload_js_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NotificationTypePersonalizat_4");
        status = nsApi.ns_web_url ("NotificationTypePersonalizat_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/NotificationTypePersonalizationSet",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038373433333231304533323734373031414439454136443741353936343241382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("NotificationTypePersonalizat_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_SMP__4");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_SMP__4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_SMP')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036353941424145334644333834443034423741413946394441333846433242442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_SMP__4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels_ChannelId__SAP_EMAI_4");
        status = nsApi.ns_web_url ("Channels_ChannelId__SAP_EMAI_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels(ChannelId='SAP_EMAIL')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044313132424433363143433134383443413739304230353936364337334644302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Channels_ChannelId__SAP_EMAI_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_3");
        status = nsApi.ns_web_url ("Xcount_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/APS_CDT_TRIALCENTER_SRV/GuidedTours/$count?$filter=substringof(%27desktop%27,%20Devices)&sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035373436364544463042333634423144423531314638453033433739334346372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_4");
        status = nsApi.ns_web_url ("Xcount_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/CPD/SC_PROJ_ENGAGMNT_MAINT_SRV/ProjEngagementsSet/$count?sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038353736334632313237304334433235414134434243423238343744324632322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results");
        status = nsApi.ns_web_url ("Results",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%27ET090PW4NWFHYYNFC7MN7PA6F%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042334639333939354537443534304642383333393033373239454637453930452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Summary");
        status = nsApi.ns_web_url ("Summary",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/HCM_CATS_MANAG_V1/Summary?sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036343534303942353346364134434133383641304238324137374641333444362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Summary", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_5");
        status = nsApi.ns_web_url ("Xcount_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/CPD/SC_REVIEW_CUST_PROJ_SRV/EngmntPrjCurntReviewStsSet/$count?$filter=EngmntProjReviewStsIsPerdEnd%20eq%20false&sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033443437423342414232454134323735384243423844364441393546354438392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_2");
        status = nsApi.ns_web_url ("Results_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBL2P1S1DK0H76GQ%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033453539453537373139324534444136413135314131313938444633333439312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_3");
        status = nsApi.ns_web_url ("Results_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBL2Q64HZ5XN40O0%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033303945374242464535423434433539423346453730343730374238323839412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_4");
        status = nsApi.ns_web_url ("Results_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBJCF92EHQ8X6C2H%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037354130324435433830353134343442393439363737433441434446384136302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/fonts/sap-launch-icons.ttf", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/thirdparty/hyphenopoly/hyphenEngine.wasm", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/thirdparty/hyphenopoly/patterns/en-us.hpb", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Results_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue");
        status = nsApi.ns_web_url ("Xvalue",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.HBA.CPD.MONITOR.CUSTOMERPROJECT.EV')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044343242323534464136463334373139393844364443334235304435343344372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_2");
        status = nsApi.ns_web_url ("Xvalue_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.AP.DISCFORECASTKPI.1MONTH')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036314233384437324242354534324241413141424331463334334646323042462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_3");
        status = nsApi.ns_web_url ("Xvalue_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SFIN.AP.OVRDPAYABLES.TODAY')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044353139423430444236444134463944423937454136443943413144414332302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_4");
        status = nsApi.ns_web_url ("Xvalue_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.AP.DISCOUNTUTIL.60DAYS')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043363143413242334242364534393245423732423045443433463930303037462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/wpb/less/quartz.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=If-Modified-Since:Mon, 22 Mar 2021 11:45:52 GMT", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/wpb/less/text_styles_quartz.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "HEADER=If-Modified-Since:Mon, 22 Mar 2021 11:45:10 GMT", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xvalue_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Capabilities_2");
        status = nsApi.ns_web_url ("Capabilities_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/ServerInfos?$expand=Services/Capabilities",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:fetch",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042464430313942333444304234423934423246384630303531443736453631332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/tnt/themes/base/fonts/SAP-icons-TNT.woff2", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/base/fonts/BusinessSuiteInAppSymbols.woff2", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Capabilities_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_4");
        status = nsApi.ns_web_url ("Xmetadata_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/$metadata",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/xml",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030443145453836423146373134364539383641373236374534434343314436342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "HEADER=If-Modified-Since:Fri, 24 Jul 2020 14:56:54 GMT",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_5");
        status = nsApi.ns_web_url ("Xmetadata_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APCASHDISCOUNTFORECAST_CDS/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034463344363938303239344134333144423038323939394139384237353735362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_5");
        status = nsApi.ns_web_url ("Xvalue_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='005056AC156F1ED689DEEB363EE38F4D',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030453732354331434342443134373144384235313234303135333344314437342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_6");
        status = nsApi.ns_web_url ("Xmetadata_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APCSHDISCUTILIZATION_CDS/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041424332363039363943363534394443414534434446334335463034443244342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_6");
        status = nsApi.ns_web_url ("Xvalue_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='FA163EC90EE61EE689C8C848FBE2558A',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044354241464634303641433034423838413341353033433838434230353733462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22Shell-home%22%2C%22Shell-home!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors,x-csrf-token", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xvalue_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("catalogue_2");
        status = nsApi.ns_web_url ("catalogue_2",
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22Shell-home%22%2C%22Shell-home!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=IDP_SESSION_MARKER_accounts"
        );

        status = nsApi.ns_end_transaction("catalogue_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Annotations__filter_Type_20e_2");
        status = nsApi.ns_web_url ("Annotations__filter_Type_20e_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/DataSources?$expand=Annotations,Attributes/UIAreas,Attributes/Annotations&$filter=Type%20eq%20%27View%27%20and%20IsInternal%20eq%20false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038303033444345324536353434343644394632463539434646414633364146332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Annotations__filter_Type_20e_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_7");
        status = nsApi.ns_web_url ("Xmetadata_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APOVRD_CDS/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046323432303343324241434234454345423337373538343233303435344630332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_7");
        status = nsApi.ns_web_url ("Xvalue_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='FA163EDF73161EE689DECB2379B7889F',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044443937394634354137423334443634424544384235463244393732394635452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Singleton_js");
        status = nsApi.ns_web_url ("Singleton_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/ssbtileslibs1/~2CF890645D9577A22F4D461AF9D912E7~C/Singleton.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037354138444537344242324134393930413843363745333631364430343843412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22Shell-home%22%2C%22Shell-home!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Singleton_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_8");
        status = nsApi.ns_web_url ("Xmetadata_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_ENGMNTPROJREVIEWQUERY_CDS/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042433542383534333344334534363838423446423832364138313233424635302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_8");
        status = nsApi.ns_web_url ("Xvalue_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='3863BB44F0201ED6BCB536E92170AF44',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044433330333039413930453534393036383144453742353437373442313538372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_5");
        status = nsApi.ns_web_url ("Results_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APCSHDISCUTILIZATION_CDS/C_APCSHDISCUTILIZATION(P_DisplayCurrency=%27EUR%27,P_StartDate=datetime%272016-01-01T00%3a00%3a00%27)/Results?sap-client=100&$skip=0&$top=1&$select=CshDiscUtilizationRatio%2cCshDiscUtilizationRatio_E&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=7200",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043413831423630443637424534303241423444313545413143393630353636302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("Xcatalogue_2");
        status = nsApi.ns_web_url ("Xcatalogue_2",
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22Shell-home%22%2C%22Shell-home!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=JSESSIONID;BIGipServereducation.hana.ondemand.com"
        );

        status = nsApi.ns_end_transaction("Xcatalogue_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersonalizedSearchMainSwitch_2");
        status = nsApi.ns_web_url ("PersonalizedSearchMainSwitch_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/PersonalizedSearchMainSwitches?$filter=Selected%20eq%20true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037313332343537464342373734393835424542373246384630423133323138322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersonalizedSearchMainSwitch_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_6");
        status = nsApi.ns_web_url ("Results_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APOVRD_CDS/C_APOVRD(P_AgingGridMeasureInDays=%2730%27,P_DisplayCurrency=%27EUR%27,P_DateFunction=%27TODAY%27,P_CriticallyOverdueThreshold=%2760%27)/Results?sap-client=100&$skip=0&$top=3&$orderby=TotalOverdueAmtInDspCrcy_E%20desc&$select=TotalOverdueAmtInDspCrcy_E%2cDisplayCurrency%2cIsCriticallyOverdue%2cOverdueCategoryName&ssbMainTileType=CT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=7200",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037423632313436363643314334463037414437303632463544363138383135312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Users___3Ccurrent_3E___2");
        status = nsApi.ns_web_url ("Users___3Ccurrent_3E___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/Users('%3Ccurrent%3E')",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032303034374631454135333934364234413738423234343038364344334334342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Users___3Ccurrent_3E___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_9");
        status = nsApi.ns_web_url ("Xmetadata_9",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/$metadata",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041433236364136333132363634454232423146394546323831444435304341332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Channels__SAP_WEBSOCKET___2");
        status = nsApi.ns_web_url ("Channels__SAP_WEBSOCKET___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata4/iwngw/notification/default/iwngw/notification_srv/0001/Channels('SAP_WEBSOCKET')",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036424232424637454435333334383231393446454643453442313843343635332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
            "URL=https://education.hana.ondemand.com/education/pub/tad/?%7B%22search%22%3A%5B%7B%22param%22%3A%22entity.hidden%22%2C%22operation%22%3A%22%3D%22%2C%22value%22%3A0%7D%2C%7B%22param%22%3A%22entity.language%22%2C%22operation%22%3A%22%3D%22%2C%22value%22%3A%22en-US%22%7D%2C%7B%22param%22%3A%22entity.h4_product%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%22SAP_S4HANA_CLOUD%22%7D%2C%7B%22param%22%3A%22entity.h4_product_version%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%222011.500%22%7D%2C%7B%22param%22%3A%22entity.context_id%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%22Shell-home%22%7D%5D%2C%22out%22%3A%5B%22entity.uid%22%2C%22entity.caption%22%2C%22entity.roles%22%2C%22entity.shortdesc%22%2C%22entity.type%22%2C%22entity.sub_type%22%2C%22entity.doc_type%22%2C%22entity.language%22%2C%22entity.clone_src%22%2C%22entity.la_category%22%5D%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://education.hana.ondemand.com/education/pub/tad/.context?%7B%22id%22%3A%22PR_3A4C8812DC44D6B0%22%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Channels__SAP_WEBSOCKET___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = HEAD
        status = nsApi.ns_start_transaction("index_2");
        status = nsApi.ns_web_url ("index_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/",
            "METHOD=HEAD",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=x-csrf-token:Fetch",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035394133443130373343443434393036413342303934383531433944444441362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("index_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("index_3");
        status = nsApi.ns_web_url ("index_3",
            "URL=https://education.hana.ondemand.com/education/pub/tad/?%7B%22search%22%3A%5B%7B%22param%22%3A%22entity.hidden%22%2C%22operation%22%3A%22%3D%22%2C%22value%22%3A0%7D%2C%7B%22param%22%3A%22entity.language%22%2C%22operation%22%3A%22%3D%22%2C%22value%22%3A%22en-US%22%7D%2C%7B%22param%22%3A%22entity.h4_product%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%22SAP_S4HANA_CLOUD%22%7D%2C%7B%22param%22%3A%22entity.h4_product_version%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%222011.500%22%7D%2C%7B%22param%22%3A%22entity.context_id%22%2C%22operation%22%3A%22like%22%2C%22value%22%3A%22Shell-home%22%7D%5D%2C%22out%22%3A%5B%22entity.uid%22%2C%22entity.caption%22%2C%22entity.roles%22%2C%22entity.shortdesc%22%2C%22entity.type%22%2C%22entity.sub_type%22%2C%22entity.doc_type%22%2C%22entity.language%22%2C%22entity.clone_src%22%2C%22entity.la_category%22%5D%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=JSESSIONID;BIGipServereducation.hana.ondemand.com"
        );

        status = nsApi.ns_end_transaction("index_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData");
        status = nsApi.ns_web_url ("CacheData",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:265",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039323633424430364137454134423942394542383739433644303542433846442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQNBL2Q64HZ5XN40O0","EvaluationId":".SAP.AP.DISCOUNTUTIL.60DAYS","ReportId":".SAP.AP.DISCOUNTUTIL.60DAYS","Data":"[{\"CshDiscUtilizationRatio\":\"0.0303\",\"CshDiscUtilizationRatio_E\":\"\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("Xcontext");
        status = nsApi.ns_web_url ("Xcontext",
            "URL=https://education.hana.ondemand.com/education/pub/tad/.context?%7B%22id%22%3A%22PR_3A4C8812DC44D6B0%22%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=JSESSIONID;BIGipServereducation.hana.ondemand.com"
        );

        status = nsApi.ns_end_transaction("Xcontext", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("report");
        status = nsApi.ns_web_url ("report",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/csp/report",
            "METHOD=POST",
            "HEADER=Content-Length:1789",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Content-Type:application/csp-report",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"csp-report":{"document-uri":"https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0","referrer":"https://my304874.s4hana.ondemand.com/ui?sap-language=EN","violated-directive":"script-src","effective-directive":"script-src","original-policy":"default-src 'self' ; script-src 'self' https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn 'unsafe-eval' ; style-src 'self' https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://*.api.here.com https://*.hereapi.cn 'unsafe-inline' ; font-src 'self' data: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://help.sap.com ; img-src 'self' https: http: data: blob: ; media-src 'self' https: http: data: blob: ; object-src blob: ; frame-src 'self' https: gap: data: blob: mailto: tel: ; worker-src 'self' blob: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn ; child-src 'self' blob: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn ; connect-src 'self' https: wss: ; base-uri 'self' ; report-uri /sap/bc/csp/report","disposition":"enforce","blocked-uri":"eval","line-number":10,"column-number":4650,"source-file":"https://ui5.sap.com","status-code":0,"script-sample":""}}",
            BODY_END,
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/appwarmup/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/resources/sap/dfa/help/wpb/files/IconFont.woff", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("report", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("manifest_json_4");
        status = nsApi.ns_web_url ("manifest_json_4",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/ghostapp/manifest.json?sap-language=EN&sap-client=100",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Accept-Language:en",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors"
        );

        status = nsApi.ns_end_transaction("manifest_json_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("InteractionEventLists_2");
        status = nsApi.ns_web_url ("InteractionEventLists_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/ESH_SEARCH_SRV/InteractionEventLists",
            "METHOD=POST",
            "HEADER=Content-Length:154",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Content-Type:application/json",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033384137333431343237384534433035393934363743334436363830363346352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ID":1,"SessionID":"82FADA0BAA1C4D5B9FD4B33707EF1EF6","Events":[{"ID":1,"Type":"SESSION_START","Timestamp":"\\/Date(1619593385937)\\/","Parameters":[]}]}",
            BODY_END,
            INLINE_URLS,
            "URL=https://help.sap.com/webassistant/context?id=23241667", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors,x-csrf-token", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("InteractionEventLists_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("context");
        status = nsApi.ns_web_url ("context",
            "URL=https://help.sap.com/webassistant/context?id=23241667",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=IDP_SESSION_MARKER_accounts",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/comp/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/fl/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/generic/app/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/generic/template/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/messagebundle_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/fl/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/generic/app/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/comp/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/table/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/library-preload.js", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/ghostapp/Component-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/table/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/messagebundle_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/f/themes/sap_fiori_3/library.css", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/comp/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/fl/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/themes/sap_fiori_3/library.css", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/table/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/themes/sap_fiori_3/library.css", END_INLINE
        );

        status = nsApi.ns_end_transaction("context", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_2");
        status = nsApi.ns_web_url ("CacheData_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:515",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030313542354435353035393934383839414333453536434442363139394243442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQNBL2P1S1DK0H76GQ","EvaluationId":".SFIN.AP.OVRDPAYABLES.TODAY","ReportId":".SFIN.AP.OVRDPAYABLES.TODAY","Data":"[{\"DisplayCurrency\":\"EUR\",\"IsCriticallyOverdue\":\"X\",\"OverdueCategoryName\":\"Critical Overdue\",\"TotalOverdueAmtInDspCrcy_E\":\"19788602.37\"},{\"DisplayCurrency\":\"EUR\",\"IsCriticallyOverdue\":\"\",\"OverdueCategoryName\":\"Uncritical Overdue\",\"TotalOverdueAmtInDspCrcy_E\":\"12180556.76\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2,"HanaCacheAge":58021}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_3");
        status = nsApi.ns_web_url ("CacheData_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:494",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041453238413637304231344434453430384235453544454533393946334433432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQNBL2P1S1DK0H76GQ","EvaluationId":".SFIN.AP.OVRDPAYABLES.TODAY","ReportId":".SFIN.AP.OVRDPAYABLES.TODAY","Data":"[{\"DisplayCurrency\":\"EUR\",\"IsCriticallyOverdue\":\"X\",\"OverdueCategoryName\":\"Critical Overdue\",\"TotalOverdueAmtInDspCrcy_E\":\"19788602.37\"},{\"DisplayCurrency\":\"EUR\",\"IsCriticallyOverdue\":\"\",\"OverdueCategoryName\":\"Uncritical Overdue\",\"TotalOverdueAmtInDspCrcy_E\":\"12180556.76\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_9");
        status = nsApi.ns_web_url ("library_parameters_json_9",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/f/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_10");
        status = nsApi.ns_web_url ("library_parameters_json_10",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_11");
        status = nsApi.ns_web_url ("library_parameters_json_11",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/comp/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_12");
        status = nsApi.ns_web_url ("library_parameters_json_12",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/fl/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_13");
        status = nsApi.ns_web_url ("library_parameters_json_13",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/unified/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_14");
        status = nsApi.ns_web_url ("library_parameters_json_14",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/table/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8"
        );

        status = nsApi.ns_end_transaction("library_parameters_json_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_4");
        status = nsApi.ns_web_url ("CacheData_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:259",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030453030314645423037413834433833383530314238334145423544334141442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQNBJCF93S8K7ZFNTQ","EvaluationId":".SAP.AP.FLEXAGING.AMOUNT","ReportId":".SAP.AP.FLEXAGING.AMOUNT","Data":"[{\"DisplayCurrency\":\"EUR\",\"AmountInDisplayCurrency_E\":\"72531617.98\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_4", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("library_parameters_json_15");
        status = nsApi.ns_web_url ("library_parameters_json_15",
            "URL=https://ui5.sap.com/1.81.6/resources/sap/uxap/themes/sap_fiori_3/library-parameters.json",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            INLINE_URLS,
            "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/ghostapp/metadata.xml", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:maxdataserviceversion,sap-cancel-on-close,sap-contextid-accept", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ushell/plugins/ghostapp/metadata.xml", "HEADER=MaxDataServiceVersion:3.0", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Accept-Language:en", "HEADER=sap-cancel-on-close:true", "HEADER=sap-contextid-accept:header", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/ListReport/i18n/i18n_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/ListReport/i18n/i18n_en.properties", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/generic/template/lib/i18n/i18n_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("library_parameters_json_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("sap_ui_version_json_2");
        status = nsApi.ns_web_url ("sap_ui_version_json_2",
            "URL=https://ui5.sap.com/1.81.6/resources/sap-ui-version.json"
        );

        status = nsApi.ns_end_transaction("sap_ui_version_json_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__U___2");
        status = nsApi.ns_web_url ("PersContainers_category__U___2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='U',id='sapui5.history.HistorySettings')?$expand=PersContainerItems",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038424446313334343338374134463942424435333446303538453138393437302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__U___2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("settings");
        status = nsApi.ns_web_url ("settings",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/lrep/flex/settings",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-CSRF-Token:fetch",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036454630344532423838364234414634393338414335393139353733383336342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("settings", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__U___3");
        status = nsApi.ns_web_url ("PersContainers_category__U___3",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='U',id='ui5.13f6ca72.0266.426b.837c.d7dba02f79a5')?$expand=PersContainerItems",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E64657465726D696E65645F737461727475705F302020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041413136444137363142444634414235393541324235343142393230463445352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__U___3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_7");
        status = nsApi.ns_web_url ("Results_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBJCF93S8K7ZFNTQ%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038384138433245464342423234434445393739393432423343373632424232352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_9");
        status = nsApi.ns_web_url ("Xvalue_9",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.AP.FLEXAGING.AMOUNT')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038374543384633334339323934394143414435364438454630424431423441462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_8");
        status = nsApi.ns_web_url ("Results_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBJCDMWNWPJIXT3Y%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043424139373435324332444534454144423942434536323845353343413744332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_9");
        status = nsApi.ns_web_url ("Results_9",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQCDT6UFTR2HDIE288%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202031444434303443334544304134454133413131313539393235413639393739312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_10");
        status = nsApi.ns_web_url ("Xvalue_10",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.FI.AR.OVERDUERECEIVABLES.TODAY')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032444543334441423942423734324438383345304632373845303144443339382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_11");
        status = nsApi.ns_web_url ("Xvalue_11",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.FI.AR.TOTALRECEIVABLES.TODAY')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045313442363036354534383734393236393539454130344137453232393137352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_10");
        status = nsApi.ns_web_url ("Results_10",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQCDT6UFRYITFN8PSN%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037434333304145393145333134413634413335303644453145423730354444382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_11");
        status = nsApi.ns_web_url ("Results_11",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQNBJCDO8W6RIUEQGT%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045364136443738413330354134353330424536304334333845384239433430352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_12");
        status = nsApi.ns_web_url ("Results_12",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQCDT6UFR6JZ6HFM6V%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202032454542443130353146393234433744383442383036454544463732433831342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_10");
        status = nsApi.ns_web_url ("Xmetadata_10",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APFLEXIBLEAGING_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130001046",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030324230394532393736334634353436414634464431393238313535463832352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_12");
        status = nsApi.ns_web_url ("Xvalue_12",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='FA163EC90EE61ED689DEBF732C0D5AAA',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130001046",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041383845413743344438303634303446383338443542384131463242323337442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_13");
        status = nsApi.ns_web_url ("Xvalue_13",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.FI.AR.FUTURERECEIVABLES.TODAY')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036364639393836383843353934333239383933463632373530394444334544362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_14");
        status = nsApi.ns_web_url ("Xvalue_14",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.FI.AR.DUNNINGLVLDISTR.TODAY')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039383444364544343139453934354430423039383231374330313230343039312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_15");
        status = nsApi.ns_web_url ("Xvalue_15",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.FI.AR.DAYSSALESOUTSTDG.LAST12M')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202037354339393136454230413434434137413439433131373044323633443639352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_13");
        status = nsApi.ns_web_url ("Results_13",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_APFLEXIBLEAGING_CDS/C_APFLEXIBLEAGING(P_AgingGridMeasureInDays=%2730%27,P_DisplayCurrency=%27EUR%27,P_NumberOfAgingGridColumns=%274%27,P_DateFunction=%27TODAY%27,P_ExchangeRateType=%27M%27)/Results?sap-client=100&$skip=0&$top=1&$select=AmountInDisplayCurrency_E%2cDisplayCurrency&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=7200",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202045423742304235314531373734433539414232424432314342383435383932412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_11");
        status = nsApi.ns_web_url ("Xmetadata_11",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_OVERDUEACCTRBLS_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130002335",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033313745454533383935444334424342423136424231303436334431454239382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_16");
        status = nsApi.ns_web_url ("Xvalue_16",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='005056B2532A1ED697F2796CE8DC4057',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130002335",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039383241373431454633343234343432394639344538353944463833333246462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_16", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_12");
        status = nsApi.ns_web_url ("Xmetadata_12",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_TOTALACCOUNTSRECEIVABLES_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130003538",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202033383130303835343343464134353744383841393035383438353346354541462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_17");
        status = nsApi.ns_web_url ("Xvalue_17",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='42F2E9AFBE7F1EE699AB75FEC6785ABF',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130003538",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038383241353839363635383234463732393443444230343631343835413231372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_17", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_14");
        status = nsApi.ns_web_url ("Results_14",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_OVERDUEACCTRBLS_CDS/C_OVERDUEACCTRBLS(P_DisplayCurrency=%27USD%27,P_ExchangeRateType=%27M%27,P_NetDueInterval3InDays=%2790%27,P_NetDueInterval1InDays=%2730%27,P_NetDueInterval2InDays=%2760%27,P_DateFunction=%27TODAY%27)/Results?sap-client=100&$skip=0&$top=1&$select=OverdueReceivablesRatio%2cOverdueReceivablesRatio_E&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=7200",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202043323341383135323441314434384632384441364134343434423546454146462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_15");
        status = nsApi.ns_web_url ("Results_15",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_TOTALACCOUNTSRECEIVABLES_CDS/C_TOTALACCOUNTSRECEIVABLES(P_DisplayCurrency=%27USD%27,P_ExchangeRateType=%27M%27,P_NetDueInterval3InDays=%2790%27,P_NetDueInterval1InDays=%2730%27,P_NetDueInterval2InDays=%2760%27,P_DateFunction=%27TODAY%27)/Results?sap-client=100&$skip=0&$top=1&$select=TotalAmountInDisplayCrcy%2cDisplayCurrency&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=7200",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039423039433441383342443234443838394641354230443430313446424632362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_13");
        status = nsApi.ns_web_url ("Xmetadata_13",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_DUNNINGLEVELDISTRIBUTION_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130001723",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202031323844383746373241433834463042383942393634423141393936464439452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_18");
        status = nsApi.ns_web_url ("Xvalue_18",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='3863BB44F0201ED699AB64EF55B795D8',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130001723",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034423939334133363436343734303744413639394333323232433238453042332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_18", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_14");
        status = nsApi.ns_web_url ("Xmetadata_14",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_FUTUREACCTRBLS_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130001827",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035323937304536343631393634313942383135463543443733463634353037422020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_19");
        status = nsApi.ns_web_url ("Xvalue_19",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='3863BB44F0201ED699AB6CBDC06035E2',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130001827",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202030384333384444434636363634453437383236334443414334393939384334362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_19", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_15");
        status = nsApi.ns_web_url ("Xmetadata_15",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_DAYSSALESOUTSTANDING_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130001602",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035354445374637373833353034353432393931303242463936333532303231412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_20");
        status = nsApi.ns_web_url ("Xvalue_20",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='3863BB44F0201ED699AB5409C725354E',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130001602",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202042334139304643424332334134453436413844444544324636354635413036412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_20", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_5");
        status = nsApi.ns_web_url ("CacheData_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:232",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034353237453141344543424334434543414236313131323845424332463144352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQNBJCDMWNWPJIXT3Y","EvaluationId":".SAP.FI.AR.OVERDUERECEIVABLES.TODAY","Data":"[{\"OverdueReceivablesRatio\":\"0.7924\",\"OverdueReceivablesRatio_E\":\"\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_16");
        status = nsApi.ns_web_url ("Results_16",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%2700O2TPKTQCDUX3KWVM5BVO033%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038363944393234353241383834394343423037413332453246333639373844352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_16", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_21");
        status = nsApi.ns_web_url ("Xvalue_21",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SFIN.CASHMGR.CASHPOSITIONVAR')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046353236453336303333304634354638424141363536444237374343463742302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_21", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_6");
        status = nsApi.ns_web_url ("CacheData_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:230",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046303832303441444546424234443735383433463835454638464443323742392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQCDT6UFTR2HDIE288","EvaluationId":".SAP.FI.AR.TOTALRECEIVABLES.TODAY","Data":"[{\"DisplayCurrency\":\"USD\",\"TotalAmountInDisplayCrcy\":\"554913569.12\"}]","CacheMaxAge":2,"CacheMaxAgeUnit":"HUR","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_7");
        status = nsApi.ns_web_url ("CacheData_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:226",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035323534364445363042334434413936393843314132433046313338453234302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"00O2TPKTQCDUX3KWVM5BVO033","EvaluationId":".SFIN.CASHMGR.CASHPOSITIONVAR","Data":"[{\"DisplayCurrency\":\"EUR\",\"AmountInDisplayCurrency\":\"1063404925.90\"}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_16");
        status = nsApi.ns_web_url ("Xmetadata_16",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_CASHPOSITIONANALYTICS_CDS/$metadata?sap-client=100&sap-language=EN&sap-context-token=20201023060917",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202035394538384538344538353634314444414336463734364431343739423738342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_16", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_22");
        status = nsApi.ns_web_url ("Xvalue_22",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='005056AC156F1EE68FD4FF11A7887F83',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210428000206",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046453445364636324443343234444545384130453244463633314339464544412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_22", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_17");
        status = nsApi.ns_web_url ("Results_17",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_CASHPOSITIONANALYTICS_CDS/C_CashPositionAnalytics(P_ExchangeRateType=%27M%27,P_DisplayCurrency=%27EUR%27)/Results?sap-client=100&$skip=0&$top=1&$select=AmountInDisplayCurrency%2cDisplayCurrency&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=59",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034443437433431323639343534433346383545463931424545453536313242342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_17", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_8");
        status = nsApi.ns_web_url ("CacheData_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:188",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202032374337434133323736304434364635393744323844394139333736324438412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA0CKPUE9ZPNORCX","EvaluationId":".SAP.QM.INSPECTION.ANALYTICS.INSPLOTE1","Data":"[{\"InspectionLotCount\":17}]","CacheMaxAge":15,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_6");
        status = nsApi.ns_web_url ("Xcount_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/SD_F1873_SO_WL_SRV/C_SalesOrderWl_F1873/$count?sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202041343341353146343133304534373738393837353337313346303933384538362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_7");
        status = nsApi.ns_web_url ("Xcount_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SAP/SD_CUSTOMER_INVOICES_CREATE/C_BillingDueListItem_F0798/$count?sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202038344543453344333443393934373439423132394638433537433445344546322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_18");
        status = nsApi.ns_web_url ("Results_18",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%276FMR9SFQJTA1Z3KNWJNN6JXRB%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202031424531364641384139314334374537414641463634454441453945433635352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_18", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_23");
        status = nsApi.ns_web_url ("Xvalue_23",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.E.1472787291393')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202046413032304535344238363934334445393241363244434439464542323542362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_23", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_17");
        status = nsApi.ns_web_url ("Xmetadata_17",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_SALESVOLUMEBYYEARQUERY_CDS/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202032344632384238443643443734303638383035333441393042464335464637382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_17", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_24");
        status = nsApi.ns_web_url ("Xvalue_24",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='42F2E9AFC4EF1EE69C97D9F01C8587A8',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202045463945373945303836363134363039424244373141363239463835373737462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_24", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_19");
        status = nsApi.ns_web_url ("Results_19",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheParameters(P_CacheType=2)/Results?$filter=ChipId%20eq%20%276FMR9SFQJTA0CKPUE9ZPNORCX%27",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202041384133423242303241324134353035423438383741373331383337443531302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_19", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__P___5");
        status = nsApi.ns_web_url ("PersContainers_category__P___5",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='P',id='sap.ushell.UserDefaultParameter')?$expand=PersContainerItems&sap-cache-id=F592CF41BD57C267A659FE68345CE67D",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202030323742353946343546413234383142394432353131463743393236444235322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__P___5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_25");
        status = nsApi.ns_web_url ("Xvalue_25",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.SAP.QM.INSPECTION.ANALYTICS.INSPLOTE1')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202041364531423536393731343134334242383646333430433837344435344538412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_25", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_26");
        status = nsApi.ns_web_url ("Xvalue_26",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/AppDescriptors(EvaluationId='.E.1493814800439')/$value",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202043433030383943453839373334373638423545374533364330443135453732372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_26", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_18");
        status = nsApi.ns_web_url ("Xmetadata_18",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/QM_INSPLOTDEF_ANALYZE_SRV/$metadata?sap-client=100&sap-language=EN&sap-context-token=20210130013303",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202031383034314543313233373234394344423138353435443937364642414132322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_18", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_27");
        status = nsApi.ns_web_url ("Xvalue_27",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='40F2E9AFC5011EE883923638F8E00940',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210130013303",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202033374641373834303744443634374531383137314638334337354337414131432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_27", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_20");
        status = nsApi.ns_web_url ("Results_20",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/QM_INSPLOTDEF_ANALYZE_SRV/C_InspectionLotKeyFigure(P_DateFunction=%27PREVIOUS365DAYS%27)/Results?sap-client=100&$skip=0&$top=1&$select=InspectionLotCount&ssbMainTileType=NT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=900",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202031463832363741353330443234393744393245333043374533333534303034312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_20", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_19");
        status = nsApi.ns_web_url ("Xmetadata_19",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/SD_SOFI/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202039374432363931373632333534334431393936443635364641333132314145372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_19", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_28");
        status = nsApi.ns_web_url ("Xvalue_28",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/catalogservice;v=2/Annotations(TechnicalName='40F2E9AFC5F81EE78BFFA2046236D1A2',Version='0001')/$value?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230302D616374696F6E2D72656D6F76655F6D6F757365646F776E5F3120202020200005756E64657465726D696E656440302E302E30202020202020202020202020202036363534383744354532364634374130383242443732433146354432453130362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xvalue_28", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_9");
        status = nsApi.ns_web_url ("CacheData_9",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:422",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036463031453744323736313834383538423634464146353344354430433636302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA1Z3KNWJNN6JXRB","EvaluationId":".E.1493814800439","Data":"[{\"Issue\":\"SO05\",\"Issue_Text\":\"Delivery Issue in Sales Orders\",\"NmbrOfAllIssues\":4800},{\"Issue\":\"SO01\",\"Issue_Text\":\"Incomplete Data in Sales Orders\",\"NmbrOfAllIssues\":81},{\"Issue\":\"SO02\",\"Issue_Text\":\"Delivery Block in Sales Orders\",\"NmbrOfAllIssues\":46}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xcount_8");
        status = nsApi.ns_web_url ("Xcount_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SAP/MM_SUPPLIER_INVOICE_LIST_ENH_SRV/C_SupplierInvoiceList/$count?sap-language=EN",
            "HEADER=Pragma:no-cache",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=Cache-Control:no-cache, no-store, must-revalidate",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E656440302E302E30202020202020202020202020202034324544423346343937384334433043394233434338364536443041463737462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Expires:0",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xcount_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Results_21");
        status = nsApi.ns_web_url ("Results_21",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/C_SALESVOLUMEBYYEARQUERY_CDS/C_SALESVOLUMEBYYEARQUERY(P_DisplayCurrency=%27USD%27,P_ExchangeRateType=%27M%27)/Results?sap-client=100&$skip=0&$top=3&$orderby=BillingDocumentDateYear%20desc&$select=SlsVolumeNetAmtInDspCrcy%2cDisplayCurrency%2cBillingDocumentDateYear%2cBillingDocumentDateYear_T&ssbMainTileType=CT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E656440302E302E30202020202020202020202020202046344635363634423536393534323639394132443546344331433339413835462020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Results_21", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("C_SlsDocFlfmtIssue");
        status = nsApi.ns_web_url ("C_SlsDocFlfmtIssue",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/SD_SOFI/C_SlsDocFlfmtIssue?sap-client=100&$skip=0&$top=3&$orderby=NmbrOfAllIssues%20desc&$select=NmbrOfAllIssues%2cIssue%2cIssue_Text&ssbMainTileType=CT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=59",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E656440302E302E30202020202020202020202020202000005341505F4532455F54415F557365722020202020202020202020202020202020756E646566696E6564202020202020202020202020202020202020202020202020202020202020200005756E64657465726D696E656440302E302E30202020202020202020202020202035343131343039443636453434313246424342353239393943323035344337312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("C_SlsDocFlfmtIssue", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("C_SlsDocFlfmtIssue_2");
        status = nsApi.ns_web_url ("C_SlsDocFlfmtIssue_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/SD_SOFI/C_SlsDocFlfmtIssue?sap-client=100&$skip=0&$top=3&$orderby=NmbrOfAllIssues%20desc&$select=NmbrOfAllIssues%2cIssue%2cIssue_Text&ssbMainTileType=CT",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=Cache-Control:max-age=59",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202039454437363843453035354534344343423734394136363038333436363039392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("C_SlsDocFlfmtIssue_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_10");
        status = nsApi.ns_web_url ("CacheData_10",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:422",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202034434133464343314339303534314537393539384146443345333444464433412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA1Z3KNWJNN6JXRB","EvaluationId":".E.1493814800439","Data":"[{\"Issue\":\"SO05\",\"Issue_Text\":\"Delivery Issue in Sales Orders\",\"NmbrOfAllIssues\":4800},{\"Issue\":\"SO01\",\"Issue_Text\":\"Incomplete Data in Sales Orders\",\"NmbrOfAllIssues\":81},{\"Issue\":\"SO02\",\"Issue_Text\":\"Delivery Block in Sales Orders\",\"NmbrOfAllIssues\":46}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_11");
        status = nsApi.ns_web_url ("CacheData_11",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:422",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044433233324139453838333234383946414138414331363537433145434438412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA1Z3KNWJNN6JXRB","EvaluationId":".E.1493814800439","Data":"[{\"Issue\":\"SO05\",\"Issue_Text\":\"Delivery Issue in Sales Orders\",\"NmbrOfAllIssues\":4800},{\"Issue\":\"SO01\",\"Issue_Text\":\"Incomplete Data in Sales Orders\",\"NmbrOfAllIssues\":81},{\"Issue\":\"SO02\",\"Issue_Text\":\"Delivery Block in Sales Orders\",\"NmbrOfAllIssues\":46}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_12");
        status = nsApi.ns_web_url ("CacheData_12",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:422",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202044384545353544463034314234343044414633344639433635354342393034352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA1Z3KNWJNN6JXRB","EvaluationId":".E.1493814800439","Data":"[{\"Issue\":\"SO05\",\"Issue_Text\":\"Delivery Issue in Sales Orders\",\"NmbrOfAllIssues\":4800},{\"Issue\":\"SO01\",\"Issue_Text\":\"Incomplete Data in Sales Orders\",\"NmbrOfAllIssues\":81},{\"Issue\":\"SO02\",\"Issue_Text\":\"Delivery Block in Sales Orders\",\"NmbrOfAllIssues\":46}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_13");
        status = nsApi.ns_web_url ("CacheData_13",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            "HEADER=Content-Length:422",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000756E64657465726D696E6564202020202020202020202020202020202020202000005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C653230322D616374696F6E2D72656D6F76655F6D6F75736575705F32202020202020200005756E64657465726D696E6564202020202020202020202020202020202020202036363639383745454131413534434241413935383744414244343941383437422020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"ChipId":"6FMR9SFQJTA1Z3KNWJNN6JXRB","EvaluationId":".E.1493814800439","Data":"[{\"Issue\":\"SO05\",\"Issue_Text\":\"Delivery Issue in Sales Orders\",\"NmbrOfAllIssues\":4800},{\"Issue\":\"SO01\",\"Issue_Text\":\"Incomplete Data in Sales Orders\",\"NmbrOfAllIssues\":81},{\"Issue\":\"SO02\",\"Issue_Text\":\"Delivery Block in Sales Orders\",\"NmbrOfAllIssues\":46}]","CacheMaxAge":1,"CacheMaxAgeUnit":"MIN","CacheType":2}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("CacheData_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_14");
        status = nsApi.ns_web_url ("CacheData_14",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("CacheData_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("CacheData_15");
        status = nsApi.ns_web_url ("CacheData_15",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV/CacheData",
            "METHOD=POST",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject", END_INLINE
        );

        status = nsApi.ns_end_transaction("CacheData_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("manifest_appdescr_3");
        status = nsApi.ns_web_url ("manifest_appdescr_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/lrep/content/~20201107164207.2351840~/apps/cus.cpm.projengagement.maint/app/sap/cpm_proj_mans1/manifest.appdescr?sap-language=EN&sap-client=100",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3636333433373138433842453134383038423442304330374239433339353444452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("manifest_appdescr_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(0.074);

        //Page Auto splitted for Link 'image' Clicked by User
        status = nsApi.ns_start_transaction("Component_preload_js_5");
        status = nsApi.ns_web_url ("Component_preload_js_5",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_proj_mans1/~AC12D786B4EC4C4EDD88C87861B2E0F5~5/Component-preload.js",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/featuretoggles1/~87EB921D92F4682F9706033A85E7E49F~5/library-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("Component_preload_js_5", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_20");
        status = nsApi.ns_web_url ("Xmetadata_20",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$metadata?sap-client=100&sap-documentation=heading&sap-language=EN&sap-context-token=20210130000226",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3635433837443045424632334134423641394538393444383630394643313931392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_20", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_21");
        status = nsApi.ns_web_url ("Xmetadata_21",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/sap/RSH_PS_MATCH_EXTERNAL_SRV/$metadata?sap-client=100&sap-value-list=none&sap-language=EN&sap-context-token=20210126105439",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3641303238394335364241453334413438394431443441324637314643394543312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_21", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xvalue_29");
        status = nsApi.ns_web_url ("Xvalue_29",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/iwfnd/CATALOGSERVICE;v=0002/Annotations(TechnicalName='RSH_PS_MATCH_EXTERNAL_ANNO_MDL',Version='0001')/$value?sap-language=EN&sap-client=100&sap-context-token=20210126105439",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3642383039454143364137453934434643424144454533423335393731383239352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/collaboration/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/commons/library-preload.js", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/commons/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/suite/ui/commons/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/collaboration/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xvalue_29", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("BaseController_js");
        status = nsApi.ns_web_url ("BaseController_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/controller/BaseController.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3639383339423042393032353134454631383637323132343930303435393730322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("BaseController_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("formatter_js");
        status = nsApi.ns_web_url ("formatter_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/formatter.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3635423330314232364144303334453031413338383235444537443235394338392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("formatter_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("MessageProcessor_js");
        status = nsApi.ns_web_url ("MessageProcessor_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/MessageProcessor.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3632374136433535363343343334313033393438413430373534343837303235442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("MessageProcessor_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("BusinessCardHelper_js");
        status = nsApi.ns_web_url ("BusinessCardHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/BusinessCardHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3632303043453434423530314334453745424439443841444541323430424343302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("BusinessCardHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("BaseHelper_js");
        status = nsApi.ns_web_url ("BaseHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/BaseHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3641393341424241344534373234393146423538364236373033323932303633442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("BaseHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("NavigationHelper_js");
        status = nsApi.ns_web_url ("NavigationHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/NavigationHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3633464346343236313336304234383732423939423430424139433930464241452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("NavigationHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("StaffingUtilHelper_js");
        status = nsApi.ns_web_url ("StaffingUtilHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/StaffingUtilHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3634314444433832384244373034413638424644453038354332443837463446362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("StaffingUtilHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardTextUtil_js");
        status = nsApi.ns_web_url ("RateCardTextUtil_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardTextUtil.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3644424335434433434233383534383231424639353938413831323732344334412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardTextUtil_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardInitParameters_js");
        status = nsApi.ns_web_url ("RateCardInitParameters_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardInitParameters.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3643354541343241443841373734414642423037383837374330384333324433412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardInitParameters_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("flp_sap_fesr_only_2");
        status = nsApi.ns_web_url ("flp_sap_fesr_only_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/flp;sap-fesr-only",
            "METHOD=POST",
            "HEADER=Content-Length:1317",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Content-Type:application/x-www-form-urlencoded;charset=utf-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("flp_sap_fesr_only_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardFragmentController_j");
        status = nsApi.ns_web_url ("RateCardFragmentController_j",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardFragmentController.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3638433343413538323741313634424145393544314446453734304641394136372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardFragmentController_j", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardPersonalizationServi");
        status = nsApi.ns_web_url ("RateCardPersonalizationServi",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/model/rateCards/RateCardPersonalizationService.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3644444631343632413932453634434132393439423530303044323630324631312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardPersonalizationServi", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardBO_js");
        status = nsApi.ns_web_url ("RateCardBO_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardBO.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3646363141384336453441463834444234414434353132363936463043384632432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardBO_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardProvider_js");
        status = nsApi.ns_web_url ("RateCardProvider_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/model/RateCardProvider.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3633374442363241423433443034383943414136354241363230363338443743342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardProvider_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardFormatter_js");
        status = nsApi.ns_web_url ("RateCardFormatter_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardFormatter.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3646353638374344323530423734463246393431324446313131374438433441442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardFormatter_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardUtil_js");
        status = nsApi.ns_web_url ("RateCardUtil_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardUtil.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3631354633353132303242413934413343393041373146323836343446424535382020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("RateCardUtil_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("RateCardMessageProcessor_js");
        status = nsApi.ns_web_url ("RateCardMessageProcessor_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/md_cndprspl_ms1/~A00B6BF1E7DAA0B672076C32D06D6E94~5/util/RateCardMessageProcessor.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3633313638373739334133453734363443394339334346414236394445464534392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ca/ui/message/message.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ca/ui/dialog/factory.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ca/ui/dialog/Dialog.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ca/ui/utils/resourcebundle.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ca/ui/i18n/i18n_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("RateCardMessageProcessor_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ListSelector_js");
        status = nsApi.ns_web_url ("ListSelector_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/ListSelector.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3638303834423144314538464534413746424642383632463330453541454142342020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_proj_mans1/~AC12D786B4EC4C4EDD88C87861B2E0F5~5/util/style.css", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("ListSelector_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_22");
        status = nsApi.ns_web_url ("Xmetadata_22",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/smi/rest_tunnel/Jam/api/v1/OData/$metadata?sap-client=100&sap-language=EN",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3632413845333931343230443734454246393345453936413535303046374130312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=sap-contextid-accept:header",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_22", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ToggleStatusSet");
        status = nsApi.ns_web_url ("ToggleStatusSet",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SAP/CA_FM_FEATURE_TOGGLE_STATUS_SRV/ToggleStatusSet?$format=json",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3641373735414635334339393934333236413339314439323746373432394641372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ToggleStatusSet", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ExtensionProvider_js");
        status = nsApi.ns_web_url ("ExtensionProvider_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/ExtensionProvider.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3635433044423433463843304334343745383744374133433242443232413138372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ExtensionProvider_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_6");
        status = nsApi.ns_web_url ("Xbatch_6",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:346",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_4872-9282-a08c",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3639333844314134373030453834384634394430393937344444464335383933322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_4872-9282-a08c
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CheckStaffingScenario?sap-client=100 HTTP/1.1
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22CustomerProject-maintainCustomerProject%22%2C%22CustomerProject-maintainCustomerProject!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors", "HEADER=Sec-Fetch-Site:same-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22CustomerProject-maintainCustomerProject%22%2C%22CustomerProject-maintainCustomerProject!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D", "METHOD=OPTIONS", "HEADER=Access-Control-Request-Method:GET", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Access-Control-Request-Headers:access-control-allow_cors,x-csrf-token", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_6", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("catalogue_3");
        status = nsApi.ns_web_url ("catalogue_3",
            "URL=https://help.sap.com/webassistant/catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22system%22%3A%5Bnull%2C%22%22%5D%2C%22appUrl%22%3A%5B%22CustomerProject-maintainCustomerProject%22%2C%22CustomerProject-maintainCustomerProject!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:cross-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=IDP_SESSION_MARKER_accounts"
        );

        status = nsApi.ns_end_transaction("catalogue_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Project_js");
        status = nsApi.ns_web_url ("Project_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/model/Project.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3646333839364641464231424334323138423044313438304139463341414645442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Project_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("KPIHelper_js");
        status = nsApi.ns_web_url ("KPIHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/KPIHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3630303336324633303032384234413242394342363846454543344130313434442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("KPIHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for application/json type
        status = nsApi.ns_start_transaction("Xcatalogue_3");
        status = nsApi.ns_web_url ("Xcatalogue_3",
            "URL=https://education.hana.ondemand.com/education/pub/tad/.catalogue?%7B%22product%22%3A%22SAP_S4HANA_CLOUD%22%2C%22version%22%3A%222011.500%22%2C%22appUrl%22%3A%5B%22CustomerProject-maintainCustomerProject%22%2C%22CustomerProject-maintainCustomerProject!whatsnew%22%5D%2C%22locale%22%3A%5B%22en-US%22%2C%22en-GB%22%5D%2C%22profile%22%3A%22%22%7D",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Access-Control-Allow_CORS:true",
            "HEADER=Sec-Fetch-Site:same-site",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=JSESSIONID;BIGipServereducation.hana.ondemand.com"
        );

        status = nsApi.ns_end_transaction("Xcatalogue_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ProjectObjectHeader_fragment");
        status = nsApi.ns_web_url ("ProjectObjectHeader_fragment",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/fragment/ProjectObjectHeader.fragment.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3631453338353030343339394434393235393538354536334232333735454643322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/ui/layout/messagebundle_en.properties", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE
        );

        status = nsApi.ns_end_transaction("ProjectObjectHeader_fragment", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Attachment_fragment_xml");
        status = nsApi.ns_web_url ("Attachment_fragment_xml",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/fragment/Attachment.fragment.xml",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3641304333423635314444363534423132383239443134393142373732464543432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Attachment_fragment_xml", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("Xmetadata_23");
        status = nsApi.ns_web_url ("Xmetadata_23",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/SAP/SD_SO_MAINT_F0719_SRV/$metadata",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Accept-Language:en",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3635343333363242414535433134444232393935313443333333344646344435432020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("Xmetadata_23", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_7");
        status = nsApi.ns_web_url ("Xbatch_7",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:341",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_b38f-f452-8b8e",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3631363942443443323146453434353338423435363935413739333044354242322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_b38f-f452-8b8e
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET IsResourceMapped?sap-client=100 HTTP/1.1
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_7", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("PersContainers_category__U___4");
        status = nsApi.ns_web_url ("PersContainers_category__U___4",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers(category='U',id='sap.ushell.services.UserRecents')?$expand=PersContainerItems",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=sap-client:100",
            "HEADER=X-CSRF-Token:Fetch",
            "HEADER=Accept-Language:EN",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3634443534313742433537393534373245384633393832443236333335384134322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("PersContainers_category__U___4", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("BaseMasterController_js");
        status = nsApi.ns_web_url ("BaseMasterController_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/controller/BaseMasterController.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3645303241334532463146333834464539414341373946354643384541394238422020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("BaseMasterController_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("GroupSortState_js");
        status = nsApi.ns_web_url ("GroupSortState_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/model/GroupSortState.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3643333033383939453542344534353341414433364535363141343641384631392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("GroupSortState_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("ListBindingHelper_js");
        status = nsApi.ns_web_url ("ListBindingHelper_js",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/util/ListBindingHelper.js",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3630313633373530373741413234353339423442343531393439314446444630372020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("ListBindingHelper_js", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("PersContainers_2");
        status = nsApi.ns_web_url ("PersContainers_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/UI2/INTEROP/PersContainers",
            "METHOD=POST",
            "HEADER=Content-Length:3537",
            "HEADER=MaxDataServiceVersion:3.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=X-CSRF-Token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=Accept-Language:EN",
            "HEADER=Content-Type:application/json",
            "HEADER=sap-language:EN",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=sap-client:100",
            "HEADER=DataServiceVersion:1.0",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3646373236384439314531383734433742393638314335414641344130433643322020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            BODY_BEGIN,
                "{"__metadata":{"id":"https://my304874.s4hana.ondemand.com:443/sap/opu/odata/UI2/INTEROP/PersContainers(id='sap.ushell.services.UserRecents',category='U')","uri":"https://my304874.s4hana.ondemand.com:443/sap/opu/odata/UI2/INTEROP/PersContainers(id='sap.ushell.services.UserRecents',category='U')","type":"INTEROP.PersContainer"},"id":"sap.ushell.services.UserRecents","category":"U","validity":0,"clientExpirationTime":"\/Date(253373423400000)\/","component":"","appName":"","PersContainerItems":[{"value":"{\"recentDay\":\"2021/4/28\",\"recentUsageArray\":[{\"oItem\":{\"title\":\"Customer Projects\",\"appType\":\"Application\",\"url\":\"#CustomerProject-maintainCustomerProject\",\"appId\":\"#CustomerProject-maintainCustomerProject\",\"timestamp\":1619593465146,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619593465146,\"aUsageArray\":[10,1],\"iCount\":11},{\"oItem\":{\"title\":\"Trial Center\",\"appType\":\"Application\",\"url\":\"#ProductTrialOffer-displayTrialCenter\",\"appId\":\"#ProductTrialOffer-displayTrialCenter\",\"timestamp\":1619593058963,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619593058963,\"aUsageArray\":[1,16,1],\"iCount\":18},{\"oItem\":{\"title\":\"Manage Sales Orders\",\"appType\":\"Application\",\"url\":\"#SalesOrder-manageSalesOrder?preferredMode=create\",\"appId\":\"#SalesOrder-manageSalesOrder\",\"timestamp\":1619522537819,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619522537819,\"aUsageArray\":[3,0],\"iCount\":3},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"customer activity repository\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"customer activity repository\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508376201,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508376201,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"IDE Full-Stack\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"IDE Full-Stack\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508141324,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508141324,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"appId\":\"#Action-search\",\"appType\":\"Search\",\"title\":\"Full\",\"url\":\"#Action-search&/top=10&filter={\\\"dataSource\\\":{\\\"type\\\":\\\"Category\\\",\\\"id\\\":\\\"All\\\",\\\"label\\\":\\\"All\\\",\\\"labelPlural\\\":\\\"All\\\"},\\\"searchTerm\\\":\\\"Full\\\",\\\"rootCondition\\\":{\\\"type\\\":\\\"Complex\\\",\\\"operator\\\":\\\"And\\\",\\\"conditions\\\":[]}}\",\"timestamp\":1619508111215,\"icon\":\"sap-icon://search\"},\"iTimestamp\":1619508111215,\"aUsageArray\":[1,0],\"iCount\":1},{\"oItem\":{\"title\":\"Trial Center\",\"appType\":\"Application\",\"url\":\"#ProductTrialOffer-displayTrialCenterDesktop&/s4hana_us/tours\",\"appId\":\"#ProductTrialOffer-displayTrialCenterDesktop\",\"timestamp\":1619507760105,\"icon\":\"sap-icon://product\"},\"iTimestamp\":1619507760105,\"aUsageArray\":[2,0],\"iCount\":2}]}","id":"RecentActivity","category":"I","containerId":"sap.ushell.services.UserRecents","containerCategory":"U"}]}",
            BODY_END
        );

        status = nsApi.ns_end_transaction("PersContainers_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_8");
        status = nsApi.ns_web_url ("Xbatch_8",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:456",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_82db-a0db-5a3d",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3636303938443636443646454534343738384136443545443630413630413233362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_82db-a0db-5a3d
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjEngagementsSet?sap-client=100&$skip=0&$top=8&$orderby=ChangedOn%20desc&$expand=ProjectRoleSet&$inlinecount=allpages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject&/Display/ProjEngagementsSet/567FHK", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/library-preload.js", "HEADER=Sec-Fetch-Site:same-origin", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_8", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("messagebundle_en_properties");
        status = nsApi.ns_web_url ("messagebundle_en_properties",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/messagebundle_en.properties",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3636324130333436393344373634454337383336374537374630344536334142452020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("messagebundle_en_properties", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("i18n_en_properties_2");
        status = nsApi.ns_web_url ("i18n_en_properties_2",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/component/project/i18n/i18n_en.properties",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3632314238413933384631354334353638384146363135443346323833353444352020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("i18n_en_properties_2", NS_AUTO_STATUS);
    
    //Page Auto splitted for Ajax Header = XMLHttpRequest
        status = nsApi.ns_start_transaction("i18n_properties");
        status = nsApi.ns_web_url ("i18n_properties",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui5_ui5/sap/cpm_reuse_ms1/~D879E071BDB3AB81394E3E8600E05C24~5/component/project/i18n/i18n.properties",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=SAP-PASSPORT:2A54482A0300E60000702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F74696C65345F636C69636B5F35202020202020202020202020202020202020202020202020200005702E757368656C6C2E72656E6465726572732E66696F72693240312E38312E3641413135334142413938463634313546424345364332383143453442363830442020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            INLINE_URLS,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/me/library-preload.js", "HEADER=Origin:https://my304874.s4hana.ondemand.com", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", END_INLINE,
                "URL=https://ui5.sap.com/1.81.6/resources/sap/me/themes/sap_fiori_3/library.css", "HEADER=Sec-Fetch-Site:cross-site", "HEADER=Sec-Fetch-Mode:no-cors", "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8", "COOKIE=IDP_SESSION_MARKER_accounts", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject&/Create", END_INLINE
        );

        status = nsApi.ns_end_transaction("i18n_properties", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_9");
        status = nsApi.ns_web_url ("Xbatch_9",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:341",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_0552-b84e-a8e6",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F616374696F6E322D627574746F6E5F636C69636B5F362020202020202020202020202020202000052E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3641373132323043333533314334303545393645303141304637313633313341412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_0552-b84e-a8e6
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET IsResourceMapped?sap-client=100 HTTP/1.1
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_9", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_10");
        status = nsApi.ns_web_url ("Xbatch_10",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:1531",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_f57e-5a66-f21b",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F616374696F6E322D627574746F6E5F636C69636B5F362020202020202020202020202020202000052E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3645443342334346354137304534343742414244444335433139303532354342302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_f57e-5a66-f21b
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET I_EngmntProjRoleType?sap-client=100&$filter=EngagementProjectType%20eq%20%27C%27%20and%20EngagementProjectRole%20ne%20%27YP_RL_0001%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_f57e-5a66-f21b
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjManagerSet?sap-client=100&$skip=0&$top=5 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_f57e-5a66-f21b
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ServOrganisationSet?sap-client=100&$filter=SalesOrg%20eq%20%27X%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_f57e-5a66-f21b
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET StageSet?sap-client=100 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_10", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_11");
        status = nsApi.ns_web_url ("Xbatch_11",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:541",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_139a-1ef5-48ea",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F616374696F6E322D627574746F6E5F636C69636B5F362020202020202020202020202020202000052E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3633443239443442304541354434383332383641374437303634323535314132392020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_139a-1ef5-48ea
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CostCentreSet?sap-client=100&$filter=OrgUnitId%20eq%20%271710%27%20and%20Validitystartdate%20eq%20datetime%272021-04-28T05%3a30%3a00%27%20and%20Validityenddate%20eq%20datetime%272021-04-28T05%3a30%3a00%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_11", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_12");
        status = nsApi.ns_web_url ("Xbatch_12",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:595",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_061e-7156-d2bd",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3600005341505F4532455F54415F5573657220202020202020202020202020202020205F5F616374696F6E322D627574746F6E5F636C69636B5F362020202020202020202020202020202000052E63706D2E70726F6A656E676167656D656E742E6D61696E7440312E38312E3643323038323744394535303634334245393538414139313542464531373444302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_061e-7156-d2bd
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ValueHelpProfitCenterSet?sap-client=100&$filter=CostCenter%20eq%20%270017101902%27%20and%20OrgUnitId%20eq%20%271710%27%20and%20Validitystartdate%20eq%20datetime%272021-04-28T05%3a30%3a00%27%20and%20Validityenddate%20eq%20datetime%272021-04-28T05%3a30%3a00%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_12", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_13");
        status = nsApi.ns_web_url ("Xbatch_13",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:420",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_1916-355d-09cc",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E6000063706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020204372656174652D2D637573742D7668695F636C69636B5F3720202020202020202020202020202020000563706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3332464343433741383638453834323441413133444233434243364432363035332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_1916-355d-09cc
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CustomerSet?sap-client=100&$orderby=Name1%20asc&$filter=OrgUnitId%20eq%20%271710%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_13", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_14");
        status = nsApi.ns_web_url ("Xbatch_14",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:435",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_7b08-92e7-765d",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020205F5F73656C656374696F6E315F636C69636B5F38202020202020202020202020202020202020202000052E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3342453736434637383932323134384542383536344536463133423843323230422020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_7b08-92e7-765d
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CustomerSet?sap-client=100&$filter=Kunnr%20eq%20%27LACU_S03%27%20and%20OrgUnitId%20eq%20%271710%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_14", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_15");
        status = nsApi.ns_web_url ("Xbatch_15",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:996",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_7818-fbd2-4def",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020205F5F73656C656374696F6E315F636C69636B5F38202020202020202020202020202020202020202000052E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3346303345414346324233464634463731414642434130413837344437393232312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_7818-fbd2-4def
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CustCurrencySet?sap-client=100&$filter=Customer%20eq%20%27LACU_S03%27%20and%20OrgId%20eq%20%271710%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_7818-fbd2-4def
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET GenerateProjectID?sap-client=100&EngmtProjectServiceOrgName='Service%2520Organization%2520-%2520Company%2520US'&CustomerName='All%2520for%2520Bikes'&EngagementProjectServiceOrg='1710'&EngagementProjectName=''&Customer='LACU_S03'&EngagementProject='' HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_15", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_16");
        status = nsApi.ns_web_url ("Xbatch_16",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:541",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_be7a-8a74-c0da",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E6000063706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020204372656174652D2D6461746572616E67652D63616C2D2D4D6F6E7468305F6D6F75736575705F3133000563706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3332413938313743344137393934354644414233393345433035313742463134332020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_be7a-8a74-c0da
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET CostCentreSet?sap-client=100&$filter=OrgUnitId%20eq%20%271710%27%20and%20Validitystartdate%20eq%20datetime%272021-04-28T05%3a30%3a00%27%20and%20Validityenddate%20eq%20datetime%272021-04-30T05%3a30%3a00%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_16", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_17");
        status = nsApi.ns_web_url ("Xbatch_17",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:595",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_5a13-8b32-cad3",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E6000063706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020204372656174652D2D6461746572616E67652D63616C2D2D4D6F6E7468305F6D6F75736575705F3133000563706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3339383230363141443345344234303339383933463834433542463443414430412020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_5a13-8b32-cad3
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ValueHelpProfitCenterSet?sap-client=100&$filter=CostCenter%20eq%20%270017101902%27%20and%20OrgUnitId%20eq%20%271710%27%20and%20Validitystartdate%20eq%20datetime%272021-04-28T05%3a30%3a00%27%20and%20Validityenddate%20eq%20datetime%272021-04-30T05%3a30%3a00%27 HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_17", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_18");
        status = nsApi.ns_web_url ("Xbatch_18",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:1634",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_1e45-d273-a48d",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E6000063706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3300005341505F4532455F54415F557365722020202020202020202020202020202020656174652D2D43504D5F435553545F50524F4A5F534156452D627574746F6E5F636C69636B5F3134000563706D2E70726F6A656E676167656D656E742E6D61696E744031312E31372E3344394431453832314335374234454243393633363138364332453237393638312020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_1e45-d273-a48d
                "HEADER=Content-Type: multipart/mixed; boundary=changeset_8e31-37ca-027e",
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                "HEADER=Content-Type: application/json",
                "HEADER=Content-Length: 681",
                    MULTIPART_BODY_BEGIN,
                MULTIPART_BOUNDARY,
                //--changeset_8e31-37ca-027e
                    BODY_BEGIN,
                        "POST ProjEngagementsSet?sap-client=100 HTTP/1.1
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0

{"RestrictTimePosting":"Y","CostCenter":"0017101902","ProjectManagerExtID":"USER1319","ProjAccountantUserid":"","ProjControllerUserid":"","ProjPartnerUserid":"","ProfitCenter":"YB101","Mpid":"As87654","OrgName":"Service Organization - Company US","Mptype":"P001","Startdate":"2021-04-28T00:00:00","Enddate":"2021-04-30T00:00:00","Stage":"P001","Customer":"LACU_S03","Customername":"All for Bikes","Title":"Record","Currency":"USD","Description":"trying to do ","OrgID":"1710","ProjMngr":"50002713","ProjMngrName":"USER1319 Trial","ProjAccountantID":"","ProjAccountantName":"","ProjControllerID":"","ProjControllerName":"","ProjPartnerID":"","ProjPartnerName":"","Confidential":"N"}",
                    BODY_END,
                MULTIPART_BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_1e45-d273-a48d
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjEngagementsSet?sap-client=100&$skip=0&$top=8&$orderby=ChangedOn%20desc&$expand=ProjectRoleSet&$inlinecount=allpages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_18", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("flp_sap_fesr_only_3");
        status = nsApi.ns_web_url ("flp_sap_fesr_only_3",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/flp;sap-fesr-only",
            "METHOD=POST",
            "HEADER=Content-Length:2328",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Content-Type:application/x-www-form-urlencoded;charset=utf-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("flp_sap_fesr_only_3", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_19");
        status = nsApi.ns_web_url ("Xbatch_19",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:944",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_712e-f9ba-29d9",
            "HEADER=sap-cancel-on-close:false",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020205F5F6D626F782D62746E2D305F636C69636B5F31362020202020202020202020202020202020202000052E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3339424143394334333245393334344246423636303137443841433936364435362020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_712e-f9ba-29d9
                "HEADER=Content-Type: multipart/mixed; boundary=changeset_7b18-f639-d7b9",
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                "HEADER=If-Match: W/\"20210428070539m\"",
                    MULTIPART_BODY_BEGIN,
                MULTIPART_BOUNDARY,
                //--changeset_7b18-f639-d7b9
                    BODY_BEGIN,
                        "DELETE ProjEngagementsSet('AS87654')?sap-client=100 HTTP/1.1
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                    BODY_END,
                MULTIPART_BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_712e-f9ba-29d9
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjEngagementsSet?sap-client=100&$skip=0&$top=8&$orderby=ChangedOn%20desc&$expand=ProjectRoleSet&$inlinecount=allpages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END,
            INLINE_URLS,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject&/", END_INLINE,
                "URL=https://my304874.s4hana.ondemand.com/ui?sap-language=EN&tour=PR_3A4C8812DC44D6B0#CustomerProject-maintainCustomerProject&/Display/ProjEngagementsSet/AS87654", END_INLINE
        );

        status = nsApi.ns_end_transaction("Xbatch_19", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_20");
        status = nsApi.ns_web_url ("Xbatch_20",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST",
            "HEADER=Content-Length:456",
            "HEADER=MaxDataServiceVersion:2.0",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=x-csrf-token:9XHu53zG4vHNdP9ekbB08w==",
            "HEADER=sap-contextid-accept:header",
            "HEADER=Accept-Language:en",
            "HEADER=Content-Type:multipart/mixed;boundary=batch_13bb-e056-b5a1",
            "HEADER=sap-cancel-on-close:true",
            "HEADER=X-XHR-Logon:accept=\"iframe,strict-window,window\"",
            "HEADER=X-Requested-With:XMLHttpRequest",
            "HEADER=DataServiceVersion:2.0",
            "HEADER=SAP-PASSPORT:2A54482A0300E600002E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3300005341505F4532455F54415F5573657220202020202020202020202020202020205F5F6D626F782D62746E2D305F636C69636B5F31362020202020202020202020202020202020202000052E757368656C6C2E72656E6465726572732E66696F7269324031312E31372E3336343031454132324538334634463330394343454537304131453431383838302020200007556CC481B8B9400D96F9A84F8F7EE21E0000000000000000000000000000000000000000000000E22A54482A",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:cors",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100",
            MULTIPART_BODY_BEGIN,                BODY_BEGIN,
                    "",
                BODY_END,
            MULTIPART_BOUNDARY,
            //--batch_13bb-e056-b5a1
                "HEADER=Content-Type: application/http",
                "HEADER=Content-Transfer-Encoding: binary",
                BODY_BEGIN,
                    "GET ProjEngagementsSet?sap-client=100&$skip=0&$top=8&$orderby=ChangedOn%20desc&$expand=ProjectRoleSet&$inlinecount=allpages HTTP/1.1
sap-cancel-on-close: true
sap-contextid-accept: header
x-csrf-token: 9XHu53zG4vHNdP9ekbB08w==
DataServiceVersion: 2.0
MaxDataServiceVersion: 2.0",
                BODY_END,
            MULTIPART_BODY_END
        );

        status = nsApi.ns_end_transaction("Xbatch_20", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("Xbatch_21");
        status = nsApi.ns_web_url ("Xbatch_21",
            "URL=https://my304874.s4hana.ondemand.com/sap/opu/odata/cpd/SC_PROJ_ENGAGMNT_MAINT_SRV/$batch?sap-client=100",
            "METHOD=POST"
        );

        status = nsApi.ns_end_transaction("Xbatch_21", NS_AUTO_STATUS);
    
    //Page Auto splitted for Method = POST
        status = nsApi.ns_start_transaction("flp_sap_fesr_only_4");
        status = nsApi.ns_web_url ("flp_sap_fesr_only_4",
            "URL=https://my304874.s4hana.ondemand.com/sap/bc/ui2/flp;sap-fesr-only",
            "METHOD=POST",
            "HEADER=Content-Length:803",
            "HEADER=Origin:https://my304874.s4hana.ondemand.com",
            "HEADER=Content-Type:application/x-www-form-urlencoded;charset=utf-8",
            "HEADER=Sec-Fetch-Site:same-origin",
            "HEADER=Sec-Fetch-Mode:no-cors",
            "HEADER=Accept-Language:en-GB,en-US;q=0.9,en;q=0.8",
            "COOKIE=sap-usercontext;SAP_SESSIONID_SV1_100"
        );

        status = nsApi.ns_end_transaction("flp_sap_fesr_only_4", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(74.745);

        return status;
    }
}
