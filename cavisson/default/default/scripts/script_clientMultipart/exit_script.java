/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 04/28/2021 12:37:55
    Flow details:
    Build details: 4.6.0 (build# 57)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.script_clientMultipart;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
