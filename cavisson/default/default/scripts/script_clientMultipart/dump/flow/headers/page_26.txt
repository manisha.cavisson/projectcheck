--Request 
GET https://ui5.sap.com/1.81.6/resources/sap-ui-version.json
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
Accept: application/json, text/javascript, */*; q=0.01
Origin: https://my304874.s4hana.ondemand.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/json
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 3892
cache-control: public, max-age=22285975
expires: Tue, 11 Jan 2022 05:30:36 GMT
date: Wed, 28 Apr 2021 06:57:41 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

