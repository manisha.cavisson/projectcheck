--Request 
GET https://my304874.s4hana.ondemand.com/ui?sap-language=EN#ProductTrialOffer-displayTrialCenter
Host: my304874.s4hana.ondemand.com
Host: my304874.s4hana.ondemand.com
Connection: keep-alive
Cache-Control: max-age=0
Upgrade-Insecure-Requests: 1
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Referer: https://my304874.s4hana.ondemand.com/sap/saml2/sp/acs/100
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: sap-usercontext=sap-language=EN&sap-client=100; SAP_SESSIONID_SV1_100=l2wt0rCeKaICMfbMFcviAwG7dPan7xHrpYz6Fj4CWMo%3d
----
--Response 
HTTP/1.1 200
status: 200
content-type: text/html; charset=utf-8
content-length: 3242
expires: -1
cache-control: no-store
x-ua-compatible: IE=edge
content-security-policy: default-src 'self' ; script-src 'self' https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn 'unsafe-eval' ; style-src 'self' https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://*.api.here.com https://*.hereapi.cn 'unsafe-inline' ; font-src 'self' data: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://help.sap.com ; img-src 'self' https: http: data: blob: ; media-src 'self' https: http: data: blob: ; object-src blob: ; frame-src 'self' https: gap: data: blob: mailto: tel: ; worker-src 'self' blob: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn ; child-src 'self' blob: https://ui5.sap.com https://*.int.sap.hana.ondemand.com https://*.int.sap.eu2.hana.ondemand.com https://siteintercept.qualtrics.com https://*.siteintercept.qualtrics.com https://*.api.here.com https://litmus.com https://*.hereapi.cn ; connect-src 'self' https: wss: ; base-uri 'self' ; report-uri /sap/bc/csp/report
sap-server: true
sap-perf-fesrec: 427874.000000
content-encoding: br
x-content-type-options: nosniff
strict-transport-security: max-age=31536000; includeSubDomains
x-xss-protection: 1; mode=block
set-cookie: sap-contextid=SID%3aANON%3avhshpsv1ci_SV1_00%3aHPbhFd8aP0kYkU18Hbaod4FHdAxBs-eigxE5AYUj-NEW; path=/sap/bc/ui2/start_up; HttpOnly; SameSite=None; secure
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ui/core/themes/sap_fiori_3/library.css
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
Origin: https://my304874.s4hana.ondemand.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: text/css
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 14645
cache-control: public, max-age=21856778
expires: Thu, 06 Jan 2022 06:17:13 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/abap.js
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 242007
cache-control: public, max-age=21856806
expires: Thu, 06 Jan 2022 06:17:41 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/m/themes/sap_fiori_3/library.css
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
Origin: https://my304874.s4hana.ondemand.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: text/css
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 91147
cache-control: public, max-age=21856711
expires: Thu, 06 Jan 2022 06:16:06 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ushell/themes/sap_fiori_3/library.css
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
Origin: https://my304874.s4hana.ondemand.com
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: text/css,*/*;q=0.1
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: text/css
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 31629
cache-control: public, max-age=22285828
expires: Tue, 11 Jan 2022 05:28:03 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-0.js
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 202234
cache-control: public, max-age=22285860
expires: Tue, 11 Jan 2022 05:28:35 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----
--Request 
GET https://ui5.sap.com/1.81.6/resources/sap/ushell_abap/bootstrap/evo/core-min-1.js
Host: ui5.sap.com
Host: ui5.sap.com
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.130 Safari/537.36
Accept: */*
Sec-Fetch-Site: cross-site
Sec-Fetch-Mode: no-cors
Referer: https://my304874.s4hana.ondemand.com/ui?sap-language=EN
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
Cookie: IDP_SESSION_MARKER_accounts=eyJhbGciOiJFQ0RILUVTK0EyNTZLVyIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOm51bGwsImN0eSI6IkpXVCIsImVwayI6eyJrdHkiOiJFQyIsIngiOiJCY1Rab3VnRnN3b0dNclpodVlHNzVGaVZwMGhfYkEtdHZYVE9YSnU0R2ZrIiwieSI6Imp2T0cyM1hIYXBhSTk1REc0dGVqYlBjOHB5dEpQWnVRMFdqOXg2RjJRSWMiLCJjcnYiOiJQLTI1NiJ9fQ.REphR5EVl2wE4tcbyfC9xufEr51-0fdO8tPiQ5LNV8WRsJ0Y19riqg.aUB_U_qlvgYY5qG3UE0PLw.XRJYz98xEKcQTRpyg3fwwGNtcKjkKR4GEahntVc6pSC0xZXfTagFH_YS2LI_9cDyRJoDO3iKJh5HWCnB8LW0uDvfbyk7m7MtkNonsduqGyTaUdi98pMRVW-YnTk0Ek1h3ibyPN9eaHZUHW9Kl4qdu0Ti1C61HDOjh3DyctgTL7C4lWg8czv2arkcYLW2fgzgs3cmNuO4UZFtOxQQe27tS6AgL22gEKqb9I7YYVRhaCOBlTtEIb63WJ_63OSlWDI8g5p8qcKN3vdRLIksXGxH_1pmZGxhs1aTA1mNvasY9GISqKXE9GuYVmsRIe993cr0_85BO0CK-4xKkIC_A6j6zJOpQxNMbIikShWcbtroIqxxxQcNc6gc62G7OeuJZYAip87_vyrGoKkFFWLvqJeSFn4F0DtqM6ZDf8NLsIC2vMv3DPqMPk7ZV_VewiPAnbFEhtD3iNWtEo6Y3GA9-vmHh4ZDiaWTLDAOv0GS4MXS7SE4_dyNiTx1ZiH0gU_529ZwpwcdFYjjSs3fUY63dpejGFJjRro0MS9jdlChDfhxHRREg9bBeM8F_LRtSiMM298HBeo5Uo9gsRJaqWLXSPyVjJh8jPNg2Qk5LTkVEYCvDpo.3WDf4BZRyuXiizNUECGVWA
----
--Response 
HTTP/1.1 200
status: 200
x-xss-protection: 0
access-control-allow-origin: *
etag: W/\"1.81.6\"
vary: accept-encoding
content-encoding: gzip
content-type: application/javascript
server: SAP
strict-transport-security: max-age=31536000; includeSubDomains; preload
content-length: 194605
cache-control: public, max-age=21856748
expires: Thu, 06 Jan 2022 06:16:43 GMT
date: Wed, 28 Apr 2021 06:57:35 GMT
timing-allow-origin: *
x-cache-akamai: stable
----

