//@ui5-bundle ssuite/smartbusiness/tiles/numeric/Component-preload.js
sap.ui.require.preload({
	"ssuite/smartbusiness/tiles/numeric/Component.js":function(){if (sap.ushell.Container.getLogonSystem().getPlatform() === "abap") {
	jQuery.sap.registerModulePath("sap.cloudfnd.smartbusiness.lib.reusetiles", jQuery.sap
			.getModulePath("ssuite.smartbusiness.tiles.numeric")
			+ "/../../../../../../../../../../sap/bc/ui5_ui5/sap/ssbtileslibs1");
}
sap.ui.define(["jquery.sap.global", "sap/ushell/Config", "sap/ui/core/UIComponent", "sap/m/VBox", "sap/ui/model/odata/AnnotationHelper",
		"sap/m/GenericTile", "sap/m/TileContent", "sap/m/NumericContent", "sap/cloudfnd/smartbusiness/lib/reusetiles/Util",
		"sap/cloudfnd/smartbusiness/lib/reusetiles/TileNavigation",
		"sap/cloudfnd/smartbusiness/lib/reusetiles/RequestManager",
		"sap/cloudfnd/smartbusiness/lib/reusetiles/TileLoadManager"], function(jQuery, Config, UIComponent, VBox, oAnno, oGenTile,
		oTileContent, oNumericContent, oSSBTilesLibUtils, oTileNavigation, oRequestManager, TileLoadManager) {
	"use strict";

	return UIComponent.extend("ssuite.smartbusiness.tiles.numeric.Component", {

		_sFileName : "ssuite.smartbusiness.tiles.numeric.Component",
		
		metadata : {
			"manifest" : "json"
		},
		putTileToLoadingState : function(){
			var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
			oUtilityFunctions.clearExistingTileContentAndAddGenericTile(this.oChipInfo, this.content,"Loading", this, "NT");			
		},
		putTileToFailedState : function(){
			var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
			oUtilityFunctions.clearExistingTileContentAndAddGenericTile(this.oChipInfo, this.content, "Failed", this, '');	
		},
		putTileToLoadedState : function(){
			var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
			oUtilityFunctions.clearExistingTileContentAndAddGenericTile(this.oChipInfo, this.content, "Loaded", this, '');	
		},
		/**
		 * tile data in case of preview mode
		 */
		_intializeNumericTileForPreviewMode : function() {
			var oGenericTile = this.content.getItems()[0];
			var numericTileContent = new sap.ui.xmlfragment("ssuite.smartbusiness.tiles.numeric.NumericContent", this);
			oGenericTile.addTileContent(numericTileContent);
			oGenericTile.attachPress(jQuery.proxy(function() {
				oTileNavigation.onPressInPreviewMode(this.oChipInfo, this);
			}, this));
		},

		/**
		 * create a json model to store the title and subtitle of the chip from the chip bag.
		 * 
		 * @returns {Promise} jQuery Promise resolved when 'tileConfig' model is populated with the values.
		 */
		_getTileConfigModel : function() {
			
			var oDeferred = jQuery.Deferred();
			try {
				var Util = new sap.cloudfnd.smartbusiness.lib.reusetiles.Util();
				var oTileConfigModel = this.content.getModel('tileConfig');
				var oTileConfigModelData = oTileConfigModel.getData();

				// personalized tiles are currently possible only in ABAP FLP - in CDM it is not possible yet
				if (this.oChipInfo && this.oChipInfo.flpType != "CDM") {
					// check for existence of thresholds in case of user personalized Tile
					if (this._localData && this._localData.tileProperties && this._localData.tileProperties.thresholds) {
						oTileConfigModelData.personalizedThresholds = JSON.parse(this._localData.tileProperties.thresholds);
					}

					// check for existence of properties in case of user personalized Tile
					if (this._localData && this._localData.tileProperties) {

					// in case of personalized tile valueMeasure is saved in tile property
						if (this._localData.tileProperties.valueMeasure) {
							oTileConfigModelData.personalizedValueMeasure = this._localData.tileProperties.valueMeasure;
						// Add additional measure in tile config payload
							// Will check whether the personalized measure is present in additional measure or not
						if (this._localData.ssbProperties && this._localData.ssbProperties.additionalMeasures)
							oTileConfigModelData.additionalMeasures = this._localData.ssbProperties.additionalMeasures;
					}

						if (this._localData.tileProperties.goalType) {
							oTileConfigModelData.personalizedGoalType = this._localData.tileProperties.goalType;
						}
					}

					// populate filters added in personalized Tile if any exists
					var oFilters = Util.getPersFilters(this.getComponentData().chip);
					if (oFilters) {
						oTileConfigModelData.personalizedFilters = oFilters;
					}
					// populate parameters added in personalized Tile if any exists
					var oParameter = Util.getPersParameters(this.getComponentData().chip);
					if (oParameter) {
						oTileConfigModelData.personalizedParameters = oParameter;
					}
				}
				
				if (jQuery.isEmptyObject(this.oChipInfo.EVALUATION) === false) {
					oTileConfigModelData.entitySet = this.oChipInfo.EVALUATION.ODATA_ENTITYSET || "";
				}
				
				if (!this._localData.noCache && !this._localData.bNoCachingEnabled) {
					oTileConfigModelData.loadFromCache = true;
				} else {
					oTileConfigModelData.loadFromCache = false;
				}
				oTileConfigModelData.cacheEnabled = !this._localData.bNoCachingEnabled;
				oTileConfigModelData.displayCachedTime = this._localData.displayCachedTime;
				
				Util.readFLPDefaultValues(this.oChipInfo.DEFAULT_PARAMETERS).done(jQuery.proxy(function(oDefaultValues) {
					oTileConfigModelData.DefaultParameters = oDefaultValues;
				}, this)).always(jQuery.proxy(function() {
					oDeferred.resolve(oTileConfigModel);
				}, this));
			} catch(oException) {
				oDeferred.reject(oException);
			}
			return oDeferred.promise();
		},

		/**
		 * local data using in the context of this component
		 */
		_initializeLocalVariables : function() {
			this._localData = {
				oneTimeLoad : false,
				ssbProperties : {

				},
			//A Deferred Object tells whether the Metamodel is loaded or not
			//Added: Tile partial clickable scenario, for the Otherdrilldown/ALP tiles, 
			//where the metadata should be loaded before navigation
				oMetaModelLoadedDeferred : jQuery.Deferred(), 
				oAppDescriptorRequest : undefined,//An Object- holds the AJAX request to fetch the AppDescriptor
				oCacheRequest : undefined,  //An Object- holds the AJAX request to fetch the Cached Data
				isCachedRequestRejected : false, //A boolean flag - tells whether the Cache request is aborted
				isAppDescriptorRequestRejected : false, //A boolean flag - tells whether the AppDescriptor request is aborted
				isMetaModelLoaded : false, //A boolean flag-tells whether the Metadata load is successful or not
				isTilePreprocessed : false, // A boolean flag-tells whether the tile is processed or not 
				isTilePressed : false, //A boolean flag-tells if the tile is pressed
			};
		},

		/**
		 * checks whether the mandatory data required to load chip is available
		 */
		_checkForTileConfigMandatoryData : function() {
			var status = {
				failed : false
			};
			try {
				var tileConfigString = this.oChipInfo && this.oChipInfo.tileConfiguration;
				var tileConfigObject = JSON.parse(tileConfigString);
				var tileProperties = JSON.parse(tileConfigObject.TILE_PROPERTIES);
				if (!tileProperties || !(tileProperties.evaluationId || tileProperties.reportId)) {
					jQuery.sap.log.error("_checkForTileConfigMandatoryData", "mandatory data (evaluationId/reportId) not available");
					status.failed = true;
				} else {
					this._localData.tileProperties = tileProperties;
				}
			} catch (exception) {
				jQuery.sap.log.error("_checkForTileConfigMandatoryData", "check for mandatory data failed - "
						+ exception.message);
				status.failed = true;
			}
			return status;
		},

		/**
		 * create content module | checks for visibility contract availability and based on whether the tile is visible
		 * triggers the initialize Content module
		 */
		createContent : function() {
			this.content = new VBox();
			this._initializeLocalVariables();
			this.oChipInfo = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype
					.determineFLPTypeAndFetchSSBTileDetails(this);
			
			var oTileConfigData = this._initializeTileConfigData();
			var oTileConfigModel = new sap.ui.model.json.JSONModel(oTileConfigData);
			this.content.setModel(oTileConfigModel, "tileConfig");
			
			Config.on("/core/home/sizeBehavior").do(jQuery.proxy(function(sSizeBehavior){
				if (sSizeBehavior != this._localData.flpTileSize) {
					// Update property in Local data.
					this._localData.flpTileSize = sSizeBehavior;
					// Update the tile property.
					var oTileConfigModel = this.content.getModel("tileConfig");
					oTileConfigModel.setProperty("/sizeBehavior", sSizeBehavior);
					oTileConfigModel.updateBindings();
				}
			}, this));
			
			if (this.oChipInfo && this.oChipInfo.flpType != "CDM") {
				this._ABAPFLPInitialization();
			} else {
				this.putTileToLoadingState();
			}
			return this.content;
		},
		
		/**
		 * Creates an initial JSON for tileConfig model.
		 */
		_initializeTileConfigData : function() {
			return {
				Title : this.oChipInfo && this.oChipInfo.Title,
				SubTitle : this.oChipInfo && this.oChipInfo.SubTitle,
				TileType : "NT",
				sizeBehavior : Config.last("/core/home/sizeBehavior"),
				displayCachedTime : ""
			};
		},

		_ABAPFLPInitialization : function() {
			var oChip = this.getComponentData().chip;
			if (!oChip.configurationUi.isEnabled() && oChip.preview.isEnabled())
				jQuery.proxy(sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.searchGenericDrillDowns, this)(this.oChipInfo);
			if (oChip.preview.isEnabled()) {
				this.putTileToLoadedState();
				this._intializeNumericTileForPreviewMode();
			} else {
				this.putTileToLoadingState();
				if (oChip && oChip.visible && oChip.visible.attachVisible) {
					oChip.visible.attachVisible(jQuery.proxy(this.tileSetVisible, this));
				}
			}
		},

		tileSetVisible : function(bVisible) {
			this._localData.tileVisible = bVisible;
			if (bVisible) {
				this._localData.visible = bVisible;
				if (!this._localData.oneTimeCacheCheckDone) {
					this._localData.oneTimeCacheCheckDone = true;
					sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.checkForCachedData(this);
				}
				// cacheRefreshRequired flag is set to true when the user goes into the drilldown for the tile,
				// or when tile is not visible and the data age reaches the maximum cache age allowed.
				// When the tile becomes visible, according to the cacheRefreshRequired flag a data load is
				// triggered.
				if (this._localData.cacheRefreshRequired) {
					this._localData.cacheRefreshRequired = false;
					if (this._localData.delayedUpdateCacheIdentifier)
						jQuery.sap.clearDelayedCall(this._localData.delayedUpdateCacheIdentifier);
					this.updateCacheDataFromBackend(true);
				}
				this._initializeContent();
				sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.checkForRequestRetrigger(this);
			}
			else if(!bVisible && !this._localData.isTilePressed){ //Don't abort the call for the tile, which is clicked
				//If tile is invisible and the AppDescriptor call is requested and not completed yet, abort the call.
				if(this._localData.oAppDescriptorRequest && this._localData.oAppDescriptorRequest.state() == "pending")
					this._localData.oAppDescriptorRequest.abort();
				if(this._localData.oCacheRequest && this._localData.oCacheRequest.state() == "pending")
					this._localData.oCacheRequest.abort();
			}
		},

		/**
		 * create a dummy GenericTile with title and subtitle in loading state | after the actual app descriptor and
		 * annotation xml has loaded this content will be replaced with tile specific content
		 */
		_initializeContent : function() {
			if (this._localData && this._localData.visible && (this._localData.noCache || this._localData.bNoCachingEnabled)
					&& !this._localData.oneTimeLoad && !this._localData.isCachedRequestRejected) {
				var mandatoryDataStatus = this._checkForTileConfigMandatoryData();
			  this._localData.oneTimeLoad = true;
				if (mandatoryDataStatus.failed) {
					this.putTileToFailedState();
				} else {
					this.putTileToLoadingState();
					this._triggerContentLoadAsynchronously();
				}
			}
		},

		/**
		 * if the KPI Aggregate is cached the cached value is displayed in the tile
		 */
		setCachedData : function(oData) {
			if (oData) {
				this._tileCachedDataModel = new sap.ui.model.json.JSONModel();
				var parseData = jQuery.parseJSON(oData.Data);
				this._tileCachedDataModel.setData(parseData);
				this._localData.CachedTime = oData.CachedTime;
				this._localData.displayCachedTime = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype
						.decideTimeToDisplay(Number(oData.CacheAge));
				this._localData.cacheAge = oData.CacheAge;
				if (oData.Iscacheinvalid == 1)
					this._localData.noCache = true;
				else {
					this._localData.cacheMaxAge = parseInt(oData.CacheMaxAge);
					this._localData.cacheMaxAgeUnit = oData.CacheMaxAgeUnit;
				}
				var mandatoryDataStatus = this._checkForTileConfigMandatoryData();
				if (mandatoryDataStatus.failed) {
					this.putTileToFailedState();
				} else {
					this._triggerContentLoadAsynchronously();
				}
			} else {
				// has to be handled
			}
		},

		/**
		 * triggers the load of the AppDescriptor and annotation document asynchronously | is systemAlias is configured it
		 * will be considered
		 */
		_triggerContentLoadAsynchronously : function() {
			var oChip = this.oChipInfo;
			var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
			var oRequestManager = sap.cloudfnd.smartbusiness.lib.reusetiles.RequestManager;
			var sSystemAlias = oChip && oUtilityFunctions.getSystemAlias(oChip);
			//This will return either a reportId or an evaluationId
			var sEvaluationId = oUtilityFunctions.extractEvalutionIdFromChip(oChip);
			if (oChip && oChip.flpType != "CDM" && this._localData.tileVisible) {
			//If the tile is invisible, Prevent the AppSescriptor call
				var oAppDescriptor = this._localData.appDescriptor;
				if (!oAppDescriptor) {
				jQuery.proxy(oRequestManager.fetchDynamicAppDescriptor, this)(oChip,sEvaluationId, sSystemAlias,
						jQuery.proxy(this._fetchOdataUrlAndAnnotationUrl,this),jQuery.proxy(this.putTileToFailedState,this));
				}
			 else {
					this._fetchOdataUrlAndAnnotationUrl(oAppDescriptor);
				}
			} else {
				this._extractInfoFromAppDescriptor();
			}
		},

		_fetchOdataUrlAndAnnotationUrl : function(oAppDescriptor) {
			this._localData.appDescriptor = oAppDescriptor;
			var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
			var oAppDescriptorValidityStatus = oUtilityFunctions.validateAppDescriptor(oAppDescriptor);
			if(oAppDescriptorValidityStatus.errorState){
				this.putTileToFailedState();
				jQuery.sap.log.error(oAppDescriptorValidityStatus.errorMessage, oAppDescriptorValidityStatus.errorDescription);
			}else{
				var ssbDescriptorStatus = oUtilityFunctions._findProperty("sap.ui.smartbusiness.app", oAppDescriptor);
				this._localData.ssbProperties = ssbDescriptorStatus.oData;
				//creates a OData model for the data source and assign it as default model for the component. 
				//createAndAssignOdatamodel
				var oRequestManager = sap.cloudfnd.smartbusiness.lib.reusetiles.RequestManager;
				var oComponent = this;
				var oChip = this.oChipInfo;
				oRequestManager.initializeODataAndAnnotationModelInComponent(oComponent,oChip,
						oAppDescriptor['sap.ui5']['models'], oAppDescriptor['sap.app']['dataSources']).done(
						jQuery.proxy(this._initializeChipDataLoad, this)).fail(jQuery.proxy(this.putTileToFailedState, this));
			}
	},

		/**
		 * have to check whether oData will be loaded by FLP
		 */
		_extractInfoFromAppDescriptor : function() {
			var oDataSources = this.oChipInfo && this.oChipInfo.dataSources;
			var oUI5Models = this.oChipInfo && this.oChipInfo.models;
			var oDataUrl = "";
			var annoDocUrl = "";
			var oRequestManager = sap.cloudfnd.smartbusiness.lib.reusetiles.RequestManager;
			var oComponent = this;
			var oChip = this.oChipInfo;
			if (oDataSources && oDataSources.mainService && oDataSources.mainService.uri
					&& oDataSources.mainServiceAnnotation && oDataSources.mainServiceAnnotation.uri) {
				this._localData.ssbProperties = {
					annotationFragments : this.oChipInfo && this.oChipInfo.annotationFragments
				};
				oDataUrl = oDataSources.mainService.uri;
				annoDocUrl = oDataSources.mainServiceAnnotation.uri;
				oRequestManager.initializeODataAndAnnotationModelInComponent(oComponent, oChip, oUI5Models, oDataSources).done(
						jQuery.proxy(this._initializeChipDataLoad, this)).fail(jQuery.proxy(this.putTileToFailedState, this));
			} else if (oDataSources && oDataSources["customer.mainService"] && oDataSources["customer.mainService"].uri
					&& oDataSources["customer.mainServiceAnnotation"] && oDataSources["customer.mainServiceAnnotation"].uri) {
				this._localData.ssbProperties = {
					annotationFragments : this.oChipInfo && this.oChipInfo.annotationFragments
				};
				oDataUrl = oDataSources["customer.mainService"].uri;
				annoDocUrl = oDataSources["customer.mainServiceAnnotation"].uri;
				oRequestManager.initializeODataAndAnnotationModelInComponent(oComponent, oChip, oUI5Models, oDataSources).done(
						jQuery.proxy(this._initializeChipDataLoad, this)).fail(jQuery.proxy(this.putTileToFailedState, this));
			} else
				this.putTileToFailedState();
		},
		/**
		 * ================================================================================================================
		 * SCHEDULERS FOR UPDATING CACHE DATA AND CACHE AGE TO BE DISPLAYED
		 * ================================================================================================================
		 */

		/**
		 * Schedules the update job for the tile when the cache gets invalidated
		 */
		initializeAutoCacheUpdateScheduler : function() {
			/*
			 * To leverage the active standby HANA node, cache handling is changed : For Tiles with cache settings as 10 min
			 * or less no automatic reload of data is triggered
			 */
			var cacheMaxAgeValue = 0;
			switch (this._localData.cacheMaxAgeUnit) {
				case "MIN" :
					cacheMaxAgeValue = this._localData.cacheMaxAge * 60;
					break;
				case "HUR" :
					cacheMaxAgeValue = this._localData.cacheMaxAge * 3600;
					break;
				case "DAY" :
					cacheMaxAgeValue = this._localData.cacheMaxAge * 86400;
					break;
				default :
					cacheMaxAgeValue = 0;
					break;
			};
			// If cacheAge is not maintained, scheduler should not be initalized for the tile
			if (this._localData.cacheAge >= 0 && this._localData.cacheMaxAge > 0 && cacheMaxAgeValue > 600) {
				var secondsToScheduleUpdate = 0;
				switch (this._localData.cacheMaxAgeUnit) {
					case "MIN" :
						secondsToScheduleUpdate = this._localData.cacheMaxAge * 60 - this._localData.cacheAge;
						break;
					case "HUR" :
						secondsToScheduleUpdate = this._localData.cacheMaxAge * 3600 - this._localData.cacheAge;
						break;
					case "DAY" :
						secondsToScheduleUpdate = this._localData.cacheMaxAge * 86400 - this._localData.cacheAge;
						break;
					default :
						secondsToScheduleUpdate = -1;
						break;
				};
				// delayTimeLimit is the maximum accepted delay in milliseconds. When the required delay is more than
				// that,
				// scheduleUpdateForExceedingDelayTimeLimit() is called to break the delay into acceptable limits.
				var delayTimeLimit = 2147483647;
				if (secondsToScheduleUpdate >= 0) {
					jQuery.sap.log.info("Cache to be updated in : ", secondsToScheduleUpdate + " seconds");
					if ((secondsToScheduleUpdate * 1000) > delayTimeLimit) {
						this._localData.exceedingPeriods = parseInt((secondsToScheduleUpdate * 1000) / delayTimeLimit);
						this._localData.remainingTime = ((secondsToScheduleUpdate * 1000) % delayTimeLimit);
						jQuery.proxy(this.scheduleUpdateForExceedingDelayTimeLimit(), this);
					} else {
						this._localData.delayedUpdateCacheIdentifier = jQuery.sap.delayedCall((secondsToScheduleUpdate * 1000),
								null, jQuery.proxy(this.updateCacheDataFromBackend, this));
					}
				}
			}
		},

		/**
		 * Creates acceptable time segments for a delay exceeding the maximum value. If acceptable time limit is X, and
		 * secondsToScheduleUpdate is Y, a delay of X seconds is generated for [integer value of (Y/X)] times which is
		 * stored in exceedingPeriods. The remainder of (Y/X) is stored in remainingTime, which is used to create the last
		 * segment of delay - after which the update function is called.
		 */
		scheduleUpdateForExceedingDelayTimeLimit : function() {
			var delayTimeLimit = 2147483647;
			if (this._localData.exceedingPeriods == -1) {
				jQuery.proxy(this.updateCacheDataFromBackend(this._localData.tileVisible), this);
			} else if (this._localData.exceedingPeriods == 0) {
				this._localData.exceedingPeriods--;
				jQuery.sap.delayedCall(this._localData.remainingTime, null, jQuery.proxy(
						this.scheduleUpdateForExceedingDelayTimeLimit, this));
			} else if (this._localData.exceedingPeriods > 0) {
				this._localData.exceedingPeriods--;
				var fFunctionToBeCalled = this.scheduleUpdateForExceedingDelayTimeLimit;
				jQuery.sap.delayedCall(delayTimeLimit, null, jQuery.proxy(fFunctionToBeCalled, this));
			}
		},

		/**
		 * This method is called when the cache gets invalidated. Resets the flags used for checking whether the annotation
		 * and metadata are loaded, and triggers a new load of data from backend and rewriting of cached data. isRefresh -
		 * used for refresh via button on tile
		 */
		updateCacheDataFromBackend : function(tileVisible, isRefresh) {
			var Util = new sap.cloudfnd.smartbusiness.lib.reusetiles.Util();
			if (tileVisible == undefined)
				tileVisible = this._localData.tileVisible;
			if (tileVisible) {
				this._localData.noCache = true;
				var oDataModel = this.getModel();
				this.content.setBusyIndicatorDelay(0);
				this.content.setBusy(true);
				if (!this._localData.oneTimeLoad) {
					this._initializeChipDataLoad(oDataModel, isRefresh);
				} else {
					TileLoadManager.isTilePreProcessingRequired(this.oChipInfo, this.content.getModel('tileConfig')).done(jQuery.proxy(function(bIsTilePreProcessingRequired) {
						
						if (bIsTilePreProcessingRequired) {
							// Triggering pre-processing again to recalculate the binding path and the filters used. 
							this._initializeTilePreprocessing(oDataModel, isRefresh);
						} else {
							if (isRefresh) {
								oDataModel.setHeaders({
										"Cache-Control" : "max-age=0"
								});
							}
							oDataModel.refresh();
						}

					}, this)).fail(jQuery.proxy(function() {
						/*
						 * Original behavior where only the header is changed and the model is refreshed.
						 */
						if (isRefresh) {
							oDataModel.setHeaders({
									"Cache-Control" : "max-age=0"
							});
						}
						oDataModel.refresh();
					}, this));
			 }
			} else {
				this._localData.cacheRefreshRequired = true;
			}
		},

		/**
		 * Initializes the scheduler for updating the displayed cache age. Obtains the time to be displayed currently and
		 * sets it as the timestamp for the tile
		 */
		analyzeAndUpdateTime : function() {
			// If cacheAge is not maintained, scheduler should not be initalized for the tile
			if (this._localData.cacheAge >= 0) {
				// Parsing the hana cache age from string to number if available
				this._localData.hanaCacheAge = (this._localData.hanaCacheAge) ? Number(this._localData.hanaCacheAge) : null;
				// If HANA cache age is more than 0 but local cache age is 0, the local cache age should be
				// populated with the hana cache age before proceeding to ensure correct age is displayed
				if (this._localData.hanaCacheAge && this._localData.hanaCacheAge > 0 && this._localData.cacheAge == 0)
					this._localData.cacheAge = this._localData.hanaCacheAge;
				this.initializeAutoTimeUpdater();
				
				var seconds = Number(this._localData.cacheAge);
				var time = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideTimeToDisplay(seconds);
				this._localData.displayCachedTime = time;
				try {
					var oGenericTile = this.content.getItems()[0];
					if (oGenericTile && oGenericTile.getContent && oGenericTile.getContent()[0]
							&& oGenericTile.getContent()[0] instanceof sap.cloudfnd.smartbusiness.lib.reusetiles.Singleton) {
						oGenericTile.getContent()[0].getContent().getTileContent()[0].setTimestamp(time);
						if(oGenericTile.getModel("tileConfig")){
							oGenericTile.getModel("tileConfig").setProperty("/displayCachedTime", time);
						}
					}
				} catch(oException) {
					jQuery.sap.log.error("ssuite.smartbusiness.tiles.numeric.Component", 
							"Failed to set timestamp for tile : " + oException);
				}
			}
		},

		/**
		 * Schedules the update job for the tile when the cache gets invalidated
		 */
		initializeAutoTimeUpdater : function() {
			var timeToSchedule = 0;
			if (this._localData.updateSecondsForScheduler)
				this._localData.cacheAge = parseInt(this._localData.cacheAge) + this._localData.updateSecondsForScheduler;
			var seconds = Number(this._localData.cacheAge);
			timeToSchedule = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideTimeToSchedule(seconds,
					timeToSchedule);
			this._localData.updateSecondsForScheduler = timeToSchedule;
			jQuery.sap.log.info("Time to be updated in : ", timeToSchedule + " seconds");
			this._localData.delayedCallMethodIdentifier = jQuery.sap.delayedCall((timeToSchedule * 1000), null, jQuery.proxy(
					this.analyzeAndUpdateTime, this));
		},

		/**
		 * initialize the XML Templating
		 */
		_initializeChipDataLoad : function(oDataModel, isRefresh) {
			try {
				oDataModel.getMetaModel().loaded().then(
						jQuery.proxy(function() {
							this._localData.isMetaModelLoaded = true;
							if (this._localData.oMetaModelLoadedDeferred)
								this._localData.oMetaModelLoadedDeferred.resolve();
							
							this._localData.isTilePreprocessed = false;
							if(this._localData.tileVisible){
								this._localData.isTilePreprocessed = true;
								jQuery.proxy(this._initializeTilePreprocessing, this)(oDataModel, isRefresh);
							}
							
						}, this));
			} catch (err) {
				var message = typeof err !== "object" || jQuery.isEmptyObject(err) ? err.toString() : JSON.stringify(err);
				jQuery.sap.log.error("_initializeChipDataLoad", "preprocessing step failed - " + message);
				this.putTileToFailedState();
			}
		},

		/**
		 * Initialize the Tile processing
		 */
		_initializeTilePreprocessing : function(oDataModel, isRefresh) {
			
			this._getTileConfigModel().done(jQuery.proxy(function(oTileConfigModel) {
					
				try {
					var preprocessor = {
							xml : {
								models : {
									selectionVariant : oDataModel.getMetaModel(),
									dataPoint : oDataModel.getMetaModel(),
									tileConfig : oTileConfigModel
								},
								bindingContexts : {
									selectionVariant : sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype
											.getBindingContextFromFragment(oDataModel.getMetaModel(),
													this._localData.ssbProperties.annotationFragments.selectionVariant, oTileConfigModel),
									dataPoint : sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.getBindingContextFromFragment(
											oDataModel.getMetaModel(), this._localData.ssbProperties.annotationFragments.dataPoint, oTileConfigModel),
									tileConfig : oTileConfigModel.createBindingContext("/")
								}
							}
						};

						var tileConfigString = this.oChipInfo && this.oChipInfo.tileConfiguration;
						var chipDetails = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.fetchChipDetails(this.oChipInfo);
						var cacheMaxAge = chipDetails.cacheMaxAge;
						var cacheMaxAgeUnit = chipDetails.cacheMaxAgeUnit;
						var maxAge = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatCacheMaxAgeInSeconds(cacheMaxAge,
								cacheMaxAgeUnit);
						// set headers for sending cache age in request header
						if (maxAge != 0) {
							if (isRefresh)
								oDataModel.setHeaders({
									"Cache-Control" : "max-age=0"
								});
							else
								oDataModel.setHeaders({
									"Cache-Control" : "max-age=" + maxAge
								});
						}
						// The OData model(s) must be made known to the preprocessor.
						sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype
								.addODataModelForPreprocessing(preprocessor, oDataModel);
						var view = sap.ui.view({
							models : {
								component : new sap.ui.model.json.JSONModel(this)
							},
							preprocessors : preprocessor,
							type : sap.ui.core.mvc.ViewType.XML,
							viewName : "ssuite.smartbusiness.tiles.numeric.NumericTile",
							viewData : this.oChipInfo
						});
						this.content.removeAllItems();
						this.content.addItem(view);
						// only case where the preprocessing should happen based on the cached data is - Caching is
						// enabled
						// (decided by flag bNoCachingEnabled) and Cache is available(decided by flag noCache)
						if (!this._localData.noCache && !this._localData.bNoCachingEnabled) {
							// Clear any previously set schedulers
							if (this._localData.delayedUpdateCacheIdentifier) {
								jQuery.sap.clearDelayedCall(this._localData.delayedUpdateCacheIdentifier);
								this._localData.delayedUpdateCacheIdentifier = null;
							}
							if (this._localData.delayedCallMethodIdentifier) {
								jQuery.sap.clearDelayedCall(this._localData.delayedCallMethodIdentifier);
								this._localData.delayedCallMethodIdentifier = null;
							}
							// Initialize schedulers to update the displayed cached age and update the cached data
							this.initializeAutoCacheUpdateScheduler();
							this.analyzeAndUpdateTime();
							this.content.setModel(this._tileCachedDataModel);
							var cachedDataBindingContext = new sap.ui.model.Context(this._tileCachedDataModel, "/0");
							this.content.setBindingContext(cachedDataBindingContext);
							this._localData.oDataModel = oDataModel;
							var oData = this._tileCachedDataModel.getData();
							var tileInstance = view.getContent()[0];
							var oTile = tileInstance.getContent();
							sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.onDataLoaded(oData, oTile);
						} else {
							this._localData.oneTimeLoad = true;
							this.content.setModel(oDataModel);
						}
						// put the tile to failed state if some data request fails in some point of time.
						oRequestManager.handleDataFailure(oDataModel, jQuery.proxy(this.putTileToFailedState, this));
						var oUtilityFunctions = sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype;
						oUtilityFunctions.setCacheAgeToTileDataAgeTimeStamp(this, this._localData.cacheAge);
					} catch (err) {
						var message = typeof err !== "object" || jQuery.isEmptyObject(err) ? err.toString() : JSON.stringify(err);
						jQuery.sap.log.error("_initializeTilePreprocessing", "preprocessing step failed - " + message);
						this.putTileToFailedState();
					}
				}, this)).fail(jQuery.proxy(function(oResponse) {
					jQuery.sap.log.error(this._sFileName + "/_initializeTilePreprocessing", oResponse || "Failed to get the 'tileConfig' model.");
					this.putTileToFailedState();
				}, this));
		},

		/**
		 * exit handler for component - clear any delayedcalls associated with this component
		 */
		exit : function() {
			// Clear any previously set schedulers
			if (this && this._localData && this._localData.delayedUpdateCacheIdentifier) {
				jQuery.sap.clearDelayedCall(this._localData.delayedUpdateCacheIdentifier);
				this._localData.delayedUpdateCacheIdentifier = null;
			}
			if (this && this._localData && this._localData.delayedCallMethodIdentifier) {
				jQuery.sap.clearDelayedCall(this._localData.delayedCallMethodIdentifier);
				this._localData.delayedCallMethodIdentifier = null;
			}
		}
	});
}, true);
},
	"ssuite/smartbusiness/tiles/numeric/NumericContent.fragment.xml":'<core:FragmentDefinition xmlns="sap.m"\n\txmlns:core="sap.ui.core">\n\t<TileContent unit="USD">\n\t\t<content>\n\t\t\t<NumericContent value="0.0" scale="M" valueColor="Good" />\n\t\t</content>\n\t</TileContent>\n</core:FragmentDefinition>',
	"ssuite/smartbusiness/tiles/numeric/NumericTile.controller.js":function(){sap.ui.define(["jquery.sap.global", "sap/ui/thirdparty/URI", "sap/m/MessageBox",
		"sap/cloudfnd/smartbusiness/lib/reusetiles/AppStateManager"],

function(jQuery, URI, MessageBox, AppStateManager) {

	"use strict";

	sap.ui.controller("ssuite.smartbusiness.tiles.numeric.NumericTile", {

		onPress : function() {

			var AppStateManager = sap.cloudfnd.smartbusiness.lib.reusetiles.AppStateManager;
			var TileNavigation = sap.cloudfnd.smartbusiness.lib.reusetiles.TileNavigation;
			var Util = new sap.cloudfnd.smartbusiness.lib.reusetiles.Util();

			var oEvaluationData = {};
			var aViewContent = this.getView().getContent();
			if (aViewContent instanceof Array && aViewContent.length) {
				// Extract evaluation data processed sand saved at Singleton.
				oEvaluationData = aViewContent[0] && aViewContent[0].data('evaluationData');
			}

			var oComponent = this.getView().getModel("component").getData();
			var oDataModel = oComponent && oComponent.getModel();
			if (oDataModel instanceof sap.ui.model.odata.v2.ODataModel) {
				oEvaluationData["odataURL"] = oDataModel.sServiceUrl;
			}

			this._oResourceBundle = oComponent.getModel("i18n").getResourceBundle();
			// Disabling the update schedulers for the selected tile
			if (oComponent._localData.delayedUpdateCacheIdentifier) {
				jQuery.sap.clearDelayedCall(oComponent._localData.delayedUpdateCacheIdentifier);
				oComponent._localData.delayedUpdateCacheIdentifier = null;
			}
			if (oComponent._localData.delayedCallMethodIdentifier) {
				jQuery.sap.clearDelayedCall(oComponent._localData.delayedCallMethodIdentifier);
				oComponent._localData.delayedCallMethodIdentifier = null;
			}
			oComponent._localData.cacheRefreshRequired = true;
			var oChipInfo = this.getView().getViewData();
			var tileConfigString = oChipInfo && oChipInfo.tileConfiguration;
			var tileConfigObject = JSON.parse(tileConfigString);
			var tileProperties = JSON.parse(tileConfigObject.TILE_PROPERTIES);
			// oDefaultParameters are stored in format
			// [{"Id":"CompanyCode","Label":"Company Code"}]
			// In case of Pers tile the config will not be present
			var oDefaultParameters = Util.fetchDefaultParameters(tileConfigObject);
			var additionalParams = oChipInfo && oChipInfo.ADDITIONAL_APP_PARAMETERS;
			var oNavigationParameters = (oChipInfo && oChipInfo.NAVIGATION_PARAMETERS) || {};

			if (tileConfigObject && jQuery.isEmptyObject(tileConfigObject.EVALUATION) === false) {
				var oEvaluationDataFromTile = JSON.parse(tileConfigObject.EVALUATION);
				if (oEvaluationDataFromTile.SADL_ANNOTATION_MODEL && oEvaluationDataFromTile.SADL_ANNOTATION_MODEL_VERSION) {
					oEvaluationData["cdsAnnotationName"] = oEvaluationDataFromTile.SADL_ANNOTATION_MODEL;
					oEvaluationData["cdsAnnotationVersion"] = oEvaluationDataFromTile.SADL_ANNOTATION_MODEL_VERSION;
				}
			}

			var oURLParameters = TileNavigation.getURLParameters(tileProperties, oNavigationParameters, additionalParams);

			var appStateKey = null;
			var oChip = {};
			var chipBag = {};
			if (oChipInfo && oChipInfo.flpType != "CDM") {
				oChip = oChipInfo && oChipInfo.chipContract;
				chipBag = oChip && oChip.bag && oChip.bag.getBag("sb_tileProperties");
			}

			var oCrossApplicationNavigation = sap.ushell.Container.getService("CrossApplicationNavigation");
			oCrossApplicationNavigation.isNavigationSupported([{
				target : {
					semanticObject : oChipInfo.semanticObject,
					action : oChipInfo.semanticAction
				},
				params : oURLParameters
			}]).done(
					jQuery.proxy(function(aResponses) {
						if (aResponses[0].supported === true) {
							// personalization is not possible in CDM FLP yet
							if (oChipInfo && oChipInfo.flpType != "CDM" && chipBag && chipBag.getProperty
									&& chipBag.getProperty("isDataStoredAsAppState", null) == 'true') {
								appStateKey = chipBag.getProperty("personalizationAppStateKey");
								TileNavigation.navigateToApplication(oChipInfo.semanticObject, oChipInfo.semanticAction,
										oURLParameters, appStateKey, true);
							} else {
								// If target applicaiton is ssb runtime pass data as iapp else xapp
								if (tileProperties.navType == "0") {
									// Here we have removed the app state creation and app state will not be passed in case a generic tile
									// is launched
									// This is because we have to consider default values, and we cannot pass it in appstate
									// as we might have to create a different section in state and also there were other cases
									TileNavigation.navigateToApplication(oChipInfo.semanticObject, oChipInfo.semanticAction,
											oURLParameters, null, false);
								} else if (tileProperties.navType == "4" || tileProperties.navType == "5") {
									if (!this.sAppStateKey) {
										// Navigation to SAC integration wrapper application with i-app-state.
										// Applicable for SAC story application & AdHoc analysis application.

										var oSACIAppStatePayload = AppStateManager
												.getIAppStatePayloadForSACIntegrationApplication(oChipInfo.Title, oEvaluationData, this
														.getView().getModel(), tileProperties, oDefaultParameters);

										var oAppState = AppStateManager.createAppState(this.getView().getModel('component').getData(),
												oSACIAppStatePayload);
										this.sAppStateKey = oAppState && oAppState.getKey();
									}
									var oSACURLParameters = TileNavigation.getURLParameters(tileProperties, oNavigationParameters,
											additionalParams, oDefaultParameters);
									TileNavigation.navigateToApplication(oChipInfo.semanticObject, oChipInfo.semanticAction,
											oSACURLParameters, this.sAppStateKey, true);

								} else {
									if (!this.sAppStateKey) {
										// create appstatepayload
										var appStatePayload = jQuery.proxy(Util.fetchStartupParameters, this)(oChip, oURLParameters,
												tileProperties);
										// Create empty app state and save the payload in that.
										var appNavSrv = sap.ushell && sap.ushell.Container
												&& sap.ushell.Container.getService("CrossApplicationNavigation");
										var appState = appNavSrv.createEmptyAppState(this.getView().getModel('component').getData());
										// Just passing the standard data in xappstate for other apps
										appState.setData({
											selectionVariant : appStatePayload
										});
										appState.save().fail(function(errorMessage) {
											jQuery.sap.log.error("saving of application state failed" + errorMessage);
										});
										this.sAppStateKey = appState && appState.getKey();
									}
									// For incident 1770406692, we have to remove EvaluationId from url parameter list to external apps
									// EvaluationId is removed from navigation url parameters after 1808/1809 release
									delete oURLParameters.EvaluationId;
									TileNavigation.navigateToApplication(oChipInfo.semanticObject, oChipInfo.semanticAction,
											oURLParameters, this.sAppStateKey, false);
								}
							}
						} else {
							var sErrorText = this._oResourceBundle.getText("NAV_ERROR_MESSAGE");
							MessageBox.error(sErrorText);
							jQuery.sap.log.error("Navigation to application is not possible",
									"you either do not have permission to execute the"
											+ " application or the device does not support the application");
						}
					}, this));
		},

		timeStampRefresh : function(oEvent) {
			var oComponent = oEvent.getSource().getModel("component").getData();
			if (oComponent._localData.delayedUpdateCacheIdentifier) {
				jQuery.sap.clearDelayedCall(oComponent._localData.delayedUpdateCacheIdentifier);
				oComponent._localData.delayedUpdateCacheIdentifier = null;
			}
			oComponent.updateCacheDataFromBackend(true, true);
			oComponent._localData.hanaCacheAge = null;
		}

	});
});
},
	"ssuite/smartbusiness/tiles/numeric/NumericTile.view.xml":'<mvc:View xmlns:mvc="sap.ui.core.mvc" xmlns="sap.m" xmlns:core="sap.ui.core"\n\txmlns:ssb="sap.cloudfnd.smartbusiness.lib.reusetiles"\n\txmlns:template="http://schemas.sap.com/sapui5/extension/sap.ui.core.template/1"\n\tcontrollerName="ssuite.smartbusiness.tiles.numeric.NumericTile">\n\t<template:with path="dataPoint>"\n\t\thelper="sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.resolveMetaModelRoot"\n\t\tvar="meta">\n\t\t<template:with path="dataPoint>"\n\t\t\thelper="sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.resolveEntityType"\n\t\t\tvar="entityType">\n\t\t\t<ssb:Singleton\n\t\t\t\tsingleton="{ parts: [ { path: \'meta>\' }, { path: \'entityType>\' }, { path: \'selectionVariant>\' }, { path: \'meta>.\' }, { path: \'meta>.\' }, { path: \'dataPoint>\' }, { path: \'meta>.\' }, { path: \'tileConfig>personalizedFilters\'}, {path: \'tileConfig>loadFromCache\'}, {path: \'tileConfig>\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatDataBinding\' }">\n\t\t\t\t<ssb:singleton>\n\t\t\t\t\t<core:Element />\n\t\t\t\t</ssb:singleton>\n\t\t\t\t<ssb:customData>\n\t\t\t\t\t<core:CustomData key="evaluationData"\n\t\t\t\t\t\tvalue="{ parts:[ {path:\'entityType>\'}, {path: \'selectionVariant>\'}, {path: \'dataPoint>\'}, { path: \'tileConfig>\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatEvaluationData\' }" />\n\t\t\t\t</ssb:customData>\n\t\t\t\t<ssb:content>\n\t\t\t\t\t<GenericTile press=".onPress" header="{tileConfig>Title}"\n\t\t\t\t\t\tsubheader="{tileConfig>SubTitle}"\n\t\t\t\t\t\tsizeBehavior="{path : \'tileConfig>\', formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.getSizeBehaviorPropertyPath\'}">\n\t\t\t\t\t\t<tileContent>\n\t\t\t\t\t\t\t<template:with path="dataPoint>Value" var="val"\n\t\t\t\t\t\t\t\thelper="sap.ui.model.odata.AnnotationHelper.resolvePath">\n\t\t\t\t\t\t\t\t<template:if test="{tileConfig>cacheEnabled}">\n\t\t\t\t\t\t\t\t\t<template:then>\n\t\t\t\t\t\t\t\t\t\t<ssb:TimeStampControl\n\t\t\t\t\t\t\t\t\t\t\ttooltip="{ parts: [{path: \'dataPoint>\'}, {path: \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'val>Org.OData.Measures.V1.Unit\'}, {path: \'tileConfig>personalizedThresholds\'}, { path: \'tileConfig>personalizedFilters\'}], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.setTooltipForNumericTile\' }"\n\t\t\t\t\t\t\t\t\t\t\trefreshOption="{tileConfig>cacheEnabled}" refresh="timeStampRefresh"\n\t\t\t\t\t\t\t\t\t\t\tbusy="true" busyIndicatorDelay="0" class="busyTileContentClick"\n\t\t\t\t\t\t\t\t\t\t\tunit="{parts : [{path : \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'dataPoint>Value\'}, {path : \'val>Org.OData.Measures.V1.Unit\'}], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideUnitBindingPath\'}">\n\t\t\t\t\t\t\t\t\t\t\t<ssb:content>\n\t\t\t\t\t\t\t\t\t\t\t\t<NumericContent truncateValueTo="5"\n\t\t\t\t\t\t\t\t\t\t\t\t\tformatterValue="true"\n\t\t\t\t\t\t\t\t\t\t\t\t\tindicator="{ parts: [ {path:\'dataPoint>Value\'}, {path:\'dataPoint>TrendCalculation\'}, {path: \'dataPoint>ValueFormat\'}, {path:\'dataPoint>CriticalityCalculation\'}, { path: \'tileConfig>personalizedFilters\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatTrendIndicator\' }"\n\t\t\t\t\t\t\t\t\t\t\t\t\tvalue="{ parts: [ {path: \'dataPoint>Value\'}, {path: \'dataPoint>ValueFormat\'}, {path: \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'val>Org.OData.Measures.V1.Unit\'} , {path: \'dataPoint>\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideEvaluationMeasureBindPath\' }"\n\t\t\t\t\t\t\t\t\t\t\t\t\tvalueColor="{ parts:[ {path:\'dataPoint>\'}, {path: \'tileConfig>personalizedThresholds\'}, { path: \'tileConfig>personalizedFilters\'}, { path: \'tileConfig>DefaultParameters\'} ],\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tformatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatCriticality\' }" />\n\t\t\t\t\t\t\t\t\t\t\t</ssb:content>\n\t\t\t\t\t\t\t\t\t\t</ssb:TimeStampControl>\n\t\t\t\t\t\t\t\t\t</template:then>\n\t\t\t\t\t\t\t\t\t<template:else>\n\t\t\t\t\t\t\t\t\t\t<TileContent busy="true" busyIndicatorDelay="0"\n\t\t\t\t\t\t\t\t\t\t\tclass="busyTileContentClick"\n\t\t\t\t\t\t\t\t\t\t\ttooltip="{ parts: [{path: \'dataPoint>\'}, {path: \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'val>Org.OData.Measures.V1.Unit\'}, {path: \'tileConfig>personalizedThresholds\'}, { path: \'tileConfig>personalizedFilters\'}], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.setTooltipForNumericTile\' }"\n\t\t\t\t\t\t\t\t\t\t\tunit="{parts : [{path : \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'dataPoint>Value\'}, {path : \'val>Org.OData.Measures.V1.Unit\'}], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideUnitBindingPath\'}">\n\t\t\t\t\t\t\t\t\t\t\t<content>\n\t\t\t\t\t\t\t\t\t\t\t\t<NumericContent truncateValueTo="5"\n\t\t\t\t\t\t\t\t\t\t\t\t\tformatterValue="true"\n\t\t\t\t\t\t\t\t\t\t\t\t\tindicator="{ parts: [ {path:\'dataPoint>Value\'}, {path:\'dataPoint>TrendCalculation\'}, {path: \'dataPoint>ValueFormat\'}, {path:\'dataPoint>CriticalityCalculation\'}, { path: \'tileConfig>personalizedFilters\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatTrendIndicator\' }"\n\t\t\t\t\t\t\t\t\t\t\t\t\tvalue="{ parts: [ {path: \'dataPoint>Value\'}, {path: \'dataPoint>ValueFormat\'}, {path: \'val>Org.OData.Measures.V1.ISOCurrency\'}, {path: \'val>Org.OData.Measures.V1.Unit\'} , {path: \'dataPoint>\'} ], formatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.decideEvaluationMeasureBindPath\' }"\n\t\t\t\t\t\t\t\t\t\t\t\t\tvalueColor="{ parts:[ {path:\'dataPoint>\'}, {path: \'tileConfig>personalizedThresholds\'}, { path: \'tileConfig>personalizedFilters\'}, { path: \'tileConfig>DefaultParameters\'} ],\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tformatter: \'sap.cloudfnd.smartbusiness.lib.reusetiles.Util.prototype.formatCriticality\' }" />\n\t\t\t\t\t\t\t\t\t\t\t</content>\n\t\t\t\t\t\t\t\t\t\t</TileContent>\n\t\t\t\t\t\t\t\t\t</template:else>\n\t\t\t\t\t\t\t\t</template:if>\n\t\t\t\t\t\t\t</template:with>\n\t\t\t\t\t\t</tileContent>\n\t\t\t\t\t</GenericTile>\n\t\t\t\t</ssb:content>\n\t\t\t</ssb:Singleton>\n\t\t</template:with>\n\t</template:with>\n</mvc:View>',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n.properties":'# The texts here represents all the translatable texts present in the SAP Smart Business KPI Application\n# __ldi.translation.uuid=c80229e0-2152-11e8-b566-0800200c9a66\n\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY = No data available\n#XTIT\nTIME_SEC =  sec.\n#XTIT\nTIME_MIN =  min.\n#XTIT\nTIME_HR =  hr.\n#XTIT\nTIME_DAY =  day\n#XTIT\nTIME_DAYS =  days\n#XTIT\nTIME_HOUR_FULL =  hour\n#XTIT\nTIME_HOURS_FULL =  hours\n#XTIT\nTIME_MINUTE_FULL =  minute\n#XTIT\nTIME_MINUTES_FULL =  minutes\n#XTIT\nTIME_SECOND_FULL =  second\n#XTIT\nTIME_SECONDS_FULL =  seconds\n#XSEL\nERROR=Error\n#XTIT\nWARNING = Warning\n#XSEL\nGOOD=Good\n#XSEL\nCRITICAL=Critical\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE = Navigation to application is not possible; you either do not have permission to execute the application or the device does not support the application.\n#XTIT\nACTUAL=Actual\n#XTIT\nFULLSCREEN_TITLE = Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeric Micro Chart',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ar.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0644\\u0627 \\u062A\\u062A\\u0648\\u0641\\u0631 \\u0623\\u064A \\u0628\\u064A\\u0627\\u0646\\u0627\\u062A\n#XTIT\nTIME_SEC=\\u062B\\u0648\\u0627\\u0646\\u064D\n#XTIT\nTIME_MIN=\\u062F\\u0642\\u0627\\u0626\\u0642\n#XTIT\nTIME_HR=\\u0633\\u0627\\u0639\\u0627\\u062A\n#XTIT\nTIME_DAY=\\u064A\\u0648\\u0645\n#XTIT\nTIME_DAYS=\\u0623\\u064A\\u0627\\u0645\n#XTIT\nTIME_HOUR_FULL=\\u0627\\u0644\\u0633\\u0627\\u0639\\u0629\n#XTIT\nTIME_HOURS_FULL=\\u0627\\u0644\\u0633\\u0627\\u0639\\u0627\\u062A\n#XTIT\nTIME_MINUTE_FULL=\\u0627\\u0644\\u062F\\u0642\\u064A\\u0642\\u0629\n#XTIT\nTIME_MINUTES_FULL=\\u062F\\u0642\\u0627\\u0626\\u0642\n#XTIT\nTIME_SECOND_FULL=\\u062B\\u0627\\u0646\\u064A\\u0629\n#XTIT\nTIME_SECONDS_FULL=\\u062B\\u0648\\u0627\\u0646\\u064D\n#XSEL\nERROR=\\u062E\\u0637\\u0623\n#XTIT\nWARNING=\\u062A\\u062D\\u0630\\u064A\\u0631\n#XSEL\nGOOD=\\u062C\\u064A\\u062F\n#XSEL\nCRITICAL=\\u062E\\u0637\\u0631\n#XSEL\nNEUTRAL=\\u0645\\u062D\\u0627\\u064A\\u062F\n#YMSG\nNAV_ERROR_MESSAGE=\\u0644\\u0627 \\u064A\\u0645\\u0643\\u0646 \\u0627\\u0644\\u062A\\u0646\\u0642\\u0644 \\u0625\\u0644\\u0649 \\u0627\\u0644\\u062A\\u0637\\u0628\\u064A\\u0642\\u061B \\u0625\\u0645\\u0627 \\u0623\\u0646\\u0647 \\u0644\\u064A\\u0633 \\u0644\\u062F\\u064A\\u0643 \\u0625\\u0630\\u0646 \\u0644\\u062A\\u0646\\u0641\\u064A\\u0630 \\u0627\\u0644\\u062A\\u0637\\u0628\\u064A\\u0642 \\u0648\\u0625\\u0645\\u0627 \\u0623\\u0646 \\u0627\\u0644\\u062C\\u0647\\u0627\\u0632 \\u0644\\u0627 \\u064A\\u062F\\u0639\\u0645 \\u0627\\u0644\\u062A\\u0637\\u0628\\u064A\\u0642.\n#XTIT\nACTUAL=\\u0627\\u0644\\u0641\\u0639\\u0644\\u064A\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0645\\u062E\\u0637\\u0637 \\u0635\\u063A\\u064A\\u0631 \\u0631\\u0642\\u0645\\u064A\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_bg.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u041D\\u044F\\u043C\\u0430 \\u043D\\u0430\\u043B\\u0438\\u0447\\u043D\\u0438 \\u0434\\u0430\\u043D\\u043D\\u0438\n#XTIT\nTIME_SEC=\\u0441\\u0435\\u043A\n#XTIT\nTIME_MIN=\\u043C\\u0438\\u043D\n#XTIT\nTIME_HR=\\u0447\\u0430\\u0441\n#XTIT\nTIME_DAY=\\u0434\\u0435\\u043D\n#XTIT\nTIME_DAYS=\\u0434\\u043D\\u0438\n#XTIT\nTIME_HOUR_FULL=\\u0447\\u0430\\u0441\n#XTIT\nTIME_HOURS_FULL=\\u0447\\u0430\\u0441\\u043E\\u0432\\u0435\n#XTIT\nTIME_MINUTE_FULL=\\u043C\\u0438\\u043D\\u0443\\u0442\\u0430\n#XTIT\nTIME_MINUTES_FULL=\\u043C\\u0438\\u043D\\u0443\\u0442\\u0438\n#XTIT\nTIME_SECOND_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\\u0430\n#XTIT\nTIME_SECONDS_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\\u0438\n#XSEL\nERROR=\\u0413\\u0440\\u0435\\u0448\\u043A\\u0430\n#XTIT\nWARNING=\\u041F\\u0440\\u0435\\u0434\\u0443\\u043F\\u0440\\u0435\\u0436\\u0434\\u0435\\u043D\\u0438\\u0435\n#XSEL\nGOOD=\\u0414\\u043E\\u0431\\u044A\\u0440\n#XSEL\nCRITICAL=\\u041A\\u0440\\u0438\\u0442\\u0438\\u0447\\u0435\\u043D\n#XSEL\nNEUTRAL=\\u041D\\u0435\\u0443\\u0442\\u0440\\u0430\\u043B\\u0435\\u043D\n#YMSG\nNAV_ERROR_MESSAGE=\\u041D\\u0435\\u0432\\u044A\\u0437\\u043C\\u043E\\u0436\\u043D\\u0430 \\u043D\\u0430\\u0432\\u0438\\u0433\\u0430\\u0446\\u0438\\u044F \\u0434\\u043E \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u0435\\u0442\\u043E; \\u0438\\u043B\\u0438 \\u043D\\u044F\\u043C\\u0430\\u0442\\u0435 \\u0440\\u0430\\u0437\\u0440\\u0435\\u0448\\u0435\\u043D\\u0438\\u0435 \\u0437\\u0430 \\u0438\\u0437\\u043F\\u044A\\u043B\\u043D\\u0435\\u043D\\u0438\\u0435 \\u043D\\u0430 \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u0435\\u0442\\u043E, \\u0438\\u043B\\u0438 \\u0443\\u0441\\u0442\\u0440\\u043E\\u0439\\u0441\\u0442\\u0432\\u043E\\u0442\\u043E \\u043D\\u0435 \\u043F\\u043E\\u0434\\u0434\\u044A\\u0440\\u0436\\u0430 \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u0435\\u0442\\u043E.\n#XTIT\nACTUAL=\\u0424\\u0430\\u043A\\u0442\\u0438\\u0447\\u0435\\u0441\\u043A\\u0438\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0427\\u0438\\u0441\\u043B\\u043E\\u0432\\u0430 \\u043C\\u0438\\u043A\\u0440\\u043E \\u0434\\u0438\\u0430\\u0433\\u0440\\u0430\\u043C\\u0430\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ca.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=No hi ha dades disponibles\n#XTIT\nTIME_SEC=segons\n#XTIT\nTIME_MIN=minuts\n#XTIT\nTIME_HR=hores\n#XTIT\nTIME_DAY=dia\n#XTIT\nTIME_DAYS=dies\n#XTIT\nTIME_HOUR_FULL=hora\n#XTIT\nTIME_HOURS_FULL=hores\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=minuts\n#XTIT\nTIME_SECOND_FULL=segon\n#XTIT\nTIME_SECONDS_FULL=segons\n#XSEL\nERROR=Error\n#XTIT\nWARNING=Advert\\u00E8ncia\n#XSEL\nGOOD=B\\u00E9\n#XSEL\nCRITICAL=Cr\\u00EDtic\n#XSEL\nNEUTRAL=Neutre\n#YMSG\nNAV_ERROR_MESSAGE=No \\u00E9s possible la navegaci\\u00F3 a l\'aplicaci\\u00F3. \\u00C9s possible que no tingueu autoritzaci\\u00F3 per executar l\'aplicaci\\u00F3 o el dispositiu no \\u00E9s compatible amb l\'aplicaci\\u00F3.\n#XTIT\nACTUAL=Real\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Microgr\\u00E0fic num\\u00E8ric\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_cs.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Nejsou k dispozici \\u017E\\u00E1dn\\u00E1 data\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=hod.\n#XTIT\nTIME_DAY=den\n#XTIT\nTIME_DAYS=dny\n#XTIT\nTIME_HOUR_FULL=hodina\n#XTIT\nTIME_HOURS_FULL=hodiny\n#XTIT\nTIME_MINUTE_FULL=minuta\n#XTIT\nTIME_MINUTES_FULL=minuty\n#XTIT\nTIME_SECOND_FULL=sekunda\n#XTIT\nTIME_SECONDS_FULL=sekundy\n#XSEL\nERROR=Chyba\n#XTIT\nWARNING=Upozorn\\u011Bn\\u00ED\n#XSEL\nGOOD=Dobr\\u00E9\n#XSEL\nCRITICAL=Kritick\\u00E1\n#XSEL\nNEUTRAL=Neutr\\u00E1ln\\u00ED\n#YMSG\nNAV_ERROR_MESSAGE=P\\u0159echod do aplikace nen\\u00ED mo\\u017En\\u00FD; bu\\u010F nem\\u00E1te opr\\u00E1vn\\u011Bn\\u00ED spustit aplikaci, nebo za\\u0159\\u00EDzen\\u00ED aplikaci nepodporuje.\n#XTIT\nACTUAL=Skute\\u010Dnost\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerick\\u00FD mikrogram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_da.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Ingen data tilg\\u00E6ngelige\n#XTIT\nTIME_SEC=sek.\n#XTIT\nTIME_MIN=min.\n#XTIT\nTIME_HR=t.\n#XTIT\nTIME_DAY=dag\n#XTIT\nTIME_DAYS=dage\n#XTIT\nTIME_HOUR_FULL=time\n#XTIT\nTIME_HOURS_FULL=timer\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=minutter\n#XTIT\nTIME_SECOND_FULL=sekund\n#XTIT\nTIME_SECONDS_FULL=sekunder\n#XSEL\nERROR=Fejl\n#XTIT\nWARNING=Advarsel\n#XSEL\nGOOD=God\n#XSEL\nCRITICAL=Kritisk\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=Navigation til applikationen er ikke mulig; enten har du ikke tilladelse til at udf\\u00F8re applikationen, eller enheden underst\\u00F8tter ikke applikationen.\n#XTIT\nACTUAL=Faktisk\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerisk mikrodiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_de.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Keine Daten vorhanden\n#XTIT\nTIME_SEC=Sek.\n#XTIT\nTIME_MIN=Min.\n#XTIT\nTIME_HR=Std.\n#XTIT\nTIME_DAY=Tag\n#XTIT\nTIME_DAYS=Tage\n#XTIT\nTIME_HOUR_FULL=Stunde\n#XTIT\nTIME_HOURS_FULL=Stunden\n#XTIT\nTIME_MINUTE_FULL=Minute\n#XTIT\nTIME_MINUTES_FULL=Minuten\n#XTIT\nTIME_SECOND_FULL=Sekunde\n#XTIT\nTIME_SECONDS_FULL=Sekunden\n#XSEL\nERROR=Fehler\n#XTIT\nWARNING=Warnung\n#XSEL\nGOOD=Gut\n#XSEL\nCRITICAL=Kritisch\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=Die Navigation zur Anwendung ist nicht m\\u00F6glich\\: entweder sind Sie nicht berechtigt die Anwendung auszuf\\u00FChren oder das Ger\\u00E4t unterst\\u00FCtzt nicht die Anwendung.\n#XTIT\nACTUAL=Ist\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerisches Mikrodiagramm\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_el.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0394\\u03B5\\u03BD \\u03C5\\u03C0\\u03AC\\u03C1\\u03C7\\u03BF\\u03C5\\u03BD \\u03B4\\u03B9\\u03B1\\u03B8\\u03AD\\u03C3\\u03B9\\u03BC\\u03B1 \\u03B4\\u03B5\\u03B4\\u03BF\\u03BC\\u03AD\\u03BD\\u03B1\n#XTIT\nTIME_SEC=\\u03B4\\u03B5\\u03C5\\u03C4\n#XTIT\nTIME_MIN=\\u03BB\\u03B5\\u03C0\\u03C4\n#XTIT\nTIME_HR=\\u03CE\\u03C1\n#XTIT\nTIME_DAY=\\u03B7\\u03BC\\u03AD\\u03C1\n#XTIT\nTIME_DAYS=\\u03B7\\u03BC\\u03AD\\u03C1\n#XTIT\nTIME_HOUR_FULL=\\u03CE\\u03C1\\u03B1\n#XTIT\nTIME_HOURS_FULL=\\u03CE\\u03C1\\u03B5\\u03C2\n#XTIT\nTIME_MINUTE_FULL=\\u03BB\\u03B5\\u03C0\\u03C4\\u03CC\n#XTIT\nTIME_MINUTES_FULL=\\u03BB\\u03B5\\u03C0\\u03C4\\u03AC\n#XTIT\nTIME_SECOND_FULL=\\u03B4\\u03B5\\u03C5\\u03C4\\u03B5\\u03C1\\u03CC\\u03BB\\u03B5\\u03C0\\u03C4\\u03BF\n#XTIT\nTIME_SECONDS_FULL=\\u03B4\\u03B5\\u03C5\\u03C4\\u03B5\\u03C1\\u03CC\\u03BB\\u03B5\\u03C0\\u03C4\\u03B1\n#XSEL\nERROR=\\u03A3\\u03C6\\u03AC\\u03BB\\u03BC\\u03B1\n#XTIT\nWARNING=\\u03A0\\u03C1\\u03BF\\u03B5\\u03B9\\u03B4\\u03BF\\u03C0\\u03BF\\u03AF\\u03B7\\u03C3\\u03B7\n#XSEL\nGOOD=\\u039A\\u03B1\\u03BB\\u03CC\n#XSEL\nCRITICAL=\\u039A\\u03C1\\u03AF\\u03C3\\u03B9\\u03BC\\u03BF\\u03C2\n#XSEL\nNEUTRAL=\\u039F\\u03C5\\u03B4\\u03AD\\u03C4\\u03B5\\u03C1\\u03BF\n#YMSG\nNAV_ERROR_MESSAGE=\\u0397 \\u03C0\\u03BB\\u03BF\\u03AE\\u03B3\\u03B7\\u03C3\\u03B7 \\u03C3\\u03C4\\u03B7\\u03BD \\u03B5\\u03C6\\u03B1\\u03C1\\u03BC\\u03BF\\u03B3\\u03AE \\u03B4\\u03B5\\u03BD \\u03B5\\u03AF\\u03BD\\u03B1\\u03B9 \\u03B5\\u03C6\\u03B9\\u03BA\\u03C4\\u03AE. \\u0395\\u03AF\\u03C4\\u03B5 \\u03B4\\u03B5\\u03BD \\u03AD\\u03C7\\u03B5\\u03C4\\u03B5 \\u03AC\\u03B4\\u03B5\\u03B9\\u03B1 \\u03BD\\u03B1 \\u03B5\\u03BA\\u03C4\\u03B5\\u03BB\\u03AD\\u03C3\\u03B5\\u03C4\\u03B5 \\u03C4\\u03B7\\u03BD \\u03B5\\u03C6\\u03B1\\u03C1\\u03BC\\u03BF\\u03B3\\u03AE \\u03AE \\u03B7 \\u03C3\\u03C5\\u03C3\\u03BA\\u03B5\\u03C5\\u03AE \\u03B4\\u03B5\\u03BD \\u03C4\\u03B7\\u03BD \\u03C5\\u03C0\\u03BF\\u03C3\\u03C4\\u03B7\\u03C1\\u03AF\\u03B6\\u03B5\\u03B9.\n#XTIT\nACTUAL=\\u03A0\\u03C1\\u03B1\\u03B3\\u03BC\\u03B1\\u03C4\\u03B9\\u03BA\\u03CC\n#XTIT\nFULLSCREEN_TITLE=\\u0388\\u03BE\\u03C5\\u03C0\\u03BD\\u03B7 \\u0395\\u03C0\\u03B9\\u03C7\\u03B5\\u03AF\\u03C1\\u03B7\\u03C3\\u03B7\n#XTIT\nNUMERIC_MICRO_CHART=\\u0391\\u03C1\\u03B9\\u03B8\\u03BC\\u03B7\\u03C4\\u03B9\\u03BA\\u03CC \\u039C\\u03B9\\u03BA\\u03C1\\u03BF-\\u0393\\u03C1\\u03AC\\u03C6\\u03B7\\u03BC\\u03B1\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_en.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=No data available\n#XTIT\nTIME_SEC=sec\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=hr\n#XTIT\nTIME_DAY=day\n#XTIT\nTIME_DAYS=days\n#XTIT\nTIME_HOUR_FULL=hour\n#XTIT\nTIME_HOURS_FULL=hours\n#XTIT\nTIME_MINUTE_FULL=minute\n#XTIT\nTIME_MINUTES_FULL=minutes\n#XTIT\nTIME_SECOND_FULL=second\n#XTIT\nTIME_SECONDS_FULL=seconds\n#XSEL\nERROR=Error\n#XTIT\nWARNING=Warning\n#XSEL\nGOOD=Good\n#XSEL\nCRITICAL=Critical\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=Navigation to application is not possible; you either do not have permission to execute the application or the device does not support the application.\n#XTIT\nACTUAL=Actual\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeric Micro Chart\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_en_US_sappsd.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=[[[\\u0143\\u014F \\u018C\\u0105\\u0163\\u0105 \\u0105\\u028B\\u0105\\u012F\\u013A\\u0105\\u0183\\u013A\\u0113\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nTIME_SEC=[[[\\u015F\\u0113\\u010B.]]]\n#XTIT\nTIME_MIN=[[[\\u0271\\u012F\\u014B.]]]\n#XTIT\nTIME_HR=[[[\\u0125\\u0157.\\u2219]]]\n#XTIT\nTIME_DAY=[[[\\u018C\\u0105\\u0177\\u2219]]]\n#XTIT\nTIME_DAYS=[[[\\u018C\\u0105\\u0177\\u015F]]]\n#XTIT\nTIME_HOUR_FULL=[[[\\u0125\\u014F\\u0171\\u0157]]]\n#XTIT\nTIME_HOURS_FULL=[[[\\u0125\\u014F\\u0171\\u0157\\u015F\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nTIME_MINUTE_FULL=[[[\\u0271\\u012F\\u014B\\u0171\\u0163\\u0113\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nTIME_MINUTES_FULL=[[[\\u0271\\u012F\\u014B\\u0171\\u0163\\u0113\\u015F\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nTIME_SECOND_FULL=[[[\\u015F\\u0113\\u010B\\u014F\\u014B\\u018C\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nTIME_SECONDS_FULL=[[[\\u015F\\u0113\\u010B\\u014F\\u014B\\u018C\\u015F\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XSEL\nERROR=[[[\\u0114\\u0157\\u0157\\u014F\\u0157\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nWARNING=[[[\\u0174\\u0105\\u0157\\u014B\\u012F\\u014B\\u011F\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XSEL\nGOOD=[[[\\u0122\\u014F\\u014F\\u018C]]]\n#XSEL\nCRITICAL=[[[\\u0108\\u0157\\u012F\\u0163\\u012F\\u010B\\u0105\\u013A\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XSEL\nNEUTRAL=[[[\\u0143\\u0113\\u0171\\u0163\\u0157\\u0105\\u013A\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#YMSG\nNAV_ERROR_MESSAGE=[[[\\u0143\\u0105\\u028B\\u012F\\u011F\\u0105\\u0163\\u012F\\u014F\\u014B \\u0163\\u014F \\u0105\\u03C1\\u03C1\\u013A\\u012F\\u010B\\u0105\\u0163\\u012F\\u014F\\u014B \\u012F\\u015F \\u014B\\u014F\\u0163 \\u03C1\\u014F\\u015F\\u015F\\u012F\\u0183\\u013A\\u0113; \\u0177\\u014F\\u0171 \\u0113\\u012F\\u0163\\u0125\\u0113\\u0157 \\u018C\\u014F \\u014B\\u014F\\u0163 \\u0125\\u0105\\u028B\\u0113 \\u03C1\\u0113\\u0157\\u0271\\u012F\\u015F\\u015F\\u012F\\u014F\\u014B \\u0163\\u014F \\u0113\\u03C7\\u0113\\u010B\\u0171\\u0163\\u0113 \\u0163\\u0125\\u0113 \\u0105\\u03C1\\u03C1\\u013A\\u012F\\u010B\\u0105\\u0163\\u012F\\u014F\\u014B \\u014F\\u0157 \\u0163\\u0125\\u0113 \\u018C\\u0113\\u028B\\u012F\\u010B\\u0113 \\u018C\\u014F\\u0113\\u015F \\u014B\\u014F\\u0163 \\u015F\\u0171\\u03C1\\u03C1\\u014F\\u0157\\u0163 \\u0163\\u0125\\u0113 \\u0105\\u03C1\\u03C1\\u013A\\u012F\\u010B\\u0105\\u0163\\u012F\\u014F\\u014B.\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nACTUAL=[[[\\u0100\\u010B\\u0163\\u0171\\u0105\\u013A\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nFULLSCREEN_TITLE=[[[\\u015C\\u0271\\u0105\\u0157\\u0163 \\u0181\\u0171\\u015F\\u012F\\u014B\\u0113\\u015F\\u015F\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n#XTIT\nNUMERIC_MICRO_CHART=[[[\\u0143\\u0171\\u0271\\u0113\\u0157\\u012F\\u010B \\u039C\\u012F\\u010B\\u0157\\u014F \\u0108\\u0125\\u0105\\u0157\\u0163\\u2219\\u2219\\u2219\\u2219\\u2219]]]\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_en_US_saptrc.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=SPpohMWxGKOpqTLRZkTKxA_No data available\n#XTIT\nTIME_SEC=RVZDYqYI+N7SN1f0ulwTQA_sec.\n#XTIT\nTIME_MIN=E9BLkHh6Yw6mt0khMO+xcQ_min.\n#XTIT\nTIME_HR=1j7d1eGF88sSwB0MEg9ekw_hr.\n#XTIT\nTIME_DAY=NO+P7yNro02Pph6yByFglg_day\n#XTIT\nTIME_DAYS=tAYys6F9mJPPbnL7JN2Zyw_days\n#XTIT\nTIME_HOUR_FULL=VUMnUYLIXRZtN9u4lLPaJw_hour\n#XTIT\nTIME_HOURS_FULL=ZDFpXKqiHQ5jopbIiMHv6w_hours\n#XTIT\nTIME_MINUTE_FULL=IKfz5IXYGkE01e4buL1Q3Q_minute\n#XTIT\nTIME_MINUTES_FULL=xLig3lorGavcIbQJsPMhfQ_minutes\n#XTIT\nTIME_SECOND_FULL=xUreEDUeSv8Or4Mh/FbSGQ_second\n#XTIT\nTIME_SECONDS_FULL=BW1e1Dx/PURlEqiG/MTGnw_seconds\n#XSEL\nERROR=1vjXrbdvzqkKNIGJy3Idnw_Error\n#XTIT\nWARNING=KlLwf8xuYoQhIqxyPmyn7A_Warning\n#XSEL\nGOOD=yCXbmDJPr422OXpieG/haw_Good\n#XSEL\nCRITICAL=Ww+kUkKChlk/eHVx8tOD3w_Critical\n#XSEL\nNEUTRAL=IgsruV7ed0Q1m6w0Y5KUJw_Neutral\n#YMSG\nNAV_ERROR_MESSAGE=nMyCRpSKnFUjyy3E5FwGVw_Navigation to application is not possible; you either do not have permission to execute the application or the device does not support the application.\n#XTIT\nACTUAL=fanx1w/Y/MciWQKmsgmsGA_Actual\n#XTIT\nFULLSCREEN_TITLE=bZMhi95liOQu6SRNGd8NWw_Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=qCbuuWcnMwTDXdxQOkqDHQ_Numeric Micro Chart\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_es.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=No hay datos disponibles\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=d\\u00EDa\n#XTIT\nTIME_DAYS=d\\u00EDas\n#XTIT\nTIME_HOUR_FULL=hora\n#XTIT\nTIME_HOURS_FULL=horas\n#XTIT\nTIME_MINUTE_FULL=minuto\n#XTIT\nTIME_MINUTES_FULL=minutos\n#XTIT\nTIME_SECOND_FULL=segundo\n#XTIT\nTIME_SECONDS_FULL=segundos\n#XSEL\nERROR=Error\n#XTIT\nWARNING=Advertencia\n#XSEL\nGOOD=Bueno\n#XSEL\nCRITICAL=Cr\\u00EDtico\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=No es posible navegar a la aplicaci\\u00F3n. No tiene permiso para ejecutarla o el dispositivo no es compatible con ella.\n#XTIT\nACTUAL=Real\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Microgr\\u00E1fico num\\u00E9rico\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_et.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Andmeid pole saadaval\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=p\\u00E4ev\n#XTIT\nTIME_DAYS=p\\u00E4eva\n#XTIT\nTIME_HOUR_FULL=tund\n#XTIT\nTIME_HOURS_FULL=tundi\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=minutit\n#XTIT\nTIME_SECOND_FULL=sekund\n#XTIT\nTIME_SECONDS_FULL=sekundit\n#XSEL\nERROR=T\\u00F5rge\n#XTIT\nWARNING=Hoiatus\n#XSEL\nGOOD=\\u00D5ige\n#XSEL\nCRITICAL=Kriitiline\n#XSEL\nNEUTRAL=Neutraalne\n#YMSG\nNAV_ERROR_MESSAGE=Rakendusse navigeerimine pole v\\u00F5imalik; teil pole rakenduse k\\u00E4ivitamise \\u00F5igust v\\u00F5i seade ei toeta rakendust.\n#XTIT\nACTUAL=Tegelik\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Arvuline mikrodiagramm\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_fi.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Tietoja ei ole k\\u00E4ytett\\u00E4viss\\u00E4\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=pv\n#XTIT\nTIME_DAYS=pv\n#XTIT\nTIME_HOUR_FULL=h\n#XTIT\nTIME_HOURS_FULL=h\n#XTIT\nTIME_MINUTE_FULL=min\n#XTIT\nTIME_MINUTES_FULL=min\n#XTIT\nTIME_SECOND_FULL=s\n#XTIT\nTIME_SECONDS_FULL=s\n#XSEL\nERROR=Virhe\n#XTIT\nWARNING=Varoitus\n#XSEL\nGOOD=Hyv\\u00E4\n#XSEL\nCRITICAL=Kriittinen\n#XSEL\nNEUTRAL=Neutraali\n#YMSG\nNAV_ERROR_MESSAGE=Navigointi sovellukseen ei ole mahdollista; oikeus sovelluksen suorittamiseen puuttuu tai laite ei tue sovellusta.\n#XTIT\nACTUAL=Toteutunut\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeerinen mikrokaavio\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_fr.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Aucune donn\\u00E9e disponible\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=jour\n#XTIT\nTIME_DAYS=jours\n#XTIT\nTIME_HOUR_FULL=heure\n#XTIT\nTIME_HOURS_FULL=heures\n#XTIT\nTIME_MINUTE_FULL=minute\n#XTIT\nTIME_MINUTES_FULL=minutes\n#XTIT\nTIME_SECOND_FULL=seconde\n#XTIT\nTIME_SECONDS_FULL=secondes\n#XSEL\nERROR=Erreur\n#XTIT\nWARNING=Avertissement\n#XSEL\nGOOD=Bien\n#XSEL\nCRITICAL=Critique\n#XSEL\nNEUTRAL=Neutre\n#YMSG\nNAV_ERROR_MESSAGE=Navigation vers l\'application impossible\\u00A0\\: vous n\'avez pas l\'autorisation pour ex\\u00E9cuter l\'application ou le terminal ne prend pas en charge l\'application.\n#XTIT\nACTUAL=R\\u00E9el\n#XTIT\nFULLSCREEN_TITLE=Smart\\u00A0Business\n#XTIT\nNUMERIC_MICRO_CHART=Micro-diagramme num\\u00E9rique\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_hi.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0915\\u094B\\u0908 \\u0921\\u0947\\u091F\\u093E \\u0909\\u092A\\u0932\\u092C\\u094D\\u0927 \\u0928\\u0939\\u0940\\u0902\n#XTIT\nTIME_SEC=\\u0938\\u0947\\u0902\\u0915\\u0921\n#XTIT\nTIME_MIN=\\u092E\\u093F\\u0928\\u091F\n#XTIT\nTIME_HR=\\u0918\\u0902\\u091F\\u093E\n#XTIT\nTIME_DAY=\\u0926\\u093F\\u0928\n#XTIT\nTIME_DAYS=\\u0926\\u093F\\u0928\n#XTIT\nTIME_HOUR_FULL=\\u0918\\u0902\\u091F\\u093E\n#XTIT\nTIME_HOURS_FULL=\\u0918\\u0902\\u091F\\u093E\n#XTIT\nTIME_MINUTE_FULL=\\u092E\\u093F\\u0928\\u091F\n#XTIT\nTIME_MINUTES_FULL=\\u092E\\u093F\\u0928\\u091F\n#XTIT\nTIME_SECOND_FULL=\\u0938\\u0947\\u0915\\u0902\\u0921\n#XTIT\nTIME_SECONDS_FULL=\\u0938\\u0947\\u0915\\u0902\\u0921\n#XSEL\nERROR=\\u0924\\u094D\\u0930\\u0941\\u091F\\u093F\n#XTIT\nWARNING=\\u091A\\u0947\\u0924\\u093E\\u0935\\u0928\\u0940\n#XSEL\nGOOD=\\u0905\\u091A\\u094D\\u091B\\u093E\n#XSEL\nCRITICAL=\\u092E\\u0939\\u0924\\u094D\\u0935\\u092A\\u0942\\u0930\\u094D\\u0923\n#XSEL\nNEUTRAL=\\u0928\\u094D\\u092F\\u0942\\u091F\\u094D\\u0930\\u0932\n#YMSG\nNAV_ERROR_MESSAGE=\\u090F\\u092A\\u094D\\u0932\\u093F\\u0915\\u0947\\u0936\\u0928 \\u0915\\u0947 \\u0932\\u093F\\u090F \\u0928\\u0947\\u0935\\u093F\\u0917\\u0947\\u0936\\u0928 \\u0938\\u0902\\u092D\\u0935 \\u0928\\u0939\\u0940\\u0902 \\u0939\\u0948; \\u0906\\u092A \\u092F\\u093E \\u0924\\u094B \\u090F\\u092A\\u094D\\u0932\\u093F\\u0915\\u0947\\u0936\\u0928 \\u0928\\u093F\\u0937\\u094D\\u092A\\u093E\\u0926\\u093F\\u0924 \\u0915\\u0930\\u0928\\u0947 \\u0915\\u0940 \\u0905\\u0928\\u0941\\u092E\\u0924\\u093F \\u0928\\u0939\\u0940\\u0902 \\u0939\\u0948 \\u092F\\u093E \\u0921\\u093F\\u0935\\u093E\\u0907\\u0938 \\u090F\\u092A\\u094D\\u0932\\u093F\\u0915\\u0947\\u0936\\u0928 \\u0915\\u093E \\u0938\\u092E\\u0930\\u094D\\u0925\\u0928 \\u0928\\u0939\\u0940\\u0902 \\u0915\\u0930\\u0924\\u093E \\u0939\\u0948.\n#XTIT\nACTUAL=\\u0935\\u093E\\u0938\\u094D\\u0924\\u0935\\u093F\\u0915\n#XTIT\nFULLSCREEN_TITLE=\\u0938\\u094D\\u092E\\u093E\\u0930\\u094D\\u091F \\u092C\\u093F\\u091C\\u093C\\u0928\\u0947\\u0938\n#XTIT\nNUMERIC_MICRO_CHART=\\u0905\\u0902\\u0915\\u0940\\u092F \\u092E\\u093E\\u0907\\u0915\\u094D\\u0930\\u094B \\u091A\\u093E\\u0930\\u094D\\u091F\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_hr.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Podaci nisu raspolo\\u017Eivi\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=sat\n#XTIT\nTIME_DAY=dan\n#XTIT\nTIME_DAYS=dani\n#XTIT\nTIME_HOUR_FULL=sat\n#XTIT\nTIME_HOURS_FULL=sati\n#XTIT\nTIME_MINUTE_FULL=minuta\n#XTIT\nTIME_MINUTES_FULL=minute\n#XTIT\nTIME_SECOND_FULL=sekunda\n#XTIT\nTIME_SECONDS_FULL=sekunde\n#XSEL\nERROR=Gre\\u0161ka\n#XTIT\nWARNING=Upozorenje\n#XSEL\nGOOD=Dobro\n#XSEL\nCRITICAL=Kriti\\u010Dno\n#XSEL\nNEUTRAL=Neutralno\n#YMSG\nNAV_ERROR_MESSAGE=Usmjeravanje na aplikaciju nije mogu\\u0107e; ili nemate dopu\\u0161tenje za izvr\\u0161enje aplikacije ili ure\\u0111aj ne podr\\u017Eava aplikaciju.\n#XTIT\nACTUAL=Stvarno\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeri\\u010Dki mikro dijagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_hu.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Nem \\u00E1ll rendelkez\\u00E9sre adat\n#XTIT\nTIME_SEC=mp\n#XTIT\nTIME_MIN=perc\n#XTIT\nTIME_HR=\\u00F3ra\n#XTIT\nTIME_DAY=nap\n#XTIT\nTIME_DAYS=nap\n#XTIT\nTIME_HOUR_FULL=\\u00F3ra\n#XTIT\nTIME_HOURS_FULL=\\u00F3ra\n#XTIT\nTIME_MINUTE_FULL=perc\n#XTIT\nTIME_MINUTES_FULL=perc\n#XTIT\nTIME_SECOND_FULL=m\\u00E1sodperc\n#XTIT\nTIME_SECONDS_FULL=m\\u00E1sodperc\n#XSEL\nERROR=Hiba\n#XTIT\nWARNING=Figyelmeztet\\u00E9s\n#XSEL\nGOOD=J\\u00F3\n#XSEL\nCRITICAL=Kritikus\n#XSEL\nNEUTRAL=Semleges\n#YMSG\nNAV_ERROR_MESSAGE=A navig\\u00E1l\\u00E1s az alkalmaz\\u00E1shoz nem lehets\\u00E9ges; vagy nincs jogosults\\u00E1ga az alkalmaz\\u00E1s v\\u00E9grehajt\\u00E1s\\u00E1hoz, vagy az eszk\\u00F6z nem t\\u00E1mogatja azt.\n#XTIT\nACTUAL=T\\u00E9nyleges\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerikus mikrodiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_it.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Nessun dato disponibile\n#XTIT\nTIME_SEC=sec\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=ore\n#XTIT\nTIME_DAY=giorno\n#XTIT\nTIME_DAYS=giorni\n#XTIT\nTIME_HOUR_FULL=ora\n#XTIT\nTIME_HOURS_FULL=ore\n#XTIT\nTIME_MINUTE_FULL=minuto\n#XTIT\nTIME_MINUTES_FULL=minuti\n#XTIT\nTIME_SECOND_FULL=secondo\n#XTIT\nTIME_SECONDS_FULL=secondi\n#XSEL\nERROR=Errore\n#XTIT\nWARNING=Messaggio di avvertimento\n#XSEL\nGOOD=Positivo\n#XSEL\nCRITICAL=Critico\n#XSEL\nNEUTRAL=Neutro\n#YMSG\nNAV_ERROR_MESSAGE=Navigazione all\'applicazione impossibile; non disponi dell\'autorizzazione ad eseguire l\'applicazione o il dispositivo non supporta l\'applicazione.\n#XTIT\nACTUAL=Effettivo\n#XTIT\nFULLSCREEN_TITLE=SAP Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Micrografico numerico\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_iw.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u05D0\\u05D9\\u05DF \\u05E0\\u05EA\\u05D5\\u05E0\\u05D9\\u05DD \\u05D6\\u05DE\\u05D9\\u05E0\\u05D9\\u05DD\n#XTIT\nTIME_SEC=\\u05E9\\u05E0\\u05D9\\u05D9\\u05D4\n#XTIT\nTIME_MIN=\\u05D3\\u05E7\\u05D4\n#XTIT\nTIME_HR=\\u05E9\\u05E2\\u05D4\n#XTIT\nTIME_DAY=\\u05D9\\u05D5\\u05DD\n#XTIT\nTIME_DAYS=\\u05D9\\u05DE\\u05D9\\u05DD\n#XTIT\nTIME_HOUR_FULL=\\u05E9\\u05E2\\u05D4\n#XTIT\nTIME_HOURS_FULL=\\u05E9\\u05E2\\u05D5\\u05EA\n#XTIT\nTIME_MINUTE_FULL=\\u05D3\\u05E7\\u05D4\n#XTIT\nTIME_MINUTES_FULL=\\u05D3\\u05E7\\u05D5\\u05EA\n#XTIT\nTIME_SECOND_FULL=\\u05E9\\u05E0\\u05D9\\u05D9\\u05D4\n#XTIT\nTIME_SECONDS_FULL=\\u05E9\\u05E0\\u05D9\\u05D5\\u05EA\n#XSEL\nERROR=\\u05E9\\u05D2\\u05D9\\u05D0\\u05D4\n#XTIT\nWARNING=\\u05D0\\u05D6\\u05D4\\u05E8\\u05D4\n#XSEL\nGOOD=\\u05D8\\u05D5\\u05D1\n#XSEL\nCRITICAL=\\u05E7\\u05E8\\u05D9\\u05D8\\u05D9\n#XSEL\nNEUTRAL=\\u05E0\\u05D9\\u05D8\\u05E8\\u05DC\\u05D9\n#YMSG\nNAV_ERROR_MESSAGE=\\u05DC\\u05D0 \\u05E0\\u05D9\\u05EA\\u05DF \\u05DC\\u05E0\\u05D5\\u05D5\\u05D8 \\u05DC\\u05D9\\u05D9\\u05E9\\u05D5\\u05DD. \\u05D0\\u05D9\\u05DF \\u05DC\\u05DA \\u05D4\\u05E8\\u05E9\\u05D0\\u05D4 \\u05DC\\u05D1\\u05E6\\u05E2 \\u05D0\\u05EA \\u05D4\\u05D9\\u05D9\\u05E9\\u05D5\\u05DD \\u05D0\\u05D5 \\u05E9\\u05D4\\u05D4\\u05EA\\u05E7\\u05DF \\u05DC\\u05D0 \\u05EA\\u05D5\\u05DE\\u05DA \\u05D1\\u05D9\\u05D9\\u05E9\\u05D5\\u05DD.\n#XTIT\nACTUAL=\\u05D1\\u05E4\\u05D5\\u05E2\\u05DC\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u05DE\\u05D9\\u05E7\\u05E8\\u05D5 \\u05EA\\u05E8\\u05E9\\u05D9\\u05DD \\u05DE\\u05E1\\u05E4\\u05E8\\u05D9\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ja.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u5229\\u7528\\u53EF\\u80FD\\u30C7\\u30FC\\u30BF\\u306A\\u3057\n#XTIT\nTIME_SEC=\\u79D2\n#XTIT\nTIME_MIN=\\u5206\n#XTIT\nTIME_HR=\\u6642\n#XTIT\nTIME_DAY=\\u65E5\n#XTIT\nTIME_DAYS=\\u65E5\n#XTIT\nTIME_HOUR_FULL=\\u6642\n#XTIT\nTIME_HOURS_FULL=\\u6642\n#XTIT\nTIME_MINUTE_FULL=\\u5206\n#XTIT\nTIME_MINUTES_FULL=\\u5206\n#XTIT\nTIME_SECOND_FULL=\\u79D2\n#XTIT\nTIME_SECONDS_FULL=\\u79D2\n#XSEL\nERROR=\\u30A8\\u30E9\\u30FC\n#XTIT\nWARNING=\\u8B66\\u544A\n#XSEL\nGOOD=\\u826F\\u3044\n#XSEL\nCRITICAL=\\u30AF\\u30EA\\u30C6\\u30A3\\u30AB\\u30EB\n#XSEL\nNEUTRAL=\\u5909\\u52D5\\u306A\\u3057\n#YMSG\nNAV_ERROR_MESSAGE=\\u30A2\\u30D7\\u30EA\\u30B1\\u30FC\\u30B7\\u30E7\\u30F3\\u306B\\u30CA\\u30D3\\u30B2\\u30FC\\u30C8\\u3067\\u304D\\u307E\\u305B\\u3093\\u3002\\u30A2\\u30D7\\u30EA\\u30B1\\u30FC\\u30B7\\u30E7\\u30F3\\u3092\\u5B9F\\u884C\\u3059\\u308B\\u6A29\\u9650\\u304C\\u306A\\u3044\\u304B\\u3001\\u30C7\\u30D0\\u30A4\\u30B9\\u3067\\u30A2\\u30D7\\u30EA\\u30B1\\u30FC\\u30B7\\u30E7\\u30F3\\u304C\\u30B5\\u30DD\\u30FC\\u30C8\\u3055\\u308C\\u3066\\u3044\\u307E\\u305B\\u3093\\u3002\n#XTIT\nACTUAL=\\u5B9F\\u969B\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u6570\\u5024\\u30DF\\u30AF\\u30ED\\u30C1\\u30E3\\u30FC\\u30C8\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_kk.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0414\\u0435\\u0440\\u0435\\u043A\\u0442\\u0435\\u0440 \\u0436\\u043E\\u049B\n#XTIT\nTIME_SEC=\\u0441\\u0435\\u043A\n#XTIT\nTIME_MIN=\\u043C\\u0438\\u043D\n#XTIT\nTIME_HR=\\u0441\\u0430\\u0493\n#XTIT\nTIME_DAY=\\u043A\\u04AF\\u043D\n#XTIT\nTIME_DAYS=\\u043A\\u04AF\\u043D\n#XTIT\nTIME_HOUR_FULL=\\u0441\\u0430\\u0493\\u0430\\u0442\n#XTIT\nTIME_HOURS_FULL=\\u0441\\u0430\\u0493\\u0430\\u0442\n#XTIT\nTIME_MINUTE_FULL=\\u043C\\u0438\\u043D\\u0443\\u0442\n#XTIT\nTIME_MINUTES_FULL=\\u043C\\u0438\\u043D\\u0443\\u0442\n#XTIT\nTIME_SECOND_FULL=\\u0435\\u043A\\u0456\\u043D\\u0448\\u0456\n#XTIT\nTIME_SECONDS_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\n#XSEL\nERROR=\\u049A\\u0430\\u0442\\u0435\n#XTIT\nWARNING=\\u0415\\u0441\\u043A\\u0435\\u0440\\u0442\\u0443\n#XSEL\nGOOD=\\u0416\\u0430\\u049B\\u0441\\u044B\n#XSEL\nCRITICAL=\\u041A\\u0440\\u0438\\u0442\\u0438\\u043A\\u0430\\u043B\\u044B\\u049B\n#XSEL\nNEUTRAL=\\u0411\\u0435\\u0439\\u0442\\u0430\\u0440\\u0430\\u043F\n#YMSG\nNAV_ERROR_MESSAGE=\\u049A\\u043E\\u043B\\u0434\\u0430\\u043D\\u0431\\u0430\\u0493\\u0430 \\u04E9\\u0442\\u0443 \\u043C\\u04AF\\u043C\\u043A\\u0456\\u043D \\u0435\\u043C\\u0435\\u0441; \\u049B\\u043E\\u043B\\u0434\\u0430\\u043D\\u0431\\u0430\\u043D\\u044B \\u043E\\u0440\\u044B\\u043D\\u0434\\u0430\\u0443\\u0493\\u0430 \\u0440\\u04B1\\u049B\\u0441\\u0430\\u0442\\u044B\\u04A3\\u044B\\u0437 \\u0436\\u043E\\u049B \\u043D\\u0435\\u043C\\u0435\\u0441\\u0435 \\u049B\\u04B1\\u0440\\u044B\\u043B\\u0493\\u044B \\u049B\\u043E\\u043B\\u0434\\u0430\\u043D\\u0431\\u0430\\u0493\\u0430 \\u049B\\u043E\\u043B\\u0434\\u0430\\u0443 \\u043A\\u04E9\\u0440\\u0441\\u0435\\u0442\\u043F\\u0435\\u0439\\u0434\\u0456.\n#XTIT\nACTUAL=\\u041D\\u0430\\u049B\\u0442\\u044B\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0421\\u0430\\u043D\\u0434\\u044B\\u049B \\u043C\\u0438\\u043A\\u0440\\u043E \\u0434\\u0438\\u0430\\u0433\\u0440\\u0430\\u043C\\u043C\\u0430\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ko.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\uC0AC\\uC6A9 \\uAC00\\uB2A5\\uD55C \\uB370\\uC774\\uD130\\uAC00 \\uC5C6\\uC74C\n#XTIT\nTIME_SEC=\\uCD08\n#XTIT\nTIME_MIN=\\uBD84\n#XTIT\nTIME_HR=\\uC2DC\\uAC04\n#XTIT\nTIME_DAY=\\uC77C\n#XTIT\nTIME_DAYS=\\uC77C\n#XTIT\nTIME_HOUR_FULL=\\uC2DC\\uAC04\n#XTIT\nTIME_HOURS_FULL=\\uC2DC\\uAC04\n#XTIT\nTIME_MINUTE_FULL=\\uBD84\n#XTIT\nTIME_MINUTES_FULL=\\uBD84\n#XTIT\nTIME_SECOND_FULL=\\uCD08\n#XTIT\nTIME_SECONDS_FULL=\\uCD08\n#XSEL\nERROR=\\uC624\\uB958\n#XTIT\nWARNING=\\uACBD\\uACE0\n#XSEL\nGOOD=\\uC88B\\uC74C\n#XSEL\nCRITICAL=\\uC704\\uD5D8\n#XSEL\nNEUTRAL=\\uC911\\uB9BD\n#YMSG\nNAV_ERROR_MESSAGE=\\uC5B4\\uD50C\\uB9AC\\uCF00\\uC774\\uC158\\uC73C\\uB85C \\uC774\\uB3D9\\uD560 \\uC218 \\uC5C6\\uC2B5\\uB2C8\\uB2E4. \\uC5B4\\uD50C\\uB9AC\\uCF00\\uC774\\uC158\\uC744 \\uC2E4\\uD589\\uD560 \\uC218 \\uC788\\uB294 \\uAD8C\\uD55C\\uC774 \\uC5C6\\uAC70\\uB098 \\uC7A5\\uCE58\\uC5D0\\uC11C \\uC5B4\\uD50C\\uB9AC\\uCF00\\uC774\\uC158\\uC774 \\uC9C0\\uC6D0\\uB418\\uC9C0 \\uC54A\\uC2B5\\uB2C8\\uB2E4.\n#XTIT\nACTUAL=\\uC2E4\\uC81C\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\uC22B\\uC790 \\uB9C8\\uC774\\uD06C\\uB85C \\uCC28\\uD2B8\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_lt.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Prieinam\\u0173 duomen\\u0173 n\\u0117ra\n#XTIT\nTIME_SEC=sekund\\u0117s\n#XTIT\nTIME_MIN=minut\\u0117s\n#XTIT\nTIME_HR=valandos\n#XTIT\nTIME_DAY=diena\n#XTIT\nTIME_DAYS=dienos\n#XTIT\nTIME_HOUR_FULL=valanda\n#XTIT\nTIME_HOURS_FULL=valandos\n#XTIT\nTIME_MINUTE_FULL=minut\\u0117\n#XTIT\nTIME_MINUTES_FULL=minut\\u0117s\n#XTIT\nTIME_SECOND_FULL=sekund\\u0117\n#XTIT\nTIME_SECONDS_FULL=sekund\\u0117s\n#XSEL\nERROR=Klaida\n#XTIT\nWARNING=\\u012Esp\\u0117jimas\n#XSEL\nGOOD=Geras\n#XSEL\nCRITICAL=Kritinis\n#XSEL\nNEUTRAL=Neutralus\n#YMSG\nNAV_ERROR_MESSAGE=Per\\u0117jimas \\u012F program\\u0105 negalimas; j\\u016Bs arba neturite leidimo paleisti program\\u0105, arba \\u012Frenginys nepalaiko programos.\n#XTIT\nACTUAL=Faktinis\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Skaitin\\u0117 mikro diagrama\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_lv.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Dati nav pieejami\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=diena\n#XTIT\nTIME_DAYS=dienas\n#XTIT\nTIME_HOUR_FULL=stunda\n#XTIT\nTIME_HOURS_FULL=stundas\n#XTIT\nTIME_MINUTE_FULL=min\\u016Bte\n#XTIT\nTIME_MINUTES_FULL=min\\u016Btes\n#XTIT\nTIME_SECOND_FULL=sekunde\n#XTIT\nTIME_SECONDS_FULL=sekundes\n#XSEL\nERROR=K\\u013C\\u016Bda\n#XTIT\nWARNING=Br\\u012Bdin\\u0101jums\n#XSEL\nGOOD=Labi\n#XSEL\nCRITICAL=Kritisks\n#XSEL\nNEUTRAL=Neitr\\u0101li\n#YMSG\nNAV_ERROR_MESSAGE=Navig\\u0101cija uz lietojumprogrammu nav iesp\\u0113jama; vai nu jums nav at\\u013Caujas izpild\\u012Bt lietojumprogrammu, vai ar\\u012B ier\\u012Bce neatbalsta lietojumprogrammu.\n#XTIT\nACTUAL=Faktisks\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Skaitliska mikrodiagramma\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ms.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Tiada data tersedia\n#XTIT\nTIME_SEC=saat\n#XTIT\nTIME_MIN=minit\n#XTIT\nTIME_HR=jam\n#XTIT\nTIME_DAY=hari\n#XTIT\nTIME_DAYS=hari\n#XTIT\nTIME_HOUR_FULL=jam\n#XTIT\nTIME_HOURS_FULL=jam\n#XTIT\nTIME_MINUTE_FULL=minit\n#XTIT\nTIME_MINUTES_FULL=minit\n#XTIT\nTIME_SECOND_FULL=saat\n#XTIT\nTIME_SECONDS_FULL=saat\n#XSEL\nERROR=Ralat\n#XTIT\nWARNING=Amaran\n#XSEL\nGOOD=Baik\n#XSEL\nCRITICAL=Kritikal\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=Navigasi untuk aplikasi tidak mungkin; sama ada anda tiada kebenaran untuk laksanakan aplikasi atau peranti tidak mempunyai sokongan aplikasi.\n#XTIT\nACTUAL=Sebenar\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Carta Mikro Berangka\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_nl.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Geen gegevens beschikbaar\n#XTIT\nTIME_SEC=sec\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=uur\n#XTIT\nTIME_DAY=dag\n#XTIT\nTIME_DAYS=dagen\n#XTIT\nTIME_HOUR_FULL=uur\n#XTIT\nTIME_HOURS_FULL=uur\n#XTIT\nTIME_MINUTE_FULL=minuut\n#XTIT\nTIME_MINUTES_FULL=minuten\n#XTIT\nTIME_SECOND_FULL=seconde\n#XTIT\nTIME_SECONDS_FULL=seconden\n#XSEL\nERROR=Fout\n#XTIT\nWARNING=Waarschuwing\n#XSEL\nGOOD=Goed\n#XSEL\nCRITICAL=Kritiek\n#XSEL\nNEUTRAL=Neutraal\n#YMSG\nNAV_ERROR_MESSAGE=Navigatie naar applicatie is niet mogelijk; u hebt geen toestemming om de applicatie uit te voeren of het apparaat ondersteunt de applicatie niet.\n#XTIT\nACTUAL=Werkelijk\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeriek microdiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_no.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Ingen tilgjengelige data\n#XTIT\nTIME_SEC=sek.\n#XTIT\nTIME_MIN=min.\n#XTIT\nTIME_HR=t\n#XTIT\nTIME_DAY=dag\n#XTIT\nTIME_DAYS=dager\n#XTIT\nTIME_HOUR_FULL=time\n#XTIT\nTIME_HOURS_FULL=timer\n#XTIT\nTIME_MINUTE_FULL=minutt\n#XTIT\nTIME_MINUTES_FULL=minutter\n#XTIT\nTIME_SECOND_FULL=sekund\n#XTIT\nTIME_SECONDS_FULL=sekunder\n#XSEL\nERROR=Feil\n#XTIT\nWARNING=Advarsel\n#XSEL\nGOOD=Bra\n#XSEL\nCRITICAL=Kritisk\n#XSEL\nNEUTRAL=N\\u00F8ytral\n#YMSG\nNAV_ERROR_MESSAGE=Det er ikke mulig \\u00E5 navigere til applikasjonen. Enten har du ikke tillatelse til \\u00E5 utf\\u00F8re applikasjonen, eller s\\u00E5 st\\u00F8tter ikke enheten applikasjonen.\n#XTIT\nACTUAL=Faktisk\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerisk mikrodiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_pl.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Brak danych\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min.\n#XTIT\nTIME_HR=godz.\n#XTIT\nTIME_DAY=dzie\\u0144\n#XTIT\nTIME_DAYS=dni\n#XTIT\nTIME_HOUR_FULL=godzina\n#XTIT\nTIME_HOURS_FULL=Godziny\n#XTIT\nTIME_MINUTE_FULL=minuta\n#XTIT\nTIME_MINUTES_FULL=minuty\n#XTIT\nTIME_SECOND_FULL=sekunda\n#XTIT\nTIME_SECONDS_FULL=sekundy\n#XSEL\nERROR=B\\u0142\\u0105d\n#XTIT\nWARNING=Ostrze\\u017Cenie\n#XSEL\nGOOD=Dobre\n#XSEL\nCRITICAL=Krytyczne\n#XSEL\nNEUTRAL=Neutralne\n#YMSG\nNAV_ERROR_MESSAGE=Nawigacja do aplikacji nie jest mo\\u017Cliwa; nie masz uprawnie\\u0144 do uruchomienia aplikacji lub urz\\u0105dzenie jej nie obs\\u0142uguje.\n#XTIT\nACTUAL=Rzeczywiste\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Mikro wykres numeryczny\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_pt.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Dados n\\u00E3o dispon\\u00EDveis\n#XTIT\nTIME_SEC=seg\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=H\n#XTIT\nTIME_DAY=dia\n#XTIT\nTIME_DAYS=dias\n#XTIT\nTIME_HOUR_FULL=hora\n#XTIT\nTIME_HOURS_FULL=horas\n#XTIT\nTIME_MINUTE_FULL=minuto\n#XTIT\nTIME_MINUTES_FULL=minutos\n#XTIT\nTIME_SECOND_FULL=segundo\n#XTIT\nTIME_SECONDS_FULL=segundos\n#XSEL\nERROR=Erro\n#XTIT\nWARNING=Advert\\u00EAncia\n#XSEL\nGOOD=Bom\n#XSEL\nCRITICAL=Cr\\u00EDtico\n#XSEL\nNEUTRAL=Neutro\n#YMSG\nNAV_ERROR_MESSAGE=A navega\\u00E7\\u00E3o para a aplica\\u00E7\\u00E3o n\\u00E3o \\u00E9 poss\\u00EDvel; voc\\u00EA n\\u00E3o tem autoriza\\u00E7\\u00E3o para executar a aplica\\u00E7\\u00E3o ou o dispositivo n\\u00E3o suporta a aplica\\u00E7\\u00E3o.\n#XTIT\nACTUAL=Real\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Microgr\\u00E1fico num\\u00E9rico\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ro.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=F\\u0103r\\u0103 date disponibile\n#XTIT\nTIME_SEC=sec\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=zi\n#XTIT\nTIME_DAYS=zile\n#XTIT\nTIME_HOUR_FULL=or\\u0103\n#XTIT\nTIME_HOURS_FULL=ore\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=minute\n#XTIT\nTIME_SECOND_FULL=secund\\u0103\n#XTIT\nTIME_SECONDS_FULL=secunde\n#XSEL\nERROR=Eroare\n#XTIT\nWARNING=Avertizare\n#XSEL\nGOOD=Bine\n#XSEL\nCRITICAL=Critic\n#XSEL\nNEUTRAL=Neutru\n#YMSG\nNAV_ERROR_MESSAGE=Navigarea la aplica\\u021Bie nu este posibil\\u0103; nu ave\\u021Bi permisiune pt. a executa aplica\\u021Bia sau dispozitivul nu suport\\u0103 aplica\\u021Bia.\n#XTIT\nACTUAL=Efectiv\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Microdiagram\\u0103 numeric\\u0103\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_ru.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u041D\\u0435\\u0442 \\u0434\\u043E\\u0441\\u0442\\u0443\\u043F\\u043D\\u044B\\u0445 \\u0434\\u0430\\u043D\\u043D\\u044B\\u0445\n#XTIT\nTIME_SEC=\\u0441\\u0435\\u043A.\n#XTIT\nTIME_MIN=\\u043C\\u0438\\u043D.\n#XTIT\nTIME_HR=\\u0447.\n#XTIT\nTIME_DAY=\\u0434\\u0435\\u043D\\u044C\n#XTIT\nTIME_DAYS=\\u0434\\u043D.\n#XTIT\nTIME_HOUR_FULL=\\u0447\\u0430\\u0441\n#XTIT\nTIME_HOURS_FULL=\\u0447.\n#XTIT\nTIME_MINUTE_FULL=\\u043C\\u0438\\u043D\\u0443\\u0442\\u0430\n#XTIT\nTIME_MINUTES_FULL=\\u043C\\u0438\\u043D.\n#XTIT\nTIME_SECOND_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\\u0430\n#XTIT\nTIME_SECONDS_FULL=\\u0441\\u0435\\u043A.\n#XSEL\nERROR=\\u041E\\u0448\\u0438\\u0431\\u043A\\u0430\n#XTIT\nWARNING=\\u041F\\u0440\\u0435\\u0434\\u0443\\u043F\\u0440\\u0435\\u0436\\u0434\\u0435\\u043D\\u0438\\u0435\n#XSEL\nGOOD=\\u0425\\u043E\\u0440\\u043E\\u0448\\u043E\n#XSEL\nCRITICAL=\\u041A\\u0440\\u0438\\u0442\\u0438\\u0447\\u043D\\u043E\n#XSEL\nNEUTRAL=\\u041D\\u0435\\u0439\\u0442\\u0440\\u0430\\u043B\\u044C\\u043D\\u043E\n#YMSG\nNAV_ERROR_MESSAGE=\\u041F\\u0435\\u0440\\u0435\\u0445\\u043E\\u0434 \\u043A \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u044E \\u043D\\u0435\\u0432\\u043E\\u0437\\u043C\\u043E\\u0436\\u0435\\u043D; \\u043B\\u0438\\u0431\\u043E \\u0443 \\u0432\\u0430\\u0441 \\u043D\\u0435\\u0442 \\u043F\\u043E\\u043B\\u043D\\u043E\\u043C\\u043E\\u0447\\u0438\\u0439 \\u043D\\u0430 \\u044D\\u0442\\u043E \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u0435, \\u043B\\u0438\\u0431\\u043E \\u0443\\u0441\\u0442\\u0440\\u043E\\u0439\\u0441\\u0442\\u0432\\u043E \\u043D\\u0435 \\u043F\\u043E\\u0434\\u0434\\u0435\\u0440\\u0436\\u0438\\u0432\\u0430\\u0435\\u0442 \\u044D\\u0442\\u043E \\u043F\\u0440\\u0438\\u043B\\u043E\\u0436\\u0435\\u043D\\u0438\\u0435.\n#XTIT\nACTUAL=\\u0424\\u0430\\u043A\\u0442\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0427\\u0438\\u0441\\u043B\\u043E\\u0432\\u0430\\u044F \\u043C\\u0438\\u043A\\u0440\\u043E\\u0434\\u0438\\u0430\\u0433\\u0440\\u0430\\u043C\\u043C\\u0430\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_sh.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Podaci nisu dostupni\n#XTIT\nTIME_SEC=sek.\n#XTIT\nTIME_MIN=min.\n#XTIT\nTIME_HR=sat\n#XTIT\nTIME_DAY=dan\n#XTIT\nTIME_DAYS=dani\n#XTIT\nTIME_HOUR_FULL=sat\n#XTIT\nTIME_HOURS_FULL=sati\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=Minuti\n#XTIT\nTIME_SECOND_FULL=sekunda\n#XTIT\nTIME_SECONDS_FULL=Sekunde\n#XSEL\nERROR=Gre\\u0161ka\n#XTIT\nWARNING=Upozorenje\n#XSEL\nGOOD=Dobro\n#XSEL\nCRITICAL=Kriti\\u010Dno\n#XSEL\nNEUTRAL=Neutralno\n#YMSG\nNAV_ERROR_MESSAGE=Usmeravanje ka aplikaciji nije mogu\\u0107e; ili nemate ovla\\u0161\\u0107enje da izvr\\u0161ite aplikaciju ili ure\\u0111aj ne podr\\u017Eava aplikaciju.\n#XTIT\nACTUAL=Stvarno\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeri\\u010Dki mikro dijagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_sk.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=D\\u00E1ta nie s\\u00FA k dispoz\\u00EDcii\n#XTIT\nTIME_SEC=s\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=de\\u0148\n#XTIT\nTIME_DAYS=dni\n#XTIT\nTIME_HOUR_FULL=hodina\n#XTIT\nTIME_HOURS_FULL=hodiny\n#XTIT\nTIME_MINUTE_FULL=min\\u00FAta\n#XTIT\nTIME_MINUTES_FULL=min\\u00FAty\n#XTIT\nTIME_SECOND_FULL=sekunda\n#XTIT\nTIME_SECONDS_FULL=sekundy\n#XSEL\nERROR=Chyba\n#XTIT\nWARNING=Upozornenie\n#XSEL\nGOOD=Dobr\\u00E9\n#XSEL\nCRITICAL=Kritick\\u00E9\n#XSEL\nNEUTRAL=Neutr\\u00E1lne\n#YMSG\nNAV_ERROR_MESSAGE=Navig\\u00E1cia na aplik\\u00E1ciu nie je mo\\u017En\\u00E1; bu\\u010F nem\\u00E1te opr\\u00E1vnenie na vykonanie aplik\\u00E1cie alebo zariadenie aplik\\u00E1ciu nepodporuje.\n#XTIT\nACTUAL=Skuto\\u010Dn\\u00E9\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numerick\\u00FD mikrograf\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_sl.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Ni razpolo\\u017Eljivih podatkov\n#XTIT\nTIME_SEC=Sekunde\n#XTIT\nTIME_MIN=Minute\n#XTIT\nTIME_HR=Ure\n#XTIT\nTIME_DAY=Dan\n#XTIT\nTIME_DAYS=Dnevi\n#XTIT\nTIME_HOUR_FULL=Ura\n#XTIT\nTIME_HOURS_FULL=Ure\n#XTIT\nTIME_MINUTE_FULL=Minuta\n#XTIT\nTIME_MINUTES_FULL=Minute\n#XTIT\nTIME_SECOND_FULL=Sekunda\n#XTIT\nTIME_SECONDS_FULL=Sekunde\n#XSEL\nERROR=Napaka\n#XTIT\nWARNING=Opozorilo\n#XSEL\nGOOD=Dobro\n#XSEL\nCRITICAL=Kriti\\u010Dno\n#XSEL\nNEUTRAL=Nevtralno\n#YMSG\nNAV_ERROR_MESSAGE=Navigacija do aplikacije ni mogo\\u010Da; nimate dovoljenja za uporabo aplikacije ali pa naprava ne podpira aplikacije.\n#XTIT\nACTUAL=Dejansko\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeri\\u010Dni mikrodiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_sv.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Inga data tillg\\u00E4ngliga\n#XTIT\nTIME_SEC=sek\n#XTIT\nTIME_MIN=min\n#XTIT\nTIME_HR=h\n#XTIT\nTIME_DAY=dag\n#XTIT\nTIME_DAYS=dagar\n#XTIT\nTIME_HOUR_FULL=timme\n#XTIT\nTIME_HOURS_FULL=timmar\n#XTIT\nTIME_MINUTE_FULL=minut\n#XTIT\nTIME_MINUTES_FULL=minuter\n#XTIT\nTIME_SECOND_FULL=sekund\n#XTIT\nTIME_SECONDS_FULL=sekunder\n#XSEL\nERROR=Fel\n#XTIT\nWARNING=Varning\n#XSEL\nGOOD=Bra\n#XSEL\nCRITICAL=Kritisk\n#XSEL\nNEUTRAL=Neutral\n#YMSG\nNAV_ERROR_MESSAGE=Navigering till applikation \\u00E4r inte m\\u00F6jlig. Antingen saknas beh\\u00F6righet att utf\\u00F6ra applikationen eller s\\u00E5 st\\u00F6djer inte enheten applikationen.\n#XTIT\nACTUAL=Verklig\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Numeriskt mikrodiagram\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_th.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0E44\\u0E21\\u0E48\\u0E21\\u0E35\\u0E02\\u0E49\\u0E2D\\u0E21\\u0E39\\u0E25\n#XTIT\nTIME_SEC=\\u0E27\\u0E34\\u0E19\\u0E32\\u0E17\\u0E35\n#XTIT\nTIME_MIN=\\u0E19\\u0E32\\u0E17\\u0E35\n#XTIT\nTIME_HR=\\u0E0A\\u0E31\\u0E48\\u0E27\\u0E42\\u0E21\\u0E07\n#XTIT\nTIME_DAY=\\u0E27\\u0E31\\u0E19\n#XTIT\nTIME_DAYS=\\u0E27\\u0E31\\u0E19\n#XTIT\nTIME_HOUR_FULL=\\u0E0A\\u0E31\\u0E48\\u0E27\\u0E42\\u0E21\\u0E07\n#XTIT\nTIME_HOURS_FULL=\\u0E0A\\u0E31\\u0E48\\u0E27\\u0E42\\u0E21\\u0E07\n#XTIT\nTIME_MINUTE_FULL=\\u0E19\\u0E32\\u0E17\\u0E35\n#XTIT\nTIME_MINUTES_FULL=\\u0E19\\u0E32\\u0E17\\u0E35\n#XTIT\nTIME_SECOND_FULL=\\u0E27\\u0E34\\u0E19\\u0E32\\u0E17\\u0E35\n#XTIT\nTIME_SECONDS_FULL=\\u0E27\\u0E34\\u0E19\\u0E32\\u0E17\\u0E35\n#XSEL\nERROR=\\u0E02\\u0E49\\u0E2D\\u0E1C\\u0E34\\u0E14\\u0E1E\\u0E25\\u0E32\\u0E14\n#XTIT\nWARNING=\\u0E04\\u0E33\\u0E40\\u0E15\\u0E37\\u0E2D\\u0E19\n#XSEL\nGOOD=\\u0E14\\u0E35\n#XSEL\nCRITICAL=\\u0E27\\u0E34\\u0E01\\u0E24\\u0E15\n#XSEL\nNEUTRAL=\\u0E1B\\u0E32\\u0E19\\u0E01\\u0E25\\u0E32\\u0E07\n#YMSG\nNAV_ERROR_MESSAGE=\\u0E44\\u0E21\\u0E48\\u0E2A\\u0E32\\u0E21\\u0E32\\u0E23\\u0E16\\u0E40\\u0E19\\u0E27\\u0E34\\u0E40\\u0E01\\u0E15\\u0E44\\u0E1B\\u0E22\\u0E31\\u0E07\\u0E41\\u0E2D\\u0E1E\\u0E1E\\u0E25\\u0E34\\u0E40\\u0E04\\u0E0A\\u0E31\\u0E19; \\u0E04\\u0E38\\u0E13\\u0E44\\u0E21\\u0E48\\u0E21\\u0E35\\u0E2A\\u0E34\\u0E17\\u0E18\\u0E34\\u0E14\\u0E33\\u0E40\\u0E19\\u0E34\\u0E19\\u0E01\\u0E32\\u0E23\\u0E41\\u0E2D\\u0E1E\\u0E1E\\u0E25\\u0E34\\u0E40\\u0E04\\u0E0A\\u0E31\\u0E19\\u0E2B\\u0E23\\u0E37\\u0E2D\\u0E2D\\u0E38\\u0E1B\\u0E01\\u0E23\\u0E13\\u0E4C\\u0E44\\u0E21\\u0E48\\u0E23\\u0E2D\\u0E07\\u0E23\\u0E31\\u0E1A\\u0E41\\u0E2D\\u0E1E\\u0E1E\\u0E25\\u0E34\\u0E40\\u0E04\\u0E0A\\u0E31\\u0E19\n#XTIT\nACTUAL=\\u0E15\\u0E32\\u0E21\\u0E08\\u0E23\\u0E34\\u0E07\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0E41\\u0E1C\\u0E19\\u0E20\\u0E39\\u0E34\\u0E21\\u0E34\\u0E44\\u0E21\\u0E42\\u0E04\\u0E23\\u0E41\\u0E1A\\u0E1A\\u0E15\\u0E31\\u0E27\\u0E40\\u0E25\\u0E02\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_tr.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Veri mevcut de\\u011Fil\n#XTIT\nTIME_SEC=Sn.\n#XTIT\nTIME_MIN=dak\n#XTIT\nTIME_HR=sa\n#XTIT\nTIME_DAY=g\\u00FCn\n#XTIT\nTIME_DAYS=g\\u00FCn\n#XTIT\nTIME_HOUR_FULL=saat\n#XTIT\nTIME_HOURS_FULL=saat\n#XTIT\nTIME_MINUTE_FULL=dakika\n#XTIT\nTIME_MINUTES_FULL=dakika\n#XTIT\nTIME_SECOND_FULL=saniye\n#XTIT\nTIME_SECONDS_FULL=saniye\n#XSEL\nERROR=Hata\n#XTIT\nWARNING=Uyar\\u0131\n#XSEL\nGOOD=\\u0130yi\n#XSEL\nCRITICAL=Kritik\n#XSEL\nNEUTRAL=N\\u00F6tr\n#YMSG\nNAV_ERROR_MESSAGE=Uygulamaya dola\\u015Fma olanakl\\u0131 de\\u011Fil; uygulamay\\u0131 y\\u00FCr\\u00FCtme izniniz yok veya ayg\\u0131t uygulamay\\u0131 desteklemiyor.\n#XTIT\nACTUAL=Fiili\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Say\\u0131sal mikro grafik\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_uk.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u0414\\u0430\\u043D\\u0456 \\u043D\\u0435\\u0434\\u043E\\u0441\\u0442\\u0443\\u043F\\u043D\\u0456\n#XTIT\nTIME_SEC=\\u0441\\u0435\\u043A.\n#XTIT\nTIME_MIN=\\u0445\\u0432.\n#XTIT\nTIME_HR=\\u0433\\u043E\\u0434.\n#XTIT\nTIME_DAY=\\u0434\\u0435\\u043D\\u044C\n#XTIT\nTIME_DAYS=\\u0434\\u043D\\u0456\\u0432\n#XTIT\nTIME_HOUR_FULL=\\u0433\\u043E\\u0434\\u0438\\u043D\\u0430\n#XTIT\nTIME_HOURS_FULL=\\u0433\\u043E\\u0434\\u0438\\u043D\n#XTIT\nTIME_MINUTE_FULL=\\u0445\\u0432\\u0438\\u043B\\u0438\\u043D\\u0430\n#XTIT\nTIME_MINUTES_FULL=\\u0445\\u0432\\u0438\\u043B\\u0438\\u043D\\u0438\n#XTIT\nTIME_SECOND_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\\u0430\n#XTIT\nTIME_SECONDS_FULL=\\u0441\\u0435\\u043A\\u0443\\u043D\\u0434\\u0438\n#XSEL\nERROR=\\u041F\\u043E\\u043C\\u0438\\u043B\\u043A\\u0430\n#XTIT\nWARNING=\\u0417\\u0430\\u0441\\u0442\\u0435\\u0440\\u0435\\u0436\\u0435\\u043D\\u043D\\u044F\n#XSEL\nGOOD=\\u0414\\u043E\\u0431\\u0440\\u0435\n#XSEL\nCRITICAL=\\u041A\\u0440\\u0438\\u0442\\u0438\\u0447\\u043D\\u043E\n#XSEL\nNEUTRAL=\\u041D\\u0435\\u0439\\u0442\\u0440\\u0430\\u043B\\u044C\\u043D\\u043E\n#YMSG\nNAV_ERROR_MESSAGE=\\u041D\\u0430\\u0432\\u0456\\u0433\\u0443\\u0432\\u0430\\u0442\\u0438 \\u0434\\u043E \\u0437\\u0430\\u0441\\u0442\\u043E\\u0441\\u0443\\u043D\\u043A\\u0443 \\u043D\\u0435\\u043C\\u043E\\u0436\\u043B\\u0438\\u0432\\u043E; \\u0432\\u0438 \\u0430\\u0431\\u043E \\u043D\\u0435 \\u043C\\u0430\\u0454\\u0442\\u0435 \\u0434\\u043E\\u0437\\u0432\\u043E\\u043B\\u0443 \\u043D\\u0430 \\u0432\\u0438\\u043A\\u043E\\u043D\\u0430\\u043D\\u043D\\u044F \\u0437\\u0430\\u0441\\u0442\\u043E\\u0441\\u0443\\u043D\\u043A\\u0443 \\u0430\\u0431\\u043E \\u043F\\u0440\\u0438\\u0441\\u0442\\u0440\\u0456\\u0439 \\u043D\\u0435 \\u043F\\u0456\\u0434\\u0442\\u0440\\u0438\\u043C\\u0443\\u0454 \\u0437\\u0430\\u0441\\u0442\\u043E\\u0441\\u0443\\u043D\\u043E\\u043A.\n#XTIT\nACTUAL=\\u0424\\u0430\\u043A\\u0442\\u0438\\u0447\\u043D\\u043E\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u0427\\u0438\\u0441\\u043B\\u043E\\u0432\\u0430 \\u043C\\u0456\\u043A\\u0440\\u043E\\u0434\\u0456\\u0430\\u0433\\u0440\\u0430\\u043C\\u0430\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_vi.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=Kh\\u00F4ng co\\u0301 s\\u0103\\u0303n d\\u01B0\\u0303 li\\u00EA\\u0323u\n#XTIT\nTIME_SEC=gi\\u00E2y\n#XTIT\nTIME_MIN=phu\\u0301t\n#XTIT\nTIME_HR=gi\\u01A1\\u0300\n#XTIT\nTIME_DAY=nga\\u0300y\n#XTIT\nTIME_DAYS=nga\\u0300y\n#XTIT\nTIME_HOUR_FULL=gi\\u01A1\\u0300\n#XTIT\nTIME_HOURS_FULL=gi\\u01A1\\u0300\n#XTIT\nTIME_MINUTE_FULL=phu\\u0301t\n#XTIT\nTIME_MINUTES_FULL=phu\\u0301t\n#XTIT\nTIME_SECOND_FULL=gi\\u00E2y\n#XTIT\nTIME_SECONDS_FULL=gi\\u00E2y\n#XSEL\nERROR=L\\u1ED7i\n#XTIT\nWARNING=Ca\\u0309nh ba\\u0301o\n#XSEL\nGOOD=T\\u00F4\\u0301t\n#XSEL\nCRITICAL=Nghi\\u00EAm tro\\u0323ng\n#XSEL\nNEUTRAL=Trung l\\u00E2\\u0323p\n#YMSG\nNAV_ERROR_MESSAGE=Kh\\u00F4ng th\\u00EA\\u0309 \\u0111i\\u00EA\\u0300u h\\u01B0\\u01A1\\u0301ng \\u0111\\u00EA\\u0301n \\u01B0\\u0301ng du\\u0323ng; ba\\u0323n kh\\u00F4ng co\\u0301 quy\\u00EA\\u0300n th\\u01B0\\u0323c thi \\u01B0\\u0301ng du\\u0323ng ho\\u0103\\u0323c thi\\u00EA\\u0301t bi\\u0323 kh\\u00F4ng h\\u00F4\\u0303 tr\\u01A1\\u0323 \\u01B0\\u0301ng du\\u0323ng.\n#XTIT\nACTUAL=Th\\u01B0\\u0323c t\\u00EA\\u0301\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=Bi\\u00EA\\u0309u \\u0111\\u00F4\\u0300 nho\\u0309 b\\u0103\\u0300ng s\\u00F4\\u0301\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_zh_CN.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u65E0\\u53EF\\u7528\\u6570\\u636E\n#XTIT\nTIME_SEC=\\u79D2\n#XTIT\nTIME_MIN=\\u5206\\u949F\n#XTIT\nTIME_HR=\\u5C0F\\u65F6\n#XTIT\nTIME_DAY=\\u5929\n#XTIT\nTIME_DAYS=\\u5929\n#XTIT\nTIME_HOUR_FULL=\\u5C0F\\u65F6\n#XTIT\nTIME_HOURS_FULL=\\u5C0F\\u65F6\n#XTIT\nTIME_MINUTE_FULL=\\u5206\\u949F\n#XTIT\nTIME_MINUTES_FULL=\\u5206\\u949F\n#XTIT\nTIME_SECOND_FULL=\\u79D2\n#XTIT\nTIME_SECONDS_FULL=\\u79D2\n#XSEL\nERROR=\\u9519\\u8BEF\n#XTIT\nWARNING=\\u8B66\\u544A\n#XSEL\nGOOD=\\u826F\\u597D\n#XSEL\nCRITICAL=\\u4E25\\u91CD\n#XSEL\nNEUTRAL=\\u4E2D\\u6027\n#YMSG\nNAV_ERROR_MESSAGE=\\u65E0\\u6CD5\\u5BFC\\u822A\\u5230\\u5E94\\u7528\\u7A0B\\u5E8F\\uFF1B\\u60A8\\u53EF\\u80FD\\u65E0\\u6743\\u6267\\u884C\\u5E94\\u7528\\u7A0B\\u5E8F\\uFF0C\\u6216\\u8BBE\\u5907\\u4E0D\\u652F\\u6301\\u6B64\\u5E94\\u7528\\u7A0B\\u5E8F\\u3002\n#XTIT\nACTUAL=\\u5B9E\\u9645\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u6570\\u5B57\\u5FAE\\u578B\\u56FE\n',
	"ssuite/smartbusiness/tiles/numeric/i18n/i18n_zh_TW.properties":'\n\n#XFLD: no data text on the tiles\nNO_DATA_AVAILABLE_KEY=\\u6C92\\u6709\\u8CC7\\u6599\n#XTIT\nTIME_SEC=\\u79D2\n#XTIT\nTIME_MIN=\\u5206\\u9418\n#XTIT\nTIME_HR=\\u5C0F\\u6642\n#XTIT\nTIME_DAY=\\u5929\n#XTIT\nTIME_DAYS=\\u5929\n#XTIT\nTIME_HOUR_FULL=\\u5C0F\\u6642\n#XTIT\nTIME_HOURS_FULL=\\u5C0F\\u6642\n#XTIT\nTIME_MINUTE_FULL=\\u5206\\u9418\n#XTIT\nTIME_MINUTES_FULL=\\u5206\\u9418\n#XTIT\nTIME_SECOND_FULL=\\u79D2\n#XTIT\nTIME_SECONDS_FULL=\\u79D2\n#XSEL\nERROR=\\u932F\\u8AA4\n#XTIT\nWARNING=\\u8B66\\u544A\n#XSEL\nGOOD=\\u826F\\u597D\n#XSEL\nCRITICAL=\\u56B4\\u91CD\n#XSEL\nNEUTRAL=\\u4E2D\\u6027\n#YMSG\nNAV_ERROR_MESSAGE=\\u7121\\u6CD5\\u700F\\u89BD\\u81F3\\u61C9\\u7528\\u7A0B\\u5F0F\\uFF1B\\u60A8\\u6C92\\u6709\\u6B0A\\u9650\\u57F7\\u884C\\u61C9\\u7528\\u7A0B\\u5F0F\\uFF0C\\u6216\\u88DD\\u7F6E\\u672A\\u652F\\u63F4\\u61C9\\u7528\\u7A0B\\u5F0F\\u3002\n#XTIT\nACTUAL=\\u5BE6\\u969B\n#XTIT\nFULLSCREEN_TITLE=Smart Business\n#XTIT\nNUMERIC_MICRO_CHART=\\u6578\\u503C\\u5FAE\\u89C0\\u5716\\u8868\n',
	"ssuite/smartbusiness/tiles/numeric/manifest.json":'{\n\t"_version": "1.3.0",\n\t"sap.app": {\n        "id": "ssuite.smartbusiness.tiles.numeric",\n        "type": "component",\n        \n        "embeddedBy": "../../../../",\n        "title": "{{FULLSCREEN_TITLE}}",\n        "ach": "CA-GTF-SB-S4H-RT",\n        "dataSources": {\n            "SSB": {\n                "uri": "/sap/opu/odata/SSB/SMART_BUSINESS_RUNTIME_SRV"\n            },\n            "ODATA_METADATA": {\n                "uri": "/sap/opu/odata/iwfnd/catalogservice;v=2"\n            }\n        },\n        "resources": "resources.json"\n    },\n    "sap.ui": {\n        "technology": "UI5",\n        "deviceTypes": {\n            "desktop": true,\n            "tablet": true,\n            "phone": false\n        },\n        "supportedThemes": [\n            "sap_hcb",\n            "sap_bluecrystal"\n        ]\n    },\n    "sap.ui5": {\n\t\t"models": {\n\t\t\t"i18n": {\n\t\t\t\t"type": "sap.ui.model.resource.ResourceModel",\n\t\t\t\t"uri": "i18n/i18n.properties"\n\t\t\t}\n\t\t},\n\t\t"dependencies": {\n\t\t\t"libs": {\n\t\t\t\t"sap.m": {}\n\t\t\t}\n\t\t},\n\t\t"contentDensities": {\n\t\t\t"compact": true,\n\t\t\t"cozy": true\n\t\t},\n\t\t"componentName": "ssuite.smartbusiness.tiles.numeric",\n\t\t"resources": {\n\t\t\t"css": [{\n\t\t\t\t"uri": "../../../../css/style.css"\n\t\t\t}]\n\t\t}\n\t},\n\t"sap.flp": {\n\t\t"tileSize": "1x1",\n\t\t"type": "tile",\n\t\t"config": {}\n\t}\n}'
},"ssuite/smartbusiness/tiles/numeric/Component-preload"
);
