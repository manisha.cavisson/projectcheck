/*
 * Copyright (C) 2009-2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define("cus/cpm/lib/projectengagement/util/ExtensionProvider",["jquery.sap.global"],function(q){var E=function(){};E.prototype.getControllerExtensions=function(c,C,a){var b=["cus.cpm.projengagement.maint","cus.cpm.internalproject.maint","cus.cpm.lib.projectengagement"];var A=false;for(var i=0;i<b.length;i++){var s=b[i];if(c.indexOf(s)!==-1){A=true;break;}}var e=["cus.cpm.projengagement.maint.controller.BillingPlan"];if(a&&e.indexOf(c)===-1&&A){return new Promise(function(r,R){r([{onInit:function(){var d=this.getResourceBundle();this.MessageProcessor.setBundle(d);}}]);});}};return E;},true);
