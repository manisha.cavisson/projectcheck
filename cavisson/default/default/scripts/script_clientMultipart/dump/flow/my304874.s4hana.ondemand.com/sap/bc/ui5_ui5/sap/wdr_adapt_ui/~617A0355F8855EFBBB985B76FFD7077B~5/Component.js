sap.ui.require([
    "sap/ui/core/Component",
    "sap/m/Button",
    "sap/m/MessageToast"
], function (Component, Button, MessageToast) {
    "use strict";
    var sComponentName = "sap.wdr.UIPluginAdaptUI";
    return Component.extend(sComponentName + ".Component", {
        metadata: {
            "manifest": "json"
        },
        init: function () {
            var rendererPromise = this._getRenderer();
            // i18
            this.i18n = this.getModel("i18n").getResourceBundle();
            rendererPromise.i18n = this.i18n;
            // buffer for post messages
            sap.wdr.UIPluginAdaptUI.Component.aPostMessage = [];
            // container
            var oContainer = this._getContainer();
            var oAppLifeCycleService = oContainer.getService("AppLifeCycle");
            oAppLifeCycleService.attachAppLoaded(this._onAppLoaded, this);
            rendererPromise.then(function (oRenderer) {
                var _onAdapt = function (oEvent) {
                    var bNavigationDone = sap.wdr.UIPluginAdaptUI.Component.prototype._navigateToCustomizingMode();
                    if (!bNavigationDone) {
                        MessageToast.show("Error: There is no available navigation target");
                    }
                };
                var oButton = oRenderer.addActionButton("sap.m.Button", {
                    id: "wdrAdaptUI",
                    icon: "sap-icon://wrench",
                    text: this.i18n.getText("adapt_button"),
                    press: _onAdapt
                }, true, false, [sap.ushell.renderers.fiori2.RendererExtensions.LaunchpadState.App]);
                // button must be hidden first
                oButton.setVisible(false);
                // during cold start, we directly call _onAppLoaded
                var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
                if (oCrossAppNavigator && oCrossAppNavigator.isInitialNavigation()) {
                    sap.wdr.UIPluginAdaptUI.Component.prototype._onAppLoaded();
                }
            });
        },

        _isCustomizingMode: function () {
            var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
            if (!oCrossAppNavigator) {
                return false;
            }
            var sHref = oCrossAppNavigator.hrefForExternal();
            if (sHref.length <= 0) {
                return false;
            }
            var aHref = sHref.split("?");
            if (aHref[1] && aHref[1].indexOf("sap-config-mode=A") !== -1) {
                return true;
            } else {
                return false;
            }
        },


        _navigateToCustomizingMode: function () {
            var oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");
            if (!oCrossAppNavigator) {
                return false;
            }
            var sHref = oCrossAppNavigator.hrefForExternal();
            if (sHref.length <= 0) {
                return false;
            }
            var aHref = sHref.split("-");
            var sSemanticObject = aHref[0].substring(1, aHref[0].length);
            var sAction = aHref[1];
            var oTarget = {
                "target": {
                    "semanticObject": sSemanticObject,
                    "action": sAction
                },
                "params": {
                    "sap-config-mode": "A"
                }
            };
            oCrossAppNavigator.toExternal(oTarget);
            return true;
        },
        _getApplConfigId: function () {
            var aFrag = this._getSrc().split("?");
            var sQuery = aFrag[1];
            var aVars = sQuery.split("&");
            var sConfigId = "";
            for (var i = 0; i < aVars.length; i++) {
                var aPair = aVars[i].split("=");
                if (aPair[0] === "sap-wd-configId") {
                    sConfigId = aPair[1];
                    break;
                }
            }
            if (sConfigId === "") {
                sQuery = aFrag[0];
                aVars = sQuery.split("/");
                for (i = 0; i < aVars.length; i++) {
                    if (aVars[i] === "wda" && aVars[i + 1]) {
                        sConfigId = aVars[i + 1];
                        break;
                    }
                    if (aVars[i] === "webdynpro" && aVars[i + 2]) {
                        sConfigId = aVars[i + 2];
                        break;
                    }
                }
            }
            return sConfigId;
        },
        _getSrc: function () {
            var oNavTarget = sap.ushell.Container.getService("NavTargetResolution");
            var oCurrRes = oNavTarget.getCurrentResolution();
            if (oCurrRes) {
                return oCurrRes.url;
            }
        },
        _getFrame: function () {
            var aFrame = document.getElementsByTagName("IFRAME");
            for (var i = 0; i < aFrame.length; i++) {
                if (aFrame[i].src && (aFrame[i].src.indexOf("app/wda/") !== -1 || aFrame[i].src.indexOf("bc/webdynpro/") !== -1)) {
                    return aFrame[i];
                }
            }
        },
        _receivePostMessage: function (oEvent) {
            if (!oEvent || oEvent.origin !== window.location.origin) {
                return;
            }
            if (!oEvent.data) {
                return;
            }
            try {
                var oData = JSON.parse(oEvent.data);
            } catch (oEx) {
                return;
            }
            if (oData && oData.type === "response" && oData.service === "sap.wdr.services.ApplWhiteList.check") {
                if (oData.body && oData.body.IsAllowed) {
                    sap.wdr.UIPluginAdaptUI.Component.prototype._adaptButtonVisibility("wdrAdaptUI", true);
                } else {
                    sap.wdr.UIPluginAdaptUI.Component.prototype._adaptButtonVisibility("wdrAdaptUI", false);
                }
                // remove post message listener
                window.removeEventListener("message", this._receivePostMessage, false);
            } else {
                // send own post messages
                if (oEvent.source) {
                    var aPostMessage = sap.wdr.UIPluginAdaptUI.Component.aPostMessage;
                    if (aPostMessage && aPostMessage.length > 0) {
                        for (var i = 0; i < aPostMessage.length; i++) {
                            oEvent.source.postMessage(JSON.stringify(aPostMessage[i]), window.location.origin);
                        }
                        sap.wdr.UIPluginAdaptUI.Component.aPostMessage = [];
                    }
                }
            }
        },
        _getUUID: function () {
            var d = new Date().getTime();

            function getRandom() {
                var oCrypto = window.crypto || window.msCrypto;
                if (oCrypto) {
                    var aInt = new Uint8Array(1);
                    return 10 / oCrypto.getRandomValues(aInt)[0];
                } else {
                    return Math.random();
                }
            }
            var uuid = 'id-xxxxxxxyxxxxx-0'.replace(/[xy]/g, function (c) {
                var r = (d + getRandom() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },
        _checkWhiteList: function () {
            var sApplConfigId = this._getApplConfigId();
            this._adaptButtonVisibility("wdrAdaptUI", false);
            if (sApplConfigId !== "") {
                window.addEventListener("message", this._receivePostMessage, false);
                // prepare request
                var oMessage = {
                    "type": "request",
                    "service": "sap.wdr.services.ApplWhiteList.check",
                    "request_id": this._getUUID(),
                    "body": {
                        "sApplConfigId": sApplConfigId
                    }
                };
                // buffer post message request
                sap.wdr.UIPluginAdaptUI.Component.aPostMessage.push(oMessage);
            }
        },
        _checkWebdynproApp: function () {
            var oCurrentApplication = this._getCurrentRunningApplication();
            var bIsWda = oCurrentApplication.applicationType === "NWBC" && !oCurrentApplication.homePage;
            return bIsWda;
        },
        _onAppLoaded: function () {
            if (this._checkWebdynproApp() && !this._isCustomizingMode()) {
                this._checkWhiteList();
            } else {
                this._adaptButtonVisibility("wdrAdaptUI", false);
            }
        },
        _getContainer: function () {
            var oContainer = jQuery.sap.getObject("sap.ushell.Container");
            if (!oContainer) {
                throw new Error(
                    "Illegal state: shell container not available; this component must be executed in a unified shell runtime context.");
            }
            return oContainer;
        },
        _getCurrentRunningApplication: function () {
            var oAppLifeCycleService = this._getContainer().getService("AppLifeCycle");
            var oApp = oAppLifeCycleService.getCurrentApplication();
            return oApp;
        },
        _adaptButtonVisibility: function (vControl, bVisible) {
            if (typeof vControl === "string") {
                vControl = sap.ui.getCore().byId(vControl);
            }
            if (!vControl) {
                return;
            }
            vControl.setVisible(bVisible);
        },
        _getRenderer: function () {
            var that = this,
                oDeferred = new jQuery.Deferred(),
                oRenderer;
            that._oShellContainer = jQuery.sap.getObject("sap.ushell.Container");
            if (!that._oShellContainer) {
                oDeferred.reject(
                    "Illegal state: shell container not available; this component must be executed in a unified shell runtime context.");
            } else {
                oRenderer = that._oShellContainer.getRenderer();
                if (oRenderer) {
                    oDeferred.resolve(oRenderer);
                } else {
                    // renderer not initialized yet, listen to rendererCreated event
                    that._onRendererCreated = function (oEvent) {
                        oRenderer = oEvent.getParameter("renderer");
                        if (oRenderer) {
                            oDeferred.resolve(oRenderer);
                        } else {
                            oDeferred.reject("Illegal state: shell renderer not available after recieving 'rendererLoaded' event.");
                        }
                    };
                    that._oShellContainer.attachRendererCreatedEvent(that._onRendererCreated);
                }
            }
            return oDeferred.promise();
        },
    });
});
