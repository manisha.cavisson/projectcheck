/*
 * Copyright (C) 2009-2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
"use strict";sap.ui.define(["cus/cpm/lib/projectengagement/util/BaseHelper"],function(B){"use strict";return B.extend("NavigationHelper",{constructor:function(){var f=sap.ushell&&sap.ushell.Container&&sap.ushell.Container.getService;this.oCrossAppNavigator=f&&f("CrossApplicationNavigation");},navigateToExternalApp:function(s,a,p){if(this.oCrossAppNavigator){this.oCrossAppNavigator.toExternal({target:{semanticObject:s,action:a},params:p});}},getExternalHash:function(s,a,p){var e=this.oCrossAppNavigator&&this.oCrossAppNavigator.hrefForExternal({target:{semanticObject:s,action:a},params:p})||"";return e;}});});
