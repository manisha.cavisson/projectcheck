/*
 * Copyright (C) 2009-2020 SAP SE or an SAP affiliate company. All rights reserved.
 */
sap.ui.define(function(){"use strict";var R={getResourceBundle:function(){return sap.ui.getCore().getLibraryResourceBundle("sap.cus.md.lib.cond.pr.cpm.ratecard");},getText:function(t,v){var i=sap.ui.getCore().getLibraryResourceBundle("sap.cus.md.lib.cond.pr.cpm.ratecard");var r=i.getText(t,v);return r;}};return R;},true);
