/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: Ruchika
    Date of recording: 08/05/2020 11:10:41
    Flow details:
    Build details: 4.3.0 (build# 98)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
