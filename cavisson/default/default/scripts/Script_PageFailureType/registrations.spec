nsl_web_find(TEXT="String Not Found", PAGE=PageFailure_CVFail, FAIL=NOTFOUND, ID="String Not Found", ActionOnFail=CONTINUE);
nsl_decl_var(RepSize_var);
nsl_decl_var(RepSize_Retvar);
nsl_check_reply_size(MODE=NotBetweenMinMax, Value1=100, Value2=110, PAGE=PageFailure_SizeTooBig, Action=Continue, VAR=RepSize_var);
nsl_check_reply_size(MODE=NotBetweenMinMax, Value1=10000, Value2=11000, PAGE=PageFailure_SizeTooSmall, Action=Continue, VAR=RepSize_Retvar);
nsl_static_var(FName:1,LName:2, File=DataFile.use, Refresh=SESSION, Mode=USE_ONCE, EncodeMode=All, OnUseOnceError=ABORTPAGE, UseOnceWithinTest=No);
nsl_unique_range_var(UniqRange, StartRange="5", UserBlockSize="10", Refresh="SESSION", Format="%d", ActionOnRangeExceed="SESSION_FAILURE");
