/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 04/07/2021 07:26:39
    Flow details:
    Build details: 4.6.0 (build# 46)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_check_U20;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
