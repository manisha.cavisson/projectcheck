/*-----------------------------------------------------------------------------
    Name: flow3
    Recorded By: cavisson
    Date of recording: 04/07/2021 07:31:32
    Flow details:
    Build details: 4.6.0 (build# 46)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Script_check_U20;
import pacJnvmApi.NSApi;

public class flow3 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        status = nsApi.ns_start_transaction("index_html_3");
        status = nsApi.ns_web_url ("index_html_3",
            "URL=http://10.10.30.41:8080/tours/index.html",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("index_html_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.727);

        status = nsApi.ns_start_transaction("login_3");
        status = nsApi.ns_web_url ("login_3",
            "URL=http://10.10.30.41:8080/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=&password=&login.x=37&login.y=0&JSFormSubmit=off",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("login_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(4.183);

        status = nsApi.ns_start_transaction("reservation_3");
        status = nsApi.ns_web_url ("reservation_3",
            "URL=http://10.10.30.41:8080/cgi-bin/reservation",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/splash_Findflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("reservation_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(5.514);

        status = nsApi.ns_start_transaction("findflight_7");
        status = nsApi.ns_web_url ("findflight_7",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?depart=Acapulco&departDate=04-08-2021&arrive=Acapulco&returnDate=04-09-2021&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=44&findFlights.y=17",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/continue.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_Searchresults.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_7", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(6.831);

        status = nsApi.ns_start_transaction("findflight_8");
        status = nsApi.ns_web_url ("findflight_8",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C04-08-2021&hidden_outboundFlight_button1=001%7C0%7C04-08-2021&hidden_outboundFlight_button2=002%7C0%7C04-08-2021&hidden_outboundFlight_button3=003%7C0%7C04-08-2021&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=90&reserveFlights.y=8",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/startover.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/purchaseflight.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_creditcard.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_8", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(7.913);

        status = nsApi.ns_start_transaction("findflight_9");
        status = nsApi.ns_web_url ("findflight_9",
            "URL=http://10.10.30.41:8080/cgi-bin/findflight?firstName=Tiger&lastName=Scott&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C04-08-2021&advanceDiscount=&buyFlights.x=96&buyFlights.y=12&.cgifields=saveCC",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/home.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/flights.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/signoff.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/bookanother.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/splash_flightconfirm.gif", END_INLINE
        );

        status = nsApi.ns_end_transaction("findflight_9", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(9.562);

        status = nsApi.ns_start_transaction("welcome_3");
        status = nsApi.ns_web_url ("welcome_3",
            "URL=http://10.10.30.41:8080/cgi-bin/welcome",
            INLINE_URLS,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/sun_swede.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/banner_animated.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/Merc10-dev/images/login.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/vep/images/velocigen.gif", END_INLINE,
                "URL=http://10.10.30.41:8080/tours/images/banner_merctur.jpg", END_INLINE
        );

        status = nsApi.ns_end_transaction("welcome_3", NS_AUTO_STATUS);
        status = nsApi.ns_page_think_time(1.386);

        return status;
    }
}
