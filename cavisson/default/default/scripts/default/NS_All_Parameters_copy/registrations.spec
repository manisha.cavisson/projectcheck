//File Parameter
nsl_static_var(uname:1,upass:2, File=DataFP.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
nsl_static_var(ParamNum:1, File=DataNumFP.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
//Search Parameter
nsl_search_var(sp_all, PAGE=index_html, LB="<title>", RB="</title>", LBMATCH=FIRST, SaveOffset=0, Search=ALL, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(sp_header, PAGE=index_html, LB="Content-", RB=": 0000002975", LBMATCH=FIRST, SaveOffset=0, Search=Header, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(sp_body, PAGE=index_html, LB="size=3>&nbsp;<b>", RB="e</b></font>", LBMATCH=FIRST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_search_var(ParamSPIndexFP, PAGE=IndexFileParameter, LB="<titlex>", RB="</titlex>", LBMATCH=FIRST, ORD=ANY, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//Declare Parameter
nsl_decl_var(param1, DefaultValue="CavissonSystems");

//Date and Time
nsl_date_var(ParamDateTime, Format="%d/%m/%y", Unique=YES, Refresh=SESSION);


//Random String
nsl_random_string_var(ParamRandomString, Min=5, Max=50, CharSet="a-zA-Z0-9", Refresh=SESSION);

//Random Number
nsl_random_number_var(ParamRandonNumber, Min=10, Max=72, Format=%05lu, Refresh=SESSION);

//Unique Number
nsl_unique_number_var(ParamUniqueNumber, Format=%07lu, Refresh=SESSION);

//XML Parameter
nsl_xml_var(ParamXML, PAGE=XML_Page, NODE=<office><Product>, VALUE=<>, ORD=1, EncodeMode=None);

//Declare Array
nsl_decl_array(ParamDeclareArray,Size=10,DefaultValue="Abcd");

//Unique Range
nsl_unique_range_var(ParamUniqueRange, StartRange="10", UserBlockSize="5", Refresh="SESSION", Format="%d", ActionOnRangeExceed="CONTINUE_WITH_LAST_VALUE");

//JSON Parameter
nsl_json_var(ParamJson, PAGE=JSON_Page, OBJECT_PATH="root.glossary.GlossDiv.GlossList.GlossEntry.ID", SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);

//Index File Parameter
nsl_index_file_var(ParamIndexFile, File=dataParamIndexFile, indexVar=ParamSPIndexFP, EncodeMode=All);
