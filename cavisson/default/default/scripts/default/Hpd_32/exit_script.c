/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: nibedita
    Date of recording: 02/10/2021 01:31:24
    Flow details:
    Build details: 4.5.0 (build# 91)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
