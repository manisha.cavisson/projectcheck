//declareparameter
nsl_decl_var(param1,DefaultValue="CavissonSystems");
nsl_static_var(uname:1,upass:2, File=.seq, Refresh=SESSION, Mode=SEQUENTIAL, EncodeMode=All);
//xmlparameter
nsl_xml_var(xmlpara, PAGE=XML_Page, NODE=<username><password>, VALUE=<>, ORD=LAST, EncodeMode=None);
//nsl_json_var(ParamJson, PAGE=JSON_Page, OBJECT_PATH="root.store.book[0].category", ORD=LAST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
//jsonparameter
nsl_json_var(para_json, PAGE=JSON_Page, OBJECT_PATH="root.store.book.price", ORD=LAST, SaveOffset=0, RETAINPREVALUE="YES", EncodeMode=None);
nsl_random_number_var(Ran-nuber, Max=4, Format=%01lu, Refresh=SESSION);
