#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"



#define STOP_ON_CP_FAIL                     0
#define CONTINUE_ON_CP_FAIL                 1
#define STOP_ON_CP_PASS                     2
#define CONTINUE_ON_CP_PASS                 3

char *ns_web_websocket_search(char *resp, char *lb, char *rb)
{     

     int msg_len = 0; 
     static char out_buf[4096];
     int count;
     char *msg_ptr = resp;
    
     out_buf[0] = 0;

     printf("WS: (UserId=%d:%d:%d), Searching string, lb = %s, rb = %s, body = %s\n", 
                 ns_get_nvmid(), ns_get_userid(), ns_get_sessid(), lb, rb, msg_ptr);
 
     //msg_ptr = ns_url_get_resp_msg(&msg_len); 
     //printf("WS_SEARCH: msg_len = [%d], msg_ptr = [%s]\n", msg_len, msg_ptr);
    
     count = ns_save_searched_string(NS_ARG_IS_BUF,msg_ptr,NS_ARG_IS_BUF,out_buf,lb,rb,NS_ORD_ANY,0, 4096);
     printf("COUNT= %d, " ,count);
     if(count < 0 )
     {
       printf("WS: (UserId=%d:%d:%d), Searching Staus = Failed, Search Count = %d\n", ns_get_nvmid(), ns_get_userid(), ns_get_sessid(), count);
       return NULL;
     }
     else
     {
       printf("WS: (UserId=%d:%d:%d), Searching Status = Pass,  Search Count = %d, Searched String = [%s]\n", 
               ns_get_nvmid(), ns_get_userid(), ns_get_sessid(), count, out_buf);
     }
     return out_buf;
}

int ns_web_websocket_check(char *check_value , int check_action)
{

     int msg_len = 0; 
     char *msg_ptr;
     int check_status;
 
     msg_ptr = ns_url_get_resp_msg(&msg_len); 
     printf("msg_len = [%d], msg_ptr = [%s]\n", msg_len, msg_ptr);
    
     char *chk_ptr = strstr(msg_ptr,check_value);
     if(!chk_ptr)
     {
       printf("Checkpoint Fail\n");
       check_status = 0;
     }
     else
     {
       printf("CheckPoint Pass\n");
       check_status = 1;
     }
     switch(check_action)
     {
       case 0:
         if(check_status == 0) return -1;
       break;
       case 1:
         if(check_status == 0) return 0;
       break;
       case 2:
         if(check_status == 1) return -1;
       break;
       case 3:
         if(check_status == 1) return 0;
       break;
     }
     return 0;
}

int ns_web_websocket_search_check(char *lb , char *rb , char *check_value , int check_action)
{

     int msg_len = 0; 
     char out_buf[1024];
     int count;
     char *msg_ptr;
 
     msg_ptr = ns_url_get_resp_msg(&msg_len); 
     printf("msg_len = [%d], msg_ptr = [%s]\n", msg_len, msg_ptr);
    
     count = ns_save_searched_string(NS_ARG_IS_BUF,msg_ptr,NS_ARG_IS_BUF,out_buf,lb,rb,NS_ORD_ANY,0,1024);
     if(!count)
     {
       printf("Search Fail\n");
       return -1;
     }
     printf("Search Pass\n"); 

     char *chk_ptr = strstr(out_buf,check_value);
     int check_status;
     if(!chk_ptr)
     {
       printf("Checkpoint Fail\n");
       check_status = 0;
     }
     else
     {
       printf("CheckPoint Pass\n");
       check_status = 1;
     }
     switch(check_action)
     {
       case 0:
         if(check_status == 0) return -1;
       break;
       case 1:
         if(check_status == 0) return 0;
       break;
       case 2:
         if(check_status == 1) return -1;
       break;
       case 3:
         if(check_status == 1) return 0;
       break;
     }
     return 0;
} 
