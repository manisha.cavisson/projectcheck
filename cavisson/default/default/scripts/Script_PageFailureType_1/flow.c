/*-----------------------------------------------------------------------------
    Name: flow
    Recorded By: admin
    Date of recording: 10/15/2016 01:17:34
    Flow details:
    Build details: 4.1.5 (build# 61)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

void flow()
{
    ns_start_transaction("PageFailure_CVFail_1");   
    ns_web_url ("PageFailure_CVFail_1",
        "URL=http://127.0.0.2/tours/index.html",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474430087.png",
        "Snapshot=webpage_1476474430335.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
    ns_page_think_time(1);
    ns_end_transaction("PageFailure_CVFail_1", NS_AUTO_STATUS);    
    ns_page_think_time(10.651);

    ns_start_transaction("PageFailure_SizeTooBig_1"); 
    ns_web_url ("PageFailure_SizeTooBig_1",
        "URL=http://127.0.0.2/cgi-bin/login?userSession=75893.0884568651DQADHfApHDHfcDtccpfAttcf&username=ajit&password=ajit&login.x=0&login.y=0&login=Login&JSFormSubmit=off",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474440870.png",
        "Snapshot=webpage_1476474441288.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(2);
	ns_end_transaction("PageFailure_SizeTooBig_1", NS_AUTO_STATUS);
    ns_page_think_time(1.946);

    //Page Auto splitted for Image Link 'Search Flights Button' Clicked by User
    ns_start_transaction("PageFailure_SizeTooSmall_1");
    ns_web_url ("PageFailure_SizeTooSmall_1",
        "URL=http://127.0.0.2/cgi-bin/reservation",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474443118.png",
        "Snapshot=webpage_1476474443596.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/splash_Findflight.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/continue.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(2);
	ns_end_transaction("PageFailure_SizeTooSmall_1", NS_AUTO_STATUS);
    ns_page_think_time(0.924);

    //Page Auto splitted for Image Link 'findFlights' Clicked by User
    ns_start_transaction("PageFailure_ConFail_1");
    ns_web_url ("PageFailure_ConFail_1",
        "URL=http://10.10.30.65:81/cgi-bin/findflight?depart=Acapulco&departDate=10-16-2016&arrive=Acapulco&returnDate=10-17-2016&numPassengers=1&seatPref=None&seatType=Coach&findFlights.x=47&findFlights.y=17&findFlights=Submit",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474444487.png",
        "Snapshot=webpage_1476474445079.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/splash_Searchresults.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/continue.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/startover.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(1);
	ns_end_transaction("PageFailure_ConFail_1", NS_AUTO_STATUS);
    ns_page_think_time(1.03);

    //Page Auto splitted for Image Link 'reserveFlights' Clicked by User
    ns_start_transaction("PageFailure_ClickAway_1");
    ns_web_url ("PageFailure_ClickAway_1",
        "URL=http://127.0.0.2/cgi-bin/findflight?outboundFlight=button0&hidden_outboundFlight_button0=000%7C0%7C10-16-2016&hidden_outboundFlight_button1=001%7C0%7C10-16-2016&hidden_outboundFlight_button2=002%7C0%7C10-16-2016&hidden_outboundFlight_button3=003%7C0%7C10-16-2016&numPassengers=1&advanceDiscount=&seatType=Coach&seatPref=None&reserveFlights.x=78&reserveFlights.y=16",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474446049.png",
        "Snapshot=webpage_1476474446603.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/splash_creditcard.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/purchaseflight.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/startover.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(1);
	ns_end_transaction("PageFailure_ClickAway_1", NS_AUTO_STATUS);
    ns_page_think_time(1.25);

    //Page Auto splitted for Image Link 'buyFlights' Clicked by User
    ns_start_transaction("PageFailure_DataExhaused_1");
    ns_web_url ("PageFailure_DataExhaused_1",
        "URL=http://127.0.0.2/cgi-bin/findflight?firstName={FName}&lastName={LName}&address1=4261+Stevenson+Blvd.&address2=Fremont%2C+CA+94538&pass1=Scott+Tiger&creditCard=&expDate=&oldCCOption=&numPassengers=1&seatType=Coach&seatPref=None&outboundFlight=000%7C0%7C10-16-2016&advanceDiscount=&buyFlights.x=93&buyFlights.y=9&.cgifields=saveCC",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474447787.png",
        "Snapshot=webpage_1476474448331.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/flights.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/home.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/signoff.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/splash_flightconfirm.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/bookanother.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(2);
	ns_end_transaction("PageFailure_DataExhaused_1", NS_AUTO_STATUS);
    ns_page_think_time(2.168);

    //Page Auto splitted for Image Link 'SignOff Button' Clicked by User
    ns_start_transaction("PageFailure_Reload_1");
    ns_web_url ("PageFailure_Reload_1",
        "URL=http://127.0.0.2/cgi-bin/welcome",
        "HEADER=Accept-Language:en-us,en;q=0.5",
        "PreSnapshot=webpage_1476474450400.png",
        "Snapshot=webpage_1476474450899.png",
        INLINE_URLS,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/banner_animated.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/sun_swede.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/Merc10-dev/images/login.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/images/banner_merctur.jpg", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE,
            "URL=http://127.0.0.2/tours/vep/images/velocigen.gif", "HEADER=Accept-Language:en-us,en;q=0.5", END_INLINE
    );
     ns_page_think_time(2);
	ns_start_transaction("PageFailure_Reload_1");
    ns_page_think_time(2.688);

}
