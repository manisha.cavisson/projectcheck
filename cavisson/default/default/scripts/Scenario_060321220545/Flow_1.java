/*-----------------------------------------------------------------------------
    Name: Flow_1
    Generated By: This is an auto generated script. You can, however, make a copy of this script using advanced script manager and enhance it.
    Date of generation: 06/03/2021 10:10:20
    Flow details:
    Build details: 4.6.1 (build# 7)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.Scenario_060321220545;
import pacJnvmApi.NSApi;



public class Flow_1 implements NsFlow
{
    public int execute(NSApi nsApi) throws Exception
    {
        int status = 0;

        nsApi.ns_start_transaction("Transaction_1");
        nsApi.ns_web_url ("Transaction_1",
        "URL=https://www.google.com",
        "METHOD=GET",
        "HEADER=Accept:text/plain,application/json,text/html,application/javascript,application/xml",
        "HEADER=User-Agent:Mozilla/5.0 (Windows; U; Windows NT 6.0; de-DE) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1",
        "HEADER=Accept-Encoding: gzip, deflate, br, compress;q=0.9"
        );
        nsApi.ns_end_transaction("Transaction_1", NS_AUTO_STATUS);

    status = nsApi.ns_page_think_time(0.0);

        return status;
    }
}
