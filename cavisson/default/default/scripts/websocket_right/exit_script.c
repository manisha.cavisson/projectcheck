/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: netstorm
    Date of recording: 09/03/2016 10:06:11
    Flow details:
    Build details: 4.1.6 (build# 10)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
