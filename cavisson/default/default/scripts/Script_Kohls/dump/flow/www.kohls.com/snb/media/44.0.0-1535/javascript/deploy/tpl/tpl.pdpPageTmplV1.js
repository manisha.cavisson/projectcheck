<script id="tpl-pdpPanel" type="text/x-jsrender">
	{{if productV2JsonData.isMobile}}
		{{for  tmpl='tpl-pdpPanel-mobile' /}}
	{{else}}
		{{for  tmpl='tpl-pdpPanel-desktop' /}}
	{{/if}}
</script>

<script id="tpl-pdpPanel-desktop" type="text/x-jsrender">
<div id="content" class="pdp_container">
	<div id="{{id:'root'}}" class="snb-container">
		{{panel:'pdpMetaInfoPanel'}}
		<div class="pdp-content">
	        {{panel:'pdpContentPanel'}}
	    </div>
	    <div class="external">
			<div id="stylitics">
				{{panel:'pdpStyliticsPanel'}}
			</div>
	        <div id="trendage">
	            {{panel:'pdpTrendagePanel'}}
	        </div>
	        <div id="PDP_recommendations">
	            {{panel:'pdpBDRecsPanel'}}
	        </div>
			<div class="dfpHorizontalAd">
	            {{if $env.enable_googleDFP_product_details_page && $env.enable_googleDFP_product_details_page_super_leaderboard}}
	                <div id="dfp_super_leaderboard" class="dfp_super_leaderboard" style="text-align: center;"></div>
	            {{/if}}
	        </div>
			{{if productV2JsonData.richCopyBlock}}
	            <div class="creative-slot seo-tag-cloud">
	                {{:productV2JsonData.richCopyBlock}}
	            </div>
	        {{/if}}
	        <div id="PDP_web_collage">
	            {{panel:'pdpWebCollageInlinePanel'}}
	        </div>
			{{if $env.curalateSiteName}}
			<div data-crl8-container-id="custom-product" data-crl8-filter="productId:'{{>productV2JsonData.webID}}'"></div>
			{{/if}}
	        <div id="PDP_ratings_details">
	            {{panel:'pdpBVDetailsPanel'}}
	        </div>
	        <div id="PDP_monetization_HL">
	            {{panel:'pdpFeaturedProductsPanel'}}
	        </div>
			<div id="PDP_recommendations_bottom">
				<div id="bd_rec_{{:$env.bdRenderingPlacementH3}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
			</div>
	        <div id="PDP_monetization_AS_links">
	            {{panel:'pdpAdSensePanel'}}
	        </div>
	        <div id="PDP_monetization_DFP">
	            {{panel:'pdpDFPAdsPanel'}}
	        </div>
	    </div>
	    {{if productV2JsonData.textCopyBlock}}
			<div class="creative-slot seo-tag-cloud">
				{{:productV2JsonData.textCopyBlock}}
			</div>
	    {{/if}}
	    <a href="javascript:void(0);" class="scrollToTop pdpScroll" alt="scroll to top"><span class="hidden-accessiblity-link">Scroll to top</span></a>
	    <a href="javascript:void(0);" class="scrollToNext pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	    <a href="javascript:void(0);" class="scrollToNextDiv pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	</div>
</div>
</script>



<script id="tpl-pdpPanel-mobile" type="text/x-jsrender">
<div id="content" class="pdp_container pdp_mobile_container">
	<div id="{{id:'root'}}" class="snb-container">
		{{panel:'pdpMetaInfoPanel'}}
		<div class="pdp-content">
	        {{panel:'pdpContentPanel'}}
	    </div>
	    <div class="external">
			<div id="stylitics">
				{{panel:'pdpStyliticsPanel'}}
			</div>
	        <div id="PDP_recommendations">
	            {{panel:'pdpBDRecsPanel'}}
	        </div>
			{{if productV2JsonData.richCopyBlock}}
	            <div class="creative-slot seo-tag-cloud">
	                {{:productV2JsonData.richCopyBlock}}
	            </div>
	        {{/if}}
			{{if $env.curalateSiteName}}
			<div data-crl8-container-id="custom-product" data-crl8-filter="productId:'{{>productV2JsonData.webID}}'"></div>
			{{/if}}
	        <div id="PDP_ratings_details">
	            {{panel:'pdpBVDetailsPanel'}}
	        </div>
	        <div id="PDP_monetization_HL">
	            {{panel:'pdpFeaturedProductsPanel'}}
	        </div>
			<div id="PDP_recommendations_bottom">
				<div id="bd_rec_{{:$env.bdRenderingPlacementH3}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
			</div>
	    </div>
	    {{if productV2JsonData.textCopyBlock}}
			<div class="creative-slot seo-tag-cloud">
				{{:productV2JsonData.textCopyBlock}}
			</div>
	    {{/if}}
	    <a href="javascript:void(0);" class="scrollToTop pdpScroll" alt="scroll to top"><span class="hidden-accessiblity-link">Scroll to top</span></a>
	    <a href="javascript:void(0);" class="scrollToNext pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	    <a href="javascript:void(0);" class="scrollToNextDiv pdpScroll" alt="next product"><span class="hidden-accessiblity-link">Next Product</sapn></a>
	</div>
</div>
</script>
<script id="tpl-pdpMetaInfo" type="text/x-jsrender">
<div id="{{id:'root'}}">
 	<div itemscope itemtype="http://schema.org/Product">
		<meta itemprop="name" content="{{:productV2JsonData.productTitle}}" />
		<meta itemprop="description" content="{{:~removeSpecialCharacters(productV2JsonData.metaInfo && productV2JsonData.metaInfo[0].metaDescription)}}" />
        <meta itemprop="url" content="{{:~getUrlForMeta(productV2JsonData.seoURL)}}" />
        <meta itemprop="image" content="{{:~getImagesForMeta(productV2JsonData)}}" />
		<meta itemprop="brand" content="{{:productV2JsonData.brand}}" />
        <meta itemprop="seller" content="Kohls" />
		{{if ~getReviewCount(productV2JsonData.ratingCount)}}<div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<meta itemprop="ratingValue" content="{{:productV2JsonData.avgRating}}" />
			<meta itemprop="ratingCount" content="{{:productV2JsonData.ratingCount}}" />
		</div>{{/if}}
		{{for productV2JsonData.SKUS ~productV2JsonData=productV2JsonData ~env=$env}}
			{{if (#index < 50) && ~checkCounterForSchema(#data, ~env.itemScopeLimit)>0 }}
				<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="{{:skuCode}}" />
                <meta itemprop="url" content="{{:~getUrlForMeta(~productV2JsonData.seoURL)}}" />
				<meta itemprop="price" content="{{:~getPriceForMeta(price, ~env)}}" />
				<meta itemprop="priceCurrency" content="USD" />
                {{if availability == "In Stock"}}
				    <meta itemprop="availability" content="http://schema.org/InStock" />
                {{else}}
                    <meta itemprop="availability" content="https://schema.org/OutOfStock" />
                {{/if}}
				<meta itemprop="itemCondition" content="http://schema.org/NewCondition" />
				{{if ~env.enableLiaSchemaPdp && ~productV2JsonData.storeIds && ~productV2JsonData.skuId }}
					<link itemprop="potentialAction" itemscope itemtype="http://schema.org/BuyAction"/>
					<div itemprop="deliveryLeadTime" itemscope itemtype="http://schema.org/QuantitativeValue">
						<span itemprop="value" content="0"/>
					</div>
					{{if skuCode == ~productV2JsonData.skuId }}
						<link itemprop="availableDeliveryMethod" href="http://schema.org/OnSitePickup"/>
						<div itemprop="availableAtOrFrom" itemscope itemtype="http://schema.org/Place">
							<span itemprop="branchCode" content="{{:~productV2JsonData.storeIds}}"/>
						</div>
					{{/if}}
				{{/if}}
				</div> {{if availability == "In Stock" }} {{for ~getSelectedSkuData(#data, ~productV2JsonData) /}} {{/if}}
			{{/if}}{{for ~updateSizeColor(~productV2JsonData, #data, skuId) /}}
		{{/for}}
	</div>
	{{if $env.brightTagEnabled}}
		{{for ~selectedSku=productV2JsonData.selectedSku tmpl="pdp_bright_tag_head" /}}
	{{/if}}
	</div>
</script>

<script id="pdp_bright_tag_head" type="text/x-jsrender">
	{{if $env.isSignalMarketingDataEnabled}}
		<script type="text/javascript">
			var pageData=
			{
			"pageDetails":{
				"pageURL":"{{:fullURL}}",
				"pageName": "{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
				"pageSite":"kohls.com",
				"pageChannel":"desktop",
				"pageType":"{{:$env.brightTagPageTypeDetail}}",
				"defaultLanguage":"{{:$env.brightTagDefaultLanguage}}",
				"browserPostalCode": "N/A",
				"timeStamp":""
				},
				"productDetails":
				{
					"departmentName":"{{:productV2JsonData.monetizationData.department}}",
					"categoryName":"{{:productV2JsonData.monetizationData.category}}",
					"subcategoryName":"{{:productV2JsonData.monetizationData.subCategory}}",
					"pageView":"Other",
					"pageFindingMethod":"{{:$env.brightTagPageFindingMethod}}",
					"pageKeywords":"N/A",
					"totalItems":"{{:$env.brightTagtotalItems}}",
					"collectionID":"{{:$env.brightTagCollectionID}}",
					{{if (~selectedSku) }}
						"pageItems": [	{
											"productName":"{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
											"productID":"{{:productV2JsonData.webID}}",
											"productSKU":"{{:~selectedSku.skuId}}",
											"originalPrice":"{{:~selectedSku.regularPrice}}",
											"isOnSale":"{{:~selectedSku.isOnSale}}",
											"salePrice":"{{:~selectedSku.salePrice}}"==''?'N/A':"{{:~selectedSku.salePrice}}",
											"isInStock":"{{:~selectedSku.inventoryStatus}}"
										}
									]
					{{/if}}
				}
			};
		{{:"<"}}/script>
		{{/if}}

		{{if !$env.isSignalMarketingDataEnabled}}
		<script type="text/javascript">
			var VisitorId = (Kjs.cookie.get("DYN_USER_ID")!="") ? Kjs.cookie.get("DYN_USER_ID") :"";
			var pageData=
			{
			"page_details":{
					"pageSite":"kohls.com",
					"pageExperience":"kohls.com",
					"pageType":"{{:$env.brightTagPageTypeDetail}}",
					"pageView":"Other",
					"pageFindingMethod":"{{:$env.brightTagPageFindingMethod}}",
					"pageKeyword":"",
					"pageTotalItems":"",
					"pageitemIDs":"{{:productV2JsonData.webID}}",
					"pageSKUIDs":"",
					"pageTypeHL":"product"
				},
				"Customer_Details":	{
					"customerID":VisitorId,
					"customerIsLoggedIn":"{{:$env.brightTagCustomerIsLoggedIn}}"
				},
				{{if (~selectedSku) }}
				"product_Details": {
									"itemName":"{{:productV2JsonData.productTitle}}".replace(/&#34;/ig, "\""),
									"itemProductID":"{{:productV2JsonData.webID}}",
									"itemSkuID":"{{:~selectedSku.skuId}}",
									"itemOriginalPrice":"{{:~selectedSku.regularPrice}}",
									"itemOnSale":"{{:~selectedSku.isOnSale}}",
									"itemSalePrice":"{{:~selectedSku.salePrice}}"==''?'N/A':"{{:~selectedSku.salePrice}}",
									"itemIsInStock":"{{:~selectedSku.inventoryStatus}}"
								}
				{{/if}}
			};
		{{:"<"}}/script>
		{{/if}}

		<script type="text/javascript">
			var ishookLogicEnabled = {{:hooklogicEnabled}};
			if(window.enableHookLogic==false){
				ishookLogicEnabled ="false";
			}
			var placement = null;
			var placementA = document.getElementById('hl_1_999');
			var placementB = document.getElementById('hl_2_999');
			if(placementA!=null){
					placement = "hl_1_999";
			}
			if(placementB!=null){
					placement = "hl_2_999";
			}
			if(placementA!=null && placementB!=null){
					placement = "hl_1_999,hl_2_999";
			}
			var pagetype ="{{:$env.brightTagPagetype}}";
		{{:"<"}}/script>
		{{if $env.brightTagsource != "common" && gettagParameter != 'true'}}
		<script type="text/javascript">
			var brightTagScriptCallParam  =  "{{:$env.bt_siteId}}";
			var brightTagScriptCallURL    =  "{{:$env.bt_URL}}" + "#site="+brightTagScriptCallParam+"&referrer=" + encodeURIComponent(document.location.href); 
			$requires(brightTagScriptCallURL, {
				text: '{ site: "{{:$env.bt_siteId}}" }',
				reload: true,
				isLast: true
			});
		{{:"<"}}/script>
		{{/if}}
</script>
<script id="tpl-pdpBreadcrumb" type="text/x-jsrender">
<div id="{{id:'root'}}" class="PDP_breadcrumb">
    <ul class="bread-crupms">
        <li id='backArrow'><span class="back-arrow" onclick="javascript:window.history.back(-1);return false;"> </span></li>
        <li id='backButton'><a onclick="javascript:window.history.back(-1);return false;" href="javascript:void(0);">Back</a> <span class="divider-vertical"></span> </li>
        {{if productV2JsonData.breadcrumbs}}
            {{for productV2JsonData.breadcrumbs ~len=productV2JsonData.breadcrumbs.length}}
                <li class="pdp-breadcrumb-name">
                    <a href="{{:seoURL}}" {{if #index == (~len-1)}}class="last-li"{{/if}}>{{:name}}</a>
                    {{if #index < (~len-1)}}<span class="next-arrow"></span>{{/if}}
                </li>
            {{/for}}
        {{/if}}
    </ul>
    <div id="pdp_breadcrumb_nextproduct" style="float:right;"></div>
</div>
</script><script id="tpl-pdpTitle" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-left-alignment" >
    <div class="pdp-error disabled" id="error-display">
		<p>
			<img src="{{:$env.resourceRoot}}images/error-icon.png" class="errorImg" alt="Info Error">
			<span>Some information is missing or invalid below.</span>
		</p>
		<p id="skuerror"></p>
	</div>
</div>
</script><script id="tpl-productImageZoom-main" type="text/x-jsrender">
	<div class="pdpHeroImageZoom" id="{{id:'root'}}">
		<div class="heroImageContainer">
			<button id="{{id:'exitZoom'}}" class="exitZoom"></button>
			<div id="heroImgWrapper">
				<div id="{{id:'slider'}}" class="slider">
					<div class="pdpCarouselZoom" id="{{id:'carousel'}}">
						<ul class="slides">
							{{if images && images.length > 0}}
								<li class="imageSlider"><img src="{{:~productImageParam(images[0].url,'1200')}}" class='pdp_heroimage'/></li>
							{{/if}}
							{{if altImages && altImages.length > 0}}
								{{for altImages}}
									<li class="imageSlider"><img src="{{:~productImageParam(#data.url,'1200')}}" class='pdp_altimage'/></li>
								{{/for}}
							{{/if}}
							{{if videos && videos.length && videos[0].url}}
								{{for videos}}
									{{if #data.url.toLowerCase().indexOf('.mp4') > -1 }}
										<li class="imageSlider"><video controls id="{{id:'video'}}" src={{:#data.url}} style="width:100%"></video></li>
									{{/if}}
								{{/for}}
							{{/if}}
						</ul>
					</div>
				</div>
				<div class="fullImage-flex-btn">
					<a href="javascript:void(0)" class="flex-prev-large" id="{{id:'prevLarge'}}"></a>
				</div>
				<div class="fullImage-flex-btn">
					<a href="javascript:void(0)" class="flex-next-large" id="{{id:'nextLarge'}}"></a>
				</div>

				<div id="{{id:'customNav'}}" class="custom-navigation">

					<a href="javascript:void(0)" class="flex-prev" id="{{id:'prev'}}"></a>
				{{if ((videos && videos.length && videos[0].url.toLowerCase().indexOf('.mp4') > -1) + (altImages && altImages.length) + (images && images.length)) > 6}}
					<div id="{{id:'pageNumCarousel'}}" class="pageNumberCarousel">1 <span class="ofText"> of</span> {{:((altImages && altImages.length) + (images && images.length) + (videos && videos.length && videos[0].url.toLowerCase().indexOf('.mp4') > -1 ))}} </div>
					{{if videos && videos.length && videos[0].url && videos[0].url.toLowerCase() && videos[0].url.toLowerCase().indexOf('.mp4') > -1 }}
						{{for videos}}
							{{if #data.url.toLowerCase().indexOf('.mp4') > -1 }}
								<a href="#!" class="flex-videoicon" id="{{id:'videoIcon'}}"></a>
							{{/if}}
						{{/for}}
					{{/if}}
				{{else}}
					<div id="{{id:'customControls'}}" class="custom-controls-container"></div>
				{{/if}}
				<a href="#" class="flex-next" id="{{id:'next'}}"></a>
				</div>
			</div>
			<div id="{{id:'swatches'}}" class="swatches"></div>
		</div>
	</div>
</script>

<script id="tpl-productImageZoom-swatches" type="text/x-jsrender">
	<div class="zoom-swatches" id="{{id:'root'}}">
		<button id="{{id:'prevSwatch'}}" class="zoom-swatch-prev" {{if  variants.colorList && variants.colorList.length < 8 }}style="display:none"{{/if}}>
			<div class="zoom-swatch-glyph"></div>
		</button>
		<div id="{{id:'colorSwatches'}}" class="zoom-color-swatches">
		{{if ~getImageData(#data) != '' && variants.swatchImage != undefined && variants.swatchImage != ''}}
			<div class="zoom-swatch-carousel-container">
				<ul id="{{id:'swatchList'}}" class="zoom-swatch-carousel"{{if  variants.colorList && variants.colorList.length < 8 }} style="display:flex"{{/if}}>
				{{for swatchList ~environment=environment}}
					<li>
					{{if ~environment=="staging" && colorFlag}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color {{:~root.activeColor == color ? 'active':''}}">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color no-inventory" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
									<span style="opacity:0"> out of stock</span>
								</a>
							</div>
						</div>
					{{else colorFlag===false}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color {{:~root.activeColor == color ? 'active':''}}">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}">{{if oosFlag}}<img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>{{/if}}
								</a>
							</div>
						</div>
					{{else colorFlag===true}}
						<div id="{{id:'outerSwatch'}}" class="zoom-swatch-color-container {{:~root.activeColor == color ? 'selected':''}}">
							<div class="zoom-swatch-color color-unavailable" title="out of stock">
								<a id="{{id:'colorSwatch'}}" data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="swatch-color" href="javascript:void(0);" style="background:url({{:backgroundURL}}) no-repeat;pointer-events: none;" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
									<span style="opacity:0"> out of stock</span>
								</a>
							</div>
						</div>
					{{/if}}
					</li>
				{{/for}}
				</ul>
			</div>
		{{/if}}
		</div>
		<button id="{{id:'nextSwatch'}}" class="zoom-swatch-next" {{if variants.colorList && variants.colorList.length < 8}}style="display:none"{{/if}}>
			<div class="zoom-swatch-glyph"></div>
		</button>
	</div>
</script>
<script id="tpl-pdpHeroImage" type="text/x-jsrender">
	<div class="pdpCarousal-Wrapper" id="{{id:'root'}}">
		{{if ~imageList(productV2JsonData).length == 1}}
		<div class="pdp-image-view">
			<div class="pdp-image-single" id="{{id:'largeImageContainer'}}"></div>
		</div>
		{{else}}
		<div class="pdp-image-view">
			<div class="pdp-image" id="{{id:'largeImageContainer'}}"></div>
			<div class="pdp-slider-cnt">
				<div id="{{id:'prevSlide'}}" class="slider-control slider-prev disabled">
					<a href="javascript:void(0)" title="Previous set of images" class="slider-button"><span class="slider-img"></span></a>
				</div>
				<div class="pdp-slider hidden" id="{{id:'slider'}}">
					<ul class="slides" id="{{id:'slides'}}">
						{{for ~imageList(productV2JsonData)}}
						<li class="slider-image {{:type}}" id="{{id:'sliderImage'}}">
							<div class="slider-image-box">
						{{if type == 'hero'}}
							<img src="{{:~productImageParam(url,'96')}}" alt="{{>altText}}"/>
						{{/if}}
						{{if type == 'alt'}}
							<img src="{{:~productImageParam(url,'96')}}" alt="{{>altText}}"/>
						{{/if}}
						{{if type == 'video'}}
							<img src="{{v:~root.$resourceRoot}}images/pdp/play-inactive.svg" alt="{{>altText}}"/>
							{{!-- <video controls src={{:url}} width="96px" height="96px"></video> --}}
						{{/if}}
							</div>
						</li>
						{{/for}}
					</ul>
				</div>
				<div id="{{id:'nextSlide'}}" class="slider-control slider-next disabled">
					<a href="javascript:void(0)" title="Next set of images" class="slider-button"><span class="slider-img"></span></a>
				</div>
			</div>
		</div>
		{{/if}}
	<div style="clear:both"></div>

	<div class="pdp-additional-links">
		<div class="pdp-additional-links-marker" style="display:flex;">
			{{if $env.enableStylitics}}
				<div class="additional-link tce-link-container" style="display:none">
					<img src="{{v:$resourceRoot('snb')}}images/9-dots.png" class="tce-link-icon"/><a id="{{id:'styliticsLink'}}" href="#tce-anchor" class="tce-jump-link">Ways to Style It</a>
				</div>
			{{/if}}
			{{if $env.enableWebcollageMosaic}}
				<div id="wc-mosaic" class='additional-link'></div>
			{{/if}}
		</div>
	</div>
</div>
</script>

<script id="tpl-largeImage-image" type="text/x-jsrender">
	<div class="easyzoom easyzoom--overlay easyzoom--with-thumbnails is-ready"><a href="{{:largeImgUrl}}"><img class="pdp-image-main-img" src="{{:url}}" alt="{{>altText}}"/></a></div>
</script>

<script id="tpl-largeImage-video" type="text/x-jsrender">
	<video controls id="video" src="{{:url}}" width="600px" height="600px"></video>
</script>
<script id="tpl-pdpHeroImage-zoom" type="text/x-jsrender">
	<div class="pdpCarousal-Wrapper" id="{{id:'root'}}">
		{{if !productV2JsonData.altImages && ~calculateProductImagesLength(productV2JsonData)}}
		<section class="slider singleImage" id="sliderSection">
			<div class="pdpCarousal"  id="{{id:'pdpCarousal'}}">
				<ul class="slides">
					<li class="imageSlider flex-active-slide"><img src="{{:~productImageParam(productV2JsonData.images[0].url,'600')}}" class='pdp_heroimage singleSlide' id="{{id:'heroimage'}}"/></li>
				</ul>
			</div>
		</section>
		{{else}}
		<section class="slider multiImage" id="sliderSection">
			<div class="pdpCarousal flexslider" id="{{id:'pdpCarousal'}}">
				<ul class="slides">
					{{if productV2JsonData.images && productV2JsonData.images.length > 0}}
						<li class="imageSlider"><img src="{{:~productImageParam(productV2JsonData.images[0].url,'600')}}" class='pdp_heroimage' id="{{id:'heroimage'}}"/></li>
					{{/if}}
					{{if productV2JsonData.altImages && productV2JsonData.altImages.length > 0}}
						{{for productV2JsonData.altImages}}
							<li class="imageSlider"><img id="{{id:'altimg'}}" {{:~productImageLazy(#data.url,'600', #index)}} class='pdp_altimage'/></li>
						{{/for}}
					{{/if}}
					{{if productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url }}
						{{for productV2JsonData.videos}}
							{{if  #data.url.toLowerCase().indexOf('.mp4') > -1 }}
								<li class="imageSlider altVideoLink" id="slide2"><video controls id="video" src={{:#data.url}} width="600px" height="600px"></video></li>
							{{/if}}
						{{/for}}
					{{/if}}
				</ul>
			</div>
			</section>
		{{/if}}
	<div style="clear:both"></div>

	<div class="custom-navigation" id="custom-navigation">
		<a href="#" class="flex-prev"></a>
		{{if ((productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1) + (productV2JsonData.altImages && productV2JsonData.altImages.length) + (productV2JsonData.images && productV2JsonData.images.length)) > 6}}
			<div class="pageNumberCarousel">1 <span class="ofText">of</span> {{:((productV2JsonData.altImages && productV2JsonData.altImages.length) + (productV2JsonData.images && productV2JsonData.images.length) + (productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1 ))}} </div>
			{{if productV2JsonData.videos && productV2JsonData.videos.length && productV2JsonData.videos[0].url && productV2JsonData.videos[0].url.toLowerCase() && productV2JsonData.videos[0].url.toLowerCase().indexOf('.mp4') > -1 }}
				{{for productV2JsonData.videos}}
					<a href="#!" class="flex-videoicon"></a>
				{{/for}}
			{{/if}}
		{{else}}
			<div class="custom-controls-container"></div>
		{{/if}}
		<a href="#" class="flex-next" onclick=""></a>
	</div>
	<div class="pdp-additional-links">
		{{if $env.enableStylitics}}
			<div class="additional-link tce-link-container" style="display:none">
				<img src="{{v:$resourceRoot('snb')}}images/9-dots.png" class="tce-link-icon"/><a id="{{id:'styliticsLink'}}" href="#tce-anchor" class="tce-jump-link">Ways to Style It</a>
			</div>
		{{/if}}
		{{if $env.enableWebcollageMosaic}}
			<div id="wc-mosaic" class='additional-link'></div>
		{{/if}}
		</ul>
	</div>
</div>
</script>
<script id="tpl-pdpContent" type="text/x-jsrender">
    {{if productV2JsonData.isMobile}}
        {{for  tmpl='tpl-pdpContent-mobile' /}}
    {{else}}
        {{for  tmpl='tpl-pdpContent-desktop' /}}
    {{/if}}
</script>
<script id="tpl-pdpContent-desktop" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="PDP_breadcrumb">
		{{panel:'pdpBreadcrumbPanel'}}
    </div>
    <div id="PDP_product_name">
		{{panel:'pdpTitlePanel'}}
    </div>
	{{if $env.enableHeroAlign}}
	<div id="PDP_colGrid">
        <div id="PDP_block1">
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="rating_summary" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
			{{!-- The following is a panel tag, not a converter, so note subtle difference in syntax --}}
			{{panel 'pdpHeroImagePanel' class="stickyPanel" /}}
        </div>
        <div id="PDP_block2">
            <div id="product-specifications">
                <div id="PDP_product_configuration">
					{{panel:'pdpPrdConfigPanel'}}
                </div>
            </div>
        </div>
    </div>
	<div id="PDP_product_details" style="width:65%;">
		{{panel:'pdpDetailsTabPanel'}}
	</div>
	{{else}}
	<div id="PDP_colGrid">
        <div id="PDP_block1">
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="rating_summary" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
			{{panel 'pdpHeroImagePanel' class="sticky-panel" /}}
			<div id="PDP_product_details" style="width:100%;">
				{{panel:'pdpDetailsTabPanel'}}
			</div>
        </div>
        <div id="PDP_block2">
            <div id="product-specifications">
                <div id="PDP_product_configuration">
					{{panel:'pdpPrdConfigPanel'}}
                </div>
            </div>
        </div>
    </div>
	{{/if}}

</div>
</script>

<script id="tpl-pdpContent-mobile" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="PDP_breadcrumb">
		{{panel:'pdpBreadcrumbPanel'}}
    </div>
    <div id="PDP_product_name">
		{{panel:'pdpTitlePanel'}}
    </div>
    <div id="PDP_colGrid_mobile">
        <div id="PDP_block1_mobile">
            {{if productV2JsonData.productTitle}}
                <div class="pdp-title-container" id="{{id:'pdpTitle'}}">
                    <h1 class="product-title">
                        {{:productV2JsonData.productTitle}}
                    </h1>
                    {{if productV2JsonData.brand}}
                        <div class="sub-product-title"> by <a href="{{:productV2JsonData.brandSeoUrl}}">{{:productV2JsonData.brand}} </a></div>
                    {{/if}}
                </div>
            {{/if}}
            <div class="pdp-main-bazarvoice-ratings">
                {{if $env.bazaarvoiceEnabled}}
                    {{if $env.bvConversion}}
                        <div data-bv-show="inline_rating" data-bv-seo="false" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
            </div>
            {{!-- The following is a panel tag, not a converter, so note subtle difference in syntax --}}
			{{panel 'pdpHeroImagePanel' class="pdp_mobile_heroimage"/}}
        </div>
        <div id="PDP_block2_mobile">
            <div id="product-specifications">
                <div id="PDP_product_configuration">
					{{panel:'pdpPrdConfigPanel'}}
                </div>
            </div>
        </div>
    </div>
	<div id="PDP_product_details" style="width:96%;">
		{{panel:'pdpDetailsTabPanel'}}
	</div>
</div>
</script>
<script id="tpl-pdpPrdConfig" type="text/x-jsrender">
<div id="{{id:'root'}}" class="product_boss_tmpl">
	<div id="PDP_z1Social_badge"></div>
	{{for productV2JsonData.teaserData ~gwpLink=productV2JsonData.staticContents.gwppwpDetailsLink }}
		<div class="gwppwpTeaserMsg">
			<div class="get-teaser"></div>
			<div class="gwp-pwp-teaser">
				{{:configuredOfferMessage}}
				{{if isBuyProduct}}
					{{if isPwpOffer}}
						<span><a href="javascript:void(0);" class="learn-more-link" id="pdp-gwp-pwp" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabpwp', 'Special Savings Item');" title="{{:~gwpLink}}">{{:~gwpLink}}</a></span>
					{{else}}
						<span><a href="javascript:void(0);" class="learn-more-link" id="pdp-gwp-pwp" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabgwp', 'giftwithpurchase');" title="{{:~gwpLink}}">{{:~gwpLink}}</a></span>
					{{/if}}
				{{/if}}
			</div>
		</div>
	{{/for}}
    {{if productV2JsonData.badges}}
		{{for ~creativePDPBadges(productV2JsonData.badges)}}
			{{if badgeName}}
				<div class="clear"></div>
				<div class = "creative_badge_block">
					<span {{if badgeName == 'best seller'}} class="creativeBest" {{else}} class="creativeTop"{{/if}}>
						<span {{if badgeName == 'best seller'}} class = "creativeBadgeBest" {{else}}class="creativeBadgeTop"{{/if}}> {{:badgeName}}</span>
					</span>
				</div>
			{{/if}}
		{{/for}}
	{{/if}}
	{{if productV2JsonData.rebate && productV2JsonData.rebate.shortDescription}}
	<div class="pdp-rebates">
		<p class="pdp-rebatetxt">
			{{:productV2JsonData.rebate.shortDescription}}
			<span><a href="javascript:void(0)" class="learn-more-link" onClick="Kjs.pdpDetailsTabPanel.Evt_openTab('#tabRebate', 'rebates');" id="pdp-rebatelnk" title="Learn More">Learn More</a></span>
		</p>
	</div>
	{{/if}}
	<div id="pdp-Pricing">
		<ul class="pricingList">
			<li>
				<div id="pdpprice-price-container">
					{{for ~productV2JsonData=productV2JsonData productV2JsonData.price tmpl=~getPriceTemplate(productV2JsonData) /}}
					{{for ~Offers=productV2JsonData.price.yourPriceInfo.appliedOffers  ~price=productV2JsonData.price tmpl='tpl-pdp-pricing-ypappliedoffers' /}}
					{{for ~Offers=productV2JsonData.price.yourPriceInfo.saveMoreOffers ~price=productV2JsonData.price tmpl='tpl-pdp-pricing-ypsavemoreoffer' /}}
				</div>
				<ul class="subpricinglist tag-nostyle-fatc-excl-price">
					{{for ~badges= ~getBogoBadges(productV2JsonData.badges)  productV2JsonData.price tmpl='tpl-pdp-pricing-promotion' /}}
					{{for tmpl='tpl-pdp-pricing-kc-section' /}}
				</ul>
				{{if productV2JsonData.price.percentageOff && productV2JsonData.price.percentageOff.length}}
					<div class="perOff">
						{{:~perOffPrice(productV2JsonData.price.percentageOff)}}
					</div>
				{{/if}}
			</li>
			{{if productV2JsonData.surcharges != null && productV2JsonData.surcharges != "" && productV2JsonData.surcharges.length>0}}
				<li>
					<div class="surcharge-msg">
					{{if productV2JsonData.surcharges != null && productV2JsonData.surcharges != "" && productV2JsonData.surcharges.length>0}}
						<div class="surcharge-fee">
							{{if productV2JsonData.surcharges[0].shippingService == null}}
								Shipping Surcharge <span>${{:productV2JsonData.surcharges[0].value}}</span>
							{{else}}
								Surcharge with <span class='surcharge_value'><a id='shipping_retuns' href='javascript:void(0);' title='{{:productV2JsonData.surcharges[0].shippingService}}'>{{:productV2JsonData.surcharges[0].shippingService}}</a></span> ${{:productV2JsonData.surcharges[0].value}}
							{{/if}}
						</div>
					{{/if}}
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.exclusions && productV2JsonData.exclusions.shortDescription}}
				<li>
					<div class="pdp-exclusions">
						<p class="pdp-exclusion-msg">{{:productV2JsonData.exclusions.shortDescription}}</p>
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.price.promotion && productV2JsonData.price.promotion.tieredPrice != null && productV2JsonData.price.promotion.tieredPrice != ""}}
				<li class="tag-nostyle-fatc-excl-price">
					<div class="tiered-price red-font">
						INSTANT SAVINGS
						{{for productV2JsonData.price.promotion.tieredPrice}}
							<span>{{>#data}}</span>
						{{/for}}
					</div>
				</li>
			{{/if}}
			{{if productV2JsonData.ShowGwpPwpElements}}
				{{if productV2JsonData.gwp && productV2JsonData.gwp.isMultipleGiftsAllowed == false}}
					<li class="gwp-terms-label tag-nostyle-fatc-excl-price">
						<div class="gwp-terms-label">{{:productV2JsonData.staticContents.gwpTermsLabel}}</div>
					</li>
				{{/if}}
				{{if productV2JsonData.pwp && productV2JsonData.pwp.isMultipleGiftsAllowed == false}}
					<li class="gwp-terms-label tag-nostyle-fatc-excl-price">
						<div class="gwp-terms-label">{{:productV2JsonData.staticContents.pwpTermsLabel}}</div>
					</li>
				{{/if}}
			{{/if}}
		</ul>
	</div>
    <div class="pdp-product-color clearfix">
		{{for productV2JsonData  ~productID=productV2JsonData.webID tmpl= "tpl-pdp-color-swatches" /}}
    </div>
	{{if productV2JsonData.variants.sizeList }}
	    <div class="pdp-product-size">
			{{if productV2JsonData.isMobile}}
		        {{for productV2JsonData  ~env=$env  ~productID=productV2JsonData.webID tmpl= "tpl-pdp-sizeOnlyDropDown" /}}
		    {{else}}
		        {{for productV2JsonData  ~env=$env  ~productID=productV2JsonData.webID tmpl= "tpl-pdp-size" /}}
		    {{/if}}
		</div>
	{{/if}}
	{{if $env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC }}
		<div class="vgc-panel">
			{{for tmpl= "tpl_PDP_VGC_tmpl" /}}
		</div>
	{{/if}}
	{{if $env.Fit && !productV2JsonData.productType.isVGC }}
        <div class="fit-element">
            {{if productV2JsonData.variants.displaySizeGuide && ( $env.fitPredictor || $env.trueFit) }}
				<div class="sizeguidewrapper">
                	<a href="javascript:void(0);" id="{{id:true 'sizeguide'}}" class="sizeguide" title="{{:productV2JsonData.staticContents.pdpSizeGuide}}">{{:productV2JsonData.staticContents.pdpSizeGuide}}</a>
				</div>
            {{/if}}
            {{if $env.fitPredictor }}
                <div class="fp-root" data-product-id="{{:productV2JsonData.webID}}"></div>
            {{/if}}
        </div>
	{{/if}}
	{{if !($env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC) }}
		<div class="pdp-product-qty-wrapper">
			<div class="pdp-product-quantity-info">
				<div class="quantity" id="{{id: 'pdpQuantity'}}">
					<img class="quantity-warning-icon" alt="quantity-warning-icon" src="{{:$env.resourceRoot}}images/warning-glyph-black.svg" />
					<label for="productQuantity" id="quantity-label" class="qty-label">Quantity</label><br/>
					<div class="pdp-quantity clearfix">
						<ul class="qty-wrap clearfix {{getItem!=undefined?(getItem.isMultipleGiftsAllowed?'':(getItem.isGiftProdPDP)?'isget':''):''}}">
							<li>
								<div class="pdp-qtty pdp-qtty-boss-decrease" tabindex="0" role="button" data-qttyoptn="decrease">
								<p class="pdp-product-decrease  pdp-qtty-boss-disable"></p>
								</div>
							</li>
							<li class="product-quantity-value-wrapper">
								<input  name="productQuantity" id="{{id: 'productQuantity'}}" alt="Product quantity" title="Product quantity" type="text" value="{{:productV2JsonData.cart != undefined ? productV2JsonData.cart.quantity:productV2JsonData.Qty}}" class="pdp-product-quantity" maxlength="3" size="2" data-qttyoptn="change" onpaste="return false" onkeyup="" autocomplete="off" {{:(getItem)?(getItem.isMultipleGiftsAllowed?'':(getItem.isGiftProdPDP)?'disabled':''):''}}/>
							</li>
							<li>
							<div class="pdp-product-increase pdp-qtty pdp-qtty-boss-increase" data-qttyoptn="increase"  tabindex="0" role="button">
								<p>+</p>
							</div>
						</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="pdp-qty-inventory-msg">
				<div id="pdp-scarcity-badge"></div>
				<div class="pdp-qty-inventory-msg-txt"></div>
			</div>
		</div>
	{{/if}}
	{{if $env.showAvailLoader }}
		<div class="fulfillment-loader hide">
			<img src="{{:$env.resourceRoot}}images/ajax_loader_30x30.gif" alt="ajax_loader">
		</div>
	{{/if}}
	<div class="pdp-shipment-info-boss clearfix{{if productV2JsonData.productType.isVGC}} vgc-addtobag{{/if}}">
		{{for productV2JsonData ~env=$env ~productType=productV2JsonData.productType tmpl= "pdp_shipment_info_boss" /}}
	</div>
	{{if $env.enableNotifyMe }}
		<div class="pdp-notify-info clearfix">
		</div>
	{{/if}}
	<div class="product_boss_tmpl" id="{{id: 'addToBagPDP'}}">
		<div class="pdp-product-addtobag addtoBagContainerBOSS">
			<div class="addtobag-container">
				<div class="addtobag-action {{if $env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC}} addtoBagContainerVGC {{/if}}">
					<div class="add-to-bag-pdp">
						<input type="button" title= "Add To Cart" alt="Add To Cart" class="{{:(productV2JsonData.cart != undefined) ?'pdp-updatebag':'pdp-addtobag'}} {{if !productV2JsonData.isSkuAvailable}}muted{{/if}}" id="addtobagID" name="{{:(productV2JsonData.gwp != undefined || productV2JsonData.pwp != undefined)?'get-':''}}pdp-addtobag" data-prodId="{{:productV2JsonData.webID}}" value="{{:(productV2JsonData.cart != undefined)?'Update Cart':'Add to Cart'}}"/>
						<img alt="kohls_img404" class="submit-loader" id="submit_loaderID_631486" src="{{:$env.resourceRoot}}images/loading.svg"/>
					</div>
				</div>
			</div>
		</div>
	</div>
    {{if !productV2JsonData.shopRunnerEnable}}
        {{if !($env.enableVgcRedesign && productV2JsonData.productType && productV2JsonData.productType.isVGC) }}
            <div class="pdp-product-links-info pdp-product-links-info-boss">
            <div id="{{:productV2JsonData.webID}}" class="prod_description1 pdp-addto-list-container">

            <!-- hiden values for addtolist and addtoregistery start -->
            <input type="hidden" id="skava_productId" value="{{:productV2JsonData.webID}}">
            <input type="hidden" class="skuId" value=""/>
            <input type="hidden" id="skava_skuUpcCode" value=""/>
            <!-- hiden values for addtolist and addtoregistery ends -->

                <div id="add_to_list_{{:productV2JsonData.webID}}" class="skava_add_to_list_class">
                    {{if $env.enableWishList && productV2JsonData.vendorName !== 'Wayfair' && ((productV2JsonData.surcharges != null && productV2JsonData.surcharges != "" && productV2JsonData.surcharges[0].shippingService != null && productV2JsonData.surcharges[0].shippingService != "") ? false : true)}}
                        <div id="skava-buttons-active" style="display:inline-flex;">
                            <div id="add_to_list" class="add-tolist add-to-list">
                                <a href="javascript:void(0);" title="Add To List">Add to List</a>
                            </div>
                        </div>
                    {{/if}}
                    {{if productV2JsonData.isStoreAvailable}}
                        <div id="{{id:'findinstore'}}" class="findinstore">
                            <a href="javascript:void(0);" class="ce-fis" title="Find In Store">Find in Store</a>
                        </div>
                    {{/if}}
                </div>
            </div>
            </div>
        {{/if}}
    {{/if}}
    {{if productV2JsonData.valueAddedIcons && productV2JsonData.valueAddedIcons.length > 0 && productV2JsonData.badges}}
        <div class="prod_badgesblock">
            {{for ~getBadgesForPDP(productV2JsonData.badges,$env.resourceRoot,$env.enableDiePhaseTwo)}}
                {{if ~isExcludeBadges(#data) }}
                    <img title="{{:title}}" alt="{{:title}}" src="{{:imageURL}}" />
                {{/if}}
            {{/for}}
        </div>
    {{/if}}
</div>
</script>

<script id="tpl-pdp-pricing-loyaltyV2-Earn" type="text/x-jsrender">
	<p class="pdpprice-kc-row-earn tag-nostyle-fatc-excl-price">
		{{if showPlus}}
			<img src="{{:~env.resourceRoot}}images/lpf/power-of-the-plus.png" class="pdpprice-kc-row-earn-icon" alt="Plus">
			{{:kohlsRewards}}
		{{else}}
			{{:kohlsRewards}}
		{{/if}}
	</p>
</script>

<script id="tpl-pdp-pricing-kohlsCash" type="text/x-jsrender">
	<p class="pdpprice-kc-row tag-nostyle-fatc-excl-price">
		<a href="{{:kohlsCash.kcExclusions}}" title="details" class="pdpprice-kc-row-link" target="_new" id="" skuId="">
			<img src="{{:~env.resourceRoot}}images/lpf/02-kohl-s-cash-3-d.png" class="pdpprice-kc-row-icon" width="29" height="22" alt="Wallet icon.">
			get {{:~kcEarned(kohlsCash)}} Kohl&#39;s Cash {{:~kcMessage(kohlsCash)}}
			<span class="pdpprice-kc-row-details" >details</span>
		</a>
	</p>
</script>


<script id="tpl-pdp-pricing-kc-section" type="text/x-jsrender">
	{{if $env.rewardsPilot}}
		{{if productV2JsonData.isKohlsCashAvail || productV2JsonData.isLoyaltyKohlsCashAvail}}
			<li>
			{{if productV2JsonData.isKohlsCashAvail}}
				{{include productV2JsonData.price ~env=$env tmpl='tpl-pdp-pricing-kohlsCash' /}}
			{{/if}}
			{{if productV2JsonData.isLoyaltyKohlsCashAvail}}
				<div class="kohlsCash loyaltyV2KohlsCashContainer tag-nostyle-fatc-excl-price">
					{{include productV2JsonData.loyaltyKohlsCash ~env=$env tmpl='tpl-pdp-pricing-loyaltyV2-Earn' /}}
				</div>
			{{/if}}
			</li>
		{{/if}}
	{{else}}
		{{if productV2JsonData.price.kohlsCash && productV2JsonData.price.kohlsCash.kcEarned.min && productV2JsonData.price.kohlsCash.kcEarningPeriod && productV2JsonData.price.kohlsCash.kcEarningPeriod.startDate != null }}
			<li>
				{{include productV2JsonData.price ~env=$env tmpl='tpl-pdp-pricing-kohlsCash' /}}
			</li>
		{{/if}}
	{{/if}}
</script>

<script id="tpl-pdp-pricing-promotion" type="text/x-jsrender">
	{{if ~badges && ~badges.length>0}}
		<li>
		<div id="pdpprice-price-promotion-container">
			<p class="pdpprice-row5">
				{{for ~badges}}
					<span class="pdpprice-row5-text"> {{:title}}</span>
				{{/for}}
			</p>
		</div>
		</li>
	{{/if}}
	{{if promotion && promotion.group !=null }}
	<li>
		<div id="pdpprice-price-promotion-container">
			<p class="pdpprice-row5">
				<span class="pdpprice-row5-text"> {{> promotion.group[0]}}</span>
			</p>
		</div>
	</li>
	{{/if}}

</script>

<script id="tpl-pdp-pricing-ypsavemoreoffer" type="text/x-jsrender">
	{{if ~Offers && ~Offers.length>0}}
		<p class="pdpprice-row3">save even more when you use</p>
		<div id="{{id:'offerCodeContainer'}}">
			{{for ~Offers ~len=~Offers.length}}
				{{if ~len==1 && #data.code && #data.code.length==13}}
					<span class="supc-offer-message">{{:~root.productV2JsonData.staticContents.supcOfferMessage}}</span>
				{{else}}
				<a href="javascript:void(0)" title="details" class="YP_details" data-idx="{{:#getIndex()}}" id="{{:#getIndex()}}" skuId="{{if (~price.skuCode)}}{{:~price.skuCode}}{{/if}}" tag="{{:~yourPriceDate(#data.offerEffectiveDate)}}" data-desc="" data-promo="">
					<p class="pdpprice-row4">
						<span class="pdpprice-row4-text-promocode-savemore">{{>#data.code}}</span>
						<span class="pdpprice-row4-text-stack">
							{{if #data.discountType == 'dollarOff'}}
								(${{>#data.discountAmount}} OFF)
							{{else}}
								({{>#data.discountAmount}}% OFF)
							{{/if}}
								<img src="https://media.kohlsimg.com/is/image/kohls/20200515-dt-price-story-info-icon?scl=1&amp;fmt=png8&amp;n" class="pdpprice-details-icon" width="14" height="14" alt="Promo details."/>
							{{if #data.paymentMessage}}{{>#data.paymentMessage}}{{else}}at checkout{{/if}}
							{{if #getIndex() < (~len-1)}} and {{/if}}
						</span>
					</p>
				</a>
				{{/if}}
			{{/for}}
		</div>
	{{/if}}
</script>

<script id="tpl-pdp-pricing-ypappliedoffers" type="text/x-jsrender">
	{{if ~Offers && ~Offers.length>0}}
		<div id="{{id:'offerCodeContainer'}}">
			{{for ~Offers ~len=~Offers.length}}
				{{if ~len==1 && #data.code && #data.code.length==13}}
					<span class="supc-offer-message">{{:~root.productV2JsonData.staticContents.supcOfferMessage}}</span>
				{{else}}
				<a href="javascript:void(0)" title="details" class="YP_details" data-idx="{{:#getIndex()}}" id="{{:#getIndex()}}" skuId="{{if (~price.skuCode)}}{{:~price.skuCode}}{{/if}}" tag="{{:~yourPriceDate(#data.offerEffectiveDate)}}" data-desc="" data-promo="">
					<p class="pdpprice-row4">
						<span class="pdpprice-row4-text-promocode">{{>#data.code}}</span>
						<span class="pdpprice-row4-text-stack">
							{{if #data.discountType == 'dollarOff'}}
								(${{>#data.discountAmount}} OFF)
							{{else}}
								({{>#data.discountAmount}}% OFF)
							{{/if}}
								<img src="https://media.kohlsimg.com/is/image/kohls/20200515-dt-price-story-info-icon?scl=1&amp;fmt=png8&amp;n" class="pdpprice-details-icon" width="14" height="14" alt="Promo details."/>
							{{if #data.paymentMessage}}{{>#data.paymentMessage}}{{else}}at checkout{{/if}}
							{{if #getIndex() < (~len-1)}} and {{/if}}
						</span>
					</p>
				</a>
				{{/if}}
			{{/for}}
		</div>
	{{/if}}
</script>
<script id="tpl-pdp-pricing-suppressed" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-reg-price">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text" style="{{:~getMainPriceFontSize(mainPriceStr, 10) }}">
			{{:mainPriceStr}}
		</span>
		<span class="pdpprice-row2-main-text-label-use">
			<a href='javascript:void(0)' class='learnMoreLink' onClick='Kjs.pdpDetailsTabPanel.Evt_openTab("#tabSpecialPricing","specialPricing");'>Learn More</a>
		</span>
	</p>
</script>
<script id="tpl-pdp-pricing-yourprice" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-sale-price pdpprice-row1-sale-price-striked">
			{{:~formatRangePriceNew(salePrice)}} {{:~getSalePriceStatusLabel(salePriceStatus)}}
		</span>
		<span class="pdpprice-row1-reg-price pdpprice-row1-reg-price-striked">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text pdpprice-row2-main-text-purple" style="{{:~getMainPriceFontSize(mainPriceStr, 12) }}">
			{{:mainPriceStr}}
		</span>
		<span class="pdpprice-row2-main-text-label-use">when you use</span>
	</p>
</script>

<script id="tpl-pdp-pricing-SalePrice" type="text/x-jsrender">
	<p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
		<span class="pdpprice-row1-reg-price pdpprice-row1-reg-price-striked">
			{{:~formatRangePriceNew(regularPrice)}} {{:~getRegularPriceLabel(regularPriceType)}}
		</span>
	</p>
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text {{if salePriceStatus && salePriceStatus.toLowerCase() !== 'mixed'}}pdpprice-row2-main-text-red{{/if}}" style="{{:~getMainPriceFontSize(mainPriceStr, salePriceStatus.length) }}">
			{{:mainPriceStr}}
		</span>
		<span class="pdpprice-row2-main-text-label-sale">{{:~getSalePriceStatusLabel(salePriceStatus, #data.yourPriceInfo)}}</span>
	</p>
</script>
<script id="tpl-pdp-pricing-regularprice" type="text/x-jsrender">
	{{!-- <p class="pdpprice-row1 tag-nostyle-fatc-excl-price">
	</p> --}}
	<p class="pdpprice-row2">
		<span class="pdpprice-row2-main-text" style="{{:~getMainPriceFontSize(mainPriceStr, 0) }}">
			{{:mainPriceStr}}
		</span>
	</p>
</script>
<script id="tpl-pdp-color-swatches" type="text/x-jsrender">
		<div class="pdp-product-size_lbls m-top10">
			{{if variants.preSelectedColor && variants.preSelectedColor != 'NO COLOR'}}
				<p class="swatch-label"> Color:
					<span class="sel-color-swatch">{{:variants.preSelectedColor}}</span>
				</p>
			{{/if}}
		</div>
		<div class="pdp-color-swatches-info" id="{{id:'pdpColorSwatch'}}">
			{{for ~colorsObj(variants, ~root.productV2JsonData.webID) ~environment=~root.productV2JsonData.environment}}
				{{if ~environment=="staging" && colorFlag}}
					<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
						<div class="pdp-product-swatch {{:~root.productV2JsonData.preSelectedColor == color ? 'active':''}}">
							<a data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="color-swatch no-inventory" href="javascript:void(0);" style="" data-bg="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
								<span style="opacity:0"> out of stock</span>
							</a>
						</div>
					</div>
				{{else colorFlag===false}}
					<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
						<div class="pdp-product-swatch {{:~root.productV2JsonData.preSelectedColor == color ? 'active':''}}">
							<a data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="color-swatch" href="javascript:void(0);" style="" data-bg="background:url({{:backgroundURL}}) no-repeat" rel="{{:largeImgURL}}">{{if oosFlag}}<img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>{{/if}}
							</a>
						</div>
					</div>
				{{else colorFlag===true}}
					<div class="pdp-product-swatch-outer {{:~root.productV2JsonData.preSelectedColor == color ? 'color-container':''}}">
						<div class="pdp-product-swatch color-unavailable" title="out of stock">
							<a data-skuColor="{{:color}}" alt="{{:color}}" title="{{:color}}" class="color-swatch" href="javascript:void(0);" style="" data-bg="background:url({{:backgroundURL}}) no-repeat;pointer-events: none;" rel="{{:largeImgURL}}"><img alt="out of stock" src='{{:~root.$env.resourceRoot}}images/swatch_unavail.gif'/>
								<span style="opacity:0"> out of stock</span>
							</a>
						</div>
					</div>
				{{/if}}
			{{/for}}
		</div>
</script>
<script id="tpl-pdp-sizeOnlyDropDown" type="text/x-jsrender">
	<label  for="size-dropdown" class="swatch-label m-top10 swatch-label-display">
		<img alt="swatch-label-warning img" class="swatch-label-warning" src="{{:~root.$env.resourceRoot}}images/warning-glyph-black.svg"/>
		{{:~root.$env.enableVgcRedesign && ~root.productV2JsonData.isGiftCard?'Amount':~root.productV2JsonData.staticContents.browseStaticSize}}
		{{if variants.displaySizeGuide && ~root.$env.Fit != true && ~root.$env.fitPredictor != true && ~root.$env.trueFit != true}}
			<div class="sizeguidewrapper">
				<a href="javascript:void(0);" id="{{id:true 'sizeguide'}}" class="sizeguide" title="{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}">{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}</a>
			</div>
		{{/if}}
	</label>
	<select id="{{id:true 'size-dropdown'}}"  aria-expanded="false">
		<option data-skusize="false" class="select-size" selected="selected" disabled = "disabled" hidden="hidden">{{:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}</option>
		{{for ~sizeObj(variants) }}
			{{if ~root.$env.environment=="staging" && sizeFlag}}
				<option class="no-inventory" value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}}</option>
			{{else sizeFlag===false}}
				<option {{if oosFlag}}class="size-dropdown-oos" {{/if}}{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}selected='selected'{{/if}} value='{{>size}}' data-skusize='{{>size}}'>{{>size}}{{if oosFlag}} - OUT OF STOCK {{/if}}</option>
			{{else sizeFlag===true}}
				<option value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}} - OUT OF STOCK</option></a>
			{{/if}}
		{{/for}}
	</select>
</script>
<script id="tpl-pdp-size" type="text/x-jsrender">
	{{if variants.sizeList  ~sizeLength = ~sizLength(variants)}}
		<label {{if ~sizeLength > 12}} for="size-dropdown" {{/if}} class="swatch-label m-top10 swatch-label-display">
				<img alt="swatch-label-warning img" class="swatch-label-warning" src="{{:~root.$env.resourceRoot}}images/warning-glyph-black.svg"/>
			{{:~root.$env.enableVgcRedesign && productType && productType.isVGC?'Amount':~root.productV2JsonData.staticContents.browseStaticSize}}{{:~sizeLength > 12?'':': '}}
			<span class="size-swatch">{{:~sizeLength > 12?'':variants.preSelectedSize && variants.preSelectedSize != "NO SIZE"?variants.preSelectedSize:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}</span>
			{{if variants.displaySizeGuide != undefined && variants.displaySizeGuide && ~root.$env.Fit != true && ~root.$env.fitPredictor != true && ~root.$env.trueFit != true}}
				<div class="sizeguidewrapper">
					<a href="javascript:void(0);" id="{{id:true 'sizeguide'}}" class="sizeguide" title="{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}">{{:~root.productV2JsonData.staticContents.pdpSizeGuide}}</a>
				</div>
			{{/if}}
		</label>
		{{if ~sizeLength <=12}}
			<div class="pdp-waist-size_info clearfix" id="{{id:'pdpSizeSwatch'}}">
				{{for ~sizeObj(variants)}}
					{{if ~root.$env.environment=="staging" && sizeFlag}}
						<a class="pdp-size-swatch size-unavailable no-inventory" title="{{>size}}" data-size='{{>size}}'  href="javascript:void(0);">
							{{>size}}
						</a>
					{{else sizeFlag===false}}
						<a class="pdp-size-swatch {{if oosFlag}}size-oos {{/if}}{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}active{{/if}}" title="{{>size}}" data-size='{{>size}}' href="javascript:void(0);">{{>size}}</a>
					{{else sizeFlag===true}}
						<a class="pdp-size-swatch size-unavailable" title="{{>size}}" data-size='{{>size}}' href="javascript:void(0);" style='pointer-events: none;'>{{>size}}</a>
					{{/if}}
				{{/for}}
			</div>
		{{else}}
			<select id="{{id:true 'size-dropdown'}}"  aria-expanded="false">
				<option data-skusize="false" class="select-size" selected="selected" disabled = "disabled" hidden="hidden">{{:~root.productV2JsonData.isGiftCard?'Please Choose an Amount':'Please Choose a Size'}}</option>
				{{for ~sizeObj(variants) }}
					{{if ~root.$env.environment=="staging" && sizeFlag}}
						<option class="no-inventory" value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}}</option>
					{{else sizeFlag===false}}
						<option {{if oosFlag}}class="size-dropdown-oos" {{/if}}{{if (~root.productV2JsonData.variants && ~root.productV2JsonData.variants.preSelectedSize && size == ~root.productV2JsonData.variants.preSelectedSize)}}selected='selected'{{/if}} value='{{>size}}' data-skusize='{{>size}}'>{{>size}}{{if oosFlag}} - OUT OF STOCK {{/if}}</option>
					{{else sizeFlag===true}}
						<option value='{{>size}}' data-skusize='{{>size}}' disabled="disabled">{{>size}}  - OUT OF STOCK </option></a>
					{{/if}}
				{{/for}}
			</select>
		{{/if}}
	{{/if}}
</script>
<script id="tpl_PDP_VGC_tmpl" type="text/x-jsrender">
	<form name="vgc create form" class="vgc-create-form" method="post" id="{{id:'vgcForm'}}">
		<p>{{:productV2JsonData.staticContents.vgcDisclaimerText}}</p>
		<label for="recipient-name" class="vgc-label firstnameField">{{:productV2JsonData.staticContents.vgcRecipientNameLabel}}</label>
		<div class="vgc-error hide"><a title="Please enter a message">*Name must be {{:$env.nameFieldMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a recipient name">*Please enter a recipient name.</a></div>
    	<input name="recipient-name" title="Recipient name" alt="Recipient name" value="" class="vgc-input input-firstnameField" maxlength={{:$env.nameFieldMaxVGC+1}} type="text" id="muteCheck"/>
		<label for="recipient-email" class="vgc-label label-emailField">{{:productV2JsonData.staticContents.vgcRecipientEmailLabel}}</label>
		<div class="vgc-error vgc-invalidEmail hide"><a title="Please enter a recipient email">*Please enter a valid recipient email address.</a></div>
		<div class="vgc-error hide"><a title="Please enter a recipient email">*Please enter a recipient email.</a></div>
    	<input name="recipient-email" title="name@mail.com" alt="Recipient Name" value="" class="vgc-input recipient-emailField" type="text" id="muteCheck" />
		<label for="sender-name" class="vgc-label firstnameField">{{:productV2JsonData.staticContents.vgcSenderNameLabel}}</label>
		<div class="vgc-error hide"><a title="Please enter a message">*Name must be {{:$env.nameFieldMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a name">*Please enter a name.</a></div>
		<input name="sender-name" value="" title="Sender name" alt="Sender name" class="vgc-input input-firstnameField" maxlength={{:$env.nameFieldMaxVGC+1}} type="text" id="muteCheck" />
		<label for="sender-email" class="vgc-label label-emailField">{{:productV2JsonData.staticContents.vgcSenderEmailLabel}}</label>
		<div class="vgc-error vgc-invalidEmail hide"><a title="Please enter an email address">*Please enter a valid email address.</a></div>
		<div class="vgc-error hide"><a title="Please enter an email address">*Please enter an email address.</a></div>
		<input name="sender-email" value=""  title="Sender email" alt="Sender email" class="vgc-input from-emailField" type="text" id="muteCheck"/><br>
		<label for="gift-message" class="vgc-label message">{{:productV2JsonData.staticContents.vgcGiftMessageLabel}}<span class="vgc-maxmessage">&nbsp;({{:$env.characterMaxVGC}} characters max)</span></label>
		<div class="vgc-error hide"><a title="Please enter a message">*Message must be {{:$env.characterMaxVGC}} characters or fewer.</a></div>
		<div class="vgc-error hide"><a title="Please enter a message">*Please enter a message.</a></div>
		<textarea name="gift-message" maxlength={{:$env.characterMaxVGC+1}} class="vgc-input vgc-message" id="muteCheck">{{:productV2JsonData.staticContents.vgcGiftMessageText}}</textarea>
		<span class="egiftCard-deliverytime-message">{{:productV2JsonData.staticContents.newvgcFulfillmentTypeText}}</span>
	</form>
</script>

<script type="text/x-jsrender" id="pdp_shipment_info_boss">
			<!--Checking shipment section -->
			<div class="pdp-product-addtobag" id="{{id:'fullfillmentContainer'}}">
				<div class="fulfillment-container">
					<form id="shipment-selection">
						{{if (bopus && bopus != 'false'||  boss && boss != 'false' || ship && ship != 'false' )}}
							<span class="fulfillment-availability-text">availability:</span>
						{{/if}}
						<div class="pdp-fullfillment-message" style="display: none;">
							<span class="pdp-fullfillment-error-icon"></span>
							<span class="pdp-fullfillment-content">
							{{if bopus}}
								{{if bopus.isdisabled }}
									{{:staticContents.tooltipSelectCheckOtherStoresMsg}}
								{{else}}
									{{:staticContents.tooltipSelectPickupOptionMsg}}
								{{/if}}
							{{/if}}
							</span>
						</div>
						{{if bopus && bopus != 'false'}}
							<div class="bossAlign">
								<input type="radio" id="bopusTab" name="radio-group" value="bopus" title="FREE Store Pickup Today" alt="FREE Store Pickup Today" data-storeId="{{:bopus.storeId}}" data-postalCode="{{:bopus.postalCode}}" {{if bopus.isdisabled}} disabled {{/if}} {{if bopus.radioButtonChecked}} checked {{/if}}/>
								{{if bopus.isPickupNotAvailable}}
									<div class="bopus-pickup-notavailable">{{:bopus.label}}</div>
								{{else}}
									<label for="bopusTab" class="bopusTab{{if bopus.isdisabled}} option-disabled{{/if}}"> {{:bopus.label}}
										<div>
											{{if bopus.showStar}}<img src="{{:~env.resourceRoot}}images/myStore.png" alt="MyStore" class="myStoreStar{{if bopus.isdisabled}} option-disabled-mystore{{/if}}" />{{/if}}
											<p class="preferedStore {{if bopus.isdisabled}}option-disabled {{/if}}">{{:bopus.storeName}} {{if bopus.distanceMiles}}<span class="ship-to-store-distance">{{:bopus.distanceMiles}} mi</span>{{/if}}</p>
										</div>
									</label>
								{{/if}}
								<div class="ship-to-store-img {{if bopus.isdisabled}} option-disabled {{/if}}">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
										<path  class="ship-to-store-svg" fill="#444" fill-rule="nonzero" d="M16.875 3.125c1.036 0 1.875.84 1.875 1.875v10.625h.63c.311 0 .569.23.613.532l.007.093a.626.626 0 0 1-.62.625H.62a.626.626 0 0 1 0-1.25h.63V5c0-1.036.84-1.875 1.875-1.875h13.75zm0 1.25H3.125A.625.625 0 0 0 2.5 5v10.625h1.25v-5.313c0-.48.362-.877.828-.93l.11-.007h10.625c.48 0 .877.362.93.828l.007.11v5.312h1.25V5a.625.625 0 0 0-.625-.625zM7.5 10.625H5v5h2.5v-5zm3.75 0h-2.5v5h2.5v-5zm3.75 0h-2.5v5H15v-5zm-.626-4.375c.315 0 .575.23.62.532l.006.093a.63.63 0 0 1-.626.625H5.626A.624.624 0 0 1 5 6.875a.63.63 0 0 1 .626-.625h8.748z"/>
									</svg>
								</div>
							</div>
						{{/if}}
						<!-- Ship to store addition -->
						{{if boss && boss != 'false'}}
							<div class="bossAlign">
								<input type="radio" id="bossTab" name="radio-group" value="boss" title="FREE Ship to Store" alt="FREE Ship to Store" data-storeId="{{:boss.storeId}}" data-postalCode="{{:boss.postalCode}}" {{if boss.isdisabled}} disabled {{/if}} {{if boss.radioButtonChecked}} checked {{/if}}/>
								<label for="bossTab" class="bopusTab{{if boss.isdisabled}} option-disabled{{/if}}">{{:boss.label}}
									<div>
										{{if boss.showStar}}<img src="{{:~env.resourceRoot}}images/myStore.png" alt="MyStore" class="myStoreStar{{if boss.isdisabled}} option-disabled-mystore{{/if}}" />{{/if}}
										<p class="preferedStore {{if boss.isdisabled}}option-disabled {{/if}}">{{:boss.storeName}} {{if boss.distanceMiles}}<span class="ship-to-store-distance">{{:boss.distanceMiles}} mi</span>{{/if}}</p>
									</div>
								</label>
								<div class="ship-to-store-img {{if boss.isdisabled}} option-disabled {{/if}}">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
										<path  class="ship-to-store-svg" fill="#444" fill-rule="nonzero" d="M16.875 3.125c1.036 0 1.875.84 1.875 1.875v10.625h.63c.311 0 .569.23.613.532l.007.093a.626.626 0 0 1-.62.625H.62a.626.626 0 0 1 0-1.25h.63V5c0-1.036.84-1.875 1.875-1.875h13.75zm0 1.25H3.125A.625.625 0 0 0 2.5 5v10.625h1.25v-5.313c0-.48.362-.877.828-.93l.11-.007h10.625c.48 0 .877.362.93.828l.007.11v5.312h1.25V5a.625.625 0 0 0-.625-.625zM7.5 10.625H5v5h2.5v-5zm3.75 0h-2.5v5h2.5v-5zm3.75 0h-2.5v5H15v-5zm-.626-4.375c.315 0 .575.23.62.532l.006.093a.63.63 0 0 1-.626.625H5.626A.624.624 0 0 1 5 6.875a.63.63 0 0 1 .626-.625h8.748z"/>
									</svg>
								</div>
							</div>
						{{/if}}
						{{if ship && ship != 'false'}}
							<div class="bossAlign">
								<input type="radio" id="shipTab" name="radio-group" value="ship" title="Ship to me" alt="Ship to Me" {{if ship.isdisabled}} disabled {{/if}} {{if ship.radioButtonChecked}} checked {{/if}}/>
								{{if ship.shipNotAvailable}}
									<div class="ship-not-available-block">
										<span class="ship-not-available-tab">{{:ship.label}}</span>
										<span class ="ship-unavailable-text">{{:ship.unavialableLable}}</span>
									</div>
								{{else}}
									<label for="shipTab" class="bopusTab{{if ship.isdisabled}} option-disabled{{/if}}"><span class="changeTextBoss"></span> {{:ship.label}}
										<div>
											{{if ~env.enableVgcRedesign && ~productType && ~productType.isVGC }}
												<p class="preferedStore shipStoreOnlyShip vgcShipmentText">{{:ship.vgcText}}</p>
											{{else}}
												<p class="preferedStore shipStoreOnlyShip">{{:ship.text}}</p>
											{{/if}}
										</div>
									</label>
								{{/if}}
								<div class="ship-to-me-img {{if ship.isdisabled}} option-disabled {{/if}}">
									<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
										<path class="ship-to-me-svg" fill="#666" fill-rule="evenodd" d="M3.401 16.5a2.999 2.999 0 0 1 5.198 0H15V5.47c0-.43-.024-.552-.091-.676a.478.478 0 0 0-.205-.201c-.13-.069-.257-.093-.698-.093H3A.75.75 0 0 1 3 3h11.006c.665 0 1.024.068 1.398.266.353.186.634.464.824.813.202.373.272.731.272 1.392V7.5h4.077c.723 0 1.108.074 1.51.289.374.2.673.5.874.875.215.401.289.786.289 1.509V16.5h.107c.355 0 .643.336.643.75s-.288.75-.643.75H21a3 3 0 0 1-6 0H9a3 3 0 0 1-6 0H1.5a.837.837 0 0 1-.4-.092.626.626 0 0 1-.258-.258.837.837 0 0 1-.092-.4c0-.154.032-.289.092-.4a.626.626 0 0 1 .258-.258.837.837 0 0 1 .4-.092v-2.25c0-.154.032-.289.092-.4a.626.626 0 0 1 .258-.258.837.837 0 0 1 .4-.092c.154 0 .289.032.4.092.111.06.199.147.258.258.06.111.092.246.092.4v2.25h.401zM16.5 9v6.401a2.999 2.999 0 0 1 4.099 1.099h1.151v-6.327c0-.496-.03-.648-.112-.802a.613.613 0 0 0-.26-.26c-.153-.082-.305-.111-.8-.111H16.5zM.75 6H6a.75.75 0 0 1 0 1.5H.75a.75.75 0 0 1 0-1.5zm1.5 2.25H6a.75.75 0 0 1 0 1.5H2.25a.75.75 0 0 1 0-1.5zm1.5 2.25H6A.75.75 0 1 1 6 12H3.75a.75.75 0 1 1 0-1.5zm14.25.75a.75.75 0 1 1 0-1.5h2.175c.456 0 .825.37.825.825v2.175a.75.75 0 1 1-1.5 0v-1.5H18zM6 19.5a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3zm12 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z"/>
									</svg>
								</div>
							</div>
						{{/if}}
						<div style="clear:both"></div>
						{{if boss && boss != 'false'}}
							<div class="addtobag-storepickup-selectstore showFindStorelocatoroverlay bossStore" style="display:block">
								{{if boss.showCheckOtherStoresLink}}<a href="javascript:void(0);" class="ce-fisCOS s-pickupstore" data-postalCode="{{:boss.postalCode}}" title="Select a store">{{:staticContents.bossCheckOtherStores}}</a>{{/if}}
							</div>
						{{else bopus && bopus != 'false'}}
							<div class="addtobag-storepickup-selectstore showFindStorelocatoroverlay bossStore" style="display:block">
								{{if bopus.showCheckOtherStoresLink}}<a href="javascript:void(0);" class="ce-fisCOS s-pickupstore" data-postalCode="{{:bopus.postalCode}}" title="Select a store">{{:staticContents.bossCheckOtherStores}}</a>{{/if}}
							</div>
						{{/if}}
						{{if oos}}
							<div class="bossAlign">
								<label class="oos-sku"> {{:oos.label}}
								</label>
							</div>
						{{/if}}
				</form>
			</div>
		</div>
</script>
<script type="text/x-jsrender" id="mosaic_smartcard_popup_template">
	<div class="smartcart-modal">
		<div class="sc-message"><p class="sc-added">{{:staticContents.savingLauncherTitle }}</p></div>
		<!-- Start: sc-container -->
		<div class="sc-container">
			<!-- Start: sc-rpanel -->
			<div class="sc-lpanel">
				<div class="sc-image"><img src="{{:modeldata.cartItems[0].itemProperties.image.url.replace('wid=180', 'wid=250').replace('hei=180', 'hei=250') }}" alt="{{~convertHTML(modeldata.cartItems[0].itemProperties.productTitle) }}" /></div>
				<h1 class="sc-title">{{:modeldata.cartItems[0].itemProperties.productTitle }}</h1>
				<!-- Pricing -->
				<div class="mainprice-container">
					{{if price.salePriceStatus != null && price.salePriceStatus.toLowerCase() == 'sale' }}
						<div class="main-pricelabel">
							<div class="main-pricelabel red-font">{{:price.salePriceStatus }}</div>
							<div class="main-price red-font">
								{{if price.salePrice && price.salePrice.minPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.salePrice.minPrice) }}
								{{/if}}
								{{if price.salePrice && price.salePrice.maxPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.salePrice.maxPrice) }}
								{{/if}}
							</div>
						</div>
						<div class="regorg-small">{{:price.regularPriceType}}&nbsp;
							{{if price.regularPrice && price.regularPrice.minPrice}}
								${{:Kjs.pdpUtils.formatPrice(price.regularPrice.minPrice) }}
							{{/if}}
							{{if price.regularPrice && price.regularPrice.maxPrice}}
								${{:Kjs.pdpUtils.formatPrice(price.regularPrice.maxPrice) }}
							{{/if}}
						</div>
					{{else}}
						<div class="main-pricelabel">
							<div class="main-pricelabel">{{:price.regularPriceType}}</div>
							<div class="main-price">
								{{if price.regularPrice && price.regularPrice.minPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.regularPrice.minPrice) }}
								{{/if}}
								{{if price.regularPrice && price.regularPrice.maxPrice}}
									${{:Kjs.pdpUtils.formatPrice(price.regularPrice.maxPrice) }}
								{{/if}}
							</div>
						</div>
					{{/if}}
				</div>
				<!-- Your Pricing -->
				{{if $env.YP_enable && price.yourPriceInfo && price.yourPriceInfo.appliedOffers && price.yourPriceInfo.appliedOffers.length }}
					<div class="your-Price"></div>
				{{/if}}
			</div>
			<!-- End: sc-lpanel -->
			<!-- Start: sc-rpanel -->
			<div class="sc-rpanel">
				<div class="sc-option-icon"></div>
				<div class="sc-option-msg">{{:staticContents.savingLauncherSubTitle }}</div>
				<div class="sc-options">
					<div id="change-to-ship" class="sc-optcont selected">
						<div class="sc-optcont-left">
							{{:staticContents.savingLauncherShipPromoMessage.replace('{0}',modeldata.incentiveDetails.incentiveThreshold) }}
						</div>
						<div class="sc-optcont-right"></div>
					</div>
					<div id="change-to-bopus" class="sc-optcont">
						<div class="sc-optcont-left">
							{{:staticContents.savingLauncherPickUpPromoMessage.replace('{0}',modeldata.incentiveDetails.storeDetails.storeName) }}
						</div>
						<div class="sc-optcont-right"></div>
					</div>
				</div>
			</div>
			<!-- End: sc-rpanel -->
		</div>
		<!-- End: sc-container -->
	</div>
 </script>
 <!-- Notify Me Changes -->
<script id="pdp_notify_me" type="text/x-jsrender">
	{{if notifyMeData.showNotifyButton || notifyMeData.status=="NOT_FOUND"}}
		<div class="clear notify-line">
			<p class="notify-text-bold">{{:staticContents.notifyMeItemOutOfStock}}</p>
			<p class="notify-msg">{{:~convertHTML(staticContents.notifyMeWewillSendYouTextmessage)}}</p>
			<span><input type="text" id="{{id:true 'inputnotifyme'}}" class="input-field add_ph_mask_filed" maxlength="13" title="Area code and number" alt="Area code and number" placeholder="Area code and number" name="notify_phone"></span>
			<span><input type="button" maxlength="13" class="notify-btn" id="notify-me" name="pdp-notify" title="Notify me" alt="Notify me" value="NOTIFY ME"></span>
			<p class="notify-msg-phone">{{:staticContents.notifyMeYourPhoneNumberUseThisItemOnly}}</p>
		</div>
	{{else}}
		{{if (notifyMeData.status=="USER_ALREADY_SUBSCRIBED") && notifyMeData.enabled && !notifyMeData.verified}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeItemOutOfStock}}</p>
				<p class="notify-msg"><img src="/snb/media/images/ic-warning.png" alt="notifyMe-icon" class="notifyme-icon"/>{{:staticContents.notifyMeVerifyYourMobileDeviceInstock}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{else notifyMeData.status=="SUCCESS" && notifyMeData.enabled}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeThanksYouAllset}}</p>
				<p class="notify-msg">{{:staticContents.notifyMeWillsendTextNotification}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{else notifyMeData.status=="USER_ALREADY_SUBSCRIBED" && notifyMeData.enabled && notifyMeData.verified}}
			<div class="clear notify-line">
				<p class="notify-text-bold">{{:staticContents.notifyMeAlreadySignup}}</p>
				<p class="notify-msg">{{:staticContents.notifyMeWillsendTextNotification}}</p>
				{{if notifyMeData.showRemoveButton}}<p class="remove-me"><a class="cancel-subscription" href="#">{{:staticContents.notifyMeCancelNotification}}</a></p>{{/if}}
			</div>
		{{/if}}
	{{/if}}
</script>
<!-- Notify Me Changes Ends -->
<script id="offer_popup_template" type="text/x-jsrender">
<div class="popup-container">
	<div class="top-section kas-top-section">
		<div class="popup-top-left"></div>
		<div class="popup-top-right">
			<div class="offer-active">
				<a href="javascript:void(0)" class="close-popup kas-close-popup" alt="Close popup" title="Close Popup"></a>
				<p class='logo-print'><img src="{{:$env.resourceRoot}}images/wallet-images/print-kohls-logo.png" alt="Kohl's logo" /></p>
				<h3>{{:description}}</h3>
				<div class="promo-code-cont">
					{{if code.length > 2}}
    					<span class="promo-code-label kas-promo-code-label">PROMO CODE: </span>
    					<span class="promo-code-value kas-promo-code-value">{{:code}}</span>
    				{{/if}}
    				<p class="offer-start-end kas-offer-start-end">{{:offerEffectiveDate.endFullDate}}</p>
    			</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="barcode-section">
			<div class="barcode kas-barcode" id="popupBarcode"></div>
			<p><span class="barcode-number kas-barcode-number">{{:barCode}}</span>
            </p>
		</div>
	<div class="mid-section kas-mid-section">
		<div class="offer-validity-container">
		<p class="offer-start-end-print kas-offer-start-end">{{:offerEffectiveDate.endDate}}</p>
		</div>
		<div class="promocode-container-print kas-promocode-container-print">
			{{if code.length > 2}}
				<p class="promo-code">PROMO CODE: <span class="promo-code-lable">{{:code}}<span></p>
			{{/if}}
		</div>
		<p class="coupon-valid-txt kas-coupon-valid-txt normal-text-transform">{{:termsConditions}}</p>
	</div>
	<div class="bottom-section kas-bottom-section">
		<ul>
			<li class="print-link kas-print-link"><a href="javascript:void(0)" alt="Print" title="print" data-printid="0" id="{{:idx}}" skuId="{{:skuId}}" class="go-to-printPopup ce-focusPP"></a></li>
		</ul>
	</div>
</div>
</script>
<script id="tpl-pdpDetailsTab" type="text/x-jsrender">
    {{if productV2JsonData.isMobile}}
        {{for  tmpl='tpl-pdpDetailsTab-mobile' /}}
    {{else}}
        {{for  tmpl='tpl-pdpDetailsTab-desktop' /}}
    {{/if}}
</script>

<script id="tpl-pdpDetailsTab-desktop" type="text/x-jsrender">
<div id="{{id:'root'}}" class="PDP_prdDetails_comp">
    <div class="pdp-tab-container-wrapper">
    <div id="{{id:'pdpTabContainer'}}" class="tab-container">
        {{if (productV2JsonData.productDetails && productV2JsonData.productDetails != '') }}
            <button id="tabProductDetails" class="tablinks {{:(productV2JsonData.defaultAccordion=='productDetails')?'active':''}}" tabContentId="productDetailsTabContent">{{:~getLowercaseText('PRODUCT DETAILS')}}</button>
        {{/if}}
        {{if (productV2JsonData.sizing && productV2JsonData.sizing != '') }}
            <button id='tabSizing' class="tablinks {{:(productV2JsonData.defaultAccordion=='sizing')?'active':''}}" tabContentId="sizingTabContent">{{:~getLowercaseText('SIZING')}}</button>
        {{/if}}
        {{if (productV2JsonData.ingredients && productV2JsonData.ingredients != '') }}
            <button class="tablinks {{:(productV2JsonData.defaultAccordion=='ingredients')?'active':''}}" tabContentId="ingredientsTabContent">{{:~getLowercaseText('INGREDIENTS')}}</button>
        {{/if}}
        {{if (productV2JsonData.careInstructions && productV2JsonData.careInstructions != '') }}
            <button class="tablinks {{:(productV2JsonData.defaultAccordion=='careInstructions')?'active':''}}" tabContentId="careInstructionsTabContent">{{:~getLowercaseText('CARE INSTRUCTIONS')}}</button>
        {{/if}}
        {{if (productV2JsonData.shippingAndReturn && productV2JsonData.shippingAndReturn != '') }}
            <button id='tabShippingAndReturns' class="tablinks {{:(productV2JsonData.defaultAccordion=='shippingAndReturn')?'active':''}}" tabContentId="shippingAndReturnTabContent">{{:~getLowercaseText('SHIPPING & RETURNS')}}</button>
        {{/if}}
        {{if (productV2JsonData.relatedInformation && productV2JsonData.relatedInformation != '') }}
            <button class="tablinks {{:(productV2JsonData.defaultAccordion=='relatedInformation')?'active':''}}" tabContentId="relatedInformationTabContent">{{:~getLowercaseText('RELATED INFORMATION')}}</button>
        {{/if}}
        {{if (productV2JsonData.gwp && productV2JsonData.gwp != '') }}
            <button id="tabgwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='gwp')?'active':''}}" tabContentId="gwpTabContent">{{:~getLowercaseText('Gift with Purchase')}}</button>
        {{/if}}
        {{if (productV2JsonData.pwp && productV2JsonData.pwp != undefined && productV2JsonData.pwp != '') }}
            <button id="tabpwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='pwp')?'active':''}}" tabContentId="pwpTabContent">Special Savings Item</button>
        {{/if}}
        {{if (productV2JsonData.rebate && productV2JsonData.rebate != undefined && productV2JsonData.rebate.longDescription != null) }}
            <button id='tabRebate' class="tablinks {{:(productV2JsonData.defaultAccordion=='rebate')?'active':''}}" tabContentId="rebateTabContent">{{:~getLowercaseText('REBATES')}}</button>
        {{/if}}
        {{if (productV2JsonData.exclusions && productV2JsonData.exclusions.longDescription != null) }}
            <button class="tablinks {{:(productV2JsonData.defaultAccordion=='exclusions')?'active':''}}" tabContentId="exclusionsTabContent">{{:~getLowercaseText('EXCLUSIONS')}}</button>
        {{/if}}
        {{if (productV2JsonData.pricingDetails && productV2JsonData.pricingDetails != '') }}
            <button id='tabSpecialPricing' class="tablinks {{:(productV2JsonData.defaultAccordion=='pricingDetails')?'active':''}}" tabContentId="pricingDetailsTabContent">{{:~getLowercaseText('SPECIAL PRICING')}}</button>
        {{/if}}
        {{if (productV2JsonData.vendorDetails && productV2JsonData.vendorDetails != '') }}
            <button id='tabVendorDetails' class="tablinks {{:(productV2JsonData.defaultAccordion=='vendorDetails')?'active':''}}" tabContentId="vendorDetailsTabContent">{{:~getLowercaseText('VENDOR DETAILS')}}</button>
        {{/if}}
    </div>
	<div class="separator"></div>
</div>
    {{if (productV2JsonData.productDetails && productV2JsonData.productDetails != '') }}
        <div id='productDetailsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='productDetails')?'block':'none'}}">{{:productV2JsonData.productDetails}}</div>
    {{/if}}
    {{if (productV2JsonData.sizing && productV2JsonData.sizing != '') }}
        <div id='sizingTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='sizing')?'block':'none'}}">{{:productV2JsonData.sizing}}</div>
    {{/if}}
    {{if (productV2JsonData.ingredients && productV2JsonData.ingredients != '') }}
        <div id='ingredientsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='ingredients')?'block':'none'}}">{{:productV2JsonData.ingredients}}</div>
    {{/if}}
    {{if (productV2JsonData.careInstructions && productV2JsonData.careInstructions != '') }}
        <div id='careInstructionsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='careInstructions')?'block':'none'}}">{{:productV2JsonData.careInstructions}}</div>
    {{/if}}
    {{if (productV2JsonData.shippingAndReturn && productV2JsonData.shippingAndReturn != '') }}
        <div id='shippingAndReturnTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='shippingAndReturn')?'block':'none'}}">{{:productV2JsonData.shippingAndReturn}}</div>
    {{/if}}
    {{if (productV2JsonData.relatedInformation && productV2JsonData.relatedInformation != '') }}
        <div id='relatedInformationTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='relatedInformation')?'block':'none'}}">{{:productV2JsonData.relatedInformation}}</div>
    {{/if}}
    {{if (productV2JsonData.gwp && productV2JsonData.gwp != '') }}
        <div id='gwpTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='gwp')?'block':'none'}}">
        {{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
            {{for tmpl="tpl-pdpDetailsTab-gwp" /}}
        {{/if}}
        </div>
    {{/if}}
    {{if (productV2JsonData.pwp && productV2JsonData.pwp != undefined && productV2JsonData.pwp != '') }}
        <div id='pwpTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='pwp')?'block':'none'}}">
        {{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
            {{for tmpl="tpl-pdpDetailsTab-gwp" /}}
        {{/if}}
        </div>
    {{/if}}
    {{if (productV2JsonData.rebate && productV2JsonData.rebate != undefined && productV2JsonData.rebate.longDescription != null) }}
        <div id='rebateTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='rebate')?'block':'none'}}">{{:productV2JsonData.rebate.longDescription}}</div>
    {{/if}}
    {{if (productV2JsonData.exclusions && productV2JsonData.exclusions.longDescription != null) }}
        <div id='exclusionsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='exclusions')?'block':'none'}}">{{:productV2JsonData.exclusions.longDescription}}</div>
    {{/if}}
    {{if (productV2JsonData.pricingDetails && productV2JsonData.pricingDetails != '') }}
        <div id='pricingDetailsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='pricingDetails')?'block':'none'}}">{{:productV2JsonData.pricingDetails}}</div>
    {{/if}}
    {{if (productV2JsonData.vendorDetails && productV2JsonData.vendorDetails != '') }}
        <div id='vendorDetailsTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='vendorDetails')?'block':'none'}}">{{:productV2JsonData.vendorDetails}}</div>
    {{/if}}
    <div class="seemoreParentDiv">
        <span class="seemore" id="{{id:'seeMore'}}"><img src="{{v:$env.resourceRoot}}images/pdp/rectangle.png"/> <span>See more</span></span>
    </div>
</div>
</script>

<script id="tpl-pdpDetailsTab-mobile" type="text/x-jsrender">
<div id="{{id:'root'}}" class="PDP_prdDetails_comp">
    <div class="pdp-tab-container-wrapper">
    <div id="{{id:'pdpTabContainer'}}" class="tab-container">
        {{if (productV2JsonData.productDetails && productV2JsonData.productDetails != '') }}
            {{include ~id='tabProductDetails' ~contentId= 'productDetailsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='productDetails') ~tabName = ~getLowercaseText('PRODUCT DETAILS') ~details=productV2JsonData.productDetails tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.sizing && productV2JsonData.sizing != '') }}
            {{include ~id='tabSizing' ~contentId= 'sizingTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='sizing') ~tabName = ~getLowercaseText('SIZING') ~details=productV2JsonData.sizing tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.ingredients && productV2JsonData.ingredients != '') }}
            {{include ~id='tablinks' ~contentId= 'ingredientsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='ingredients') ~tabName = ~getLowercaseText('INGREDIENTS') ~details=productV2JsonData.ingredients tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.careInstructions && productV2JsonData.careInstructions != '') }}
            {{include ~id='tablinks' ~contentId= 'careInstructionsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='careInstructions') ~tabName = ~getLowercaseText('CARE INSTRUCTIONS') ~details=productV2JsonData.careInstructions tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.shippingAndReturn && productV2JsonData.shippingAndReturn != '') }}
            {{include ~id='tabShippingAndReturns' ~contentId= 'shippingAndReturnTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='shippingAndReturn') ~tabName = ~getLowercaseText('SHIPPING & RETURNS') ~details=productV2JsonData.shippingAndReturn tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.relatedInformation && productV2JsonData.relatedInformation != '') }}
            {{include ~id='tablinks' ~contentId= 'relatedInformationTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='relatedInformation') ~tabName = ~getLowercaseText('RELATED INFORMATION') ~details=productV2JsonData.relatedInformation tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.gwp && productV2JsonData.gwp != '') }}
            <button id="tabgwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='gwp')?'active':''}}" tabContentId="gwpTabContent">{{:~getLowercaseText('Gift with Purchase')}}</button>
            <div class="separator"></div>
            <div id='gwpTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='gwp')?'block':'none'}}">
            {{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
                {{for tmpl="tpl-pdpDetailsTab-gwp" /}}
            {{/if}}
            </div>
        {{/if}}
        {{if (productV2JsonData.pwp && productV2JsonData.pwp != undefined && productV2JsonData.pwp != '') }}
            <button id="tabpwp" class="tablinks {{:(productV2JsonData.defaultAccordion=='pwp')?'active':''}}" tabContentId="pwpTabContent">Special Savings Item</button>
            <div class="separator"></div>
            <div id='pwpTabContent' class="tabcontent" style="display:{{:(productV2JsonData.defaultAccordion=='pwp')?'block':'none'}}">
            {{if (productV2JsonData.teaserData && productV2JsonData.teaserData != '') }}
                {{for tmpl="tpl-pdpDetailsTab-gwp" /}}
            {{/if}}
            </div>
        {{/if}}
        {{if (productV2JsonData.rebate && productV2JsonData.rebate != undefined && productV2JsonData.rebate.longDescription != null) }}
            {{include ~id='tabRebate' ~contentId= 'rebateTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='rebate') ~tabName = ~getLowercaseText('REBATES') ~details=productV2JsonData.rebate.longDescription tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.exclusions && productV2JsonData.exclusions.longDescription != null) }}
            {{include ~id='tablinks' ~contentId= 'exclusionsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='exclusions') ~tabName = ~getLowercaseText('EXCLUSIONS') ~details=productV2JsonData.exclusions.longDescription tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.pricingDetails && productV2JsonData.pricingDetails != '') }}
            {{include ~id='tabSpecialPricing' ~contentId= 'pricingDetailsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='pricingDetails') ~tabName = ~getLowercaseText('SPECIAL PRICING') ~details=productV2JsonData.pricingDetails tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
        {{if (productV2JsonData.vendorDetails && productV2JsonData.vendorDetails != '') }}
            {{include ~id='tabVendorDetails' ~contentId= 'vendorDetailsTabContent' ~isDefault=(productV2JsonData.defaultAccordion=='vendorDetails') ~tabName = ~getLowercaseText('VENDOR DETAILS') ~details=productV2JsonData.pricingDetails tmpl='tpl-pdpDetailsTab-tab' /}}
        {{/if}}
    </div>
</div>
</div>
</script>

<script type='text/x-jsrender' id='tpl-pdpDetailsTab-gwp'>
    {{if productV2JsonData.teaserData.getProductDetails && productV2JsonData.teaserData.getProductDetails.getProducts}}
		<div class="qualify-message">{{:productV2JsonData.teaserData.initialMsg}}</div>
        <div class="offer-message">{{:productV2JsonData.teaserData.dynamicOfferMessage}}</div>
		<div class="carousel external-control">
			<div class="prev-button"><button class="prev-item disabled"><div class="carousel-button"></div></button></div>
			<div id="giftpro" class="fleft getCarousel">
				<ul>
					<li class="adjustprice">
					{{for productV2JsonData.teaserData.getProductDetails.getProducts}}
							<div class="item get-item">
							<div class="get-image {{:regularPrice==''?'zero-dollar':''}}">
									<a href="{{:seoUrl}}{{:seoUrl=='' ?'javascript:void(0);':''}}" title="{{:displayName}}"><img src="{{:imageURL}}" alt="{{:displayName}}"/>
								<span class="get-prod-title">{{:displayName}}</span></a>
								</div>
								{{if yourPrice != 'undefined' && yourPrice != '' && yourPrice != undefined }}
									<p class="get-prod-yourprice price-hide">Your Price:&nbsp;{{:yourPrice}}</p>
								{{/if}}
								{{if salePrice != 'undefined' && salePrice != '' && salePrice != undefined && ~root.productV2JsonData.pwp }}
									<p class="get-prod-saleprice">Sale:&nbsp;{{:salePrice}}</p>
								{{else}}
									{{if ~root.productV2JsonData.pwp }}
										<p class="get-prodprice">Regular:&nbsp;{{:regularPrice}}</p>
									{{else regularPrice != ''}}
										<p class="get-prodprice">Value:&nbsp;{{:regularPrice}}</p>
									{{/if}}
								{{/if}}
							</div>
						{{if (#index+1) % 4 == 0 && ~root.productV2JsonData.teaserData.getProductDetails.getProductCount > (#index+1)}}
						</li>
						<li class="adjustprice">
						{{/if}}
					{{/for}}
					</li>
				</ul>
			</div>
		</div>
        <div class="clear"></div>
    {{/if}}
</script>

<script type='text/x-jsrender' id='tpl-pdpDetailsTab-tab'>
    <button id="{{:~id}}" class="tablinks {{:~isDefault?'active':''}}" tabContentId="{{:~contentId}}">{{:~tabName}}</button>
    <div class="separator"></div>
    <div id='{{:~contentId}}' class="tabcontent" style="display:{{:~isDefault?'block':'none'}}">{{:~details}}</div>
    <div class="seemore_cnt_mobile  disabled">
        <span class="seemore_mobile" id="seemore-{{:~contentId}}"><img src="{{v:$env.resourceRoot}}images/pdp/rectangle.png"/> <span>See more</span></span>
    </div>
</script>
<script id="tpl-pdpFloatingCart-main" type="text/x-jsrender">
<div id="{{id:'root'}}">
	<div id="{{id:true 'floating-atc-equity'}}">
		<div class="floating-atc-content">
			<div class="floating-atc-left">
				<div class="floating-atc-img"><img src="{{:imageSrc}}"/></div>
				<div class="floating-atc-info">
					<div class="floating-atc-title"><p class="floating-atc-prod-title">{{:productTitle}}</p></div>
					<div class="floating-atc-swatches" id="{{id:'info'}}">
						{{if productColor}}<div class="floating-atc-color-swatch">Color: <span>{{>productColor}}</span></div>{{/if}}
						{{if productSize}}<div class="floating-atc-size-swatch">Size: <span>{{>productSize}}</span></div>{{/if}}
					</div>
				</div>
			</div>
			<div class="floating-atc-right">
				<div class="floating-atc-price" id="{{id:'pricing'}}"></div>
				<div class="floating-atc-finalize" id="{{id:'finalize'}}">
					<div class="floating-atc-shipping">
						<p class="floating-atc-boss" id="{{id:'boss'}}">{{:shippingText}}</p>
						<p class="floating-atc-store" id="{{id:'store'}}">{{:shippingStoreText}}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</script>
<script id="tpl-pdpAdSense" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-afsh-container">
    {{if $env.enable_afsh}}
        <div class="gglad_block_afshcontainer ggl_sponsered_links">
            <div class="ggl_greybg">Sponsored Links</div>
            <div class="ggl_helplink_container">
                <span class="ggl_helptext_afsh">What&#39;s this?</span>
            </div>
            <div id="afshcontainer"></div>
            <div class="ggl_tooltip_content_afsh">
                <p class="ggl-tooltip-title">
                    Sponsored Links
                <img src="/snb/media/images/ggl_close_btn.png" alt="closebtn" class="afsh_close_help"/>
                </p>
                <p class="ggl_help_content">{{:$env.googleAdSense_whatsthis_helptext}}</p>
            </div>
        </div>
        <script type="text/javascript" charset="utf-8">
        $ready(function _$ready_disableGoogleAdSenseThumbnails($){
            var afshContainer = ".gglad_block_afshcontainer";
            if(window.disableGoogleAdSenseThumbnails == true){
                $(afshContainer).hide();
            }
        });
        {{:"<"}}/script>
    {{/if}}
    {{if $env.enable_googleDFP_product_details_page && $env.enable_googleDFP_product_details_page_728x90}}
		<div id="dfp_leaderboard_728x90" style="text-align: center;"></div>
	{{/if}}
    <div class="pdp-afs-dfpRectangle">
        <div class="pdp-afs-container">
            {{if $env.afsh_show_bottom_rail_ads && $env.enable_afsh}}
                <div class="gglad_block_adcontainer1 ggl_sponsered_links">
                    <div class="ggl_greybg">Sponsored Links</div>
                    <div class="ggl_helplink_container">
                        <span class="ggl_helptext">What&#39;s this?</span>
                    </div>
                    <div id="adcontainer1"></div>
                </div>
            {{/if}}
            {{if $env.enable_admarketplace}}
                <div class="gglad_block_adcontainer5 ggl_sponsered_links" id="admarketplace-container">
                    <div class="ggl_greybg">Sponsored Links</div>
                    <div class="ggl_helplink_container">
                        <span class="ggl_helptext">What&#39;s this?</span>
                    </div>
                    <div id="amp_unit"></div>
                </div>
            {{/if}}
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
        var Dsp_numberofproducts = '',
            show_bottom_rail_ads = true,
            pageName = 'regularProductPage',
            queryVal = "{{:productV2JsonData.productTitle}}",
            channelVal = 'null';

        var pubIdForMVT = 'partner-kohls-pdp';
        var queryForMVT = "{{:productV2JsonData.productTitle}}";
        if(show_bottom_rail_ads){
            function getNumberOfAds(numAdsMap) {
                var adelem, adnameNum;
                for (var name in numAdsMap) {
                    adnameNum = name + ' has ' + numAdsMap[name] + ' ads';
                    if(numAdsMap[name] <= 0) {
                        adelem = ".gglad_block_" + name;
                        $(adelem).hide();
                        if (name == 'adcontainer1' && (enable_admarketplace != 'undefined')) {
                            showAdMarketPlace();
                        }
                    }
                }
            }
            if(queryVal != "" && (window.disableGoogleAdSenseText == undefined || window.disableGoogleAdSenseText == false)){
                    var pageOptions = {
                    'linkTarget': "{{:$env.linkTarget}}",
                    'pubId': "{{:$env.googleAds_pubId_partnerKohls}}",
                    'query': "{{if productV2JsonData.metaInfo!=null}}{{:productV2JsonData.metaInfo[0].metaDescription}}{{else}}0{{/if}}",
                    'hl': "{{:$env.hl}}",
                    'domainLinkAboveDescription': "{{:$env.domainLinkAboveDescription}}",
                    'detailedAttribution': "{{:$env.detailedAttribution}}",
                    'attributionText': '',
                    'adPage': "{{:$env.adPage}}",
                    'channel': channelVal,
                    'adsResponseCallback': getNumberOfAds,
                    'adtest': '{{:$env.adtest}}',
                    'titleBold': "{{:$env.titleBold}}",
                    'plusOnes': "{{:$env.plusOnes}}",
                    'sellerRatings': "{{:$env.sellerRatings}}",
                    'adLayout': "{{:$env.pageOptionsAdLayout}}",
                    'siteLinks': "{{:$env.siteLinks}}"
                };

                var adblock1 = {
                    'container': 'adcontainer1',
                    'width': "{{:$env.adblock_bottomrail_width}}",
                    'number': "{{:$env.adblock_bottomrail_number}}",
                    'lines': "{{:$env.adblock_bottomrail_lines}}",
                    'fontFamily': "{{:$env.adblock_fontFamily}}",
                    'fontSizeTitle': "{{:$env.adblock_fontSizeTitle}}",
                    'fontSizeDescription': "{{:$env.adblock_fontSizeDescription}}",
                    'fontSizeDomainLink': "{{:$env.adblock_fontSizeDomainLink}}",
                    'fontSizePlusOnes': "{{:$env.adblock_bottomrail_fontSizePlusOnes}}",
                    'colorTitleLink': "{{:$env.adblock_colorTitleLink}}",
                    'colorText': "{{:$env.adblock_colorText}}",
                    'colorDomainLink': "{{:$env.adblock_colorDomainLink}}",
                    'colorBackground': "{{:$env.adblock_colorBackground}}",
                    'colorBorder': "{{:$env.adblock_colorBorder}}",
                    'borderSelections': "{{:$env.adblock_borderSelections}}",
                    'colorPlusOnes': "{{:$env.adblock_bottomrail_colorPlusOnes}}",
                    'noTitleUnderline': "{{:$env.adblock_bottomrail_noTitleUnderline}}",
                    'detailedAttribution': "{{:$env.adblock_bottomrail_detailedAttribution}}",
                    'longerHeadlines': "{{:$env.longerHadblock_bottomrail_longerHeadlineseadlines}}"
                };
                _googCsa('ads', pageOptions, adblock1);
            }
        }
    {{:"<"}}/script>

    <script type="text/javascript">
    $ready(function _$ready_google_adsense($) {
        mypage = $(location).attr('href');
        isSearchPage = mypage.match(/search.jsp/gi);
        if(!isSearchPage){
            $(".ggl_helptext_2").on('click',function(){
                var offset = $(this).offset();
                topPosition = offset.top + 15;
                leftPosition = offset.left - 245;
                if($(".kohls-ad-wallpaper").length > 0){
                    var topPositionNew = $(".ggl_tooltip_content").parents("#container").offset().top;
                    var leftPositionNew = $(".ggl_tooltip_content").parents("#container").offset().left;
                    topPosition = topPosition - topPositionNew;
                    leftPosition = leftPosition - leftPositionNew;
                }
                $('.ggl_tooltip_content').toggle();
                $('.ggl_tooltip_content').css({'top':topPosition,'left':leftPosition});
            });

            $('.ggl_helptext').on('click', function(e){
                var offset = $(this).offset();
                var topPosition = offset.top+15;
                var leftPosition = offset.left-1;
                if($(".kohls-ad-wallpaper").length > 0){
                    var topPositionNew = $(".ggl_tooltip_content").parents("#container").offset().top;
                    var leftPositionNew = $(".ggl_tooltip_content").parents("#container").offset().left;
                    topPosition = topPosition - topPositionNew;
                    leftPosition = leftPosition - leftPositionNew;
                }
                $('.ggl_tooltip_content').toggle();
                $('.ggl_tooltip_content').css({'top':topPosition,'left':leftPosition});
            });
            $('.ggl_helptext_afsh').on('click', function(e){
                var topPosition = 32;
                var adsWidth = $(".gglad_block_afshcontainer.ggl_sponsered_links").width();
                adsWidth = adsWidth - 325;
                $('.ggl_tooltip_content_afsh').toggle();
                $('.ggl_tooltip_content_afsh').css({'top':topPosition,'left':adsWidth+'px'});
            });
            $('.close_help_container').on('click', function(){
                $(".ggl_tooltip_content").hide();
            });

            $('.afsh_close_help').on('click', function(){
                $(".ggl_tooltip_content_afsh").hide();
            });
        }
    });
    {{:"<"}}/script>

    {{if $env.enable_google_ads_site && $env.enable_afsh}}
    <script type="text/javascript" charset="utf-8">
        if((window.disableGoogleAdSenseThumbnails == undefined || window.disableGoogleAdSenseThumbnails == false) && (window.enableBottomrailThumbnailAds == undefined || window.enableBottomrailThumbnailAds == true)){
            var container = "afshcontainer";
            var channel = '';
            var pageName = 'regularProductPage';
            var afshParameters = "tops and tees womens dana buchman";
            var pThree = productV2JsonData.monetizationData.subCategory ? productV2JsonData.monetizationData.subCategory : "";
            var queryValue = "";
            function hideContainer(containerName, adsLoaded)
            {
                if (!adsLoaded){
                    var adelem = ".gglad_block_" + container;
                    $(adelem).hide();
                }
            }
            function hideContainer2()
            {
                var adelem2 = ".gglad_block_" + container;
                $(adelem2).hide();
            }
            function enableAfsAll() {
                queryValue = afshParameters;
                if (!isQueryEmpty()) {
                    if(channel != "") {
                        channel+="+";
                    }
                    channel += "test_pdp_query_allsignals";
                }
            }
            function enableSubcategory() {
                queryValue = pThree;
                if (!isQueryEmpty()) {
                    if(channel != "") {
                        channel+="+";
                    }
                    channel += "test_pdp_query_p3";
                }
            }
            function isQueryEmpty() {
                if (queryValue === "") {
                    queryValue = "{{:productV2JsonData.productTitle}}";
                    queryValue = queryValue.replace(/[^\w\s]/gi, '');
                    return true;
                }
                return false;
            }
            if (pageName === "regularProductPage" || pageName === "collectionPDPPage") {
                if (window.enablePdpAfsAll == true && window.enablePdpAfsSubcategory == false) {
                    enableAfsAll();
                } else if (window.enablePdpAfsAll == false && window.enablePdpAfsSubcategory == true){
                    enableSubcategory();
                } else {
                    var queryGroup = "test_pdp_query_p3";
                    if (queryGroup != null && queryGroup != "googleAds_AFSh_PDP_query_value") {
                        queryGroup = queryGroup.toLowerCase();
                        if (queryGroup != "test_pdp_query_p3") {
                            enableAfsAll();
                        } else {
                            enableSubcategory();
                        }
                    } else {
                        enableAfsAll();
                    }
                }
            } else {
                queryValue = "{{:productV2JsonData.productTitle}}";
            }
            if (queryValue != "") {
                var afsh_pageOptions = {
                    'pubId'    : "{{:$env.afsh_pdp_pubId}}",
                    'query'    : queryValue,
                    'channel'  : channel,
                    'adsafe'   : "{{:$env.adsafe}}",
                    'adtest'   : "{{:$env.adtest}}",
                    'hl'       : "{{:$env.hl}}",
                    'theme'    : "{{:$env.afsh_background_theme}}",
                    'priceMin' : "{{:$env.afsh_priceMin}}",
                    'priceMax' : "{{:$env.afsh_priceMax}}",
                    'priceCurrency': "{{:$env.afsh_priceCurrency}}",
                    'linkTarget'   : "{{:$env.linkTarget}}"
                };
                var afsh_adblock = {
                    'container'        : container,
                    'width'            : "{{:$env.afsh_width}}",
                    'height'           : 265,
                    'adLoadedCallback' : hideContainer
                };
                _googCsa('plas', afsh_pageOptions, afsh_adblock);
            } else {
                hideContainer2();
            }
        }
    {{:"<"}}/script>
    {{/if}}
</script>
<script id="tpl-pdpBDRecs" type="text/x-jsrender">
<div id="{{id:'root'}}">
{{if $env.bigDataEnabled && $env.bigDataRecs }}
    <div id="bd_rec_{{:$env.bdRenderingPlacement}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
    <div id="bd_rec_{{:$env.bdRenderingPlacementH2}}" style="width:100%;max-width:1400px" class="bd-render" data-template-type="{{:$env.bdRenderingTemplate}}" data-title-mode="center_small"></div>
{{/if}}
</div>
</script>
<script id="tpl-pdpBVDetails" type="text/x-jsrender">
    {{if productV2JsonData.isMobile}}
        {{for  tmpl='tpl-pdpBVDetails-mobile' /}}
    {{else}}
        {{for  tmpl='tpl-pdpBVDetails-desktop' /}}
    {{/if}}
</script>

<script id="tpl-pdpBVDetails-desktop" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-BV-tabs clearfix">
    {{if $env.bvConversion}}
        <div data-bv-show="review_highlights" data-bv-product-id="{{:productV2JsonData.webID}}" style="margin:10px 0 40px 0;"></div>
    {{/if}}
    <div class="product-cd">
        <ul class="productcdMenu" role="tablist">
            {{if $env.bazaarvoiceEnabled}}
            <li role="tab"><a id="bv-content-show" class="rating-content active" onclick="sc_bvTabOpen('{{:productV2JsonData.webId}}')" href="#rating-content">Ratings &amp; Reviews</a>
            </li>
            <li role="tab">
                <a href="#qa-content" id="Qusetionstab" onclick="sc_bvQATabOpen('{{:productV2JsonData.webId}}')">
                Q&A</a>
            </li>
            <script type="text/javascript" >
                var numberOfReviews = "{{:$env.numberOfReviews}}";
                var isValidforBV = "{{:$env.isValidforBV}}";
            {{:"<"}}/script>
            {{/if}}
        </ul>
        <div id="qa-content" class="display-none">
            <div class="prod_description1">
                <div class="Bdescription">
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvQuestions" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="questions" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                </div>
            </div>
            <div class="hor_separator"></div>
        </div>
        {{if $env.bazaarvoiceEnabled}}
            <div class="hidden_accordion_segments">
                <div id="ratings-segment"></div>
            </div>
            <div id="rating-content" >
                {{:productV2JsonData.bvResponse}}
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvReviews" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="reviews" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                <div id="submissionContainerUrlParameters" style="display: none;"></div>
                <div id="submissionContainerUrl" style="display: none;"></div>
                <div class="hor_separator"></div>
            </div>
        {{/if}}
    </div>
</div>
</script>

<script id="tpl-pdpBVDetails-mobile" type="text/x-jsrender">
<div id="{{id:'root'}}" class="pdp-BV-tabs clearfix">
    <div class="product-cd">
        {{if $env.bazaarvoiceEnabled}}
            <div id="rating-content" >
                {{:productV2JsonData.bvResponse}}
                {{if $env.bvConversion}}
                    {{if $env.bvLazyLoading}}
                        <div id="bvReviews" data-bv-productId="{{:productV2JsonData.webID}}" style="min-height:150px">
                            <div>
                                <img src="/snb/media/images/pmp_imgs/pmp_search_loader.gif" class="bv_review_loader" alt="review loader"/>
                            </div>
                        </div>
                    {{else}}
                        <div data-bv-show="reviews" data-bv-productId="{{:productV2JsonData.webID}}"></div>
                    {{/if}}
                {{/if}}
                <div id="submissionContainerUrlParameters" style="display: none;"></div>
                <div id="submissionContainerUrl" style="display: none;"></div>
                <div class="hor_separator"></div>
            </div>
        {{/if}}
    </div>
</div>
</script>
<script id="tpl-pdpDFPAds" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div class="dfpThreeAmigosAd">
        {{if $env.enable_googleDFP_product_details_page}}
            <div id="pdp-threeamigos">
                {{if $env.enable_googleDFP_collection_page}}
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottomleft}}
                            <div id="dfp_medium_rectangle_300x250_bottomleft" class="dfp_medium_rectangle_300x250_bottomleft">
                            </div>
                        {{/if}}
                    </div>
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottommiddle}}
                            <div id="dfp_medium_rectangle_300x250_bottommiddle" class="dfp_medium_rectangle_300x250_bottommiddle">
                            </div>
                        {{/if}}
                    </div>
                    <div class="amigo-container">
                        {{if $env.enable_googleDFP_collection_page_300x250_bottomright}}
                            <div id="dfp_medium_rectangle_300x250_bottomright" class="dfp_medium_rectangle_300x250_bottomright">
                            </div>
                        {{/if}}
                    </div>
                {{/if}}
            </div>
        {{/if}}
    </div>
</div>
</script>
<script id="tpl-pdpFeaturedProducts" type="text/x-jsrender">
<div id="{{id:'root'}}">
    {{if $env.isNewHookLogicEnabled}}
        <div id="ProductDetailWeb-Carousel"></div>
    {{/if}}
</div>
</script><script id="tpl-pdpTrendage" type="text/x-jsrender">
<div id="{{id:'root'}}">
    <div id="trendage" class="trendage">
    {{if $env.enableTrendageGrid }}
        <div id="trendage_grid" class="trendage_grid"></div>
    {{/if}}
    </div>
    {{if $env.enableTrendageCarousal }}
    <div id="trendage_carousal" class="trendage_carousal"></div>
    {{/if}}
</div>
</script><script id="tpl-pdpWebCollageInline" type="text/x-jsrender">
<div id="{{id:'root'}}">
{{if $env.enableWebcollageInline }}
    <div id="wc-power-page"></div>
{{/if}}
</div>
</script>