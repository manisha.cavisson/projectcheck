/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: cavisson
    Date of recording: 05/21/2021 01:41:39
    Flow details:
    Build details: 4.6.0 (build# 68)
    Modification History:
-----------------------------------------------------------------------------*/

package com.cavisson.scripts.script_kohls_checkProxy;
import pacJnvmApi.NSApi;

public class exit_script
{
    public static int execute(NSApi nsApi)
    {
        return 0;
    }
}
