
--Response 
HTTP/1.1 200
status: 200
date: Fri, 21 May 2021 08:17:29 GMT
content-type: application/json
content-length: 42
server: nginx/1.18.0
trace-id: 402a084501b12f64
vary: Origin
set-cookie: lidid=51e452ef-39b5-42e8-b7ed-83178ae8eaea; Max-Age=63072000; Expires=Sun, 21 May 2023 08:17:29 GMT; Path=/; Domain=.liadm.com; Secure; HTTPOnly
request-time: 1
referrer-policy: origin-when-cross-origin, strict-origin-when-cross-origin
x-frame-options: DENY
x-pixel-event-id: 14658916-4c51-48bf-8114-6787175483cb
x-xss-protection: 1; mode=block
x-content-type-options: nosniff
strict-transport-security: max-age=31536000; includeSubDomains
access-control-allow-origin: https://www.kohls.com
access-control-allow-credentials: true
x-permitted-cross-domain-policies: master-only
----

--Response 
HTTP/1.1 200
status: 200
x-amz-id-2: vI/Nj8PQ+mTPD0w4Rn5q8N5I4twtAgKx2QnUokzHZLSzf+G9NJi+Xa16/tBOeUJMbq4kUp8Ymvg=
x-amz-request-id: BWAJY76QJR6YS30T
last-modified: Wed, 28 Apr 2021 10:29:09 GMT
etag: \"345b014159f097e79876cdd654d26be9\"
content-encoding: br
x-amz-version-id: abxdP6d745uIVU72ngtqQUxAPPOcD0KE
accept-ranges: bytes
content-type: application/javascript;charset=utf-8
content-length: 32191
server: AmazonS3
cache-control: max-age=31536000
expires: Sat, 21 May 2022 08:17:29 GMT
date: Fri, 21 May 2021 08:17:29 GMT
access-control-allow-origin: *
----

--Response 
HTTP/1.1 200 OK
Cache-Control: private, no-cache, max-age=0
Content-Encoding: gzip
Content-Type: text/html; charset=UTF-8
Date: Fri, 21 May 2021 08:17:31 GMT
ETag: 1.61803398874
Set-Cookie: _li_ss=MgUIBhC-DzIFCAoQvg8yBQh6EL0PMgYIiwEQvg8yBQgLEL4PMgUICxC-DzIFCHkQvQ8; Max-Age=2592000; Expires=Sun, 20 Jun 2021 08:17:31 GMT; Path=/s
Strict-Transport-Security: max-age=31536000; includeSubDomains
trace-id: ddf3803cf44c408e
Vary: Accept-Encoding
Content-Length: 640
Connection: keep-alive
----

--Response 
HTTP/1.1 204
status: 204
date: Fri, 21 May 2021 08:17:30 GMT
timing-allow-origin: *
cache-control: no-cache, no-store, must-revalidate, pre-check=0, post-check=0
pragma: no-cache
expires: Sun, 24 Oct 1982 23:00:00 GMT
access-control-allow-origin: *
access-control-allow-methods: GET, POST, OPTIONS
access-control-allow-headers: Access-Control-Expose-Headers, Content-Type, Content-Compression, X-Requested-With
content-disposition: inline
----

--Response 
HTTP/1.1 200 OK
Content-Type: image/gif
Content-Length: 19
Expires: Fri, 21 May 2021 08:17:30 GMT
Cache-Control: max-age=0, no-cache, no-store
Pragma: no-cache
Date: Fri, 21 May 2021 08:17:30 GMT
Connection: keep-alive
Set-Cookie: _lc2_fpi=0b10d8358f40--01f6700cpcqvx6swnfx7rby23x; Max-Age=63072000; Domain=.kohls.com; SameSite=Lax
----
--Request 
GET http://localhost:9222/devtools/Images/treeoutlineTriangles.svg
Host: localhost:9222
Host: localhost:9222
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept: image/webp,image/apng,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
 POSTDATA {"p":"display","d":"desktop","c":"89471702884776","s":"Y304tuBM","w":"1863551621585031440kTu0K7n3tlpf5","t":1621585033500,"pg":{"t":1621585031440,"e":[]},"gt":"1461","ac":"1492","sl":[{"s":"0b13ae99-d2ae-3f3c-696b-abe2367d5c9b","t":1621585031950,"xslots":{"TPL":{"6":{"brq":1,"bp":1}},"BRT":{"6":{"brq":1,"be":1}},"APNX":{"6":{"brq":1,"bt":1}},"SVRN":{"7":{"brq":1,"bp":1}},"PUBM":{"6":{"brq":1,"be":1}},"RUBI":{"6":{"brq":1,"bp":1}},"OATHM":{"7":{"brq":1,"bp":1}},"INDX":{"4":{"brq":1,"bp":1}}}},{"s":"db4747c3-0d7f-db61-cd9a-e4fdd97052b0","t":1621585031954,"xslots":{"TPL":{"4":{"brq":1,"bp":1}},"BRT":{"4":{"brq":1,"be":1}},"APNX":{"4":{"brq":1,"bt":1}},"SVRN":{"5":{"brq":1,"bp":1}},"PUBM":{"4":{"brq":1,"be":1}},"RUBI":{"4":{"brq":1,"bp":1}},"OATHM":{"5":{"brq":1,"bp":1}},"INDX":{"3":{"brq":1,"bp":1}}}},{"s":"542e63bd-902a-056a-4225-97a1a1c7bc9c","t":1621585031964,"xslots":{"TPL":{"7":{"brq":1,"bp":1}},"BRT":{"7":{"brq":1,"be":1}},"APNX":{"7":{"brq":1,"bt":1}},"SVRN":{"8":{"brq":1,"bp":1}},"PUBM":{"7":{"brq":1,"be":1}},"RUBI":{"7":{"brq":1,"bp":1}},"OATHM":{"8":{"brq":1,"bp":1}},"INDX":{"5":{"brq":1,"bp":1}}}},{"s":"identity","t":1621585031455,"xslots":{"LVINT":{"before":{"brq":"0","bp":1,"pt":"50"}},"LVRAMP":{"before":{"brq":"0","be":1,"pt":"50"}},"ADSORG":{"before":{"brq":"0","brs":1,"pt":"50"}}}}]}
--Response 
HTTP/1.1 200 OK
Content-Length: 120
Content-Type: image/svg+xml
----
--Request 
GET http://localhost:9222/devtools/Images/smallIcons.svg
Host: localhost:9222
Host: localhost:9222
Connection: keep-alive
User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36
Accept: image/webp,image/apng,image/*,*/*;q=0.8
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: no-cors
Accept-Encoding: gzip, deflate, br
Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
----
--Response 
HTTP/1.1 200 OK
Content-Length: 15394
Content-Type: image/svg+xml
----

