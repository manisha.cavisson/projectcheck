/*!
 * Bazaarvoice curations 1.2.0 config
 * Fri, 12 Mar 2021 16:51:37 GMT
 *
 * http://bazaarvoice.com/
 *
 * Copyright 2021 Bazaarvoice. All rights reserved.
 *
 */
window.performance && window.performance.mark && window.performance.mark('bv_loader_configure_curations_start');BV["curations"].configure({"client":"Kohls","curalateEnabled":false,"curationsEnabled":false})