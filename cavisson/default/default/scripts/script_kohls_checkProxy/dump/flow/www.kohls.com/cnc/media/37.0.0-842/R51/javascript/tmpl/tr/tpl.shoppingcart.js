<script id="shoppingBagTemplate" type="text/x-jQuery-tmpl">
	<div id="sb-tmpl-inner-container">
		<div class="notification">
			{{if trJsonData.fromShoprunner}}
				<p>{{html convertHTML(trLabels.kls_boss_static_shoprunner_empty_shopping_bag_msg)}}</p>
			{{else}}
				<p>{{html convertHTML(trLabels.kls_boss_static_tr_cart_shoppingBagInfo)}}</p>
			{{/if}}
		</div>
		{{if cartItems}}
			{{if cartItems.length > 0}}
				{{tmpl '#sb-checkout-btn'}}
				<div id="shoppingCartLineItem_container" class="Checkout_button_margin">
					{{if $env.enableSmartCart == 'true' && !$env.ksCncShoppingBagSmartCartEnabled && Kjs.trEliteLoyaltyBanner.bannerDisplayCheck(trJsonData, 'smartcart') === 'smartcart'}}
						{{if trJsonData.incentiveDetails}}
							{{if trJsonData.incentiveDetails.smartIncentiveStatus}}
							{{if trJsonData.incentiveDetails.smartIncentiveStatus.toLocaleLowerCase() == "qualified"}}
								<div id="incentive-text" class="sc-qualified SavingsLauncher_Filled">
									<span class="smartCartQualified">${updateIncentiveMessage(trLabels.kls_static_smartcard_incentive_qualified,trJsonData.incentiveDetails.incentiveAmount)}
									</span>
								</div>
							{{/if}}
							{{if trJsonData.incentiveDetails.smartIncentiveStatus.toLocaleLowerCase() == "eligible"}}
								<div id="incentive-text" class="sc-eligible SavingsLauncher_Line">
									<span class="smartCartEligible">${updateIncentiveMessage(trLabels.kls_static_smartcard_incentive_eligible,trJsonData.incentiveDetails.incentiveAmount)}
									</span>
								</div>
							{{/if}}
							{{/if}}
						{{/if}}
					{{/if}}
					{{if $env.ksCncShoppingBagSmartCartEnabled}}
						<div id="smartCartNew">
							{{! tmpl '#newIncentiveTemplate'}}
						</div>
					{{/if}}
						<div class="boss-shoppingcart-cartLineItem">
 					      {{tmpl '#shoppingBagLineItemsTemplate'}}
					    </div>
				</div>
				{{tmpl '#sb-checkout-btn'}}
			{{else}}
				<!-- shopping bag empty -->
				<div class="shoppingbag_container">
					<div class="shoppingbag_empty">
						{{html convertHTML(trLabels.kls_static_tr_cart_empty_shoppingBag)}}
					</div>

					{{if !trJsonData.fromShoprunner && !$env.isKioskEnabled}}
						{{if trJsonData.isGuest == "true"}}
							<div class="shopping_sign">
							{{if trJsonData.isRedesignSingleLoginPage}}
							{{html convertHTML(trLabels.kls_static_tr_cart_had_items)}} <a href="/myaccount/kohls_login.jsp" ex-href="/myaccount/kohls_login.jsp" data-href="/myaccount/modal/kohls_modal_login.jsp"  id="signInLink">{{html convertHTML(trLabels.kls_static_tr_cart_signIn)}}   </a> {{html convertHTML(trLabels.kls_static_tr_cart_view_this_items)}}
							{{else}}
								{{html convertHTML(trLabels.kls_static_tr_cart_had_items)}} <a href="#" ex-href="/myaccount/kohls_login.jsp" data-href="/myaccount/modal/kohls_modal_login.jsp" class="signin_popup" id="signInLink">{{html convertHTML(trLabels.kls_static_tr_cart_signIn)}}</a> {{html convertHTML(trLabels.kls_static_tr_cart_view_this_items)}}
							{{/if}}
							</div>
						{{/if}}
					{{/if}}

					<div class="shoppingbag_frame" style="border:0px solid #000;text-align:center;">
						<a href="/home.jsp" name="Checkout" class="button_startshoppingBtn bossEmptyCart">{{html convertHTML(trLabels.kls_static_boss_empty_cart)}}</a>
					</div>
				</div>
			{{/if}}
		{{else}}
			<!-- shopping bag empty -->
			<div class="shoppingbag_container">
				<div class="shoppingbag_empty">
					{{html convertHTML(trLabels.kls_static_tr_cart_empty_shoppingBag)}}
				</div>

				{{if !trJsonData.fromShoprunner && !$env.isKioskEnabled}}
					{{if trJsonData.isGuest == "true"}}
						<div class="shopping_sign">
						{{if trJsonData.isRedesignSingleLoginPage}}
						{{html convertHTML(trLabels.kls_static_tr_cart_had_items)}} <a href="/myaccount/kohls_login.jsp" ex-href="/myaccount/kohls_login.jsp" data-href="/myaccount/modal/kohls_modal_login.jsp"  id="signInLink">{{html convertHTML(trLabels.kls_static_tr_cart_signIn)}}   </a> {{html convertHTML(trLabels.kls_static_tr_cart_view_this_items)}}
						{{else}}
							{{html convertHTML(trLabels.kls_static_tr_cart_had_items)}} <a href="#" ex-href="/myaccount/kohls_login.jsp" data-href="/myaccount/modal/kohls_modal_login.jsp" class="signin_popup" id="signInLink">{{html convertHTML(trLabels.kls_static_tr_cart_signIn)}}</a> {{html convertHTML(trLabels.kls_static_tr_cart_view_this_items)}}
						{{/if}}
						</div>
					{{/if}}
				{{/if}}

				<div class="shoppingbag_frame" style="border:0px solid #000;text-align:center;">
					<a href="/home.jsp" name="Checkout" class="button_startshoppingBtn bossEmptyCart">{{html convertHTML(trLabels.kls_static_boss_empty_cart)}}</a>
				</div>
			</div>
		{{/if}}		
	</div>
</script>
<script type="text/javascript">
function updateIncentiveMessage(message,amount){
	return message.replace('{0}',amount);
}
function updateStoreText( message , store ){
	if(store && store.incentiveDetails &&  store.incentiveDetails.storeDetails && store.incentiveDetails.storeDetails.storeName){
		return message.replace('{0}',store.incentiveDetails.storeDetails.storeName);
	}else{
		var storeName = Kjs.cookie.get("K_favstore"),storeDetails,text;
		 storeDetails=storeName.split('|');
		if(storeDetails && storeDetails.length > 0 )
		 text=storeDetails[2]
		return message.replace('{0}',text);
	}
}
</script>

<script id="newIncentiveTemplate" type="text/x-jQuery-tmpl">
	{{if trJsonData.incentiveDetails && trJsonData.incentiveDetails.showModule && trJsonData.incentiveDetails.incentiveEligible}}
		{{if trJsonData.incentiveDetails.incentiveQualified && trJsonData.incentiveDetails.minCartThresholdMet}}
		    <div class="Rectangle-87 Qualified-new">
				<img src="${$env.resourceRoot}images/ic-discount-o.png" class="ic-discount-o">
				<div class="qualified-message-container">{{html updateIncentiveMessage(trLabels.kls_static_new_smartcard_incentive_qualified,trJsonData.incentiveDetails.incentiveAmount)}}</div>
			</div>
		{{/if}}
		{{if !trJsonData.incentiveDetails.incentiveQualified || !trJsonData.incentiveDetails.minCartThresholdMet}}
			<div class="Rectangle-87">
				<img src="${$env.resourceRoot}images/ic-discount-o.png" class="ic-discount-o">
				{{html updateIncentiveMessage(trLabels.kls_static_new_smartcard_incentive_eligible,trJsonData.incentiveDetails.incentiveAmount)}}
			</div>
		{{/if}}
		<div class="box-copy">
			{{if trJsonData.incentiveDetails.incentiveQualified && !trJsonData.incentiveDetails.minCartThresholdMet}}
				<img src="${$env.resourceRoot}images/ic-warning-g.png" >
				<span class="Sorry-your-cart-no">{{html updateIncentiveMessage(trLabels.kls_static_new_smartcard_incentive_err_message,trJsonData.incentiveDetails.incentiveAmount)}}</span>
			{{else}}
				<label class="switch" tabindex='0' role="checkbox" aria-label="${trLabels.kls_static_pick_up_my_order}" {{if trJsonData.incentiveDetails.incentiveQualified && trJsonData.incentiveDetails.minCartThresholdMet}} aria-checked="checked" {{/if}}>
					<input type="checkbox" {{if trJsonData.incentiveDetails.incentiveQualified && trJsonData.incentiveDetails.minCartThresholdMet}} checked {{/if}} id="pickUpMyOrder"> 
					<span class="slider round"></span>
				</label>
				<div class="pickup_message">
					<span class="Pick-up-my-order">${trLabels.kls_static_pick_up_my_order}</span>
					<span id="update-pickup-count"></span>
				</div>
			{{/if}}
		</div>
	{{/if}}
	<!-- KOSPC-16034  edge case scennario which is mentioned in the defect-->
	{{if trJsonData.incentiveDetails && !trJsonData.incentiveDetails.showModule && trJsonData.incentiveDetails.incentiveEligible}}
		{{if !trJsonData.incentiveDetails.incentiveQualified || !trJsonData.incentiveDetails.minCartThresholdMet}}
			<div class="Rectangle-87">
				<img src="${$env.resourceRoot}images/ic-discount-o.png" class="ic-discount-o">
				{{html updateIncentiveMessage(trLabels.kls_static_new_smartcard_incentive_eligible,trJsonData.incentiveDetails.incentiveAmount)}}
			</div>
		{{/if}}
		<div class="box-copy">
			<img src="${$env.resourceRoot}images/ic-warning-g.png" >
			<span class="Sorry-your-cart-no">{{html updateStoreText(trLabels.kls_static_new_smartcard_incentive_oos_err_message,trJsonData)}}</span> 
			</div>
	{{/if}}
</script>
<script id="newIncentiveOOSError" type="text/x-jQuery-tmpl">
	<img src="${$env.resourceRoot}images/ic-warning-g.png" />
	<span class="Sorry-your-cart-no">{{html message}}</span>
</script>

<script id="shoppingBagLineItemsTemplate" type="text/x-jQuery-tmpl">

	<div id="shoppingCartMessages" class="msgContainer messages"></div>
	<div class="shoppingbag_container shoppingCartLineItemHeader "  style="display:none" >
		<div class="shoppingbag_item">{{html convertHTML(trLabels.kls_static_tr_cart_productDescription)}}</div >
		{{if !$env.isKioskEnabled}}
		<div class="shoppingbag_quantity">{{html convertHTML(trLabels.kls_static_tr_cart_quantity)}}</div >
		{{/if}}
		<div class="shoppingbag_itemtotal_heading">{{html convertHTML(trLabels.kls_static_tr_cart_item_total)}}</div >
	</div>
	<hr/  style="display:none" >

	<div class="shoppingBag">
		{{each(i,item) cartItems}}
			<div class="shoppingBagItem shoppingCartLineItem"

				data-commerceId="${cartItemId}"
				data-productId="${productId}"
				data-quantity = "${quantity}"
				data-skuColor="{{if itemProperties}} ${itemProperties.color} {{/if}}"
				data-primarySize="{{if itemProperties}} ${itemProperties.size} {{/if}}"
				data-secondarySize="${secondarySize}"
				data-skuName="${skuName}"
				data-skuId="${skuId}"
				data-price="{{if itemPriceInfo}} ${itemPriceInfo.regularUnitPrice} {{/if}}"
				data-skuUpcCode="${upcCode}"
				data-zipCode="${Kjs.trCommonCheckout.getStoreIdOrName(shippingInfo.shipmentRefId,'isZipcode')}"
				data-storeId="${Kjs.trCommonCheckout.getStoreIdOrName(shippingInfo.shipmentRefId,false)}"
				data-storeIdforBOSS="${Kjs.trCommonCheckout.getStoreNameForBoss(item).storeId}"
				{{if item.giftInfo && item.giftInfo.isGiftSelected}} 
				data-isGiftSelected="true"
				{{else}}
				data-isGiftSelected="false"
				{{/if}}
				{{if item.offerInfo && item.offerInfo.length > 0 }}
					{{each(offerIndex,obj) item.offerInfo}}
						{{if obj.gwpPwpInfo}} 
							{{if obj.gwpPwpInfo.buyItem }}
								data-isGiftAdded = "${obj.gwpPwpInfo.getItemAdded}"
								data-itemType = "{{if obj.gwpPwpInfo.buyItem}}buyItem{{/if}}{{if obj.gwpPwpInfo.getItem}}getItem{{/if}}"
								{{if obj.gwpPwpInfo.buyItemId }}
									data-buyProdId = "${obj.gwpPwpInfo.groupId}"
								{{/if}}
							{{/if}}
							{{if obj.gwpPwpInfo.getItem && obj.gwpPwpInfo.buyItemId }}
								data-getProdId = "${obj.gwpPwpInfo.groupId}"
							{{/if}}
							{{if obj.gwpPwpInfo.buyItem && obj.gwpPwpInfo.pwpItem }}
								data-isPwp = "${obj.gwpPwpInfo.pwpItem}"
							{{/if}}
						{{/if}}
					{{/each}}	
				{{/if}}
			>
				<!--Div BLOCK for zineOne KOSPC-16807-->
				<div class="z1PreItem"></div>
          			<!--ERROR MESSAGE BLOCK -->
				 

				{{if successMsg}}
					<div class="ItemMsgContainer" style="width:590px;">
					<div class="ItemMsgBlock fleft">
					<span class="successMsg">${successMsg}</span>
					</div>
					<div class="ItemMsgButtonClose fright"></div>
					<div class="clear"/>
					</div>
				{{/if}}
				
				<div class="shoppingbag_itemlayout">
						<div class=item-thumb fleft>
							{{if itemProperties}}
								<a href="{{if itemProperties.productSeoURL}}${itemProperties.productSeoURL}{{else}}#{{/if}}"> <img width="60" height="60" src="{{if itemProperties.image}} ${itemProperties.image.url}{{/if}}"/></a>
							{{/if}}
						</div>
					<div class="shoppingbag_productlayout">
						{{tmpl(item) '#tmpl_include_sku_details'}}
						<div>
						{{tmpl(item) '#tmpl_include_salePrice'}}
						</div>
						{{tmpl(item) '#tmpl_include_applied_offers_LID'}}

						{{tmpl(item) '#tmpl_include_shippingSurcharges'}}

						{{tmpl(item) '#tmpl_include_gwpInfo'}}

						{{tmpl(item) '#tmpl_include_promotionMsgList'}}

						<!--Div BLOCK for zineOne KOSPC-16807-->
						<div  class="z1PostItem"></div>
						<!--PICKUPITEM BLOCK -->
						
					{{if !$env.isKioskEnabled}}
						{{if !Kjs.trCommonCheckout.isPickupItem(shippingInfo.shipmentRefId)}}
						<div  class="items_suggested">
							{{if !trJsonData.fromShoprunner}}
								<ul>
									<li class="shoppingbag_giftwrapper">
										{{if itemType!='MARKETPLACE' && itemType!='VGC'}}
											{{if !($data.offerInfo && $data.offerInfo.gwpPwpInfo && $data.offerInfo.gwpPwpInfo.getItem && $data.offerInfo.gwpPwpInfo.getItem.isGiftProdPDP)}}
											<label for="gift_${cartItemId}" class="shoppingbag_pdtleft tr_chkbox {{if giftInfo}} {{if giftInfo.isGiftSelected}}tr_chkbox tr_chkboxchkd{{/if}}{{/if}}">{{html convertHTML(trLabels.kls_static_tr_cart_makeThisAsGift)}}
											<input type="checkbox" id="gift_${cartItemId}" value="true" class="gift_item visually-hidden"  {{if giftInfo}} {{if giftInfo.isGiftSelected}}checked{{/if}}{{/if}}/>
											<span class="checkmark popupCheck"></span>
											</label>
											{{/if}}

										{{/if}}
										{{if registryInfo}}
											{{if registryInfo.registryType == 'Bridal'}}<span class="regicon_thumb marginl20  fleft"></span>{{/if}}
											{{if registryInfo.registryType == 'Baby'}}<span class="babyReg_icon marginl20  fleft"></span>{{/if}}
											{{if registryInfo.registryType == 'wish list'}}<span class="gifticon_thumb  marginl20 fleft"></span> {{/if}}
											{{if registryInfo.registryType == 'special event' || registryInfo.registryType == 'Specialevent'}}<span class="gifticon_thumb  marginl20 fleft"></span> {{/if}}
											<span class="registryName padl">
											{{html convertHTML(registryInfo.registryName)}}
											</span>
										{{/if}}
									</li>
								</ul>
							{{/if}}
						</div>
						{{else}}
							<div  class="items_suggested">
								{{if !trJsonData.fromShoprunner}}
									{{if itemType!='VGC'}}
									<ul>
										<li class="shoppingbag_giftwrapper">
										<label for="gift_${cartItemId}" class="shoppingbag_pdtleft tr_chkbox {{if giftInfo}} {{if giftInfo.isGiftSelected}}tr_chkbox tr_chkboxchkd{{/if}}{{/if}}">{{html convertHTML(trLabels.kls_static_tr_cart_makeThisAsGift)}}
										<input type="checkbox" id="gift_${cartItemId}" value="true" class="gift_item visually-hidden"  {{if giftInfo}} {{if giftInfo.isGiftSelected}}checked{{/if}}{{/if}}/>
										<span class="checkmark popupCheck"></span>
										</label>
											
										</li>
									</ul>
									{{/if}}
								{{/if}}
							</div>
							{{if typeof bopusItemErrorMessage != "undefined" && bopusItemErrorMessage !="" && (trJsonData.bopusKillSwitch1 || trJsonData.storePickupTabKillSwitch)}}
								<div class="bopusItemErrorMessage has-error-message"><span class="bopusItemError">${bopusItemErrorMessage}</span></div>
							{{/if}}
							<div class="PickupStoreMsg">{{html convertHTML(trLabels.kls_static_bopus_pick_up_at_msg)}}&nbsp;${Kjs.trCommonCheckout.getStoreIdOrName(shippingInfo.shipmentRefId,true)}&nbsp;store&nbsp;(&nbsp;<span id="pickup_${commerceId}" class="ChangeToShipMsg">{{html convertHTML(trLabels.kls_static_bopus_change_to_ship)}}</span>&nbsp;)</div>
						{{/if}}
					{{/if}}

						
						{{if registryInfo || (offerInfo && offerInfo.length > 0 && Kjs.trCommonCheckout.gwpPwpInfoBlock(item))}}	
							{{tmpl(item) '#cart_bossRegistryOrGwpContainer'}}
						{{else}}
							{{tmpl(item) '#cart_bossContainer'}}
						{{/if}}
					

					</div>
				</div>

				<!-- GWP/PWP QUANTITY RELATED CONDITIONS BLOCK -->

				{{if Kjs.trCommonCheckout.quantityBlock(item)}}							
					<div class="fleft quantity" style="width:40px;">
						<input type="hidden" maxlength="3" name="ship_bill_quantity" {{if errorMsg}}class="ship_bill_quantity ship_bill_quantity_error"{{else}}class="ship_bill_quantity"{{/if}}  value=${quantity != 0  ? quantity : minimumAllowedQuantity} size="3" id="${cartItemId}">
						<span>${quantity}</span>
					</div>							
				{{else}}
					{{if item.itemType != 'VGC'}}
						{{tmpl(item) '#tmpl_include_quant_box'}}
					{{/if}}
				{{/if}}

				<div class="fright">
					<div class="shoppingbag_itemtotal shoppingbag_v2_itemtotal">
						<!--GWP/PWP PRICE TOTAL PRICE RELATED CONDITIONS BLOCK -->
						{{if Kjs.trCommonCheckout.quantityBlock(item)}}															
							<span class="freeLableForGift">${trLabels.kls_static_tr2_pb_free_shipping_label}</span>
						{{else}}
							{{if itemPriceInfo}}
								{{if (typeof showDiscountPrice !="undefined" && showDiscountPrice == false)|| (typeof trJsonData.promoApplied =='undefined' || trJsonData.promoApplied == true) }}
									${addCurrency(itemPriceInfo.promoAdjustedExtendedPrice)}
								{{else}}
									{{if eligibility.mapEligible}}
										<span class="mapPricingLabel">{{html convertHTML(trLabels.kls_static_map_pricing_label)}}</span>
									{{else}}
										{{if itemPriceInfo.discount && itemPriceInfo.discount != 0 }}
											<span class='freeLableForGift'>-${addCurrency(itemPriceInfo.discount)}</span>
											<br />
										{{/if}}
										{{if itemPriceInfo.grossPrice}}
										${addCurrency(itemPriceInfo.grossPrice)}
										{{/if}}
									{{/if}}	
								{{/if}}	
								
							{{/if}}								
						{{/if}}
					</div>
					<p class="clearfix">		
						<!--GWP/PWP EDIT URL RELATED CONDITIONS BLOCK -->
						{{if item.offerInfo && item.offerInfo.length > 0 && Kjs.trCommonCheckout.gwpPwpInfoBlock(item)}}
							{{each(offerIndex,obj) item.offerInfo}}	
								{{if obj.gwpPwpInfo}}
									{{if obj.gwpPwpInfo.getItem}}									
										{{if obj.gwpPwpInfo.gwpItem}}					
											{{if obj.gwpPwpInfo.isGetItemEditAllowed}}
												<a href="/checkout/v2/cart/tr_gwp_edit_item_display.jsp?productId=${productId}&skuId=${skuId}&cItemId=${cartItemId}&action=edit" data-cartItemId="${cartItemId}" data-buy-item-id="${obj.gwpPwpInfo.buyItemId}" class="bag_edit_item gwp_edit_item_multiple">
													{{html convertHTML(trLabels.kls_static_tr_cart_editThisItem)}}
												</a>
											{{/if}}				
										{{else}}
											{{if item.itemProperties}}
												<a href="${itemProperties.productEditSeoUrl}" class="bag_edit_item_pwp">{{html convertHTML(trLabels.kls_static_tr_cart_editThisItem)}}</a>
											{{/if}}				
										{{/if}}									
									{{else}}
										{{if item.itemProperties}}
											<a href="${itemProperties.productEditSeoUrl}" class="bag_edit_item_pwp">{{html convertHTML(trLabels.kls_static_tr_cart_editThisItem)}}</a>
										{{/if}}
									{{/if}}
								{{/if}}	
							{{/each}}
						{{else}}
							<a href="${itemProperties.productEditSeoUrl}" class="bag_edit_item_pdp">{{html convertHTML(trLabels.kls_static_tr_cart_editThisItem)}}</a>
						{{/if}}
					</p>
					{{if $env.ksCnCSaveForLater && itemType != 'VGC' && !Kjs.trCommonCheckout.quantityBlock(item)}}
						<p class="clearfix mtopbottom10">
							<a class="saveForLater" href="javascript:void(0)" id="save-for-later">{{html convertHTML(trLabels.kls_static_saveforlater_text)}}</a>
						</p>
					{{/if}}
					{{if !$env.isKioskEnabled && !trJsonData.fromShoprunner && $env.enableWishList}}
						{{if itemType == 'LTL'}}
							<p id="add_to_list_${cartItemId}" class="add_to_list_bag text_right clearfix add_tolist_registry" style="visibility:hidden;"></p>
						{{else}}
							{{if itemType != 'MARKETPLACE' && itemType != 'VGC' && !Kjs.trCommonCheckout.quantityBlock(item) }}
								<p id="add_to_list_${cartItemId}" class="add_to_list_bag text_right clearfix add_tolist_registry boss_add_tolist_registry"></p>
							{{/if}}
						{{/if}}
					{{/if}}	
					<!--START: WPE-299 -->
					<p class="clearfix mtopbottom10"><a class="bag_remove_item gwp-remove-item-shoppingcart" href="javascript:void(0)" id="remove_${cartItemId}">{{html convertHTML(trLabels.kls_static_tr_cart_remove)}}</a></p>
					<!--END: WPE-299 -->									
				</div>
			</div>	

			{{if (cartItems.length - 1) != i }}<hr/>{{/if}}
		{{/each}}
	</div>
	
	<form method="post" action="/cnc/checkout/cartItems/update" id="cart_update" name="cart_update"></form>
</script>

<script id="sb-checkout-btn" type="text/x-jQuery-tmpl">
<!-- START: Changes for COD-39:  Shopping Bag Changes: Checkout Button -->
	<div class="customer-options-bar">
		{{if trJsonData.fromShoprunner}}
			<div class="sr_expressCheckoutCartContainer">
				<span class="shoprunner_checkout_text">${trLabels.kls_static_shoprunner_checkout_with_msg}</span>
				<div name="sr_expressCheckoutCartDiv"></div>
			</div>
		{{else}}

		   <input type="button" style="" name="Checkout" id="button_checkout_sb_top" value="{{html convertHTML(trLabels.kls_static_boss_shopping_bag_checkout)}}" class="button_checkout_tr boss_button_checkout_tr"/>
		  
		{{/if}}
	</div>
<!-- END: Changes for COD-39:  Shopping Bag Changes: Checkout Button -->
</script>

<script id="sb-quantity-Box" type="text/x-jQuery-tmpl">
	<div class="fleft quantity" style="width:100px;">
		<span class="dec shopping_cart_dec">-</span>
		<input type="text" maxlength="3" name="ship_bill_quantity" {{if errorMsg}}class="ship_bill_quantity ship_bill_quantity_error "{{else}}class="ship_bill_quantity"{{/if}} value=${requestedQuantity != 0 && errorMsg == '' ? requestedQuantity : quantity} size="3" id="${cartItemId}" {{if gwpInfo.getItem}}readonly{{/if}}>
		<span class="inc shopping_cart_inc">+</span>
	</div>
</script>

<form method="post" action="/cnc/checkout/cartItemRemove" id="remove_item_form" name="remove_item_form">
	
</form>


