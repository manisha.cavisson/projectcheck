var s_account = (typeof (multiSuite) !== "undefined") ? multiSuite : External && External.omniture && External.omniture.suitesStandard;	//Value kohlscomdev needs to be fetched from ZK to load the configured value.
			/** START : [RW-532] - Session Timeout Update **/
            if($env.isKioskEnabled){
		       var kioskSecureSoftTimeoutInSeconds = parseInt($env.kioskSecureSoftTimeoutInSeconds),
                   kioskSecurehardTimeoutInSeconds = parseInt($env.kioskSecurehardTimeoutInSeconds),
                   kioskNonSecureSiftTimeoutInSeconds = parseInt($env.kioskNonSecureSiftTimeoutInSeconds),
                   kioskNonSecureHardTimeoutInSeconds = parseInt($env.kioskNonSecureHardTimeoutInSeconds);
            }
            /** END : [RW-532] - Session Timeout Update  **/
function setCommonOmnitureForPageLoad() {
     var
        pageType = arguments.length > 0 ? arguments[0] : 'my account', // Service dependent
        isKioskReq = false,  // From Request Object
        allowedCheckoutPages =['shoppingCartV2', 'checkout'],
        allowedAccountPages =['purchase-history','myAccount','giftCardCheckBalance'];
        var loggedInStatus,transient;
        oneTimeEvents = '';
        if($.inArray($env.page,allowedCheckoutPages) != -1){
            isCartEmpty = trJsonData&&trJsonData.cartItems&&trJsonData.cartItems.length>0?false:true;
            loggedInStatus = (trJsonData && trJsonData.customerDetails && trJsonData.customerDetails.isLoggedIn) ? trJsonData.customerDetails.isLoggedIn:'N/A';
        }
        else if($.inArray($env.page,allowedAccountPages) != -1){
            isCartEmpty = Kjs.cookie.get("VisitorBagTotals")?false:true;
            loggedInStatus = pageData&&pageData.page_details&&pageData.page_details.isUserLoggedIn?pageData.page_details.isUserLoggedIn:'N/A';
            
        }
        else if(window.$env.page=="orderConfirmV2"){
            isCartEmpty = true;
            var isNewGuestLoggedIn = getCookieByName('isNewGuestLoggedIn');
            loggedInStatus = (trJsonData && trJsonData.customerDetails && trJsonData.customerDetails.isLoggedIn) ? trJsonData.customerDetails.isLoggedIn:'N/A';
            if(isNewGuestLoggedIn && isNewGuestLoggedIn === "true"){
                loggedInStatus = "false"; // explicitly set not loggedin where the user new created
                Kjs.cookie.set('isNewGuestLoggedIn', null); // remove this cookie after usage
            }
        }
    $err.exec(function _sitecatalyst() { // [EAEV-12]
        //var trackingCookiePrefix='klsbrwcki|'; // configurable prefix
        var omniPlatform = (External && External.omniture && External.omniture.omniPlatform) ? External.omniture.omniPlatform : $log.error('Please provide omniPlatform omniture data'); // Config
        var profileId = getCookieByName('VisitorId') || '', // Getting from Cookie
            loyaltyID = 'no loyalty id';
            if(profileId==''){
                profileId = trJsonData && trJsonData.customerDetails && trJsonData.customerDetails.customerID || typeof payload != 'undefined' && payload.profile && payload.profile.profileId;
                if(!profileId && window.$env.omniture=='orderDetail'){
                if(customerDetails && customerDetails.customerID){
                    profileId=customerDetails.customerID
                }
                }
            }
            loyaltyID = 'no loyalty id';
        if (loggedInStatus == "true") {
            loyaltyID = (getCookieByName('loyalty_id')) || ((trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.rewards && trJsonData.purchaseEarnings.rewards.loyaltyId)?trJsonData.purchaseEarnings.rewards.loyaltyId:loyaltyID);
            if (loyaltyID == "") {
                loyaltyID = 'no loyalty id';
            }
        }

        s.currencyCode = "USD"; // hardcoded in atg
        s.pageType = pageType;//service
        s.prop4 = s.pageType; // Service
        s.prop9 = s.pageType; // Service 
        s.prop10 = s.pageType; // Service 
        s.prop11 = s.pageType; // Service 
        if(window.External){
            if(loggedInStatus=="false"){
                transient =false;
                s.eVar22 = "desktop";
            }
            else
            {
                transient= true;
            }
            getEvar17Value(transient,loyaltyID);
        s.eVar3 = '';
        if(trJsonData && trJsonData.omnitureServerDate){
        s.prop22 = trJsonData.omnitureServerDate.curDate; // Constructed in Script
        s.eVar18 = trJsonData.omnitureServerDate.hours; // Constructed in js
        s.eVar19 = trJsonData.omnitureServerDate.day; // Constructed in js
        s.eVar20 = trJsonData.omnitureServerDate.currentDayInWeek; // Constructed in js   
        } else if (omnitureServerDate) {
        s.prop22 = omnitureServerDate.curDate; // Constructed in Script
        s.eVar18 = omnitureServerDate.hours; // Constructed in js
        s.eVar19 = omnitureServerDate.day; // Constructed in js
        s.eVar20 = omnitureServerDate.currentDayInWeek; // Constructed in js              
        }        
        s.prop50 = "D=s_tempsess"; // Hardcoded    
        loggedInStatus=="false"?s.eVar70 = profileId:s.eVar39 = loggedInStatus=="true"?profileId:"";
        if (!s.eVar39) {
            if(window.$env.page=='myAccount' && window.$env.omniture=='manageOrder'){
            s.eVar39 = "";
            s.prop10 = "";
            s.prop11 = "";
            }
        }
// Set omniture for free shipping message in store
if($env.ksFreeShipMsgShoppingCart && trJsonData.purchaseEarnings && !trJsonData.purchaseEarnings.loyaltyPilotUser && $env.page == 'shoppingCartV2'){
    //KOSPC-20335: In order to able remove and add right value in existing field.
	if(s.prop75){
		var values = s.prop75.split(',');
		for(var i = 0; i < values.length; i ++){
			if(values[i] && values[i].indexOf('fsrec') > 0){
				values[i] = '';
			}
		}
		var remergeValue = '';
		for(var i = 0; i < values.length; i ++){
			if(values[i]){
				remergeValue = remergeValue+values[i] +',';
			}
		}
		s.prop75 = remergeValue;
	}else{
		s.prop75 = '';
	}
    if(Kjs.trOrderSummary.getFreeShipDelta() > 0){
        s.prop75=  s.prop75+'w>fsrec|<$' + Kjs.trOrderSummary.getFreeShipDelta().toFixed(2) + '>';
    } else  {
        s.prop75=  s.prop75+'w>fsrec|<$0>';
    }
}

        if(trJsonData && trJsonData.pilotProgram){
            if((trJsonData && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser) || (trJsonData && trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser)){
                if ((trJsonData && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings && trJsonData.purchaseEarnings.kohlsCashEarnings.loyaltySystem == false) || ($env.page == "orderConfirmV2" && trJsonData && trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltySystem == false )) {
                    if (s.prop52 == "") {
                        s.prop52 = "ERR_CART_LOYALTY_001|kohls cash earn service failed";
                    } else if (s.prop52.indexOf("err_cart_loyalty_001|kohls cash earn service failed") == -1) {
                        s.prop52 += ",ERR_CART_LOYALTY_001|kohls cash earn service failed";
                    }
                }
            }
            if (!(trJsonData && trJsonData.customerDetails && trJsonData.customerDetails.rewardsPilotService)) {
                s.prop52 == "" ? s.prop52 = "ERR_CART_LOYALTY_002|rewards pilot check failed" : s.prop52 += ",ERR_CART_LOYALTY_002|rewards pilot check failed"; //fix for KOSPC-10072
            } //KOSPC-12886
        }
        s.eVar40 = omniPlatform; //Service dependent related to profile platform
        var orderDetailsMap = JSON.parse((External && External.omniture && External.omniture.orderDetailsMap)?External.omniture.orderDetailsMap:$log.error('Please provide omniture data'));
        s.eVar42 = orderDetailsMap.emptyCart;
        if (!isCartEmpty){
            s.eVar42 = orderDetailsMap[!transient];
        }
        s.eVar68 = s.pageName;
         s.eVar71 = ((External && External.omniture && External.omniture.trackingCookiePrefix)?External.omniture.trackingCookiePrefix:$log.error('Please provide omniture data')) + ((profileId!='')?profileId:$log.error('Please provide omniture data')); //${eVar71Prefix}${anonymousProfileId}${loggedInProfileId}
            s.eVar73 = loyaltyID //Service/Cookie dependent
        if (document.URL.indexOf('giftcard/gift_card_check_balance.jsp') !== -1) {
            s.pageName = "";
            s.pageType = "";
            s.events = "";
            s.prop4 = "";
            s.prop9 = "";
            s.prop10 = "";
            s.prop11 = "";
        }

        // KOSPC-6792 
        if (window.$env.page == "orderConfirmV2") {
            if (trJsonData && trJsonData.welcomePilotOverlay && trJsonData.ksCnCLoyaltyWelcomeOverlayEnabled) {
                if (getCookieByName('getRegistered')=="true"){
                    var VisitorUsaFullName, account_creation_failed = false;
                    VisitorUsaFullName = getCookieByName('VisitorUsaFullName');
                    if (VisitorUsaFullName!==""){
                        if (trJsonData && trJsonData.errors && trJsonData.errors.length > 0) {
                            $.each(trJsonData.errors, function (i, currentError) {
                                if(currentError.code=="ERR_ORDER_040"){
                                    account_creation_failed=true;
                                }
                            });
                        }
                        if (!account_creation_failed) {
                            s.events == "" ? s.events = "event37" : s.events = s.events + ",event37";
                        }
                    }
                }
                if (trJsonData && trJsonData.loyaltyInfo && trJsonData.loyaltyInfo.enrollLoyalty) {
                    s.events == "" ? s.events = "event50" : s.events = s.events + ",event50";
                }
                if (trJsonData && trJsonData.userData && trJsonData.userData.loyaltyId != "") {
                    if (trJsonData && trJsonData.loyaltyInfo && trJsonData.loyaltyInfo && trJsonData.loyaltyInfo.errors && !trJsonData.loyaltyInfo.errors.length > 0) {
                        s.events == "" ? s.events = "event51" : s.events = s.events + ",event51";
                    }
                }
            }
            // if (trJsonData.isGuest = "true") {
            //     s.events == "" ? s.events = "event37" : s.events = s.events + ",event37"; // KOSPC-11925
            // }
            if (trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser && trJsonData.orderSummary.purchaseEarnings.loyaltySystem){
                s.events == "" ? s.events = "event121" : s.events = s.events + ",event121";
            }
            if (trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser){
                //First cookie 'userFormDataLoyaltyId' captures user input in Billing page and the 'loyalty_id' captures user action in AM pages.
                if (getCookieByName('userFormDataLoyaltyId')=="" && getCookieByName('loyalty_id')==""){
                        s.events == "" ? s.events = "event120" : s.events = s.events + ",event120";
                }
            }

            /* set eVar95 : Two values separated by a pipe.		 KOSPC-11824	
                // First value: Kohls Charge on file (yes/no)			
                // Second value: Current primary payment method that is set in their account
            */
            if (typeof $env.sessionStatus !== 'undefined' && $env.sessionStatus) {
                if ($env.sessionStatus == 'ANONYMOUS') {
                    s.eVar95 = 'guest';
                } else {
                    if ($env.haveKohlsCharge == 'regular' && $env.preferredPaymentType) {
                        s.eVar95 = 'yes|kohls charge';
                    } else if ($env.haveKohlsCharge == 'regular') {
                        s.eVar95 = 'yes|non-KCC';
                    } else if ($env.haveKohlsCharge == 'elite' && $env.preferredPaymentType) {
                        s.eVar95 = 'yes|elite kohls charge';
                    } else if ($env.haveKohlsCharge == 'elite' && !$env.preferredPaymentType) {
                        s.eVar95 = 'yes|non-KCC';
                        if(trJsonData && trJsonData.paymentDetails && trJsonData.paymentDetails.creditCardDetails && trJsonData.paymentDetails.creditCardDetails.length){
                            trJsonData.paymentDetails.creditCardDetails.forEach(function(card){
                                if(card.paymentType.toLowerCase()==="kohlscharge" || card.paymentType.toLowerCase()==="kohls charge"){
                                    s.eVar95 = "yes|kohls charge";
                                }
                            });
                        }
                    } else if ($env.eliteCardOnProfile && $env.elitepreferredPaymentTypeSet) {
                        s.eVar95 = 'yes|elite kohls charge';
                    } else if ($env.eliteCardOnProfile && !$env.elitepreferredPaymentTypeSet) {
                        s.eVar95 = 'yes|non-KCC';
                    } else {
                        s.eVar95 = 'no|no primary';
                        if(trJsonData && trJsonData.paymentDetails && trJsonData.paymentDetails.creditCardDetails && trJsonData.paymentDetails.creditCardDetails.length){
                            trJsonData.paymentDetails.creditCardDetails.forEach(function(card){
                                if(card.paymentType.toLowerCase()==="visa"){
                                    s.eVar95 = "no|visa";
                                }
                            });
                        }
                    }
                }
            }
        }
        Kjs.storage.clearCookie('getRegistered');
            // end KOSPC-6792 
        }
        else {
            $log.error('Please provide omniture data');
        }
        if (isKioskReq) {
            s.eVar22 = "desktop responsive";	//Service
            s.eVar43 = "";
        }else{
            s.eVar22 = "desktop";
        }
        // /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
        // var s_code=s.t();if(s_code)document.write(s_code);
        if (typeof (s) === 'undefined') {
            $log.warn('siteCatalyst.js - captureOmniturePageName - Omniture API not present');
            return;
        }
        s.t();
    });
}

function setUserTrackingBean(pageName, trackingId) {
        if (typeof (s) === 'undefined') {
            $log.warn('siteCatalyst.js - captureOmniturePageName - Omniture API not present');
            return;
        }
        if(window.$env.page=='shoppingCartV2' || window.$env.page =='checkout'){
            // KOSPE-1380, trackingId = 8 i.e Item removed from cart, s.products should have
            // item which is removed, it is already handled at the time of item removal
            if (trJsonData && trJsonData.cartItems && trackingId != 8) {
                s.products = Kjs.commonGlobalFunctions.omnitureCheckoutGetProductIdsFromCart(trJsonData);
            }
         s.eVar48 = (trJsonData && trJsonData.cartId)?trJsonData.cartId:'';
        }
        var evar60 = '';
        if(trJsonData && trJsonData.orderSummary && trJsonData.orderSummary.promoCodes && trJsonData.orderSummary.promoCodes.length > 0){
            $(trJsonData.orderSummary.promoCodes).each(function(i,promo){
                if(promo && promo.type && promo.type=='TLD'){
                    evar60 += (evar60!=''?',':'')+'o_'+promo.id;
                }
            });
        }

        //oneTimeEventList contains the one time events used for cleaning up s.events after the event is thrown.
        var oneTimeEventList=['scCheckout','event67'];
        //oneTimeList is a global var used to push the event 
        //into s.events when needed and clear the same for the other operations
        oneTimeEventList.forEach(function(event,i){
            //replace is called twice to remove the event and comma appropriately
            s.events = s.events.replace(event+",",'');
            s.events = s.events.replace(event,'');
            
        });
        
        if(typeof oneTimeEvents != 'undefined' && oneTimeEvents){
            s.events += (s.events==''?'':',')+ oneTimeEvents;
            oneTimeEvents = '';
        }

    var userTrackingBean = {
        "enable_rich_relevance": "true",
        "currencyCode": s.currencyCode,
        "pageName": s.pageName,
        "pageType": s.PageType,
        "prop1": "",
        "prop2": "",
        "prop3": "",
        "prop4": s.pageType,
        "prop5": "",
        "prop6": "",
        "prop7": "",
        "prop9": s.pageType,
        "prop10": s.pageType,
        "prop11": s.pageType,
        "prop16": "",
        "prop17": s.prop17,
        "prop22": s.prop22,
        "prop24": "",
        "prop25": "",
        "prop37": "",
        "prop38": "",
        "prop43": "",
        "prop44": "",
        "prop45": "",
        "prop46": "",
        "prop50": s.prop50,
        "prop52": s.prop52,
        "eVar22": "desktop",
        "eVar3": "",
        "eVar8": "",
        "eVar9": s.pageType,
        "events": s.events,
        "products": s.products,
        "eVar5": "",
        "eVar14": "",
        "eVar17": s.prop17,
        "eVar18": s.eVar18,
        "eVar19": s.eVar19,
        "eVar20": s.eVar20,
        "eVar23": "",
        "eVar24": "",
        "eVar25": "",
        "eVar26": "",
        "eVar27": "",
        "eVar28": "",
        "eVar35": "",
        "eVar36": "",
        "eVar39": s.eVar39,
        "eVar40": s.eVar40,
        "eVar42": s.eVar42,
        "eVar46": "",
        "eVar47": "",
        "eVar48": s.eVar48,
        "state": "",
        "zip": "",
        "eVar59": "",
        "eVar60": evar60,
        "eVar61": "",
        "eVar68": s.eVar68,
        "purchaseID": "",
        "eVar71": s.eVar71,
        "eVar73": s.eVar73,
        "eVar7": ""
    };
    if(pageName == 'order confirm' && evar60){
        s.eVar60 = evar60;
    }

    // Add 'userTrackingBean' to trJsonData object
    if (trJsonData) {
        trJsonData['userTrackingBean'] = userTrackingBean;
    }
}

function captureOmniturePageName(pageName) {
    $err.exec(function _captureomnipagename() { // [EAEV-12]
        if (typeof (s) === 'undefined') {
            $log.warn('siteCatalyst.js - captureOmniturePageName - Omniture API not present');
            return;
        }
        /*Start: modified for ATG-3969 changes*/
        s.pageName = s.eVar68 = s.prop53 = s.prop4 = s.prop9 = pageName;
        //Fix for ATG-4092
        if (!s.events) {
            s.events = '';
        }
        if(!s.prop52){
            s.prop52 = '';
        }
        if (!s.products) {
            s.products = '';
        }
        /*End: modified for ATG-3969 changes*/
    });
}

function getCookieByName(n) {
    return Kjs.cookie.get(n);	//[EAEV-130]
}

function setOmniProducts(cartItem) {
    if (!$omniture.isOmnitureExists() || !cartItem) {
        return;
    }
    var productString = '',
        totalPrice = cartItem.itemPriceInfo && cartItem.itemPriceInfo.promoAdjustedExtendedPrice ? cartItem.itemPriceInfo.promoAdjustedExtendedPrice : 0;

    productString = ';' + cartItem.skuId + ';' + cartItem.quantity + ';' + totalPrice;
    $omniture.setVar('products',productString);
}

function getEvar17Value(transient,loyaltyID){
//code for evar17 prop17 start
    // Transient depending on profile info coming from service
    var // transient = false, // Service/Cookie dependent related to profile information
        omniKohlsLogin = (External && External.omniture && External.omniture.omniKohlsLogin) ? External.omniture.omniKohlsLogin : $log.error('Please provide omniture data'), // Config
        omniKohlsNotLogin = (External && External.omniture && External.omniture.omniKohlsNotLogin) ? External.omniture.omniKohlsNotLogin : $log.error('Please provide omniture data'), // Config
        loyLoginStatus = (External && External.omniture && External.omniture.omniLoyNotLogin) ? External.omniture.omniLoyNotLogin : $log.error('Please provide omniture data'), // Config
        omniLoyLogin = (External && External.omniture && External.omniture.omniLoyLogin) ? External.omniture.omniLoyLogin : $log.error('Please provide omniture data'),
        omniLoyLogin2018 = (External && External.omniture && External.omniture.omniLoyLogin2018) ? External.omniture.omniLoyLogin2018 : $log.error('Please provide omniture data'),
        omniLoyEliteLogin2018 = (External && External.omniture && External.omniture.omniLoyEliteLogin2018) ? External.omniture.omniLoyEliteLogin2018 : $log.error('Please provide omniture data'),
        loyaltymvcloggedin = (External && External.omniture && External.omniture.loyaltymvcloggedin) ? External.omniture.loyaltymvcloggedin : $log.error('Please provide omniture data'),
        kohlssoftloggedin = (External && External.omniture && External.omniture.kohlssoftloggedin) ? External.omniture.kohlssoftloggedin : $log.error('Please provide omniture data'),
        loyaltymvcsoftloggedin = (External && External.omniture && External.omniture.loyaltymvcsoftloggedin) ? External.omniture.loyaltymvcsoftloggedin : $log.error('Please provide omniture data'),
        mvckohlscharge = (External && External.omniture && External.omniture.mvckohlscharge) ? External.omniture.mvckohlscharge : $log.error('Please provide omniture data');
        
        if(window.$env.page=='orderConfirmV2'){
            if (trJsonData && trJsonData.loyaltyInfo && trJsonData.loyaltyInfo.loyaltyId) {
                if (trJsonData.loyaltyInfo.loyaltyId != '') {
                    loyaltyID = trJsonData.loyaltyInfo.loyaltyId;
                }
            }
            //KOSPC-18673
            if (trJsonData && trJsonData.nuData && trJsonData.nuData.interdictionType && trJsonData.nuData.interdictionType.toLowerCase() == '3ds') {
                s.prop60 = 'nd:on|3ds|' + trJsonData.nuData.threeDSAuthStatus;
            }
            //For LPFV2 Omniture Requirement KOSPC-19034 --second Part KOSPC-19977
            if($env.ksLoyaltyV2 && trJsonData && trJsonData.paymentDetails && trJsonData.paymentDetails.creditCardDetails && trJsonData.paymentDetails.creditCardDetails.length > 0 && !trJsonData.paymentDetails.creditCardDetails[0].eliteStatus && trJsonData.paymentDetails.creditCardDetails[0].mvc){ 
                 s.eVar7 = mvckohlscharge;
            }
        }    
        if (loyaltyID !== 'no loyalty id') {
        loyLoginStatus = omniLoyLogin;
        if($env.page == 'checkout' && trJsonData.eliteLoyaltyData){
            $env.isMvc = trJsonData.eliteLoyaltyData.mvc;
        }

        //For LPFV2 Omniture Requirement KOSPC-19034
        //KOSPC-20729: condition for Order review in case of new card added
        if ($env.ksLoyaltyV2 && trJsonData && trJsonData.purchaseEarnings && !trJsonData.purchaseEarnings.loyaltyPilotUser && $env.isMvc) {
            if (trJsonData.userData.sessionStatus == 'LOGGEDIN' || ($env.page == 'checkout' && trJsonData.customerDetails && trJsonData.customerDetails.isLoggedIn=='true')) {
                loyLoginStatus = loyaltymvcloggedin;

            } else if (trJsonData.userData.sessionStatus == 'SOFTLOGGEDIN') {
                loyLoginStatus = loyaltymvcsoftloggedin;
                omniKohlsLogin = kohlssoftloggedin;
            }
        }
        else if (trJsonData && trJsonData.pilotProgram && !$env.isElite) {
            if ((trJsonData && trJsonData.purchaseEarnings && trJsonData.purchaseEarnings.loyaltyPilotUser) || (trJsonData && trJsonData.orderSummary && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser)) {
                loyLoginStatus = omniLoyLogin2018
            }
        } else if ($env.isElite && $env.eliteProgram) {
            loyLoginStatus = omniLoyEliteLogin2018
        }
    } else if (trJsonData && trJsonData.isGuest == "true" && trJsonData.orderSummary.purchaseEarnings && trJsonData.orderSummary.purchaseEarnings.loyaltyPilotUser) {
        //Added for KOSPC-18712 "prop17 and eVar17 are not as expected in ocp for orders which is placed using Elite KCC for Guest user"
        if (trJsonData.paymentDetails && trJsonData.paymentDetails.creditCardDetails && trJsonData.paymentDetails.creditCardDetails[0] && trJsonData.paymentDetails.creditCardDetails[0].eliteStatus) {
            loyLoginStatus = omniLoyEliteLogin2018;
        } else {
            loyLoginStatus = omniLoyLogin2018;
        }
    }
    if (transient) {
        s.prop17 = s.eVar17 = omniKohlsLogin + '|' + loyLoginStatus;
    } else {
        s.prop17 = s.eVar17 = omniKohlsNotLogin + '|' + loyLoginStatus;
    }
    //code for evar17 prop17 end
}