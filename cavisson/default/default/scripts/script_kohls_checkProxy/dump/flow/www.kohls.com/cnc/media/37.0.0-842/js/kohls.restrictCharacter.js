(function($) {
    $.fn.extend({
        restrictCharacter: function(pattern) {
            var targets = $(this);
            var restrictHandler = function() {
                var val = $(this).val();
                var newVal = val.replace(pattern, "");
                if (val !== newVal) {
                    $(this).val(newVal)
                }
            };
            /*
             * Unbind event before binding again. This will help solve the
             * problem of unnecessary(multiple & redundant) function calls
             */
            targets.off("keyup").on("keyup", restrictHandler);
            targets.off("paste").on("paste", restrictHandler);
            targets.off("change").on("change", restrictHandler);
            targets.off("blur").on("blur", restrictHandler)
        }
    })
})(jQuery);