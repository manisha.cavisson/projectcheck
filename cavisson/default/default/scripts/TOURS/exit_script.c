/*-----------------------------------------------------------------------------
    Name: exit_script
    Recorded By: puja
    Date of recording: 04/02/2021 12:22:02
    Flow details:
    Build details: 4.6.0 (build# 42)
    Modification History:
-----------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ns_string.h"

int exit_script()
{
    return 0;
}
